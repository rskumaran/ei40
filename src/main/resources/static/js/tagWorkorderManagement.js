app
	.controller(
		'tagWorkorderManagementController',
		function($scope, $http, $window, $rootScope, $location, $timeout) {
			$scope.user = JSON.parse($window.sessionStorage
					.getItem("user"));
			if ($scope.user == null) {
				$location.path('/login');
				return;
			}
			$scope.workOrderTagTypes =["Material", "RMA", "Parts"];
			$scope.authen ={};
			$scope.checkTagMacId = {};
			$scope.checkTagMacId ={};
			$scope.checkTagTagId ={};
			$scope.checkTagType ={};
			$scope.checkTagWorkOrderId ={};
			$scope.tagData = new Array();
			$scope.tagViewData = new Array();
			$scope.tagDataFilterList = new Array();
			$scope.searchText = "";
			//$scope.tagPeopleViewScreen = true;
			$scope.searchButton = true;
			$scope.taggID = "";
			$scope.tagRenameData = [];
			$scope.workorderArray = [];
			$scope.checktagIdTable=[];
			if ($scope.fieldName == undefined) {
				$scope.fieldName = {};
			}
			
 			
			$scope.varTagIdTextAssigned = '<div style="color:blue">';
			$scope.varTagIdTextFree = '<div style="color:green">';
			$scope.varTagIdTextNoTag = '<div style="color:red">';
			$scope.fetchTagIdTable=[];
			$('.select2').select2(); // searchable dropdown  in fetch screen intialization 
			$('#checktagIdSelect').select2(); 
			//to enable/disable sub menus
			$scope.user = JSON.parse($window.sessionStorage.getItem("user"));
			//if ($scope.user != null) {
				//$rootScope.$broadcast('updatemenu', $scope.user.role);
			if( $scope.user.capabilityDetails != undefined && $scope.user.capabilityDetails != null){
				if ($scope.user.capabilityDetails.WTVA ==1){
					$scope.showViewAllBtn = true;	   	
			    }
				else{
					$scope.showViewAllBtn = false;
				}
				if ($scope.user.capabilityDetails.WTAR ==1){
					$scope.showAssignReturnBtn = true;
				}
				else{
					$scope.showAssignReturnBtn = false;
				}
				if ($scope.user.capabilityDetails.WTED ==1){
					$scope.showEditBtn = true;
				}
				else{
					$scope.showEditBtn = false;
				}
				if ($scope.user.capabilityDetails.WTRN ==1){
					$scope.showRenameBtn = true;
				}
				else{
					$scope.showRenameBtn = false;
				}
				
			}
			// input field validation
			$("#id_currentTag, #id_newTagId, #checktagIdSelect, #id_workOrderIdAssignTag, #id_currentTag, #id_newTagId").keypress(
					function(e) {
						// if the letter is not alphabets then display error
						// and don't type anything
						$return = ((e.which > 64) && (e.which < 91)) || ((e.which> 96) && (e.which < 123)) ||(e.which == 8) || (e.which == 32)  || (e.which >= 48 && e.which <= 57)
						if (!$return)
											
						{
							
							return false;
						}
					});	

			$scope.searchOption = function() {
				$scope.filterOption();
			}

			$scope.filterOption = function() {

				$scope.tagViewData = [];
				$scope.tagDataFilterList = [];
				
				for (var i = 0; i < $scope.tagData.length; i++) {							

					if ($scope.searchText == '') {
						$scope.tagDataFilterList
								.push($scope.tagData[i]);
					} else {
						if (
								($scope.tagData[i].tagId != undefined
										&& $scope.tagData[i].tagId != null ? $scope.tagData[i].tagId
										: "").toLowerCase()
										.includes(
										$scope.searchText.toLowerCase())
								/*||($scope.tagData[i].groupId != undefined
										&& $scope.tagData[i].groupId != null ? $scope.tagData[i].groupId
												: "").toLowerCase()
												.includes(
										$scope.searchText.toLowerCase())*/
								|| ($scope.tagData[i].macId != undefined
										&& $scope.tagData[i].macId != null ? $scope.tagData[i].macId
												: "")
										.toLowerCase().includes(
												$scope.searchText
														.toLowerCase())
								|| ($scope.tagData[i].type != undefined
												&& $scope.tagData[i].type != null ? $scope.tagData[i].type
														: "").toLowerCase()
														.includes(
												$scope.searchText.toLowerCase())						
								/*||($scope.tagData[i].manufacturer != undefined
										&& $scope.tagData[i].manufacturer != null ? $scope.tagData[i].manufacturer
												: "").toLowerCase()
								.includes(
												$scope.searchText
														.toLowerCase())*/
								|| ($scope.tagData[i].status != undefined
										&& $scope.tagData[i].status != null ? $scope.tagData[i].status
												: "").toLowerCase()
								
												.includes(
												$scope.searchText
														.toLowerCase())
								|| ($scope.tagData[i].assignmentId != undefined
										&& $scope.tagData[i].assignmentId != null ? $scope.tagData[i].assignmentId
										: "").toLowerCase()
										.includes(
												$scope.searchText
														.toLowerCase())
								|| ($scope.tagData[i].assignmentDate != undefined
										&& $scope.tagData[i].assignmentDate != null ? $scope.tagData[i].assignmentDate
										: "").toLowerCase()
										.includes(
												$scope.searchText
														.toLowerCase())
								/*|| ($scope.tagData[i].createdDate != undefined
										&& $scope.tagData[i].createdDate != null ? $scope.tagData[i].createdDate
										: "").toLowerCase()
										.includes(
												$scope.searchText
														.toLowerCase())*/
								|| ($scope.tagData[i].modifiedDate != undefined
										&& $scope.tagData[i].modifiedDate != null ? $scope.tagData[i].modifiedDate
										: "").toLowerCase()
										.includes(
												$scope.searchText
														.toLowerCase())) {
							$scope.tagDataFilterList
									.push($scope.tagData[i]);

						}

					}
					
				}

				$scope.totalItems = $scope.tagDataFilterList.length;
				$scope.currentPage = 1;
				$scope.getTotalPages();

				if ($scope.tagData.length == 0
						|| $scope.tagDataFilterList.length == 0) {
					$scope.tagNoData = true;

				} else {
					
					$scope.tagNoData = false;

			if ($scope.tagDataFilterList.length > $scope.numPerPage){
				$scope.tagViewData = $scope.tagDataFilterList
						.slice(0, $scope.numPerPage);
			$scope.PaginationTab=true;
			}
			else {
				$scope.tagViewData = $scope.tagDataFilterList;
				$scope.PaginationTab=false;
			}
		
				}
			}
			//API call for Work Order ID dropdown 
			
		/*	$scope.getMaterialViewAllData = function() {

				 $rootScope.showloading('#loadingBar');
				 if ($rootScope.checkNetconnection() == false) { 
						$rootScope.hideloading('#loadingBar');						
				         $.alert({
						          title: $rootScope.MSGBOX_TITLE,
			                       content: 'Please check the Internet Connection',
			                       icon: 'fa fa-info-circle',
			                    });
								    return;
								 } 
				$http({
					method : 'GET',
					url : './material',

				})
						.then(
								function(response) {
									 $rootScope.hideloading('#loadingBar');
									if (response != null
											&& response.data != null
											&& response.data.length != 0
											&& response.data != "BAD_REQUEST"
											&& response.data != undefined
											&& response.data.length != 0) {

										
										$scope.workorderArray.length = 0;

										for (var i = 0; i < response.data.length; i++) {
											if (response.data[i].active == true) {														
												if(response.data[i].ordereNo != null && response.data[i].ordereNo != "" && response.data[i].ordereNo != undefined)
												$scope.workorderArray.push(response.data[i].ordereNo);
											}
										}
										
									
										if ($scope.workorderArray.length == 0) {
											$scope.workorderArray.push("No Order Id ");
										} 
										
									} else {
										$scope.workorderArray.push("No Order Id ");
										
									}
								},
								function error(response) {
									$rootScope
											.hideloading('#loadingBar');
									if (response.status = 403) {
										$
												.alert({
													title : $rootScope.MSGBOX_TITLE,
													content : 'Session timed-out or Invalid. Please logout and login again',
													type : 'blue',
													columnClass : 'small',
													autoClose : false,
													icon : 'fa fa-info-circle',
												});
									}

								});
			}*/
			
			
			//
			
			
			
			// kumar 02/Jul filter option <-
			$scope.editWorkOrderTag = function() {
				$scope.tagWorkOrderEditScreen = false;
				$scope.tagWorkOrderUpdateScreen = true;
				$scope.searchButton = false;
				if($scope.authen.status == "Assigned" ||$scope.authen.status == "Blocked"|| $scope.authen.status == "Overdue")
					$scope.editStatusAssignmentType = true;
				else if($scope.authen.status == "Free" ||$scope.authen.status == "NotWorking"|| $scope.authen.status == "Expired")
					$scope.editStatusAssignmentType = false;
			}
			
			$('#datetimepicker4').datetimepicker({
				format : 'YYYY-MMM-DD',
				Default: true,
				minDate : new Date()
			});
			 $('#stimepicker').datetimepicker({
				 
			      format: 'HH:mm'
		    });
        $('#etimepicker').datetimepicker({
			      format: 'HH:mm'
		    });
        
			//---------------
        var reA = /[^a-zA-Z]/g;
		var reN = /[^0-9]/g;

		function sortAlphaNum(a1, b1) {
			var a = a1.tagId;
			var b = b1.tagId;
			var aA = a.replace(reA, "");
			var bA = b.replace(reA, "");
			if (aA === bA) {
				var aN = parseInt(a.replace(reN, ""), 10);
				var bN = parseInt(b.replace(reN, ""), 10);
				return aN === bN ? 0 : aN > bN ? 1 : -1;
			} else {
				return aA > bA ? 1 : -1;
			}
		}
			$scope.getTagData = function() {
				if (!$rootScope.checkNetconnection()) { 
					$rootScope.alertDialogChecknet();
					return; 
					}
				$rootScope.showloading('#loadingBar');
			$http({
				method : 'GET',
				url : './tag',
				
			})
					.then(
							function(response) {
								$rootScope
								.hideloading('#loadingBar');
								if (response != null
										&& response.data != null
										&& response.data.length != 0
										&& response.data != "BAD_REQUEST"
										&& response.data != undefined
										&& response.data.length != 0) {

										$scope.tagData.length = 0;									
										response.data
										.sort(sortAlphaNum);
										//$scope.tagData = response.data;
										
										for(var i=0; i< response.data.length; i++){
											var fromTime ="",toTime ="" ,assignmentDate="", createdDate="",modifiedDate="";
											if(response.data[i].fromTime != null && response.data[i].fromTime != "" && response.data[i].fromTime != undefined){
												 fromTime =response.data[i].fromTime;
											}
											if(response.data[i].toTime != null && response.data[i].toTime != "" && response.data[i].toTime != undefined){
												toTime =response.data[i].toTime;
											}
											if(response.data[i].createdDate != null && response.data[i].createdDate != "" && response.data[i].createdDate != undefined){
												response.data[i].createdDate = response.data[i].createdDate.substring(0, response.data[i].createdDate.length-9);
												 createdDate = moment(response.data[i].createdDate,"YYYY-MMM-DD").format("MMM DD, YYYY");
											}
											if(response.data[i].modifiedDate != null && response.data[i].modifiedDate != "" && response.data[i].modifiedDate != undefined){
											response.data[i].modifiedDate = response.data[i].modifiedDate.substring(0, response.data[i].modifiedDate.length-9);													
											 modifiedDate = moment(response.data[i].modifiedDate,"YYYY-MMM-DD").format("MMM DD, YYYY");
											}
											if(response.data[i].assignmentDate != null && response.data[i].assignmentDate != "" && response.data[i].assignmentDate != undefined){
												response.data[i].assignmentDate = response.data[i].assignmentDate.substring(0, response.data[i].assignmentDate.length-9);													
												assignmentDate = moment(response.data[i].assignmentDate,"YYYY-MMM-DD").format("MMM DD, YYYY");
												}
											var tagType = response.data[i].type
											if (response.data[i].type == "Asset Chrono" || response.data[i].type == "Asset NonChrono" || response.data[i].type == "Materials")
												tagType = response.data[i].subType
												
												var workOrderTagType = response.data[i].type;
												var tagStatus =response.data[i].status;
												if (workOrderTagType == "Materials" || tagStatus == "Free" || 
													(tagStatus == "Lost" && (workOrderTagType == "Materials")))
												{	
											$scope.tagData.push( {
											'tagId':  response.data[i].tagId ,
											'groupId':  response.data[i].groupId,
											'macId':  response.data[i].macId,
											'manufacturer':  response.data[i].manufacturer,
											'status':  response.data[i].status,
											'assignmentId':  response.data[i].assignmentId,
											'assignmentDate':assignmentDate,
											'fromTime':  fromTime,
											'toTime': toTime,
											'createdDate':  createdDate,
											'modifiedDate': modifiedDate,
											'major': response.data[i].major,
											'minor': response.data[i].minor,
											'type': tagType,
											'restart':response.data[i].restart
											});
												}
											}
										/*$scope.tagRenameData.length	= 0;
										for(var j=0; j< response.data.length; j++){
											if(response.data[j].renamed == 0){
												$scope.tagRenameData.push(response.data[j].macId);
											}
										 
												
										}
										if ($scope.tagRenameData.length == 0)
											$scope.tagRenameData.push("No Tags Found ");
										*/
										
										//$scope.checktagIdTable.length	= 0;
										$scope.fetchTagIdTable = [];
										for(var k=0; k< response.data.length; k++){
											var varTagIdRec={};
											if(response.data[k].tagId != undefined && response.data[k].tagId != '' && response.data[k].tagId !=null){
												if(response.data[k].type != undefined && response.data[k].type != '' && response.data[k].type !=null){
												if(response.data[k].type == "Materials") {
													varTagIdRec['id'] = response.data[k].tagId
													varTagIdRec['text'] = $scope.varTagIdTextAssigned + response.data[k].tagId + '<div>';
													$scope.fetchTagIdTable.push(varTagIdRec);
												}
													
													}
												if(response.data[k].status != undefined && response.data[k].status != '' && response.data[k].status !=null){
													if(response.data[k].status == "Free"){

														//$scope.checktagIdTable.push(response.data[k].tagId);
														varTagIdRec['id'] = response.data[k].tagId
														varTagIdRec['text'] = $scope.varTagIdTextFree + response.data[k].tagId + '<div>';
														$scope.fetchTagIdTable.push(varTagIdRec);
													
													}
												}
											}
										 
												
										}
										/*if ($scope.checktagIdTable.length == 0)
										$scope.checktagIdTable.push("No Tags Found ");*/
									if ($scope.fetchTagIdTable.length == 0){
										varTagIdRec['id'] = "NoTag";
										varTagIdRec['text'] = $scope.varTagIdTextFree + 'No Tags Found' + '<div>';
										
									}
									$('#checktagIdSelect').select2('destroy');
									$('#checktagIdSelect').select2({
										data: $scope.fetchTagIdTable,
									      templateResult: function (d) {
									    	  //$scope.fieldName.checktagId=$(d.text);
									    	  return $(d.text); 
									    	  },
									      templateSelection: function (d) { 
									    	  return $(d.text); 
									    	  },
									});
									$('#checktagIdSelect').trigger('change.select2'); 
								
									//$scope.fieldName.checktagId=$scope.checktagIdTable[0];
										//----
									if ($scope.tagData.length == 0) {
										$scope.tagNoData = true;
										return;
									} else {
										$scope.tagNoData = false;
									}
									
									if ($scope.tagData.length > 10) {
										$scope.PaginationTab = true;
									} else {
										$scope.PaginationTab = false;
									}
									$scope.totalItems = $scope.tagData.length;
									

									$scope.getTotalPages();

									$scope.currentPage = 1;
									if ($scope.tagData.length > $scope.numPerPage) {
										$scope.tagViewData = $scope.tagData
												.slice(
														0,
														$scope.numPerPage);
										$scope.hasPrevious = true;
										$scope.isFirstPage = true;
										$scope.hasNext = false;
										$scope.isLastPage = false;
									} else {
										$scope.tagViewData = $scope.tagData;
									}
								}else{
									$scope.tagNoData = true;
								}
						
						},
						function error(response) {
							$rootScope.hideloading('#loadingBar');
							$rootScope.fnHttpError(response);
						});
						
		
		}
			
			//$scope.getTagData();
			
			// assign Tag
			
			$scope.assignTag = function (){
				
				var	assignTagParameter ={
							
						 "macId": $scope.checkTagMacId,
					      "type": "workmetrial",
					      "subType": $scope.authen.tagSubType,
					      "peopleId": $scope.authen.workOrderId
							
					};
					
				var workOrderId = document.getElementById("id_workOrderIdAssignTag");
				 if (workOrderId.value.length == 0) {
					 	$scope.workOrderIdError = true;
						$scope.workOrderId_error_msg = "Please enter work order Id ";
						$timeout(function() {
							$scope.workOrderIdError = false;

						}, 2000);

						return;
					}
				 if (!$rootScope.checkNetconnection()) { 
						$rootScope.alertDialogChecknet();
						return; 
						}
					$rootScope.showloading('#loadingBar');
				 $scope.promise= $http.post('./allocation',assignTagParameter)
		          .then(function(result) {
		        	  $rootScope.hideloading('#loadingBar');
		        	  
			        	if (result.data.status == "success"){
			        		$rootScope.alertDialog(result.data.message);
			        		// iterate over each element in the array
			        		 
								for (var i = 0; i < $scope.fetchTagIdTable.length; i++){
								  // look for the entry with a matching `code` value
								  if ($scope.fetchTagIdTable[i].id == $scope.taggID){
									  $scope.fetchTagIdTable[i].text =  $scope.varTagIdTextAssigned + $scope.taggID + '<div>';
									  break;
								  }
								}
			        		 
			        		 $scope.cancelTagAssign();
	                     							
			        	}else if(result.data.status == "error"){
			        		$rootScope.alertDialog(result.data.message);
			        	}
			        	 
			          }, function(error) {
							$rootScope.hideloading('#loadingBar');
							$rootScope.fnHttpError(error);
						});
			}
			
			
			 $scope.rowHighilited = function(row) {
				 $scope.selectedRow = row;
				// $scope.authen = $scope.tagData[row];
				 $scope.authen = JSON.parse(JSON.stringify($scope.tagViewData[row]));
				 $scope.statusEdit = true; //kumar 21-Jan-21 Enable Edit button only when it is true
				 if ($scope.authen.status == "Free"){
						
						$scope.names = ["Free","Lost", "Not Working",
								"Expired" ];
					}
					else if ($scope.authen.status == "Expired") {
						// $scope.showDetails = true;
						$scope.statusEdit = false;
						$scope.names = [ "Expired" ];
						// $scope.names = [ "Expired", "Lost", "Not
						// Working", "Blocked" ];
					} else if ($scope.authen.status == "Overdue") {

						$scope.showDetails = true;

						$scope.names = [  "Overdue","Lost",
								"Not Working", "Blocked" ];
					} else if ($scope.authen.status == "Blocked") {
						 
						$scope.showDetails = true;
						/*$scope.names=[];
						$scope.names[0] = $scope.authen.status;*/
						$scope.names = ["Blocked","Lost",
								"Not Working","Assigned" ];
						
					} else if ($scope.authen.status == "Lost" && $scope.authen.restart == true ) {
						//$scope.statusEdit = true;
						$scope.showDetails = true;
						$scope.names = ["Lost", "Free" ];
					}else if ($scope.authen.status == "Lost") {
						$scope.statusEdit = false;
						$scope.names = [ "Lost" ];
					} else if ($scope.authen.status == "Not Working") {
						$scope.statusEdit = false;
						$scope.names = [ "Not Working" ];
					} else if ($scope.authen.status == "Assigned") {
						
						if ($scope.authen.type == 'Visitor' ||
								$scope.authen.type == 'Contractor' ||
								$scope.authen.type == 'Vendor'){
						if ($scope.authen.assignmentDate != undefined
							&& $scope.authen.assignmentDate != '' && $scope.authen.assignmentDate != null) {
							var tagAssignmentDate =  moment($scope.authen.assignmentDate);
							var currentDay = moment(new Date()).format('MMM DD, YYYY');
							var diff = moment(currentDay) - tagAssignmentDate;
							if (diff == 0){ //today 
								 var today = new Date();
								 var currentTime = ("0" + today.getHours()).slice(-2)
									+ ":"
									+ ("0" + today.getMinutes()).slice(-2);
								 var toTime =$scope.authen.toTime.substring(0,$scope.authen.toTime.length-3);
								 var fromTime = $scope.authen.fromTime;
								  a = toTime.split(':');
								  b = currentTime.split(':');
								 var toTimeMinutes = (+a[0]) * 60 + (+a[1]);
								 var currentTimeMinutes = (+b[0]) * 60 + (+b[1]);
								 							 
								 
								 
								 if (currentTimeMinutes > toTimeMinutes)
																											
								 $scope.names = [ "Assigned", "Lost", "Not Working","Blocked", "Expired","Overdue" ];
								 else
								 $scope.names = [ "Assigned", "Lost", "Not Working","Blocked", "Expired" ];
							}else if (diff > 0){ //assigned date is previous date
								$scope.names = [ "Assigned", "Lost", "Not Working",
									"Blocked", "Expired","Overdue"  ];
							}
							else{
								$scope.names = [ "Assigned", "Lost", "Not Working",
									"Blocked", "Expired"  ];
							}
						}
						}else {
							$scope.names = [ "Assigned", "Lost", "Not Working",	"Expired"  ];
						}
					}
				
			 }
			 
			 $scope.updateTag = function(){
				 
					if ($scope.names[0] == $scope.authen.status) {
						$rootScope.showAlertDialog("Please select other status");
						return;
					}
				 var updateTagParameter={
						"tagId": $scope.authen.tagId,
						"status": $scope.authen.status,
						"type" : 'tagWorkorder'
					};
				 if (!$rootScope.checkNetconnection()) { 
						$rootScope.alertDialogChecknet();
						return; 
						}
					$rootScope.showloading('#loadingBar');
				 $scope.promise= $http.post('./updateTagStatus',updateTagParameter)
		          .then(function(result) {
		        	  $rootScope.hideloading('#loadingBar');
		        	  if ($scope.authen.status == "Lost"){ //kumar 21-Jan-21
							$scope.authen.restart = false;
						}
						else if ($scope.authen.status == "Free"){ //kumar 21-Jan-21
							$scope.authen.restart = true;
							$scope.authen.type = "";
							$scope.authen.assignmentId ="";
							$scope.authen.assignmentDate="";
							$scope.authen.modifiedDate=moment(new Date()).format('MMM DD, YYYY');
						}
		        	  $scope.tagViewData[$scope.selectedRow] = $scope.authen;
			        	if (result.data.status == "success"){
							$rootScope.showAlertDialog(result.data.message);    
			        		$scope.cancelTag(); 	
			        	}else if(result.data.status == "error"){
							$rootScope.showAlertDialog(result.data.message);   
			        	}
			        	 
			          }, function(error) {
							$rootScope.hideloading('#loadingBar');
							$rootScope.fnHttpError(error);
						});
			 }
			 $scope.cancelTag = function (){
					
					$scope.authen ={};
					$scope.selectedRow = -1;
					
					$scope.tagWorkOrderEditScreen = true;
					$scope.tagWorkOrderUpdateScreen = false;
					$scope.searchButton = true;
				}
			 $scope.cancelEdit = function(){
	        		
	        		$scope.selectedRow = -1;
	        		$scope.searchButton = true;
					 }
			 
			
			
			 $scope.cancelReturnTag = function(){
				 $scope.tagWorkOrderReturnScreen = false;
				 $scope.fetchScreen = true ;
				 $("#checktagIdSelect").empty();
				 $('#checktagIdSelect').select2('destroy');
				 $('#checktagIdSelect').select2({
						data: $scope.fetchTagIdTable,
					      templateResult: function (d) {
					    	  //$scope.fieldName.checktagId=$(d.text);
					    	  return $(d.text); 
					    	  },
					      templateSelection: function (d) { 
					    	  return $(d.text); 
					    	  },
					});
					$('#checktagIdSelect').trigger('change.select2'); 

				 }
			 $scope.returnTag = function(){
				 var returnTagParameter ={
						 
						     "macId":$scope.checkTagMacId,
						     "type": "workmetrial",
						     "peopleId":$scope.tagReturnworkOrderId
				 };
				 if (!$rootScope.checkNetconnection()) { 
						$rootScope.alertDialogChecknet();
						return; 
						}
					$rootScope.showloading('#loadingBar');
				 $scope.promise= $http.post('./returnTag',returnTagParameter)
				 .then(function(result) {
					 $rootScope.hideloading('#loadingBar');
					 if (result.data.status == "success"){						 
						 
						 $rootScope.alertDialog(result.data.message);
						 for (var i = 0; i < $scope.fetchTagIdTable.length; i++){
							  // look for the entry with a matching `code` value
							  if ($scope.fetchTagIdTable[i].id == $scope.checkTagTagId){
								  $scope.fetchTagIdTable[i].text =  $scope.varTagIdTextFree + $scope.checkTagTagId + '<div>';
								  break;
							  }
							}
						 $scope.cancelReturnTag();
					 }
					 else if(result.data.status == "error"){
						 $rootScope.alertDialog(result.data.message);
					 }
				    	 
				      }, function(error) {
							$rootScope.hideloading('#loadingBar');
							$rootScope.fnHttpError(error);
						}); 
					 
					 
				 }
			 

 $scope.fetchTag = function(){
	
	/* var fetchTag = document.getElementById("checktagIdSelect");
	 if (fetchTag.value.length == 0) {
		 $rootScope.hideloading('#loadingBar');
			$scope.fetchTagError = true;
			$scope.fetchTag_error_msg = "Please enter Tag ID ";
			$timeout(function() {
				$scope.fetchTagError = false;

			}, 2000);

			return;
		}*/
	 var varCheckTagID =  $('#checktagIdSelect').val();
	 
	 if (varCheckTagID==undefined || varCheckTagID==null || varCheckTagID=='' ) {
			$rootScope.alertErrorDialog('Please Enter Tag ID ');
			return;
		}
	 
		if (varCheckTagID == "NoTag"){
			$rootScope.alertErrorDialog('Tags are not available');
			return;
		}
									
		var checkTagParameter = {
			"tagId" : varCheckTagID
		};
		if (!$rootScope.checkNetconnection()) { 
			$rootScope.alertDialogChecknet();
			return; 
			}
		$rootScope.showloading('#loadingBar');
	 $scope.promise= $http.post('./checkTag', checkTagParameter)
     .then(function(result) {
    	 
    	 $rootScope.hideloading('#loadingBar');
        	if (result.data.status == "success"){
        		
        		if(result.data.tagStatus =="free"){
        			$scope.checkTagMacId =result.data.tag.macId;
        			var tagId = result.data.tag.tagId;
        			$scope.taggID = tagId;
        			$("#id_tagIdAssignTag").val(tagId);
        			$("#id_workOrderIdAssignTag").val("");
        			$scope.authen.tagSubType = $scope.workOrderTagTypes[0];
        			if($scope.workorderArray != undefined && $scope.workorderArray != '' && $scope.workorderArray != null)
        			$scope.authen.workOrderId = $scope.workorderArray[0];
        			$scope.assignScreen = true ;
        			$scope.fetchScreen = false;
        			$scope.tagWorkOrderReturnScreen = false;
        			
        			
        			
        			
        		}else if(result.data.tagStatus =="Assigned" || result.data.tagStatus =="assigned"){
    				
    				
    				if(result.data.tag.type == "Materials"){
    					$scope.checkTagMacId =result.data.tag.macId;
        				$scope.checkTagTagId =result.data.tag.tagId;
    					$scope.checkTagType =result.data.tag.subType;
    				
    				$scope.checkTagWorkOrderId =result.data.tag.assignmentId;
    				
    				$scope.tagWorkOrderReturnScreen = true;
    				$scope.fetchScreen = false;
    				 $scope.tagReturnTagID = $scope.checkTagTagId;
    				 $scope.tagReturnTagType = $scope.checkTagType;
    				 $scope.tagReturnworkOrderId = $scope.checkTagWorkOrderId;
    				 
    				}
    			
        		else
        			{
        			$rootScope.alertDialog(result.data.message);
        			}
        		
        		
        	}else {
        		$rootScope.alertDialog(result.data.message);
        	}
        	}else if (result.data.status == "error") {
        		$rootScope.alertDialog(result.data.message);
        	}
          }, function(error) {
				$rootScope.hideloading('#loadingBar');
				$rootScope.fnHttpError(error);
			}); 
	 		 
			 }
 

	//pagination button
	
	$scope.currentPage = 1;
	$scope.numPerPage = 10;
	$scope.maxSize = 3;
	$scope.totalItems = 0;
	$scope.tagDataFilterList = [];
	$scope.tagViewData = [];
	$scope
			.$watch(
					'currentPage + numPerPage',
					function() {
						var begin = (($scope.currentPage - 1) * $scope.numPerPage), end = begin
								+ $scope.numPerPage;
						if ($scope.searchText == '') // kumar
							// 02/July
							// for
							// filter
							// text
							$scope.tagViewData = $scope.tagData
									.slice(begin, end);
						else
							$scope.tagViewData = $scope.tagDataFilterList
									.slice(begin, end);
					});

	$scope.currentPage = 1;

	$scope.maxSize = 3;

	$scope.numPerPage = 10;
	$scope.totalItems = 0;
	$scope.totalPages = 0;

	$scope.hasNext = true;
	$scope.hasPrevious = true;
	$scope.isFirstPage = true;
	$scope.isLastPage = true;
	$scope.serial = 1;

	$scope
			.$watch(
					"currentPage",
					function(newVal, oldVal) {
						if (newVal !== oldVal) {

							if (newVal > $scope.totalPages) {
								$scope.currentPage = $scope.totalPages;
							} else {
								$scope.currentPage = newVal;
							}

							$scope.serial = (($scope.currentPage - 1) * $scope.numPerPage) + 1;

						}
					});

	$scope.enablePageButtons = function() {
		$scope.serial = (($scope.currentPage - 1) * $scope.numPerPage) + 1;
		$scope.selectedRow = -1;
		if ($scope.totalPages <= 1) {
			$scope.hasPrevious = true;
			$scope.isFirstPage = true;
			$scope.hasNext = true;
			$scope.isLastPage = true;

		} else if ($scope.totalPages > 1
				&& $scope.currentPage == $scope.totalPages) {

			$scope.hasPrevious = false;
			$scope.isFirstPage = false;
			$scope.hasNext = true;
			$scope.isLastPage = true;
		} else if ($scope.currentPage != 1
				&& $scope.currentPage < $scope.totalPages) {

			$scope.hasPrevious = false;
			$scope.isFirstPage = false;
			$scope.hasNext = false;
			$scope.isLastPage = false;

		} else {
			$scope.hasPrevious = true;
			$scope.isFirstPage = true;
			$scope.hasNext = false;
			$scope.isLastPage = false;
		}
	};

	$scope.getTotalPages = function() {

		if (($scope.totalItems % $scope.numPerPage) > 0)
			$scope.totalPages = parseInt($scope.totalItems
					/ $scope.numPerPage) + 1;
		else
			$scope.totalPages = parseInt($scope.totalItems
					/ $scope.numPerPage);

		$scope.enablePageButtons();

	};

	$scope.goFirstPage = function() {
		$scope.currentPage = 1;
		$scope.enablePageButtons();
	};

	$scope.goPreviousPage = function() {
		if ($scope.currentPage > 1)
			$scope.currentPage = $scope.currentPage - 1;
		$scope.enablePageButtons();
	};

	$scope.goNextPage = function() {
		if ($scope.currentPage <= $scope.totalPages)
			$scope.currentPage = $scope.currentPage + 1;
		$scope.enablePageButtons();
	};

	$scope.goLastPage = function() {
		$scope.currentPage = $scope.totalPages;
		$scope.enablePageButtons();
	};
 
 
	$scope.displayTagWorkOrderAssignScreen = function() {
		$("#checktagIdSelect").val(""); 
		//$scope.tagPeopleViewScreen = false;
		$scope.tagWorkOrderEditScreen = false;
		$scope.tagWorkOrderUpdateScreen = false;
		
		$scope.fetchScreen = true;
		$scope.assignScreen = false;
		$scope.tagWorkOrderReturnScreen = false;
		//$("#idTagPeopleView").removeClass("active");
		$("#idTagWorkOrderAssign").addClass("active");
		$("#idTagWorkOrderEdit").removeClass("active");
		
		//$("#idTagPeopleReturn").removeClass("active");
		$scope.searchButton = false;
		//$scope.getMaterialViewAllData();
		$scope.getTagData();
		$scope.fieldName.checktagId = $scope.checktagIdTable[0];
	};
	$scope.displayTagWorkOrderEditScreen = function() {
		$scope.tagDataFilterList = [];
		$scope.tagViewData = [];	
		$scope.searchText = "";	
		
		$scope.selectedRow = -1; 
		$scope.currentPage = 1;
		$scope.searchButton = true;
		//$scope.tagPeopleViewScreen = false;
		$scope.tagWorkOrderEditScreen = true;
		$scope.tagWorkOrderUpdateScreen = false;
		
		$scope.fetchScreen = false;
		$scope.assignScreen = false;
		$scope.tagWorkOrderReturnScreen = false;
		//$("#idTagPeopleView").removeClass("active");
		$("#idTagWorkOrderAssign").removeClass("active");
		$("#idTagWorkOrderEdit").addClass("active");
		
		//$("#idTagPeopleReturn").removeClass("active");
		$scope.getTagData();
	};
	
	
	$scope.tagAssignBackToView = function (){
		var formID=document.getElementById("assignTagForm").reset();			
		
				$scope.authen ={};
				$scope.selectedRow = -1;
				//$scope.tagPeopleViewScreen = true;
				$scope.searchButton = true;
				$scope.fetchScreen = false;
				$scope.assignScreen = false;
				$scope.tagWorkOrderReturnScreen = false;
		
	}
	$scope.cancelTagAssign = function (){
		$scope.fetchScreen = true;
		$scope.assignScreen = false;
		$scope.tagWorkOrderReturnScreen = false;
		$("#checktagIdSelect").empty();
		$('#checktagIdSelect').select2('destroy');
		$('#checktagIdSelect').select2({
			data: $scope.fetchTagIdTable,
		      templateResult: function (d) {
		    	  //$scope.fieldName.checktagId=$(d.text);
		    	  return $(d.text); 
		    	  },
		      templateSelection: function (d) { 
		    	  return $(d.text); 
		    	  },
		});
		$('#checktagIdSelect').trigger('change.select2'); 

		
		
	}
	//to display enabled sub menus
	if( $scope.user.capabilityDetails != undefined && $scope.user.capabilityDetails != null){
	   /* if ($scope.user.capabilityDetails.WTVA !=0){	    	
	    	$scope.displayTagPeopleViewScreen();			
		}*/	
		 if ($scope.user.capabilityDetails.WTAR !=0){	    	
		    	$scope.displayTagWorkOrderAssignScreen();			
			}
	    if ($scope.user.capabilityDetails.WTAR !=1){	    	
	    	$scope.displayTagWorkOrderEditScreen();
		}		
		/*if ($scope.user.capabilityDetails.WTVA !=1&&
				$scope.user.capabilityDetails.WTAR !=1){			
			$scope.displayTagWorkOrderEditScreen();
		}					
		if ($scope.user.capabilityDetails.WTVA !=1&&
				$scope.user.capabilityDetails.WTAR !=1&&
				$scope.user.capabilityDetails.WTED !=1){			
			 $scope.displayTagPeopleRenameScreen();
		}*/
		}
	
		});
