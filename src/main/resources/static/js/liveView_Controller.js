app
		.controller(
				'liveView_Controller',
				function($scope, $http, $rootScope, $window, $timeout,
						$location, $filter, $interval, $routeParams) {
					$scope.user = JSON.parse($window.sessionStorage
							.getItem("user"));
					if ($scope.user == null) {
						$location.path('/login');
						return;
					}
					$('#map').css('height', ($(window).height() - 120));
					$('#tagListView').css('height', ($(window).height() - 100));
					
					var maxZoomValue = 18;
					var minZoomValue = 8;
					var defZoomValue = 8;

					$scope.success = false;
					$scope.loading = false;

					$scope.showBuildings = true;
					$scope.showWalls = true;
					$scope.showPartitions = true;
					$scope.showDoors = true;
					$scope.showPathways = true;
					$scope.showZone = true;
					$scope.isTrack = false;
					$scope.showDoNotUse = false;
					
					

					$scope.drawLine = false;
					$scope.drawaRectangle = false;
					$scope.drawPolygon = false;

					// $scope.showSubType = true;
					$scope.showTagType = false;
					$scope.tagData = true;
					$scope.serviceProviderStatus = true;
					$scope.outlineList = [];
					$scope.doorsList = [];
					$scope.wallList = [];
					$scope.partitionList = [];
					$scope.pathwayList = [];
					$scope.zoneList = [];
					$scope.posDetails = [];
					$scope.tagConfigColor = {};
					$scope.liveTagDetails = {};

					$scope.assignedTagMacId = $rootScope.trackingMacId; 
					$scope.trackOutboundId = $rootScope.trackOutboundId;
					
					if($scope.assignedTagMacId!=undefined &&  $scope.assignedTagMacId!='')
					{ 
						$scope.assignedTagMacIdArray = $scope.assignedTagMacId.split(',');
					}else
						$scope.assignedTagMacIdArray = [];
					
					$scope.liveMenu = $window.sessionStorage
							.getItem("menuName");
					
					/*
					 * var slider = new Slider("#sliderView", { ticks: [0, 100,
					 * 200, 300, 400], ticks_positions: [0, 30, 70, 90, 100],
					 * ticks_labels: ['$0', '$100', '$200', '$300', '$400'],
					 * ticks_snap_bounds: 30 });
					 */
					
					$scope.viewType ='Live View';
					$scope.historySelectedDate = moment(new Date()).format('MMM DD, YYYY');
					  
					$scope.zoneNameList = {};
					$scope.tagNameList = {};
					
					 $('#idDatePicker').datetimepicker({
							format : 'MMM DD, YYYY', 
							defaultDate: new Date(),
							maxDate : new Date()
						});
					 
					 
					 $scope.onHistoryDateChange = function(){
					      // console.log($scope.historySelectedDate);
						  $scope.setSliderData();
						  $scope.tqidval= 0; 
						  $scope.viewTime = '00:00';
						  $scope.getHistoricalData();
						  
							 
					 };
					 
					 $scope.showHistoricalView = function(){ 
						 
						 if($scope.viewType =='Track Back')
						 {
							 $scope.viewType ='Live View';
							 $("#showHistoricalViewBtn").text('Show Historical View');
							 $scope.applyFilters();
						 }else
						 {
							 $scope.zoneNameList = {};
							 
							 $scope.zoneList.forEach(element => {
									$scope.zoneNameList[element['id']] =element['name'];
								}); 
							 
							 $scope.tagNameList = {};
							 $scope.posDetails.forEach(element => {
									$scope.tagNameList[element['macId']]=element['tagId'];
								}); 
							 
							 $("#showHistoricalViewBtn").text('Show Live View');
							 $scope.viewType ='Track Back';
							 $scope.historySelectedDate = moment(new Date()).format('MMM DD, YYYY');
							 $scope.timeval= $scope.timeIntervals[0];
							 $scope.tqidval= 0; 
							 $scope.viewTime = '00:00';
							 $scope.getHistoricalData();
							 
						 } 
						 
						 $("#configurationView").hide();
							
					 };

					 
					 
					 $scope.timeIntervals =["12:00 AM","12:15 AM", "12:30 AM", "12:45 AM", "01:00 AM", "01:15 AM", "01:30 AM", "01:45 AM",
							"02:00 AM", "02:15 AM", "02:30 AM", "02:45 AM", "03:00 AM", "03:15 AM", "03:30 AM", "03:45 AM", "04:00 AM", "04:15 AM", "04:30 AM",
							"04:45 AM", "05:00 AM", "05:15 AM", "05:30 AM", "05:45 AM", "06:00 AM", "06:15 AM", "06:30 AM ", "06:45 AM", "07:00 AM", "07:15 AM",
							"07:30 AM", "07:45 AM", "08:00 AM", "08:15 AM", "08:30 AM", "08:45 AM", "09:00 AM", "09:15 AM", "09:30 AM", "09:45 AM", "10:00 AM",
							"10:15 AM", "10:30 AM","10:45 AM", "11:00 AM", "11:15 AM", "11:30 AM", "11:45 AM", "12:00 PM", "12:15 PM", "12:30 PM", "12:45 PM", "01:00 PM", "01:15 PM",
							"01:30 PM", "01:45 PM", "02:00 PM", "02:15 PM", "02:30 PM", "02:45 PM", "03:00 PM", "03:15 PM", "03:30 PM", "03:45 PM", "04:00 PM", "04:15 PM",
							"04:30 PM", "04:45 PM", "05:00 PM", "05:15 PM", "05:30 PM", "05:45 PM", "06:00 PM", "06:15 PM", "06:30 PM", "06:45 PM", "07:00 PM",
							"07:15 PM", "07:30 PM", "07:45 PM", "08:00 PM", "08:15 PM", "08:30 PM", "08:45 PM", "09:00 PM", "09:15 PM", "09:30 PM", "09:45 PM", "10:00 PM",
							"10:15 PM", "10:30 PM", "10:45 PM", "11:00 PM", "11:15 PM", "11:30 PM", "11:45 PM"
							];
					  
					 
					 $scope.setSliderData = function(){
						 
						$scope.timeIntervals = [];
						if(!isNaN($scope.historySelectedDate))
							$scope.historySelectedDate = moment($scope.historySelectedDate).format('MMM DD, YYYY');
						
						var maxDate;
						if($scope.historySelectedDate == moment(new Date()).format('MMM DD, YYYY')){
							maxDate = new Date();
						}else
						{
							maxDate = moment($scope.historySelectedDate+' 11:59 PM');
						}
						
						var curDate = moment($scope.historySelectedDate);
						do {
							  var timeValue = moment(curDate).format('hh:mm A');
							  
							  $scope.timeIntervals.push(timeValue);
							  curDate = moment(curDate).add(5, 'minutes');
							  
						}while (curDate<=maxDate);
						 
						// console.log($scope.timeIntervals);
						
						$scope.timeval= $scope.timeIntervals[0];
						 
						 $( "#slider-range-min" ).slider({
								range: "min",
								min: 0,
								max: $scope.timeIntervals.length-1,
								value:0,
								slide: function( event, ui ) {
									
						 			$scope.timeval= $scope.timeIntervals[ui.value];
									$scope.tqidval= ui.value; 
									   
									$scope.viewTime = moment($scope.historySelectedDate+' '+$scope.timeval).format('HH:mm'); 
	                            	$scope.getHistoricalDataByTime();
									$scope.$apply();
									
								} 
							});
					 }
					 
					$scope.setSliderData();
					 
					$scope.showTagListView = true;

					$scope.liveViewPageName = "";
					
					$scope.otherStatusTypes = ["Lost", "Not Working", "Expired", "Blocked"];

					$scope.subTypesAllArray = ["All", "Free", "Employee", "Visitor","Vendor" , "Contractor", "Gages",
						"Tools", "Fixtures", "Supplies", "Vehicles", "Parts", "Material", "RMA", "LSLM", "Inventory", "WareHouse Materials" ];
					$scope.subTypesPeopleArray = ["All", "Employee", "Visitor","Vendor" , "Contractor"];
					$scope.subTypesAssetsArray = ["All", "Gages", "Tools","Fixtures","Supplies", "Vehicles" ];
					$scope.subTypesWipArray = ["All", "Parts", "Material","RMA" ];
					$scope.skuSubTypeArray = ["All", "LSLM", "Inventory" ];
					$scope.wareHouseSubTypeArray = ["All", "WareHouse"];
					$scope.typesArray1 = ["All", "People", "Assets", "WIP", "SKU", "WareHouse"];
					$scope.typesArray = [];
					$scope.typesArray1.forEach(element => {
						$scope.typesArray.push({
							"type" :element
						});
					}); 
					$scope.type = 'All';
					
					var orgName = "";
					// $scope.tagType = "All";
					$scope.subtype = "All";

					$scope.pathwayPatterns = [ {
						name : 'Line-Cross',
						img : 'dist/img/path/image.gif'
					}, {
						name : 'Diamond',
						img : 'dist/img/path/diamond.gif'
					}, {
						name : 'Square',
						img : 'dist/img/path/square.gif'
					}, {
						name : 'Polygon',
						img : 'dist/img/path/polygon.gif'
					} ];

					$scope.searchTag = "";

					$scope.subTypesArray = []; 
					$scope.subTypesArray1 = [];
					
					$scope.updateSubTypeArray = function(){
						$scope.subTypesArray = []; 
						$scope.subTypesArray1 = []; 
						$scope.isSubTypeEnabled = true;
						if ($scope.type == "All") {  
							$scope.subTypesAllArray.forEach(element => {
								
								$scope.subTypesArray1.push(element);
								$scope.subTypesArray.push({
									"type" :element
								});
							}); 
						} else if ($scope.type == "People") {  
							$scope.subTypesPeopleArray.forEach(element => {
								$scope.subTypesArray1.push(element);
								$scope.subTypesArray.push({
									"type" :element
								});
							}); 
							
						} else if ($scope.type == "Assets") {  
							$scope.subTypesAssetsArray.forEach(element => {
								$scope.subTypesArray1.push(element);
								$scope.subTypesArray.push({
									"type" :element
								});
							}); 
							
						} else if ($scope.type == "WIP") {  
							$scope.subTypesWipArray.forEach(element => {
								$scope.subTypesArray1.push(element);
								$scope.subTypesArray.push({
									"type" :element
								});
							}); 
							
						}else if ($scope.type == "SKU") {  
							$scope.skuSubTypeArray.forEach(element => {
								$scope.subTypesArray1.push(element);
								$scope.subTypesArray.push({
									"type" :element
								});
							}); 
							
						}else if ($scope.type == "WareHouse") {  

							$scope.isSubTypeEnabled = false;
							$scope.wareHouseSubTypeArray.forEach(element => {
								$scope.subTypesArray1.push(element);
								$scope.subTypesArray.push({
									"type" :element
								});
							}); 
							
						}
					}
					 
					$scope.type ='All';
					$scope.isTypeEnabled = false;
					$scope.isSubTypeEnabled = true;
					
					
				 
					if($scope.liveMenu == "live" ){
						$scope.type ='All';
						$scope.isTypeEnabled = true;
					}else if($scope.liveMenu == "peopleLiveView" ){
						$scope.type ='People';
					}else if($scope.liveMenu == "assetLiveView" ){
						$scope.type ='Assets';
					}else if($scope.liveMenu == "workorderLiveView" ){
						$scope.type ='WIP';
					}else if($scope.liveMenu == "skuLiveView" ){
						$scope.type ='SKU';
					}else if($scope.liveMenu == "warehouseLiveView" ){
						$scope.type ='WareHouse';
						$scope.subType ='All';
						$scope.isSubTypeEnabled = false;
					}
					  
					
					$scope.updateSubTypeArray();

					$scope.showSubType = true;
					$scope.filterZone = "All";
					var map = L.map('map', {
						editable : true,
						maxZoom : maxZoomValue,
						minZoom : minZoomValue,
						zoom : defZoomValue,
						crs : L.CRS.Simple,
						doubleClickZoom : false,
						zoomSnap : 0.01,
						zoomDelta : 0.25,
						scrollWheelZoom : false, // disable original zoom
						// function
						smoothWheelZoom : true, // enable smooth zoom
						smoothSensitivity : 1
					// zoom speed. default is 1
					}).setView([ 0, 0 ], 14);

					var ZoomViewer = L.Control
							.extend({
								 options: {
									  position: 'topleft' 
								 },
								onAdd : function() {

									var container = L.DomUtil.create('div');
									var gauge = L.DomUtil.create('div');
									container.style.width = '100px';
									container.style.background = 'rgba(255,255,255,0.5)';
									container.style.textAlign = 'center';

									map.on('zoomstart zoom zoomend', function(
											ev) {
										gauge.innerHTML = 'Zoom: '
												+ ((map.getZoom()-minZoomValue)+1).toFixed(2);

									})
									container.appendChild(gauge);

									return container;
								}
							});

					(new ZoomViewer).addTo(map);
					
					map.zoomControl.setPosition('topleft');
					
					function getTagData(macId){
						

						for (var i = 0; i < $scope.posDetails.length; i++) {
							if($scope.posDetails[i].macId==macId){
								return $scope.posDetails[i];
							}
						}
						
						return null;
					}

					function fnStopTracking() {

						$scope.isTrack = false;
						$scope.isOnTrackedClicked = false;
						
						if($scope.liveMenu == "live" ){ 
							$scope.isTypeEnabled = true;
						}else
							$scope.isTypeEnabled = false;
						
					     $rootScope.setPageName($scope.liveViewPageName);

						disconnect();
 
						$scope.trackTagsBtn.addTo(map); 
						$scope.stopBtnIcon.remove();
 
						$scope.closebtn();

						$scope.assetList = [];

						var i = 0;
						for ( var macId in $scope.assetTagList) {

							var assetData = $scope.assetTagList[macId];
							var assetLive = $scope.liveTagDetails[macId];

							if(assetLive==undefined)
							{
								assetLive = getTagData(macId);
							} 
							 
							assetData.x = assetLive.x;
							assetData.y = assetLive.y;

							assetData.name = assetLive.name;
							assetData.macId = assetLive.macId;
							assetData.type = assetLive.type;
							assetData.id = i;
							assetData.zoneName = assetLive.zoneName;
							assetData.last_seen = assetLive.lastSeen;

							if ($scope.assetLiveColorArray
									.hasOwnProperty(assetData.macId)) {
								assetData.colorCode = $scope.assetLiveColorArray[assetData.macId];
								assetData.selected = true;
							} else {
								if (assetData.colorCode == undefined
										|| assetData.colorCode == null
										|| assetData.colorCode == '')
									assetData.colorCode = '#089b3c';
								assetData.selected = false;
							}

							$scope.assetList.push(assetData);
							i++;
						}

						$scope.setAssetsIcons();
						$timeout(
								function() {
									for (var i = 0; i < $scope.assetList.length; i++) {
										$("#liveColorId" + i)
												.val(
														$scope.assetList[i].colorCode == undefined
																|| $scope.assetList[i].colorCode == '' ? '#089b3c'
																: $scope.assetList[i].colorCode);
									}
								}, 100);

					}
					$scope.$on('$destroy', function() {
						disconnect();
					});
					
					function fnTrackTags() {
						if ($rootScope.checkNetconnection() == false) {
							$rootScope.hideloading('#loadingBar');
							$rootScope.alertDialog('Please check the Internet Connection');  
							return;
						}
						
						
						$scope.isTrack = true;
						$scope.isTypeEnabled = false;

						$scope.onTrack();
					}

					$('#mapOptions')
							.multiselect(
									{
										// includeSelectAllOption: true,
										// enableFiltering: true,
										// enableCaseInsensitiveFiltering: true,
										nonSelectedText : 'Select map options',
										numberDisplayed : 1,
										buttonContainer : '<div class="btn-group" id="mapOptions-container"></div>',
										selectedClass : 'active multiselect-selected',
										onChange : function(option, checked,
												select) {
											var valueStr = $(option).val();
											if (valueStr == "Zone") {
												fnZoneSelect(checked);
											} else if (valueStr == "Grid") {
												fnGridSelect(checked);
											} else {
												fnOptionSelect(checked,
														valueStr);
											}

										}
									});

					function fnZoneSelect(zoneSelect) {

						$scope.showZone = zoneSelect;

						if (zoneSelect) {
							fnShowZones();
						} else {
							map.eachLayer(function(layer) {
								if (layer.menuType !== undefined
										&& layer.menuType == "Zone") {
									layer.remove();
								}
							});
						}

					}
					function fnOptionSelect(optionSelect, layerType) {
						if (layerType == "Door") {
							$scope.showDoors = optionSelect;
						} else if (layerType == "Wall") {
							$scope.showWalls = optionSelect;
						} else if (layerType == "Partition") {
							$scope.showPartitions = optionSelect;
						} else if (layerType == "Building") {
							$scope.showBuildings = optionSelect;
						} else if (layerType == "Pathway") {
							$scope.showPathways = optionSelect;
						}
						if (optionSelect) {
							getAllOutlinedetails();
						} else {
							map.eachLayer(function(layer) {
								if (layer.menuType !== undefined
										&& layer.menuType == layerType) {
									layer.remove();
								}
							});
						}
					}
					function fnGridSelect(optionSelect) {
						$scope.isCheckedGrid = optionSelect;
						if (optionSelect == true) {
							$scope.grid.onAdd(map);
						} else {
							$scope.grid.onRemove(map);
						}
					}

					$scope.metersPerPixelX = 0;
					$scope.metersPerPixelY = 0;
					$scope.originX = 0;
					$scope.originY = 0;

					function loadMap() {

						var height = $scope.mapHeight;
						var width = $scope.mapWidth;
						$scope.editableLayers = new L.FeatureGroup();
						map.addLayer($scope.editableLayers);

						map.setZoom(defZoomValue);

						var southWest = map.unproject([ 0, height ],
								defZoomValue);
						var northEast = map.unproject([ width, 0 ],
								defZoomValue);
						map.setMaxBounds(new L.LatLngBounds(southWest,
								northEast));

						map.fitBounds(new L.LatLngBounds(southWest, northEast));

						if ($scope.campusRect != undefined
								|| $scope.campusRect != null) {
							$scope.campusRect.onRemove(map);
						}

						var bounds1 = [
								[ pixelsToLatLng(parseFloat(0), parseFloat(0)) ],
								[ pixelsToLatLng(parseFloat(0),
										parseFloat(height)) ],
								[ pixelsToLatLng(parseFloat(width),
										parseFloat(height)) ],
								[ pixelsToLatLng(parseFloat(width),
										parseFloat(0)) ] ];

						$scope.campusRect = L.rectangle(bounds1, {
							color : $scope.mapColor,
							weight : 2,
							fillColor : $scope.mapColor,
							fillOpacity : 0.8
						});
						$scope.campusRect.addTo(map);
						$scope.campusRect.addTo($scope.editableLayers);

						if ($scope.marker != undefined || $scope.marker != null) {
							$scope.marker.onRemove(map);
						}

						var originDotIcon = L.icon({
							iconUrl : './assets/img/red_circle_tr.png',
							iconSize : [ 12, 12 ]
						// size of the icon

						});

						$scope.marker = new L.Marker(pixelsToLatLng(0, 0), {
							icon : originDotIcon
						});

						$scope.marker.addTo(map);
						var originValueIcon = L.icon({
							iconUrl : './assets/img/origin_tr.png',
							iconSize : [ 29, 16 ]
						// size of the icon

						});
						$scope.marker2 = new L.Marker(pixelsToLatLng(1, -1), {
							icon : originValueIcon
						});
						$scope.marker2.addTo(map);
						map.on('move', function(e) {

						});
						// Dragend event of map for update marker position
						map.on('dragend', function(e) {
							var cnt = map.getCenter();
							var position = $scope.marker.getLatLng();
							lat = Number(position['lat']).toFixed(5);
							lng = Number(position['lng']).toFixed(5);

						});

						$scope.mapDetails = {};
						$scope.mapDetails.width_m = width;
						$scope.mapDetails.height_m = height;
						$scope.mapDetails.height = height;
						$scope.mapDetails.width = width;
						$scope.mapDetails.mapType = 'custom';
						$scope.mapHeightMeter = Number(height).toFixed(2); 
						$scope.mapWidthMeter = Number(width).toFixed(2);
						
						if ($scope.grid != undefined || $scope.grid != null) {
							$scope.grid.onRemove(map);
						}

						$scope.grid = L.grid({
							options : {
								position : 'topright',
								bounds : new L.LatLngBounds(southWest,
										northEast),
								mapDetails : $scope.mapDetails,
								defZoomValue : defZoomValue,
								gridSize : 1
							}
						});

						$timeout(function() {
							map.setView([ $scope.campusRect.getCenter().lat,
									$scope.campusRect.getCenter().lng ], map
									.getZoom());

							getAllOutlinedetails();
							$scope.fitZoomLevel = map.getZoom();
						}, 1500);
					}

					// loadMap();

					function isNumberKey(evt) {
						var charCode = (evt.which) ? evt.which : evt.keyCode;
						if (charCode != 46 && charCode > 31
								&& (charCode < 48 || charCode > 57))
							return false;

						return true;
					}

					function getCampusDetails() {

						$http({
							method : 'GET',
							url : './campusDetails'
						})
								.then(
										function success(response) {
											$rootScope
													.hideloading('#loadingBar');
											if (response != null
													&& response.data != null
													&& response.data != "BAD_REQUEST") {
												$scope.campusDetails = response.data.data;

												$scope.mapHeight = $scope.campusDetails.height;
												$scope.mapWidth = $scope.campusDetails.width;
												$scope.map_height = $scope.campusDetails.height;
												$scope.map_width = $scope.campusDetails.width;
												$scope.mapColor = $scope.campusDetails.colorCode;
												$scope.campusId = $scope.campusDetails.campusId;
												$scope.campusName = $scope.campusDetails.name;
												$scope.metersPerPixelX = $scope.campusDetails.metersPerPixelX;
												$scope.metersPerPixelY = $scope.campusDetails.metersPerPixelY;
												$scope.originX = $scope.campusDetails.originX;
												$scope.originY = $scope.campusDetails.originY;

												$timeout(function() {
													loadMap();
												}, 500);
											}
										},
										function error(response) {
											$rootScope
													.hideloading('#loadingBar');
											$rootScope.fnHttpError(response);
										});
					}

					getCampusDetails();

					function latLngToPixels(latlng) {
						var pixelArr = [];
						for (var i = 0; i < latlng[0].length; i++) {

							var latlngArr = latlng[0][i];

							var pt = map.project(
									[ latlngArr.lat, latlngArr.lng ],
									defZoomValue);

							pixelArr.push({
								"x" : (parseFloat(pt.x)).toFixed(2),
								"y" : (parseFloat(pt.y)).toFixed(2),
							});

						}
						return pixelArr;
					}

					function latLngToPixels1(latlng) {
						var pixelArr = [];
						for (var i = 0; i < latlng.length; i++) {

							var latlngArr = latlng[i];

							var pt = map.project(
									[ latlngArr.lat, latlngArr.lng ],
									defZoomValue);

							pixelArr.push({
								"x" : (parseFloat(pt.x)).toFixed(2),
								"y" : (parseFloat(pt.y)).toFixed(2),
							});

						}
						return pixelArr;
					}

					function roundVal(latlng) {
						var pixelArr = [];
						for (var i = 0; i < latlng.length; i++) {

							var pt = latlng[i];

							pixelArr.push({
								"x" : (parseFloat(pt.x)).toFixed(),
								"y" : (parseFloat(pt.y)).toFixed(),
							});

						}
						return pixelArr;
					}

					function editlatLngToPixels(latlng) {
						var pixelArr = [];
						for (var i = 0; i < latlng[0].length; i++) {

							var latlngArr = latlng[0][i];
							var pt = pt = map.project([ latlngArr.lat,
									latlngArr.lng ], defZoomValue);

							pixelArr.push({
								"x" : pt.x.toFixed(),
								"y" : pt.y.toFixed(),
							});

						}
						return pixelArr;
					}

					function pixelsToLatLng(x, y) {
						return map.unproject([ x, y ], defZoomValue);
					}

					$scope.getQuuppaAccessPoints = function() {
						if ($scope.isCheckedShowAp || $scope.isAPView) {
							for (var i = 0; i < $scope.apList.length; i++) {
								$scope.addLocatorAPViews(i);
							}
						}
					}

					$scope.show_alert_error = false;

 
					$scope.closeDetailsView = function(){ 
						$("#employeeDetailsView").hide();
						$("#visitorDetailsView").hide(); 
						$("#assetDetailsView").hide(); 
						$("#vehicleDetailsView").hide();
						$("#skuDetailsView").hide();
						$("#materialDetailsView").hide();  
						$("#wareHouseDetailsView").hide();
					} 
					
					function fnConfiguration(){ 

						$scope.showTagListView = false; 
						$("#configurationView").show();
						$("#tagListView").hide(); 
						$scope.closeDetailsView(); 
					}
					
					function fnTagsSearchView(){ 

						$scope.showTagListView = true; 
						$("#configurationView").hide();
						$("#tagListView").show();
						$scope.closeDetailsView(); 
						 
						    /*
							 * $('#mapDiv').removeClass('col-lg-12');
							 * $('#mapDiv').addClass('col-lg-8');
							 */
					}
					
					
					$scope.getMoreInfo = function(){ 
						$scope.getAssienmentDetails($scope.selectedMacId); 
					}
					
					
					
					$scope.closeRightSideDiv = function(){ 

						$("#configurationView").hide();
						$("#tagListView").hide(); 
						$scope.closeDetailsView();
					} 
					  
					$scope.closeRightSideDiv();

					
					
					
					function fnReload() {
						
						if($scope.viewType =='Track Back')
						{
							return;
						}
						
						   
						if ($scope.isTrack == true) {
							$
									.confirm({
										title : $rootScope.MSGBOX_TITLE,
										content : 'Tracking will be stopped and its details will be reset. Do you want to continue?',
										type : 'blue',
										columnClass : 'medium',
										autoClose : false,
										icon : 'fa fa-info-circle',
										buttons : {
											yes : {
												text : 'Yes',
												btnClass : 'btn-blue',
												action : function() {
													$scope.selectedRow = null;
													$scope.selectedMacId = null;
													$scope.assetLiveColorArray = {};
													fnStopTracking();
												}
											},
											no : function() {

											}
										}
									});
						} else {
							/*
							 * map.eachLayer(function(layer) { if
							 * (layer.menuType !== undefined) { layer.remove(); }
							 * });
							 * 
							 * getAllOutlinedetails();
							 */

							$rootScope.showloading('#loadingBar');
							$scope.selectAll = false;
							$scope.getQuuppaAssetDetail();
							
						}
					}

					$scope.configurationBtn = L.easyButton({
						states : [ {
							icon : '<i class="fa fa-cog" style="font-size:20px; color:green"></i>',
							title : 'Reload',
							onClick : fnConfiguration
						} ]
					});

					$scope.configurationBtn.addTo(map);
			
					$scope.reloadBtn = L.easyButton({
								states : [ {
									icon : '<i class="fas fa-sync-alt" style="font-size:20px; color:green"></i>',
									title : 'Reload',
									onClick : fnReload
								} ]
							});

					$scope.reloadBtn.addTo(map);
					
					$scope.searchBtn = L.easyButton({
						states : [ {
							icon : '<i class="fas fa-search" style="font-size:20px; color:green"></i>',
							title : 'Tags Search View',
							onClick : fnTagsSearchView
						} ]
					});

					$scope.searchBtn.addTo(map);
			
			
					$scope.trackTagsBtn = L
							.easyButton({
								states : [ {

									icon : '<i style="font-size:20px; color:blue" class="fas fa-binoculars"></i>',
									title : 'Track tags',
									onClick : fnTrackTags
								} ]
							});

					$scope.trackTagsBtn.addTo(map);

					$scope.stopBtnIcon = L
							.easyButton({
								states : [ {

									icon : '<i style="font-size:20px;color:red" class="far">&#xf256;</i>',
									title : 'Stop Tracking',
									onClick : fnStopTracking
								} ]
							});

					$scope.addmapDrawOptions = function() {
						var options = {
							position : 'topright',
							draw : {
								polygon : $scope.drawPolygon,
								polyline : $scope.drawLine,
								circlemarker : false,
								circle : false,
								marker : false,
								rectangle : $scope.drawaRectangle
							},
							edit : false

						};
						$scope.drawControl = new L.Control.Draw(options);
						$scope.drawControlEdit = new L.Control.Draw({
							draw : false
						});
						map.addControl($scope.drawControl);
					}
					$scope.addmapDrawOptions();

					$scope.selectedRow = null;
					$scope.selectedMacId = null;
					  
					$scope.popup = L.popup({
						maxWidth : 400
					});

					$scope.area = function(points) {
						var area = 0, i, j, point1, point2;

						var length = points.length;

						for (i = 0, j = length - 1; i < length; j = i, i += 1) {
							point1 = points[i];
							point2 = points[j];
							area += point1.x * point2.y;
							area -= point1.y * point2.x;
						}
						area /= 2;

						return Math.abs(area).toFixed(3);
					};

					// return true if overlap
					function checkGridOverlap(gridPoints) {

						var array = [];

						for (var k = 0; k < gridPoints.length; k++) {
							array.push([ parseInt(gridPoints[k].x),
									parseInt(gridPoints[k].y) ]);
						}
						array.push([ parseInt(gridPoints[0].x),
								parseInt(gridPoints[0].y) ]);

						var poly1 = turf.polygon([ array ]);

						var vertices = $scope.outlineVertices;
						var bounds1 = [];
						for (var j = 0; j < vertices.length; j++) {
							bounds1.push([ parseInt(vertices[j].x),
									parseInt(vertices[j].y) ]);
						}

						bounds1.push([ parseInt(vertices[0].x),
								parseInt(vertices[0].y) ]);

						var poly2 = turf.polygon([ bounds1 ]);
						if (turf.booleanOverlap(poly1, poly2)
								|| turf.booleanContains(poly1, poly2)
								|| turf.booleanContains(poly2, poly1)) {
							return true;
						}

						return false;
					}

					$scope.refreshWalls = function() {
						map.eachLayer(function(layer) {
							if (layer.menuType !== undefined
									&& layer.menuType == "Wall") {
								layer.remove();
							}
						});

						$("#selectedWall").hide(); 

						$scope.drawControl.remove();
						$scope.addmapDrawOptions()
						$scope.getAllWalls();
					}

					$scope.refreshPartition = function() {
						map.eachLayer(function(layer) {
							if (layer.menuType !== undefined
									&& layer.menuType == "Partition") {
								layer.remove();
							}
						});
						$("#selectedPartition").hide();
						$scope.drawControl.remove();
						$scope.addmapDrawOptions()
						$scope.getAllPartitions();
					}

					$scope.refreshPathway = function() {
						map.eachLayer(function(layer) {
							if (layer.menuType !== undefined
									&& layer.menuType == "Pathway") {
								layer.remove();
							}
						});
						$("#selectedPathway").hide();
						$scope.drawControl.remove();
						$scope.addmapDrawOptions()
						$scope.getAllPathways();
					}

					function refreshOutlinedetails() {

						$timeout(function() {
							$scope.success = false;
							$scope.drawOutlineDetails();

							$scope.outlineType = "Tracking Outline";
							$scope.wallType = "Normal";
							getAllOutlinedetails();
						}, 1000);
					}

					function getAllOutlinedetails() {
						map.eachLayer(function(layer) {
							if (layer.id !== undefined) {
								layer.remove();
							}
						});

						$http({
							method : 'GET',
							url : './outlineDetails/' + $scope.campusId
						})
								.then(
										function success(response) {
											$rootScope
													.hideloading('#loadingBar');
											if (response != null
													&& response.data != null
													&& response.data != "BAD_REQUEST") {

												$scope.outlineList.length = 0;
												if (response.data.status == 'success') {
													for (var i = 0; i < response.data.data.length; i++) {
														if (response.data.data[i].active)
															$scope.outlineList
																	.push(response.data.data[i]);
													}

													if ($scope.showBuildings) {
														if ($scope.outlineList.length != 0) {
															for (var i = 0; i < $scope.outlineList.length; i++) {
																if ($scope.outlineList[i].active) {
																	$scope.outlineList[i].area = $scope
																			.area($scope.outlineList[i].coordinateList);
																	var bounds = getBoundsArray($scope.outlineList[i].coordinateList);
																	var layer = null;

																	$scope.colorCode = $scope.outlineList[i].bgColor;
																	var theme = {
																		color : $scope.colorCode,
																		weight : 1,
																		fillColor : $scope.colorCode,
																		fillOpacity : 0.5
																	};
																	if ($scope.outlineList[i].geometryType == undefined
																			|| $scope.outlineList[i].geometryType == null
																			|| $scope.outlineList[i].geometryType == "polygon"
																			|| $scope.outlineList[i].geometryType == "") {

																		$scope.outlineList[i].geometryType = "polygon";
																		layer = L
																				.polygon(
																						bounds,
																						theme);

																	} else {
																		layer = L
																				.rectangle(
																						bounds,
																						theme);
																	}

																	layer
																			.addTo(
																					map)
																			.addTo(
																					$scope.editableLayers)

																	$scope.outlinesArea = $scope
																			.area(editlatLngToPixels(layer
																					.getLatLngs()));

																	layer.id = i;
																	layer.outlinesId = $scope.outlineList[i].outlinesId;
																	layer.name = $scope.outlineList[i].name;
																	layer.outlinesArea = $scope.outlinesArea;
																	layer.colorCode = $scope.colorCode;
																	layer.menuType = "Building";
																	layer.wallType = $scope.outlineList[i].wallType;
																	layer.palceType = $scope.outlineList[i].placeType;
																	layer.geometryType = $scope.outlineList[i].geometryType;
																	layer.coordinateList = bounds;

																}
															}
														} else {
															$scope.noList = true;
															$scope.valueList = false;
															$scope.alert_error_msg = "No outlines details are available";
															$scope
																	.showErrorMessageView();
														}
													}

													if ($scope.showDoors)
														$scope.getAllDoors();
													if ($scope.showWalls)
														$scope.getAllWalls();
													if ($scope.showPartitions)
														$scope
																.getAllPartitions();
													if ($scope.showPathways)
														$scope.getAllPathways();
													if ($scope.showZone)
														$scope.getZoneDetails();

												} else {
													$scope.alert_error_msg = response.data.message;
													$scope
															.showErrorMessageView();
												}

											}
										},
										function error(response) {
											$rootScope
													.hideloading('#loadingBar');
											$rootScope.fnHttpError(response);
										});
					}

					$scope.getAllPathways = function() {

						$scope.isPathwayEdit = false;

						$http({
							method : 'GET',
							url : './pathWay'
						})
								.then(
										function success(response) {
											$rootScope
													.hideloading('#loadingBar');
											if (response != null
													&& response.data != null
													&& response.data != "BAD_REQUEST") {

												$scope.pathwayList.length = 0;
												if (response.data.status == 'success') {
													for (var i = 0; i < response.data.data.length; i++) {
														if (response.data.data[i].active)
															$scope.pathwayList
																	.push(response.data.data[i]);
													}

													if ($scope.pathwayList.length != 0) {
														for (var i = 0; i < $scope.pathwayList.length; i++) {

															var bounds = getBoundsArray($scope.pathwayList[i].coordinates);
															var theme = {
																color : $scope.pathwayList[i].colorCode,
																weight : 3,
																fillColor : '#00FFFFFF',
																fill : 'url(dist/img/path/image.gif)',
																fillOpacity : 10
															};

															var layer = null;
															if ($scope.pathwayList[i].geometryType == "polygon") {
																layer = L
																		.polygon(
																				bounds,
																				theme);
															} else {
																layer = L
																		.rectangle(
																				bounds,
																				theme);
															}

															layer
																	.addTo(map)
																	.addTo(
																			$scope.editableLayers);

															layer.menuType = "Pathway";
															layer.campusId = $scope.pathwayList[i].campusDetails.campusId;
															layer.pathwayId = $scope.pathwayList[i].id;
															layer.pathwayName = $scope.pathwayList[i].name;
															layer.colorCode = $scope.pathwayList[i].colorCode;
															layer.geometryType = $scope.pathwayList[i].geometryType;
															layer.coordinateList = bounds;

														}
													}

												}
											}
										},
										function error(response) {
											$rootScope
													.hideloading('#loadingBar');
											$rootScope.fnHttpError(response);
										});

					}
					$scope.getAllPartitions = function() {

						$scope.isPartitionEdit = false;

						$http({
							method : 'GET',
							url : './partitionsDetails'
						})
								.then(
										function success(response) {
											$rootScope
													.hideloading('#loadingBar');
											if (response != null
													&& response.data != null
													&& response.data != "BAD_REQUEST") {

												$scope.partitionList.length = 0;
												if (response.data.status == 'success') {
													for (var i = 0; i < response.data.data.length; i++) {
														if (response.data.data[i].active)
															$scope.partitionList
																	.push(response.data.data[i]);
													}

													if ($scope.partitionList.length != 0) {
														for (var i = 0; i < $scope.partitionList.length; i++) {

															var bounds = getBoundsArray($scope.partitionList[i].coordinates);
															var theme = {
																color : $scope.partitionList[i].colorCode,
																weight : 3,
																fillColor : $scope.partitionList[i].colorCode,
																fillOpacity : 0
															};
															var layer = null;

															if ($scope.partitionList[i].geometryType == "polyline") {

																layer = L
																		.polyline(
																				bounds,
																				theme);
															} else if ($scope.partitionList[i].geometryType == "polygon") {
																layer = L
																		.polygon(
																				bounds,
																				theme);
															} else {
																layer = L
																		.rectangle(
																				bounds,
																				theme);
															}

															layer
																	.addTo(map)
																	.addTo(
																			$scope.editableLayers);

															layer.outlinesId = $scope.partitionList[i].outlinesDetails.outlinesId;
															layer.menuType = "Partition";
															layer.partitionId = $scope.partitionList[i].id;
															layer.partitionName = $scope.partitionList[i].name;
															layer.colorCode = $scope.partitionList[i].colorCode;
															layer.geometryType = $scope.partitionList[i].geometryType;
															layer.coordinateList = bounds;

														}
													}

												}
											}
										},
										function error(response) {
											$rootScope
													.hideloading('#loadingBar');
											$rootScope.fnHttpError(response);
										});

					}

					$scope.getAllWalls = function() {
						$scope.isWallEdit = false;

						$http({
							method : 'GET',
							url : './wallsDetails'
						})
								.then(
										function success(response) {
											$rootScope
													.hideloading('#loadingBar');
											if (response != null
													&& response.data != null
													&& response.data != "BAD_REQUEST") {

												$scope.wallList.length = 0;
												if (response.data.status == 'success') {
													for (var i = 0; i < response.data.data.length; i++) {
														if (response.data.data[i].active)
															$scope.wallList
																	.push(response.data.data[i]);
													}

													if ($scope.wallList.length != 0) {
														for (var i = 0; i < $scope.wallList.length; i++) {

															var bounds = getBoundsArray($scope.wallList[i].coordinates);
															var theme = {
																color : $scope.wallList[i].colorCode,
																weight : 3,
																fillColor : $scope.wallList[i].colorCode,
																fillOpacity : 0
															};
															var layer = null;

															if ($scope.wallList[i].geometryType == "polyline") {
																layer = L
																		.polyline(
																				bounds,
																				theme);
															} else if ($scope.wallList[i].geometryType == "polygon") {
																layer = L
																		.polygon(
																				bounds,
																				theme);
															} else {
																layer = L
																		.rectangle(
																				bounds,
																				theme);
															}

															layer
																	.addTo(map)
																	.addTo(
																			$scope.editableLayers);
															layer.outlinesId = $scope.wallList[i].outlinesDetails.outlinesId;
															layer.menuType = "Wall";
															layer.wallId = $scope.wallList[i].id;
															layer.name = $scope.wallList[i].name;
															layer.colorCode = $scope.wallList[i].colorCode;
															layer.geometryType = $scope.wallList[i].geometryType;
															layer.coordinateList = bounds;

														}
													}

												}
											}
										},
										function error(response) {
											$rootScope
													.hideloading('#loadingBar');
											$rootScope.fnHttpError(response);
										});

					}

					$scope.getAllDoors = function() {

						$http({
							method : 'GET',
							url : './doorsDetails'
						})
								.then(
										function success(response) {
											$rootScope
													.hideloading('#loadingBar');
											if (response != null
													&& response.data != null
													&& response.data != "BAD_REQUEST") {

												$scope.doorsList.length = 0;
												if (response.data.status == 'success') {
													for (var i = 0; i < response.data.data.length; i++) {
														if (response.data.data[i].active)
															$scope.doorsList
																	.push(response.data.data[i]);
													}

													if ($scope.doorsList.length != 0) {
														for (var i = 0; i < $scope.doorsList.length; i++) {
															var bounds = getBoundsArray($scope.doorsList[i].coordinates);
															var layer = L
																	.polyline(
																			bounds,
																			{
																				color : $scope.doorsList[i].colorCode,
																				weight : 1,
																				fillColor : $scope.doorsList[i].colorCode,
																				fillOpacity : 0.5
																			})
																	.addTo(map)
																	.addTo(
																			$scope.editableLayers);

															layer.outlinesId = $scope.doorsList[i].outlinesDetails.outlinesId;
															layer.menuType = "Door";
															layer.doorId = $scope.doorsList[i].id;
															layer.doorName = $scope.doorsList[i].name;
															layer.doorColorCode = $scope.doorsList[i].colorCode;
															layer.geometryType = "Line";
															layer.coordinateList = bounds;

														}
													}

												}
											}
										},
										function error(response) {
											$rootScope
													.hideloading('#loadingBar');
											$rootScope.fnHttpError(response);
										});
					}

					$scope.drawOutlineDetails = function() {

						map.eachLayer(function(layer) {
							if (layer.id !== undefined) {
								layer.remove();
							}
						});

						if ($scope.outlineList.length != 0) {
							for (var i = 0; i < $scope.outlineList.length; i++) {

								$scope.outlineList[i].area = $scope
										.area($scope.outlineList[i].coordinateList);

								var bounds = getBoundsArray($scope.outlineList[i].coordinateList);
								var theme = {
									color : $scope.outlineList[i].colorCode,
									weight : $scope.outlineList[i].wallWidth,
									fillColor : $scope.outlineList[i].colorCode,
									fillOpacity : 0.5
								};
								var layer = null;

								if ($scope.outlineList[i].geometryType == undefined
										|| $scope.outlineList[i].geometryType == null
										|| $scope.outlineList[i].geometryType == "polygon"
										|| $scope.outlineList[i].geometryType == "") {

									$scope.outlineList[i].geometryType = "polygon";
									layer = L.polygon(bounds, theme);
								} else {
									layer = L.rectangle(bounds, theme);
								}

								layer.addTo(map).addTo($scope.editableLayers);

								$scope.outlinesArea = $scope
										.area(editlatLngToPixels(layer
												.getLatLngs()));
								layer.id = i;
								layer.outlinesId = $scope.outlineList[i].outlinesId;
								layer.name = $scope.outlineList[i].outlineName;
								layer.outlinesArea = $scope.outlinesArea;
								layer.colorCode = $scope.outlineList[i].colorCode;

								layer.wallType = $scope.outlineList[i].wallType;
								layer.placeType = $scope.outlineList[i].placeType;
								layer.geometryType = $scope.outlineList[i].geometryType;
								layer.coordinateList = bounds;

							}
						} else {
							$scope.noList = true;
							$scope.valueList = false;
							$scope.alert_error_msg = "No outlines details are available";
							$scope.showErrorMessageView();
						}

					}

					function getBoundsArray(points) {
						var bounds = [];
						for (var i = 0; i < points.length; i++) {

							bounds.push(pixelsToLatLng(parseFloat(points[i].x),
									parseFloat(points[i].y)));

						}
						return bounds;
					}

					function getPathwayPattern(pathwayPattern) {
						for (var i = 0; i < $scope.pathwayPatterns.length; i++) {
							if ($scope.pathwayPatterns[i].name == pathwayPattern) {
								return $scope.pathwayPatterns[i].img;
							}
						}

						return $scope.pathwayPatterns[0].img;
					}

					function getPosByOutlineId(outlinesId) {

						for (var i = 0; i < $scope.outlineList.length; i++) {
							var id = $scope.outlineList[i].outlinesId;
							if (id != '' && id == outlinesId) {
								return i;
							}
						}

						return -1;
					}

					function GetSortOrder(prop) {
						return function(a, b) {
							if (a[prop].toLowerCase() > b[prop].toLowerCase()) {
								return 1;
							} else if (a[prop].toLowerCase() < b[prop]
									.toLowerCase()) {
								return -1;
							}
							return 0;
						}
					}

					$scope.tableSortType = '';
					$scope.tableSortisAsc = '';
					$scope.sortType = '';

					$scope.tableSortOrder = function(type, isAsc) {
						if (type == 'idTableName')
							$scope.assetList.sort(GetSortOrder("name"));
						else if (type == 'idTableZone')
							$scope.assetList.sort(GetSortOrder("zoneName"));
						else if (type == 'idTableType')
							$scope.assetList.sort(GetSortOrder("type"));

						if (!isAsc) {
							$scope.assetList.reverse();
						}

						$scope.tableSortType = type;
						$scope.tableSortisAsc = isAsc;

						$scope.setAssetsIcons();

						$timeout(
								function() {
									for (var i = 0; i < $scope.assetList.length; i++) {
										$("#liveColorId" + i)
												.val(
														$scope.assetList[i].colorCode == undefined
																|| $scope.assetList[i].colorCode == '' ? '#089b3c'
																: $scope.assetList[i].colorCode);
									}
								}, 200);

					}

					$scope.searchOption = function() {
 
						$scope.closebtn();
						$scope.applyFilters();
						$scope.checkIfAllSelected();
					}
					
					$scope.updateAssetsIcons = function() {
						
						map.eachLayer(function(layer) {
							if (layer.macId !== undefined) {
								
								if ($scope.assetLiveColorArray
										.hasOwnProperty(layer.macId)){
									var asset = $scope.liveTagDetails[layer.macId]; 
									layer.setLatLng(pixelsToLatLng(asset.x, asset.y));
									
									 var myIcon =  new L.DivIcon({
											className : 'assetslabel',
											html : '<span style=color:'
													+ $scope.assetLiveColorArray[asset.macId] + '><i class="'
													+$scope.assetTagList[asset.macId].icon + '"></i></span>'
										});
									 
									 layer.setIcon(myIcon);
								         
								}else{
									layer.remove(); 
								} 
							}

						});
						
						$timeout(
								function() {
									for (var i = 0; i < $scope.assetList.length; i++) {
										$("#liveColorId" + i)
												.val(
														$scope.assetList[i].colorCode == undefined
																|| $scope.assetList[i].colorCode == '' ? '#089b3c'
																: $scope.assetList[i].colorCode);
									}
								}, 50);
					}

					$scope.setAssetsIcons = function() {

						map.eachLayer(function(layer) {
							if (layer.assetid !== undefined) {
								layer.remove();
							}

						});

						for (var i = 0; i < $scope.assetList.length; i++) {
							var asset = $scope.assetList[i];

							$scope.assetLocation = new L.Marker(pixelsToLatLng(
									asset.x, asset.y), {
								icon : new L.DivIcon({
									className : 'assetslabel',
									html : '<span style=color:'
											+ asset.colorCode + '><i class="'
											+ asset.icon + '"></i></span>'
								})
							});
							$scope.assetLocation.assetid = i;
							$scope.assetLocation.macId = asset.macId;
							$scope.assetLocation.name = asset.name;
							$scope.assetLocation.assetType = asset.type;
							$scope.assetLocation.menuType = "Asset";
							$scope.assetLocation.addTo(map);

							$scope.assetLocation.on('mouseover', mouseoverpop);
							$scope.assetLocation.on('mouseout', mouseout);
							$scope.assetLocation.on('click', onAssetClick);
							$("#liveColorId" + i).val(asset.colorCode);
						}

						$timeout(
								function() {
									for (var i = 0; i < $scope.assetList.length; i++) {
										$("#liveColorId" + i)
												.val(
														$scope.assetList[i].colorCode == undefined
																|| $scope.assetList[i].colorCode == '' ? '#089b3c'
																: $scope.assetList[i].colorCode);
									}
								}, 50);
					}

					var $sortable = $('.sortable');

					$sortable.on('click', function() {

						var $this = $(this);

						var asc = $this.hasClass('asc');
						var desc = $this.hasClass('desc');
						$sortable.removeClass('asc').removeClass('desc');
						var isAsc;
						if (desc || (!asc && !desc)) {
							$this.addClass('asc');
							isAsc = true;
						} else {
							$this.addClass('desc');
							isAsc = false;
						}

						var id = this.id;
						$scope.sortType = id;
						$scope.tableSortOrder(id, isAsc)

					});

					$scope.getZoneDetails = function() {

						if ($rootScope.checkNetconnection() == false) {
							$rootScope.hideloading('#loadingBar');
							$rootScope.alertDialog('Please check the Internet Connection');  
 
							return;
						}

						$http({
							method : 'GET',
							url : './zone'
						})
								.then(
										function success(response) {
											$rootScope
													.hideloading('#loadingBar');
											if (response != null
													&& response.data != null
													&& response.data != "BAD_REQUEST") {

												$scope.noList = false;
												$scope.zoneLists = response.data.data;
												$scope.zoneList = [];

												$scope.zoneLayers = new L.FeatureGroup();
												map.addLayer($scope.zoneLayers);

												for (var i = 0; i < $scope.zoneLists.length; i++) {
													if ($scope.zoneLists[i].active) {
														if ($scope.zoneLists[i].zoneType == "Trackingzone") {
															$scope.zoneLists[i].zoneType = "Tracking zone";
														} else {
															$scope.zoneLists[i].zoneType = "Dead Zone";
														}
														$scope.zoneList
																.push($scope.zoneLists[i])

													}
												}
												$scope.zoneList
														.sort(GetSortOrder("name"));
												$scope.zoneFilterList = [];
												$scope.zoneFilterList.push({
													"type" : "All"
												});
												for (var i = 0; i < $scope.zoneList.length; i++) {
													if ($scope.zoneList[i].active) {
														$scope.zoneFilterList
																.push({
																	"type" : $scope.zoneList[i].name
																});
													}
												}

												fnShowZones();
												$scope.getTagColorConfigData();
											} else {
												$scope.noList = true;

												// $scope.errorMsg="No zones
												// are available";

											}
										});

					}
					$scope.filterZoneChange = function() {
						$scope.filterZone = $("#zoneFilter option:selected")
								.text();
						map.eachLayer(function(layer) {
							if (layer.menuType !== undefined
									&& layer.menuType == "Zone") {
								layer.remove();
							}
						});
 
						$scope.closebtn();

						fnShowZones();
						$scope.applyFilters();
						$scope.checkIfAllSelected();
					}
					$scope.filterSubTypeChange = function() {
						$scope.subtype = $(
								"#colorSubtypeSelect option:selected").text();
						$scope.applyFilters(); 
						$scope.closebtn();
						$scope.checkIfAllSelected();
					}
					
					$scope.filterTypeChange = function() {
						$scope.type = $(
						"#colorTypeSelect option:selected").text();
						$scope.updateSubTypeArray();
						$scope.subtype ='All';
						$scope.applyFilters(); 
						$scope.closebtn();
						$scope.checkIfAllSelected();
					}

					$("#selectedTagDetailView").hide();
					$scope.closebtn = function() {
						map.closePopup();
						$scope.selectedRow = null;
						$scope.selectedMacId = null;
						$("#selectedTagDetailView").hide(); 
					}

					function fnShowZones() {
						if ($scope.showZone) {
							if ($scope.zoneList.length != 0) {
								for (var i = 0; i < $scope.zoneList.length; i++) {
									if ($scope.filterZone == undefined
											|| $scope.filterZone == null
											|| $scope.filterZone == "All"
											|| $scope.zoneList[i].name == $scope.filterZone) {
										if ($scope.zoneList[i].active) {
											$scope.zoneList[i].area = $scope
													.area($scope.zoneList[i].coordinateList);
											var bounds = getBoundsArray($scope.zoneList[i].coordinateList);
											// var classification;
											if ($scope.zoneList[i].zoneClassification != "") {
												$scope.colorCode = $scope.zoneList[i].zoneClassification.color;
											} else {
												$scope.colorCode = "#ff7800"
											}

											var theme = {
												color : $scope.colorCode,
												weight : 1,
												fillColor : $scope.colorCode,
												fillOpacity : 0.5
											};

											var layer = null;

											if ($scope.zoneList[i].geometryType == undefined
													|| $scope.zoneList[i].geometryType == null
													|| $scope.zoneList[i].geometryType == "polygon"
													|| $scope.zoneList[i].geometryType == "") {

												$scope.zoneList[i].geometryType = "polygon";
												layer = L
														.polygon(bounds, theme);

											} else {
												layer = L.rectangle(bounds,
														theme);
											}

											layer.addTo(map).addTo(
													$scope.zoneLayers);

											layer.id = $scope.zoneList[i].id;
											layer.primaryid = $scope.zoneList[i].id
											layer.name = $scope.zoneList[i].name;
											layer.classification = $scope.zoneList[i].zoneClassification.className;
											layer.capacity = $scope.zoneList[i].capacity;
											layer.ppm = $scope.ppm;
											layer.coordinateList = bounds;

											layer.colorCode = $scope.colorCode;
											layer.geometryType = $scope.zoneList[i].geometryType;
											layer.zoneType = $scope.zoneList[i].zoneType;
											layer.zonearea = $scope.zoneList[i].area;
											layer.menuType = "Zone";

											if ($scope.filterZone != "All") {
												map
														.fitBounds(layer
																.getBounds());
											}
										}
									}

								}

								if ($scope.filterZone == "All") {
									map
											.fitBounds($scope.campusRect
													.getBounds());
								}

							}
						} else {
							map.eachLayer(function(layer) {
								if (layer.menuType !== undefined
										&& layer.menuType == "Zone") {
									layer.remove();
								}
							});
						}
					}
					$scope.isOnTrackedClicked = false;
					$scope.assetLiveColorArray = {};

					$scope.onTrack = function() {
						map.setZoom($scope.fitZoomLevel);
						var hasSelectedItem = false;
						if (!$scope.isOnTrackedClicked) {
							$scope.assetLiveColorArray = {};
							angular
									.forEach(
											$scope.assetList,
											function(asset, index) {
												if (asset.selected) {
													hasSelectedItem = true;
													var selectedColor = document
															.getElementById("liveColorId"
																	+ index).value;
													$scope.assetLiveColorArray[asset.macId] = selectedColor;

												}
											});

						}

						map.closePopup();
						if (hasSelectedItem) {
							$scope.stopBtnIcon.addTo(map);
							$scope.trackTagsBtn.remove();
							$scope.isOnTrackedClicked = true;
							connect(); 
						} else {

							if ($scope.isOnTrackedClicked) {
								$scope.stopBtnIcon.remove();
								$scope.trackTagsBtn.addTo(map);
							}

							$scope.isTrack = false;
							
							if($scope.liveMenu == "live" ){ 
								$scope.isTypeEnabled = true;
							}else
								$scope.isTypeEnabled = false;
							
							$scope.isOnTrackedClicked = false;
							$scope.alert_error_msg = "Please select the tag";
							$scope.showErrorMessageView();
							return;
						}
					}

					$scope.alert_error_msg = "Please select the tag"; 
					$scope.showErrorMessageView = function() { 
                        toastr.error($scope.alert_error_msg, "EI4.0");  
					}

					$scope.checkAll = function() {
						angular.forEach($scope.assetList, function(asset) {
							asset.selected = $scope.selectAll;
						});
					};
					
					$scope.checkIfAllSelected = function() {
						$scope.selectAll = $scope.assetList.every(function(
								asset) {
							return asset.selected == true
						})
					};
					$scope.getTagColorConfigData = function() {

						$rootScope.showloading('#loadingBar');
						$http({
							method : 'GET',
							url : './configuration/' + orgName,

						})
								.then(
										function success(response) {
											if (response != null
													&& response.data != null
													&& response.data.length != 0
													&& response.data != "BAD_REQUEST"
													&& response.data.data != undefined
													&& response.data.data.length != 0) {

												$rootScope
														.hideloading('#loadingBar');

												for (var i = 0; i < response.data.data.length; i++) {
													$scope.tagConfigColor.orgName = response.data.data[i].orgName;
													$scope.tagConfigColor.logAge = response.data.data[i].logAge;
													$scope.tagConfigColor.transactionDataAge = response.data.data[i].transactionDataAge;
													$scope.tagConfigColor.staffTagColor = response.data.data[i].staffTagColor;
													$scope.tagConfigColor.visitorTagColor = response.data.data[i].visitorTagColor;
													$scope.tagConfigColor.gageTagColor = response.data.data[i].gageTagColor;
													$scope.tagConfigColor.toolTagColor = response.data.data[i].toolTagColor;
													$scope.tagConfigColor.fixtureTagColor = response.data.data[i].fixtureTagColor;
													$scope.tagConfigColor.partTagColor = response.data.data[i].partTagColor;
													$scope.tagConfigColor.materialTagColor = response.data.data[i].materialTagColor;
													$scope.tagConfigColor.rmaTagColor = response.data.data[i].rmaTagColor;

													$scope.tagConfigColor.vendorTagColor = response.data.data[i].vendorTagColor;
													$scope.tagConfigColor.contractorTagColor = response.data.data[i].contractorTagColor;
													$scope.tagConfigColor.vehicleTagColor = response.data.data[i].vehicleTagColor;
													$scope.tagConfigColor.inventoryTagColor = response.data.data[i].inventoryTagColor;
													$scope.tagConfigColor.lslmTagColor = response.data.data[i].lslmtagColor;
													$scope.tagConfigColor.wareHouseTagColor = response.data.data[i].wareHouseTagColor;
													$scope.tagConfigColor.suppliesTagColor = response.data.data[i].suppliesTagColor;
													
													$scope.tagConfigColor.operationStartTime = response.data.data[i].operationStartTime;
													$scope.tagConfigColor.operationEndTime = response.data.data[i].operationEndTime;
												}

											} else if (response.data.status == "error") {
												$rootScope
														.hideloading('#loadingBar');
												$scope.tagConfigColor.staffTagColor = "#0000ff";
												$scope.tagConfigColor.visitorTagColor = "#0000ff";
												$scope.tagConfigColor.gageTagColor = "#0000ff";
												$scope.tagConfigColor.toolTagColor = "#0000ff";
												$scope.tagConfigColor.fixtureTagColor = "#0000ff";
												$scope.tagConfigColor.partTagColor = "#0000ff";
												$scope.tagConfigColor.materialTagColor = "#0000ff";
												$scope.tagConfigColor.rmaTagColor = "#0000ff";
												$scope.tagConfigColor.vendorTagColor = "#0000ff";
												$scope.tagConfigColor.contractorTagColor = "#0000ff";
												$scope.tagConfigColor.vehicleTagColor= "#0000ff";
												$scope.tagConfigColor.inventoryTagColor = "#0000ff";
												$scope.tagConfigColor.lslmTagColor = "#0000ff";
												$scope.tagConfigColor.wareHouseTagColor = "#0000ff";
												$scope.tagConfigColor.suppliesTagColor = "#0000ff";
											} else {
												$rootScope
														.hideloading('#loadingBar');
											}
											$scope.getQuuppaAssetDetail();

										},
										function error(response) {
											$rootScope
													.hideloading('#loadingBar');
											$rootScope.fnHttpError(response);
										});

					}
					
					
					
					$scope.assignedTagDetails = {};
					
					$scope.getAssignedTagDetails = function() {
						
						$rootScope.showloading('#loadingBar');
						
						if ($rootScope.checkNetconnection() == false) {
							$rootScope.hideloading('#loadingBar');
							return;
						}
						
						$http({
							method : 'GET',
							url : './getAssignedTagDetails'
						})
								.then(
										function success(response) {
											$rootScope
													.hideloading('#loadingBar');
											if (response != null
													&& response.data != null
													&& response.data != "BAD_REQUEST") {

												if (response.data.status == 'success') {
													$scope.assignedTagDetails = response.data.data;
													console.log($scope.assignedTagDetails);
												}
											} 
												
										},
										function error(response) {
											$rootScope
													.hideloading('#loadingBar');
											$rootScope.fnHttpError(response);
										});
					}
					
					$scope.getQuuppaAssetDetail = function() {

						map.eachLayer(function(layer) {
							if (layer.assetid !== undefined) {
								layer.remove();
							}

						});
						if ($rootScope.checkNetconnection() == false) {
							$rootScope.hideloading('#loadingBar');
							return;
						}

						$http({
							method : 'GET',
							url : './tagPositions'
						})
								.then(
										function success(response) {
											$rootScope
													.hideloading('#loadingBar');
											if (response != null
													&& response.data != null
													&& response.data != "BAD_REQUEST") {

												if (response.data.status == 'success') {

													$rootScope
															.hideloading('#loadingBar');
													if (response.data.data.length != 0) {

														$scope.getAssignedTagDetails();
														$scope.tagData = true;
														$scope.tagNoData = false;
														$scope.posDetails = response.data.data;
														$scope.applyFilters();
														 										
														$timeout(function() { 
															
															$scope.liveViewPageName = $rootScope.getPageName();
															
															if($scope.assignedTagMacId!=undefined &&  $scope.assignedTagMacId!='')
															{ 
																
																if($scope.assignedTagMacIdArray.length==1){
																	$scope.getAssienmentDetails($scope.assignedTagMacId); 
																}else
																{ 
																	$rootScope.trackingMacId = '';
																	$scope.assignedTagMacId = '';
																	
																	$scope.trackOutboundId= '';
																	$rootScope.trackOutboundId= '';
																}
																
																$scope.assignedTagMacIdArray.length = 0;
																var zoneName = '';
																var sameZone = true;
																
																for(var i=0; i<$scope.assetList.length; i++){
																	if( $scope.assetList[i].selected == true){
																		if(zoneName==''){
																			zoneName = $scope.assetList[i].zoneName;
																		}else
																		{
																			if(zoneName == $scope.assetList[i].zoneName){
																				sameZone = true;
																			}else
																			{
																				sameZone = false;
																				break;
																			}
																		} 
																	}
																}
																
																
																if(sameZone)
																{
																	for(var i=0; i<$scope.zoneList.length; i++){
																		if($scope.zoneList[i].name == zoneName){ 
																			var bounds = getBoundsArray($scope.zoneList[i].coordinateList);
																			map.fitBounds(bounds);
																		}
																	}
																	
																}
																
																// fnTrackTags();
																
															}else if($scope.trackOutboundId!=undefined &&  $scope.trackOutboundId!=''){
																 
																
																for(var i=0; i<$scope.zoneList.length; i++){
																	if($scope.zoneList[i].name == $scope.trackOutboundId){ 
																		var bounds = getBoundsArray($scope.zoneList[i].coordinateList);
																		map.fitBounds(bounds);
																		break;
																	}
																}
																
																var pName = "Live tracking for Outbound Area:- "+$scope.trackOutboundId;
																$rootScope.setPageName(pName);   
																
																$rootScope.trackingMacId = '';
																$scope.assignedTagMacId = '';
																
																$scope.trackOutboundId= '';
																$rootScope.trackOutboundId= '';
															}
															
														}, 1000);
														
													} else {
														$scope.tagData = false;
														$scope.tagNoData = true;
														$scope.noDataText = "No updates for last 1 minutes";
													}
												}
											}
										},
										function error(response) {
											$rootScope
													.hideloading('#loadingBar');
											$rootScope.fnHttpError(response);
										});

					}
					
					$scope.assienmentDetails = {};
					
					$scope.getAssienmentDetails = function(assignedTagMacId) {
						
						if ($rootScope.checkNetconnection() == false) {
							$rootScope.hideloading('#loadingBar');
							return;
						}
						
						$http({
							method : 'GET',
							url : './assignmentDetails/'+assignedTagMacId
						})
								.then(
										function success(response) {
											$rootScope
													.hideloading('#loadingBar');
											if (response != null
													&& response.data != null
													&& response.data != "BAD_REQUEST") {

												if (response.data.status == 'success') {

													$rootScope.hideloading('#loadingBar');
													
													$scope.assienmentDetails = response.data; 
													
													
															$timeout(function() {  
		  
																	var type = $scope.assienmentDetails.type;
																	
																	$scope.assienmentType = $scope.assienmentDetails.type;
																	
																	var pName = "Live tracking for ";
																	$rootScope.trackingMacId = '';
																	$scope.tagId = $scope.assienmentDetails.tagId;
																	$scope.macId = $scope.assienmentDetails.macId;
																	$scope.showDoNotUse = true;
																	
																	if (type  ==  "Employee") { 
																		
																		if($scope.assienmentDetails[type]!=undefined){
																			pName =  pName+"Employee:-" +$scope.assienmentDetails[type].firstName+" "+$scope.assienmentDetails[type].lastName;
																			$scope.empId = $scope.assienmentDetails[type].empId;
																			$scope.empName = $scope.assienmentDetails[type].firstName+" "+$scope.assienmentDetails[type].lastName;
																			$scope.department = $scope.assienmentDetails[type]['department'].deptName;
																			$scope.shift = $scope.assienmentDetails[type]['shiftDetails'].shiftName;
																		}else
																		{
																			pName =  pName+"Employee";
																			$scope.empId = '';
																			$scope.empName = '';
																			$scope.department = '';
																			$scope.shift = '';
																	
																		} 
																		
																		$("#employeeDetailsView").show(); 
																	} else if (type  ==  "Visitor" || type  ==  "Vendor" || type  ==  "Contractor" ) { 
																		
																		if($scope.assienmentDetails[type]!=undefined){
																			pName =  pName+ type+":-"  +$scope.assienmentDetails[type].firstName+" "+$scope.assienmentDetails[type].lastName;
																			$scope.visName = $scope.assienmentDetails[type].firstName+" "+$scope.assienmentDetails[type].lastName;
																			$scope.visCompany = $scope.assienmentDetails[type].company;
																			$scope.visDate = $scope.assienmentDetails[type].date;
																			$scope.visFromTime = $scope.assienmentDetails[type].fromTime;
																			$scope.visToTime = $scope.assienmentDetails[type].toTime;
																			$scope.visContactPerson = $scope.assienmentDetails[type].contactPerson;
																		}else{
																			pName =  pName+ type;
																			$scope.visName =  '';
																			$scope.visCompany =  '';
																			$scope.visDate =  '';
																			$scope.visFromTime =  '';
																			$scope.visToTime =  '';
																			$scope.visContactPerson =  '';
																		}
																		
																		
																		$("#visitorDetailsView").show(); 
																		
																	} else   if (type  ==  "Materials" ) { 
																		if($scope.assienmentDetails[type]!=undefined){
																			pName =  pName+  "Work Order No :-" +$scope.assienmentDetails[type][0].ordereNo;
																			$scope.ordereNo = $scope.assienmentDetails[type][0].ordereNo;
																			$scope.materialDetails = $scope.assienmentDetails[type];
																		}else{
																			pName =  pName+ type; 
																			$scope.ordereNo = '';
																			$scope.materialDetails = [];
																		}
																		
																		$("#configurationView").hide();
																		$("#tagListView").hide(); 
																		$("#materialDetailsView").show(); 
																		
																	} else  if (type  ==  "Asset NonChrono" ) { 
																		if($scope.assienmentDetails[type]!=undefined){
																			pName =  pName+  "Asset Id:-" +$scope.assienmentDetails[type].assetId;
																			
																			$scope.assetId = $scope.assienmentDetails[type].assetId;
																			$scope.assetType = $scope.assienmentDetails[type].assetType;
																			$scope.assetSelectedSubType = $scope.assienmentDetails[type].subType;
																			$scope.assetSelectedType = $scope.assienmentDetails[type].type;
																			$scope.rangeValue = $scope.assienmentDetails[type].rangeValue;
																			$scope.manufacturer = $scope.assienmentDetails[type].manufacturer;
																		}else{

																			pName =  pName+ type;
																			
																			$scope.assetId = '';
																			$scope.assetType = '';
																			$scope.assetSelectedSubType = '';
																			$scope.assetSelectedType = '';
																			$scope.rangeValue = '';
																			$scope.manufacturer = '';
																			$scope.validityDate = ''; 
																			
																		}
																		 
																		$("#assetDetailsView").show(); 
																		
																	} else  if (type  ==  "Asset Chrono" ) { 
																		
																		if($scope.assienmentDetails[type]!=undefined){
																			pName =  pName+  "Asset Id:-" +$scope.assienmentDetails[type].assetId;
																			
																			$scope.assetId = $scope.assienmentDetails[type].assetId;
																			$scope.assetType = $scope.assienmentDetails[type].assetType;
																			$scope.assetSelectedSubType = $scope.assienmentDetails[type].subType;
																			$scope.assetSelectedType = $scope.assienmentDetails[type].type;
																			$scope.rangeValue = $scope.assienmentDetails[type].rangeValue;
																			$scope.manufacturer = $scope.assienmentDetails[type].manufacturer; 
																			$scope.validityDate = $scope.assienmentDetails[type].validityDate;  
																		}else{
																			
																			pName =  pName+ type;
																			$scope.assetId =  ''; 
																			$scope.assetType =  ''; 
																			$scope.assetSelectedSubType =  ''; 
																			$scope.assetSelectedType =  ''; 
																			$scope.rangeValue =  ''; 
																			$scope.manufacturer =  ''; 
																			$scope.validityDate = ''; 
																		}
																		 
																		
																	   if($scope.validityDate!=undefined && $scope.validityDate!=null && $scope.validityDate!='')
					                                                   {
																		   if (moment($scope.validityDate, 'YYYY-MMM-DD')<moment(moment(new Date(), 'YYYY-MMM-DD').format('YYYY-MMM-DD'), 'YYYY-MMM-DD'))
																		   { 
							                         	 						$scope.showDoNotUse = false;
						                         							}else
						                         	 							$scope.showDoNotUse = true; 
					                                                     
					                                                   }else
					                         	 							$scope.showDoNotUse = true;
																		
																		$("#assetDetailsView").show(); 
																		
																		
																	} else  if (type  ==  "Vehicles" ) { 
																		
																		if($scope.assienmentDetails[type]!=undefined){
																			pName =  pName+  "Vehicle Id:-" +$scope.assienmentDetails[type].assetId;
																			
																			$scope.assetId = $scope.assienmentDetails[type].assetId;
																			$scope.assetType = $scope.assienmentDetails[type].assetType;
																			$scope.subType = $scope.assienmentDetails[type].subType; 
																		}else{
																			pName =  pName+  type;
																			
																			$scope.assetId =  ''; 
																			$scope.assetType =  ''; 
																			$scope.subType =  '';  
																		}
																		
																		
																		$("#vehicleDetailsView").show(); 
																		
																	} else  if (type  ==  "LSLM" || type  ==  "Inventory" ) { 
																		
																		if($scope.assienmentDetails[type]!=undefined){
																			pName =  pName+  "UPC Code:-" +$scope.assienmentDetails[type].upcCode;
																			
																			$scope.assignBatchNo = $scope.assienmentDetails[type].batchNo;
																			$scope.assignSKUNo = $scope.assienmentDetails[type].skuNo; 
																			$scope.assignUPCCode = $scope.assienmentDetails[type].upcCode; 
																		}else{
																			pName =  pName+  type;
																			
																			$scope.assignBatchNo = '';  
																			$scope.assignSKUNo = ''; 
																			$scope.assignUPCCode = ''; 
																		}
																		
																		
																		$("#skuDetailsView").show(); 
																		
																	}else  if (type  ==  "WareHouse" ) { 
																		if($scope.assienmentDetails[type]!=undefined){
																			pName =  pName+  "WareHouse Materials SKU Number:-" +$scope.assienmentDetails[type]['SKU No'];
																			
																			$scope.wareHouseData = $scope.assienmentDetails[type]; 
																			

																			delete $scope.wareHouseData['expiry_date'];
																			delete $scope.wareHouseData['modified_date'];
																			delete $scope.wareHouseData['created_date'];
																			delete $scope.wareHouseData['manufacture_date'];
																			delete $scope.wareHouseData['dispatch_date'];
																			delete $scope.wareHouseData['zone_id'];
																			delete $scope.wareHouseData['mac_id'];
																			
																			$scope.wareHouseData['Issue Date'] =moment(new Date($scope.wareHouseData['issue_date'])).format('YYYY-MMM-DD');
																			delete $scope.wareHouseData['issue_date'];
																			 
																			
																			$scope.wareHouseData['Tag Id'] = $scope.wareHouseData['tag_id'];
																			delete $scope.wareHouseData['tag_id'];
																			
																			$scope.wareHouseData['Arrival Date'] = moment(new Date($scope.wareHouseData['arrival_date'])).format('YYYY-MMM-DD');
																			delete $scope.wareHouseData['arrival_date'];
																			  
																			$scope.expiryDate = $scope.wareHouseData['Expiry Date'];
																			
																			 if($scope.expiryDate!=undefined && $scope.expiryDate!=null && $scope.expiryDate!='')
							                                                   {
							                                                    	if (moment($scope.expiryDate, 'DD-MM-YYYY')<moment(moment(new Date(), 'DD-MM-YYYY').format('DD-MM-YYYY'), 'DD-MM-YYYY')){ 
								                         	 							$scope.showDoNotUse = false;
								                         							}else
								                         	 							$scope.showDoNotUse = true;
								                                                     
							                                                    	$scope.wareHouseData['Expiry Date'] = moment($scope.expiryDate, 'DD-MM-YYYY').format('YYYY-MMM-DD');
							                                                   }else
							                         	 							$scope.showDoNotUse = true;
																			 
																			addWarehouseDetailsTable();
																			$scope.showWarehouseDetails('sku');
																		}else{
																			pName =  pName+  type; 
																			$scope.wareHouseData = null; 
																		}
																		
																		$("#wareHouseDetailsView").show(); 
																	}

																	if($scope.assignedTagMacId !=null && $scope.assignedTagMacId!=''){ 
																		$rootScope.setPageName(pName);   
																		$scope.assignedTagMacId='';
																	}
														}, 1000); 
													 
												}
											}
										},
										function error(response) {
											$rootScope
													.hideloading('#loadingBar');
											$rootScope.fnHttpError(response);
										});
					}
					
					
					$scope.showWarehouseDetails = function(type)
					{
						if(type=='storage'){ 
							$("#warehouseDetailsTable").hide();  
							$("#warehouseDetailsInfo").show();
							
							if($scope.wareHouseData==null || !$scope.wareHouseData.hasOwnProperty('Storage Info'))
							{
								$("#warehouseDetailsInfo").text('Storage Info not available.');
							}else
								$("#warehouseDetailsInfo").text($scope.wareHouseData['Storage Info']);
							  

							$('#storageDetails').addClass('btn-primary');
							$('#deliveryDetails').removeClass('btn-primary');
							$('#skuDetails').removeClass('btn-primary');
							

							$('#storageDetails').css("color", "#ffffff");
							$('#deliveryDetails').css("color", "#000000");
							$('#skuDetails').css("color", "#000000");
							 
						}else if(type=='delivery'){ 
							$("#warehouseDetailsTable").hide();  
							$("#warehouseDetailsInfo").show();

							$('#storageDetails').removeClass('btn-primary');
							$('#deliveryDetails').addClass('btn-primary');
							$('#skuDetails').removeClass('btn-primary');

							$('#storageDetails').css("color", "#000000");
							$('#deliveryDetails').css("color", "#ffffff");
							$('#skuDetails').css("color", "#000000");
							
							if($scope.wareHouseData==null || !$scope.wareHouseData.hasOwnProperty('Delivery Info'))
							{
								$("#warehouseDetailsInfo").text('Delivery Info not available.');
							}else
								$("#warehouseDetailsInfo").text($scope.wareHouseData['Delivery Info']);
							
						}else if(type=='sku'){

							$('#storageDetails').removeClass('btn-primary');
							$('#deliveryDetails').removeClass('btn-primary');
							$('#skuDetails').addClass('btn-primary');

							$('#storageDetails').css("color", "#000000");
							$('#deliveryDetails').css("color", "#000000");
							$('#skuDetails').css("color", "#ffffff");
							
							$("#warehouseDetailsTable").show();  
							$("#warehouseDetailsInfo").hide();
						}
						
					}
					
					function addWarehouseDetailsTable() {
						
						
						  var myTableDiv = document.getElementById("warehouseDetailsTable");
						  myTableDiv.innerHTML = '';
						  
						  var table = document.createElement('TABLE'); 
						  table.className += "table table-bordered tbodyWidth";
						  
						  var tableBody = document.createElement('TBODY');
						  table.appendChild(tableBody);

						 var keyNames =  Object.keys($scope.wareHouseData); 
						  
						  for (var i = 0; i < keyNames.length; i++) {
						    var tr = document.createElement('TR');
						    tableBody.appendChild(tr);

						    for (var j = 0; j < 2; j++) {
						      var td = document.createElement('TD');
						      td.width = '75';
						      if(j==0)
						    	  td.appendChild(document.createTextNode(keyNames[i]+":"));
						      else
						      {
						    	  if($scope.wareHouseData[''+keyNames[i]]==null||$scope.wareHouseData[''+keyNames[i]]==''||
						    			  $scope.wareHouseData[''+keyNames[i]]=='null')
						    		  td.appendChild(document.createTextNode(''));
						    	  else						    		  
						    		  td.appendChild(document.createTextNode($scope.wareHouseData[''+keyNames[i]]));
						      }
						      tr.appendChild(td);
						    }
						  }
						  myTableDiv.appendChild(table);
						}
					
					$scope.sendBuzzerAlert= function(){  
						
						if($scope.selectedMacId!=undefined && $scope.selectedMacId!=''){
							$rootScope.showloading('#loadingBar');
							$http({
								method : 'GET',
								url : './buzzerAlert/'+$scope.selectedMacId
							})
									.then(
											function success(response) {
												$rootScope.hideloading('#loadingBar');
												if (response != null
														&& response.data != null
														&& response.data != "BAD_REQUEST") {
	
													if (response.data.status == 'success') {
	                                                    toastr.success(response.data.message, "EI4.0"); 													
													}else
	                                                    toastr.error(response.data.message, "EI4.0"); 
												}
											},
											function error(response) {
												$rootScope
														.hideloading('#loadingBar');
												$rootScope.fnHttpError(response);
											});

						}else{
                            toastr.error('Please select tag to sent alert.', "EI4.0");   
						}
					}
					
					$scope.updateLiveData = function() {

						$scope.assetList = [];

						var i = 0;
						for ( var macId in $scope.assetLiveColorArray) {

							var assetData = $scope.assetTagList[macId];
							var assetLive = $scope.liveTagDetails[macId];

							assetData.x = assetLive.x;
							assetData.y = assetLive.y;

							assetData.name = assetLive.name;
							assetData.macId = assetLive.macId;
							assetData.type = assetLive.type;
							assetData.id = i;
							assetData.subType = assetLive.subType;
							assetData.zoneName = assetLive.zoneName;
							assetData.last_seen = assetLive.lastSeen;
							assetData.colorCode = $scope.assetLiveColorArray[macId];
							assetData.selected = true;

							$scope.assetList.push(assetData);
							i++;
						}

						$scope.updateAssetsIcons();
					 
						if ($scope.selectedMacId != null) {  
							
							$timeout(function() {
								var assetLive = $scope.liveTagDetails[$scope.selectedMacId];
								$scope.last_seen =  $scope.convertTimestamp(assetLive.lastSeen); 
								$scope.zoneName = assetLive.zoneName;
								$scope.name = assetLive.name;
							}, 100);
						}

					}
					 
				

					$scope.applyFilters = function() {
 
						$scope.closebtn();

						$scope.assetTagList = {};
						$scope.assetList = [];
						var colorVal = '#089b3c';
						
						var searchValue = $scope.searchTag.toLowerCase();
						
						var filterData = [];
						if($scope.viewType =='Live View')
							filterData = $scope.posDetails;
						else
							filterData = $scope.historicalAssetData;
						
						for (var i = 0; i < filterData.length; i++) {
  
									if (($scope.subtype == "All" &&  $scope.subTypesArray1.includes(filterData[i].type)) 
											|| ($scope.type == "All" &&  $scope.subtype == "All" && filterData[i].type =='WareHouse') 
											|| ($scope.subtype == filterData[i].type || ($scope.subtype == "WareHouse Materials" && "WareHouse" == filterData[i].type ))
											|| (($scope.type == "WIP" || $scope.type == "All") &&  (
													($scope.subtype == "All" &&  $scope.subTypesArray1.includes(filterData[i].subType)) ||
													$scope.subtype == filterData[i].subType))
											||(  $scope.type == "All" && $scope.subtype == "All" &&$scope.otherStatusTypes.includes(filterData[i].type))) {
										 
									if ($scope.filterZone == null
											|| $scope.filterZone == "All"
											|| filterData[i].zoneName == $scope.filterZone) {
										if ($scope.searchTag == ''
												||  filterData[i].assignmentId
														.toLowerCase()
														.includes(searchValue)
												|| ( $scope.type == "WareHouse" ? "materials".includes(searchValue) : filterData[i].type
														.toLowerCase()
														.includes(searchValue))
												|| ( $scope.type == "All" && filterData[i].type=="WareHouse"  ? "materials".includes(searchValue) : filterData[i].type
														.toLowerCase()
														.includes(searchValue))
												||  (filterData[i].zoneName !=undefined && filterData[i].zoneName !=null && filterData[i].zoneName
														.toLowerCase()
														.includes(searchValue))) {

											
											
											if($scope.assignedTagMacIdArray==null || $scope.assignedTagMacIdArray.length==0 ||
													$scope.assignedTagMacIdArray.includes(filterData[i].macId)){ 
												 
												if($scope.trackOutboundId==null || $scope.trackOutboundId=='' || $scope.trackOutboundId==filterData[i].zoneName)
												{
												
													var asset = {};
		
													asset.x = filterData[i].x;
													asset.y = filterData[i].y;
		
													if(filterData[i].type == "Vendor")
														asset.name = "Vendor "+filterData[i].assignmentId;
													else if(filterData[i].type == "Visitor")
														asset.name = "Visitor "+filterData[i].assignmentId;
													else if(filterData[i].type == "Contractor")
														asset.name = "Contractor "+filterData[i].assignmentId;
													else if(filterData[i].type == "Materials" || filterData[i].type == "RMA"
														|| filterData[i].type == "Material" || filterData[i].type == "Parts")
														asset.name = "WO "+filterData[i].assignmentId;
													else  
													{
														asset.name =  filterData[i].assignmentId; 
													}
													
													asset.tagId = filterData[i].tagId;
													asset.macId = filterData[i].macId;
													if ($scope.type == "WareHouse" || filterData[i].type == "WareHouse")
														asset.type = 'Materials';
													else if ($scope.type == "WIP" || filterData[i].type == "Materials")
														asset.type = filterData[i].subType;
													else 
														asset.type = filterData[i].type;
													
		
													asset.assetDisplayType = filterData[i].type;
													asset.subType = filterData[i].subType;
													asset.id = i;
													asset.zoneName = filterData[i].zoneName;
													asset.last_seen = filterData[i].lastSeen;
													 
													 
													if($scope.assignedTagMacIdArray.includes(asset.macId)){ 
														asset.selected = true;
													}else
													{
														 if($scope.trackOutboundId!=undefined &&  $scope.trackOutboundId!=''
															 &&  $scope.trackOutboundId == filterData[i].zoneName){
															 asset.selected = true;
														 }else
															 asset.selected = false;
													}
		
													var icon = "";
		
													if ($scope.type == "WareHouse")
													{ 
														colorVal = $scope.tagConfigColor.wareHouseTagColor;
														icon = "fas fa-warehouse";
													}else if (asset.type == "Employee") {
														colorVal = $scope.tagConfigColor.staffTagColor;// 21089e
														icon = "fa fa-user";
													} else if (asset.type == "Visitor") {
														colorVal = $scope.tagConfigColor.visitorTagColor;
														icon = "fa fa-user";
													} else if (asset.type == "Vendor") {
														colorVal = $scope.tagConfigColor.vendorTagColor;
														icon = "fa fa-user"; 
													} else if (asset.type == "Contractor") {
														colorVal = $scope.tagConfigColor.contractorTagColor;
														icon = "fa fa-user";
													} else if (asset.type == "Gages") {
														colorVal = $scope.tagConfigColor.gageTagColor;
														icon = "fas fa-tools";
													} else if (asset.type == "Tools") {
														colorVal = $scope.tagConfigColor.toolTagColor;
														icon = "fas fa-tools";
													} else if (asset.type == "Fixtures") {
														colorVal = $scope.tagConfigColor.fixtureTagColor;
														icon = "fas fa-tools";
													} else if (asset.type == "Vehicles") {
														colorVal = $scope.tagConfigColor.vehicleTagColor;
														icon = "fas fa-taxi";
													} else if (asset.type == "Parts") {
														colorVal = $scope.tagConfigColor.partTagColor;
														icon = "fas fa-cog";
													} else if (asset.type == "Material" || asset.type == "Materials") {
														colorVal = $scope.tagConfigColor.materialTagColor;
														icon = "fas fa-cog";
													} else if (asset.type == "RMA") {
														colorVal = $scope.tagConfigColor.rmaTagColor;
														icon = "fas fa-cog";		
													} else if (asset.type == "Inventory") {
														colorVal = $scope.tagConfigColor.inventoryTagColor;
														icon = "fab fa-deezer";
													} else if (asset.type == "LSLM") {
														colorVal = $scope.tagConfigColor.lslmTagColor;
														icon = "fas fa-hourglass-half";
													} else if (asset.type == "WareHouse") {
														colorVal = $scope.tagConfigColor.wareHouseTagColor;
														icon = "fas fa-warehouse";
													}else if (asset.type == "Supplies") {
														colorVal = $scope.tagConfigColor.suppliesTagColor;
														icon = "fas fa-cart-arrow-down";
													}else   {
														colorVal = '#089b3c'; 
														icon = "fab fa-bluetooth";
													} 
													if ($scope.assetLiveColorArray
															.hasOwnProperty(asset.macId))
														asset.colorCode = $scope.assetLiveColorArray[asset.macId];
													else
														asset.colorCode = colorVal;
		
													asset.icon = icon; 
													$scope.assetTagList[filterData[i].macId] = asset;
													
													
												}

											}
										}
									}
								} 
						}
						
						for ( var k in $scope.assetTagList) {
							 
							if($scope.assignedTagMacId!=undefined && $scope.assignedTagMacId !=''  && $scope.isTrack == true){ 
								if($scope.assignedTagMacIdArray.includes($scope.assetTagList[k].macId))
								{
									$scope.assetList.push($scope.assetTagList[k]);
								}
							}else
								$scope.assetList.push($scope.assetTagList[k]);
							
						}

						if ($scope.assetList.length == 0
								&& $scope.filterZone != "All") {
							if ($scope.isTrack) {
								$scope.noDataText = "No tag found in "
										+ $scope.filterZone + " at "
										+ $scope.current_time;
							} else
								$scope.noDataText = "No tag found in "
										+ $scope.filterZone;
							$scope.tagData = false;
							$scope.tagNoData = true;
							$scope.isFilter = true;
						} else if ($scope.assetList.length == 0
								&& $scope.subtype != "All") {
							if ($scope.isTrack) { 
								$scope.noDataText = "No tag found in "
										+ $scope.subtype + " at "
										+ $scope.current_time;
							} else
								$scope.noDataText = "No tag found for "
										+ $scope.subtype;
							$scope.tagData = false;
							$scope.tagNoData = true;
							$scope.isFilter = true;
						} else if ($scope.assetList.length == 0
								&& $scope.searchTag != "") {
							$scope.noDataText = "No tag found";
							$scope.tagData = false;
							$scope.tagNoData = true;
							$scope.isFilter = false;
						} else {
							$scope.tagData = true;
							$scope.tagNoData = false;
							$scope.isFilter = false;
						}

						if ($scope.sortType != '') {
							$scope.tableSortOrder($scope.sortType,
									$scope.sortisAsc);
						} else {
							$scope.setAssetsIcons();
							$timeout(
									function() {
										for (var i = 0; i < $scope.assetList.length; i++) {
											$("#liveColorId" + i)
													.val(
															$scope.assetList[i].colorCode == undefined
																	|| $scope.assetList[i].colorCode == '' ? '#089b3c'
																	: $scope.assetList[i].colorCode);
										}
									}, 100);
						}
 
						if ($scope.selectedRow != null) {
							// $scope.last_seen=
							// $scope.assetList[$scope.selectedRow].last_seen;
							$scope.last_seen = $scope
									.convertTimestamp($scope.assetList[$scope.selectedRow].last_seen);

							$scope.zoneName = $scope.assetList[$scope.selectedRow].zoneName;
							$scope.name = $scope.assetList[$scope.selectedRow].name;
						}

					}

					$scope.getLiveColorCode = function(name, colorcode) {

						for (var i = 0; i < $scope.assetLiveColorArray.length; i++) {
							if ($scope.assetLiveColorArray[i].name == name) {
								return $scope.assetLiveColorArray[i].color;
							}
						}

						return colorcode;
					}

					var tooltipPopup;
					function mouseout(e) {
						map.closePopup(tooltipPopup);
					}

					function mouseoverpop(e) {
						tooltipPopup = L.popup({
							offset : L.point(0, -5)
						});
						
						if(e.target.name!='')
							tooltipPopup.setContent(e.target.name);
						else 
							tooltipPopup.setContent(e.target.assetType);
						
						tooltipPopup.setLatLng(e.target.getLatLng());
						tooltipPopup.openOn(map);

					}
					
					var selectedRowCSS = {
						'background' : '#4da3ff',
						'color' : '#000000',
						'font-weight' : 'bold'
					};
					var unSelectedRowCSS = {
						'background' : '#ffffff',
						'color' : '#000000',
						'font-weight' : 'normal'
					};

					function onAssetClick(e) {

						$(".tbodyWidth").css({
							'height' : "200px"
						});

						$("#selectedTagDetailView").show(); 
						$scope.selectedRow = e.sourceTarget.assetid;
						$scope.selectedMacId = e.sourceTarget.macId;
						
						$scope.name = $scope.assetList[e.sourceTarget.assetid].name;
						$scope.zoneName = $scope.assetList[e.sourceTarget.assetid].zoneName;
						$scope.mac = $scope.assetList[e.sourceTarget.assetid].mac; 
						$scope.selectedTagId = $scope.assetList[e.sourceTarget.assetid].tagId;

						// $scope.last_seen=
						// $scope.assetList[e.sourceTarget.assetid].last_seen;
						$scope.last_seen = $scope
								.convertTimestamp($scope.assetList[e.sourceTarget.assetid].last_seen);

 						 
//
						$scope.updateDetailsView();
						
						$scope.$apply();
						var rowCount = ($('#assetTable tr').length) - 1;
						for (var i = 0; i < rowCount; i++) {
							$(
									"#assetTable > tbody > tr:nth-child("
											+ (i + 1) + ")").css(
									unSelectedRowCSS);
						}

						$(
								"#assetTable > tbody > tr:nth-child("
										+ (e.sourceTarget.assetid + 1) + ")")
								.css(selectedRowCSS);

						map.eachLayer(function(layer) {
							if (layer.assetid !== undefined) {
								if (e.sourceTarget.assetid == layer.assetid) {
									layer.setOpacity(1);
								} else {
									layer.setOpacity(0.3);
								}
							}
						});

						map
								.panTo(pixelsToLatLng(
										parseFloat($scope.assetList[e.sourceTarget.assetid].x),
										parseFloat($scope.assetList[e.sourceTarget.assetid].y)));
						
						
						 $scope.selectedAssetType = e.sourceTarget.assetType;
	                        
	                        if($scope.selectedAssetType =='Inventory' || $scope.selectedAssetType =='LSLM'){
	                            $('#deductQuantity').val('');
	                            $('#idInventoryModal').modal('show');
	                            $scope.getInventoryQuantityDetails();
	                        }else if($scope.assetList[e.sourceTarget.assetid].assetDisplayType =='WareHouse'){
	                        	$scope.getAssienmentDetails($scope.selectedMacId); 
	                        }else
                        	{
                        		fnTagsSearchView();
                        		fnMoveScrollBar();
                        	}
	                        
	                        
	                        
					}
					function fnMoveScrollBar() {
						var rows = $('#assetTable tr');
						var lSelectedRow = $scope.selectedRow;
						// to move scrollbar at zeroth index
						lSelectedRow = lSelectedRow +1;
						rows[lSelectedRow].scrollIntoView({
							behavior : 'smooth',
							block : 'center'
						});

					}

					$scope.showMoreInfoBtn = false;
					$scope.selectZone = function(index) {

						$(".tbodyWidth").css({
							'height' : "200px"
						});

						$("#selectedTagDetailView").show();  
						$scope.closeDetailsView();
						
						$scope.selectedRow = index;
						var rowCount = ($('#assetTable tr').length) - 1;
						for (var i = 0; i < rowCount; i++) {
							$(
									"#assetTable > tbody > tr:nth-child("
											+ (i + 1) + ")").css(
									unSelectedRowCSS);
						}

						$(
								"#assetTable > tbody > tr:nth-child("
										+ (index + 1) + ")")
								.css(selectedRowCSS);

						map.eachLayer(function(layer) {
							if (layer.assetid !== undefined) {
								if (index == layer.assetid) {
									layer.setOpacity(1);
									tooltipPopup = L.popup({
										offset : L.point(0, -5)
									});
									if(layer.name!='')
										tooltipPopup.setContent(layer.name);
									else 
										tooltipPopup.setContent(layer.assetType);
									
									tooltipPopup.setLatLng(layer._latlng);
									tooltipPopup.openOn(map);
								} else {
									layer.setOpacity(0.3);
								}
							}
						});
						$scope.name = $scope.assetList[index].name;
						$scope.zoneName = $scope.assetList[index].zoneName;
						$scope.selectedMacId = $scope.assetList[index].macId;
						$scope.last_seen = $scope
								.convertTimestamp($scope.assetList[index].last_seen);
						
						
						$scope.updateDetailsView();
							 		 
						$scope.ibeacon_minor = $scope.assetList[index].ibeacon_minor;
						$scope.ibeacon_uuid = $scope.assetList[index].ibeacon_uuid;
						$scope.ibeacon_major = $scope.assetList[index].ibeacon_major;

						if (map.getZoom() > $scope.fitZoomLevel) {
							map.panTo(pixelsToLatLng(
									parseFloat($scope.assetList[index].x),
									parseFloat($scope.assetList[index].y)));
						}

					}
					
					$scope.updateDetailsView = function()
					{
						$scope.columnName0 = "";
						$scope.columnName = "";
						$scope.columnName1 = "";
						$scope.columnValue0 = "";
						$scope.columnValue = "";
						$scope.columnValue1 = "";
						
						$("#columnDiv").show(); 
						$("#column1Div").show(); 
						  
						$scope.showMoreInfoBtn = true;
						$scope.columnValue0 = $scope.name; 
						if($scope.assetList[$scope.selectedRow].type == "Vendor" || $scope.assetList[$scope.selectedRow].type == "Visitor" || 
								$scope.assetList[$scope.selectedRow].type == "Contractor" || $scope.assetList[$scope.selectedRow].type == "Employee")
						{
							$scope.columnName = "First Name";
							$scope.columnName1 = "Last Name";
							$scope.columnName0 = "Id";
							
							
							if($scope.assignedTagDetails!=null && $scope.assignedTagDetails.hasOwnProperty($scope.selectedMacId ))
							{
								$scope.columnValue = $scope.assignedTagDetails[$scope.selectedMacId].firstName;
								$scope.columnValue1 = $scope.assignedTagDetails[$scope.selectedMacId].lastName;
							}else
							{
								$scope.columnValue = "";
								$scope.columnValue1 = ""; 
							} 
						}else if($scope.assetList[$scope.selectedRow].type == "Gages" || $scope.assetList[$scope.selectedRow].type == "Tools" || 
								$scope.assetList[$scope.selectedRow].type == "Fixtures" || $scope.assetList[$scope.selectedRow].type == "Supplies"
									 || $scope.assetList[$scope.selectedRow].type == "Vehicles")
						{ 
							
							$scope.columnName = "Asset Type";
							$scope.columnName1 = "Sub Type";
							$scope.columnName0 = "Asset Id";
							if($scope.assignedTagDetails!=null && $scope.assignedTagDetails.hasOwnProperty($scope.selectedMacId ))
							{
								$scope.columnValue = $scope.assignedTagDetails[$scope.selectedMacId].assetType;
								$scope.columnValue1 = $scope.assignedTagDetails[$scope.selectedMacId].subType;
							}else
							{
								$scope.columnValue = "";
								$scope.columnValue1 = ""; 
							}
							
						}else if($scope.assetList[$scope.selectedRow].type == "LSLM" || $scope.assetList[$scope.selectedRow].type == "Inventory")
						{
							$scope.columnName = "Description";
							$scope.columnName1 = "Expiry Date";
							$scope.columnName0 = "UPC Code";
							
							if($scope.assignedTagDetails!=null && $scope.assignedTagDetails.hasOwnProperty($scope.selectedMacId ))
							{
								$scope.columnValue = $scope.assignedTagDetails[$scope.selectedMacId].description;
								$scope.columnValue1 = $scope.assignedTagDetails[$scope.selectedMacId].expiryDate;
							}else
							{
								$scope.columnValue = "";
								$scope.columnValue1 = ""; 
							}
							
						}else if($scope.assetList[$scope.selectedRow].type == "Material" || $scope.assetList[$scope.selectedRow].type == "Materials")
						{
							$scope.columnName = "Source";
							$scope.columnName1 = "Destination";
							$scope.columnName0 = "Order No";
							if($scope.assignedTagDetails!=null && $scope.assignedTagDetails.hasOwnProperty($scope.selectedMacId ))
							{
								var _type = $scope.assignedTagDetails[$scope.selectedMacId].type;
								
								if(_type=="WareHouse")
								{
									$scope.columnName = "";
									$scope.columnName1 = "";
									$scope.columnName0 = "SKU No";
									$("#columnDiv").hide(); 
									$("#column1Div").hide(); 
									
									$scope.columnValue = $scope.assignedTagDetails[$scope.selectedMacId].skuNo;
									$scope.columnValue1 = ""; 
								}else
								{
									$scope.columnValue = $scope.assignedTagDetails[$scope.selectedMacId].source;
									$scope.columnValue1 = $scope.assignedTagDetails[$scope.selectedMacId].destination;
								}
								
							}else
							{
								$scope.columnValue = "";
								$scope.columnValue1 = ""; 
							}
							
						}else if($scope.assetList[$scope.selectedRow].type == "Parts" || $scope.assetList[$scope.selectedRow].type == "RMA")
						{ 
							$scope.columnName = "Source";
							$scope.columnName1 = "Destination";
							$scope.columnName0 = "Order No";
							if($scope.assignedTagDetails!=null && $scope.assignedTagDetails.hasOwnProperty($scope.selectedMacId ))
							{
 								$scope.columnValue = $scope.assignedTagDetails[$scope.selectedMacId].source;
								$scope.columnValue1 = $scope.assignedTagDetails[$scope.selectedMacId].destination; 
								
							}else
							{
								$scope.columnValue = "";
								$scope.columnValue1 = ""; 
							}
							
						}else
						{

							$scope.columnName0 = "Mac Id";
							$scope.columnValue0 = $scope.selectedMacId;
							$scope.columnName = "Status";
							$scope.columnValue  = $scope.assetList[$scope.selectedRow].type ;
							$scope.showMoreInfoBtn = false;
							$("#column1Div").hide(); 
						}
					}
					
					$scope.convertTimestamp = function(timestamp) {

						var now_utc = new Date(timestamp);
						var mnth = ("0" + (now_utc.getMonth() + 1)).slice(-2);
						var day = ("0" + now_utc.getDate()).slice(-2);
						var year = now_utc.getFullYear();
						var hour = ("0" + now_utc.getHours()).slice(-2);
						var mins = ("0" + now_utc.getMinutes()).slice(-2);
						var secs = ("0" + now_utc.getSeconds()).slice(-2);
						var date_str = moment(now_utc).format('MMM. DD, YYYY')
								+ ' ' + hour + ':' + mins + ':' + secs;
						return date_str;

					}

					var stompClient = null;
					function connect() {
						var socket = new SockJS('/ei4o/gs-guide-websocket');
						stompClient = Stomp.over(socket);
						stompClient
								.connect(
										{},
										function(frame) {
											// console.log('Connected: ' +
											// frame);
											subscribe();
											stompClient
													.subscribe(
															'/topic/livedata',
															function(greeting) {
																
																if($scope.viewType !='Track Back')
																 {
																	// messageReceivedTime();
																	if ($scope.liveMenu == "live"
																			|| $scope.liveMenu == "peopleLiveView"
																			|| $scope.liveMenu == "assetLiveView"
																			|| $scope.liveMenu == "workorderLiveView"
																		    || $scope.liveMenu == "skuLiveView"
																			|| $scope.liveMenu == "warehouseLiveView") {
																		$scope.liveTagDetails = JSON
																				.parse(greeting.body).tagPostions;
																		$scope
																				.updateLiveData();
																	} else {
																		disconnect();
																	}
																 } 

															});
										});
					}

					function disconnect() {
						if (stompClient !== null) {
							unsubscribe();
							stompClient.disconnect();
						}
						// console.log("Disconnected");
					}

					function subscribe() {
						stompClient.send("/ei4o/subscribe", {}, JSON
								.stringify({
									'type' : 'subscribe',
									'viewType':$scope.liveMenu
								}));
					}

					function unsubscribe() {
						stompClient.send("/ei4o/subscribe", {}, JSON
								.stringify({
									'type' : 'unsubscribe' 
								}));
					}

					function updateClock() {

						var now = new Date();
						var hour = now.getHours();
						var mins = now.getMinutes();
						var secs = now.getSeconds();
						if (hour <= 9) {
							hour = "0" + hour;
						}
						if (mins <= 9) {
							mins = "0" + mins;
						}
						if (secs <= 9) {
							secs = "0" + secs;
						}

						$scope.currentTime1 = hour + ':' + mins + ':' + secs;
						// call this function again in 1000ms
						setTimeout(updateClock, 1000);
					}

					function messageReceivedTime() {

						var now = new Date();
						var hour = now.getHours();
						var mins = now.getMinutes();
						var secs = now.getSeconds();
						if (hour <= 9) {
							hour = "0" + hour;
						}
						if (mins <= 9) {
							mins = "0" + mins;
						}
						if (secs <= 9) {
							secs = "0" + secs;
						}

						$scope.messageReceivedTime = hour + ':' + mins + ':'
								+ secs;

					}

					// updateClock(); // initial call

					// connect();
					
					 $scope.getInventoryQuantityDetails  = function() {
						 $scope.skuTransaction = false;
						 $scope.skuTransactionView = false;
						 
	                        $http({
	                            method : 'GET',
	                            url : './getSKUAvailableQuantity/'+$scope.selectedTagId 
	                        })
	                                .then(
	                                        function success(response) {
	                                            $rootScope
	                                            .hideloading('#loadingBar');
	                                            fnMoveScrollBar();
	                                            
	                                            if (response != null
	                                                    && response.data != null
	                                                    && response.data != "BAD_REQUEST") {
	                                                if (response.data.status == 'success') {
	                                                    var data = response.data.data;
	                                                    
	                                                    $scope.upcCode = data.skuDetails.upcCode;
	                                                    $scope.skuNo = data.skuDetails.skuNo;
	                                                    $scope.batchNo = data.skuDetails.batchNo;
	                                                    $scope.skuDescription =  data.skuAllocation.description;
	                                                    $scope.skuTotalQuantity = data.skuAllocation.quantity;
	                                                    $scope.skuAvailableQuantity = data.skuAllocation.availableQuantity; 
	                                                    $scope.skuTransaction = data.skuAllocation.transaction;
	                                                    $scope.skuExpiryDate = data.skuAllocation.expiryDate;
	                                                    
	                                                    if($scope.skuTransaction && $scope.skuAvailableQuantity>0)
                                                    	{
	                                                    	$scope.skuTransactionView = true;
                                                    	}else
                                                    		$scope.skuTransaction = false;
	                                                    
	                                                   if($scope.skuExpiryDate!=undefined && $scope.skuExpiryDate!=null && $scope.skuExpiryDate!='')
	                                                   {
	                                                    	if (moment($scope.skuExpiryDate, 'YYYY-MMM-DD') < moment(moment(new Date(), 'YYYY-MMM-DD').format('YYYY-MMM-DD'), 'YYYY-MMM-DD')){ 
		                         	 							$scope.showDoNotUse = false;
		                         							}else
		                         	 							$scope.showDoNotUse = true;
		                                                     
	                                                   }else
	                         	 							$scope.showDoNotUse = true;
	                                                    	
	                                                    
	                                                }else {
	                                                    $rootScope.alertErrorDialog(response.data.message);   
	                                                }
	                                            }
	                                        },
	                                        function error(response) {
	                                            $rootScope
	                                                    .hideloading('#loadingBar');
	                                            $rootScope.fnHttpError(response);
	                                        });
	                    }
					 
					 $scope.historicalData = '';
					 $scope.historicalAssetData = [];
					 
					 $scope.getHistoricalData  = function() {
						 $rootScope.showloading('#loadingBar');
						 
	                        $http({
	                            method : 'POST',
	                            url : './historyTagPositions',
	                            data:{
	                                "date": moment($scope.historySelectedDate).format('YYYY-MMM-DD') 
	                            }
	                        })
	                                .then(
	                                        function success(response) {
	                                            $rootScope
	                                            .hideloading('#loadingBar');
	                                            if (response != null
	                                                    && response.data != null
	                                                    && response.data != "BAD_REQUEST") {
	                                                if (response.data.status == 'success') { 
	                                                	$scope.historicalData = response.data.data; 
	                                                	$scope.getHistoricalDataByTime();
	                                                     
	                                                }else {
	                                                    $rootScope.alertErrorDialog(response.data.message);   
	                                                }
	                                            }
	                                        },
	                                        function error(response) {
	                                            $rootScope
	                                                    .hideloading('#loadingBar');
	                                            $rootScope.fnHttpError(response);
	                                        });
	                    }
					 
					 
					 $scope.getHistoricalDataByTime = function()
					 {

						 $scope.historicalAssetData = [];
						 if($scope.historicalData!=undefined && $scope.historicalData!=null && $scope.historicalData!=''){
							 
							 var viewTimeData = $scope.historicalData[$scope.viewTime];
							 if(viewTimeData!=undefined && viewTimeData!=null && viewTimeData!=''){
								 
								 var keyNames =  Object.keys(viewTimeData);  
								  for (var i = 0; i < keyNames.length; i++) {
									 var tagData =  viewTimeData[keyNames[i]];
									 
									 var zName =  $scope.zoneNameList[tagData['zoneId']];
									 if(zName==undefined || zName==null)
										 zName = '';
									 
									 $scope.historicalAssetData.push({
										 'x':tagData['x'],
										 'y':tagData['y'],
										 'tagId': $scope.tagNameList[keyNames[i]],
										 'zoneName': zName,
										 'type': tagData['type'],
										 'subType':tagData['subType'],
										 'assignmentId':tagData['assignmentId'],
										 'macId':keyNames[i],
										 'lastSeen':$scope.historySelectedDate+' '+$scope.viewTime
									 });
								  } 
									
							 }else
							 toastr.error('Tag data not found for selected time '+$scope.viewTime, "EI4.0");
						 }else
						{
							 toastr.error('Tag data not found for selected date '+$scope.historySelectedDate, "EI4.0");
						}
						 
						 $scope.applyFilters();
					 }
					 
					 
	                    $scope.updateInventoryQuantityDetails  = function() {
	                        $http({
	                            method : 'POST',
	                            url : './updateSKUAssetAvailableQuantity',
	                            data:{
	                                "tagId":$scope.name,
	                                "upcCode":$scope.upcCode,
	                                "quantity":$scope.deductQuantity
	                            }
	                        })
	                                .then(
	                                        function success(response) {
	                                            $rootScope
	                                            .hideloading('#loadingBar');
	                                            if (response != null
	                                                    && response.data != null
	                                                    && response.data != "BAD_REQUEST") {
	                                                if (response.data.status == 'success') {
	                                                	$scope.skuTransaction = false;
	                                                    $scope.skuAvailableQuantity = $scope.deductQuantity;
	                                                    $('#idInventoryModal').modal('hide');
	                                                    toastr.success('Quantity updated successfully', "EI4.0");
	                                                }else {
	                                                    $rootScope.alertErrorDialog(response.data.message);   
	                                                }
	                                            }
	                                        },
	                                        function error(response) {
	                                            $rootScope
	                                                    .hideloading('#loadingBar');
	                                            $rootScope.fnHttpError(response);
	                                        });
	                    }
	                    
	                    $scope.inventoryUpdateBtnClik = function() {
	                        
	                        $scope.deductQuantity = $('#deductQuantity').val();
	                        
	                        if($scope.deductQuantity<=$scope.skuAvailableQuantity)
	                        {
	                            $scope.updateInventoryQuantityDetails();
	                                                    
	                        }else{
	                            $rootScope.alertErrorDialog('Deduct Quantity value is greater than Available Quantity');  

	                        }
	                    };
	                    
	                    $scope.inventoryCancelBtnClik = function() {
                        	$scope.skuTransaction = false;
                        	$scope.skuTransactionView = true;
                            $('#idInventoryModal').modal('hide');
	                    	
	                    }
	                    
	                    $scope.showTransactionView = function() { 
                        	$scope.skuTransactionView = false;
	                    }
	                    
	                    
						
						
	                    $('#minAmt').on('input', function(e) {
	                            if (/^(\d+(\.\d{0,2})?)?$/.test($(this).val())) {
	                                // Input is OK. Remember this value
	                                $(this).data('prevValue', $(this).val());
	                            } else {
	                                // Input is not OK. Restore previous value
	                                $(this).val($(this).data('prevValue') || '');
	                            }
	                        }).trigger('input'); // Initialise the
													// `prevValue` data
													// properties

				});
