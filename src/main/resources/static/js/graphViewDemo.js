app.controller('graphViewDemoController',
		function($scope, $http, $rootScope, $window, $timeout,$location) {

	$scope.user = JSON.parse($window.sessionStorage
			.getItem("user"));
	if ($scope.user == null) {
		$location.path('/login');
		return;
	}
	$('#map').css('height',($(window).height()-135)); 
	
	

	if ($scope.user.capabilityDetails != undefined
			&& $scope.user.capabilityDetails != null) {
		if ($scope.user.capabilityDetails.DTAN == 1) {
			$scope.showTimeAnalysisBtn = true;
		} else {
			$scope.showTimeAnalysisBtn = false;
		}
		if ($scope.user.capabilityDetails.DPAR == 1) {
			$scope.showPlantArrivalRateBtn = true;
		} else {
			$scope.showPlantArrivalRateBtn = false;
		}
		if ($scope.user.capabilityDetails.DPFA == 1) {
			$scope.showPlantFloorBtn = true;
		} else {
			$scope.showPlantFloorBtn = false;
		}
		if ($scope.user.capabilityDetails.DWOA == 1) {
			$scope.showWorkOrderFloorBtn = true;
		} else {
			$scope.showWorkOrderFloorBtn = false;
		}
		if ($scope.user.capabilityDetails.DZFA == 1) {
			$scope.showZoneFlowBtn = true;
		} else {
			$scope.showZoneFlowBtn = false;
		}
		
	}
	$scope.viewValue="Column Chart"; 
	$scope.varComboTypeofdaysAging = "30"
	$('#idPFAStartDatePicker').datetimepicker({
		format : 'LL',					
		maxDate : new Date()
	});
		
	$scope.org=$window.sessionStorage.getItem("org"); 	 
	/*if ($rootScope.checkNetConnection() == false) { 
			$rootScope.hideloading('#loadingBar');						
			return;
					 } 	*/
	$scope.org = 'pump';	
	$scope.orgoperationalendtime="11:45 PM"
	$scope.orgoperationalstarttime="12:45 AM"
	$scope.alternateAtt ="./demo/Attendance1";
	$scope.isproductanalysisClicked = true;
	$scope.isEmployeedwelltimeClicked = false;
	$scope.isZoneoccupancyClicked = false;
	$scope.isZonetrafficClicked = false;
	$scope.isZonearrivalrateClicked = false;
	$scope.isZoneorgattendanceClicked = false; 
	$scope.isMaterialTraceClicked = false;
    $scope.isMaterialTrace=false;
	$scope.selectdate=false; 
	$scope.typeofday=true;
	$scope.dateValueDwell = "1";
	$scope.typeofdays=$scope.dateValueDwell;
	$scope.dateValueOccupancy="1";	
	$scope.dateValueArrival="1";
	$scope.dropdowntype="Column Chart";
	$scope.viewValueDwell="Column Chart";
	$scope.viewValueOccupancy="Column";
	$scope.viewValueTraffic="Total Traffic";
	$scope.viewValueAttendance="Organization Attendance";
	$scope.totalInventry = "";
	$scope.zoneList = [];
	$scope.zoneClassificationList = [];
	$scope.exportArrayData = [];
	$scope.dateValueTraffic=moment(new Date()).format("MMM DD, YYYY");
	$scope.dateValueAttendance=moment(new Date()).format("MMM DD, YYYY");
	// $('#selectdate').val(moment(new Date()).format("MMM DD, YYYY"));
	$scope.zoneKeyOccupancy="";
	$scope.zoneKeyArrival="";
	// $scope.nodate=true;
	$('#chart-area').empty();
	
	$scope.dwellzoneData=[];
	$scope.dwellcolumnzoneData=[];
	$scope.occupancyzoneData=[];
	$scope.trafficzoneData=[];
	$scope.arrivalzoneData=[];
	$scope.attendancezoneData=[];
	$scope.ctTextContent = [];
	$scope.dataListTimeanalysis=[];
	$scope.timeanalysisdummyList=[];	
	 $scope.typesArray = []; 
	 $scope.subTypesArray = [];
	 $scope.type = "People";
	 $scope.subtype = "Employee";	 
	 $("#colorSubtypeSelectDiv").show();
	  
	 $scope.typesArray.push({"type":"People"});
	 $scope.typesArray.push({"type":"Assets"});
	 $scope.typesArray.push({"type":"WIP"}); 

 	$scope.subTypesArray.push({"type":"Employee"});
 	$scope.subTypesArray.push({"type":"Visitor"}); 	
	
	
	
	$scope.filterTypeChange=function(){ 
		$scope.subTypesArray = [];
	    $scope.type = $("#colortypeSelect option:selected").text(); 
	  
		 
	   /* if($scope.type=="All")
    	{   
	    	 $("#colorSubtypeSelectDiv").hide();
	    	 $scope.subtype = "All";
			 $scope.empList = [];
	  
	    	 $scope.empList = $scope.employees.concat($scope.visitors).concat($scope.tools)
				.concat($scope.gages).concat($scope.fixtures).concat($scope.parts)
				.concat($scope.materials).concat($scope.rmas)						
    	}
	    else */if($scope.type=="People")
    	{   $scope.subtype = "Employee";
	    	$("#colorSubtypeSelectDiv").show(); 
	    	$scope.subTypesArray.push({"type":"Employee"});
	    	$scope.subTypesArray.push({"type":"Visitor"}); 	
	    	if( $scope.visitors!=null ) {
				$scope.empList = $scope.employees
			}	
	    	
    	}else if($scope.type=="Assets")
		{ 
    		$scope.subtype = "Gages";
    		$("#colorSubtypeSelectDiv").show(); 
    		$scope.subTypesArray.push({"type":"Gages"});
    		$scope.subTypesArray.push({"type":"Tools"}); 
    		$scope.subTypesArray.push({"type":"Fixtures"});    		
	    	if( $scope.visitors!=null ) {
				$scope.empList = $scope.gages
			}			
		}else if($scope.type=="WIP"){ 
			$scope.subtype = "Parts";
			$("#colorSubtypeSelectDiv").show(); 
			$scope.subTypesArray.push({"type":"Parts"});
			$scope.subTypesArray.push({"type":"Materials"}); 
			$scope.subTypesArray.push({"type":"RMAs"});
			if( $scope.visitors!=null ) {
				$scope.empList = $scope.parts
			}			
		}
	    $scope.empList.sort(GetSortOrder("uId"));
		$scope.emp=$scope.empList[0].uId;
		$scope.previousEmployee= $scope.emp	
}

$scope.filterSubTypeChange=function(){ 
    $scope.subtype = $("#colorSubtypeSelect option:selected").text(); 
   
    if($scope.subtype=="Employee")
	{     	$scope.empList = $scope.employees	
	}else if($scope.subtype=="Visitor")
	{  		$scope.empList = $scope.visitors	
	}else if($scope.subtype=="Gages")
	{  		$scope.empList = $scope.gages
	}else if($scope.subtype=="Tools")
	{  		$scope.empList = $scope.tools	
	}else if($scope.subtype=="Fixtures")
	{   	$scope.empList = $scope.fixtures	
	}else if($scope.subtype=="Parts")
	{ 		$scope.empList = $scope.parts
	}else if($scope.subtype=="Materials")
	{ 		$scope.empList = $scope.materials	
	}else if($scope.subtype=="RMAs")
	{ 		$scope.empList = $scope.rmas
	}
    $scope.empList.sort(GetSortOrder("uId"));
	$scope.emp=$scope.empList[0].uId;
	$scope.previousEmployee= $scope.emp	
}
	$scope.displayTimeAnalysisScreen = function() { 
		$("#idTimeAnalysis").addClass("active");
		$("#idPlantArrivalRate").removeClass("active");
		$("#idPlantFloor").removeClass("active");
		$("#idWorkOrderFloor").removeClass("active");
		$("#idZoneFlow").removeClass("active"); 
		
		$('#chart-area').empty();
		$scope.dataList=[];			
		$scope.dataList=$scope.empdwelldummyList;
		$scope.dataListTimeanalysis=$scope.timeanalysisdummyList;	 	
		$scope.viewValue=$scope.viewValueDwell; 
		$scope.emp=$scope.previousEmployee
		$scope.isEmployeedwelltimeClicked = true;
		$scope.isZoneoccupancyClicked = false;
		$scope.isZonetrafficClicked = false;
		$scope.isZonearrivalrateClicked = false;
		$scope.isZoneorgattendanceClicked = false;
		$scope.isMaterialTraceClicked = false;
		$scope.isproductanalysisClicked = false;
        $scope.isMaterialTrace=false;			
		$scope.typeofday=true;		
		$scope.typeofdays=$scope.dateValueDwell;
		$('#startdate').val($scope.DwellStartDate);
		$('#enddate').val($scope.DwellEndDate);
		$scope.selectdate=false; 
		$scope.customdates=false;
		
		//if($scope.timeanalysisdummyList==undefined || $scope.timeanalysisdummyList==null|| $scope.timeanalysisdummyList.length==0)
		  $scope.empList = [];
          $scope.empList = $scope.employees;        
			$scope.getZoneDetails();
       // else
        //	$scope.viewOption();	
	}	
	$scope.displayPlantArrivalRateScreen = function() {	
		$("#idTimeAnalysis").removeClass("active");
		$("#idPlantArrivalRate").addClass("active");
		$("#idPlantFloor").removeClass("active");
		$("#idWorkOrderFloor").removeClass("active");
		$("#idZoneFlow").removeClass("active"); 
		
		$scope.dataList=[];
		$('#chart-area').empty();
		$scope.dataList=$scope.arrivaldummyList;		
		$scope.viewValue="Line Chart"; 
		$scope.isEmployeedwelltimeClicked = false;
		$scope.isZoneoccupancyClicked = false;
		$scope.isZonetrafficClicked = false;
		$scope.isZonearrivalrateClicked = true;
		$scope.isZoneorgattendanceClicked = false;	
		$scope.isMaterialTraceClicked = false;
		$scope.isproductanalysisClicked = false;
        $scope.isMaterialTrace=false;	
		$scope.typeofday=true;		
		$scope.typeofdays=$scope.dateValueArrival;
		$('#startdate').val($scope.ArrivalStartDate);
		$('#enddate').val($scope.ArrivalEndDate);
		$scope.selectdate=false; 
		$scope.customdates=false; 
		if($scope.zoneKeyArrival==""){
			$scope.zone=$scope.zoneList[0].name;
			}
			else{
			$scope.zone=$scope.zoneKeyArrival;
			}		
		
		if($scope.arrivaldummyList==undefined || $scope.arrivaldummyList==null|| $scope.arrivaldummyList.length==0)
			$scope.getZoneDetails();
        else
        	$scope.viewOption();
	}	

	$scope.displayPlantFloorScreen = function() {
		$("#idTimeAnalysis").removeClass("active");
		$("#idPlantArrivalRate").removeClass("active");
		$("#idPlantFloor").addClass("active");
		$("#idWorkOrderFloor").removeClass("active");
		$("#idZoneFlow").removeClass("active"); 
		
		$scope.dataList=[];		
		$scope.dataList=$scope.attendancedummyList;	
		$('#chart-area').empty();
		$scope.viewValue=$scope.viewValueAttendance; 		
		$scope.isEmployeedwelltimeClicked = false;
		$scope.isZoneoccupancyClicked = false;
		$scope.isZonetrafficClicked = false;
		$scope.isZonearrivalrateClicked = false;
		$scope.isZoneorgattendanceClicked = true;
		$scope.isMaterialTraceClicked = false;
		$scope.isproductanalysisClicked = false;
        $scope.isMaterialTrace=false;
		$scope.selectdate=true;  		
		$scope.varPFAStartTime = moment(new Date()).format('MMM DD, YYYY');
		$("#idPFAEditStartDatePicker").val(moment(new Date()).format('YYYY-MMM-DD'));
		$scope.customdates=false; 
		$scope.typeofday=false;
		if($scope.attendancedummyList==undefined || $scope.attendancedummyList==null|| $scope.attendancedummyList.length==0)
			$scope.getZoneDetails();
        else
        	$scope.viewOption();
	}
	$scope.displayWorkOrderFloorScreen = function() { 
		$("#idTimeAnalysis").removeClass("active");
		$("#idPlantArrivalRate").removeClass("active");
		$("#idPlantFloor").removeClass("active");
		$("#idWorkOrderFloor").addClass("active");
		$("#idZoneFlow").removeClass("active"); 
		
		$('#chart-area').empty();	
		$scope.dataList=[];			
		$scope.isEmployeedwelltimeClicked = false;
		$scope.isZoneoccupancyClicked = false;
		$scope.isZonetrafficClicked = false;
		$scope.isZonearrivalrateClicked = false;
		$scope.isZoneorgattendanceClicked = false;
		$scope.isproductanalysisClicked = false;
		$scope.isMaterialTraceClicked = true;
		$scope.isMaterialTrace=true;
		$scope.typeofday=true;		
		$scope.selectdate=false; 
		$scope.customdates=false;
		$scope.chartViewShow=false;
		$scope.viewValue="Line Chart"; 
		$scope.getZoneDetails();		
	}
	
	
	$scope.productanAlysisDays = "1";
    $scope.productanAlysisMapType = "Column Chart"; 
    $scope.productanAlysisZone = "All";
    $scope.productanAlysisSortBy = "0";
   
	$scope.displayZoneFlowScreen = function() {
		$("#idTimeAnalysis").removeClass("active");
		$("#idPlantArrivalRate").removeClass("active");
		$("#idPlantFloor").removeClass("active");
		$("#idWorkOrderFloor").removeClass("active");
		$("#idZoneFlow").addClass("active"); 
        
        $scope.dataList=[]; 
        $scope.zone = 'All'; 
		$scope.viewValue = $scope.productanAlysisMapType;  
        $scope.selectedZone = $scope.productanAlysisZone; 
        $scope.typeofdays =  $scope.productanAlysisDays ;
        $scope.sortBy = $scope.productanAlysisSortBy;
        $scope.agingsortBy = "2";
        	
        $scope.dataList = $scope.productAnalysisList;
        $scope.isEmployeedwelltimeClicked = false;
        $scope.isZoneoccupancyClicked = false;
        $scope.isZonetrafficClicked = false;
        $scope.isZonearrivalrateClicked = false;
        $scope.isZoneorgattendanceClicked = false;
        $scope.isproductanalysisClicked = true;
		$scope.isMaterialTraceClicked = false;
		$scope.isMaterialTrace=false;
        $scope.typeofday=true;         
        $('#startdate').val($scope.ProductStartDate);
        $('#enddate').val($scope.ProductEndDate);
        $scope.selectdate=false; 
        $scope.customdates=false;        
        
        $scope.analysisZoneList = [];
        $scope.analysisZoneList.push({'name':"All"});
        for (var n = 0; n < $scope.zoneList.length; n++) {
        	if($scope.zoneList[n].name!='Corporate Office')
        		$scope.analysisZoneList.push({'name':$scope.zoneList[n].name});
        }
        /*
		 * if($scope.zoneKeyProduct==""){ $scope.selectedZone =
		 * $scope.analysisZoneList[0].name; } else{ $scope.selectedZone =
		 * $scope.zoneKeyProduct; }
		 */
        
      // if($scope.productAnalysisList==undefined || $scope.productAnalysisList ==null || $scope.productAnalysisList.length==0)
        //$timeout(function() {
        	$scope.getZoneDetails();
        //	 }, 500);
       // else
        	//$scope.viewOption();
    }
	
	$('#startdate')
	.datetimepicker({
		format : 'L',
		maxDate : new Date()
	})
	.on(
			'dp.change',
			function(e) {				
				$scope.invalid = '';
				$scope.s_date = $("#startdate").val();
				$scope.e_date = $("#enddate").val();
				if ($scope.s_date != ''
						&& $scope.e_date != '') {
					var sDate = new Date($scope.s_date);
					var eDate = new Date($scope.e_date);
					if (sDate > eDate) {
						
						 $.alert({
	                            title: $rootScope.MSGBOX_TITLE,
	                            content: 'Start date should be less than or equal to end date',
	                            icon: 'fa fa-info-circle',
	                             });						
						
					}
				} 
			});

$('#enddate')
	.datetimepicker({
		format : 'L',
		maxDate : new Date()
	})
	.on(
			'dp.change',
			function(e) {				
				$scope.invalid = '';
				$scope.s_date = $("#startdate").val();
				$scope.e_date = $("#enddate").val();
				$("#endDateErrorMessage").show().fadeOut("slow");
				if ($scope.s_date != ''
						&& $scope.e_date != '') {
					var sDate = new Date($scope.s_date);
					var eDate = new Date($scope.e_date);
					if (sDate > eDate) {						
						 $.alert({
	                            title: $rootScope.MSGBOX_TITLE,
	                            content: 'End date should be greater than or equal to start date',
	                            icon: 'fa fa-info-circle',
	                             });						
					} 
				} 
			}); 


	$("#dwelltime").keypress(
			function(e) {				
				// if the letter is not digit then display error and don't type
				// anything
				
				if (e.which != 8 && e.which != 0
						&& (e.which < 48 || e.which > 57)) {
					// display error message
					// $("#dwelltimeErrorMessage").html("Digits
					// Only").show().fadeOut("slow");
					// $window.alert("Digits Only");
					return false;
				}
			});	
	
	
	$scope.headingTitle = "Zone Analytics";
	$scope.siteId;
	$scope.mapId;
	$scope.orgDetails;
	$scope.region;
	$rootScope.hideloading('#loadingBar');

	var screenHeight = $window.innerHeight;
   	var screenWidth = $window.innerWidth;

	$scope.mapDetails = {};
	//$scope.zoneClassificationList = [];
	$scope.classifications = [];
	$scope.isDrawEvent = false;
	$scope.imageOverlay;
	
	$scope.loaddwelHeatMap=function(){		
		
		 function convertTimestamp(timestamp, convertint = 1) {
				var fromdate;
				if (convertint === 1)
					fromdate = parseInt(timestamp, 10);
				else fromdate = timestamp;

				var d = new Date(0);				
				d.setUTCSeconds(fromdate); // Convert the passed timestamp to
											// milliseconds
				var now_utc = new Date(d.toUTCString().slice(0, -4));
				var mnth = ("0" + (now_utc.getMonth() + 1)).slice(-2);
				var day = ("0" + now_utc.getDate()).slice(-2);
				return [now_utc.getFullYear(), mnth, day].join("-");
				
			}
		
		$('#chart-area').empty();

		var chartdat=[];
		$scope.dwellzoneData={};
		// $scope.zoneList.sort(GetSortOrder("name"));
		for (var n = 0; n < $scope.zoneList.length; n++) {
			var cDate = convertTimestamp($scope.zoneList[n].created_time);
			var mDate = convertTimestamp($scope.zoneList[n].modified_time);
			var cStatus = $scope.zoneList[n].zoneStatus;
			
			if (cStatus == "Active") {				
				if (new Date(cDate).toISOString().slice(0, 10) <= new Date($scope.enddate).toISOString().slice(0, 10)) {
				
					$scope.dwellzoneData[$scope.zoneList[n].id] =  {
							'zoneID': $scope.zoneList[n].id,
							'zoneName':$scope.zoneList[n].name, 
							'dWellTime':0, 
							'colorValue':0, 
					};
					
				}

			}
			else if (cStatus == "Inactive") {
				if ((new Date($scope.startdate).toISOString().slice(0, 10) <= new Date(cDate).toISOString().slice(0, 10)) && 
						(new Date($scope.enddate).toISOString().slice(0, 10) <= new Date(cDate).toISOString().slice(0, 10))) {			
										

					if (new Date(cDate).toISOString().slice(0, 10) == new Date($scope.enddate).toISOString().slice(0, 10)) {
						
						$scope.dwellzoneData[$scope.zoneList[n].id] =  {
								'zoneID': $scope.zoneList[n].id,
								'zoneName':$scope.zoneList[n].name, 
								'dWellTime':0, 
								'colorValue':0, 
						};
					
					}
				} else {
				
					if (new Date(mDate).toISOString().slice(0, 10) >= new Date($scope.startdate).toISOString().slice(0, 10)) {						
						$scope.dwellzoneData[$scope.zoneList[n].id] =  {
								'zoneID': $scope.zoneList[n].id,
								'zoneName':$scope.zoneList[n].name, 
								'dWellTime':0, 
								'colorValue':0, 
						};
						
					}

				}			
		}
		}
		
		if (Object.keys($scope.dwellzoneData).length == 0){			
			$scope.norecords=true;
			$scope.no_data_text = "No Record found";
			$('#chart-area').empty();
			$scope.exportXLS = false;
			$scope.exportPDF = false;
		
		}
	else
		{	
		$scope.norecords=false;
		$scope.dataList.sort(GetSortOrder("zoneName")); 
		for (var i = 0; i < $scope.dataList.length; i++) {
			var dateObj = $scope.dataList[i];
			if (dateObj != undefined && dateObj != null) {
				if($scope.dwellzoneData.hasOwnProperty(dateObj.zoneID))
				{
					var listObj = $scope.dwellzoneData[dateObj.zoneID];

					listObj.zoneID=dateObj.zoneID; 
					listObj.zoneName=dateObj.zoneName;
					/*
					 * Display in minutes like as SAP Dashboard
					 * if(dateObj.dWellTime/60>1000){ var
					 * num=dateObj.dWellTime/60/1000;
					 * listObj.dWellTime=num.toFixed(2)+"K"; } else rsk
					 * 21/Nov/20
					 */	
						listObj.dWellTime=(dateObj.dWellTime/60).toFixed(2); 
					listObj.colorValue=dateObj.dWellTime/60; 
					$scope.dwellzoneData[dateObj.zoneID]=listObj;
					
				}  
			}
		}
		
		$scope.loaddwelHeatMapDisplay();
		}
	}
		$scope.loaddwelHeatMapDisplay=function(){			
			$scope.exportArrayData=[];
			
				if(Object.keys($scope.empdwelldummyList).length==0){
					
					$scope.viewValue=$scope.viewValueDwell; 
					$scope.isEmployeedwelltimeClicked = true;
					$scope.isZoneoccupancyClicked = false;
					$scope.isZonetrafficClicked = false;
					$scope.isZonearrivalrateClicked = false;
					$scope.isZoneorgattendanceClicked = false;						
					$scope.typeofday=true;		
					$scope.typeofdays=$scope.dateValueDwell;
					$('#startdate').val($scope.DwellStartDate);
					$('#enddate').val($scope.DwellEndDate);
					$scope.selectdate=false; 
					if($scope.typeofdays==0){
						$scope.customdates=true;  				
								$('#startdate').val($scope.DwellStartDate);	
								$('#enddate').val($scope.DwellEndDate);
					}else
					{$scope.customdates=false;}
					
					$scope.norecords=true;
					$scope.exportXLS = false;
					$scope.exportPDF = false;
					$scope.no_data_text = "No Record found";
					$('#chart-area').empty();
					
				}
			else{
				
				var chartdat=[];
				$('#chart-area').empty();
				$scope.norecords=false;
				$scope.generateDateTime();
			for ( var key in $scope.dwellzoneData) {
				if ($scope.dwellzoneData[key].colorValue == 0){ // rsk 21/Nov/20
																// avoid black
													// color
		
					chartdat.push( {
						'label': $scope.dwellzoneData[key].zoneName +"\n"+ Math.round($scope.dwellzoneData[key].dWellTime),				
						'value': 1,
						'colorValue':  1
					});
					$scope.exportArrayData.push( {
						'Zone_Name': $scope.dwellzoneData[key].zoneName  ,				
						'Dwell_Time': Math.round($scope.dwellzoneData[key].dWellTime),
						
					});
				}
				else {
				chartdat.push( {
					'label': $scope.dwellzoneData[key].zoneName +"\n"+ Math.round($scope.dwellzoneData[key].dWellTime),				
					'value': 1,
					'colorValue':  $scope.dwellzoneData[key].colorValue
				});
				$scope.exportArrayData.push( {
					'Zone_Name': $scope.dwellzoneData[key].zoneName  ,				
					'Dwell_Time': Math.round($scope.dwellzoneData[key].dWellTime),
					
				});
				}
			}
			$scope.exportXLS = true;
			$scope.exportPDF = true;
			// to export chart data
            $scope.filename = "EI4.0"    + $scope.previousEmployee + "_"     + "Time ";
            $scope.sheetname = $scope.previousEmployee + "_"     +"Time";
			$scope.headerArray=[];
		
			
			$scope.generateDateTime();
			
			if(moment($scope.DwellStartDate).format("MMM DD, YYYY")==moment($scope.DwellEndDate).format("MMM DD, YYYY"))
				 $scope.dwelltitledate=moment($scope.DwellStartDate).format("MMM DD, YYYY");
				else
					$scope.dwelltitledate=moment($scope.DwellStartDate).format("MMM DD, YYYY")+" - "+moment($scope.DwellEndDate).format("MMM DD, YYYY");
			
		 
			
	        if($scope.typeofdays=="1")
	            $scope.exportDetailTitle='Zone Analysis for '+$scope.emp+" on "+$scope.dwelltitledate;
	        else
	            $scope.exportDetailTitle='Zone Analysis for '+$scope.emp+" from "+$scope.dwelltitledate;
	        
	            $scope.pdfHeader="EI4.0 Analytics - User Zone Analysis Chart";
	            
				$scope.pdfDate=$scope.dwelltitledate;
				
			var container = document.getElementById('chart-area');
			var data = {
					series :chartdat
			};
			var options = {
					chart: {
						width: screenWidth * 0.80,
						height: screenHeight * 0.80,
						format: '1,000',
						title: {
						text: $scope.exportDetailTitle,
						align: "center"							
						}
												
					},
					series: {
						showLabel: true,
						useColorValue: true,
						zoomable: false,
						useLeafLabel: true
					},
					tooltip: {
						// suffix: '?'
					},
					legend: {
						align: 'right'
					},
					chartExportMenu: {
				        visible: false  // default is true.
				    }

			};
			var theme = {
					series: { 
						 startColor: '#faee84',
						endColor: '#8f0a13',
						 /*
							 * startColor: '#ffefef', endColor: '#ac4142',
							 */
						// colors:[ "#778899", "#66ff66", "#ffcc00", "#ff6600",
						// "#990000"],
						overColor: '#75b5aa',
						borderColor: '#FFFFFF'
					}
			};

			// For apply theme

			tui.chart.registerTheme('myTheme', theme);
			options.theme = 'myTheme';

			tui.chart.treemapChart(container, data, options);
			
		}				
		}
	$scope.loadzoneTraffic=function(){		
		
		var titile="";
		$('#chart-area').empty();
		$scope.norecords=false;
		var chartdat=[];
		var zoneData={};
		var color=[];
		
		// $scope.zoneList.sort(GetSortOrder("name"));
		for (var n = 0; n < $scope.zoneList.length; n++) {
			var zoneKey = $scope.zoneList[n].id;			
			$scope.getZoneFinalList(zoneKey, $scope.activestartdateTraffic,  $scope.activeenddateTraffic);				
			if ($scope.FINAL_DAY_ARRAY.includes($scope.startdate)) {
			
			var colorcode="#69696e";
			
			if($scope.zoneList[n].classification!=null && $scope.zoneList[n].classification!=undefined && $scope.zoneList[n].classification!='')
				for (var k = 0; k < $scope.zoneClassificationList.length; k++){
					if ($scope.zoneList[n].classification == $scope.zoneClassificationList[k].classification)
						colorcode = $scope.zoneClassificationList[k].colorCode
				}
			
			zoneData[$scope.zoneList[n].id] =  {
					'zoneID': $scope.zoneList[n].id,
					'zoneName':$scope.zoneList[n].name, 
					'total':0,
					'peak':0,
					'averger':0,
					'Classification':$scope.zoneList[n].classification,
					'colorValue':colorcode, 
			};			
			}
			}
		
		
		
		if (Object.keys(zoneData).length == 0){
			$scope.exportXLS=false;
			$scope.exportPDF=false;
				$scope.norecords=true;
				$scope.no_data_text = "No Record found";
				$('#chart-area').empty();
			
			}
		else
			{
			$scope.exportXLS=true;
			$scope.exportPDF=true;
			
		$scope.dataList.sort(GetSortOrder("zoneName")); 
		var peakminVal = "";
		var avgminVal = "";
		for (var i = 0; i < $scope.dataList.length; i++) {
			var dateObj = $scope.dataList[i];
			if (dateObj != undefined && dateObj != null) {
				var zoneObjList = dateObj.zone;
				peakminVal = 999999;
				avgminVal = 99999;
				if (zoneObjList != undefined && zoneObjList != null && zoneObjList.length>0) {
					zoneObjList.sort(GetSortOrder("zoneName"));

					for (var j = 0; j < zoneObjList.length; j++) {
						if(zoneData.hasOwnProperty(zoneObjList[j].zoneID))
						{	 if (zoneObjList[j].dWellTime.maximum < peakminVal) {
							peakminVal = zoneObjList[j].dWellTime.maximum ? zoneObjList[j].dWellTime.maximum : peakminVal;	}	
						{	 if (Math.ceil(zoneObjList[j].dWellTime.average) < avgminVal) {
							avgminVal = Math.ceil(zoneObjList[j].dWellTime.average) ? Math.ceil(zoneObjList[j].dWellTime.average) : avgminVal;	}	
							var listObj = zoneData[zoneObjList[j].zoneID];
							listObj.total=zoneObjList[j].dWellTime.total; 
							listObj.peak=zoneObjList[j].dWellTime.maximum;							
							listObj.averger=Math.ceil(zoneObjList[j].dWellTime.average);
							if(zoneData[dateObj.zoneID]!=undefined){
							zoneData[dateObj.zoneID]=listObj;
							
							}

						}  
					}}

			}
		}
		
		$scope.getClassificationsFinalList();
		$scope.exportArrayData=[];
		
		for ( var key in zoneData) {
			$scope.generateDateTime();
			
			 if($scope.viewValue=="Total Traffic"){
					$scope.viewValueTraffic=$scope.viewValue
					$scope.exportDetailTitle="";
					$scope.pdfHeader="";
					$scope.pdfDate="";
					
						$scope.exportDetailTitle="Total Traffic @ All Zone on "+$('#selectdate').val();
						$scope.pdfHeader="Zone Analytics - Zone Total Traffic Chart";
						$scope.pdfDate=$('#selectdate').val();
						
					$scope.chartTitleTraffic="Total Traffic @ All Zone on "+$('#selectdate').val();
					$scope.filename="Total Traffic";
					$scope.sheetname="Total-Traffic";
				
						chartdat.push( {
							'label': zoneData[key].zoneName +"\n"+ "Total Traffic "+zoneData[key].total,
							'value': 1,
							'colorValue':  zoneData[key].total
						});
						$scope.exportArrayData.push( {
							'Zone-Name': zoneData[key].zoneName ,
							'Total Traffic': zoneData[key].total,
							
						});
					
				}else if($scope.viewValue=="Peak Traffic"){
					$scope.viewValueTraffic=$scope.viewValue
					$scope.exportDetailTitle="";
					$scope.pdfHeader="";
					$scope.pdfDate="";
					var legendFirstValue=1;
						$scope.exportDetailTitle="Peak Traffic @ All Zone on "+$('#selectdate').val();
						$scope.pdfHeader="Zone Analytics - Zone Peak Traffic Chart";
						$scope.pdfDate=$('#selectdate').val();
						
					$scope.chartTitleTraffic="Peak Traffic @ All Zone on "+$('#selectdate').val();
					$scope.filename="Peak Traffic";
					$scope.sheetname="Peak-Traffic";
					if (zoneData[key].peak == 0) { // rsk 21/Nov/20 avoid black
													// color
						chartdat.push({
							'label': zoneData[key].zoneName +"\n"+ "Peak Traffic "+zoneData[key].peak,
							'value': 1,
							'colorValue': 0
						});
						$scope.exportArrayData.push( {
							'Zone-Name': zoneData[key].zoneName ,
							'Peak Traffic': zoneData[key].peak,

						});

					}
							else {
						
						chartdat.push( {
							'label': zoneData[key].zoneName +"\n"+ "Peak Traffic "+zoneData[key].peak,
							'value': 1,
							'colorValue':  zoneData[key].peak
						});
						$scope.exportArrayData.push( {
							'Zone-Name': zoneData[key].zoneName ,
							'Peak Traffic': zoneData[key].peak,
							
						});
					}
				}else if($scope.viewValue=="Average Traffic"){
				$scope.viewValueTraffic=$scope.viewValue
				$scope.exportDetailTitle="";
				$scope.pdfHeader="";
				$scope.pdfDate="";
				var avglegendFirstValue=0;
					$scope.exportDetailTitle="Average Traffic (15 mins interval) @ All Zone on "+$('#selectdate').val();
					$scope.pdfHeader="Zone Analytics - Zone Average Traffic Chart";
					$scope.pdfDate=$('#selectdate').val();
					
				$scope.chartTitleTraffic="Average Traffic (15 mins interval) @ All Zone on "+$('#selectdate').val();
				$scope.filename="Average Traffic";
				$scope.sheetname="Average-Traffic";
				
		if (zoneData[key].averger == 0){ // rsk 21/Nov/20 avoid black
													// color
					chartdat.push( {
						'label': zoneData[key].zoneName +"\n"+ "Average Traffic "+zoneData[key].averger,
						'value': 1,
						'colorValue': 1
					});
					$scope.exportArrayData.push( {
						'Zone-Name': zoneData[key].zoneName ,
						'Average Traffic': zoneData[key].averger,						
					});
					
				}
		
				else 
				{
					
				chartdat.push( {
						'label': zoneData[key].zoneName +"\n"+ "Average Traffic "+zoneData[key].averger,
						'value': 1,
						'colorValue':  zoneData[key].averger
					});
					$scope.exportArrayData.push( {
						'Zone-Name': zoneData[key].zoneName ,
						'Average Traffic': zoneData[key].averger,						
					});
					
				}
			}	else if($scope.viewValue=="Classification Chart"){
				$('#chart-area').empty();
				$scope.norecords=false;
				$scope.viewValueTraffic=$scope.viewValue
				$scope.exportDetailTitle="";
				$scope.pdfHeader="";
				$scope.pdfDate="";
				
					$scope.exportDetailTitle="Traffic @ All Zone on "+$('#selectdate').val();
					$scope.pdfHeader="Zone Analytics - Zone Traffic Chart";
					$scope.pdfDate=$('#selectdate').val();
					
				$scope.chartTitleTraffic="Traffic @ All Zone on "+$('#selectdate').val();
				$scope.filename="Zone Traffic";
				$scope.sheetname="Zone-Traffic";
				
				var zoneclassification =zoneData[key].Classification;
				var listObj =zoneData[key];
				for (var k = 0; k < $scope.zoneClassificationFinalList.length; k++) {
					var zoneclassObj = $scope.zoneClassificationFinalList[k];
					if(zoneclassification==zoneclassObj.classification)  {
						listObj.colorValue=zoneclassObj.colorCode; 
						listObj.Classification=zoneclassObj.classification; 
						break;
						}
					else{
						listObj.colorValue="#778899",
							listObj.Classification="No Class"
					}

				}
				color.push(listObj.colorValue);			
										
				
				/*
				 * rsk 23-Nov-20 Added classification. since legend is not in
				 * tui-chart
				 */
				chartdat.push( {
					'label': zoneData[key].zoneName +"\n"+ "Total Traffic "+zoneData[key].total+"\n"+ "Peak Traffic "+ zoneData[key].peak+"\n"+ "Average Traffic "+ zoneData[key].averger 
					+"\n"+ "Classification :" + listObj.Classification,
					'value': 1 
				});
				$scope.exportArrayData.push( {
					'Zone-Name': zoneData[key].zoneName ,
					'Classification': listObj.Classification,
					'Total Traffic': zoneData[key].total,
					'Peak Traffic': zoneData[key].peak,
					'Average Traffic': zoneData[key].averger,					
				});
			}
		}

		var container = document.getElementById('chart-area');
		if($scope.viewValue=="Classification Chart"){
			var data = {
					series :chartdat
			};
			var options = {
					chart: {
						width: screenWidth * 0.80,
						height: screenHeight * 0.80,
						format: '1,000',
						title:{
							text: $scope.chartTitleTraffic, 
							align: "center"
						}
						// title: $scope.chartTitleTraffic,
						
					},
					series: {
						showLabel: true, 
						zoomable: false,
						useLeafLabel: true
					},
					tooltip: {
						// suffix: '?'
					},
					legend: {
						align: 'right'
					},
					chartExportMenu: {
				        visible: false  // default is true.
				    }
			};
			var theme = {
					series: {
						colors:color ,
						borderColor: '#ffffff'

					}
			};

			// For apply theme

			tui.chart.registerTheme('myTheme', theme);
			options.theme = 'myTheme';

			tui.chart.treemapChart(container, data, options);
		}else{
			var data = {
					series :chartdat
			};
			var options = {
					chart: {
						width: screenWidth * 0.80,
						height: screenHeight * 0.80,
						format: '1,000',
						title:{
							text: $scope.chartTitleTraffic,
							align: "center"
						}
						// title: $scope.chartTitleTraffic,
						
					},
					series: {
						showLabel: true,
						useColorValue: true,
						zoomable: false,
						useLeafLabel: true
					},
					tooltip: {
						// suffix: '?'
					},
					legend: {
						align: 'right'
					},
					chartExportMenu: {
				        visible: false  // default is true.
				    }
			};
			var theme = {
					series: {
						// startColor: '#faee84',
							// endColor: '#8f0a13',
						startColor: '#ffefef',
						 endColor: '#ac4142',
						 // colors:["#ccffff", "#66ff66", "#ffcc00",
							// "#ff6600", "#990000"],
						overColor: '#75b5aa',
						borderColor: '#FFFFFF'
					}
			};

			// For apply theme

			tui.chart.registerTheme('myTheme', theme);
			options.theme = 'myTheme';

			tui.chart.treemapChart(container, data, options);
		
		}
		}}
	}

	$scope.loadoccupancyColumnchart= function(){	
			
		
		$scope.exportArrayData=[];			
		var zoneName='';		
		$scope.norecords=false;
		$scope.exportXLS = true;
		$scope.exportPDF = true;
		$('#chart-area').empty(); 
		var chartXData = [];
		var peakDataList = [];
		var avgDataList = [];
		var maxDataList = [];
		var maxTime = 0;
		$scope.dataList.sort(GetSortOrder("date"));
		$scope.zone= $scope.previousZoneOccupancy
		$scope.zoneKeyOccupancy=$scope.zone;
		$scope.getZoneFinalList($scope.zoneKeyOccupancy,  $scope.activestartdateOccupancy, $scope.activeenddateOccupancy);
		
			$scope.finalOccopancyColumnList=[];
			for (var i = 0; i < $scope.dataList.length; i++) {
				if ($scope.FINAL_DAY_ARRAY.includes($scope.dataList[i].date)) 
					{
				var dateObj = $scope.dataList[i];
				$scope.finalOccopancyColumnList[i]=dateObj;
					}			
			}
			for (var i = 0; i < $scope.finalOccopancyColumnList.length; i++) {
				var dateObj = $scope.finalOccopancyColumnList[i];
			
			
			if (dateObj != undefined && dateObj != null) {
				var zonedataObj=dateObj.zone;
				for (var j = 0; j < zonedataObj.length; j++) {
					
					if(zonedataObj[j].zoneID==$scope.zone){
																				
						zoneName=zonedataObj[j].zoneName;
						
					
						chartXData.push(moment(dateObj.date).format("MMM DD, YYYY"));
						var utcDate =moment(dateObj.date).format("MMM DD, YYYY");	
								
						if (zonedataObj[j].dWellTime.maximum > maxTime)
							maxTime = zonedataObj[j].dWellTime.maximum;

						peakDataList.push(zonedataObj[j].dWellTime.maximum);
						avgDataList.push(zonedataObj[j].dWellTime.average);
						maxDataList.push(zonedataObj[j].zoneRestrictions);
						$scope.exportArrayData.push({					
							'Date':utcDate,
							'Max-Capacity':zonedataObj[j].zoneRestrictions, 
							'Peak-Occupancy':zonedataObj[j].dWellTime.maximum,
							'Average-Occupancy':zonedataObj[j].dWellTime.average,				
							
						});
                   }
					
				}
			}
			
		}
					
		if(maxDataList[0]>maxTime)
			maxTime=maxDataList[0];	

				if(zoneName != "")
					{
					$scope.generateDateTime();
					if(moment($scope.OccupancyStartDate).format("MMM DD, YYYY")==moment($scope.OccupancyEndDate).format("MMM DD, YYYY"))
						 $scope.Occupancytitledate=moment($scope.OccupancyStartDate).format("MMM DD, YYYY");
					else
						$scope.Occupancytitledate=moment($scope.OccupancyStartDate).format("MMM DD, YYYY")+" - "+moment($scope.OccupancyEndDate).format("MMM DD, YYYY");
					$scope.exportDetailTitle="";
					$scope.pdfHeader="";
					$scope.pdfDate="";
					
						$scope.exportDetailTitle=zoneName +' Occupancy from '+$scope.Occupancytitledate;
						$scope.pdfHeader="Zone Analytics - Occupancy Chart";
						$scope.pdfDate=$scope.Occupancytitledate;
						$scope.filename = "Zone Occupancy_"+zoneName;
						$scope.sheetname =zoneName+"_Occupancy";
					var container = document.getElementById('chart-area');
		var data = {
				categories: chartXData,
				series: {
					column: [
						{
							name: 'Peak Occupancy',
							data: peakDataList
						},
						{
							name: 'Average Occupancy',
							data: avgDataList
						}
						], 
						line: [
							{
								name: 'Target',
								data: maxDataList
							}
							]
				}

		};
		var options = {
				chart: {
					width: screenWidth * 0.80,
					height: screenHeight * 0.80,					
					title:{
						text: zoneName+" Occupancy "+$scope.Occupancytitledate ,
						align: "center"
					}
				},
				series: {
					showLabel: true,

					       line: {
					           showDot: true,
					         
					       }
					   },
				yAxis: [{
					title: 'Peak & Average Occupancy',
					min: 0,
					max: maxTime
				},
				{
					title: 'target',
					min: 0,
					max: maxTime	
				}],
				xAxis: {
					title: 'Date'
				},
				legend: {
					align: 'right'
				},
				chartExportMenu: {
			        visible: false  // default is true.
			    },
			    tooltip: {
			    	  align: 'center top',
			        grouped: true,
			        template: function(chartXData, peakDataList, avgDataList, maxDataList) {
			          
			            var head = '<div style="padding-left:10px; width: 250px; font-size: 14px; background-color: #696969; opacity: 1; color: #ffffff;">' + "Date             :" +chartXData +   '</div>';
			               var body = '<div style="padding-left:10px; width: 250px; font-size: 14px;  background-color: #696969; opacity: 1; color: #ffffff;">'+"Peak Occupancy :" + peakDataList[0].value + '</div>'+
			            '<div style="padding-left:10px; width: 250px; font-size: 14px;  background-color: #696969; opacity: 1; color: #ffffff;">'+"Average Occupancy :" + peakDataList[1].value + '</div>' +
			            '<div style="padding-left:10px; width: 250px; font-size: 14px;  background-color: #696969; opacity: 1; color: #ffffff;">'+"Maximum-Capacity:" + peakDataList[2].value + '</div>';
			            return head+body ;
			        }
			    },
				
				theme: 'newTheme'
		};
		tui.chart.registerTheme('newTheme', {
			series: {
				column: {
					colors: ['#74abe2','#e8743b']
				},
				line: {
					colors: ['#000000']
				}
			}
		});
		tui.chart.comboChart(container, data, options).hideTooltip();;
	
		
	}else{
		$scope.exportXLS = false;
		$scope.exportPDF = false;
		$scope.norecords=true;
		$scope.no_data_text = "No Record found";
		$('#chart-area').empty();
	}
					
					}	
	
		
	$scope.loadColumnchart = function() {
		
		$scope.exportArrayData=[];
		
		$('#chart-area').empty(); 
		$scope.dwellcolumnchartData = [];
		$scope.dwellcolumnzoneData = [];
		$scope.maxTime = 0;
		$scope.dataList.sort(sortAlphaNum);
			
		 function convertTimestamp(timestamp, convertint = 1) {
				var fromdate;
				if (convertint === 1)
					fromdate = parseInt(timestamp, 10);
				else fromdate = timestamp;

				var d = new Date(0);				
				d.setUTCSeconds(fromdate); // Convert the passed timestamp to
											// milliseconds
				var now_utc = new Date(d.toUTCString().slice(0, -4));
				var mnth = ("0" + (now_utc.getMonth() + 1)).slice(-2);
				var day = ("0" + now_utc.getDate()).slice(-2);
				return [now_utc.getFullYear(), mnth, day].join("-");				
			} 
		 
				$scope.chartTitle='Zone Analysis for '+$scope.subtype+" from "+$scope.titledate;
				$scope.loadColumnchartDisplay();
	 
	}
	
	
	$scope.getDwellTime = function(uid) {
		
			
		 for(j=0; j<$scope.dataListTimeanalysis.length; j++){		 
			if(uid == $scope.dataListTimeanalysis[j].uId ){
				return $scope.dataListTimeanalysis[j];
			}
		 }
		 
		 return null;
	}
	
	$scope.loadColumnchartDisplay = function() {
		
		// var chartdata=[];
		if (Object.keys($scope.timeanalysisdummyList).length == 0){	
			
			$scope.norecords=true;
			$scope.no_data_text = "No Record found";
			$('#chart-area').empty();
			$scope.exportXLS = false;
			$scope.exportPDF = false;
			
		}
	else
		{	
		
		$('#chart-area').empty();
		$scope.norecords=false;
		$scope.exportXLS = true;
		$scope.exportPDF = true;
		$scope.generateDateTime();
		if(moment($scope.DwellStartDate).format("MMM DD, YYYY")==moment($scope.DwellEndDate).format("MMM DD, YYYY"))
			 $scope.dwelltitledate=moment($scope.DwellStartDate).format("MMM DD, YYYY");
			else
				$scope.dwelltitledate=moment($scope.DwellStartDate).format("MMM DD, YYYY")+" - "+moment($scope.DwellEndDate).format("MMM DD, YYYY");
		
		$scope.exportDetailTitle="";
		$scope.pdfHeader="";
		$scope.pdfDate="";
		
			if($scope.typeofdays == "1")
			$scope.exportDetailTitle='Zone Analysis for '+$scope.subtype+" on "+$scope.dwelltitledate;
		else
			$scope.exportDetailTitle='Zone Analysis for '+$scope.subtype+" from "+$scope.dwelltitledate;
		
			$scope.pdfHeader="EI4.0 Analytics - User Time Analysis Chart";
			$scope.pdfDate=$scope.dwelltitledate;
            $scope.filename = "EI4.0"    + $scope.previousEmployee + "_"     + "Time ";
            $scope.sheetname = $scope.previousEmployee + "_"     +"Time";
			

			var primaryZone, secondaryZone, thirdZone, otherZone= "";
			var userIdList="";
			var pdwell="";
			var sdwell=""; 
			var tdwell="";
			var otherDwell="";
			var totaldwell =0;
			$scope.tableViewArray=[]; 
			$scope.primaryZone=[]; 
			$scope.secondaryZone=[];
			$scope.thirdZone=[];
			 $scope.userIdList=[];
			var asset = [];
			 $scope.pdwellValue =[];
			 $scope.sdwellValue =[];
			 $scope.tdwellValue =[];
			 $scope.odwellValue =[];
			 
			 for(i=0; i<$scope.empList.length; i++){
				 
					 var dwellData = $scope.getDwellTime($scope.empList[i].uId);
					 if(dwellData!=null){
						 
						 userIdList = dwellData.uId;						 
					 	 primaryZone = dwellData.primaryzone;
						 secondaryZone = dwellData.secondaryzone;
						 thirdZone = dwellData.thirdzone;
						 
						 if(dwellData.primaryDwellTime!= undefined 
								 && dwellData.primaryDwellTime!= null 
								 && dwellData.primaryDwellTime>0)
							 pdwell = Math.round(dwellData.primaryDwellTime/60);
						 else
							 pdwell = 0;
							 
						 if(dwellData.secondayDwellTime!= undefined 
								 && dwellData.secondayDwellTime!= null 
								 && dwellData.secondayDwellTime>0)
							 sdwell = Math.round(dwellData.secondayDwellTime/60);
						 else
							 sdwell = 0;
						 if(dwellData.thirdzoneDwellTime!= undefined 
								 && dwellData.thirdzoneDwellTime!= null 
								 && dwellData.thirdzoneDwellTime>0)
							 tdwell = Math.round(dwellData.thirdzoneDwellTime/60);
						 else
							 tdwell = 0;
						 
						 if(dwellData.otherzoneDwellTime!= undefined 
								 && dwellData.otherzoneDwellTime!= null 
								 && dwellData.otherzoneDwellTime>0)
							 otherDwell = Math.round(dwellData.otherzoneDwellTime/60);
						 else
							 otherDwell = 0;
						 
						 $scope.userIdList.push(userIdList);
						 $scope.primaryZone.push(primaryZone);
						 $scope.secondaryZone.push(secondaryZone);
						 $scope.thirdZone.push(thirdZone);
						 $scope.pdwellValue.push(pdwell);
						 $scope.sdwellValue.push(sdwell);
						 $scope.tdwellValue.push(tdwell);
						 $scope.odwellValue.push(otherDwell);
						 $scope.exportArrayData.push( {
						'Tag Type': userIdList ,
						'Primary Zone': primaryZone,
						'Time in Primary Zone': pdwell,
						'Secondary Zone': secondaryZone,
						'Time in Secondary Zone': sdwell,
						'Third Zone': thirdZone,
						'Time in Third Zone': tdwell,
						'Time in other Zone': otherDwell,
						}); 
						
						 if(pdwell>totaldwell)
							 totaldwell = pdwell;
						 if(sdwell>totaldwell)
							 totaldwell = sdwell;
						 if(tdwell>totaldwell)
							 totaldwell = tdwell;
						 if(otherDwell>totaldwell)
							 totaldwell = otherDwell;
					 }  			 
			 }
			
			// to set maximum height for y axis
			var maxLimit = totaldwell+ 100;
			
		var ticksStyle = {
		   	    fontColor: '#495057',
		   	    fontStyle: 'bold'
		   	  }

		   	  var mode = 'index'
		   	  var intersect = true	   	  

		   	 $('#chart-Test').remove();
			  $('#chartview').append('<canvas id="chart-Test"  height="550" width="1200"></canvas>');
		   	    var $TimeAnalyticsChart = $('#chart-Test')
		   	var rectangleSet = false;
		   	  // eslint-disable-next-line no-unused-vars
		   	 
		   	  var chartTest = new Chart($TimeAnalyticsChart, {
		   	    type: 'bar',
		   	    data: {
		   	     labels: $scope.userIdList,
		   	    	datasets: [
		   	        {
		   	label: 'Primary Zone',
		   	          backgroundColor: '#2E86C1',
		   	          borderColor: '#007bff',
		   	          data:$scope.pdwellValue
		   	        },
		   	        {
		   	label: 'Secondary Zone',
		   	          backgroundColor: '#27AE60',
		   	          borderColor: '#ced4da',
		   	          data:$scope.sdwellValue 
		   	        },
		   	        {
		   	label: 'Third Zone',
		   	          backgroundColor: '#D2691E',
		   	          borderColor: '#1c4166',
		   	          data: $scope.tdwellValue
		   	        },
		   	     {
		   			   	label: 'Other Zones',
		   			   	          backgroundColor: '#ff0000',
		   			   	          borderColor: '#ced4da',
		   			   	          data: $scope.odwellValue
		   			   	        },
		   	      ]
		   	    },
		   	    options: {
		   	      maintainAspectRatio: true,
		   	 responsive: true,
		   	      tooltips: {
		   	        mode: mode,
		   	        intersect: intersect
		   	      },
		   	      hover: {
		   	        mode: mode,
		   	        intersect: intersect
		   	      },
		   	      legend: {
		   	        display: true,
		   	position: 'right'
		   	      },
		   	 animation:{
		   	        onComplete : function(){
		   	            /*Your code here*/
		   	if (!rectangleSet) {
		   	                        var scale = window.devicePixelRatio;                      

		   	                        var sourceCanvas = chartTest.canvas;
		   	                        var copyWidth = chartTest.scales['y-axis-0'].width - 10;
		   	                        var copyHeight = chartTest.scales['y-axis-0'].height + chartTest.scales['y-axis-0'].top + 10;

		   	                        var targetCtx = document.getElementById("axis-Test").getContext("2d");

		   	                        targetCtx.scale(scale, scale);
		   	                        targetCtx.canvas.width = copyWidth * scale;
		   	                        targetCtx.canvas.height = copyHeight * scale;

		   	                        targetCtx.canvas.style.width = `${copyWidth}px`;
		   	                        targetCtx.canvas.style.height = `${copyHeight}px`;
		   	                        targetCtx.drawImage(sourceCanvas, 0, 0, copyWidth * scale, copyHeight * scale, 0, 0, copyWidth * scale, copyHeight * scale);

		   	                        var sourceCtx = sourceCanvas.getContext('2d');

		   	                        // Normalize coordinate system to use css pixels.

		   	                        sourceCtx.clearRect(0, 0, copyWidth * scale, copyHeight * scale);
		   	                        rectangleSet = true;
		   	                    }        
		   	},
		   	onProgress: function () {
		   	                    if (rectangleSet === true) {
		   	                        var copyWidth = chartTest.scales['y-axis-0'].width;
		   	                        var copyHeight = chartTest.scales['y-axis-0'].height + chartTest.scales['y-axis-0'].top + 10;

		   	                        var sourceCtx = chartTest.canvas.getContext('2d');
		   	                        sourceCtx.clearRect(0, 0, copyWidth, copyHeight);
		   	                    }
		   	                }
		   	    },
		   	      scales: {
		   	        yAxes: [{
		   	          // display: false,
		   	          gridLines: {
		   	            display: true,
		   	            lineWidth: '4px',
		   	            color: 'rgba(0, 0, 0, .2)',
		   	            zeroLineColor: 'transparent'
		   	          },
		   	  scaleLabel: {
		   	        display: true,
		   	        labelString: 'Time(mins)'
		   	      },
		   	          ticks: $.extend({
		   	            beginAtZero: true,

		   	            // Include a dollar sign in the ticks
		   	            callback: function (value) {
		   	              //if (value >= 1000) {
		   	              //  value /= 1000
		   	              //  value += 'k'
		   	              //}

		   	              //return '$' + value
		   	              return value
		   	            }
		   	          }, ticksStyle)
		   	        }],
		   	        xAxes: [{
		   	          display: true,
		   	 scaleLabel: {
		   	        display: true,
		   	        labelString: 'Employee'
		   	      },
		   	          gridLines: {
		   	            display: true
		   	          },
		   	          ticks: ticksStyle
		   	        }]
		   	      }
		   	    }
		   	  })
		
		}
	}
	


    $scope.getProductAnalysisData = function(name) {
    
	     for(j=0; j<$scope.dataList.length; j++){         
	        if(name == $scope.dataList[j].zoneName ){
	            return $scope.dataList[j];
	        }
	     }
	     
	     return null;
	}
    
	$scope.loadProductanalysisHeatMap = function(){
		$('#chart-area').empty();
		
		var colordata=['#ff5153','#6cff6b', '#009100'];
		var color=[];
		var chartdat=[];
		$scope.exportArrayData=[];
		var ocuupancyRate=120;

		var zoneData= [];
		 
		
		if($scope.selectedZone=='All')
		{
			for (var n = 0; n < $scope.zoneList.length; n++) {
				var zoneName = $scope.zoneList[n].name;			
				var productanalysisData = $scope.getProductAnalysisData(zoneName);
				if(productanalysisData!=null)
				{
					if($scope.zoneList[n].name!='Corporate Office')
					{
						zoneData.push( {
								'zoneID': $scope.zoneList[n].id,
								'zoneName':$scope.zoneList[n].name,
								'incoming': productanalysisData.incoming, 
								'outgoing':productanalysisData.outgoing, 
								'waiting': productanalysisData.waiting  
						});
					}
				}
			}
		
		}else
		{
			for (var n = 0; n < $scope.dataList.length; n++) {
				
				if($scope.dataList[n].name!='Corporate Office')
				{
					zoneData.push( {
						'zoneID': $scope.dataList[n].id,
						'zoneName':$scope.dataList[n].name,
						'incoming': $scope.dataList[n].incoming, 
						'outgoing':$scope.dataList[n].outgoing, 
						'waiting': $scope.dataList[n].waiting  
				});
				}
				
			}	
		}
		
		
		for (var n = 0; n < zoneData.length; n++) {
			
			
			var fromTime= "";
			var endTime= "";
			var colorTime = 0;
			if($scope.typeofdays=="1"){
				colorTime = 1;
				fromTime=  moment(new Date).format("MMM DD, YYYY");
				endTime=  moment(new Date).format("MMM DD, YYYY");
			}else if($scope.typeofdays=="2"){
				colorTime = 2;
				endTime=  moment(new Date).format("MMM DD, YYYY");
				fromTime=  moment(new Date).subtract(1, 'days').format("MMM DD, YYYY") ;
	;
			}else if($scope.typeofdays=="7"){ 
				colorTime = 7;
				endTime=  moment(new Date).format("MMM DD, YYYY");
				fromTime=  moment(new Date).subtract(6, 'days').format("MMM DD, YYYY") ;
			}else if($scope.typeofdays=="14"){ 
				colorTime = 14;
				endTime=  moment(new Date).format("MMM DD, YYYY");
				fromTime=  moment(new Date).subtract(13, 'days').format("MMM DD, YYYY") ;
			}
			
			var target = 12 * colorTime;
            
            var total = zoneData[n].outgoing ;
            if(total>target){
                color.push(colordata[2]);
			}else  if(total==target){
                color.push(colordata[1]);
            }else if(total < target){
                color.push(colordata[0]);
            } 
			
			chartdat.push({
				'label':zoneData[n].zoneName+"\n"+"Incoming : "+zoneData[n].incoming+"\n"+"Outgoing : "+zoneData[n].outgoing
				+"\n"+"Waiting : "+zoneData[n].waiting,
				'value': 1  
			});
			$scope.exportArrayData.push({
			 	'Zone-Name':zoneData[n].zoneName,	
				'Incoming':zoneData[n].incoming,
				'Outgoing':zoneData[n].outgoing,
				'Waiting':zoneData[n].waiting 
			});
		}
		
	 	$scope.exportXLS = true;
		$scope.exportPDF = true;
		// to export chart data
		$scope.generateDateTime();
	/*
	 * if(moment($scope.OccupancyStartDate).format("MMM DD,
	 * YYYY")==moment($scope.OccupancyEndDate).format("MMM DD, YYYY"))
	 * $scope.Occupancytitledate = moment($scope.OccupancyStartDate).format("MMM
	 * DD, YYYY"); else $scope.Occupancytitledate =
	 * moment($scope.OccupancyStartDate).format("MMM DD, YYYY")+" -
	 * "+moment($scope.OccupancyEndDate).format("MMM DD, YYYY");
	 */ 
		
		
		
		$scope.exportDetailTitle="";
		$scope.pdfHeader="";
		$scope.pdfDate="";
		
		if($scope.selectedZone!='All')
		{
			if($scope.typeofdays=="1"){
				$scope.exportDetailTitle='Zone Flow Analytics for '+$scope.selectedZone+' on '+moment(new Date()).format("MMM DD, YYYY");
			}else 
				$scope.exportDetailTitle='Zone Flow Analytics for '+$scope.selectedZone+" from "+fromTime+" to "+endTime;
		}
		else 
		{
			if($scope.typeofdays=="1"){
				$scope.exportDetailTitle='Zone Flow Analytics on '+moment(new Date()).format("MMM DD, YYYY");
			}else 
				$scope.exportDetailTitle="Zone Flow Analytics from "+fromTime+" to "+endTime; 
		}
		
		$scope.pdfHeader="Zone Flow Analytics - Heat Map";
		$scope.pdfDate= fromTime;
		$scope.filename = "ZFA_"	+ $scope.selectedZone + "_"	 + "Zone Flow Analytics";
		$scope.sheetname = $scope.selectedZone+ "_"	 +"Zone Flow Analytics";
		 
		var container = document.getElementById('chart-area');
		var data = {
				series :chartdat
		};
		var options = {
				chart: {
					width: screenWidth * 0.80,
					height: screenHeight * 0.80,
					format: '1,000',
					title:{
						text: $scope.exportDetailTitle , 
						align: "center"
					}
					
					
				},
				series: {
					showLabel: true, 
					zoomable: false,
					useLeafLabel: true
				},
				tooltip: {
					// suffix: '?'
				},
				legend: {
					align: 'right'
				},
				chartExportMenu: {
			        visible: false  // default is true.
			    }
		};
		var theme = {
				series: {
					colors:color ,
					borderColor: '#ffffff'

				}
		};

		// For apply theme

		tui.chart.registerTheme('myTheme', theme);
		options.theme = 'myTheme';

		tui.chart.treemapChart(container, data, options); 
		
	}
	



	$scope.loadColumnChartProductAnalysisDisplay = function() {
		
		// var chartdata=[];
		if ($scope.dataList.length == 0){	
			
			$scope.norecords=true;
			$scope.no_data_text = "No Record found";
			$('#chart-area').empty();
			$scope.exportXLS = false;
			$scope.exportPDF = false;
			
		}
	else
		{	
		
		$('#chart-area').empty();
		$scope.norecords=false;
		$scope.exportXLS = true;
		$scope.exportPDF = true;
		$scope.generateDateTime();
		/*
		 * if(moment($scope.DwellStartDate).format("MMM DD,
		 * YYYY")==moment($scope.DwellEndDate).format("MMM DD, YYYY"))
		 * $scope.dwelltitledate=moment($scope.DwellStartDate).format("MMM DD,
		 * YYYY"); else
		 * $scope.dwelltitledate=moment($scope.DwellStartDate).format("MMM DD,
		 * YYYY")+" - "+moment($scope.DwellEndDate).format("MMM DD, YYYY");
		 */
		$scope.exportDetailTitle = "" 
			
		$scope.pdfHeader="";
		$scope.pdfDate="";
		
		var fromTime= "";
		var endTime= "";
		if($scope.typeofdays=="1"){
			fromTime=  moment(new Date).format("MMM DD, YYYY");
			endTime=  moment(new Date).format("MMM DD, YYYY");
		}else if($scope.typeofdays=="2"){
			endTime=  moment(new Date).format("MMM DD, YYYY");
			fromTime=  moment(new Date).subtract(1, 'days').format("MMM DD, YYYY") ;
		}else if($scope.typeofdays=="7"){ 
			endTime=  moment(new Date).format("MMM DD, YYYY");
			fromTime=  moment(new Date).subtract(6, 'days').format("MMM DD, YYYY") ;
		}else if($scope.typeofdays=="14"){ 
			endTime=  moment(new Date).format("MMM DD, YYYY");
			fromTime=  moment(new Date).subtract(13, 'days').format("MMM DD, YYYY") ;
		}
		if($scope.selectedZone!='All')
		{
			if($scope.typeofdays=="1"){
				$scope.exportDetailTitle='Zone Flow Analytics for '+$scope.selectedZone+' on '+moment(new Date()).format("MMM DD, YYYY");
			}else 
				$scope.exportDetailTitle='Zone Flow Analytics for '+$scope.selectedZone+" from "+fromTime+" to "+endTime;
		}
		else 
		{
			if($scope.typeofdays=="1"){
				$scope.exportDetailTitle='Zone Flow Analytics on '+moment(new Date()).format("MMM DD, YYYY");
			}else 
				$scope.exportDetailTitle="Zone Flow Analytics from "+fromTime+" to "+endTime; 
		}
			
			$scope.pdfHeader="Zone Flow Analytics - Chart";
			$scope.pdfDate= $scope.dwelltitledate;
			$scope.filename =  $scope.selectedZone + "_"	 + "Zone Flow Analytics";
			$scope.sheetname = $scope.selectedZone+ "_"	 +"Zone Flow Analytics";
			 
			  
			 var incomingValue =[];
			 var outgoingValue =[];
			 var waitingValue =[];
			 var zoneNameValue =[];
			 var zoneIDValue =[]; 
 			  
			 var avgIncomingDataList =[]; 
			 var avgOutGoingDataList =[]; 
			 var avgWaitingDataList =[];  
 			 
			 var totaldwell = 0;
			 if($scope.selectedZone=='All')
			{
				 
				 var zoneDataList = [];
				 
				for (var n = 0; n < $scope.zoneList.length; n++) {
					var zoneName = $scope.zoneList[n].name;			
					if(zoneName!='Corporate Office')
					{
						var productanalysisData = $scope.getProductAnalysisData(zoneName);
						if(productanalysisData!=null)
						{
							
							productanalysisData.id = $scope.zoneList[n].id; 
							productanalysisData.name = $scope.zoneList[n].name; 
							zoneDataList.push(productanalysisData);  
						}
					}
				}
				
				 if ($scope.sortBy == '0')
					 zoneDataList = zoneDataList
							.sort(GetSortOrder1("incoming"));
				else  if ($scope.sortBy == '1')
					zoneDataList = zoneDataList
							.sort(GetSortOrder1("outgoing"));
				else 
					zoneDataList = zoneDataList
							.sort(GetSortOrder1("waiting"));
				 
				for (var n = 0; n < zoneDataList.length; n++) {
					
					incomingValue.push(zoneDataList[n].incoming);
					outgoingValue.push(zoneDataList[n].outgoing);
					waitingValue.push(zoneDataList[n].waiting);
					zoneNameValue.push(zoneDataList[n].name);
					zoneIDValue.push(zoneDataList[n].id); 
					
					if(zoneDataList[n].incoming>totaldwell)
						 totaldwell = zoneDataList[n].incoming;
					if(zoneDataList[n].outgoing>totaldwell)
						 totaldwell = zoneDataList[n].outgoing;
					if(zoneDataList[n].waiting>totaldwell)
						 totaldwell = zoneDataList[n].waiting;
					 
				}
			
			}else
			{
				
				 if ($scope.sortBy == '0')
					 $scope.dataList = $scope.dataList
							.sort(GetSortOrder1("incoming"));
				else  if ($scope.sortBy == '1')
					$scope.dataList = $scope.dataList
							.sort(GetSortOrder1("outgoing"));
				else 
					$scope.dataList = $scope.dataList
							.sort(GetSortOrder1("waiting"));
				 
				for (var n = 0; n < $scope.dataList.length; n++) { 
					 
					incomingValue.push($scope.dataList[n].incoming);
					outgoingValue.push($scope.dataList[n].outgoing);
					waitingValue.push($scope.dataList[n].waiting);
					zoneNameValue.push( moment(new Date).subtract(n, 'days').format("MMM DD, YYYY"));
					zoneIDValue.push($scope.dataList[n].zoneId);  
					
					if($scope.dataList[n].incoming>totaldwell)
						 totaldwell = $scope.dataList[n].incoming;
					if($scope.dataList[n].outgoing>totaldwell)
						 totaldwell = $scope.dataList[n].outgoing;
					if($scope.dataList[n].waiting>totaldwell)
						 totaldwell = $scope.dataList[n].waiting; 
					
					    
				}	
			}
			  
			 var incomingTot = 0, outgoingTot = 0, waitingTot = 0; 
			 for (var n = 0; n < incomingValue.length; n++) { 
				 incomingTot = incomingTot+incomingValue[n];
				 outgoingTot = outgoingTot+outgoingValue[n];
				 waitingTot = waitingTot+waitingValue[n];
			 }
			 
			 
			 var incomingAvg = Math.round(incomingTot/incomingValue.length); 
			 var outgoingAvg = Math.round(outgoingTot/outgoingValue.length); 
			 var waitingAvg = Math.round(waitingTot/waitingValue.length); 
			 
			 for (var n = 0; n < incomingValue.length; n++) { 
				 avgIncomingDataList.push(incomingAvg);
				 avgOutGoingDataList.push(outgoingAvg);
				 avgWaitingDataList.push(waitingAvg);
				 
			 }
			 
			// to set maximum height for y axis
			var maxLimit = totaldwell;
			
			
		
		}
		$scope.drawWorkOrderFlowChart(incomingValue, outgoingValue, waitingValue, zoneNameValue);
	}
	$scope.drawWorkOrderFlowChart = function(IncomingData, OutgoingData, WaitingData, ZoneLabelList){
		
		var ticksStyle = {
				fontColor: '#495057',
				fontStyle: 'bold'
			}
			var mode = 'index'
			var intersect = true
			 $scope.sortingChat = function(dataSortIndex,otherIndex1,otherIndex2) {
		    	// Sorting data and label based on sorting type
		    	var dataset = $scope.ZoneFloorChart.data.datasets[dataSortIndex];
		    	var dataset1 = $scope.ZoneFloorChart.data.datasets[otherIndex1];
		    	var dataset2 = $scope.ZoneFloorChart.data.datasets[otherIndex2];
		    	for(var i = 0; i < dataset.data.length; i++){
		    	     
		    		   // Last i elements are already in place
		    		   for(var j = 0; j < ( dataset.data.length - i -1 ); j++){
		    		       
		    		     // Checking if the item at present iteration
		    		     // is greater than the next iteration
		    		     if(parseInt(dataset.data[j]) < parseInt(dataset.data[j+1])){
		    		         
		    		       // If the condition is true then swap them
		    		       var temp = dataset.data[j];
		    		       var tempOtherValue1 = dataset1.data[j];
		    		       var tempOtherValue2 = dataset2.data[j];
		    		       var tmpLabel = $scope.ZoneFloorChart.data.labels[j];
		    		       
		    		       dataset.data[j] = dataset.data[j + 1];
		    		       dataset1.data[j] = dataset1.data[j + 1];
		    		       dataset2.data[j] = dataset2.data[j + 1];
		    		       
		    		       $scope.ZoneFloorChart.data.labels[j]=$scope.ZoneFloorChart.data.labels[j+1]
		    		       
		    		       dataset.data[j+1] = temp;
		    		       dataset1.data[j+1] = tempOtherValue1;
		    		       dataset2.data[j+1] = tempOtherValue2;

		    		       $scope.ZoneFloorChart.data.labels[j+1] = tmpLabel;
		    		     }
		    		   }
		    		 }
		    	$scope.ZoneFloorChart.update(); // update the chart
		    }
			 //$('#chartviewParent').empty();
			 $('#idChartAgingCanvas').remove();
			 $('#chartviewAgingchart').append('<canvas id="idChartAgingCanvas"  height="550px" width="1200px"></canvas>');
					  var $ZoneFloorChartInHtml = $('#idChartAgingCanvas')
				  // eslint-disable-next-line no-unused-vars
				  
				  // eslint-disable-next-line no-unused-vars
				  $scope.ZoneFloorChart = new Chart($ZoneFloorChartInHtml, {
				    type: 'bar',
				    data: {
				      labels: ZoneLabelList,
				      datasets: [
						        {
									label: 'Incoming', 
						          backgroundColor: '#6958db',
						          borderColor: '#6958db',
						          data: IncomingData
						        },
						        {
									label: 'Outgoing',
						          backgroundColor: 'green',
						          borderColor: 'green',
						          data: OutgoingData
						        },
						        {
									label: 'Waiting',
						          backgroundColor: '#007bff',
						          borderColor: '#007bff',
						          data: WaitingData
						        }
						      ]
				    },
				    options: {
				      maintainAspectRatio: false,
					  responsive: true,
				      tooltips: {
				        mode: mode,
				        intersect: intersect
				      },
				      hover: {
				        mode: mode,
				        intersect: intersect
				      },
				      legend: {
				        display: true,
						position: 'top'
				     },
					 //11Jan
				      scales: {
				        yAxes: [{
				          // display: false,
				          gridLines: {
				            display: true,
				            lineWidth: '4px',
				            color: 'rgba(0, 0, 0, .2)',
				            zeroLineColor: 'transparent'
				          },
						   scaleLabel: {
				        display: true,
				        labelString: '# Of Work Order' 
				      },
				          ticks: $.extend({
				            beginAtZero: true,

				            // Include a dollar sign in the ticks
				            callback: function (value) {
				              // if (value >= 1000) {
				              // value /= 1000
				              // value += 'k'
				              // }

				              return value
				              // /return value
				            }
				          }, ticksStyle)
				        }],
				        xAxes: [{
				          display: true,
						  scaleLabel: {
				        display: true,
				        labelString: 'Work Station'
				      },
				          gridLines: {
				            display: true
				          },
				          ticks: ticksStyle
				        }]
				      }
				    }
				  })  
			//$scope.sortingChat(0,1,2);
		 
		    	var dataSortIndex = 0;
		    	var otherIndex1,otherIndex2;
		    	if ($scope.productanAlysisSortBy == 0) {
		    		
		    		dataSortIndex = 0
		    		otherIndex1=1;
		    		otherIndex2=2;
		    		
		    		
		    	}else if ($scope.productanAlysisSortBy == 1){
		    		
		    		dataSortIndex = 1;
		    		otherIndex1=0;
		    		otherIndex2=2;
		    	}else {
		    		dataSortIndex = 2;
		    		otherIndex1=0;
		    		otherIndex2=1;
		    		
		    	}
		    	$scope.sortingChat(dataSortIndex,otherIndex1,otherIndex2);
		    	
	}
	    $scope.changeMapType = function(){
        
        if($scope.isEmployeedwelltimeClicked){
        	
        	$scope.type = "People";
            $scope.subtype = "Employee";  
            $("#colorSubtypeSelectDiv").show();
            
            $scope.subTypesArray = []; 
            $scope.subTypesArray.push({"type":"Employee"});
            $scope.subTypesArray.push({"type":"Visitor"}); 
            $scope.empList = [];
            $scope.empList = $scope.employees;
            $scope.empList.sort(GetSortOrder("uId"));
			$scope.emp=$scope.empList[0].uId;
			$scope.previousEmployee= $scope.emp

            $scope.getZoneDetails();
        }else if($scope.isproductanalysisClicked){ 
            $scope.selectedZone = "All";
            $scope.typeofdays = "1";
            $scope.typeofday=true;
				
            $scope.getZoneDetails();
        }else  if($scope.isMaterialTraceClicked){
            $scope.typeofdays = "1";
        	$scope.chartViewShow=false;
        	
			if($scope.viewValue=="Line Chart"){
				$scope.isDateSelected =false;
				$scope.typeofday=true;	 
				$scope.selectdate=false; 			
				$scope.getZoneDetails();
			}else if($scope.viewValue=="Table View"){				
				$scope.typeofday=false;
				$scope.viewOption();
			}				
        }else
            $scope.viewOption();
    }
	
	$scope.loadMap= function(){
		// $scope.mistLogin();
		// to display slider starts here
		//$scope.orgoperationalstarttime="08:00 AM"
		//$scope.orgoperationalendtime="07:15 PM"
		  
		var startpos=0;
		var endpos=0;
	 	var pos=0;
		var categories=[]; 
		for(var st=0;st<arr.length;st++){
			if(startpos==0){
				if(arr[st]==$scope.orgoperationalstarttime){
					if(st!=0 && st>4)
						startpos=st-4;
					else
						startpos = 0;
				}
			}
			if(endpos==0){
				if(arr[st]==$scope.orgoperationalendtime){
					if(st!=96 && st<=92)
						endpos=st+4;
					else
						endpos = 94
				}
			}
		}
		for(var ct=startpos;ct<=endpos;ct++){
			categories.push(arr[ct]);
		} 
		
		$scope.timeval=categories[startpos]; 		
		$scope.tqidval=startpos;
		
		$( "#slider-range-min" ).slider({
			range: "min",
			min: 0,
			max: categories.length-1,
			value:0,
			slide: function( event, ui ) {				
	 			$scope.timeval=categories[ui.value];
				$scope.tqidval=ui.value;
				$scope.getZones()
				$scope.$apply();				
			} 
		});
		// to display slider ends here
		$scope.getZones();
	}

	$scope.loadoccupancyHeatMap=function(){
		
		$('#chart-area').empty();
		var colordata=["#69696e","#2f522f","#33ff33","#a6a638" ,"#FF8C00" ,"#ff0000","#cc0000"];
		var color=[];
		var chartdat=[];
		$scope.exportArrayData=[];
		var ocuupancyRate=120;

		var zoneData={};
		// $scope.zoneList.sort(GetSortOrder("name"));
		for (var n = 0; n < $scope.zoneList.length; n++) {
			var zoneKey = $scope.zoneList[n].id;						
			$scope.getZoneFinalList(zoneKey,  $scope.activestartdateOccupancy, $scope.activeenddateOccupancy);	
			for (var i = 0; i < $scope.dataList.length; i++) {
			if ($scope.FINAL_DAY_ARRAY.includes($scope.dataList[i].date)) {
				var zoneclassification =$scope.zoneList[n].classification;
				var className="";
				
				$scope.getClassificationsFinalList();
				
				for (var k = 0; k < $scope.zoneClassificationFinalList.length; k++) {
					var zoneclassObj = $scope.zoneClassificationFinalList[k];
					if(zoneclassification==zoneclassObj.classification) 
						{
						 
						className=zoneclassification; 
						break;
						}
					else{
						
						className="No Class"
					}

				}		
			zoneData[$scope.zoneList[n].id] =  {
					'zoneID': $scope.zoneList[n].id,
					'zoneName':$scope.zoneList[n].name,
					'Max':0, 
					'Peak':0,
					'Average':0,
					'Classification':className,
					'OccupancyRate':0, 
			};
		}
		}
		}
		if (Object.keys(zoneData).length == 0){
			$scope.exportXLS = false;
			$scope.exportPDF = false;
			$scope.norecords=true;
			$scope.no_data_text = "No Record found";
			$('#chart-area').empty();
		}
	else
		{
		for (var i = 0; i < $scope.dataList.length; i++) {
			var dateObj = $scope.dataList[i];
			if (dateObj != undefined && dateObj != null) {
				var zoneObjList = dateObj.zone;

				if (zoneObjList != undefined && zoneObjList != null && zoneObjList.length>0) {
					zoneObjList.sort(GetSortOrder("zoneName"));

					for (var j = 0; j < zoneObjList.length; j++) {
						var zoneObj = zoneObjList[j];
						
						var zoneclassification =zoneObj.zoneClassification;
						var className="";
						
						$scope.getClassificationsFinalList();
						
						for (var k = 0; k < $scope.zoneClassificationFinalList.length; k++) {
							var zoneclassObj = $scope.zoneClassificationFinalList[k];
							if(zoneclassification==zoneclassObj.classification) 
								{
								 
								className=zoneclassification; 
								break;
								}
							else{
								
								className="No Class"
							}

						}
					
						if(zoneObj.zoneRestrictions!=0)
							ocuupancyRate=zoneObj.dWellTime.maximum/zoneObj.zoneRestrictions*100;

						if(zoneData.hasOwnProperty(zoneObj.zoneID))
						{
							var listObj = zoneData[zoneObj.zoneID];
							if(listObj.Peak<zoneObj.dWellTime.maximum){
								listObj.Max=zoneObj.zoneRestrictions; 
								listObj.Peak=zoneObj.dWellTime.maximum;
								listObj.Average=zoneObj.dWellTime.average;
								listObj.Classification=className;
								listObj.OccupancyRate=Math.floor(ocuupancyRate);
								zoneData[zoneObj.zoneID]=listObj;

							}
						} 
					}

				}
			}
		} 

		for ( var key in zoneData) {
			if(zoneData[key].OccupancyRate==0){
				color.push(colordata[0]);
			}else if(zoneData[key].OccupancyRate>0&&zoneData[key].OccupancyRate<50){
				color.push(colordata[1]);
			}else if(zoneData[key].OccupancyRate>=50&&zoneData[key].OccupancyRate<75){
				color.push(colordata[2]);
			}else	if(zoneData[key].OccupancyRate>=75&&zoneData[key].OccupancyRate<90){
				color.push(colordata[3]);
			}else	if(zoneData[key].OccupancyRate>=90&&zoneData[key].OccupancyRate<100){
				color.push(colordata[4]);
			}else	if(zoneData[key].OccupancyRate>=100&&zoneData[key].OccupancyRate<110){
				color.push(colordata[5]);
			}else	if(zoneData[key].OccupancyRate>=110){
				color.push(colordata[6]);
			}
			chartdat.push({
				'label':zoneData[key].zoneName+"\n"+"Classification : "+zoneData[key].Classification+"\n"+"Max : "+zoneData[key].Max+"\n"+"Peak : "+zoneData[key].Peak
				+"\n"+"Average : "+zoneData[key].Average+"\n"+"Occupancy-Rate in % : "+zoneData[key].OccupancyRate,
				'value': 1,
				'zoneID': zoneData[key].zoneID,
				'zoneName':zoneData[key].zoneName,
				'Max':zoneData[key].Max, 
				'Peak':zoneData[key].Peak,
				'Average':zoneData[key].Average,
				'Classification':zoneData[key].Classification,
				'Occupancy-Rate in %':zoneData[key].OccupancyRate,

			});
			$scope.exportArrayData.push({
				'Zone-Name':zoneData[key].zoneName,	
				'Classification':zoneData[key].Classification,
				'Max-Capacity':zoneData[key].Max, 
				'Peak-Occupancy':zoneData[key].Peak,
				'Average-Occupancy':zoneData[key].Average,				
				'Occupancy-Rate in %':zoneData[key].OccupancyRate
			});
		}
		
		$scope.exportXLS = true;
		$scope.exportPDF = true;
		// to export chart data
		$scope.generateDateTime();
		if(moment($scope.OccupancyStartDate).format("MMM DD, YYYY")==moment($scope.OccupancyEndDate).format("MMM DD, YYYY"))
			 $scope.Occupancytitledate=moment($scope.OccupancyStartDate).format("MMM DD, YYYY");
		else
			$scope.Occupancytitledate=moment($scope.OccupancyStartDate).format("MMM DD, YYYY")+" - "+moment($scope.OccupancyEndDate).format("MMM DD, YYYY");
		
		$scope.exportDetailTitle="";
		$scope.pdfHeader="";
		$scope.pdfDate="";
		
			$scope.exportDetailTitle='Peak Occupancy from '+$scope.Occupancytitledate;
			$scope.pdfHeader="Zone Analytics - Occupancy Chart";
			$scope.pdfDate=$scope.Occupancytitledate;
            $scope.filename = "EI4.0"    + "Occupancy";
			$scope.sheetname ="Zone_Occupancy";
			
		var container = document.getElementById('chart-area');
		var data = {
				series :chartdat
		};
		var options = {
				chart: {
					width: screenWidth * 0.80,
					height: screenHeight * 0.80,
					format: '1,000',
					title:{
						text: 'Peak Occupancy from '+$scope.Occupancytitledate, 
						align: "center"
					}
					
					
				},
				series: {
					showLabel: true, 
					zoomable: false,
					useLeafLabel: true
				},
				tooltip: {
					// suffix: '?'
				},
				legend: {
					align: 'right'
				},
				chartExportMenu: {
			        visible: false  // default is true.
			    }
		};
		var theme = {
				series: {
					colors:color ,
					borderColor: '#ffffff'

				}
		};

		// For apply theme

		tui.chart.registerTheme('myTheme', theme);
		options.theme = 'myTheme';

		tui.chart.treemapChart(container, data, options);
	}
	}
	$scope.loadoccupancyClassificationchart=function(){
		
		$('#chart-area').empty();
		$scope.norecords=false;
		var color=[];
		var chartdat=[];
		var zoneData={};
		$scope.exportArrayData=[];
		// $scope.zoneList.sort(GetSortOrder("name"));
		for (var n = 0; n < $scope.zoneList.length; n++) {
			var zoneKey = $scope.zoneList[n].id;						
			$scope.getZoneFinalList(zoneKey,  $scope.activestartdateOccupancy, $scope.activeenddateOccupancy);	
			for (var i = 0; i < $scope.dataList.length; i++) {
			if ($scope.FINAL_DAY_ARRAY.includes($scope.dataList[i].date)) {
			var colorcode="#69696e";
			
			if($scope.zoneList[n].classification!=null && $scope.zoneList[n].classification!=undefined && $scope.zoneList[n].classification!='')
				for (var k = 0; k < $scope.zoneClassificationList.length; k++){
					if ($scope.zoneList[n].classification == $scope.zoneClassificationList[k].classification)
						colorcode = $scope.zoneClassificationList[k].colorCode
				}

			zoneData[$scope.zoneList[n].id] =  {
					'zoneID': $scope.zoneList[n].id,
					'zoneName':$scope.zoneList[n].name,
					'Max':0, 
					'Peak':0,
					'Average':0,
					'Classification':$scope.zoneList[n].classification,
					'colorcode':colorcode,
					'OccupancyRate':0, 
			};
		}
			}
			}
		if (Object.keys(zoneData).length == 0){
			$scope.exportXLS = false;
			$scope.exportPDF = false;
			$scope.norecords=true;
			$scope.no_data_text = "No Record found";
			$('#chart-area').empty();
		}
	else
		{
		$scope.exportXLS = true;
		$scope.exportPDf = true;
		$scope.dataList.sort(GetSortOrder("zoneName")); 
		for (var i = 0; i < $scope.dataList.length; i++) {
			var dateObj = $scope.dataList[i];
			if (dateObj != undefined && dateObj != null) {

				var zoneObjList = dateObj.zone;

				if (zoneObjList != undefined && zoneObjList != null && zoneObjList.length>0) {
					zoneObjList.sort(GetSortOrder("zoneName"));
					var ocuupancyRate=120;
					for (var j = 0; j < zoneObjList.length; j++) {
						var zoneObj = zoneObjList[j];
						if(zoneObj.zoneRestrictions!=0)
							ocuupancyRate=zoneObj.dWellTime.maximum/zoneObj.zoneRestrictions*100;

						if(zoneData.hasOwnProperty(zoneObj.zoneID))
						{
							var listObj = zoneData[zoneObj.zoneID];
							if(listObj.Peak<zoneObj.dWellTime.maximum){
								listObj.Max=zoneObj.zoneRestrictions; 
								listObj.Peak=zoneObj.dWellTime.maximum;
								listObj.Average=zoneObj.dWellTime.average;
								listObj.OccupancyRate=Math.floor(ocuupancyRate);

								zoneData[zoneObj.zoneID]=listObj;

							}
						} 
					}

				} 
			}
		}
		for ( var key in zoneData) {
			var zoneclassification =zoneData[key].Classification;
			var listObj =zoneData[key];
			
			$scope.getClassificationsFinalList();
			
			for (var k = 0; k < $scope.zoneClassificationFinalList.length; k++) {
				var zoneclassObj = $scope.zoneClassificationFinalList[k];
				if(zoneclassification==zoneclassObj.classification) 
					{
					listObj.colorcode=zoneclassObj.colorCode; 
					listObj.Classification=zoneclassObj.classification; 
					break;
					}
				else{
					listObj.colorcode="#778899",
						listObj.Classification="No Class"
				}

			}
			color.push(listObj.colorcode);
			chartdat.push( {
				'label':listObj.zoneName+"\n"+"Classification : "+listObj.Classification+"\n"+"Max : "+listObj.Max+"\n"+"Peak : "+listObj.Peak
				+"\n"+"Average : "+listObj.Average+"\n"+"Occupancy-Rate : "+listObj.OccupancyRate+" %",
				'value': 1,
				'zoneID': listObj.zoneID,
				'zoneName':listObj.zoneName,
				'Max':listObj.Max, 
				'Peak':listObj.Peak,
				'Average':listObj.Average,
				'Classification':listObj.Classification,
				'Occupancy-Rate in %':listObj.OccupancyRate,
			});
			$scope.exportArrayData.push( {
				'Zone-Name':listObj.zoneName,
				'Classification':listObj.Classification,
				'Max-Capacity':listObj.Max, 
				'Peak-Occupancy':listObj.Peak,
				'Average-Occupancy':listObj.Average,				
				'Occupancy-Rate in %':listObj.OccupancyRate,
			});
		}
		
		// to export chart data
		$scope.generateDateTime();
		var container = document.getElementById('chart-area');
		if(moment($scope.OccupancyStartDate).format("MMM DD, YYYY")==moment($scope.OccupancyEndDate).format("MMM DD, YYYY"))
			 $scope.Occupancytitledate=moment($scope.OccupancyStartDate).format("MMM DD, YYYY");
		else
			$scope.Occupancytitledate=moment($scope.OccupancyStartDate).format("MMM DD, YYYY")+" - "+moment($scope.OccupancyEndDate).format("MMM DD, YYYY");
		
		$scope.exportDetailTitle="";
		$scope.pdfHeader="";
		$scope.pdfDate="";
		
			$scope.exportDetailTitle='Peak Occupancy from '+$scope.Occupancytitledate;
			$scope.pdfHeader="Zone Analytics - Occupancy Chart";
			$scope.pdfDate=$scope.Occupancytitledate;
            $scope.filename = "EI4.0"    + "Occupancy";
			$scope.sheetname ="Zone_Occupancy";
		var data = {
				series :chartdat
		};
		var options = {
				chart: {
					width: screenWidth * 0.80,
					height: screenHeight * 0.80,
					format: '1,000',					
					title:{
						text: 'Peak Occupancy from '+$scope.Occupancytitledate, 
						align: "center"
					}
					
				},
				series: {
					showLabel: true, 
					zoomable: false,
					useLeafLabel: true
				},
				tooltip: {
					// suffix: '?'
				},
				legend: {
					align: 'right'
				},
				chartExportMenu: {
			        visible: false  // default is true.
			    }
		};
		var theme = {
				series: {
					colors:color ,
					borderColor: '#ffffff'

				}
		};

		// For apply theme

		tui.chart.registerTheme('myTheme', theme);
		options.theme = 'myTheme';

		tui.chart.treemapChart(container, data, options);
	}
	}
	$scope.loadClassificationchart=function(){
		
		$('#chart-area').empty();
		var color=[];
		var chartdat=[];
		var zoneData={};
		$scope.exportArrayData=[];
	
		 function convertTimestamp(timestamp, convertint = 1) {
				var fromdate;
				if (convertint === 1)
					fromdate = parseInt(timestamp, 10);
				else fromdate = timestamp;

				var d = new Date(0);				
				d.setUTCSeconds(fromdate); // Convert the passed timestamp to
											// milliseconds
				var now_utc = new Date(d.toUTCString().slice(0, -4));
				var mnth = ("0" + (now_utc.getMonth() + 1)).slice(-2);
				var day = ("0" + now_utc.getDate()).slice(-2);
				return [now_utc.getFullYear(), mnth, day].join("-");
				
			}
		for (var n = 0; n < $scope.zoneList.length; n++) {
			var colorcode="#69696e";
			var cDate = convertTimestamp($scope.zoneList[n].created_time);
			var mDate = convertTimestamp($scope.zoneList[n].modified_time);
			var cStatus = $scope.zoneList[n].zoneStatus;
			
			
			if($scope.zoneList[n].classification!=null && $scope.zoneList[n].classification!=undefined && $scope.zoneList[n].classification!='')
				for (var k = 0; k < $scope.zoneClassificationList.length; k++){
					if ($scope.zoneList[n].classification == $scope.zoneClassificationList[k].classification)
						colorcode = $scope.zoneClassificationList[k].colorCode
				}
				
			
			if (cStatus == "Active") {				
				if (new Date(cDate).toISOString().slice(0, 10) <= new Date($scope.enddate).toISOString().slice(0, 10)) {
				
					zoneData[$scope.zoneList[n].id] =  {
							'zoneID': $scope.zoneList[n].id,
							'zoneName':$scope.zoneList[n].name, 
							'Classification':$scope.zoneList[n].classification,
							'colorcode':colorcode,
							'dWellTime':0
					};
					
				}

			}
			else if (cStatus == "Inactive") {
				if ((new Date($scope.startdate).toISOString().slice(0, 10) <= new Date(cDate).toISOString().slice(0, 10)) && 
						(new Date($scope.enddate).toISOString().slice(0, 10) <= new Date(cDate).toISOString().slice(0, 10))) {			
										

					if (new Date(cDate).toISOString().slice(0, 10) == new Date($scope.enddate).toISOString().slice(0, 10)) {
						
						zoneData[$scope.zoneList[n].id] =  {
								'zoneID': $scope.zoneList[n].id,
								'zoneName':$scope.zoneList[n].name, 
								'Classification':$scope.zoneList[n].classification,
								'colorcode':colorcode,
								'dWellTime':0
						};
					
					}
				} else {
				
					if (new Date(mDate).toISOString().slice(0, 10) >= new Date($scope.startdate).toISOString().slice(0, 10)) {						
						zoneData[$scope.zoneList[n].id] =  {
								'zoneID': $scope.zoneList[n].id,
								'zoneName':$scope.zoneList[n].name, 
								'Classification':$scope.zoneList[n].classification,
								'colorcode':colorcode,
								'dWellTime':0
						};
						
					}

				}

			}
			
		}
		if (Object.keys(zoneData).length == 0){
			$scope.exportXLS = false;
			$scope.exportPDF = false;
			$scope.norecords=true;
			$scope.no_data_text = "No Record found";
			$('#chart-area').empty();
		}
	else
		{
		$scope.exportXLS = true;
		$scope.exportPDF = true;
		$scope.dataList.sort(GetSortOrder("zoneName")); 
		for (var i = 0; i < $scope.dataList.length; i++) {
			var dateObj = $scope.dataList[i];
			if (dateObj != undefined && dateObj != null) {
				if(zoneData.hasOwnProperty(dateObj.zoneID))
				{
					var listObj = zoneData[dateObj.zoneID];
					listObj.dWellTime=dateObj.dWellTime; 
					zoneData[dateObj.zoneID]=listObj;

				} 
			}
		}
		for ( var key in zoneData) {
			var zoneclassification =zoneData[key].Classification;
			var listObj =zoneData[key];
			
			$scope.getClassificationsFinalList();
			
			for (var k = 0; k < $scope.zoneClassificationFinalList.length; k++) {
				var zoneclassObj = $scope.zoneClassificationFinalList[k];
				if(zoneclassification==zoneclassObj.classification) 
					{
					listObj.colorcode=zoneclassObj.colorCode; 
					listObj.Classification=zoneclassObj.classification; 
					break;
					}
				else{
					listObj.colorcode="#778899",
					listObj.Classification="No Class"
				}

			}
			color.push(listObj.colorcode);
			/*
			 * rsk 23-Nov-20 added classification, since legend is not in
			 * tui-chart
			 */
			chartdat.push( {
				'label': listObj.zoneName +"\n"+Math.round(listObj.dWellTime/60)
 						+"\n",
				'value': 1
			});
			$scope.exportArrayData.push( {
				'Zone-Name': listObj.zoneName , 
				'Dwell-Time': Math.round(listObj.dWellTime/60),					 
					
		
	});
		}
        $scope.filename = "EI4.0"    + $scope.previousEmployee + "_"     + "Time ";
        $scope.sheetname = $scope.previousEmployee + "_"     +"Time";
		$scope.generateDateTime();
		if(moment($scope.DwellStartDate).format("MMM DD, YYYY")==moment($scope.DwellEndDate).format("MMM DD, YYYY"))
			 $scope.dwelltitledate=moment($scope.DwellStartDate).format("MMM DD, YYYY");
		else
			$scope.dwelltitledate=moment($scope.DwellStartDate).format("MMM DD, YYYY")+" - "+moment($scope.DwellEndDate).format("MMM DD, YYYY");
		
		$scope.exportDetailTitle="";
		$scope.pdfHeader="";
		$scope.pdfDate="";
		
			if($scope.typeofdays="1")
			$scope.exportDetailTitle='Zone Analysis for '+$scope.emp+" on "+$scope.dwelltitledate;
		else
			$scope.exportDetailTitle='Zone Analysis for '+$scope.emp+" from "+$scope.dwelltitledate;
		
			$scope.pdfHeader="Ei4.0 Analytics - User Time Analysis Chart";
			$scope.pdfDate=$scope.dwelltitledate;
		var container = document.getElementById('chart-area');
		var data = {
				series :chartdat
		};
		var options = {
				chart: {
					width: screenWidth * 0.80,
					height: screenHeight * 0.80,
					format: '1,000',
					title:{
						text: $scope.exportDetailTitle,
						align: "center"
					}
					
				},
				series: {
					showLabel: true, 
					zoomable: false,
					useLeafLabel: true
				},
				tooltip: {
					
				},
				legend: {
					align: 'right'
				},
				chartExportMenu: {
			        visible: false  // default is true.
			    }
		};
		var theme = {
				series: {
					colors:color ,
					borderColor: '#ffffff'

				}
		};

		// For apply theme

		tui.chart.registerTheme('myTheme', theme);
		options.theme = 'myTheme';

		tui.chart.treemapChart(container, data, options);
	}
	}
	$scope.loadzoneArrivalRatechart=function(){
		$scope.exportArrayData=[];	
		
		var zoneName='';
		var zoneKey='';
		$('#chart-area').empty(); 
		$scope.exportXLS = true;
		$scope.exportPDF = true;
		var series = []; 
		var series1 = [];
		var categories=[];
		var maxTime = 0;
		var startpos=0;
		var endpos=0;
		$scope.dataList.sort(GetSortOrder("date"));	
		$scope.norecords=false;
		$scope.zone= $scope.previousZoneArrival
		$scope.zoneKeyArrival=$scope.zone;
		$scope.getZoneFinalList($scope.zoneKeyArrival,  $scope.activestartdateArrival,  $scope.activeenddateArrival);	
		$scope.dateArray =[];
		
		
		for(var st=0;st<arr.length;st++){
			if(startpos==0){
				if(arr[st]== "09:00 AM"){
					if(st!=0 && st>4)
						startpos=st-4;
					else
						startpos = 0;
				}
			}
			if(endpos==0){
				if(arr[st]== "06:00 PM"){
					if(st!=96 && st<=92)
						endpos=st+4;
					else
						endpos = 94
				}
			}
		}
		for(var ct=startpos;ct<=endpos;ct++){
			if(arr[ct].includes("00")){
				categories.push(arr[ct])

			}
				else{
					categories.push(""); 

			}
		}
		
		for (var n = 0; n < $scope.dataList.length; n++) {
			$scope.dateArray.push( moment(new Date).subtract(n, 'days').format("MMM DD, YYYY"));
		}
		$scope.finalArrivalList=[];
		for (var i = 0; i < $scope.dataList.length; i++) {
			// if ($scope.FINAL_DAY_ARRAY.includes($scope.dataList[i].date))
				// {
			var dateObj = $scope.dataList[i];
			$scope.finalArrivalList[i]=dateObj;
				// }
		}
		for (var i = 0; i < $scope.finalArrivalList.length; i++) {
			var dateObj = $scope.finalArrivalList[i];
			var date = $scope.dateArray[i];
			if (dateObj != undefined && dateObj != null) {
				var zonedataObj=dateObj.zone;
				var chartXData = [];
				var obj = {};
				if(zonedataObj.length>0 && zonedataObj[0].zoneName!= undefined && zonedataObj[0].zoneName != null && zonedataObj[0].zoneName==$scope.zone){
					zoneName=zonedataObj[0].zoneName; 

					// if (zonedataObj[0].dWellTime.maximum > maxTime)
						// maxTime = zonedataObj[0].dWellTime.maximum;
					maxTime = 10
						
					for(var ct=startpos;ct<=endpos;ct++){

						var pos="TQ"+(ct+1);
						if(ct<9)// changed as 9 from 10 - to display TQ10
								// instead of TQ010
							pos="TQ0"+(ct+1);
						if(zonedataObj[0].tQ!=undefined&&zonedataObj[0].tQ!=null&&zonedataObj[0].tQ[pos]!=undefined&&zonedataObj[0].tQ[pos]!=null){
							chartXData.push(zonedataObj[0].tQ[pos])
							
						}
						else{
							
							chartXData.push("0")
						} 

					}
				
					series.push( { 
					'name': date,  
						'data': chartXData
					});
					
											
				}
			}
		}		
		
		var dataArray=[];
		for(i=0;i<series.length;i++){ 				
			dataArray.push(series[i].data);				
									
			}	
		// to display chart data in excel
		for(i=0;i<categories.length;i++){ 	
			 var obj={};
			 obj['Time']=categories[i];
		for(j=0;j<series.length;j++){				
					obj[series[j].name]=dataArray[j][i];					 
			
			}
		$scope.exportArrayData.push(obj);
		}
		 data = {
					categories: categories,
					series: series
			};
		$scope.generateDateTime();
		if(moment($scope.ArrivalStartDate).format("MMM DD, YYYY")==moment($scope.ArrivalEndDate).format("MMM DD, YYYY"))
			 $scope.arrivaltitledate=moment($scope.ArrivalStartDate).format("MMM DD, YYYY");
		else
			$scope.arrivaltitledate=moment($scope.ArrivalStartDate).format("MMM DD, YYYY")+" - "+moment($scope.ArrivalEndDate).format("MMM DD, YYYY");
		$scope.exportDetailTitle="";
		$scope.pdfHeader="";
		$scope.pdfDate="";
		
        if($scope.typeofdays == "1")
            $scope.exportDetailTitle='People Arrival Rate @ '+zoneName+' on '+$scope.arrivaltitledate;
        else
            $scope.exportDetailTitle='People Arrival Rate @ '+zoneName+' from '+$scope.arrivaltitledate;
         
			$scope.pdfHeader="EI4.0 Analytics - People Arrival Rate - Zone";
			$scope.pdfDate=$scope.arrivaltitledate;
			
		// $scope.chartTitleTraffic='Arrival Rate @ '+zoneName+' from
		// '+$scope.arrivaltitledate;
		$scope.filename="Arrival_Rate_"+zoneName;
		$scope.sheetname="People_Arrival_Rate";
		if(series.length>0){// to set border color for lines in line chart
			 dynamicColorArray =[];
				for(var i = 0; i<dataArray.length; i++){
					 dynamicColorArray.push('#'+(0x1000000+(Math.random())*0xffffff).toString(16).substr(1,6))
				}
				
				// to set X-asis label for
				
				var firstIndex = startpos;//arr.indexOf($scope.orgoperationalstarttime); 11Jan
				var lastIndex = endpos;//arr.indexOf($scope.orgoperationalendtime);		11Jan
				const labelForPlantTrafficchart = arr.slice(firstIndex, lastIndex);
				
			// to draw line chart
				 $('#axis-Test').remove();
				 $('#chartviewParent').append('<canvas id="axis-Test"  height="300" width="0"></canvas>');
				 $('#chart-Test').remove();
				 $('#chartview').append('<canvas id="chart-Test"  height="550" width="1200"></canvas>');
			   	 var plantTrafficCanvas = $('#chart-Test')
			   	 
			   	
			var plantTrafficChartTimeLabel = [];
			var dataForDisplay= [];
			for(var i = 0; i<data.series.length; i++){
				var	dataOptions = {
				   	    label: data.series[i].name,
				   	    data: data.series[i].data,
				   	    lineTension: 0,
				   	    fill: false,
				   	   borderColor: dynamicColorArray[i]
				   	  };
				dataForDisplay.push(dataOptions);
				   	
				   	plantTrafficChartTimeLabel = {
				   	  labels: labelForPlantTrafficchart,
				   	  datasets: dataForDisplay
				   	}
			}
		   	

		   	var chartOptions = {
		   	  legend: {
		   	    display: true,
		   	    position: 'top',
		   	    labels: {
		   	      boxWidth: 80,
		   	      fontColor: 'black'
		   	    }
		   	  }	
		   	};
		   	var ticksStyle = {
					fontColor: '#495057',
					fontStyle: 'bold'
				}
		   	var lineChart = new Chart(plantTrafficCanvas, {
		   	  type: 'line',
		   
		   	  data: plantTrafficChartTimeLabel,
		   	  options: {
		        /*
				 * scales: { y: { suggestedMin: 0, suggestedMax: 100 } },
				 */
		   		  
		        legend: {
			   	    display: true,
			   	    position: 'top',
			   	    labels: {
			   	      boxWidth: 80,
			   	      fontColor: 'black'
			   	    }
			   	  },
			   	scales: {
                    xAxes: [{
          display: true,
		  scaleLabel: {
        display: true,
        labelString: 'Time'
      },
          gridLines: {
            display: true
          },
          ticks: ticksStyle
        }],
                    yAxes: [{
          // display: false,
          gridLines: {
            display: true,
            lineWidth: '4px',
            color: 'rgba(0, 0, 0, .2)',
            zeroLineColor: 'transparent'
          },
		   scaleLabel: {
        display: true,
        labelString: '# of People'
      },
          ticks: $.extend({
            beginAtZero: true,
           
            stepSize: 1, //12Jan
            // Include a dollar sign in the ticks 11Jan
            callback: function (value) {
			value = value ;
              //if (value >= 1000) {
              //  value /= 1000
              //  value += 'k'
              //}

              //return '$' + value
              return value
            }
          }, ticksStyle)
        }]
                }

		    }
		   	});}else{
			$scope.norecords=true;
			$scope.no_data_text = "No Record found";
			$('#chart-area').empty();
			$scope.exportXLS = false;
			$scope.exportPDF = false;
		}
   	 
	}
	$scope.loadorgattendance=function(){
		
		$scope.exportArrayData=[];
		$scope.exportXLS = true;
		$scope.exportPDF = true;		
		$('#chart-area').empty(); 

		var series = []; 
		var employeeSeries = [];
		var visitorSeries = [];
		var totalSeries = [];
		var categories=[];
		var maxTime = 0;
		var startpos=0;
		var endpos=0;
		for(var st=0;st<arr.length;st++){
			if(startpos==0){
				if(arr[st]==$scope.orgoperationalstarttime){
					if(st!=0 && st>4)
						startpos=st-4;
					else
						startpos = 0;
				}
			}
			if(endpos==0){
				if(arr[st]==$scope.orgoperationalendtime){
					if(st!=96 && st<=92)
						endpos=st+4;
					else
						endpos = 94
				}
			}
		}
		for(var ct=startpos;ct<=endpos;ct++){
			if(arr[ct].includes("00"))
				categories.push(arr[ct])
				else
					categories.push(""); 
		}
		for (var i = 0; i < $scope.dataList.length; i++) {
			var dateObj = $scope.dataList[i].orgAttendance;
			if (dateObj != undefined && dateObj != null) {

				var total = [];
				var employee = []; 
				var visitor = [];

				var t,e,ve,vi="";

				for(var ct=startpos;ct<=endpos;ct++){

					var pos="TQ"+(ct+1);
					
					if(ct<9) // changed as 9 from 10 - to display TQ10
								// instead of TQ010
						pos="TQ0"+(ct+1);
					
					if(dateObj!=undefined&&dateObj!=null&&dateObj[pos]!=undefined&&dateObj[pos]!=null){

						if(dateObj[pos].tot!=undefined&&dateObj[pos].tot!=null){
							total.push(dateObj[pos].tot);
							
							if (dateObj[pos].tot > maxTime)
								maxTime = dateObj[pos].tot;
						}else
							total.push("0");
						
						if(dateObj[pos].user!=undefined&&dateObj[pos].user!=null){
							if(dateObj[pos].user.emp!=undefined&&dateObj[pos].user.emp!=null)
								employee.push(dateObj[pos].user.emp);							
							else
							employee.push("0");
							
							if(dateObj[pos].user.vis!=undefined&&dateObj[pos].user.vis!=null)
								visitor.push(dateObj[pos].user.vis);								
							else
								visitor.push("0"); 
							
						}else
						{
							employee.push("0"); 
							visitor.push("0");
							
						}
					}
					else{ 
						total.push("0");
						employee.push("0"); 
						visitor.push("0");						
						
					} 


				}
				employeeSeries.push(employee);
				visitorSeries.push(visitor);  
				totalSeries.push(total);
				
			}
		}
		for(var i=0; i<categories.length; i++){
		    $scope.exportArrayData.push({
		    	"Time": categories[i],
		    	"Employee": employee[i],
		    	"Visitor": visitor[i],		    	
		    	"Total": total[i]
		    });
		    
		}		
		
		
		$scope.exportDetailTitle="";
		$scope.pdfHeader="";
		$scope.pdfDate="";
		$scope.generateDateTime();
			$scope.exportDetailTitle="Plant"+' Occupancy - People for '+$('#idPFAEditStartDatePicker').val();
			$scope.pdfHeader="EI4.0 Analytics - Plant  Occupancy Chart";
			$scope.pdfDate=$('#idPFAEditStartDatePicker').val();
			
		// $scope.chartTitleTraffic="Plant"+' Analysis (People) for
		// '+$('#idPFAEditStartDatePicker').val();
		$scope.filename=$scope.org+' Attendance';
		$scope.sheetname=$scope.org+' Attendance';
		// to display plant analysis line chart
	  
	  	$('#axis-Test').remove();
		 $('#chartviewParent').append('<canvas id="axis-Test"  height="300" width="0"></canvas>');
		 $('#chart-Test').remove();
		 $('#chartview').append('<canvas id="chart-Test"  height="550" width="1200"></canvas>');
	   	 var plantOccupancyCanvas = $('#chart-Test')
	   	var data1 = {
	   	    label: "Employee",
	   	    data: employeeSeries[0],
	   	    lineTension: 0,
	   	    fill: false,
	   	    borderColor: '#FF6347'
	   	  };

	   	var data2 = {
	   	    label: "Visitor",
	   	    data: visitorSeries[0],
	   	    lineTension: 0,
	   	    fill: false,
	   	    borderColor: '#006400'
	   	  };
		var data3 = {
		   	    label: "Total",
		   	    data: totalSeries[0],
		   	    lineTension: 0,
		   	    fill: false,
		   	    borderColor: '#00008b'
		   	  };		
		
		// to set label for line chart x-axis
		
		
		var firstIndex =arr.indexOf($scope.orgoperationalstarttime);
		var lastIndex = arr.indexOf($scope.orgoperationalendtime);		
		const labelForPlantOccupancychart = arr.slice(firstIndex, lastIndex);
		
	   	var plantOccupancyData = {
	   	  labels: labelForPlantOccupancychart,
	   	  datasets: [data1, data2, data3]
	   	};

	   	var chartOptions = {
	   	  legend: {
	   	    display: true,
	   	    position: 'top',
	   	    labels: {
	   	      boxWidth: 80,
	   	      fontColor: 'black'
	   	    }
	   	  }
	   	};

	   	var lineChart = new Chart(plantOccupancyCanvas, {
	   	  type: 'line',
	   	  data: plantOccupancyData,
	   	  options: chartOptions
	   	});		

	}
	$scope.loadzoneattendance= function(){	
		
		$('#chart-area').empty(); 
		var categories=[];
		var colordata=["#bac1c4","#19b7e4","#66fe66","#fecc01" ,"#fd6600","#990302"];
		var color=[];
		var chartdat=[];
		var zoneData={};
		var zoneDataclone={};
		var ocuupancyRate=120;
		var maxTime = 0;
		var startpos=0;
		var endpos=0;
		var total=0;
		var employee=0;
		var visitor=0; 
		var pos=0;
		$scope.exportArrayData=[];
		for(var st=0;st<arr.length;st++){
			if(startpos==0){
				if(arr[st]==$scope.orgoperationalstarttime){
					if(st!=0 && st>4)
						startpos=st-4;
					else
						startpos = 0;
				}
			}
			if(endpos==0){
				if(arr[st]==$scope.orgoperationalendtime){
					if(st!=96 && st<=92)
						endpos=st+4;
					else
						endpos = 94
				}
			}
		}
		for(var ct=startpos;ct<=endpos;ct++){
			categories.push(arr[ct]);
		}
		// $scope.zoneList.sort(GetSortOrder("name"));
		for (var n = 0; n < $scope.zoneList.length; n++) {
			var zoneKey = $scope.zoneList[n].id;		
			// $scope.getZoneFinalList(zoneKey,$scope.activestartdateAttendance,
			// $scope.activeenddateAttendance);
			
			// if ($scope.FINAL_DAY_ARRAY.includes($scope.dataList[0].date)) {
			
			zoneData[$scope.zoneList[n].id] =  {
					'zoneID': $scope.zoneList[n].id,
					'zoneName':$scope.zoneList[n].name, 
					'value':0 
			};
		// }
		}
		if (Object.keys(zoneData).length == 0){
			$scope.exportXLS = false;
			$scope.exportPDF = false;
		$scope.norecords=true;
		$scope.no_data_text = "No Record found";
		$('#chart-area').empty();
		$scope.chartTitleZoneattendance='Floor Analysis (People) for  '+$('#idPFAEditStartDatePicker').val();
	}
else
	{
	$scope.exportXLS = true;	
	$scope.exportPDF = true;
		$scope.timeval=categories[0]; 
		$scope.loadData= function(datapos){
			$scope.exportArrayData=[];
			maxTime=0;
			if(startpos+datapos<9) // changed as 9 from 10 - to display TQ10
									// instead of TQ010
				pos="TQ0"+(startpos+datapos+1);
			else
				pos="TQ"+(startpos+datapos+1);

			for (var i = 0; i < $scope.dataList.length; i++) {
				var dateObj = $scope.dataList[i];
				zoneDataclone=zoneData;
				if (dateObj != undefined && dateObj != null) {
					var zoneObjList = dateObj.zone;
					var orgObjList = dateObj.orgAttendance;


					if (zoneObjList != undefined && zoneObjList != null && zoneObjList.length>0&&orgObjList != undefined && orgObjList != null) {
						zoneObjList.sort(GetSortOrder("zoneName"));
						if(orgObjList[pos]!=undefined&&orgObjList[pos]!=null){

							if(orgObjList[pos].tot!=undefined&&orgObjList[pos].tot!=null)
								total=orgObjList[pos].tot;
							else
								total=0;

							if(orgObjList[pos].user!=undefined&&orgObjList[pos].user!=null){
								if(orgObjList[pos].user.emp!=undefined&&orgObjList[pos].user.emp!=null)
									employee=orgObjList[pos].user.emp;
								else
									employee=0;
								if(orgObjList[pos].user.vis!=undefined&&orgObjList[pos].user.vis!=null)
									visitor=orgObjList[pos].user.vis;
								else
									visitor=0;
								
							}else
							{
								employee=0; 
								visitor=0;
							}
						}
						else{ 
							total=0;
							employee=0; 
							visitor=0;

						}
						for (var j = 0; j < zoneObjList.length; j++) {
							var zoneObj = zoneObjList[j];
							if (zoneObj.tQ[pos]!=undefined&&zoneObj.tQ[pos]!=null){
								if(zoneDataclone.hasOwnProperty(zoneObj.zoneID))
								{
									var listObj = zoneDataclone[zoneObj.zoneID];
									listObj.value=zoneObj.tQ[pos];  
									zoneDataclone[zoneObj.zoneID]=listObj;
									if (zoneObj.tQ[pos] > maxTime)
										maxTime = zoneObj.tQ[pos];
								} 
							}else{
								if(zoneDataclone.hasOwnProperty(zoneObj.zoneID))
								{
									var listObj = zoneDataclone[zoneObj.zoneID];
									listObj.value=0;  
									zoneDataclone[zoneObj.zoneID]=listObj;
									 
								} 
							}

						}
					}
				}
			}

			chartdat.length=0;
			color.length=0;
			for ( var key in zoneDataclone) {
				var listObj = zoneDataclone[key];
				if(zoneDataclone[key].value==0){
					listObj.value="No Value";
					zoneDataclone[key]=listObj;
				} 
				
				if(zoneDataclone[key].value=="No Value"){
					color.push(colordata[0]);  
				}else {
					var val=Math.round((zoneDataclone[key].value/maxTime)*100); 
					if(val>0&&val<20){ 
						color.push(colordata[1]);
					}else if(val>=20&&val<40){
						color.push(colordata[2]);
					}else	if(val>=40&&val<60){
						color.push(colordata[3]);
					}else	if(val>=60&&val<80){
						color.push(colordata[4]);
					}else	if(val>=80){
						color.push(colordata[5]);
					}
				}

				chartdat.push({
					'label':zoneDataclone[key].zoneName+"\n"+zoneDataclone[key].value,
					'value': 1,
				});
				
				$scope.exportArrayData.push({
					'Zone-Name':zoneDataclone[key].zoneName,
					'Attendance': zoneDataclone[key].value,
				});
			}

			$('#chart-area').empty(); 
			
			
			$scope.exportDetailTitle="";
			$scope.pdfHeader="";
			$scope.pdfDate="";
			$scope.generateDateTime();
				$scope.exportDetailTitle='Floor Analysis (People) for '+$('#idPFAEditStartDatePicker').val()+" @ "+$scope.timeval+" ["+" Employee "+employee+" Visitor "+visitor+" total "+total+" ]"
				$scope.pdfHeader="Analytics - Floor Analysis Chart";
				$scope.pdfDate=$('#idPFAEditStartDatePicker').val();
				
			// $scope.chartTitleTraffic='Floor Analysis (People) for
			// '+$('#idPFAEditStartDatePicker').val()+" @ "+$scope.timeval+" ["+"
			// Employee "+employee+" Visitor "+visitor+" total "+total+" ]"
			$scope.filename="Floor_Analysis";
			$scope.sheetname="Floor_Analysis";
			var container = document.getElementById('chart-area');
			var data = {
					series :chartdat
			};
			var options = {
					chart: {
						width: screenWidth * 0.80,
						height: screenHeight * 0.80,
						title: {
							text:
							'Floor Analysis (People) for '+$('#idPFAEditStartDatePicker').val()+" @ "+$scope.timeval+" ["+" Employee "+employee+" Visitor "+visitor+" total "+total+" ]",  
						format: '1,000',
						align:'center'
						}
					},
					series: {
						showLabel: true, 
						zoomable: false,
						useLeafLabel: true
					},
					tooltip: {

					},
					legend: {
						align: 'right'
					},
					chartExportMenu: {
				        visible: false  // default is true.
				    }
			};
			var theme = {
					series: {
						colors:color ,
						borderColor: '#ffffff'

					}
			};

			// For apply theme

			tui.chart.registerTheme('myTheme', theme);
			options.theme = 'myTheme';

			tui.chart.treemapChart(container, data, options);
		}
		$scope.loadData($scope.tqidval);
		// $scope.exportArrayData=[];
		/*$( "#slider-range-min" ).slider({
			range: "min",
			min: 0,
			max: categories.length-1,
			value:0,
			slide: function( event, ui ) {
				$scope.timeval=categories[ui.value];  
				$scope.loadData(ui.value);
				$scope.$apply();  
			}
		});*/
	}
	}
$scope.viewOption = function(){
		
	map.closePopup(); 
	$scope.iscolumnchartselected=false; 	
		
		if($scope.viewValue=="Heat Map"){
			$scope.map=false;
			if($scope.dataList!=undefined && $scope.dataList.length!=0) {
				$scope.norecords=false;

				if($scope.isEmployeedwelltimeClicked){
					$("#tagsSelectDiv").show();	
			$scope.viewValueDwell=$scope.viewValue; 
			$scope.emp=$scope.previousEmployee
				$scope.typeofday=true;		
				$scope.typeofdays=$scope.dateValueDwell;
				$('#startdate').val($scope.DwellStartDate);
				$('#enddate').val($scope.DwellEndDate);
				$scope.selectdate=false; 
				if($scope.typeofdays==0){
					$scope.customdates=true;  				
							$('#startdate').val($scope.DwellStartDate);	
							$('#enddate').val($scope.DwellEndDate);
				}else
				{$scope.customdates=false;}
					$scope.loaddwelHeatMap();
				}
				else if($scope.isZoneoccupancyClicked)
				{
					$scope.typeofday=true;							
					$scope.typeofdays=$scope.dateValueOccupancy;
					$('#startdate').val($scope.OccupancyStartDate);
					$('#enddate').val($scope.OccupancyEndDate);
					$scope.selectdate=false; 
					if($scope.typeofdays==0){
						$scope.customdates=true;  				
								$('#startdate').val($scope.OccupancyStartDate);	
								$('#enddate').val($scope.OccupancyEndDate);
					}else
					{$scope.customdates=false;}					
					
					$scope.loadoccupancyHeatMap();
				}else if ($scope.isproductanalysisClicked){ 
					$scope.productanAlysisMapType = $scope.viewValue;
					$scope.loadProductanalysisHeatMap()
				}
			} else {
				if($scope.isEmployeedwelltimeClicked){					
					
					$scope.viewValueDwell=$scope.viewValue; 
					$scope.emp=$scope.previousEmployee
					$scope.typeofday=true;		
					$scope.typeofdays=$scope.dateValueDwell;
					$('#startdate').val($scope.DwellStartDate);
					$('#enddate').val($scope.DwellEndDate);
					$scope.selectdate=false; 
					if($scope.typeofdays==0){
						$scope.customdates=true;  				
								$('#startdate').val($scope.DwellStartDate);	
								$('#enddate').val($scope.DwellEndDate);
					}else
					{
						$scope.customdates=false;
					}
					

					$scope.norecords = true;
					$scope.exportXLS = false;
					$scope.exportPDF = false;
					
					if($scope.empdwelldummyList==undefined){
						$scope.no_data_text = "Please Generate Report.";
						$scope.dropdowntype="Heat Map";
					}
					else{ 
						$scope.no_data_text = "No Record found";	
					}			
				
				$('#chart-area').empty();
				
			}else if($scope.isZoneoccupancyClicked){
					$scope.typeofday=true;							
					$scope.typeofdays=$scope.dateValueOccupancy;
					$('#startdate').val($scope.OccupancyStartDate);
					$('#enddate').val($scope.OccupancyEndDate);
					$scope.selectdate=false; 
					if($scope.typeofdays==0){
						$scope.customdates=true;  				
								$('#startdate').val($scope.OccupancyStartDate);	
								$('#enddate').val($scope.OccupancyEndDate);
					}else
					{$scope.customdates=false;}					
							
					if($scope.occupancydummyList==undefined){
						$scope.norecords = true;
						$scope.exportXLS = false;
						$scope.exportPDF = false;
						$scope.no_data_text = "Please Generate Report.";
						$scope.dropdowntype="Heat Map";
						}
						else{
							$scope.exportXLS = false;
							$scope.exportPDF = false;
							$scope.norecords=true;
							$scope.no_data_text = "No Record found";	
						}	
					$('#chart-area').empty();
				}else if ($scope.isproductanalysisClicked){ 
					$scope.typeofday=true;
					$scope.productanAlysisMapType = $scope.viewValue;
					$scope.exportXLS = false;
					$scope.exportPDF = false;
					$scope.norecords=true;
					if($scope.productAnalysisList==undefined) 
						$scope.no_data_text = "Please Generate Report.";	
					else
						$scope.no_data_text = "No Record found";	
					$('#chart-area').empty();
					
				}
			}
		}else if($scope.viewValue=="Column Chart"){
			$scope.iscolumnchartselected=true; 
			$scope.map=false; 
			if($scope.dataList!=undefined && $scope.dataList.length!=0 ){
			if($scope.isEmployeedwelltimeClicked){
				$scope.chartViewShow = true;
				$("#tagsSelectDiv").hide();		
					$scope.norecords=false;	
					$scope.viewValueDwell=$scope.viewValue; 
					
					$scope.emp=$scope.previousEmployee
					$scope.typeofday=true;		
					$scope.typeofdays=$scope.dateValueDwell;
					$('#startdate').val($scope.DwellStartDate);
					$('#enddate').val($scope.DwellEndDate);
					$scope.selectdate=false; 
					$scope.dwelltimeminities = $scope.previousDwellTimeGT
					if($scope.typeofdays==0){
						$scope.customdates=true;  				
								$('#startdate').val($scope.DwellStartDate);	
								$('#enddate').val($scope.DwellEndDate);
					}else
					{$scope.customdates=false;}
					
				$scope.loadColumnchart();					
					
				}
					
				else if($scope.isZoneoccupancyClicked){	
					$scope.norecords=false;
					$scope.typeofday=true;							
					$scope.typeofdays=$scope.dateValueOccupancy;
					$('#startdate').val($scope.OccupancyStartDate);
					$('#enddate').val($scope.OccupancyEndDate);
					$scope.selectdate=false; 
					if($scope.typeofdays==0){
						$scope.customdates=true;  				
								$('#startdate').val($scope.OccupancyStartDate);	
								$('#enddate').val($scope.OccupancyEndDate);
					}else
					{$scope.customdates=false;}					
					
					$scope.loadoccupancyColumnchart();
				} else if ($scope.isproductanalysisClicked){ 
					$scope.chartViewShow = false;
					$scope.productanAlysisMapType = $scope.viewValue;
					$scope.loadColumnChartProductAnalysisDisplay();
				}
			} else  {
				if($scope.isEmployeedwelltimeClicked){
					$scope.viewValueDwell=$scope.viewValue; 
					$scope.emp=$scope.previousEmployee
					$scope.typeofday=true;		
					$scope.typeofdays=$scope.dateValueDwell;
					$('#startdate').val($scope.DwellStartDate);
					$('#enddate').val($scope.DwellEndDate);
					$scope.selectdate=false; 
					if($scope.typeofdays==0){
						$scope.customdates=true;  				
								$('#startdate').val($scope.DwellStartDate);	
								$('#enddate').val($scope.DwellEndDate);
					}else
					{
						$scope.customdates=false; 
					}
					

					$scope.exportXLS = false;
					$scope.exportPDF = false;
					$scope.norecords = true;
					if($scope.empdwelldummyList==undefined){
						$scope.no_data_text = "Please Generate Report.";
						$scope.dropdowntype="Column Chart";
					}
					else{ 
						$scope.no_data_text = "No Record found";	
					}
					$('#chart-area').empty();
				}
					else if($scope.isZoneoccupancyClicked){
						$scope.typeofday=true;							
						$scope.typeofdays=$scope.dateValueOccupancy;
						$('#startdate').val($scope.OccupancyStartDate);
						$('#enddate').val($scope.OccupancyEndDate);
						$scope.selectdate=false; 
						if($scope.typeofdays==0){
							$scope.customdates=true;  				
									$('#startdate').val($scope.OccupancyStartDate);	
									$('#enddate').val($scope.OccupancyEndDate);
						}else
						{$scope.customdates=false;}					
						
								

						$scope.exportXLS = false;
						$scope.exportPDF = false;
						$scope.norecords = true;
						
						if($scope.occupancydummyList==undefined){
							$scope.no_data_text = "Please Generate Report.";
							$scope.dropdowntype="Column Chart";
						}
						else{ 
							$scope.no_data_text = "No Record found";	
						}	
						$('#chart-area').empty();
					}else if ($scope.isproductanalysisClicked){ 
						$scope.typeofday=true;
						$scope.productanAlysisMapType = $scope.viewValue;
						$scope.exportXLS = false;
						$scope.exportPDF = false;
						$scope.norecords=true;
						$scope.no_data_text = "No Record found";	
						$('#chart-area').empty();
						
					}
				}
		}else if($scope.viewValue=="Total Traffic"){
			$scope.map=false;
			if($scope.dataList!=undefined && $scope.dataList.length!=0){ 
				$scope.norecords=false;
				$scope.viewValueTraffic=$scope.viewValue; 					
				$scope.selectdate=true; 						
				$('#selectdate').val($scope.dateValueTraffic);		
				$scope.customdates=false;		
				$scope.typeofday=false;
				$scope.loadzoneTraffic();
			} else {		
								
				$scope.viewValueTraffic=$scope.viewValue; 				
				$scope.selectdate=true; 						
				$('#selectdate').val($scope.dateValueTraffic);		
				$scope.customdates=false;		
				$scope.typeofday=false;
				
				if($scope.trafficdummyList==undefined){
					$scope.exportXLS = false;
					$scope.exportPDF = false;
					$scope.norecords = true;
					$scope.no_data_text = "Please Select Date";
					$scope.dropdowntype="Total Traffic";
					}
					else{
						$scope.exportXLS = false;
						$scope.exportPDF = false;
						$scope.norecords=true;
						$scope.no_data_text = "No Record found";	
					}	
				$('#chart-area').empty();
					
			}
			
		}else if($scope.viewValue=="Peak Traffic"){
			$scope.map=false;
			if($scope.dataList!=undefined && $scope.dataList.length!=0){ 
				$scope.norecords=false;
				$scope.viewValueTraffic=$scope.viewValue; 								
				$scope.selectdate=true; 						
				$('#selectdate').val($scope.dateValueTraffic);		
				$scope.customdates=false;		
				$scope.typeofday=false;
				$scope.loadzoneTraffic();
			} else {
				$scope.viewValueTraffic=$scope.viewValue; 	
				$scope.selectdate=true; 						
				$('#selectdate').val($scope.dateValueTraffic);		
				$scope.customdates=false;		
				$scope.typeofday=false;

				$scope.exportXLS = false;
				$scope.exportPDF = false;
				$scope.norecords = true;
				
				if($scope.trafficdummyList==undefined){
					$scope.no_data_text = "Please Generate Report.";
					$scope.dropdowntype="Peak Traffic";
				}
				else{ 
					$scope.no_data_text = "No Record found";	
				}	
				$('#chart-area').empty();			
								
			}
		}else if($scope.viewValue=="Average Traffic"){
			$scope.map=false;
			if($scope.dataList!=undefined && $scope.dataList.length!=0){ 
				$scope.norecords=false;
				$scope.viewValueTraffic=$scope.viewValue; 				
				$scope.selectdate=true; 						
				$('#selectdate').val($scope.dateValueTraffic);		
				$scope.customdates=false;		
				$scope.typeofday=false;
				$scope.loadzoneTraffic();
			} else {	
				$scope.viewValueTraffic=$scope.viewValue; 				
				$scope.selectdate=true; 						
				$('#selectdate').val($scope.dateValueTraffic);		
				$scope.customdates=false;		
				$scope.typeofday=false;

				$scope.exportXLS = false;
				$scope.exportPDF = false;
				$scope.norecords = true;
				
				if($scope.trafficdummyList==undefined){ 
					$scope.no_data_text = "Please Generate Report.";
					$scope.dropdowntype="Average Traffic";
				}
				else{ 
					$scope.no_data_text = "No Record found";	
				}	
				$('#chart-area').empty();
			}
		}else if($scope.viewValue=="Classification Chart"){
			$scope.map=false;
			if($scope.dataList!=undefined && $scope.dataList.length!=0 ){
			if($scope.isEmployeedwelltimeClicked){	
				$("#tagsSelectDiv").show();	
					$scope.norecords=false;
					$scope.viewValueDwell=$scope.viewValue; 
					$scope.emp=$scope.previousEmployee
					$scope.typeofday=true;		
					$scope.typeofdays=$scope.dateValueDwell;
					$('#startdate').val($scope.DwellStartDate);
					$('#enddate').val($scope.DwellEndDate);
					$scope.selectdate=false; 
					if($scope.typeofdays==0){
						$scope.customdates=true;  				
								$('#startdate').val($scope.DwellStartDate);	
								$('#enddate').val($scope.DwellEndDate);
					}else
					{$scope.customdates=false;}
					$scope.loadClassificationchart();
				}				
				else if($scope.isZoneoccupancyClicked){
					$scope.norecords=false;
					$scope.typeofday=true;							
					$scope.typeofdays=$scope.dateValueOccupancy;
					$('#startdate').val($scope.OccupancyStartDate);
					$('#enddate').val($scope.OccupancyEndDate);
					$scope.selectdate=false; 
					if($scope.typeofdays==0){
						$scope.customdates=true;  				
								$('#startdate').val($scope.OccupancyStartDate);	
								$('#enddate').val($scope.OccupancyEndDate);
					}else
					{$scope.customdates=false;}					
					
					$scope.loadoccupancyClassificationchart();}
				else if($scope.isZonetrafficClicked)
					{$scope.norecords=false;
					$scope.viewValueTraffic=$scope.viewValue; 				
					$scope.selectdate=true; 						
					$('#selectdate').val($scope.dateValueTraffic);		
					$scope.customdates=false;		
					$scope.typeofday=false;
					$scope.loadzoneTraffic();}
			} else {
				if($scope.isEmployeedwelltimeClicked){
					$scope.viewValueDwell=$scope.viewValue; 
					$scope.emp=$scope.previousEmployee
					$scope.typeofday=true;		
					$scope.typeofdays=$scope.dateValueDwell;
					$('#startdate').val($scope.DwellStartDate);
					$('#enddate').val($scope.DwellEndDate);
					$scope.selectdate=false; 
					if($scope.typeofdays==0){
						$scope.customdates=true;  				
								$('#startdate').val($scope.DwellStartDate);	
								$('#enddate').val($scope.DwellEndDate);
					}else
					{$scope.customdates=false;}	
					
					if($scope.empdwelldummyList==undefined){
						$scope.exportXLS = false;
						$scope.exportPDF = false;
						$scope.norecords = true;
						$scope.no_data_text = "Please Select Employee and Date";
						$scope.dropdowntype="Classification Chart";
						}
						else{
							$scope.exportXLS = false;
							$scope.exportPDF = false;
							$scope.norecords=true;
							$scope.no_data_text = "No Record found";	
						}
					$('#chart-area').empty();
					
					}				
					else if($scope.isZoneoccupancyClicked)
					{	
						$scope.typeofday=true;							
						$scope.typeofdays=$scope.dateValueOccupancy;
						$('#startdate').val($scope.OccupancyStartDate);
						$('#enddate').val($scope.OccupancyEndDate);
						$scope.selectdate=false; 
						if($scope.typeofdays==0){
							$scope.customdates=true;  				
									$('#startdate').val($scope.OccupancyStartDate);	
									$('#enddate').val($scope.OccupancyEndDate);
						}else
						{$scope.customdates=false;}					

						$scope.exportXLS = false;
						$scope.exportPDF = false;
						$scope.norecords=true;
						if($scope.occupancydummyList==undefined){

							$scope.no_data_text = "Please Generate Report.";
							$scope.dropdowntype="Classification Chart";
							}
							else{
								$scope.no_data_text = "No Record found";	
							}	
						$('#chart-area').empty();
						}				
					else if($scope.isZonetrafficClicked)
					{	
						$scope.viewValueTraffic=$scope.viewValue; 				
						$scope.selectdate=true; 						
						$('#selectdate').val($scope.dateValueTraffic);		
						$scope.customdates=false;		
						$scope.typeofday=false;

						$scope.exportXLS = false;
						$scope.exportPDF = false;
						$scope.norecords = true;
						
						if($scope.trafficdummyList==undefined){
							$scope.no_data_text = "Please Generate Report.";
							$scope.dropdowntype="Classification Chart";
							}
							else{ 
								$scope.no_data_text = "No Record found";	
							}	
						$('#chart-area').empty();
						
						}				
				} 		
	
			
		}else if($scope.viewValue=="Line Chart"){
			// $scope.exportXLS = false;
			if($scope.zoneList!=undefined && $scope.zoneList.length!=0){
				$scope.norecords=false;
			}
			$scope.map=false;
			if($scope.dataList!=undefined && $scope.dataList.length!=0) {
				$scope.norecords=false;	
				$scope.typeofday=true;							
				$scope.typeofdays=$scope.dateValueArrival;
				$('#startdate').val($scope.ArrivalStartDate);
				$('#enddate').val($scope.ArrivalEndDate);
				$scope.selectdate=false; 
				if($scope.typeofdays==0){
					$scope.customdates=true;  				
							$('#startdate').val($scope.ArrivalStartDate);	
							$('#enddate').val($scope.ArrivalEndDate);
				}else
				{$scope.customdates=false;}		
				$scope.chartViewShow = true;
				$scope.loadzoneArrivalRatechart();
			} else {	
				$scope.typeofday=true;							
				$scope.typeofdays=$scope.dateValueArrival;
				$('#startdate').val($scope.ArrivalStartDate);
				$('#enddate').val($scope.ArrivalEndDate);
				$scope.selectdate=false; 
				if($scope.typeofdays==0){
					$scope.customdates=true;  				
							$('#startdate').val($scope.ArrivalStartDate);	
							$('#enddate').val($scope.ArrivalEndDate);
				}else
				{$scope.customdates=false;}	
				

				$scope.exportXLS = false;
				$scope.exportPDF = false;
				$scope.norecords = true;
				
				if($scope.arrivaldummyList==undefined){
					$scope.no_data_text = "Please Generate Report.";
					$scope.dropdowntype="Line Chart";
				}
				else{ 
					$scope.no_data_text = "No Record found";	
				}	
				$('#chart-area').empty();
				
			}
			
		if($scope.isMaterialTraceClicked){	
				$('#chart-area').empty(); 
				$scope.chartViewShow = false;
				// document.getElementById("genButn").style.display = "block";
				$scope.isMaterialTrace=false;
				$scope.typeofday=true;	
				$scope.materialData = [];						
				$scope.typeofdays=$scope.dateValueArrival;
				$('#startdate').val($scope.ArrivalStartDate);
				$('#enddate').val($scope.ArrivalEndDate);
				$scope.selectdate=false; 
				$scope.exportXLS = false;
				$scope.exportPDF = false;
				$scope.norecords=true;
				$scope.no_data_text = "No Record found";
		}
		}else if($scope.viewValue=="Organization Attendance"){
			$scope.chartViewShow = true;
			$scope.map=false;
			if($scope.dataList!=undefined && $scope.dataList.length!=0){ 
				$scope.norecords=false;
				$scope.viewValueAttendance=$scope.viewValue; 								
				$scope.selectdate=true; 						
				$('#idPFAEditStartDatePicker').val($scope.dateValueAttendance);					
				$scope.customdates=false;		
				$scope.typeofday=false;
				$scope.loadorgattendance();
			} else {	
				// $scope.dateValueAttendance=moment($('#idPFAEditStartDatePicker').val()).format("MMM
				// DD, YYYY");
				$scope.viewValueAttendance=$scope.viewValue; 								
				$scope.selectdate=true; 						
				$('#idPFAEditStartDatePicker').val($scope.dateValueAttendance);		
				$scope.customdates=false;		
				$scope.typeofday=false;

				$scope.exportXLS = false;
				$scope.exportPDF = false;
				$scope.norecords = true;
				
				if($scope.attendancedummyList==undefined){

					$scope.no_data_text = "Please Generate Report.";
					$scope.dropdowntype="Organization Attendance";
				}
				else{ 
					$scope.no_data_text = "No Record found";	
				}	
				$('#chart-area').empty();
					
			}
		}else if($scope.viewValue=="Zone Attendance"){
			$scope.map=false;
			if($scope.dataList!=undefined && $scope.dataList.length!=0) {
				$scope.norecords=false;
				$scope.viewValueAttendance=$scope.viewValue; 								
				$scope.selectdate=true; 						
				$('#idPFAEditStartDatePicker').val($scope.dateValueAttendance);		
				$scope.customdates=false;		
				$scope.typeofday=false;
				$scope.loadzoneattendance();
			} else {	
				
				$scope.viewValueAttendance=$scope.viewValue; 								
				$scope.selectdate=true; 						
				$('#idPFAEditStartDatePicker').val($scope.dateValueAttendance);		
				$scope.customdates=false;		
				$scope.typeofday=false;
				$scope.exportXLS = false;
				$scope.exportPDF = false;
				$scope.norecords = true;
				if($scope.attendancedummyList==undefined){
					$scope.no_data_text = "Please Generate Report.";
					$scope.dropdowntype="Zone Attendance";
				}
				else{ 
					$scope.no_data_text = "No Record found";	
				}	
				$('#chart-area').empty();
			}
		}else if($scope.viewValue=="Table View"){
			$scope.map=false;		
			$scope.chartViewShow = false;
			if($scope.dataList!=undefined && $scope.dataList.length!=0 ){
			if($scope.isEmployeedwelltimeClicked){	
				$("#tagsSelectDiv").hide();	
					$scope.norecords=false;
					$scope.viewValueDwell=$scope.viewValue; 
					$scope.emp=$scope.previousEmployee
					$scope.typeofday=true;		
					$scope.typeofdays=$scope.dateValueDwell;
					$('#startdate').val($scope.DwellStartDate);
					$('#enddate').val($scope.DwellEndDate);
					$scope.selectdate=false; 
					if($scope.typeofdays==0){
						$scope.customdates=true;  				
								$('#startdate').val($scope.DwellStartDate);	
								$('#enddate').val($scope.DwellEndDate);
					}else
					{$scope.customdates=false;}					
					$scope.loadDwellTableView();
				}else if ($scope.isproductanalysisClicked) 
                {
					$scope.typeofday=true;
					$scope.productanAlysisMapType = $scope.viewValue;
                    $scope.norecords=false;
                    $scope.isMaterialTrace=false;
                    $scope.pdfHeader="Zone Flow Analytics - Table View";
					$scope.pdfDate= $scope.dwelltitledate;
					$scope.filename = "ZFA_Zone Flow Analytics";
					$scope.sheetname = "Zone Flow Analytics";

                    
					$scope.exportArrayData = [];

                    $scope.exportXLS = true;
                    $scope.exportPDF = false;

                    $scope.productanAlysisList = []; 
                    $('#chart-area').empty();
                    $scope.selectedZone = 'All';
                    
                    for (var n = 0; n < $scope.zoneList.length; n++) {
                        var zoneName = $scope.zoneList[n].name;            
                        var productanalysisData = $scope.getProductAnalysisData(zoneName);
                        if(productanalysisData!=null)
                        {
                            if($scope.zoneList[n].name!='Corporate Office')
                            {
                                $scope.productanAlysisList.push( {
                                    'zoneID': $scope.zoneList[n].id,
                                    'zoneName':$scope.zoneList[n].name,
                                    'incoming': productanalysisData.incoming, 
                                    'outgoing':productanalysisData.outgoing, 
                                    'waiting': productanalysisData.waiting  
                                });
									$scope.exportArrayData.push( {
                                    'zoneID': $scope.zoneList[n].id,
                                    'zoneName':$scope.zoneList[n].name,
                                    'incoming': productanalysisData.incoming, 
                                    'outgoing':productanalysisData.outgoing, 
                                    'waiting': productanalysisData.waiting  
                                });
                            }
                            
                        }
                    }
                    
                    $scope.totalItems = $scope.productanAlysisList.length;
                    $scope.currentPage = 1;
                    $scope.getTotalPages();
                    $scope.productanFilterAlysisList = [];
                    
                    if ($scope.productanAlysisList.length > $scope.numPerPage)
                        $scope.productanFilterAlysisList =$scope.productanAlysisList
                                .slice(0, $scope.numPerPage);
                    else
                        $scope.productanFilterAlysisList = $scope.productanAlysisList;   
                     
                }
			}
			if($scope.isMaterialTraceClicked){
				$scope.isDateSelected =true;
				$scope.isMaterialTrace=true;
				$scope.typeofday=false;	
				$scope.chartViewShow = true;
				 $('#axis-Test').remove();
				 $('#chartviewParent').append('<canvas id="axis-Test"  height="0" width="0"></canvas>');
				 $('#chart-Test').remove();
				 $('#chartview').append('<canvas id="chart-Test"  height="0" width="0"></canvas>');
				// document.getElementById("genButn").style.display = "none";
				 $rootScope.showloading('#loadingBar');
				$http({
								method : 'GET',
								url : './demo/workOrderTable',
								headers : {
									'orgName' : $scope.org
								}
							}).then( 
						function(response) {
					 $rootScope.hideloading('#loadingBar');
						if (response.data.status == "error") {						
						if (response.data.message == "Session timed-out or Invalid") {
							// alert(response.data.message + ". Please logout
							// and login again");
							$.alert({
			                    title: $rootScope.MSGBOX_TITLE,
			                    content: response.data.message + ". Please logout and login again",
			                    icon: 'fa fa-info-circle',
			                     });
							}
							else{
							// alert(response.data.message);
							 $.alert({
				                    title: $rootScope.MSGBOX_TITLE,
				                    content: response.data.message,
				                    icon: 'fa fa-info-circle',
				                     });
							}						
						return;
					} 
						if (response != null && response.data != null
								&& response.data != "BAD_REQUEST") {	
							
							 $scope.materialList = response.data;
				     		 $scope.materialListVal=[];
                    $scope.pdfHeader="Work Order Flow Analytics - Table View";
                    $scope.pdfDate= $scope.dwelltitledate;
                    $scope.filename = "WOFA_Work Order Flow Analytics";
                    $scope.sheetname =  "Work Order Flow Analytics";
                    $scope.exportDetailTitle = "Work Order Flow Analytics - Table View";
					$scope.exportArrayData = [];

                    $scope.exportXLS = true;
                    $scope.exportPDF = false; 
					$scope.norecords = false;
						for(var i=0; i<$scope.materialList.length;i++){
							var ptimeval=0;
							var timein;
							var timeout;
							for(var j=0; j<$scope.materialList[i].production.length;j++){
								ptimeval = ptimeval+parseInt($scope.materialList[i].production[j].ptime);
							}
							var ptime="00:00:"+ptimeval+"";
							timein = ($scope.materialList[i].timein);
							timeout = ($scope.materialList[i].timeout);
							zone = $scope.materialList[i].production.length;
							$scope.materialListVal.push({"name":$scope.materialList[i].name,"timein":timein,"timeout":timeout,"ptime":ptime})
							$scope.exportArrayData.push({"name":$scope.materialList[i].name,"timein":timein,"timeout":timeout,"ptime":ptime})
	 					    }
								}
								})
									
			}
			
		}else if($scope.viewValue=="Constraint Analytics"){
			
			var zoneList =[];
		    var zoneData=[];		   
				$scope.exportDetailTitle=""
				$scope.exportXLS = false;
                $scope.exportPDF = true; 
                $scope.customdates=false;		
        		$scope.typeofday=false;
        		$scope.map=false;
    			$scope.norecords=false; 
    			$scope.exportDetailTitle="Constraint Analytics on " + moment(new Date()).format("MMM DD, YYYY");
    			$scope.filename = "Constraint_Analytics"	
    			$scope.chartViewShow = true;  
    			 $('#axis-Test').remove();
				 $('#chartviewParent').append('<canvas id="axis-Test"  height="300" width="0"></canvas>');
				 $('#chart-Test').remove();
				 $('#chartview').append('<canvas id="chart-Test"  height="350" width="900"></canvas>');
    			var ticksStyle = {
    					fontColor: '#495057',
    					fontStyle: 'bold'
    				}
    				var mode = 'index'
    				var intersect = true	
    				var zoneList = [
    					'Tool Crib',
    					'Midship Sub Assy',
    					'Resistance Welding 1 (RW1)',
    					'Manufacturing support 2',
    					'Large Valve Assembly 2(LA2)',
    					'Pump Test',
    					'Large Valve Assembly 3(LA3)',
    					'Fixture Storage 1',
    					'Large Valve Assembly 1(LA1)',
    					'Horizontal Mill 2',
    					'Inspect',
    					'Hydrant Valve Warehouse',
    					'Resistance Welding 2 (RW2)',
    					'Oil Storage',
    					'Exper Shop',
    					'Pump Parts Stores',
    					'Shipping and Receiving',
    					'Future Assy Cells',
    					'Fixture Storage 2',
    					'Horizontal Mill 1(HM1)',
    					'Hydrant and Valve Ops',
    					'Large Bonnet Machining',
    					'Maintenance',
    					'Midship Pump Assy',
    					'New Workcells',
    					'Chip Storage',
    					'Future Assy',
    					'Small Pump Assembly',
    					'Pump Paint and Crate',
    					'Manufacturing support 1',
    					'Large Valve Machining(LV1)',
    					'Static Test'
    					  ]
    					  var zoneData=[
    					'6.25',	'6.0', '5.85', '5.5',  '5.25',
    					'5.0',	'4.5', '4.27', '4.10', '4.0',
    					'1.83',	'1.73','1.67', '1.5',  '1.33',
    					'1.30',	'1.25','1.12', '1.0',  '1.0',
    					'0.75',	'0.75','0.62', '0.6',  '0.55',
    					'0.49',	'0.41','0.38', '0.33', '0.27',
    					'0.20',	'0.1' ]
    					var zoneBarColor=[
    					'#007bff','#007bff','#007bff',	'#007bff','#007bff','#007bff',
    					'#007bff','#007bff','#007bff',	'#007bff','#007bff','#007bff',
    					'#007bff','#007bff','#007bff',	'#007bff','#007bff','#007bff',
    					'#007bff','#007bff','#007bff',	'#007bff','#007bff','#007bff',
    					'#007bff','#007bff','#007bff',	'#007bff','#007bff','#007bff',
    					'#007bff','#007bff' ]
    				   function generateLabels() {
    			        var chartLabels = [];
    			        //for (x = 1; x < zoneList.length+1; x++) {
    						chartLabels.push(zoneList[0]);
    			        //}
    			        return chartLabels;
    			    }

    			    function generateData() {
    			        var chartData = [];
    			       //for (x = 1; x < zoneList.length+1; x++) {
    			            chartData.push(zoneData[0]);
    			        //}
    			        return chartData;
    			    }

    			    function addData(numData, chart) {
    			        for (var i = 1; i < zoneList.length+1; i++) {
    			            chart.data.datasets[0].data.push(zoneData[i]);
    			            chart.data.labels.push(zoneList[i]);
    			            var newwidth = $('.chartAreaWrapper2').width() + 40;
    			            $('.chartAreaWrapper2').width(newwidth);
    			        }
    			    }

    			    var chartData = {
    			            labels: generateLabels(),
    			            datasets: [{
    			                label: "Days",
    			                data: generateData(),
    			    			backgroundColor: zoneBarColor
    			              //borderColor: '#007bff'
    			    			
    			    			
    			            }]
    			        };				  
    			         var rectangleSet = false;
    					  var $constraintChartInHtml = $('#chart-Test')
    					  // eslint-disable-next-line no-unused-vars
    					  var constraintChart = new Chart($constraintChartInHtml, {
    				            type: 'bar',
    				            data: chartData,
    				            maintainAspectRatio: false,
    				            responsive: true,
    				            options: {
    				                tooltips: {
    				                    titleFontSize: 0,
    				                    titleMarginBottom: 0,
    				                    bodyFontSize: 12
    				                },
    				                legend: {
    				                    display: true,
    				                     position: 'right'
    				                },
    				                scales: {
    				                    xAxes: [{
    				          display: true,
    						  scaleLabel: {
    				        display: true,
    				        labelString: 'Zone'
    				      },
    				          gridLines: {
    				            display: true
    				          },
    				          ticks: ticksStyle
    				        }],
    				                    yAxes: [{
    				          // display: false,
    				          gridLines: {
    				            display: true,
    				            lineWidth: '4px',
    				            color: 'rgba(0, 0, 0, .2)',
    				            zeroLineColor: 'transparent'
    				          },
    						   scaleLabel: {
    				        display: true,
    				        labelString: 'Days'
    				      },
    				          ticks: $.extend({
    				            beginAtZero: true,

    				            // Include a dollar sign in the ticks 11Jan
    				            callback: function (value) {
    							value = value ;
    				              //if (value >= 1000) {
    				              //  value /= 1000
    				              //  value += 'k'
    				              //}

    				              //return '$' + value
    				              return value
    				            }
    				          }, ticksStyle)
    				        }]
    				                },
    				                animation: {
    				                    onComplete: function () {
    				                        if (!rectangleSet) {
    				                            var scale = window.devicePixelRatio;                       

    				                            var sourceCanvas = constraintChart.canvas;
    				                            var copyWidth = constraintChart.scales['y-axis-0'].width - 10;
    				                            var copyHeight = constraintChart.scales['y-axis-0'].height + constraintChart.scales['y-axis-0'].top + 10;

    				                            var targetCtx = document.getElementById("axis-Test").getContext("2d");

    				                            targetCtx.scale(scale, scale);
    				                            targetCtx.canvas.width = copyWidth * scale;
    				                            targetCtx.canvas.height = copyHeight * scale;

    				                            targetCtx.canvas.style.width = `${copyWidth}px`;
    				                            targetCtx.canvas.style.height = `${copyHeight}px`;
    				                            targetCtx.drawImage(sourceCanvas, 0, 0, copyWidth * scale, copyHeight * scale, 0, 0, copyWidth * scale, copyHeight * scale);

    				                            var sourceCtx = sourceCanvas.getContext('2d');

    				                            // Normalize coordinate system to use css pixels.

    				                            sourceCtx.clearRect(0, 0, copyWidth * scale, copyHeight * scale);
    				                            rectangleSet = true;
    				                        }
    				                    },
    				                    onProgress: function () {
    				                        if (rectangleSet === true) {
    				                            var copyWidth = constraintChart.scales['y-axis-0'].width;
    				                            var copyHeight = constraintChart.scales['y-axis-0'].height + constraintChart.scales['y-axis-0'].top + 10;

    				                            var sourceCtx = constraintChart.canvas.getContext('2d');
    				                            sourceCtx.clearRect(0, 0, copyWidth, copyHeight);
    				                        }
    				                    }
    				                }
    				            }
    				        });
    				        addData(zoneList.length-1, constraintChart);
    					//change the color of the bar
    					 var dataset = constraintChart.data.datasets[0];
    					    for(var i=0;i<dataset.data.length;i++){
    					       var color="green";
    					       //You can check for bars[i].value and put your conditions here
    						   if(dataset.data[i] <=1){
    					       	dataset.backgroundColor[i] = "green";
    					       }
    						   else if(dataset.data[i]<=2){
    					       	dataset.backgroundColor[i] = "yellow"
    					       }
    						   else
    							   dataset.backgroundColor[i] = "red"
    					    }
    					    constraintChart.update(); //update the chart  
			

		
			
			
		}else if($scope.viewValue=="Current Inventory"){
			
			var zoneList =[];
		    var zoneDollarData=[];		   
				$scope.exportDetailTitle=""
				 $scope.exportXLS = false;
                $scope.exportPDF = true; 
                $scope.customdates=false;		
        		$scope.typeofday=false;
        		 $scope.chartViewShow=false;	
			var zoneList = [
				'Tool Crib','Midship Sub Assy','Resistance Welding 1 (RW1)','Manufacturing support 2',
				'Large Valve Assembly 2(LA2)',	'Pump Test','Large Valve Assembly 3(LA3)',	'Fixture Storage 1',
				'Large Valve Assembly 1(LA1)',	'Horizontal Mill 2','Inspect',	'Hydrant Valve Warehouse',
				'Resistance Welding 2 (RW2)','Oil Storage',	'Exper Shop','Pump Parts Stores','Shipping and Receiving',
				'Future Assy Cells','Fixture Storage 2','Horizontal Mill 1(HM1)','Hydrant and Valve Ops','Large Bonnet Machining',
				'Maintenance',	'Midship Pump Assy','New Workcells','Chip Storage',	'Future Assy',	'Small Pump Assembly',
				'Pump Paint and Crate',		'Manufacturing support 1',	'Large Valve Machining(LV1)',	'Static Test' ]
			  var zoneDollarData=[
				  '6000', '5900', '5720', '5600','5450',  '5200', '4950', '4780', '4600','4300', '4150', '3000', '2980', '2700','2550',
				  '2400', '2250', '2150', '2000','1940',  '1820', '1700', '1600', '1450','1200', '1100', '950',   '800',  '600', '400',  '300',  '200' ]
			$scope.totalInventryValue = 0
			for(var i = 0; i < zoneDollarData.length; i++)
			{
				$scope.totalInventryValue = $scope.totalInventryValue + parseInt(zoneDollarData[i]);
			}
//11Jan		
			$scope.filename = "Current_Inventory"	
			$scope.totalInventry = "Total Inventory : " + "$"+ $scope.totalInventryValue.toString().replace(/\B(?<!\.\d*)(?=(\d{3})+(?!\d))/g, ","); 
				$scope.exportDetailTitle="Current Inventory $ on " + moment(new Date()).format("MMM DD, YYYY");
			$scope.map=false;
			$scope.norecords=false;
			var ticksStyle = {
					fontColor: '#495057',
					fontStyle: 'bold'
				}
				var mode = 'index'
				var intersect = true	
				var zoneList = [
					'Tool Crib',
					'Midship Sub Assy',
					'Resistance Welding 1 (RW1)',
					'Manufacturing support 2',
					'Large Valve Assembly 2(LA2)',
					'Pump Test',
					'Large Valve Assembly 3(LA3)',
					'Fixture Storage 1',
					'Large Valve Assembly 1(LA1)',
					'Horizontal Mill 2',
					'Inspect',
					'Hydrant Valve Warehouse',
					'Resistance Welding 2 (RW2)',
					'Oil Storage',
					'Exper Shop',
					'Pump Parts Stores',
					'Shipping and Receiving',
					'Future Assy Cells',
					'Fixture Storage 2',
					'Horizontal Mill 1(HM1)',
					'Hydrant and Valve Ops',
					'Large Bonnet Machining',
					'Maintenance',
					'Midship Pump Assy',
					'New Workcells',
					'Chip Storage',
					'Future Assy',
					'Small Pump Assembly',
					'Pump Paint and Crate',
					'Manufacturing support 1',
					'Large Valve Machining(LV1)',
					'Static Test'
					  ]
				  var zoneDollarData=[
					  '6000', '5900', '5720', '5600','5450',
					  '5200', '4950', '4780', '4600','4300',
					  '4150', '3000', '2980', '2700','2550',
					  '2400', '2250', '2150', '2000','1940',
					  '1820', '1700', '1600', '1450','1200',
					  '1100', '950',   '800',  '600', '400',
					  '300',  '200' ]
			 $('#idChartAgingCanvas').remove();
			 $('#chartviewAgingchart').append('<canvas id="idChartAgingCanvas"  height="550px" width="1200px"></canvas>');
				  var $inventoryChart = $('#idChartAgingCanvas')
				  // eslint-disable-next-line no-unused-vars
				  
				  // eslint-disable-next-line no-unused-vars
				  var dollarChart = new Chart($inventoryChart, {
				    type: 'bar',
				    data: {
				      labels: zoneList,
				      datasets: [
				        {
							
				          backgroundColor: '#007bff',
				          borderColor: '#007bff',
				          data: zoneDollarData
				        }
				      ]
				    },
				    options: {
				      maintainAspectRatio: false,
					  responsive: true,
				      tooltips: {
				        mode: mode,
				        intersect: intersect
				      },
				      hover: {
				        mode: mode,
				        intersect: intersect
				      },
					  "animation": {
				      "duration": 1
				    },
				      legend: {
				        display: false,
				     },
				      scales: {
				        yAxes: [{
				          // display: false,
				          gridLines: {
				            display: true,
				            lineWidth: '4px',
				            color: 'rgba(0, 0, 0, .2)',
				            zeroLineColor: 'transparent'
				          },
						   scaleLabel: {
				        display: false,
				        labelString: 'Days'
				      },
				          ticks: $.extend({
				            beginAtZero: true,

				            // Include a dollar sign in the ticks
				            callback: function (value) {
				              //if (value >= 1000) {
				              //  value /= 1000
				              //  value += 'k'
				              //}
				            if (parseInt(value) >= 1000) {
				                return '$' + value.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
				            } else {
				                return '$' + value;
				            }
				            // return '$' + value
				              ///return value
				            }
				          }, ticksStyle)
				        }],
				        xAxes: [{
				          display: true,
						  scaleLabel: {
				        display: true,
				        labelString: 'Zone'
				      },
				          gridLines: {
				            display: true
				          },
				          ticks: ticksStyle
				        }]
				      }
				    }
				  })
			
		}else if($scope.viewValue=="Aging Chart"){
			
			 $scope.exportXLS = false;
            $scope.exportPDF = true; 
            $scope.customdates = false;		
    		$scope.typeofday = false;     		
    		$scope.map=false;
			$scope.norecords=false;			
			$scope.exportDetailTitle=""
			$scope.exportDetailTitle="Aging Chart on " + moment(new Date()).format("MMM DD, YYYY");
			$scope.filename = "Aging_Chart"
			var agingZoneList1 = AgingMoreThan2DaysData1 = Aging2DaysData1 = Aging1DayData1= [];		     
			var ticksStyle = {
					fontColor: '#495057',
					fontStyle: 'bold'
				}
				var mode = 'index'
				var intersect = true	
				var agingZoneList = [
					'Large Valve Assembly 3(LA3)',
					'Shipping and Receiving',
					'Midship Pump Assy',
					'Future Assy Cells',
					'Fixture Storage 1',
					'Tool Crib',
					'Large Valve Assembly 1(LA1)',
					'Midship Sub Assy',
					'Resistance Welding 1 (RW1)',
					'Fixture Storage 2',
					'Manufacturing support 2',
					'Future Assy',
					'Horizontal Mill 1(HM1)',
					'Horizontal Mill 2',
					'Inspect',
					'Hydrant Valve Warehouse',
					'New Workcells',
					'Small Pump Assembly',
					'Hydrant and Valve Ops',
					'Large Bonnet Machining',
					'Resistance Welding 2 (RW2)',
					'Pump Paint and Crate',
					'Oil Storage',
					'Maintenance',
					'Large Valve Assembly 2(LA2)',
					'Manufacturing support 1',
					'Static Test',
					'Pump Test',
					'Chip Storage',
					'Exper Shop',
					'Large Valve Machining(LV1)',
					'Pump Parts Stores'
					  ] 
					var Aging1DayData=[
					'4',
					'3',
					'5',
					'2',
					'6',
					'3',
					'4',
					'2',
					'5',
					'2',
					'6',
					'4',
					'2',
					'3',
					'1',
					'4',
					'3',
					'6',
					'2',
					'5',
					'1',
					'4',
					'3',
					'2',
					'5',
					'3',
					'5',
					'2',
					'1',
					'4',
					'3',
					'1' ]

					var Aging2DaysData=[
					'8',
					'10',
					'7',
					'13',
					'8',
					'15',
					'18',
					'13',
					'9',
					'14',
					'13',
					'11',
					'15',
					'10',
					'9',
					'16',
					'14',
					'8',
					'17',
					'12',
					'16',
					'13',
					'12',
					'10',
					'11',
					'15',
					'9',
					'13',
					'10',
					'8',
					'11',
					'9' ]

					var AgingMoreThan2DaysData=[
					'98',
					'96',
					'95',
					'93',
					'91',
					'89',
					'88',
					'85',
					'82',
					'79',
					'78',
					'75',
					'72',
					'70',
					'68',
					'66',
					'63',
					'61',
					'59',
					'57',
					'54',
					'51',
					'50',
					'47',
					'45',
					'44',
					'41',
					'39',
					'37',
					'35',
					'31',
					'29' ]
			/*var agingZoneListVAriable = AgingMoreThan2DaysDataVAriable = Aging2DaysDataVAriable = Aging1DayDataVAriable = "";
			if($scope.selectedZone == "All"){
				 agingZoneList1 = agingZoneList;	
				 AgingMoreThan2DaysData1 = 	AgingMoreThan2DaysData;      
		         Aging2DaysData1 = Aging2DaysData;	       
		         Aging1DayData1 = Aging1DayData;
			}else{
				for(var i=0; i<agingZoneList.length; i++){
					if($scope.selectedZone == agingZoneList[i]){
						
						agingZoneListVAriable = agingZoneList[i]);	
						AgingMoreThan2DaysDataVAriable = AgingMoreThan2DaysData[i]);	      
						Aging2DaysDataVAriable = Aging2DaysData[i]);		       
						Aging1DayDataVAriable = Aging1DayData[i]);	
					}
				}
			agingZoneList1.push(agingZoneListVAriable);	
				 AgingMoreThan2DaysData1.push(AgingMoreThan2DaysDataVAriable);	      
		         Aging2DaysData1.push(Aging2DaysDataVAriable);		       
		         Aging1DayData1.push(Aging1DayDataVAriable);	
			}*/
			 $('#idChartAgingCanvas').remove();
			 $('#chartviewAgingchart').append('<canvas id="idChartAgingCanvas"  height="550px" width="1200px"></canvas>');
					  var $AgingChartInHtml = $('#idChartAgingCanvas')
					  // eslint-disable-next-line no-unused-vars
					  
					  // eslint-disable-next-line no-unused-vars
					  var AgingChart = new Chart($AgingChartInHtml, {
					    type: 'bar',
					    data: {
					      labels: agingZoneList,
					      datasets: [
							        {
										label: '> 2 Days',
							          backgroundColor: '#6958db',
							          borderColor: '#6958db',
							          data: AgingMoreThan2DaysData
							        },
							        {
										label: '2 Days',
							          backgroundColor: 'green',
							          borderColor: 'green',
							          data: Aging2DaysData
							        },
							        {
										label: 'Today',
							          backgroundColor: '#007bff',
							          borderColor: '#007bff',
							          data: Aging1DayData
							        }
							      ]
					    },
					    options: {
					      maintainAspectRatio: false,
						  responsive: true,
					      tooltips: {
					        mode: mode,
					        intersect: intersect
					      },
					      hover: {
					        mode: mode,
					        intersect: intersect
					      },
					      legend: {
					        display: true,
							position: 'top'
					     },
						 //11Jan
					      scales: {
					        yAxes: [{
					          // display: false,
					          gridLines: {
					            display: true,
					            lineWidth: '4px',
					            color: 'rgba(0, 0, 0, .2)',
					            zeroLineColor: 'transparent'
					          },
							   scaleLabel: {
					        display: true,
					        labelString: '# Of Work Order'
					      },
					          ticks: $.extend({
					            beginAtZero: true,

					            // Include a dollar sign in the ticks
					            callback: function (value) {
					              //if (value >= 1000) {
					              //  value /= 1000
					              //  value += 'k'
					              //}

					              return value
					              ///return value
					            }
					          }, ticksStyle)
					        }],
					        xAxes: [{
					          display: true,
							  scaleLabel: {
					        display: true,
					        labelString: 'Zone'
					      },
					          gridLines: {
					            display: true
					          },
					          ticks: ticksStyle
					        }]
					      }
					    }
					  })  
				
			   
			    	var dataSortIndex = 0;
			    	var otherIndex1,otherIndex2;
			    	if ($scope.varComboTypeofdaysAging == 1) {
			    		
			    		dataSortIndex = 2;
			    		otherIndex1=0;
			    		otherIndex2=1;
			    		
			    	}else if ($scope.varComboTypeofdaysAging == 2){
			    		
			    		dataSortIndex = 1;
			    		otherIndex1=0;
			    		otherIndex2=2;
			    	}else {
			    		dataSortIndex = 0
			    		otherIndex1=1;
			    		otherIndex2=2;
			    		
			    	}
			    	
			    	
			    	//Sorting data and label based on sorting type
			    	var dataset = AgingChart.data.datasets[dataSortIndex];
			    	var dataset1 = AgingChart.data.datasets[otherIndex1];
			    	var dataset2 = AgingChart.data.datasets[otherIndex2];
			    	for(var i = 0; i < dataset.data.length; i++){
			    	     
			    		   // Last i elements are already in place  
			    		   for(var j = 0; j < ( dataset.data.length - i -1 ); j++){
			    		       
			    		     // Checking if the item at present iteration 
			    		     // is greater than the next iteration
			    		     if(parseInt(dataset.data[j]) < parseInt(dataset.data[j+1])){
			    		         
			    		       // If the condition is true then swap them
			    		       var temp = dataset.data[j];
			    		       var tempOtherValue1 = dataset1.data[j];
			    		       var tempOtherValue2 = dataset2.data[j];
			    		       var tmpLabel = AgingChart.data.labels[j];
			    		       
			    		       dataset.data[j] = dataset.data[j + 1];
			    		       dataset1.data[j] = dataset1.data[j + 1];
			    		       dataset2.data[j] = dataset2.data[j + 1];
			    		       
			    		       AgingChart.data.labels[j]=AgingChart.data.labels[j+1]
			    		       
			    		       dataset.data[j+1] = temp;
			    		       dataset1.data[j+1] = tempOtherValue1;
			    		       dataset2.data[j+1] = tempOtherValue2;

			    		       AgingChart.data.labels[j+1] = tmpLabel;
			    		     }
			    		   }
			    		 }
			    	AgingChart.update(); //update the chart
			   
		}
		else{		
			$scope.norecords=false;
			$scope.chartViewShow=false;			
			$scope.loadMap();
		}
		
	}
		
		
	$scope.workOrderChart= function(){
			$('#chart-area').empty();
				var startpos=0;
			var endpos=0; 
		 	var pos=0; 
			var categories=[]; 
			var series = [];
			 $scope.exportArrayData=[];
 
           $rootScope.showloading('#loadingBar'); 

				
				for(var st=0;st<arr.length;st++){
				if(startpos==0){
				if(arr[st]=="09:00 AM"){
					if(st!=0 && st>4)
						startpos=st-4;
					else
						startpos = 0;
					}
				}
				if(endpos==0){
				if(arr[st]=="06:00 PM"){
					if(st!=96 && st<=92)
						endpos=st+4;
					else
						endpos = 94
				}
				}
				}
			for(var ct=startpos;ct<=endpos;ct++){
			if(arr[ct].includes("00")){
				categories.push(arr[ct])

			}
				else{
					categories.push(""); 

			}
			}
		
			var dateSelection={};
			
			var fromTime= "";
			var endTime= ""; 
 
		       	if($scope.typeofdays=="1"){
				fromTime=  moment(new Date).format("MMM DD, YYYY");
				endTime=  moment(new Date).format("MMM DD, YYYY");
								dateSelection ={
										userId :"alluser",					
										days: 'today' 
								}	
							}else if($scope.typeofdays=="2"){
								endTime=  moment(new Date).format("MMM DD, YYYY");
				fromTime=  moment(new Date).subtract(1, 'days').format("MMM DD, YYYY") ;
								dateSelection ={
										userId :"alluser",					
										days: 'twodays' 
								}	
							}else if($scope.typeofdays=="7"){
								endTime=  moment(new Date).format("MMM DD, YYYY");
				fromTime=  moment(new Date).subtract(6, 'days').format("MMM DD, YYYY") ;
								dateSelection ={
										userId :"alluser",					
										days: 'sevendays' 
								}	
							}else if($scope.typeofdays=="14"){
								endTime=  moment(new Date).format("MMM DD, YYYY");
				fromTime=  moment(new Date).subtract(13, 'days').format("MMM DD, YYYY") ;
								dateSelection ={
										userId :"alluser",					
										days: 'fourteendays' 
								}	
							}
		$scope.exportDetailTitle="";
		$scope.pdfHeader="";
		$scope.pdfDate="";
		 
			if($scope.typeofdays=="1"){
				$scope.exportDetailTitle='Work Order Flow Analytics on '+moment(new Date()).format("MMM DD, YYYY");
			}else 
				$scope.exportDetailTitle='Work Order Analytics from '+fromTime+" to "+endTime;
		 
		$scope.exportXLS = true;
		$scope.exportPDF = true;
		$scope.pdfHeader="Work Order Flow Analytics - Line Chart";
		$scope.pdfDate= fromTime;
		$scope.filename = "WOFA_Work Order Flow Analytics";
		$scope.sheetname = "Work Order Flow Analytics";
							$http({
								method : 'POST',
								url : './demo/workOrderchart',
								data:dateSelection,
								headers : {
									'orgName' : $scope.org
								}
							}).then( 
									function(response) {
						 
						$rootScope.hideloading('#loadingBar');
						if (response.data.status == "error") {
						if (response.data.message == "Session timed-out or Invalid") {
							// alert(response.data.message + ". Please logout
							// and login again");
							$.alert({
			                    title: $rootScope.MSGBOX_TITLE,
			                    content: response.data.message + ". Please logout and login again",
			                    icon: 'fa fa-info-circle',
			                     });
							}
							else{
							// alert(response.data.message);
							 $.alert({
				                    title: $rootScope.MSGBOX_TITLE,
				                    content: response.data.message,
				                    icon: 'fa fa-info-circle',
				                     });
							}						
						return;
					} 
					if (response != null && response.data != null
							&& response.data != "BAD_REQUEST") {

						
                        $scope.materialData = response.data;		
						var visible=true;				
						for(var j=0; j<$scope.materialData.materialTrace.zones.length;j++){
							if(j>1)
							visible=false;
							var tQData =$scope.materialData.materialTrace.zones[j];
							if (tQData != undefined && tQData != null) {
							var chartXData = [];
							for(var ct=startpos;ct<=endpos;ct++){
							var pos="TQ"+(ct+1);
							if(ct<9)// changed as 9 from 10 - to display TQ10
									// instead of TQ010
							pos="TQ0"+(ct+1);
							if(tQData.tQ!=undefined && tQData.tQ!=null && tQData.tQ[pos]!=undefined && tQData.tQ[pos]!=null){
							chartXData.push(tQData.tQ[pos].pr)
							
							}
							else{
							
							chartXData.push("0")
							} 
							

				   			}
						series.push( {
						// 'name':new Date(dateObj.date).toUTCString().slice(5,
						// 16),
						'name': tQData.zoneName, // to display date in mmm
													// dd, yyyy format
						'data': chartXData,
						 'visible': visible
						});
						
	 
							}
							
							}
					if(series.length>0){
								$scope.norecords = false;
					
							// to display chart data in excel
							for(i=0;i<categories.length;i++){ 	
								 var obj={};
								 obj['Time']=categories[i];
								for(j=0;j<series.length;j++){				
										obj[series[j].name]= series[j].data[i];					 
								
								}
							$scope.exportArrayData.push(obj);
							}

							var container = document.getElementById('chart-area');
							var data = {
									categories: categories,
									series: series
							};
							
							var options = {
									chart: {
										width: screenWidth * 0.70,
										height: screenHeight * 0.70										
									},
									yAxis: {
										title: '',
										min: 0,
										// max: maxTime,
										pointOnColumn: true
									},
									xAxis: {
										title: 'Time'
									},
									series: {
										spline: false,
										showDot: true
									},
									tooltip: {
										// template:'null'
									},
									chartExportMenu: {
								        visible: false  // default is true.
								    }
							};
					
					
							tui.chart.lineChart(container, data, options);
							
							}else{
								$scope.norecords=true;
								$scope.no_data_text = "No Record found";
								$('#chart-area').empty();
								$scope.exportXLS = false;
								$scope.exportPDF = false;
							}
												
							}
                    })
                        .error(
                                    function(response) {
                                    	$rootScope.hideloading('#loadingBar');
                                    	$rootScope.fnHttpError(response);
                            }); 
					}
	
	$scope.empListDataAllUser = function(){
		if ($scope.checkNetconnection() == false) { 	         
	          $rootScope.hideloading('#loadingBar')
	           // alert("Please check the Internet Connection");
	            return;
	        } 
		$scope.empListAll =[];
		
		var data1={};
		if($scope.typeofdays=="1"){
			data1 ={
					userId :"alluser",					
					days: 'today' 
			}	
		}else if($scope.typeofdays=="2"){
			data1 ={
					userId :"alluser",					
					days: 'twodays' 
			}	
		}else if($scope.typeofdays=="7"){
			data1 ={
					userId :"alluser",					
					days: 'sevendays' 
			}	
		}else if($scope.typeofdays=="14"){
			data1 ={
					userId :"alluser",					
					days: 'fourteendays' 
			}	
		}
		$http({
			method : 'POST',
			url : './demo/dwelldetails',
			data:data1,
			headers : {
				'orgName' : $scope.org
			}

		}).then( 
				function(response) {
					if (response != null && response.data != null
							&& response.data != "BAD_REQUEST") {
										
							$scope.empListAll = response.data.userDetails;
							
							$scope.dataListTimeanalysis= $scope.empListAll;
							$scope.timeanalysisdummyList=$scope.empListAll;
							
							
							$scope.showDwellDetails();
					} else {
						$scope.noList = true;
					}
				});
	}

	
	$scope.empListData = function(){
		if ($scope.checkNetconnection() == false) { 	         
	          $rootScope.hideloading('#loadingBar')
	           // alert("Please check the Internet Connection");
	            return;
	        } 
		$scope.empList =[];
		
		$http({
			method : 'GET',
			url : './demo/allusers',
			headers : {
				'orgName' : $scope.org
			}
		}).then( 
				function(response) {
					if (response != null && response.data != null
							&& response.data != "BAD_REQUEST") {
						
						// employee
						$scope.employees= response.data.userDetails.employees;						
						$scope.employees.sort(sortAlphaNum1); 
						
						 for(var n=0; n<$scope.employees.length; n++)
						 {
							var vis = $scope.employees[n];
							vis.type = 'employees'; 
						 }
		
						// visitors
						$scope.visitors= response.data.userDetails.visitors;						
						if($scope.visitors!=null){
							$scope.visitors.sort(sortAlphaNum1); 
							 for(var n=0; n<$scope.visitors.length; n++)
							 {
								var vis = $scope.visitors[n];
								vis.type = 'visitors'; 
							 }
						}
						// tools
						$scope.tools= response.data.userDetails.tools;						
						if($scope.tools!=null){
							$scope.tools.sort(sortAlphaNum1); 
							for(var n=0; n<$scope.tools.length; n++)
							 {
								var vis = $scope.tools[n];
								vis.type = 'tools'; 
							 }
						}
						// gages
						$scope.gages= response.data.userDetails.gages;						
						if($scope.gages!=null){
							$scope.gages.sort(sortAlphaNum1); 
							for(var n=0; n<$scope.gages.length; n++)
							 {
								var vis = $scope.gages[n];
								vis.type = 'gages'; 
							 }
						}
						// fixtures
						$scope.fixtures= response.data.userDetails.fixtures;						
						if($scope.fixtures!=null){
							$scope.fixtures.sort(sortAlphaNum1); 
							for(var n=0; n<$scope.fixtures.length; n++)
							 {
								var vis = $scope.fixtures[n];
								vis.type = 'fixtures'; 
							 }
						}
						// parts
						$scope.parts= response.data.userDetails.parts;						
						if($scope.parts!=null){
							$scope.parts.sort(sortAlphaNum1); 
							for(var n=0; n<$scope.parts.length; n++)
							 {
								var vis = $scope.parts[n];
								vis.type = 'parts'; 
							 }
						}
						// materials
						$scope.materials= response.data.userDetails.materials;						
						if($scope.materials!=null){
							$scope.materials.sort(sortAlphaNum1); 
							for(var n=0; n<$scope.materials.length; n++)
							 {
								var vis = $scope.materials[n];
								vis.type = 'materials'; 
							 }
						}
						// rmas
						$scope.rmas= response.data.userDetails.rmas;						
						if($scope.rmas!=null){
							$scope.rmas.sort(sortAlphaNum1); 
							for(var n=0; n<$scope.rmas.length; n++)
							 {
								var vis = $scope.rmas[n];
								vis.type = 'rmas'; 
							 }
						}
						
						if($scope.type == "People"){
							$scope.empList = $scope.employees;
							$scope.empList.sort(GetSortOrder("uId"));
							$scope.emp=$scope.empList[0].uId;
							$scope.previousEmployee= $scope.emp						
						}
						$scope.typeofdays="1";						

						
						/*if ($scope.user.capabilityDetails.ZOA.ZDT !=0){
							$scope.employeedwelltime();
							
						}
						if ($scope.user.capabilityDetails.ZOA.ZDT ==0){
							$scope.zoneoccupancy();
						}
						if ($scope.user.capabilityDetails.ZOA.ZDT ==0&&
								$scope.user.capabilityDetails.ZOA.ZOC ==0){
							$scope.zonetraffic();
						}
						if ($scope.user.capabilityDetails.ZOA.ZDT ==0&&
								$scope.user.capabilityDetails.ZOA.ZOC ==0&&
								$scope.user.capabilityDetails.ZOA.ZTR ==0){
							$scope.zonearrivalrate();
						}					
						if ($scope.user.capabilityDetails.ZOA.ZDT ==0&&
								$scope.user.capabilityDetails.ZOA.ZOC ==0&&
								$scope.user.capabilityDetails.ZOA.ZTR ==0 &&
								$scope.user.capabilityDetails.ZOA.ZAR ==0){
							$scope.zoneorgattendance();
						}*/
						
					} else {
						$scope.noList = true;
					}
				});
	}

	$scope.getZoneClassifications = function() {
		if ($scope.checkNetconnection() == false) { 	         
	          $rootScope.hideloading('#loadingBar')
	           // alert("Please check the Internet Connection");
	            return;
	        } 
		$http({
			method : 'GET',
			url :  './demo/classification',
			headers : {
				'orgName' : $scope.org,
				'mapId' : $scope.mapId,
				'siteId' : $scope.siteId,
				'region':$scope.region
			}
		})
		.then(
				function success(response) {
					
					if (response != null
							&& response.data != null
							&& response.data != "BAD_REQUEST") {
						$scope.zoneClassificationList.length = 0;
						var classifications = response.data.classifications;

						for (var i = 0; i < classifications.length; i++) {
							
								$scope.zoneClassificationList.push(classifications[i]);
							
						}

						$scope.zoneClassificationList.sort(GetSortOrder("classification"));
					} else {
						$scope.noList = true;
						
					}
				});
	};
	
$scope.getClassificationsFinalList = function() {
		

		$scope.zoneClassificationFinalList = [];
		
		 function convertTimestamp(timestamp, convertint = 1) {
				var fromdate;
				if (convertint === 1)
					fromdate = parseInt(timestamp, 10);
				else fromdate = timestamp;

				var d = new Date(0);				
				d.setUTCSeconds(fromdate); // Convert the passed timestamp to
											// milliseconds
				var now_utc = new Date(d.toUTCString().slice(0, -4));
				var mnth = ("0" + (now_utc.getMonth() + 1)).slice(-2);
				var day = ("0" + now_utc.getDate()).slice(-2);
				return [now_utc.getFullYear(), mnth, day].join("-");
				
			}
		function formatDate(d) {
			var month = '' + (d.getMonth() + 1), day = ''
					+ d.getDate(), year = d.getFullYear();

			if (month.length < 2)
				month = '0' + month;
			if (day.length < 2)
				day = '0' + day;

			return [ year, month, day ].join('-');
		}

		for (var i = 0; i < $scope.zoneClassificationList.length; i++) {
			
			var cDate = convertTimestamp($scope.zoneClassificationList[i].created_time);
			var mDate = convertTimestamp($scope.zoneClassificationList[i].modified_time);
			var cStatus = $scope.zoneClassificationList[i].status;
			var cClassification = $scope.zoneClassificationList[i].classification;		
					
					
					if (cStatus == "Active") {				
						if (new Date(cDate).toISOString().slice(0, 10) <= new Date($scope.enddate).toISOString().slice(0, 10)) {
					
					$scope.zoneClassificationFinalList
							.push($scope.zoneClassificationList[i]);
				}

			} else if (cStatus == "Inactive") {
				if ((new Date($scope.startdate).toISOString().slice(0, 10) <= new Date(cDate).toISOString().slice(0, 10)) && 
						(new Date($scope.enddate).toISOString().slice(0, 10) <= new Date(cDate).toISOString().slice(0, 10))) {			
										

					if (new Date(cDate).toISOString().slice(0, 10) == new Date($scope.enddate).toISOString().slice(0, 10)) {
						
						$scope.zoneClassificationFinalList
								.push($scope.zoneClassificationList[i]);

					}
				} else {
					if (new Date(mDate).toISOString().slice(0, 10) >= new Date($scope.startdate).toISOString().slice(0, 10)) {
						
						$scope.zoneClassificationFinalList
								.push($scope.zoneClassificationList[i]);
					}

				}

			}

		}
		
	}


	$scope.getQuuppamap = function() {
		if ($scope.checkNetconnection() == false) { 	         
	          $rootScope.hideloading('#loadingBar')
	          // alert("Please check the Internet Connection");
	            return;
	        } 
		
		// map.closePopup();
			map.eachLayer(function(layer) { 
				if(layer.id!==undefined) {
					layer.remove();  
				}
				 
 				if(layer.labelmarkerid!==undefined){
					layer.remove();   
				}
			});
		 if ($scope.imageOverlay!=null && map.hasLayer($scope.imageOverlay))
			 map.removeLayer( $scope.imageOverlay); 
		 

		 if ($scope.marker1!=null && map.hasLayer($scope.marker1))
			 map.removeLayer($scope.marker1); 
		  
		 if ($scope.marker!=null && map.hasLayer($scope.marker))
			 map.removeLayer($scope.marker); 
		 

		  
		 if($scope.grid!=undefined)
			 $scope.grid.onRemove(map);
		 
		 $scope.imageOverlay = null;
		 $scope.marker1 = null;
		 $scope.marker = null;
		
		$http({
			method : 'GET',
			url : './demo/mapdetails',
			headers : {
				'orgName' : $scope.org
			} 
			})
				.then(
						function success(response) {
							if (response != null
									&& response.data != null
									&& response.data != "BAD_REQUEST"
									&& response.data.url != null
									&& response.data.url != "") {

								/*
								 * $scope.mapImage = response.data.url;
								 * $scope.ppmx = response.data.ppmx; $scope.ppmy =
								 * response.data.ppmy;
								 * 
								 * $scope.editableLayers = new L.FeatureGroup();
								 * map.addLayer( $scope.editableLayers);
								 * 
								 * 
								 * $scope.mapDetails.url = response.data.url;
								 * $scope.mapDetails.width =
								 * response.data.width/$scope.ppmx;
								 * $scope.mapDetails.height =
								 * response.data.height/$scope.ppmy;
								 * 
								 * $scope.mapDetails.height_m=
								 * response.data.height;
								 * $scope.mapDetails.width_m=
								 * response.data.width; $scope.mapDetails.ppmx=
								 * response.data.ppmx; $scope.mapDetails.ppmy=
								 * response.data.ppmy;
								 * $scope.mapDetails.mapType= 'quuppa';
								 * map.setZoom(defZoomValue);
								 */
								 $scope.mapImage = response.data.url;
								 $scope.ppm = response.data.ppmx;
								 $scope.ppmx = response.data.ppmx;
								 $scope.ppmy = response.data.ppmy;

								 $scope.editableLayers = new L.FeatureGroup();
								 map.addLayer( $scope.editableLayers);

								 $scope.mapDetails = response.data;  
								 $scope.mapDetails.mapType= 'quuppa';  
								 map.setZoom(defZoomValue);
								
								var southWest = map
										.unproject(
												[
														0,
														$scope.mapDetails.height ],
														defZoomValue);
								var northEast = map.unproject(
										[ $scope.mapDetails.width,
												0 ], defZoomValue);
							  map
										.setMaxBounds(new L.LatLngBounds(
												southWest,
												northEast)); 
								
								map.fitBounds(new L.LatLngBounds(southWest,northEast)); 

								$scope.imageOverlay = L.imageOverlay(
										$scope.mapImage,
										new L.LatLngBounds(
												southWest,
												northEast))
										.addTo(map);
								
								$scope.marker  = new L.Marker(pixelsToLatLng(0,0), {
								    icon: new L.DivIcon({
								        className: 'my-div-icon',
								        html: '<div> <img class="staringImage" src="./assets/img/red_circle.png"/> '
								    })
								});
								$scope.marker.addTo(map);
								
								$scope.grid = L.grid({options: {
							        position: 'topright',
							        bounds: new L.LatLngBounds(
											southWest,
											northEast),
									mapDetails:	$scope.mapDetails,
									defZoomValue:defZoomValue,
									gridSize: 1
							    }});
								
								$scope.marker1 = new L.Marker(pixelsToLatLng(-35, -5), {
								    icon: new L.DivIcon({
								        className: 'my-div-icon',
								        html: '<span class="staringlabel">(0,0)</span>'
								    })
								});
								$scope.marker1.addTo(map);
								

								$http({
									method : 'GET',
									url : './demo/zones',
									headers : {
										'orgName' : $scope.org
									}
								})
								.then(
										function success(response) {
											
											$rootScope.hideloading('#loadingBar');
											if (response != null
													&& response.data != null
													&& response.data != "BAD_REQUEST") { 
												$scope.zoneClassificationList = response.data.classification;	
												$scope.zoneList = response.data.zone;	
												$scope.zoneList.sort(GetSortOrder("name"));
												$scope.zone=$scope.zoneList[0].id;
												
												$scope.getZones(); 
											}else {
												$scope.noList = true;
												
											}
										}/*
											 * ,function error(response) {
											 * $rootScope.hideloading('#loadingBar');
											 * if (response.status == 403) {
											 * alert("Session timed-out or
											 * Invalid. Please logout and login
											 * again"); } }
											 */);
								
							} else {
								$scope.noList = true;
								
							}
						},function error(response) {
							$rootScope.hideloading('#loadingBar');							
							$rootScope.fnHttpError(response);
							
				    });
	};
	
	$scope.mistLogin = function() {
		if ($scope.checkNetconnection() == false) { 	         
	          $rootScope.hideloading('#loadingBar')
	          // alert("Please check the Internet Connection");
	            return;
	        } 
		$http({
			method : 'GET',
			url : './mistLogin',
			headers : {
				'orgName' : $scope.org
			}
		}).then(
				function success(response) {
					
					if (response != null
							&& response.data != null
							&& response.data != "BAD_REQUEST" && response.data.status == "success") {

						$scope.orgDetails = response.data.orgDetails ;
						$scope.siteId = $scope.orgDetails.siteId;
						$scope.mapId = $scope.orgDetails.mapId[0];
						$scope.orgId = $scope.orgDetails.orgId;
						$scope.region = $scope.orgDetails.region;

						map.closePopup();
						map.eachLayer(function(layer) { 
							if(layer.id!==undefined) {
								layer.remove();  
							}
						});
						if ($scope.imageOverlay!=null && map.hasLayer($scope.imageOverlay))
							map.removeLayer( $scope.imageOverlay); 


						if ($scope.marker1!=null && map.hasLayer($scope.marker1))
							map.removeLayer($scope.marker1); 

						if ($scope.marker!=null && map.hasLayer($scope.marker))
							map.removeLayer($scope.marker); 

  						if($scope.grid!=undefined)
							$scope.grid.onRemove(map);

						$scope.imageOverlay = null;
						$scope.marker1 = null;
						$scope.marker = null;


						$scope.getmap(); 
					} else {
						$scope.noList = true;
						if(response.data.message!=undefined && response.data.message!=null)
							// alert(response.data.message);
						 $.alert({
	                            title: $rootScope.MSGBOX_TITLE,
	                            content: response.data.message,
	                            icon: 'fa fa-info-circle',
	                             });
						
					}
				},function error(response) {
					
					$rootScope.hideloading('#loadingBar');
					$rootScope.fnHttpError(response);
		    }); 
	};




	$scope.selectedRow = null;
	$scope.isEdit = false;

	var selectId = null;

	var maxZoomValue = 18;
	var minZoomValue = 8;   
	var defZoomValue = 8;  
	var map = L.map('map', { 
		editable : true,
		maxZoom : maxZoomValue,
		minZoom : minZoomValue,
		zoom: defZoomValue,
		crs : L.CRS.Simple,
		doubleClickZoom : false,
		dragging:true,
		zoomDelta: 0.25,
		zoomSnap : 0.01,
		scrollWheelZoom: false, // disable original zoom function
		  smoothWheelZoom: true,  // enable smooth zoom 
		  smoothSensitivity: 1   // zoom speed. default is 1
	}).setView([ 0, 0 ], 14);


	var ZoomViewer = L.Control.extend({
		onAdd: function(){

			var container= L.DomUtil.create('div');
			var gauge = L.DomUtil.create('div');
			container.style.width = '100px';
			container.style.background = 'rgba(255,255,255,0.5)';
			container.style.textAlign = 'center';
			map.on('zoomstart zoom zoomend', function(ev){
				gauge.innerHTML = 'Zoom: ' + ((map.getZoom()-minZoomValue)+1).toFixed(2);
			})
			container.appendChild(gauge);

			return container;
		}
	});

	(new ZoomViewer).addTo(map);

	
	window.addEventListener('resize', function(event){ 
	    // get the width of the screen after the resize event
	    var width = document.documentElement.clientWidth;
	    // tablets are between 768 and 922 pixels wide
	    // phones are less than 768 pixels wide
	    if (width < 768) {
	        // set the zoom level to 10
	        map.setZoom(10);
	    }  else {
	        // set the zoom level to 8
	        map.setZoom(8);
	    }
	});
	
	var command = L.control({position: 'topright'});

	command.onAdd = function (map) {
		var div = L.DomUtil.create('div', 'command');

		div.innerHTML = '<form>Grid (1X1M) <input id="command" type="checkbox" /></form>'; 
		return div;
	};

	command.addTo(map);


	// add the event handler
	function handleCommand() {
		// alert("Clicked, checked = " + this.checked);
		if(this.checked==true) {
			$scope.grid.onAdd(map)
		} else {
			$scope.grid.onRemove(map);
		}
	}

	document.getElementById ("command").addEventListener ("click", handleCommand, false);

  
	function latLngToPixels(latlng) {
		var pixelArr = [];
		for (var i = 0; i < latlng[0].length; i++) {

			var latlngArr = latlng[0][i];

			var pt= map.project([ latlngArr.lat,
				latlngArr.lng ], defZoomValue); 

			pixelArr.push({
				"x" : pt.x.toFixed(3),
				"y" : pt.y.toFixed(3),
			});


		}
		return pixelArr;
	} 

	function editlatLngToPixels(latlng) {
		var pixelArr = [];
		for (var i = 0; i < latlng[0].length; i++) {

			var latlngArr = latlng[0][i];

			var pt= map.project([
				(latlngArr.lat / $scope.ppm),
				(latlngArr.lng / $scope.ppm) ], defZoomValue);


			pixelArr.push({
				"x" : pt.x.toFixed(3),
				"y" : pt.y.toFixed(3),
			});

		}
		return pixelArr;
	} 

	function pixelsToLatLng(x, y) {
		return map.unproject([ x, y ], defZoomValue);
	} 



	$scope.ppm = "";


	$scope.getmap = function() {
		if ($scope.checkNetconnection() == false) { 	         
	          $rootScope.hideloading('#loadingBar')
	          // alert("Please check the Internet Connection");
	            return;
	        } 
		$http({
			method : 'GET',
			url : './mistmapDetails',
			headers : {
				'orgName' : $scope.org,
				'mapId' : $scope.mapId,
				'siteId' : $scope.siteId,
				'region':$scope.region
			}
		})
		.then(
				function success(response) {
				
					if (response != null
							&& response.data != null
							&& response.data != "BAD_REQUEST"
								&& response.data.url != null
								&& response.data.url != "") {

						$scope.mapDetails = response.data;
						$scope.mapImage = response.data.url;
						$scope.ppm = response.data.ppm;
						$scope.mapDetails.mapType= 'mist';
						$scope.editableLayers = new L.FeatureGroup();
						map.addLayer( $scope.editableLayers);

						map.setZoom(defZoomValue);
						var southWest = map
						.unproject(
								[
									0,
									response.data.height ],
									defZoomValue);
						var northEast = map.unproject(
								[ response.data.width,
									0 ], defZoomValue);
						
						map.setMaxBounds(new L.LatLngBounds(
								southWest,
								northEast)); 
						
						map.fitBounds(new L.LatLngBounds(southWest,northEast)); 

						$scope.imageOverlay = L.imageOverlay(
								$scope.mapImage,
								new L.LatLngBounds(
										southWest,
										northEast))
										.addTo(map);

						$scope.marker  = new L.Marker(pixelsToLatLng(0,0), {
							icon: new L.DivIcon({
								className: 'my-div-icon',
								html: '<div> <img class="staringImage" src="./assets/img/red_circle.png"/> '
							})
						});
						$scope.marker.addTo(map);

						$scope.grid = L.grid({options: {
							position: 'topright',
							bounds: new L.LatLngBounds(
									southWest,
									northEast),
									mapDetails:	$scope.mapDetails,
									defZoomValue:defZoomValue,
									gridSize: 1
						}});

						$scope.marker1 = new L.Marker(pixelsToLatLng(-35, -5), {
							icon: new L.DivIcon({
								className: 'my-div-icon',
								html: '<span class="staringlabel">(0,0)</span>'
							})
						});
						$scope.marker1.addTo(map);


						$scope.getZones(); 

					} else {
						
						$scope.noList = true;
					}
				},function error(response) {
					$rootScope.hideloading('#loadingBar');
					$rootScope.fnHttpError(response);
		    });
	};


	$scope.getZones = function(){
		$scope.exportPDF = false;
		$scope.exportXLS = false;
		map.eachLayer(function(layer) {
			if(layer.id !== undefined){ 
				layer.remove();
			}
			if(layer.labelmarkerid!==undefined){
				layer.remove();  
			}
		}); 
		// $scope.zoneList.sort(GetSortOrder("name"));
		if($scope.isZoneoccupancyClicked==true){
			var zoneData=[];
			$scope.zoneList.sort(GetSortOrder("name"));
			for (var n = 0; n < $scope.zoneList.length; n++) {
				var colorcode="#69696e";
				if($scope.zoneList[n].classification!=null && $scope.zoneList[n].classification!=undefined && $scope.zoneList[n].classification!='')
					colorcode=$scope.zoneList[n].classification;

				zoneData.push ({
					'zoneID': $scope.zoneList[n].id,
					'zoneName':$scope.zoneList[n].name,
					'Max':0, 
					'Peak':0,
					'Average':0,
					'Classification':$scope.zoneList[n].classification,
					'colorcode':colorcode,
					'OccupancyRate':0, 
				});
			}

			if($scope.dataList!=undefined && $scope.dataList.length!=0) {	
				$scope.dataList.sort(GetSortOrder("zoneName")); 
				for (var i = 0; i < $scope.dataList.length; i++) {
					var dateObj = $scope.dataList[i];
					if (dateObj != undefined && dateObj != null) {

						var zoneObjList = dateObj.zone;

						if (zoneObjList != undefined && zoneObjList != null && zoneObjList.length>0) {
							zoneObjList.sort(GetSortOrder("zoneName"));
							var ocuupancyRate=120;
							for (var j = 0; j < zoneObjList.length; j++) {
								var zoneObj = zoneObjList[j];
								if(zoneObj.zoneRestrictions!=0)
									ocuupancyRate=zoneObj.dWellTime.maximum/zoneObj.zoneRestrictions*100;

								if(zoneData.filter(x => x.zoneID ===zoneObj.zoneID)[0])
								{
									var listObj = zoneData.filter(x => x.zoneID ===zoneObj.zoneID)[0]; 
									if(listObj.Peak<zoneObj.dWellTime.maximum){ 
										listObj.Max=zoneObj.zoneRestrictions; 
										listObj.Peak=zoneObj.dWellTime.maximum;
										listObj.Average=zoneObj.dWellTime.average;
										listObj.OccupancyRate=Math.floor(ocuupancyRate);

										zoneData.push(listObj);

									}
								} 
							}

						} 
					}
				}
			}

		}


		if($scope.isZoneorgattendanceClicked==true){
			
			var startpos=0;
			var endpos=0; 
		 	var pos=0; 
		 	
			for(var st=0;st<arr.length;st++){
				if(startpos==0){
					if(arr[st]==$scope.orgoperationalstarttime){
						if(st!=0 && st>4)
							startpos=st-4;
						else
							startpos = 0;
					}
				}
				if(endpos==0){
					if(arr[st]==$scope.orgoperationalendtime){
						if(st!=96 && st<=92)
							endpos=st+4;
						else
							endpos = 94
					}
				}
			}
		 	 
			if((startpos+$scope.tqidval)<9)// to display tq10 value
											// geetha/27-3-2021
				pos="TQ0"+(startpos+$scope.tqidval+1); 
			else
				pos="TQ"+(startpos+$scope.tqidval+1);

			var zoneDataclone={}; 
			var zoneData ={};
			$scope.zoneList.sort(GetSortOrder("name"));
			for (var n = 0; n < $scope.zoneList.length; n++) {
				zoneData[$scope.zoneList[n].id] =  {
						'zoneID': $scope.zoneList[n].id,
						'zoneName':$scope.zoneList[n].name, 
						'value':0 
				};
			}
			
			
			for (var i = 0; i < $scope.dataList.length; i++) {
				var dateObj = $scope.dataList[i];
				zoneDataclone=zoneData;
				if (dateObj != undefined && dateObj != null) {
					var zoneObjList = dateObj.zone;
					var orgObjList = dateObj.orgAttendance;

 					if (zoneObjList != undefined && zoneObjList != null && zoneObjList.length>0&&orgObjList != undefined && orgObjList != null) {
						zoneObjList.sort(GetSortOrder("zoneName"));
						if(orgObjList[pos]!=undefined&&orgObjList[pos]!=null){

							if(orgObjList[pos].tot!=undefined&&orgObjList[pos].tot!=null)
								total=orgObjList[pos].tot;
							else
								total=0;

							if(orgObjList[pos].user!=undefined&&orgObjList[pos].user!=null){
								if(orgObjList[pos].user.emp!=undefined&&orgObjList[pos].user.emp!=null)
									employee=orgObjList[pos].user.emp;
								else
									employee=0;
								if(orgObjList[pos].user.vis!=undefined&&orgObjList[pos].user.vis!=null)
									visitor=orgObjList[pos].user.vis;
								else
									visitor=0; 
							}else
							{
								employee=0; 
								visitor=0;
							}
						}
						else{ 
							total=0;
							employee=0; 
							visitor=0;

						}
						for (var j = 0; j < zoneObjList.length; j++) {
							var zoneObj = zoneObjList[j];
							if (zoneObj.tQ[pos]!=undefined&&zoneObj.tQ[pos]!=null){
								
								for (var k = 0; k <  $scope.zoneList.length; k++){
								if(zoneDataclone[$scope.zoneList[k].id].zoneName == zoneObj.zoneName)
								{
									var listObj = zoneDataclone[$scope.zoneList[k].id];
									listObj.value=zoneObj.tQ[pos];  
									zoneDataclone[$scope.zoneList[k].id]=listObj;
									 
								} 
								}
							}
						}
					}
				}  
			} 
 		}

		for (var i = 0; i < $scope.zoneList.length; i++) {
			//var zoneRemovedDate = $scope.zoneList[i].modified_time;
			//var zoneCreatedDate = $scope.zoneList[i].created_time;
			//var zoneStatus = $scope.zoneList[i].zoneStatus;
			//if(zoneStatus == 'Active')
			//{
			//	zoneRemovedDate = new Date().getTime() / 1000;
			//}  
			
			//if(checkZoneStates(zoneCreatedDate, zoneRemovedDate, (new Date($scope.startdate).getTime()/1000), (new Date($scope.enddate).getTime()/1000)))
			//{
				 
			// if($scope.zoneList[i].zoneStatus=="Active") {
				var bounds  = [];
				
				$scope.colorcode = "#ff7800"
				var classIndex = 0;
				if($scope.zoneList[i].zoneClassification != undefined && $scope.zoneList[i].zoneClassification != null &&
						$scope.zoneList[i].zoneClassification > 0){ 
					//var classification = $scope.zoneClassificationList.filter(x => x.classification ===$scope.zoneList[i].zoneClassification)[0];
					for (classIndex = 0; classIndex < $scope.zoneClassificationList.length; classIndex++) {
						if ($scope.zoneClassificationList[classIndex].id ==$scope.zoneList[i].zoneClassification)
							{
								$scope.colorcode = $scope.zoneClassificationList[classIndex].color;
								break;
							}
					}
				}

				if($scope.isEmployeedwelltimeClicked==true) {
					if($scope.dataList!=undefined && $scope.dataList!=0) {
						var zoneDwelltime = $scope.dataList.filter(x => x.zoneID ===$scope.zoneList[i].id)[0];
						if(zoneDwelltime!=null && zoneDwelltime!=undefined && zoneDwelltime!='') {
							$scope.zoneDwelltime = Math.round(zoneDwelltime.dWellTime/60);
						}else{  
							$scope.zoneDwelltime = 0;
						}
					} else {
						$scope.zoneDwelltime = 0;
					}
				}else if($scope.isZoneoccupancyClicked==true){
					if($scope.dataList!=undefined && $scope.dataList.length!=0) {
						var zoneOccupancy = zoneData.filter(x => x.zoneID ===$scope.zoneList[i].id)[0];
						$scope.max = zoneOccupancy.Max;
						$scope.peak = zoneOccupancy.Peak;
						$scope.average = zoneOccupancy.Average;
						$scope.occupancyRate =zoneOccupancy.OccupancyRate;
					} else {
						$scope.max = 0;
						$scope.peak = 0;
						$scope.average = 0;
						$scope.occupancyRate =0;
					}
				}else if($scope.isZonetrafficClicked==true){
					if($scope.dataList!=undefined && $scope.dataList.length!=0) {
						var zonetrafficlist = $scope.dataList[0].zone.filter(x => x.zoneID ===$scope.zoneList[i].id)[0];
						if(zonetrafficlist!=null && zonetrafficlist!=undefined && zonetrafficlist!='') {
							$scope.zoneDwelltime = zonetrafficlist.dWellTime;
							$scope.totaltraffic = $scope.zoneDwelltime.total
							$scope.peaktraffic = $scope.zoneDwelltime.maximum
							$scope.averagetraffic = Math.ceil($scope.zoneDwelltime.average)
						}else{  
							$scope.totaltraffic = 0;
							$scope.peaktraffic = 0;
							$scope.averagetraffic = 0;
						}
					}else {
						$scope.totaltraffic = 0;
						$scope.peaktraffic = 0;
						$scope.averagetraffic = 0;
					}
				}else if($scope.isZoneorgattendanceClicked==true){
					if(zoneData!=undefined && zoneData.length!=0) {
						var tqDataObj = zoneDataclone[$scope.zoneList[i].id];
						if(tqDataObj!=null && tqDataObj!=undefined && tqDataObj!='') {
							if(tqDataObj.value!=undefined) {
								$scope.tq = tqDataObj.value; 
							}else {
								$scope.tq = 0;
							}
						} else {
							$scope.tq = 0;
						}
					} else {
						$scope.tq = 0;
					}
				}


				if($scope.zoneList[i].geometryType== undefined || $scope.zoneList[i].geometryType==null || $scope.zoneList[i].geometryType=="polygon" || $scope.zoneList[i].geometryType==""){ 
					for (var j = 0; j < $scope.zoneList[i].coordinateList.length; j++) {  
						bounds.push(pixelsToLatLng(parseFloat($scope.zoneList[i].coordinateList[j].x / $scope.ppmx) 
								,parseFloat($scope.zoneList[i].coordinateList[j].y / $scope.ppmy)));

					}

					//$scope.zoneList[i].zoneType="polygon";
					var pol = L
					.polygon(
							bounds,
							{
								color : $scope.colorcode ,
								weight : 1,
								fillColor :$scope.colorcode , 
								fillOpacity: 0.5
							})
							.addTo(
									map)
									.addTo(
											$scope.editableLayers)
											.on('mouseover', mouseOverPopup); 

					pol.id = i;
					pol.primaryid = $scope.zoneList[i].id
					pol.name = $scope.zoneList[i].name;
					pol.classification = $scope.zoneClassificationList[classIndex].className;
					pol.restrictions = $scope.zoneList[i].restrictions;
					pol.ppm = $scope.ppm;
					pol.vertices = bounds;
					pol.map_id = $scope.zoneList[i].map_id;
					pol.colorcode = $scope.colorcode;
					pol.zoneType = $scope.zoneList[i].geometryType;

					var html='';
					if($scope.isEmployeedwelltimeClicked==true) {
						pol.dWellTime =$scope.zoneDwelltime;
						html = "<div style='width: 85px; color:white;font-size: 12px;'>"+$scope.zoneList[i].name+"</div><div style='font-size: 11px'>"+$scope.zoneDwelltime+"</div>";
					} else if($scope.isZoneoccupancyClicked==true){
						pol.occupancyRate =$scope.occupancyRate;
						pol.peak =$scope.peak;
						pol.average =$scope.average;
						pol.max =$scope.max;
						html = "<div style='width: 85px;color:white;font-size: 12px;'>"+$scope.zoneList[i].name+"</div><div style='width: 120px;font-size: 11px'>Occupancy Rate :"+$scope.occupancyRate+" %</div>";
					}else if($scope.isZonetrafficClicked==true){
						pol.totaltraffic = $scope.totaltraffic;
						pol.peaktraffic = $scope.peaktraffic;
						pol.averagetraffic = $scope.averagetraffic;
						html = "<div style='width: 65px;color:white;font-size: 12px;'>"+$scope.zoneList[i].name+"</div><div style='font-size: 11px;width: 100px;'>Total traffic :"+$scope.totaltraffic+"</div> <div style='font-size: 11px;width: 100px;'>Peak  traffic :"+$scope.peaktraffic+"</div> <div style='font-size: 11px;width: 100px;'>Average traffic :"+$scope.averagetraffic+"</div>";
					}else if($scope.isZoneorgattendanceClicked==true){
						pol.tq = $scope.tq;
						html = "<div style='color:Purple; font-size: 12px;width: 120px;'>No.of Occupancy :"+$scope.tq+"</div>";
					}else if($scope.isproductanalysisClicked){
						//rec.tq = $scope.tq;
						
						var productanalysisData = $scope.getProductAnalysisData($scope.zoneList[i].name);
						if(productanalysisData!=null)
						{ 
							html = "<div style='width: 85px;color:white;font-size: 12px;'>"+$scope.zoneList[i].name 
							+"<br/>Incoming:"+productanalysisData.incoming
							+"<br/>Outgoing:"+productanalysisData.outgoing
							+"<br/>Waiting:"+productanalysisData.waiting+"</div>";
							 
							pol.incoming = productanalysisData.incoming;
							pol.outgoing = productanalysisData.outgoing;
							pol.waiting = productanalysisData.waiting;
							
						}else
						{	html = "<div style='width: 85px;color:white;font-size: 12px;'>"+$scope.zoneList[i].name 
							+"<br/>Incoming: 0" 
							+"<br/>Outgoing: 0" 
							+"<br/>Waiting: 0"+"</div>";

							pol.incoming = 0;
							pol.outgoing = 0;
							pol.waiting = 0;
						} 
						
					}
					$scope.labelmarker  = new L.Marker(pol.getBounds().getCenter(), {
						icon: new L.DivIcon({
							className: 'labelmap',
							html: html
						})
					}); 


				} else {

					var bounds1 = [
						[ pixelsToLatLng(
								parseFloat($scope.zoneList[i].coordinateList[0].x / $scope.ppmx) ,
								parseFloat($scope.zoneList[i].coordinateList[0].y/ $scope.ppmy)) ],
								[ pixelsToLatLng(
										parseFloat($scope.zoneList[i].coordinateList[1].x/ $scope.ppmx),
										parseFloat($scope.zoneList[i].coordinateList[1].y/ $scope.ppmy)) ],
										[ pixelsToLatLng(
												parseFloat($scope.zoneList[i].coordinateList[2].x/ $scope.ppmx),
												parseFloat($scope.zoneList[i].coordinateList[2].y/ $scope.ppmy)) ],
												[ pixelsToLatLng(
														parseFloat($scope.zoneList[i].coordinateList[3].x/ $scope.ppmx),
														parseFloat($scope.zoneList[i].coordinateList[3].y/ $scope.ppmy)) ] ];
					var rec = L
					.rectangle(
							bounds1,
							{
								color : $scope.colorcode ,
								weight : 1,
								fillColor :$scope.colorcode , 
								fillOpacity: 0.5
							})
							.addTo(
									map)
									.addTo(
											$scope.editableLayers)
											.on('mouseover', mouseOverPopup); 

					rec.id = i;
					rec.primaryid = $scope.zoneList[i].id
					rec.name = $scope.zoneList[i].name;
					rec.classification = $scope.zoneClassificationList[classIndex].className;
					rec.restrictions = $scope.zoneList[i].restrictions;
					rec.ppm = $scope.ppm;
					rec.vertices = bounds1;
					rec.map_id = $scope.zoneList[i].map_id;
					rec.zoneType = $scope.zoneList[i].geometryType; 
					rec.colorcode = $scope.colorcode;
					var html='';
					if($scope.isEmployeedwelltimeClicked==true) {
						rec.dWellTime =$scope.zoneDwelltime;
						html = "<div style='width: 85px; color:white;font-size: 12px;'>"+$scope.zoneList[i].name+"</div><div style='font-size: 11px'>"+$scope.zoneDwelltime+"</div>";
					} else if($scope.isZoneoccupancyClicked==true){
						rec.occupancyRate =$scope.occupancyRate;
						rec.peak =$scope.peak;
						rec.average =$scope.average;
						rec.max =$scope.max;
						html = "<div style='width: 85px;color:white;font-size: 12px;'>"+$scope.zoneList[i].name+"</div><div style='width: 120px;font-size: 11px'>Occupancy Rate :"+$scope.occupancyRate+" %</div>";
					}else if($scope.isZonetrafficClicked==true){
						rec.totaltraffic = $scope.totaltraffic;
						rec.peaktraffic = $scope.peaktraffic;
						rec.averagetraffic = $scope.averagetraffic;
						html = "<div style='width: 65px;color:white;font-size: 12px;'>"+$scope.zoneList[i].name+"</div><div style='font-size: 11px;width: 100px;'>Total traffic :"+$scope.totaltraffic+"</div> <div style='font-size: 11px;width: 100px;'>Peak  traffic :"+$scope.peaktraffic+"</div> <div style='font-size: 11px;width: 100px;'>Average traffic :"+$scope.averagetraffic+"</div>";
					}else if($scope.isZoneorgattendanceClicked==true){
						rec.tq = $scope.tq;
						html = "</div><div style='color:Purple; font-size: 12px;width: 120px;'>No.of Occupancy :"+$scope.tq+"</div>";
					}else if($scope.isproductanalysisClicked){
						//rec.tq = $scope.tq;
						
						var productanalysisData = $scope.getProductAnalysisData($scope.zoneList[i].name);
						if(productanalysisData!=null)
						{ 
							html = "<div style='width: 85px;color:white;font-size: 12px;'>"+$scope.zoneList[i].name 
							+"<br/>Incoming:"+productanalysisData.incoming
							+"<br/>Outgoing:"+productanalysisData.outgoing
							+"<br/>Waiting:"+productanalysisData.waiting+"</div>";
							 
							rec.incoming = productanalysisData.incoming;
							rec.outgoing = productanalysisData.outgoing;
							rec.waiting = productanalysisData.waiting;
							
						}else
						{	html = "<div style='width: 85px;color:white;font-size: 12px;'>"+$scope.zoneList[i].name 
							+"<br/>Incoming: 0" 
							+"<br/>Outgoing: 0" 
							+"<br/>Waiting: 0"+"</div>";

							rec.incoming = 0;
							rec.outgoing = 0;
							rec.waiting = 0;
						}
						
						
					}
 
					$scope.labelmarker  = new L.Marker(rec.getBounds().getCenter(), {
						icon: new L.DivIcon({
							className: 'labelmap',
							html: html
						})
					}); 

				}
				$scope.labelmarker.addTo(map)
				$scope.labelmarker.labelmarkerid = i;
			// }
			//}
		}

	} 
  
	
	
	function checkZoneStates(zoneCreatedTime, zoneEndDate, startDate, endDate)
	{
		if (zoneCreatedTime ===startDate  || zoneCreatedTime  === endDate  
	    ||  zoneEndDate === startDate || zoneEndDate  === endDate  )
			return true;
		
		if (zoneCreatedTime < startDate && startDate < zoneEndDate) return true; // b
																					// starts
																					// in a
	    if (zoneCreatedTime < endDate   && endDate   < zoneEndDate) return true; // b
																					// ends
																					// in a
	    if (startDate <  zoneCreatedTime && zoneEndDate   <  endDate) return true; // a in
																					// b
	    return false;
	}
	
	$scope.typeofdays =1;
	$scope.getZoneDetails = function(){	
		if($scope.isMaterialTraceClicked){
			$scope.workOrderChart();	
             return;
		}
		
		$rootScope.showloading('#loadingBar');
		if($scope.typeofdays!=0 && $scope.typeofdays!=1) {
			$scope.enddate = moment(new Date()).format("YYYY-MM-DD")
			$scope.startdate  = moment().subtract($scope.typeofdays-1, 'days').format("YYYY-MM-DD")
		}
		else if($scope.typeofdays==1){
			$scope.startdate = moment(new Date()).format("YYYY-MM-DD")
			$scope.enddate = moment(new Date()).format("YYYY-MM-DD")
		}
		else if($scope.typeofdays==0){
			$scope.startdate = moment($('#startdate').val()).format("YYYY-MM-DD")
			$scope.enddate = moment($('#enddate').val()).format("YYYY-MM-DD")
			
			
			if ($scope.startdate > $scope.enddate) {								
				// alert("Start date should be less than or equal to end date");
				$.alert({
                    title: $rootScope.MSGBOX_TITLE,
                    content: "Start date should be less than or equal to end date",
                    icon: 'fa fa-info-circle',
                     });
				$rootScope.hideloading('#loadingBar')
				return;
				
			} else if ($scope.enddate < $scope.startdate){
				// alert("End date should be greater than or equal to start
				// date");
				$.alert({
                    title: $rootScope.MSGBOX_TITLE,
                    content: "End date should be greater than or equal to start date",
                    icon: 'fa fa-info-circle',
                     });
				$rootScope.hideloading('#loadingBar')
				return;
			}
			
		}
		var url;
		var data={}
		var method;
		if($scope.isZoneoccupancyClicked ==true){
			if(($scope.dateValueOccupancy == $scope.typeofdays)&&($scope.zone == $scope.previousZoneOccupancy)
					&&(($scope.activestartdateOccupancy == $scope.startdate)&&($scope.activeenddateOccupancy == $scope.enddate))){				
				$rootScope.hideloading('#loadingBar')
				return;
			}else
				{
				map.closePopup(); 
			   	$rootScope.showloading('#loadingBar');
				map.eachLayer(function(layer) {
					if(layer.id !== undefined){ 
						layer.remove();
					}
					if(layer.labelmarkerid!==undefined){ 
						layer.remove();  
					}
				});
				}
			if ($scope.checkNetconnection() == false) { 
				$rootScope.hideloading('#loadingBar')
		         // alert("Please check the Internet Connection");
				$.alert({
                    title: $rootScope.MSGBOX_TITLE,
                    content: "Please check the Internet Connection",
                    icon: 'fa fa-info-circle',
                     });
		            return;
		        } 
			url="./demo/occupancydetails";
			method='POST';
			if($scope.typeofdays=="1"){
				data ={
					zoneId :"allzones",
					days: 'today'
				}
			}else if($scope.typeofdays=="2"){
				data ={
					zoneId :"allzones",
					days: 'twodays'
				}
			}else if($scope.typeofdays=="7"){
				data ={
					zoneId :"allzones",
					days: 'sevendays'
				}
			}else if($scope.typeofdays=="14"){
				data ={
					zoneId :"allzones",
					days: 'fourteendays'
				} 
			}
			
		}else if($scope.isEmployeedwelltimeClicked ==true){
			
			map.closePopup(); 
		   	$rootScope.showloading('#loadingBar');
			map.eachLayer(function(layer) {
				if(layer.id !== undefined){ 
					layer.remove();
				}
				if(layer.labelmarkerid!==undefined){ 
					layer.remove();  
				}
			});
			// }
			if ($scope.checkNetconnection() == false) { 
				$rootScope.hideloading('#loadingBar')
		        // alert("Please check the Internet Connection");
				$.alert({
                    title: $rootScope.MSGBOX_TITLE,
                    content: "Please check the Internet Connection",
                    icon: 'fa fa-info-circle',
                     });
		            return;
		        } 
			$scope.emp = "Employee 1"
			url="./demo/dwelldetails";	
			method='POST';
			if($scope.typeofdays=="1"){
				data ={
						userId :$scope.emp,					
						days: 'today' 
				}	
			}else if($scope.typeofdays=="2"){
				data ={
						userId :$scope.emp,					
						days: 'twodays' 
				}	
			}else if($scope.typeofdays=="7"){
				data ={
						userId :$scope.emp,					
						days: 'sevendays' 
				}	
			}else if($scope.typeofdays=="14"){
				data ={
						userId :$scope.emp,					
						days: 'fourteendays' 
				}	
			}
			
		}else if($scope.isZonetrafficClicked){
			$scope.startdate = moment($('#selectdate').val()).format("YYYY-MM-DD")
			$scope.enddate =moment($('#selectdate').val()).format("YYYY-MM-DD")
			 
			if($scope.activestartdateTraffic == $scope.startdate){				
				$rootScope.hideloading('#loadingBar')
				return;
			}else
				{
				map.closePopup(); 
			   	$rootScope.showloading('#loadingBar');
				map.eachLayer(function(layer) {
					if(layer.id !== undefined){ 
						layer.remove();
					}
					if(layer.labelmarkerid!==undefined){ 
						layer.remove();  
					}
				});
				}
			if ($scope.checkNetconnection() == false) { 
				$rootScope.hideloading('#loadingBar')
		          // alert("Please check the Internet Connection");
				$.alert({
                    title: $rootScope.MSGBOX_TITLE,
                    content: "Please check the Internet Connection",
                    icon: 'fa fa-info-circle',
                     });
		            return;
		        } 
			
			method='GET';
			url="./demo/Footfall_ArrivalRate";
			data ={
					/*
					 * startDate:$scope.startdate, endDate:$scope.enddate
					 */
			}
			
		}
		else if($scope.isZonearrivalrateClicked){
			if(($scope.dateValueArrival == $scope.typeofdays)&&($scope.zone == $scope.previousZoneArrival)
					&&(($scope.activestartdateArrival == $scope.startdate)&&($scope.activeenddateArrival == $scope.enddate))){				
				$rootScope.hideloading('#loadingBar')
				return;
			}
			if ($scope.checkNetconnection() == false) { 
				$rootScope.hideloading('#loadingBar')
		        // alert("Please check the Internet Connection");
				$.alert({
                    title: $rootScope.MSGBOX_TITLE,
                    content: "Please check the Internet Connection",
                    icon: 'fa fa-info-circle',
                     });
		            return;
		        }
			url="./demo/arrivaldetails";
			method='POST';
			if($scope.typeofdays=="1"){
				data ={
					zoneId :$scope.zone,
					days: 'today'
				}
			}else if($scope.typeofdays=="2"){
				data ={
					zoneId :$scope.zone,
					days: 'twodays'
				}
			}else if($scope.typeofdays=="7"){
				data ={
					zoneId :$scope.zone,
					days: 'sevendays'
				}
			}else if($scope.typeofdays=="14"){
				data ={
					zoneId :$scope.zone,
					days: 'fourteendays'
				} 
			}
			// url="./ZoneFootFall";
			// data ={
			// startDate:$scope.startdate,
			// endDate:$scope.enddate,
			// zoneId:$scope.zone
			// }
			
		}else if($scope.isZoneorgattendanceClicked ==true){
		 
			$scope.startdate = moment($('#idPFAEditStartDatePicker').val()).format("YYYY-MM-DD")
			$scope.enddate = moment($('#idPFAEditStartDatePicker').val()).format("YYYY-MM-DD")
			if($scope.activestartdateAttendance == $scope.startdate){				
				$rootScope.hideloading('#loadingBar')
				return;
			}else
				{
				map.closePopup(); 
			   	$rootScope.showloading('#loadingBar');
				map.eachLayer(function(layer) {
					if(layer.id !== undefined){ 
						layer.remove();
					}
					if(layer.labelmarkerid!==undefined){ 
						layer.remove();  
					}
				});
				}
			if ($scope.checkNetconnection() == false) { 
				$rootScope.hideloading('#loadingBar')
		        // alert("Please check the Internet Connection");
				$.alert({
                    title: $rootScope.MSGBOX_TITLE,
                    content: "Please check the Internet Connection",
                    icon: 'fa fa-info-circle',
                     });
		            return;
		        }
			method='GET';
			
			if($scope.alternateAtt == "./demo/Attendance"){
					url="./demo/Attendance1";
					$scope.alternateAtt = "./demo/Attendance1";
			}
			else if($scope.alternateAtt == "./demo/Attendance1"){
					url="./demo/Attendance";	
					$scope.alternateAtt = "./demo/Attendance";
			}
			
			
			data ={
					
			}
			
			/*
			 * url="./attendance"; data ={ startDate:$scope.startdate }
			 */
		}else if($scope.isproductanalysisClicked)
		{
			
			$scope.productanAlysisZone = $scope.selectedZone;
            $scope.productanAlysisDays = $scope.typeofdays;
            
            if($scope.viewValue=="Column Chart")
                $scope.productanAlysisSortBy = $scope.sortBy;
            else 
                $scope.productanAlysisSortBy = '0';

			  data={} 
			  url="./demo/productivity";	
			  method='POST';
			if($scope.typeofdays=="1"){
				data ={
						zone :$scope.selectedZone,					
						days: 'today' 
				}	
			}else if($scope.typeofdays=="2"){
				data ={
						zone :$scope.selectedZone,					
						days: 'twodays' 
				}	
			}else if($scope.typeofdays=="7"){
				data ={
						zone :$scope.selectedZone,					
						days: 'sevendays' 
				}	
			}else if($scope.typeofdays=="14"){
				data ={
						zone :$scope.selectedZone,					
						days: 'fourteendays' 
				}	
			}
		}

		$http({
			method : method,
			url : url,
			data:data,
			headers : {
				'orgName' : $scope.org
			}
		}).then(
				function success(response) {	
					
					if (response.data.status == "error") {
						$rootScope.hideloading('#loadingBar');
						if (response.data.message == "Session timed-out or Invalid") {
							// alert(response.data.message + ". Please logout
							// and login again");
							$.alert({
			                    title: $rootScope.MSGBOX_TITLE,
			                    content: response.data.message + ". Please logout and login again",
			                    icon: 'fa fa-info-circle',
			                     });
							}
							else{
							
							 $.alert({
				                    title: $rootScope.MSGBOX_TITLE,
				                    content: response.data.message,
				                    icon: 'fa fa-info-circle',
				                     });
							}						
						return;
					} 
					if (response != null && response.data != null
							&& response.data != "BAD_REQUEST") {

						$rootScope.hideloading('#loadingBar');
						// $scope.prevflag = "true";
						if($scope.isZoneoccupancyClicked ==true){
							$scope.dataList=[];
							$scope.occupancydummyList=[];
							$scope.dataList= response.data.occupancy;						
							$scope.occupancydummyList=response.data.occupancy;
							$scope.previousZoneOccupancy= $scope.zone	
							$scope.viewValueOccupancy=$scope.viewValue;
							var dateSelected=$scope.typeofdays;
							$scope.dateValueOccupancy=dateSelected;				
							if($scope.dateValueOccupancy!=0 && $scope.dateValueOccupancy!=1) {
								$scope.OccupancyStartDate  = moment().subtract($scope.typeofdays-1, 'days').format("YYYY-MM-DD")
								$scope.OccupancyEndDate = moment(new Date()).format("YYYY-MM-DD")	
								$scope.activestartdateOccupancy=moment().subtract($scope.typeofdays-1, 'days').format("YYYY-MM-DD")
								$scope.activeenddateOccupancy=moment(new Date()).format("YYYY-MM-DD")
							}
							else if($scope.dateValueOccupancy==1){
								$scope.OccupancyStartDate = moment(new Date()).format("YYYY-MM-DD")
								$scope.OccupancyEndDate = moment(new Date()).format("YYYY-MM-DD")
								$scope.activestartdateOccupancy=moment(new Date()).format("YYYY-MM-DD")
									$scope.activeenddateOccupancy=moment(new Date()).format("YYYY-MM-DD")
							}
							if($scope.dateValueOccupancy == 0)
							{
								$scope.activestartdateOccupancy=moment($('#startdate').val()).format("YYYY-MM-DD");
								$scope.activeenddateOccupancy=moment($('#enddate').val()).format("YYYY-MM-DD");
								$scope.OccupancyStartDate= moment($('#startdate').val()).format("MMM DD, YYYY");
								$scope.OccupancyEndDate=moment($('#enddate').val()).format("MMM DD, YYYY");
								if($scope.typeofdays==0){
									$scope.customdates=true;  				
											$('#startdate').val($scope.OccupancyStartDate);	
											$('#enddate').val($scope.OccupancyEndDate);
								}
							}
						}else if($scope.isEmployeedwelltimeClicked  ==true){
								
							$scope.dataList=[];
							$scope.empdwelldummyList=[];
							$scope.dataList = response.data.zones;
							$scope.empdwelldummyList = response.data.zones;							
							$scope.empListDataAllUser();							
							return;
													
						}else if($scope.isZonetrafficClicked){
							$scope.dataList=[];
							$scope.trafficdummyList=[];
							$scope.dataList= response.data.footFall;
							$scope.trafficdummyList=response.data.footFall;
							
							$scope.viewValueTraffic=$scope.viewValue;
							$scope.dateValueTraffic=moment($('#selectdate').val()).format("MMM DD, YYYY");
							$scope.activestartdateTraffic = moment($('#selectdate').val()).format("YYYY-MM-DD")
							$scope.activeenddateTraffic =moment($('#selectdate').val()).format("YYYY-MM-DD")
							
						}else if($scope.isZonearrivalrateClicked){
							$scope.dataList=[];
							$scope.arrivaldummyList=[];
							$scope.dataList= response.data.footFall;
							$scope.arrivaldummyList=response.data.footFall;
							
							$scope.previousZoneArrival = $scope.zone
							var dateSelected=$scope.typeofdays;
							$scope.dateValueArrival=dateSelected;	
							$scope.viewValueArrival=$scope.viewValue;
							if($scope.dateValueArrival!=0 && $scope.dateValueArrival!=1) {			
								$scope.ArrivalStartDate  = moment().subtract($scope.typeofdays-1, 'days').format("YYYY-MM-DD")
								$scope.ArrivalEndDate = moment(new Date()).format("YYYY-MM-DD")
								$scope.activestartdateArrival= moment().subtract($scope.typeofdays-1, 'days').format("YYYY-MM-DD")
								$scope.activeenddateArrival=moment(new Date()).format("YYYY-MM-DD")
							}
							else if($scope.dateValueArrival==1){
								$scope.activestartdateArrival= moment(new Date()).format("YYYY-MM-DD")
								$scope.activeenddateArrival=moment(new Date()).format("YYYY-MM-DD")
								$scope.ArrivalStartDate = moment(new Date()).format("YYYY-MM-DD")
								$scope.ArrivalEndDate = moment(new Date()).format("YYYY-MM-DD")
							}
							else if($scope.dateValueArrival == 0)
							{
								$scope.activestartdateArrival= moment($('#startdate').val()).format("YYYY-MM-DD");
								$scope.activeenddateArrival=moment($('#enddate').val()).format("YYYY-MM-DD");
						
								$scope.ArrivalStartDate= moment($('#startdate').val()).format("MMM DD, YYYY");
								$scope.ArrivalEndDate=moment($('#enddate').val()).format("MMM DD, YYYY");
								if($scope.typeofdays==0){
									$scope.customdates=true;  				
											$('#startdate').val($scope.ArrivalStartDate);	
											$('#enddate').val($scope.ArrivalEndDate);
								}
							}
							
						}else if($scope.isZoneorgattendanceClicked==true){
							$scope.dataList=[];
							$scope.attendancedummyList=[];
							$scope.dataList= response.data.attendance;
							$scope.attendancedummyList=response.data.attendance;
							
							$scope.viewValueAttendance=$scope.viewValue;
							$scope.dateValueAttendance=moment($('#idPFAEditStartDatePicker').val()).format("MMM DD, YYYY");
							$scope.activestartdateAttendance = moment($('#idPFAEditStartDatePicker').val()).format("YYYY-MM-DD")
							$scope.activeenddateAttendance = moment($('#idPFAEditStartDatePicker').val()).format("YYYY-MM-DD")
							
						}else if($scope.isproductanalysisClicked){
							if(data.zone=='All')
							{
								$scope.dataList = [];
								$scope.productAnalysisList = response.data.zones;
							}else
							{ 
								$scope.productAnalysisList = [];
								
								var dayArray = response.data.production.day;
								for (var i = 0; i < dayArray.length; i++) { 
									var pa_data = {};
									pa_data.incoming = dayArray[i].incoming;
									pa_data.outgoing = dayArray[i].outgoing;
									pa_data.waiting = dayArray[i].waiting;
									pa_data.zoneID = response.data.production.zoneID;
									pa_data.zoneName = response.data.production.zoneName;
									
									$scope.productAnalysisList.push(pa_data);
								} 
								 
							}
							
							$scope.dataList= $scope.productAnalysisList; 
						}
					}
					
						$scope.viewOption();
					},
				
				function error(response) {
						
						$rootScope.hideloading('#loadingBar');						
						$rootScope.fnHttpError(response);
			    });
	   	 
	}
	$scope.displayZoneFlowScreen();
	$scope.showDwellDetails = function(){
					
		$scope.previousEmployee= $scope.emp							
		$scope.viewValueDwell=$scope.viewValue;
		
		var dateSelected=$scope.typeofdays;
		$scope.dateValueDwell= dateSelected;	
		$scope.previousDwellTimeGT = $scope.dwelltimeminities;
		if($scope.dateValueDwell!=0 && $scope.dateValueDwell!=1) {
			$scope.DwellEndDate = moment(new Date()).format("YYYY-MM-DD")
			$scope.DwellStartDate  = moment().subtract($scope.typeofdays-1, 'days').format("YYYY-MM-DD")
			$scope.activestartdateDwell  = moment().subtract($scope.typeofdays-1, 'days').format("YYYY-MM-DD")
			$scope.activeenddateDwell = moment(new Date()).format("YYYY-MM-DD")
			
		}
		else if($scope.dateValueDwell==1){
			$scope.activestartdateDwell  = moment(new Date()).format("YYYY-MM-DD")
			$scope.activeenddateDwell = moment(new Date()).format("YYYY-MM-DD")			
		
			$scope.DwellStartDate = moment(new Date()).format("YYYY-MM-DD")
			$scope.DwellEndDate = moment(new Date()).format("YYYY-MM-DD")
		}
			
		else if($scope.dateValueDwell == 0)
		{
			$scope.activestartdateDwell= moment($('#startdate').val()).format("YYYY-MM-DD");
			$scope.activeenddateDwell=moment($('#enddate').val()).format("YYYY-MM-DD");
			
			$scope.DwellStartDate= moment($('#startdate').val()).format("MMM DD, YYYY");
		    $scope.DwellEndDate=moment($('#enddate').val()).format("MMM DD, YYYY");
			if($scope.typeofdays==0){
				$scope.customdates=true;  				
				$('#startdate').val($scope.DwellStartDate);	
				$('#enddate').val($scope.DwellEndDate);														
						
			}								
		}
		
		$scope.viewOption();
	}

	$scope.getAllZones=function(){
		$http({
			method : 'GET',
			url : './demo/zones',
			headers : {
				'orgName' : $scope.org
			}
		})
		.then(
				function success(response) {
					
					$rootScope.hideloading('#loadingBar');
					if (response != null
							&& response.data != null
							&& response.data != "BAD_REQUEST") { 

						$scope.zoneList = response.data.zone;	
						//$scope.zoneList = response.data;
						$scope.zoneList.sort(GetSortOrder("name"));
						$scope.zone=$scope.zoneList[0].id;
						$scope.zoneName=$scope.zoneList[0].name;
						$scope.displayZoneFlowScreen(); //11Jan
					 }else {
						
						$scope.noList = true;
					}
				}/*
					 * , function error(response) {
					 * $rootScope.hideloading('#loadingBar'); if
					 * (response.status == 403) { alert("Session timed-out or
					 * Invalid. Please logout and login again"); } }
					 */);
	}

	$scope.getAllZones();
	
	$scope.getData = function() {
		if ($scope.org != null && $scope.org != "") {
			$rootScope.showloading('#loadingBar');
			$scope.getQuuppamap();
			
			
 			$scope.empListData();
			//$scope.getAllZones();
			$scope.getZoneClassifications();
			
			 
		} else {   
			// alert('ERROR : Please select organization');

		}
	};

	$scope.getData();

	function isInsidePolygon(point, vs) {
		// ray-casting algorithm based on
		// http://www.ecse.rpi.edu/Homepages/wrf/Research/Short_Notes/pnpoly.html

		var x = point[0], y = point[1];

		var inside = false;
		for (var i = 0, j = vs.length - 1; i < vs.length; j = i++) {
			var xi = vs[i][0], yi = vs[i][1];
			var xj = vs[j][0], yj = vs[j][1];

			var intersect = ((yi > y) != (yj > y))
			&& (x < (xj - xi) * (y - yi) / (yj - yi) + xi);
			if (intersect) inside = !inside;
		}

		return inside;
	};






	function isInsideRect(x1, y1, x2, y2, x, y) {
		if (x > x1 && x < x2 && y > y1 && y < y2)
			return true;

		return false;
	}


	var options = {
			position : 'topright',
			draw : {
				polygon : false,
				polyline : false,
				circlemarker : false,
				circle : false,
				marker : false,
				rectangle : false
			},
			edit : false

	}; 
	$scope.drawControl = new L.Control.Draw(options);
	$scope.drawControlEdit = new L.Control.Draw({
		draw: false
	});
	map.addControl($scope.drawControl);




	$scope.popup = L.popup({
		maxWidth : 400
	});


	var mouseroverid = null;
	function mouseOverPopup(e) {

		if(!$scope.isEdit && !$scope.isDrawEvent){ 

			if(mouseroverid!=e.sourceTarget.id){
				var name = e.sourceTarget.name;
				var restrictions = e.sourceTarget.restrictions;
				var classification = e.sourceTarget.classification;

				if(name==undefined || name==null)
					name = '-';
				if(restrictions==undefined || restrictions==null)
					restrictions = '-';
				if(classification==undefined || classification==null)
					classification = '-';

				var popupContent ='';
				if($scope.isZoneoccupancyClicked==true){
					popupContent = "<strong>Zone :" + name + "</strong><br /><strong>Classification:" +
					classification + "</strong><br /> <strong>Max :" + e.sourceTarget.max + "</strong><br /><strong>Peak :"+e.sourceTarget.peak+"</strong><br /><strong>Avg :"+e.sourceTarget.average+"</strong><br /><strong>Occupancy Rate :"+e.sourceTarget.occupancyRate+" %</strong>";
				} else if($scope.isEmployeedwelltimeClicked==true){
					popupContent = "<strong>Zone :" + name + "</strong><br /><strong>DwellTime :"+e.sourceTarget.dWellTime+"</strong>";
				} else if($scope.isZonetrafficClicked==true) {
					popupContent = "<strong>Zone :" + name + "</strong><br /><strong>Classification:" +
					classification + "</strong><br /> <strong>Max Occupancy:" + restrictions + "</strong><br /><strong>Total traffic :"+e.sourceTarget.totaltraffic+"</strong><br /><strong>Peak traffic :"+e.sourceTarget.peaktraffic+"</strong><br /><strong>Average traffic :"+Math.ceil(e.sourceTarget.averagetraffic)+"</strong>";
				}else if($scope.isZoneorgattendanceClicked==true){
					popupContent = "<strong>Zone :" + name + "</strong><br /><strong>No.of Occupancy:"+e.sourceTarget.tq+"</strong>";

				}else if($scope.isproductanalysisClicked){
					popupContent = "<strong>Zone :" + name + "</strong><br />"+
					"<strong>Incoming:" + e.sourceTarget.incoming + "</strong><br /> "+
					"<strong>Outgoing:" + e.sourceTarget.outgoing + "</strong><br />"+
					"<strong>Waiting:"+e.sourceTarget.waiting+"</strong>"; 
				}

				$scope.popup.setLatLng(e.sourceTarget._bounds.getCenter());
				$scope.popup.setContent(popupContent);
				map.openPopup($scope.popup);
				mouseroverid =e.sourceTarget.id;
			}
		} 
	}



	function mouseOutClosePopup(e) {
		map.closePopup(); 
	}


	function GetSortOrder(prop) {    
		return function(a, b) {    
			if(a[prop]!=null&&b[prop]!=null){
				if (a[prop].toLowerCase() > b[prop].toLowerCase()) {    
					return 1;    
				} else if (a[prop].toLowerCase() < b[prop].toLowerCase()) {    
					return -1;    
				}    
			}
			return 0;    
		}    
	}  
	
	function GetSortOrder1(prop) {
		return function(a, b) {
			if (a[prop] < b[prop]) {
				return 1;
			} else if (a[prop] > b[prop]) {
				return -1;
			}
			return 0;
		}
	}
	
	$scope.customdates=false;

	$scope.SelectBoxChanged = function() {
		// $scope.prevflag = "false";
		
	}
		$scope.checkCustomDate = function() {
			// $scope.prevflag = "false";
			if ($scope.typeofdays == 0) {
				$scope.customdates = true;
				var d = new Date();
				startdate = formatDate(d);
				$scope.s_date = startdate;
				$scope.e_date = startdate;
				startdate = formatDate1($scope.s_date);
				$('#startdate').val(startdate);
				$('#enddate').val(startdate);
				
				
				function formatDate(d) {
					var month = '' + (d.getMonth() + 1), day = ''+ d.getDate(), year = d.getFullYear();
					if (month.length < 2)
						month = '0' + month;
					if (day.length < 2)
						day = '0' + day;
					return [ year, month, day ].join('-');
				}

				// return format Jan 01, 2020
				function formatDate1(date) {
					var mS = [ 'Jan', 'Feb', 'Mar', 'Apr', 'May', 'June','Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec' ];
					var sp_date = date.split("-");			

					return mS[parseInt(sp_date[1]) - 1] + ' ' + sp_date[2]	+ ", " + sp_date[0];
				}

			} else {
				$scope.customdates = false;
				$('#startdate').val('');
				$('#enddate').val('');
			}
		}
	
	
	
$scope.getZoneFinalList = function(keyValue, startdate, enddate) {

		$scope.DAY_ARRAY = [];
		$scope.FINAL_DAY_ARRAY = [];
		
		$scope.zoneStatusFinalList = [];	
		
		function formatDate(d) {
			var month = '' + (d.getMonth() + 1), day = ''
					+ d.getDate(), year = d.getFullYear();

			if (month.length < 2)
				month = '0' + month;
			if (day.length < 2)
				day = '0' + day;

			return [ year, month, day ].join('-');
		}
		
		function convert(str) {
			var date = new Date(str)
            var now_utc = new Date(date.toUTCString().slice(0, -4));
            var mnth = ("0" + (now_utc.getMonth() + 1)).slice(-2);
            var day = ("0" + now_utc.getDate()).slice(-2);
            return [now_utc.getFullYear(), mnth, day].join("-");
		}		
		
		 function convertTimestamp(timestamp, convertint = 1) {
				var fromdate;
				if (convertint === 1)
					fromdate = parseInt(timestamp, 10);
				else fromdate = timestamp;

				var d = new Date(0);				
				d.setUTCSeconds(fromdate); // Convert the passed timestamp to
											// milliseconds
				var now_utc = new Date(d.toUTCString().slice(0, -4));
				var mnth = ("0" + (now_utc.getMonth() + 1)).slice(-2);
				var day = ("0" + now_utc.getDate()).slice(-2);
				return [now_utc.getFullYear(), mnth, day].join("-");
				
			}
		

        var getDatesBetweenDates = (startDate, endDate) => {
            let dates = []
                // to avoid modifying the original date
            const theDate = new Date(startDate);
            const theendDate = new Date(endDate); 

            while (theDate < theendDate) {
                dates = [...dates, new Date(theDate)]
                theDate.setDate(theDate.getDate() + 1)
            }
            dates = [...dates, endDate]
            return dates
        }
		
       
		for (var i = 0; i < $scope.zoneList.length; i++) {
			if($scope.zoneList[i].id == keyValue){
			
			var cDate = convertTimestamp($scope.zoneList[i].created_time);
			var mDate = convertTimestamp($scope.zoneList[i].modified_time);
			var cStatus = $scope.zoneList[i].zoneStatus;
			
									
			if (cStatus == "Active") {				
				if (new Date(cDate).toISOString().slice(0, 10) <= new Date(enddate).toISOString().slice(0, 10)) {
				
					$scope.DAY_ARRAY = getDatesBetweenDates(cDate,enddate);
					
				}

			}
			else if (cStatus == "Inactive") {
				if ((new Date(startdate).toISOString().slice(0, 10) <= new Date(cDate).toISOString().slice(0, 10)) && 
						(new Date(enddate).toISOString().slice(0, 10) <= new Date(cDate).toISOString().slice(0, 10))) {			
										

					if (new Date(cDate).toISOString().slice(0, 10) == new Date(enddate).toISOString().slice(0, 10)) {
						
						$scope.DAY_ARRAY = getDatesBetweenDates(cDate, mDate);
					
					}
				} else {
				
					if (new Date(mDate).toISOString().slice(0, 10) >= new Date(startdate).toISOString().slice(0, 10)) {						
						$scope.DAY_ARRAY = getDatesBetweenDates(cDate, mDate);
						
					}

				}

			}
			
			for (var da = 0; da < $scope.DAY_ARRAY.length; da++) {

				$scope.FINAL_DAY_ARRAY[da] = convert($scope.DAY_ARRAY[da].toString());
				// $scope.FINAL_DAY_ARRAY[da] =$scope.DAY_ARRAY[da].toString();
				
			}
			
		}
		}
		
			
	}


// active for traffic
$scope.trafficgetZoneFinalList = function(keyValue) {		
	
	$scope.trafficDAY_ARRAY = [];
	$scope.trafficFINAL_DAY_ARRAY = [];
	
	$scope.zoneStatusFinalList = [];	
	
	function formatDate(d) {
		var month = '' + (d.getMonth() + 1), day = ''
				+ d.getDate(), year = d.getFullYear();

		if (month.length < 2)
			month = '0' + month;
		if (day.length < 2)
			day = '0' + day;

		return [ year, month, day ].join('-');
	}
	
	function convert(str) {
		var date = new Date(str)
        var now_utc = new Date(date.toUTCString().slice(0, -4));
        var mnth = ("0" + (now_utc.getMonth() + 1)).slice(-2);
        var day = ("0" + now_utc.getDate()).slice(-2);
        return [now_utc.getFullYear(), mnth, day].join("-");
	}		
	
	 function convertTimestamp(timestamp, convertint = 1) {
			var fromdate;
			if (convertint === 1)
				fromdate = parseInt(timestamp, 10);
			else fromdate = timestamp;

			var d = new Date(0);				
			d.setUTCSeconds(fromdate); // Convert the passed timestamp to
										// milliseconds
			var now_utc = new Date(d.toUTCString().slice(0, -4));
			var mnth = ("0" + (now_utc.getMonth() + 1)).slice(-2);
			var day = ("0" + now_utc.getDate()).slice(-2);
			return [now_utc.getFullYear(), mnth, day].join("-");
			
		}
	 
	

    var getDatesBetweenDates = (startDate, endDate) => {
        let dates = []
            // to avoid modifying the original date
        const theDate = new Date(startDate);
        const theendDate = new Date(endDate); 

        while (theDate < theendDate) {
            dates = [...dates, new Date(theDate)]
            theDate.setDate(theDate.getDate() + 1)
        }
        dates = [...dates, endDate]
        return dates
    }
	
   
	for (var i = 0; i < $scope.zoneList.length; i++) {
		if($scope.zoneList[i].id == keyValue){
		
		var cDate = convertTimestamp($scope.zoneList[i].created_time);
		var mDate = convertTimestamp($scope.zoneList[i].modified_time);
		var cStatus = $scope.zoneList[i].zoneStatus;
		
		if (cStatus == "Active") {				
			if (new Date(cDate).toISOString().slice(0, 10) <= new Date($scope.dateValueTraffic).toISOString().slice(0, 10)) {
			
				$scope.trafficDAY_ARRAY = getDatesBetweenDates(cDate, $scope.dateValueTraffic);
				
			}

		}
		else if (cStatus == "Inactive") {
			if ((new Date($scope.dateValueTraffic).toISOString().slice(0, 10) <= new Date(cDate).toISOString().slice(0, 10)) && 
					(new Date($scope.dateValueTraffic).toISOString().slice(0, 10) <= new Date(cDate).toISOString().slice(0, 10))) {			
									

				if (new Date(cDate).toISOString().slice(0, 10) == new Date($scope.dateValueTraffic).toISOString().slice(0, 10)) {
					
					$scope.trafficDAY_ARRAY = getDatesBetweenDates(cDate, mDate);
				
				}
			} else {
			
				if (new Date(mDate).toISOString().slice(0, 10) >= new Date($scope.dateValueTraffic).toISOString().slice(0, 10)) {						
					$scope.trafficDAY_ARRAY = getDatesBetweenDates(cDate, mDate);
					
				}

			}

		}
		
		for (var da = 0; da < $scope.trafficDAY_ARRAY.length; da++) {

			$scope.trafficFINAL_DAY_ARRAY[da] = convert($scope.trafficDAY_ARRAY[da].toString());
			// $scope.FINAL_DAY_ARRAY[da] =$scope.DAY_ARRAY[da].toString();
			
		}
		
	}
	}
	
		
}
	var reN = /[^0-9]/g;

	function sortAlphaNum(a1, b1) {

		var aA = a1.dWellTime;
		var bA =b1.dWellTime;
		if (aA === bA) {
			return aA === bA ? 0 : aA < bA ? 1 : -1;
		} else {
			return aA < bA ? 1 : -1;
		}
	}
	function formatDate(d) {
		var month = '' + (d.getMonth() + 1), day = ''
			+ d.getDate(), year = d.getFullYear();

		if (month.length < 2)
			month = '0' + month;
		if (day.length < 2)
			day = '0' + day;

		return [ year, month, day ].join('-');
	}

	var arr=["12:15 AM", "12:30 AM", "12:45 AM", "01:00 AM", "01:15 AM", "01:30 AM", "01:45 AM",
		"02:00 AM", "02:15 AM", "02:30 AM", "02:45 AM", "03:00 AM", "03:15 AM", "03:30 AM", "03:45 AM", "04:00 AM", "04:15 AM", "04:30 AM",
		"04:45 AM", "05:00 AM", "05:15 AM", "05:30 AM", "05:45 AM", "06:00 AM", "06:15 AM", "06:30 AM ", "06:45 AM", "07:00 AM", "07:15 AM",
		"07:30 AM", "07:45 AM", "08:00 AM", "08:15 AM", "08:30 AM", "08:45 AM", "09:00 AM", "09:15 AM", "09:30 AM", "09:45 AM", "10:00 AM",
		"10:15 AM", "10:30 AM","10:45 AM", "11:00 AM", "11:15 AM", "11:30 AM", "11:45 AM", "12:00 PM", "12:15 PM", "12:30 PM", "12:45 PM", "01:00 PM", "01:15 PM",
		"01:30 PM", "01:45 PM", "02:00 PM", "02:15 PM", "02:30 PM", "02:45 PM", "03:00 PM", "03:15 PM", "03:30 PM", "03:45 PM", "04:00 PM", "04:15 PM",
		"04:30 PM", "04:45 PM", "05:00 PM", "05:15 PM", "05:30 PM", "05:45 PM", "06:00 PM", "06:15 PM", "06:30 PM", "06:45 PM", "07:00 PM",
		"07:15 PM", "07:30 PM", "07:45 PM", "08:00 PM", "08:15 PM", "08:30 PM", "08:45 PM", "09:00 PM", "09:15 PM", "09:30 PM", "09:45 PM", "10:00 PM",
		"10:15 PM", "10:30 PM", "10:45 PM", "11:00 PM", "11:15 PM", "11:30 PM", "11:45 PM","12:00 AM"
		];
	
	
 	
	
	/**
	 * Description sort the string based on number attached to it.
	 * 
	 * @function sortAlphaNum1
	 * @access private
	 * @since 1.0.0
	 * @static
	 * @param a1 -
	 *            string 1
	 * @param b1 -
	 *            string 2
	 * @returns 1 or -1
	 */

	function sortAlphaNum1(a1, b1) {

		var reA = /[^a-zA-Z]/g;
		var reN = /[^0-9]/g;

		var a = a1.uId;
		var b = b1.uId;
		var aA = a.replace(reA, "");
		var bA = b.replace(reA, "");
		if (aA === bA) {
			var aN = parseInt(a.replace(reN, ""), 10);
			var bN = parseInt(b.replace(reN, ""), 10);
			return aN === bN ? 0 : aN > bN ? 1 : -1;
		} else {
			return aA > bA ? 1 : -1;
		}
	}
	$scope.checkNetconnection=function(){
	
	      return $window.navigator.onLine;
  }
	
	$scope.exportExcelData=function(){
		if ($scope.checkNetconnection() == false) { 
	         // alert("Please check the Internet Connection");
	          $.alert({
                  title: $rootScope.MSGBOX_TITLE,
                  content: "Please check the Internet Connection",
                  icon: 'fa fa-info-circle',
                   });
	            return;
	        }   
	   	 else{
		var export_date = new Date();
		var exportDD = export_date
				.getDate();
		var exportMM = export_date
				.getMonth() + 1;
		var exportYYYY = export_date
				.getFullYear();

		if (exportDD.length < 2) {
			exportDD = "0" + exportDD;
		}
		if (exportMM.length < 2) {
			exportMM = "0" + exportMM;
		} 
		
		var exportDate = moment(export_date).format("MMM DD, YYYY")
		
		var exportHour = export_date.getHours();
		var exportMinutes = export_date.getMinutes();
		if (exportMinutes < 10) {
			exportMinutes = "0"
					+ exportMinutes;
		}
		var exportAmPm = "AM";
		if (exportHour > 12) { 
			exportHour -= 12;
			exportAmPm = "PM";
		} else if (exportHour === 12) {
			exportAmPm = "PM";
		} else if (exportHour === 0) {
			exportHour += 12;
		}

		var exportTime1 = exportHour + ":"
				+ exportMinutes + " "
				+ exportAmPm;
		var headerArray=[];
		var detailarray=[];
		var descarray=[];
		 detailarray=['Report Generated By','Organization','Report Generated Date & Time','Report'];
		descarray=[$scope.user.userName,$scope.org ,exportDate+ ", "+exportTime1,$scope.exportDetailTitle];
		
		
		for(var i=0; i<detailarray.length; i++){
			headerArray.push( {
				'Details': detailarray[i],				
				'Description':descarray[i] ,				
			});		    
		}
		
		
		var export_file_name = $scope.filename	+ "_"+ exportDate + "_"	+ exportTime1 + ".xlsx";
		var ws = XLSX.utils.json_to_sheet($scope.exportArrayData);	
		var wscols = [
		    {wch:20},
		    {wch:10}, 
		    {wch:10},
		    {wch:10},
		    {wch:10},
		    {wch:10},
		    {wch:10},
		];
		var ws1 = XLSX.utils.json_to_sheet(headerArray);
		var wscols1 = [
		    {wch:30},
		    {wch:35},   
		];
		ws['!cols'] = wscols;
		ws1['!cols'] = wscols1;
		var wb = XLSX.utils.book_new();
		XLSX.utils.book_append_sheet(wb, ws, $scope.sheetname);		
		XLSX.utils.book_append_sheet(wb, ws1, "Export Details");		
		XLSX.writeFile(wb,export_file_name );	
		}
	
	}
	$scope.exportPDFData=function(){
		if ($scope.checkNetconnection() == false) { 
	        // alert("Please check the Internet Connection");
			 $.alert({
                 title: $rootScope.MSGBOX_TITLE,
                 content: "Please check the Internet Connection",
                 icon: 'fa fa-info-circle',
                  });
	            return;
	        }   
	   	 else{
		function getTarget(e) {
			  return e.target || e.srcElement;			  
			}
		var export_date = new Date();
		var exportDD = export_date
				.getDate();
		var exportMM = export_date
				.getMonth() + 1;
		var exportYYYY = export_date
				.getFullYear();

		if (exportDD.length < 2) {
			exportDD = "0" + exportDD;
		}
		if (exportMM.length < 2) {
			exportMM = "0" + exportMM;
		} 
		
		var exportDate = moment(export_date).format("MMM DD, YYYY")
		
		var exportHour = export_date.getHours();
		var exportMinutes = export_date.getMinutes();
		if (exportMinutes < 10) {
			exportMinutes = "0"
					+ exportMinutes;
		}
		var exportAmPm = "AM";
		if (exportHour > 12) { 
			exportHour -= 12;
			exportAmPm = "PM";
		} else if (exportHour === 12) {
			exportAmPm = "PM";
		} else if (exportHour === 0) {
			exportHour += 12;
		}

		var exportTime1 = exportHour + ":" + exportMinutes + " " + exportAmPm;
		
		// to export as PDF
		var data = document.getElementById('chartview');	
		html2canvas(data).then(canvas => {
			var imgWidth = 208;
			var imgHeight = canvas.height * imgWidth / canvas.width;
			const contentDataURL = canvas.toDataURL('image/png')
			let pdf = new jspdf('l', 'mm', 'a4');
			var position = 0;

			if($scope.isproductanalysisClicked  && $scope.viewValue=='Aging Chart' || $scope.isproductanalysisClicked  && $scope.viewValue=='Current Inventory' || $scope.isproductanalysisClicked  && $scope.viewValue=='Column Chart'){ 
				domtoimage.toPng(document.getElementById('idChartAgingCanvas')).then(function (chartImage) {
					var img = new Image();				
					img.src = chartImage;
					pdf.setFontSize(12);
					pdf.text(110, 7, $scope.exportDetailTitle);
					pdf.setLineWidth(0.1);
					pdf.line(0, 10, 800, 10);
					pdf.setFontSize(11);
					pdf.text(20, 15, "Date : " + $scope.exportDetailTitle);
					pdf.setFontSize(11);
					pdf.text(20, 20, "Report Generated by : " + $scope.user.userName);					
					pdf.setFontSize(11);
					pdf.text(20, 25, "Report Generated on : " + exportDate + ", "	+ exportTime1);				
					pdf.setLineWidth(0.1);	       
					pdf.line(0, 30, 800, 33);
					pdf.addImage(chartImage, 'PNG', 5, 31, 250, 130);					
					pdf.save($scope.filename	+ "_"+ exportDate + " "	+ exportTime1 + ".pdf");
				});		
			}else if($scope.isMaterialTraceClicked){ 
				domtoimage.toPng(document.getElementById('chart-area')).then(function (chartImage) {
					var img = new Image();				
					img.src = chartImage;
					pdf.setFontSize(12);
					pdf.text(110, 7, $scope.exportDetailTitle);
					pdf.setLineWidth(0.1);
					pdf.line(0, 10, 800, 10);
					pdf.setFontSize(11);
					pdf.text(20, 15, "Date : " + $scope.exportDetailTitle);
					pdf.setFontSize(11);
					pdf.text(20, 20, "Report Generated by : " + $scope.user.userName);					
					pdf.setFontSize(11);
					pdf.text(20, 25, "Report Generated on : " + exportDate + ", "	+ exportTime1);				
					pdf.setLineWidth(0.1);	       
					pdf.line(0, 30, 800, 33);
					pdf.addImage(chartImage, 'PNG', 5, 31, 250, 130);	
					pdf.save($scope.filename	+ "_"+ exportDate + " "	+ exportTime1 + ".pdf");
				});		
			}					
			else{
				domtoimage.toPng(document.getElementById('chart-Test'))
				.then(function (chartImage) {
					var img = new Image();				
					img.src = chartImage;
					pdf.setFontSize(12);
					pdf.text(110, 7, $scope.exportDetailTitle);
					pdf.setLineWidth(0.1);
					pdf.line(0, 10, 800, 10);
					pdf.setFontSize(11);
					pdf.text(20, 15, "Date : " + $scope.exportDetailTitle);
					pdf.setFontSize(11);
					pdf.text(20, 20, "Report Generated by : " + $scope.user.userName);					
					pdf.setFontSize(11);
					pdf.text(20, 25, "Report Generated on : " + exportDate + ", "	+ exportTime1);				
					pdf.setLineWidth(0.1);	       
					pdf.line(0, 30, 800, 33);					
					pdf.addImage(chartImage, 'PNG', 5, 31, 250, 130);	
					pdf.save($scope.filename	+ "_"+ exportDate + " "	+ exportTime1 + ".pdf");
				});		
			}			
		});
/*
 * //to export as image domtoimage.toPng(document.getElementById('chart-area'))
 * .then(function (blob) { window.saveAs(blob, 'my-node3.png'); });
 */
		
	   	 }			  

}
	$scope.generateDateTime=function(){
		
		var export_date = new Date();
		var exportDD = export_date.getDate();
		var exportMM = export_date.getMonth() + 1;
		var exportYYYY = export_date.getFullYear();

		if (exportDD.length < 2) {	exportDD = "0" + exportDD;	}
		if (exportMM.length < 2) {	exportMM = "0" + exportMM;	} 	
				
		var exportHour = export_date.getHours();
		var exportMinutes = export_date.getMinutes();
		if (exportMinutes < 10) {exportMinutes = "0"+ exportMinutes;	}
		var exportAmPm = "AM";
		if (exportHour > 12) { 
			exportHour -= 12;
			exportAmPm = "PM";
		} else if (exportHour === 12) {
			exportAmPm = "PM";
		} else if (exportHour === 0) {
			exportHour += 12;
		}
		var exportDate = moment(export_date).format("MMM DD, YYYY");
		var exportTime1 = exportHour + ":" + exportMinutes + " " + exportAmPm;
		$scope.generateDateTimeDisplay = "Report Generated on "+	exportDate  +", "+ 	exportTime1; 			  

}
	 
	$scope.loadDwellTableView = function(){
		$scope.loadColumnchart();
		$('#chart-area').empty();
		// data to display in table view
		for(n=0; n<$scope.empList.length; n++){
		$scope.tableViewArray[n] =  {
			'tagType': $scope.userIdList[n],
			'primaryZone': $scope.primaryZone[n],
			'secondaryZone': $scope.secondaryZone[n],
			'thirdZone': $scope.thirdZone[n],
			'pdwellValue':$scope.pdwellValue[n], 
			'sdwellValue':$scope.sdwellValue[n],
			'tdwellValue':$scope.tdwellValue[n],
			'odwellValue':$scope.odwellValue[n],
			
	};		
		
		}
		$scope.totalItems = $scope.tableViewArray.length;
		$scope.currentPage = 1;
		$scope.getTotalPages();
		$scope.tagTypeList = [];
		
		if ($scope.tableViewArray.length > $scope.numPerPage)
			$scope.tagTypeList =$scope.tableViewArray
					.slice(0, $scope.numPerPage);
		else
			$scope.tagTypeList = $scope.tableViewArray;	
		
		
		$scope.exportXLS = true;
		$scope.exportPDF = false;
	
	}
	$scope.currentPage = 1;
	$scope.numPerPage = 10;
	$scope.maxSize = 3;
	$scope.totalItems = 0;
	$scope.tableViewArray1 = [];
	$scope.tableViewArray = [];
	$scope
			.$watch(
					'currentPage + numPerPage',
					function() {
						var begin = (($scope.currentPage - 1) * $scope.numPerPage), end = begin
								+ $scope.numPerPage;
						
						if ($scope.isproductanalysisClicked)
						{
							if ($scope.productanAlysisList != undefined){
								$scope.productanFilterAlysisList = $scope.productanAlysisList
								.slice(begin, end);
							}
						}else
							$scope.tagTypeList = $scope.tableViewArray
								.slice(begin, end);
						
						if($scope.currentPage==1)
							$scope.pageStartNo = 0;
						else 
							$scope.pageStartNo =  ($scope.currentPage-1) * $scope.numPerPage ;


					});

	$scope.currentPage = 1;

	$scope.maxSize = 3;
	$scope.pageStartNo = 0;
	$scope.numPerPage = 10;
	$scope.totalItems = 0;
	$scope.totalPages = 0;

	$scope.hasNext = true;
	$scope.hasPrevious = true;
	$scope.isFirstPage = true;
	$scope.isLastPage = true;
	$scope.serial = 1;

	$scope
			.$watch(
					"currentPage",
					function(newVal, oldVal) {
						if (newVal !== oldVal) {

							if (newVal > $scope.totalPages) {
								$scope.currentPage = $scope.totalPages;
							} else {
								$scope.currentPage = newVal;
							}

							$scope.serial = (($scope.currentPage - 1) * $scope.numPerPage) + 1;

						}
					});

	$scope.enablePageButtons = function() {
		$scope.serial = (($scope.currentPage - 1) * $scope.numPerPage) + 1;
		$scope.selectedRow = -1;
		if ($scope.totalPages <= 1) {
			$scope.hasPrevious = true;
			$scope.isFirstPage = true;
			$scope.hasNext = true;
			$scope.isLastPage = true;

		} else if ($scope.totalPages > 1
				&& $scope.currentPage == $scope.totalPages) {

			$scope.hasPrevious = false;
			$scope.isFirstPage = false;
			$scope.hasNext = true;
			$scope.isLastPage = true;
		} else if ($scope.currentPage != 1
				&& $scope.currentPage < $scope.totalPages) {

			$scope.hasPrevious = false;
			$scope.isFirstPage = false;
			$scope.hasNext = false;
			$scope.isLastPage = false;

		} else {
			$scope.hasPrevious = true;
			$scope.isFirstPage = true;
			$scope.hasNext = false;
			$scope.isLastPage = false;
		}
	};

	$scope.getTotalPages = function() {

		if (($scope.totalItems % $scope.numPerPage) > 0)
			$scope.totalPages = parseInt($scope.totalItems
					/ $scope.numPerPage) + 1;
		else
			$scope.totalPages = parseInt($scope.totalItems
					/ $scope.numPerPage);

		$scope.enablePageButtons();

	};

	$scope.goFirstPage = function() {
		$scope.currentPage = 1;
		$scope.enablePageButtons();
	};

	$scope.goPreviousPage = function() {
		if ($scope.currentPage > 1)
			$scope.currentPage = $scope.currentPage - 1;
		$scope.enablePageButtons();
	};

	$scope.goNextPage = function() {
		if ($scope.currentPage <= $scope.totalPages)
			$scope.currentPage = $scope.currentPage + 1;
		$scope.enablePageButtons();
	};

	$scope.goLastPage = function() {
		$scope.currentPage = $scope.totalPages;
		$scope.enablePageButtons();
	};
	

});
