var app = angular.module('app', [ 'ngRoute', 'ngResource', 'ngCookies', 
		'ui.bootstrap', 'ngFileUpload', 'smart-table','angular.css.injector','ngIdle','ui.mask' ]);

app.run([ '$rootScope', '$cookies', '$http','Idle',
		function($rootScope, $cookies, $http,Idle) {
			$rootScope.error = false;
			$rootScope.success = false;
			$http.defaults.headers.post['X-CSRFToken'] = $cookies.csrftoken;
		} ]);
		
app.config(function($routeProvider,IdleProvider, KeepaliveProvider) {
	$routeProvider
 
	.when('/home', {
		templateUrl : './views/home.html'
	}).when('/login', {
		templateUrl : './views/login.html'
	}).when('/configuration', {
		templateUrl : './views/configuration.html'
	//}).when('/changePassword', { //moved to profile screen and removed
	//	templateUrl : './views/changePassword.html'
	}).when('/forgotPassword', {
		templateUrl : './views/forgotPassword.html'
	}).when('/dashboard', {
		templateUrl : './views/dashboard.html'
	}).when('/register', {
		templateUrl : './views/register.html'
	}).when('/liveviewmobile/:org/:serviceprovider/:sessionid', {
		templateUrl : './views/liveviewmobile.html'   
	}).when('/topologyEditor', {
		templateUrl : './views/topologyEditor.html'
	}).when('/zoneManagement', {
		templateUrl : './views/zoneManagement.html'
	}).when('/locatorInfo', {
		templateUrl : './views/locatorInfo.html'
    }).when('/peopleType', {
        templateUrl : './views/graphView.html'
    }).when('/tagPeople', {
        templateUrl : './views/tagPeople.html'
    }).when('/tagAsset', {
        templateUrl : './views/tagAsset.html'
    }).when('/tagWorkorder', {
        templateUrl : './views/tagWorkorder.html'
    }).when('/employee', {
        templateUrl : './views/employee.html'
    }).when('/otherUser', {
        templateUrl : './views/otherUser.html'
    }).when('/plantTraffic', {
        templateUrl : './views/graphView.html'
    }).when('/assetChrono', {
        templateUrl : './views/assetChrono.html'
    }).when('/assetNonChrono', {
        templateUrl : './views/assetNonChrono.html'
    }).when('/department', {
        templateUrl : './views/department.html'
    }).when('/material', {
        templateUrl : './views/material.html'
    }).when('/classification', {
        templateUrl : './views/classification.html'
    }).when('/pathway', {
        templateUrl : './views/pathway.html'
    }).when('/assetVehicle', {
        templateUrl : './views/assetVehicle.html'
    }).when('/calibration', {
        templateUrl : './views/calibration.html'
    }).when('/workstation', {
        templateUrl : './views/workstation.html'
    }).when('/agingChart', {
        templateUrl : './views/graphView.html'
    }).when('/constraintAnalytics', {
        templateUrl : './views/graphView.html'
    }).when('/inventoryChart', {
        templateUrl : './views/graphView.html'
    }).when('/zoneFlowAnalytics', {
        templateUrl : './views/graphView.html'
    }).when('/workOrderFlowAnalytics', {
        templateUrl : './views/graphView.html'
    }).when('/assetTimeAnalytics', {
        templateUrl : './views/graphView.html'
    }).when('/accessmanagement', {
        templateUrl : './views/accessmanagement.html'
    }).when('/accountCreation', {
        templateUrl : './views/accountCreate.html'
    }).when('/plantOccupancy', {
        templateUrl : './views/graphView.html' 
    }).when('/alertInfo', {
        templateUrl : './views/alertInfo.html' 
    }).when('/peopleLiveView', {
        templateUrl : './views/liveView.html' 
    }).when('/peopleLiveView/:macId', {
        templateUrl : './views/liveView.html' 
    }).when('/assetLiveView', {
        templateUrl : './views/liveView.html' 
    }).when('/assetLiveView/:macId', {
        templateUrl : './views/liveView.html' 
    }).when('/workorderLiveView', {
        templateUrl : './views/liveView.html' 
    }).when('/workorderLiveView/:macId', {
        templateUrl : './views/liveView.html' 
    }).when('/live', {
        templateUrl : './views/liveView.html' 
    }).when('/skuLiveView', {
        templateUrl : './views/liveView.html' 
    }).when('/skuLiveView/:macId', {
        templateUrl : './views/liveView.html' 
    }).when('/serverLog', {
        templateUrl : './views/serverLog.html' 
    }).when('/employeeProfile', {
        templateUrl : './views/employeeLogProfile.html' 
    }).when('/assetProfile', {
        templateUrl : './views/assetProfile.html' 
    }).when('/visitorProfile', {
        templateUrl : './views/visitorProfile.html' 
    }).when('/vehicleProfile', {
        templateUrl : './views/vehicleProfile.html' 
    }).when('/genie', {
        templateUrl : './views/genie.html' 
    }).when('/userProfile', {
        templateUrl : './views/userProfile.html' 
    }).when('/notification', {
        templateUrl : './views/notification.html' 
    }).when('/materialSKU', {
        templateUrl : './views/sku.html' 
    }).when('/skuTransaction', {
        templateUrl : './views/skuTransaction.html' 
    }).when('/tagSKU', {
        templateUrl : './views/tagSKU.html' 
    }).when('/tag', {
        templateUrl : './views/configurationTag.html' 
    }).when('/tagWarehouse', {
        templateUrl : './views/warehouseTag.html' 
    }).when('/materialWarehouse', {
        templateUrl : './views/warehouseMaterial.html' 
    }).when('/warehouseLiveView', {
        templateUrl : './views/liveView.html' 
    }).when('/ei40Analytics', {
        templateUrl : './views/graphViewDemo.html' 
    }).when('/shopFloorAnalysis', {
        templateUrl : './views/shopFloorAnalysis.html' 
    }).when('/liveDemo', {
        templateUrl : './views/shopFloorAnalysis.html' 
    }).when('/peopleLiveViewDemo', {
        templateUrl : './views/shopFloorAnalysis.html' 
    }).when('/assetLiveViewDemo', {
        templateUrl : './views/shopFloorAnalysis.html' 
    }).when('/workorderLiveViewDemo', {
        templateUrl : './views/shopFloorAnalysis.html' 
    }).when('/skuLiveViewDemo', {
        templateUrl : './views/shopFloorAnalysis.html' 
    }).when('/warehouseLiveViewDemo', {
        templateUrl : './views/shopFloorAnalysis.html' 
    }).otherwise({
		redirectTo : '/login'  
	});
        IdleProvider.idle(1500); //1500 25 min because session timeout is 30 mins. logout before 5 mins when there is no operations in screens
        IdleProvider.timeout(0);
        //KeepaliveProvider.interval(5); // heartbeat every 10 min
        IdleProvider.interrupt('keydown wheel mousedown touchstart touchmove scroll mousemove');
        IdleProvider.windowInterrupt('focus');

	//$locationProvider.html5Mode(true);
      });

