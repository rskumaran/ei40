app
		.controller(
				'warehouseTagController',
				function($scope, $http, $window, $rootScope, $location,
						$timeout) {
					$scope.user = JSON.parse($window.sessionStorage
							.getItem("user"));
					if ($scope.user == null) {
						$location.path('/login');
						return;
					}

					if ($scope.authen == undefined) {
						$scope.authen = {};
					}
					if ($scope.fieldName == undefined) {
						$scope.fieldName = {};
					} 
					

					$scope.trackMacId = '';
					$scope.assignedTagMacId = '';
					$scope.checkTagMacId = {};
					$scope.checkTagMacId = {};
					$scope.checkTagTagId = {};
					$scope.checkTagType = {};
					//$scope.checkTagPeopleId = {};
					$scope.tagData = new Array();
					//$scope.peopleIdTable = [];
					$scope.checktagIdTable = [];
					$scope.tagDataFilterList = new Array();
					//$scope.locationList = [];
					$scope.zoneDetails = [];
					$scope.searchText = "";
					//var menudata = [];
					$scope.searchButton = true;
					
					// input field validation

					$("#id_skuNoAssignTag, #id_skuBatchNoAssignTag, #checktagIdSelect").keypress(
							function(e) {
								// if the letter is not alphabets then display
								// error
								// and don't type anything
								$return = ((e.which > 64) && (e.which < 91))
										|| ((e.which > 96) && (e.which < 123))
										|| (e.which == 8) || (e.which == 32)
										|| (e.which >= 48 && e.which <= 57)
								if (!$return)

								{

									return false;
								}
							});

					
					// to enable/disable sub menus
					$scope.user = JSON.parse($window.sessionStorage
							.getItem("user"));
					$scope.showLocateBtn = false;
					if ($scope.user.capabilityDetails != undefined
							&& $scope.user.capabilityDetails != null) {
						
						
						if ($scope.user.capabilityDetails.HTED == 1) {
							$scope.showEditBtn = true;
						} else {
							$scope.showEditBtn = false;
						}
						
						if ($scope.user.capabilityDetails.HKLV == 1) {
							$scope.showLocateBtn = true;
						}
						
					}

					//$scope.showEditBtn = false;
					$rootScope.checkNetconnection = function() {

						return $window.navigator.onLine;
					}
					$scope.searchOption = function() {
						$scope.filterOption();
					}

					$scope.filterOption = function() {

						$scope.tagViewData = [];
						$scope.tagDataFilterList = [];
 
						
						for (var i = 0; i < $scope.tagData.length; i++) {

							if ($scope.searchText == '') {
								$scope.tagDataFilterList
										.push($scope.tagData[i]);
							} else {
								if (($scope.tagData[i].tagId != undefined
										&& $scope.tagData[i].tagId != null ? $scope.tagData[i].tagId
										: "").toLowerCase().includes(
										$scope.searchText.toLowerCase())
										/*|| ($scope.tagData[i].groupId != undefined
												&& $scope.tagData[i].groupId != null ? $scope.tagData[i].groupId
												: "").toLowerCase()
												.includes(
														$scope.searchText
																.toLowerCase())*/
										|| ($scope.tagData[i].macId != undefined
												&& $scope.tagData[i].macId != null ? $scope.tagData[i].macId
												: "").toLowerCase()
												.includes(
														$scope.searchText
																.toLowerCase())
										/*|| ($scope.tagData[i].manufacturer != undefined
												&& $scope.tagData[i].manufacturer != null ? $scope.tagData[i].manufacturer
												: "").toLowerCase()
												.includes(
														$scope.searchText
																.toLowerCase())*/
										|| ($scope.tagData[i].status != undefined
												&& $scope.tagData[i].status != null ? $scope.tagData[i].status
												: "").toLowerCase()

												.includes(
														$scope.searchText
																.toLowerCase())
										|| ($scope.tagData[i].assignmentId != undefined
												&& $scope.tagData[i].assignmentId != null ? $scope.tagData[i].assignmentId
												: "").toLowerCase()
												.includes(
														$scope.searchText
																.toLowerCase())
										|| ($scope.tagData[i].type != undefined
												&& $scope.tagData[i].type != null ? $scope.tagData[i].type
												: "").toLowerCase()
												.includes(
														$scope.searchText
																.toLowerCase())
										|| ($scope.tagData[i].assignmentDate != undefined
												&& $scope.tagData[i].assignmentDate != null ? $scope.tagData[i].assignmentDate
												: "").toLowerCase()
												.includes(
														$scope.searchText
																.toLowerCase())
										/*|| ($scope.tagData[i].createdDate != undefined
												&& $scope.tagData[i].createdDate != null ? $scope.tagData[i].createdDate
												: "").toLowerCase()
												.includes(
														$scope.searchText
																.toLowerCase())*/
										|| ($scope.tagData[i].modifiedDate != undefined
												&& $scope.tagData[i].modifiedDate != null ? $scope.tagData[i].modifiedDate
												: "").toLowerCase()
												.includes(
														$scope.searchText
																.toLowerCase())) {
									$scope.tagDataFilterList
											.push($scope.tagData[i]);

								}

							}

						}

						$scope.totalItems = $scope.tagDataFilterList.length;
						$scope.currentPage = 1;
						$scope.getTotalPages();
						$scope.getAssignedTagIds($scope.tagDataFilterList);
						if ($scope.tagData.length == 0
								|| $scope.tagDataFilterList.length == 0) {
							$scope.tagNoData = true;

						} else {

							$scope.tagNoData = false;

							if ($scope.tagDataFilterList.length > $scope.numPerPage) {
								$scope.tagViewData = $scope.tagDataFilterList
										.slice(0, $scope.numPerPage);
								$scope.PaginationTab = true;
							} else {
								$scope.tagViewData = $scope.tagDataFilterList;
								$scope.PaginationTab = false;
							}

						}
					}// kumar 02/Jul filter option <-

					$scope.editWareHouseTag = function() {
						$scope.wareHouseTagEditScreen = false;
						$scope.wareHouseTagUpdateScreen = true;
						$scope.searchButton = false;
						if ($scope.authen.status == "Assigned"
								|| $scope.authen.status == "Blocked"
								|| $scope.authen.status == "Overdue")
							$scope.editStatusAssignmentType = true;
						else if ($scope.authen.status == "Free"
								|| $scope.authen.status == "NotWorking"
								|| $scope.authen.status == "Expired")
							$scope.editStatusAssignmentType = false;
					}

					
					// allowable zone list API
				
					var reA = /[^a-zA-Z]/g;
					var reN = /[^0-9]/g;

					function sortAlphaNum(a1, b1) {
						var a = a1.tagId;
						var b = b1.tagId;
						var aA = a.replace(reA, "");
						var bA = b.replace(reA, "");
						if (aA === bA) {
							var aN = parseInt(a.replace(reN, ""), 10);
							var bN = parseInt(b.replace(reN, ""), 10);
							return aN === bN ? 0 : aN > bN ? 1 : -1;
						} else {
							return aA > bA ? 1 : -1;
						}
					}
					$scope.getTagData = function() {
						if (!$rootScope.checkNetconnection()) { 
							$rootScope.alertDialogChecknet();
							return; 
							}
						$rootScope.showloading('#loadingBar');
						$http({
							method : 'GET',
							url : './tag',

						})
								.then(
										function(response) {
											$rootScope
													.hideloading('#loadingBar');
											if (response != null
													&& response.data != null
													&& response.data.length != 0
													&& response.data != "BAD_REQUEST"
													&& response.data != undefined
													&& response.data.length != 0) {

												$scope.tagData.length = 0;
												response.data
														.sort(sortAlphaNum);
												for (var i = 0; i < response.data.length; i++) {
													var fromTime = "", toTime = "",createdDate="",modifiedDate="",assignmentDate="";

													if (response.data[i].fromTime != undefined
															&& response.data[i].fromTime != null
															&& response.data[i].fromTime != ""
															) {
														fromTime = response.data[i].fromTime;
													}
													if (response.data[i].toTime != undefined
															&& response.data[i].toTime != null
															&& response.data[i].toTime != ""
															) {
														toTime = response.data[i].toTime;
													}
													if (response.data[i].createdDate != undefined
															&& response.data[i].createdDate != null
															&& response.data[i].createdDate != ""
															) {
														response.data[i].createdDate = response.data[i].createdDate
																.substring(
																		0,
																		response.data[i].createdDate.length - 9);
														 createdDate = moment(
																response.data[i].createdDate,"YYYY-MMM-DD")
																.format(
																		"MMM DD, YYYY");
													}
													if ( response.data[i].modifiedDate != undefined
															&& response.data[i].modifiedDate != null
															&& response.data[i].modifiedDate != ""
															) {
														response.data[i].modifiedDate = response.data[i].modifiedDate
																.substring(
																		0,
																		response.data[i].modifiedDate.length - 9);
														 modifiedDate = moment(
																response.data[i].modifiedDate,"YYYY-MMM-DD")
																.format(
																		"MMM DD, YYYY");
													}
													if (response.data[i].assignmentDate != undefined 
															&& response.data[i].assignmentDate != null
															&& response.data[i].assignmentDate != ""
															) {
														response.data[i].assignmentDate = response.data[i].assignmentDate
																.substring(
																		0,
																		response.data[i].assignmentDate.length - 9);
														assignmentDate = moment(
																response.data[i].assignmentDate,"YYYY-MMM-DD")
																.format(
																		"MMM DD, YYYY");
													}
													var tagType = response.data[i].type
													if (response.data[i].type == "WareHouse")
														tagType = "Warehouse Material";
													

													var skuTagType = response.data[i].type;
													var tagStatus = response.data[i].status;
													if (skuTagType == "WareHouse"|| tagStatus == "Free"
															|| (tagStatus == "Lost" && (skuTagType == "WareHouse" ))) {
														$scope.tagData
																.push({
																	'tagId' : response.data[i].tagId,
																	'groupId' : response.data[i].groupId,
																	'macId' : response.data[i].macId,
																	'manufacturer' : response.data[i].manufacturer,
																	'status' : response.data[i].status,
																	'assignmentId' : response.data[i].assignmentId,
																	'assignmentDate' : assignmentDate,
																	'fromTime' : fromTime,
																	'toTime' : toTime,
																	'createdDate' : createdDate,
																	'modifiedDate' : modifiedDate,
																	'major' : response.data[i].major,
																	'minor' : response.data[i].minor,
																	'type' : tagType,
																	'restart' : response.data[i].restart
																});
													}
												}
												
												$scope.fetchTagIdTable = [];
												for (var k = 0; k < response.data.length; k++) {
													var varTagIdRec = {};
													if (response.data[k].tagId != undefined
															&& response.data[k].tagId != ''
															&& response.data[k].tagId != null) {
														if (response.data[k].type != undefined
																&& response.data[k].type != ''
																&& response.data[k].type != null) {
															if (response.data[k].type == "LSLM"
																	|| response.data[k].type == "Inventory") {
																varTagIdRec['id'] = response.data[k].tagId
																varTagIdRec['text'] = $scope.varTagIdTextAssigned
																		+ response.data[k].tagId
																		+ '<div>';
																$scope.fetchTagIdTable
																		.push(varTagIdRec);
															}
														}
														if (response.data[k].status != undefined
																&& response.data[k].status != ''
																&& response.data[k].status != null) {
															if (response.data[k].status == "Free") {

																// $scope.checktagIdTable.push(response.data[k].tagId);
																varTagIdRec['id'] = response.data[k].tagId
																varTagIdRec['text'] = $scope.varTagIdTextFree
																		+ response.data[k].tagId
																		+ '<div>';
																$scope.fetchTagIdTable
																		.push(varTagIdRec);
															}

														}
													}

												}
												
												if ($scope.fetchTagIdTable.length == 0) {
													varTagIdRec['id'] = "NoTag";
													varTagIdRec['text'] = $scope.varTagIdTextFree
															+ 'No Tags Found'
															+ '<div>';

												}
											
												if ($scope.tagData.length == 0) {
													$scope.tagNoData = true;
													return;
												} else {
													$scope.tagNoData = false;
												}

												if ($scope.tagData.length > 10) {
													$scope.PaginationTab = true;
												} else {
													$scope.PaginationTab = false;
												}
												$scope.totalItems = $scope.tagData.length;

												$scope.getTotalPages();
												$scope.filterOption();
												$scope.getAssignedTagIds($scope.tagData);

												$scope.currentPage = 1;
												if ($scope.tagData.length > $scope.numPerPage) {
													$scope.tagViewData = $scope.tagData
															.slice(
																	0,
																	$scope.numPerPage);
													$scope.hasPrevious = true;
													$scope.isFirstPage = true;
													$scope.hasNext = false;
													$scope.isLastPage = false;
												} else {
													$scope.tagViewData = $scope.tagData;
												}
											} else {
												$scope.tagNoData = true;
											}
										},
										function error(response) {
											$rootScope.hideloading('#loadingBar');
											$rootScope.fnHttpError(response);
										});

					}

					// $scope.getTagData();

					// assign Tag

					

					$scope.trackTag = function() {
						if($scope.assignedTagMacId!=undefined && $scope.assignedTagMacId!=null 
								&& $scope.assignedTagMacId!='')
						{
							$rootScope.showFullView();
							if ($scope.user.role == "DemoUser")
							{
								$window.sessionStorage.setItem("menuName", "shopFloorAnalysis");
								$window.sessionStorage.setItem("demoMenuName", "skuLiveView");
								$location.path('/shopFloorAnalysis');
							}else{
								$window.sessionStorage.setItem("menuName",
										"skuLiveView");
								$location.path('/skuLiveView/'
									+ $scope.assignedTagMacId);
							}
						}else{
							toastr.error('No assigned tags to track.', "EI4.0");  
						}

					}
					
					$scope.trackSingleTag = function() {
						if ($scope.trackMacId != undefined
								&& $scope.trackMacId != null
								&& $scope.trackMacId != '') {
							
							$rootScope.trackingMacId = $scope.trackMacId;
							$rootScope.showFullView();
							if ($scope.user.role == "DemoUser")
							{
								$window.sessionStorage.setItem("menuName", "shopFloorAnalysis");
								$window.sessionStorage.setItem("demoMenuName", "warehouseLiveView");
								$location.path('/shopFloorAnalysis');
							}else{
								$window.sessionStorage.setItem("menuName",
										"skuLiveView");
								$location.path('/skuLiveView');
							}
						} else {
							toastr.error('No assigned tags to track.', "EI4.0");
						}
					}

					$scope.rowHighilited = function(row) {
						$scope.selectedRow = row;
						$scope.trackMacId = ''; 

						var a = new Array();
						var b = new Array();
						// $scope.authen = $scope.tagData[row];
						$scope.authen = JSON.parse(JSON
								.stringify($scope.tagViewData[row]));
						$scope.statusEdit = true; //kumar 21-Jan-21 Enable Edit button only when it is true
						if ($scope.authen.status == "Free") {

							$scope.names = [ "Free", "Lost", "Not Working",
									"Expired" ];
						} else if ($scope.authen.status == "Expired") {
							// $scope.showDetails = true;
							$scope.statusEdit = false;
							$scope.names = [ "Expired" ];
							// $scope.names = [ "Expired", "Lost", "Not
							// Working", "Blocked" ];
						} else if ($scope.authen.status == "Overdue") {

							$scope.showDetails = true;

							$scope.names = [ "Overdue", "Lost", "Not Working",
									"Blocked" ];
						} else if ($scope.authen.status == "Blocked") {

							$scope.showDetails = true;
							/*
							 * $scope.names=[]; $scope.names[0] =
							 * $scope.authen.status;
							 */
							$scope.names = [ "Blocked", "Lost", "Not Working",
									"Assigned" ];

						} else if ($scope.authen.status == "Lost"
								&& $scope.authen.restart == true) {
							// $scope.statusEdit = true;
							$scope.showDetails = true;
							$scope.names = [ "Lost", "Free" ];
						} else if ($scope.authen.status == "Lost") {
							$scope.statusEdit = false;
							$scope.names = [ "Lost" ];
						} else if ($scope.authen.status == "Not Working") {
							$scope.statusEdit = false;
							$scope.names = [ "Not Working" ];
						} else if ($scope.authen.status == "Assigned") {

							if ($scope.authen.type == 'Visitor'
									|| $scope.authen.type == 'Contractor'
									|| $scope.authen.type == 'Vendor') {
								if ($scope.authen.assignmentDate != undefined
										&& $scope.authen.assignmentDate != ''
										&& $scope.authen.assignmentDate != null) {
									var tagAssignmentDate = moment($scope.authen.assignmentDate);
									var currentDay = moment(new Date()).format(
											'MMM DD, YYYY');
									var diff = moment(currentDay)
											- tagAssignmentDate;
									if (diff == 0) { // today
										var today = new Date();
										var currentTime = ("0" + today
												.getHours()).slice(-2)
												+ ":"
												+ ("0" + today.getMinutes())
														.slice(-2);
										var toTime = $scope.authen.toTime
												.substring(
														0,
														$scope.authen.toTime.length - 3);
										var fromTime = $scope.authen.fromTime;
										a = toTime.split(':');
										b = currentTime.split(':');
										var toTimeMinutes = (+a[0]) * 60
												+ (+a[1]);
										var currentTimeMinutes = (+b[0]) * 60
												+ (+b[1]);

										if (currentTimeMinutes > toTimeMinutes)

											$scope.names = [ "Assigned",
													"Lost", "Not Working",
													"Blocked", "Expired",
													"Overdue" ];
										else
											$scope.names = [ "Assigned",
													"Lost", "Not Working",
													"Blocked", "Expired" ];
									} else if (diff > 0) { // assigned date is
															// previous date
										$scope.names = [ "Assigned", "Lost",
												"Not Working", "Blocked",
												"Expired", "Overdue" ];
									} else {
										$scope.names = [ "Assigned", "Lost",
												"Not Working", "Blocked",
												"Expired" ];
									}
								}
							} else {
								$scope.names = [ "Assigned", "Lost",
										"Not Working", "Expired" ];
							}

							if ($scope.authen.type == 'LSLM'
									|| $scope.authen.type == 'Inventory') {
								$scope.trackMacId = $scope.authen.macId;  
							} 

						}

					}
					
					

					$scope.updateTag = function() {
						if ($scope.names[0] == $scope.authen.status) {
							$rootScope.showAlertDialog("Please select other status");
							return;
						} 
						var updateTagParameter = {
							"tagId" : $scope.authen.tagId,
							"status" : $scope.authen.status,
							"type" : 'tagWarehouse'
						};
						if (!$rootScope.checkNetconnection()) { 
							$rootScope.alertDialogChecknet();
							return; 
							}
						$rootScope.showloading('#loadingBar');
						$scope.promise = $http
								.post('./updateTagStatus', updateTagParameter)
								.then(
										function(result) {
											$rootScope
													.hideloading('#loadingBar');
											if ($scope.authen.status == "Lost"){ //kumar 21-Jan-21
												$scope.authen.restart = false;
											}
											else if ($scope.authen.status == "Free"){ //kumar 21-Jan-21
												$scope.authen.restart = true;
												$scope.authen.type = "";
												$scope.authen.assignmentId ="";
												$scope.authen.assignmentDate="";
												$scope.authen.modifiedDate=moment(new Date()).format('MMM DD, YYYY');
											}
											$scope.tagViewData[$scope.selectedRow] = $scope.authen;
											if (result.data.status == "success") {
												$rootScope.showAlertDialog(result.data.message);  
												$scope.cancelTag();

											} else if (result.data.status == "error") {
												$rootScope.showAlertDialog(result.data.message);  
											}

										},
										function(error) {
											$rootScope.hideloading('#loadingBar');
											$rootScope.fnHttpError(error);
										});
					}
					$scope.cancelTag = function() {

						$scope.authen = {};
						$scope.selectedRow = -1;

						$scope.wareHouseTagEditScreen = true;
						$scope.wareHouseTagUpdateScreen = false;
						$scope.searchButton = true;
					}
					$scope.cancelEdit = function() {

						$scope.selectedRow = -1;
						$scope.searchButton = true;
						$scope.trackMacId = '';
					}

					
					
					// allowable zone list API
					$scope.getZoneDetails = function() {
						if (!$rootScope.checkNetconnection()) { 
							//$rootScope.alertDialogChecknet();
							return; 
							}
						$rootScope.showloading('#loadingBar');
					$http({
						method : 'GET',
						url : './zone',

					})
							.then(
									function(response) {
										if (response != null
												&& response.data != null
												&& response.data.length != 0
												&& response.data != "BAD_REQUEST"
												&& response.data.data != undefined
												&& response.data.data.length != 0) {

											//$scope.locationList = [];
											$scope.zoneDetails = [];

											/*for (var i = 0; i < response.data.data.length; i++) {
												if (response.data.data[i].active == true) {
													if(response.data.data[i].zoneType == "Trackingzone"){
													$scope.locationList.push (response.data.data[i].name);
												}
												}
											}*/
											for (var i = 0; i < response.data.data.length; i++) {
												if (response.data.data[i].active == true) {
													if(response.data.data[i].zoneType == "Trackingzone"){
													$scope.zoneDetails.push ({
														'zoneName' : response.data.data[i].name,
														'zoneId' : response.data.data[i].id
														});
												}
													
												}
											}	
											//menudata = JSON
											//		.parse(JSON
											//				.stringify($scope.locationList));
											if ($scope.zoneDetails.length > 0)
												$scope.skuZoneName = $scope.zoneDetails[0].zoneName;
											//$scope.fnCreateTreeWithZoneNme();
										} else {
											toastr.error('No zones are available',"EI4.0")	
										}
									},function error(response) {
										$rootScope.hideloading('#loadingBar');
										$rootScope.fnHttpError(response);
									});
					}
					 
					// fetch button function triggered when enter key pressed
					$("#checktagIdSelect").keyup(function(event) {
					    if (event.keyCode === 13) {
					        $("#id_fetchButtonWarehouseTag").click();
					    }
					});

					
					// now, add a new to the TABLE.
					$scope.fnaddmaterialTableRow = function(listItem) {

						var materialTab = document
								.getElementById('materialTable');

						var rowCnt = materialTab.rows.length; // table row																	
						var tr = materialTab.insertRow(rowCnt); 																	
						tr = materialTab.insertRow(rowCnt);

						for (var c = 0; c < $scope.materialTableHeaders.length; c++) {
							var td = document.createElement('td'); // table
							// definition.
							td = tr.insertCell(c);

							var node = document.createElement('div');
							node.style.marginLeft = '5px';
							node.style.marginRight = '5px';
							node.style.width = '200px';

							node.innerHTML = listItem[$scope.materialTableHeaders[c]];
							if(node.innerHTML == "undefined" || node.innerHTML == null || node.innerHTML == "null" )
								node.innerHTML = ""
							td.appendChild(node);
						}
					}


					// pagination button
					$scope.currentPage = 1;

					$scope.currentPage = 1;
					$scope.numPerPage = 10;
					$scope.maxSize = 3;
					$scope.totalItems = 0;
					$scope.tagDataFilterList = [];
					$scope.tagViewData = [];
					$scope
							.$watch(
									'currentPage + numPerPage',
									function() {
										var begin = (($scope.currentPage - 1) * $scope.numPerPage), end = begin
												+ $scope.numPerPage;
										if ($scope.searchText == '') // kumar
											// 02/July
											// for
											// filter
											// text
											$scope.tagViewData = $scope.tagData
													.slice(begin, end);
										else
											$scope.tagViewData = $scope.tagDataFilterList
													.slice(begin, end);
										$scope.enablePageButtons();
									});

					$scope.currentPage = 1;

					$scope.maxSize = 3;

					$scope.numPerPage = 10;
					$scope.totalItems = 0;
					$scope.totalPages = 0;

					$scope.hasNext = true;
					$scope.hasPrevious = true;
					$scope.isFirstPage = true;
					$scope.isLastPage = true;
					$scope.serial = 1;

					$scope
							.$watch(
									"currentPage",
									function(newVal, oldVal) {
										if (newVal !== oldVal) {

											if (newVal > $scope.totalPages) {
												$scope.currentPage = $scope.totalPages;
											} else {
												$scope.currentPage = newVal;
											}

											$scope.serial = (($scope.currentPage - 1) * $scope.numPerPage) + 1;
											$scope.enablePageButtons();
										}
									});

					$scope.enablePageButtons = function() {
						$scope.serial = (($scope.currentPage - 1) * $scope.numPerPage) + 1;
						$scope.selectedRow = -1;
						if ($scope.totalPages <= 1) {
							$scope.hasPrevious = true;
							$scope.isFirstPage = true;
							$scope.hasNext = true;
							$scope.isLastPage = true;

						} else if ($scope.totalPages > 1
								&& $scope.currentPage == $scope.totalPages) {

							$scope.hasPrevious = false;
							$scope.isFirstPage = false;
							$scope.hasNext = true;
							$scope.isLastPage = true;
						} else if ($scope.currentPage != 1
								&& $scope.currentPage < $scope.totalPages) {

							$scope.hasPrevious = false;
							$scope.isFirstPage = false;
							$scope.hasNext = false;
							$scope.isLastPage = false;

						} else {
							$scope.hasPrevious = true;
							$scope.isFirstPage = true;
							$scope.hasNext = false;
							$scope.isLastPage = false;
						}
						 
					};

					$scope.getTotalPages = function() {

						if (($scope.totalItems % $scope.numPerPage) > 0)
							$scope.totalPages = parseInt($scope.totalItems
									/ $scope.numPerPage) + 1;
						else
							$scope.totalPages = parseInt($scope.totalItems
									/ $scope.numPerPage);

						$scope.enablePageButtons();

					};

					$scope.getAssignedTagIds = function(assetList) {

						$scope.assignedTagMacId = '';

						if (assetList != undefined && assetList != null) {
							for (var i = 0; i < assetList.length; i++) {
								 
								if (assetList[i].status == 'Assigned' &&
										(assetList[i].type == 'LSLM' || assetList[i].type == 'Inventory' )) {
									if (assetList[i].macId != undefined && assetList[i].macId != null
											&& assetList[i].macId != '') {
										if ($scope.assignedTagMacId == '')
											$scope.assignedTagMacId = assetList[i].macId;
										else
											$scope.assignedTagMacId = $scope.assignedTagMacId
													+ ","
													+ assetList[i].macId;

									}
								}
							}
						}

					}
					
					$scope.goFirstPage = function() {
						$scope.currentPage = 1;
						$scope.enablePageButtons();
					};

					$scope.goPreviousPage = function() {
						if ($scope.currentPage > 1)
							$scope.currentPage = $scope.currentPage - 1;
						$scope.enablePageButtons();
					};

					$scope.goNextPage = function() {
						if ($scope.currentPage <= $scope.totalPages)
							$scope.currentPage = $scope.currentPage + 1;
						$scope.enablePageButtons();
					};

					$scope.goLastPage = function() {
						$scope.currentPage = $scope.totalPages;
						$scope.enablePageButtons();
					};

					
					$scope.displayTagWHEditScreen = function() {
						
						$scope.tagDataFilterList = [];
						$scope.tagViewData = [];
						$scope.searchText = "";

						$scope.selectedRow = -1;
						$scope.currentPage = 1;
						$scope.searchButton = true;
						//$scope.tagPeopleViewScreen = false;
						$scope.wareHouseTagEditScreen = true;
						$scope.wareHouseTagUpdateScreen = false;
						
						//$scope.fetchScreen = false;
						$scope.assignScreen = false;
						//$scope.tagSKUReturnScreen = false;
						//$("#idTagPeopleView").removeClass("active");
						//$("#idTagPeopleAssign").removeClass("active");
						$("#idwareHouseTagEdit").addClass("active");
						//$("#idTagPeopleReturn").removeClass("active");
						$scope.getTagData();

					};
					

					$scope.tagAssignBackToView = function() {
						var formID = document.getElementById("assignTagForm")
								.reset();

						$scope.authen = {};
						$scope.selectedRow = -1;
						//$scope.tagPeopleViewScreen = true;
						$scope.searchButton = true;
						//$scope.fetchScreen = false;
						$scope.assignScreen = false;
						//$scope.tagSKUReturnScreen = false;

					}
					$scope.dateTimeReset = function() {

					}
					$scope.fnGetDictionaryLength = function(dict) {
						var nCount = 0;
						for (i in dict)
							++nCount;
						return nCount;
					}
					
					$scope.fnSelectZoneFromUI = function() {
						if ($scope.fnGetDictionaryLength($rootScope.ZoneSelection) == 0)
						{
						$rootScope.ZoneSelection.zoneSelect = "true";
						$rootScope.ZoneSelection.tagId = $scope.authen.tagId;
						$rootScope.ZoneSelection.skuNo = $scope.skuNo  ;
						//$rootScope.ZoneSelection.locationList = $scope.locationList;
						$rootScope.ZoneSelection.zoneDetails = $scope.zoneDetails;
						$window.sessionStorage.setItem("menuName","zoneManagement");
						$location.path('/zoneManagement');
						//locationList
						}
					}
					

					//to display enabled sub menus
					if ($scope.user.capabilityDetails != undefined
							&& $scope.user.capabilityDetails != null) {
						
						/*if ($scope.user.capabilityDetails.HTAR != 0) {
							$scope.displayTagPeopleAssignScreen();
						}*/
						if ($scope.user.capabilityDetails.HTED != 0) {
							$scope.displayTagWHEditScreen();
						}
						
					}

				});
