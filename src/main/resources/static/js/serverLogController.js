app
		.controller(
				'serverLogController',
				function($scope, $http, $rootScope, $window, $timeout,
						$location) {
					$scope.user = JSON.parse($window.sessionStorage
							.getItem("user"));
					if ($scope.user == null) {
						$location.path('/login');
						return;
					}
					if (!$rootScope.checkNetconnection()) { 				
					 	$rootScope.alertDialogChecknet();
						return; 
						}
					$scope.logLeastDate = "";
					$scope.loadData = true;
					$scope.searchButton = true;
					// $scope.org = $window.sessionStorage.getItem("org");
					/*$scope.fromTime = 0;
					$scope.toTime = 0;*/
					$scope.logType = "Server Log";
					$scope.searchText = "";
					/*
					 * $scope.$on('updateorg', function(event, data) {
					 * $scope.org = data; $scope.logsList.length = 0;
					 * $scope.logList.length = 0; $scope.logFilterList.length =
					 * 0; $scope.fromTime = 0; $scope.toTime = 0;
					 * $scope.apiCallTime = 0; $scope.loadData = true;
					 * $scope.quuppalogbtnvisibility(); $scope.getData(); });
					 */

					$scope.headingTitle = "Logger";

					// For selected button color change

					$scope.searchText = '';
					$scope.logList = new Array();
					$scope.logsList = new Array();
					$scope.logValuesList = new Array();
					$scope.logFilterList = new Array();

					$scope.scrollsys = function() {
						
						if ($scope.logsList.length == 0) //dont scroll if data is not available
							return;
						
						var table = document.querySelector('#logtablesys');
						var line = 0;
						var rows = table.querySelectorAll('#logtablerowsys');
						rows[line].scrollIntoView({
							behavior : 'smooth',
							block : 'center'
						});
					};

					/*$scope.getUTCTime = function() {
						var date = new Date();
						return Date.UTC(date.getUTCFullYear(), date
								.getUTCMonth(), date.getUTCDate(), date
								.getUTCHours(), date.getUTCMinutes(), date
								.getUTCSeconds());
					}*/

					/*$scope.getUTCFromTime = function(toTime, mins) {
						var d = new Date(toTime);

						if (mins == 0)
							return toTime;
						else
							return toTime - (mins * 60000);
					}*/

					$scope.getData = function() {

						if ($scope.toTime == 0) {
							$scope.toTime = $scope.getUTCTime();
						} else {
							$scope.toTime = $scope.fromTime;
						}

						if ($scope.apiCallTime < 2)
							$scope.fromTime = $scope.getUTCFromTime(
									$scope.toTime, 60);
						else
							$scope.fromTime = $scope.getUTCFromTime(

							);

						// $rootScope.showloading('#loadingBar');

						 $scope.getServerlogs();
						$scope.isrefreshLogViewClicked = false;

					};
					$scope.localizeDateStr = function(date_to_convert_str) {

						/*if (date_to_convert_str == undefined
								|| date_to_convert_str == null
								|| date_to_convert_str == '')
							return '';

						var date1 = new Date(date_to_convert_str);
						var utcDate = date1.toUTCString();
						var date = new Date(utcDate); // $scope.getDate(utcDate);

						return moment(date).format("hh:mm:ss A MMM DD, YY");*/
						return date_to_convert_str.slice(0,20);
					}

					$scope.getDate = function(date_to_convert_str) {

						var date = date_to_convert_str.split(" ");
						var _date = date[0];
						var _time = date[1].split(".");

						return new Date(_date + 'T' + _time[0] + 'Z');
					}

					$scope.getServerlogs = function() {

						if (!$rootScope.checkNetconnection()) { 
							$rootScope.hideloading('#loadingBar');	// to hide when scroll is used
						 	$rootScope.alertDialogChecknet();
							return; 
							}
						var apiLogType = "";
						if($scope.logType == "Server Log")
							apiLogType = "server";
						else if($scope.logType == "Employee Profile")
							apiLogType = "staff";
						else if($scope.logType == "Asset Profile")
							apiLogType = "asset";
						else if($scope.logType == "Visitor Profile")
							apiLogType = "visitor";
						else if($scope.logType == "Vehicle Profile")
							apiLogType = "vehicle";
						else if($scope.logType == "Material Profile")
							apiLogType = "material";
						else if($scope.logType == "Genie")
							apiLogType = "genie";								
						$rootScope
						.showloading('#loadingBar');

						$scope.promise = $http({
							method : 'POST',
							url : './getLogs',
							data : {
								"logDate" : $scope.logLeastDate,
								"noOfRecords" : "50",
								"logType": apiLogType
								}
						})
								.then(
										
										function success(response) {
											$rootScope.hideloading('#loadingBar');		
											if (response != null
													&& response.data != null
													&& response.data != "BAD_REQUEST") {
												$scope.logValuesList = response.data;
												var listSize = $scope.logValuesList.length;												
												
												if(response.data != undefined && response.data.length != 0 ){													
													let minlogDate  =  moment(response.data[0].date.slice(0,20));													
													for (var i = 0; i < response.data.length; i++) {													
															if (moment(response.data[i].date.slice(0,20)) < minlogDate) {
																$scope.logLeastDate = (response.data[i].date).toString();
														}														
													}														
												}
												
												if (listSize > 0) {
													$scope.loadData = true;
													//$scope.apiCallTime = 0;
													$scope.logList = $scope.logList
															.concat($scope.logValuesList
																	.sort(sortAlphaNum));
													$scope.filterOption();

													$scope.logValuesList.length = 0;
												} else {

													//$scope.apiCallTime = $scope.apiCallTime + 1;

													if (listSize == 0) {
														$
																.alert({
																	title : $rootScope.MSGBOX_TITLE,
																	content : 'ERROR : No Data Available',
																	icon : 'fa fa-times',
																});
													}
													$scope.loadData = false;

												}
												

											} else {
												$scope.noList = true;
											}
										},
										function error(response) {
											$rootScope.hideloading('#loadingBar');		
											$rootScope.fnHttpError(response);
										});
					};

					$scope.viewServerLog = function() {
						$scope.logsList.length = 0;
						$scope.logList.length = 0;
						$scope.logFilterList.length = 0;
						$scope.loadData = true;				

						$scope.getServerlogs();

					};
					$scope.viewServerLog();
					$scope.selectLogsChanged = function() {
						$scope.logLeastDate = "";
						$scope.viewServerLog();
					}
					$scope.refresh = function() {
						$scope.logsList.length = 0;
						$scope.logList.length = 0;
						$scope.logFilterList.length = 0;
						$scope.isrefreshLogViewClicked = true;
						$scope.loadData = true;
						$scope.searchText ="";
						$scope.logLeastDate ="";
						$scope.getServerlogs();

					};

					$scope.loadMore = function getData(tableState) {
						if ($scope.loadData) {
							$rootScope.showloading('#loadingBar');
							$timeout(function() {

								// if we reset (like after a search or an order)
								if (tableState.pagination.start != 0) {
									// we load more
									$rootScope.hideloading('#loadingBar');
									$scope.getServerlogs();

								}
							}, 1000);
						}
					};
					
					$scope.searchOption = function() {
						$scope.filterOption();
					}
					
					$scope.filterOption = function() {

						$scope.logFilterList =[];
						$scope.logsList =[];
						
						for (var i = 0; i < $scope.logList.length; i++) {
						
								if ($scope.searchText == '') {
									$scope.logFilterList
											.push($scope.logList[i]);
									
								} else {
									
									if (($scope.logList[i].date != undefined && $scope.logList[i].date != null ?
										$scope.logList[i].date: "").toLowerCase().includes($scope.searchText.toLowerCase())
									|| ($scope.logList[i].major != undefined && $scope.logList[i].major != null ?
										$scope.logList[i].major: "").toLowerCase().includes($scope.searchText.toLowerCase())
									|| ($scope.logList[i].minor != undefined && $scope.logList[i].minor != null ?
										$scope.logList[i].minor: "").toLowerCase().includes($scope.searchText.toLowerCase())
									|| ($scope.logList[i].type != undefined && $scope.logList[i].type != null ?
										$scope.logList[i].type: "").toLowerCase().includes($scope.searchText.toLowerCase())
									|| ($scope.logList[i].params != undefined	&& $scope.logList[i].params != null ?
										$scope.logList[i].params:"").toLowerCase().includes($scope.searchText.toLowerCase())
									|| ($scope.logList[i].result != undefined && $scope.logList[i].result != null ? 
										$scope.logList[i].result: "").toLowerCase().includes($scope.searchText.toLowerCase())) 
									{
										$scope.logFilterList.push($scope.logList[i]);

									}
								}
							}
						
						$scope.logsList = $scope.logsList
								.concat($scope.logFilterList);
						if ($scope.logList.length == 0	|| $scope.logsList.length == 0) {
							$scope.serverLogNoData = true;

						} else {
							$scope.serverLogNoData = false;
							
						}
					}

					function sortAlphaNum(a1, b1) {
						var a = $scope.getDate(a1.date).getTime();
						var b = $scope.getDate(b1.date).getTime();

						if (a === b) {

							return a === a ? 0 : a < b ? 1 : -1;
						} else {
							return a < b ? -1 : 1;
						}
					}
					/*
					 * if( $scope.user.capabilityDetails != undefined &&
					 * $scope.user.capabilityDetails != null){ if
					 * ($scope.user.capabilityDetails.LOG.LEL != 0) {
					 * $scope.viewUserLog(); } if
					 * ($scope.user.capabilityDetails.LOG.LEL == 0) {
					 * $scope.viewSystemLog(); } }
					 */
				});