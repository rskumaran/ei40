app
		.controller(
				'assetChronoController',
				function($scope, $http, $window, $rootScope, $location,
						$timeout) {
					$scope.user = JSON.parse($window.sessionStorage
							.getItem("user"));
					if ($scope.user == null) {
						$location.path('/login');
						return;
					}

					$scope.assetChronoViewAllScreen = true;
					$scope.searchButton = true;
					 
					$scope.assignedTagMacId = '';
					$scope.trackMacId = '';
					$scope.showLocateBtn = false;
					
					$scope.assetChronoData = new Array();
					$scope.assetChronoViewData = new Array();
					$scope.assetChronoDataFilterList = new Array();
					$scope.fetchAssetIdAPIData = new Array();
					$scope.assetIdTable=[];
					$scope.searchText = "";
					
					$scope.firstSearchANDDataArray = new Array();
					$scope.secondSearchORDataArray = new Array();
					$scope.firstSearchText = "";
					$scope.secondSearchText = "";
					$scope.validityDateBeforeEdit = "";
					 $scope.chronoAssetType =["Tools", "Gages", "Fixtures", "Supplies"];
					
					if ($scope.authen == undefined) {
						$scope.authen = {};
					}
					$('.select2').select2(); // searchable dropdown in fetch
												// screen intialization
					$scope.authen.type = $scope.chronoAssetType[0]; // tools/gages/fixtures
					var today = new Date();

					$('#datetimepicker4').datetimepicker({
						format : 'YYYY-MMM-DD',
						minDate : new Date()
					});
					$('#dateTimeEditPicker').datetimepicker({
						format : 'YYYY-MMM-DD'
					});

					/*
					 * $('#datetimepicker5').datetimepicker({ format :
					 * 'YYYY-MMM-DD', Default : true, minDate : new Date() });
					 */
					
					$('#stimepicker').datetimepicker({ 
						format : 'YYYY-MMM-DD HH:mm',
						 minDate : new Date(),
						 maxDate : new Date(),
					icons:
                    { time: 'fas fa-clock'}
					});

					/*
					 * $('#etimepicker').datetimepicker({ format : 'YYYY-MMM-DD
					 * HH:mm', minDate : new Date() });
					 */
					$('#etimepicker').datetimepicker({
						format : 'YYYY-MMM-DD HH:mm',
						minDate : new Date(),
						icons:
		                    { time: 'fas fa-clock'}
					});
/*
 * var startTime = ("0" + today.getHours()).slice(-2) + ":" + ("0" +
 * today.getMinutes()).slice(-2); var endTime = ("0" + (today.getHours() + 2))
 * .slice(-2) + ":" + ("0" + today.getMinutes()).slice(-2);
 */
					var startTime = moment(new Date()).format('YYYY-MMM-DD HH:mm');
					var endTime = moment(new Date()).format('YYYY-MMM-DD HH:mm');
					endTime =  moment(new Date()).format('YYYY-MMM-DD ')+("0" + (today.getHours() + 2))
					.slice(-2)
					+ ":"
					+ ("0" + today.getMinutes()).slice(-2);
			$("#stime").val(startTime);
			$("#etime").val(endTime);
			$scope.authen.stime = startTime;
			$scope.authen.etime = endTime;
			$scope.authen.validityDate = moment(new Date()).format('YYYY-MMM-DD');
			$("#datetimepicker4").val(moment(new Date()).format('YYYY-MMM-DD'));
			$("#dateTimeEditPicker").val(moment(new Date()).format('YYYY-MMM-DD'));
					// Date range as a button
					// $scope.dateRange= moment(new Date()).format("MMM DD,
					// YYYY") + ' - ' + moment(new Date()).format("MMM DD,
					// YYYY") ;
					   $('#idDaterange-btn').daterangepicker(
					     {
					       ranges   : {
					         'Today'       : [moment(), moment()],
					        /*
							 * 'Tomorrow' : [moment().add(1, 'days'),
							 * moment().add(1, 'days')],
							 */
					         'Next 7 Days' : [moment().add(6, 'days'), moment()],
					         'Next 14 Days': [moment().add(13, 'days'), moment()],
					         'Next Month': [moment().add(1, 'month').startOf('month'), moment().add(1, 'month').endOf('month')],
					       },
// startDate: moment().subtract(1, 'days'),
					       startDate: moment(),
					       endDate  : moment()
					     }
					   )
					$('#idDaterange-btn').on('cancel.daterangepicker', function(ev, picker) {
					// do something, like clearing an input
					// $('#idDaterange-btn').val('');
					// $scope.dateRange = "";
					// $scope.$apply();
					});
					$('#idDaterange-btn').on('apply.daterangepicker', function(ev, picker) {
					// do something, like clearing an input
					$('#idDaterange-btn').val(picker.startDate.format('MMM DD, YYYY') + ' - ' + picker.endDate.format('MMM DD, YYYY'));
					// alert(picker.endDate.format('MMMM D, YYYY') + ' - ' +
					// picker.startDate.format('MMMM D, YYYY'));

					if(picker.chosenLabel == "Custom Range" || picker.chosenLabel == "Next Month" ) {
						$scope.startDateExpiry =  picker.startDate.format('MMM DD, YYYY');
						$scope.endDateExpiry = picker.endDate.format('MMM DD, YYYY');
						//Filter based on date
						$scope.disableCompareDate = true;
						if(picker.chosenLabel == "Custom Range" && ($scope.startDateExpiry == $scope.endDateExpiry)){
							$scope.disableCompareDate = false;
							// toastr.info("Dates are same","EI40")
						}
						if (picker.chosenLabel == "Next Month"){
							$scope.dateRange = picker.chosenLabel;
						} else {
							$scope.dateRange = picker.startDate.format('MMM DD, YYYY') + ' - ' + picker.endDate.format('MMM DD, YYYY');
						}

					}else{
						$scope.startDateExpiry =   picker.endDate.format('MMM DD, YYYY');
						$scope.endDateExpiry = picker.startDate.format('MMM DD, YYYY');
						// $scope.dateRange = picker.endDate.format('MMM D,
						// YYYY') +
						// ' - ' + picker.startDate.format('MMM D, YYYY');
						//Filter based on date
						$scope.disableCompareDate = true;
						if(picker.chosenLabel == "Today" && ($scope.startDateExpiry == $scope.endDateExpiry)){
							$scope.disableCompareDate = false;
							$scope.varCompareDate = "Equal";
						}
						$scope.dateRange = picker.chosenLabel;

					}

					$scope.getAssetChronoExpiryData();
					$scope.$apply();
					});
					   
				// to enable/disable sub menus
					$scope.user = JSON.parse($window.sessionStorage
							.getItem("user"));

					if ($scope.user.capabilityDetails != undefined
							&& $scope.user.capabilityDetails != null) {
						if ($scope.user.capabilityDetails.ACVA == 1) {
							$scope.showViewAllBtn = true;
						} else {
							$scope.showViewAllBtn = false;
						}
						if ($scope.user.capabilityDetails.ACAD == 1) {
							$scope.showAddBtn = true;
						} else {
							$scope.showAddBtn = false;
						}
						if ($scope.user.capabilityDetails.ACED == 1) {
							$scope.showEditBtn = true;
						} else {
							$scope.showEditBtn = false;
						}
						if ($scope.user.capabilityDetails.ACAR == 1) {
							$scope.showAssignReturnBtn = true;
						} else {
							$scope.showAssignReturnBtn = false;
						}

						 if ($scope.user.capabilityDetails.ASLV == 1) {
	                            $scope.showLocateBtn = true;
	                        }  

					}
					$scope.sortBasedOnValidityDateFunction = function(a,b) {  
					    var dateA = new Date(a.validityDate).getTime();
					    var dateB = new Date(b.validityDate).getTime();
					    return dateA > dateB ? 1 : -1;  
					} 
					

					// input field validation
					$("#assetId, #id_assetTypeAddAsset, #id_subTypeAddAsset, #id_rangeAddAsset,  #id_assetIdEditAsset, #id_assetTypeEditAsset, #id_subTypeEditAsset, #id_rangeEditAsset,#id_rangeEditAsset, #idInputfetchAssetId").keypress(
							function(e) {
								// if the letter is not alphabets then display
								// error
								// and don't type anything
								$return = ((e.which > 64) && (e.which < 91)) || ((e.which> 96) && (e.which < 123)) ||(e.which == 8) || (e.which == 32)  || (e.which >= 48 && e.which <= 57)
								if (!$return)
													
								{
									
									return false;
								}
							});	
					$("#id_manufacturerAddAsset, #id_manufacturerEditAsset").keypress(
							function(e) {
								// if the letter is not alphabets then display
								// error
								// and don't type anything
								$return = ((e.which > 64) && (e.which < 91)) 
								|| ((e.which > 43) && (e.which < 47))
								|| ((e.which> 96) && (e.which < 123)) 
								||(e.which == 8) || (e.which == 32)  
								|| (e.which == 95)|| (e.which == 38)
								|| (e.which >= 48 && e.which <= 57)
								if (!$return)
													
								{
									return false;
								}
							});	
					
					$scope.searchOption = function() {
						$scope.filterOption();
					}

					$scope.trackTag = function() {
						if($scope.assignedTagMacId!=undefined && $scope.assignedTagMacId!=null 
								&& $scope.assignedTagMacId!='')
						{
							
							$rootScope.trackingMacId = $scope.assignedTagMacId;
							$rootScope.showFullView();
							var currentMenuName = $window.sessionStorage.getItem("menuName");
							if ($scope.user.role == "DemoUser")
							{
								$window.sessionStorage.setItem("menuName", "shopFloorAnalysis");
								$window.sessionStorage.setItem("demoMenuName", "assetLiveView");
								$location.path('/shopFloorAnalysis');
							}else{
								$window.sessionStorage.setItem("menuName", "assetLiveView");
								$location.path('/assetLiveView'); 
							}
							$rootScope.$broadcast('sourcePreviousScreen',currentMenuName);
						}else{
							toastr.error('No assigned tags to track.', "EI4.0");  
						}
						 
					}
					
					$scope.trackSingleTag = function() {
						if ($scope.trackMacId != undefined
								&& $scope.trackMacId != null
								&& $scope.trackMacId != '') {
							
							$rootScope.trackingMacId = $scope.trackMacId;
							$rootScope.showFullView();
							var currentMenuName = $window.sessionStorage.getItem("menuName");
							if ($scope.user.role == "DemoUser")
							{
								$window.sessionStorage.setItem("menuName", "shopFloorAnalysis");
								$window.sessionStorage.setItem("demoMenuName", "assetLiveView");
								$location.path('/shopFloorAnalysis');
							}else{
								$window.sessionStorage.setItem("menuName",
										"assetLiveView");
								$location.path('/assetLiveView');
							}
							$rootScope.$broadcast('sourcePreviousScreen',currentMenuName);
						} else {
							toastr.error('No assigned tags to track.', "EI4.0");
						}
					}
					
					
					$scope.filterOption = function() {
						$scope.assetChronoViewData = [];
						$scope.assetChronoDataFilterList = [];
						 
						for (var i = 0; i < $scope.assetChronoData.length; i++) {
							if ($scope.searchText == '') {
								 
								$scope.assetChronoDataFilterList
										.push($scope.assetChronoData[i]);
							} else {
								if (($scope.assetChronoData[i].assetId != undefined
										&& $scope.assetChronoData[i].assetId != null ? $scope.assetChronoData[i].assetId
										: "").toLowerCase().includes(
										$scope.searchText.toLowerCase())
										|| ($scope.assetChronoData[i].assetType != undefined
												&& $scope.assetChronoData[i].assetType != null ? $scope.assetChronoData[i].assetType
												: "").toLowerCase()
												.includes(
														$scope.searchText
																.toLowerCase())
										|| ($scope.assetChronoData[i].type != undefined
												&& $scope.assetChronoData[i].type != null ? $scope.assetChronoData[i].type
												: "").toLowerCase()
												.includes(
														$scope.searchText
																.toLowerCase())						
										|| ($scope.assetChronoData[i].subType != undefined
												&& $scope.assetChronoData[i].subType != null ? $scope.assetChronoData[i].subType
												: "").toLowerCase()
												.includes(
														$scope.searchText
																.toLowerCase())
										|| ($scope.assetChronoData[i].rangeValue != undefined
												&& $scope.assetChronoData[i].rangeValue != null ? $scope.assetChronoData[i].rangeValue
												: "").toLowerCase()
												.includes(
														$scope.searchText
																.toLowerCase())
										|| ($scope.assetChronoData[i].manufacturer != undefined
												&& $scope.assetChronoData[i].manufacturer != null ? $scope.assetChronoData[i].manufacturer
												: "").toLowerCase()
												.includes(
														$scope.searchText
																.toLowerCase())
										|| ($scope.assetChronoData[i].validityDate != undefined
												&& $scope.assetChronoData[i].validityDate != null ? $scope.assetChronoData[i].validityDate
												: "").toLowerCase()
												.includes(
														$scope.searchText
																.toLowerCase())
										|| ($scope.assetChronoData[i].assignmentStatus != undefined
												&& $scope.assetChronoData[i].assignmentStatus != null ? $scope.assetChronoData[i].assignmentStatus
												: "").toLowerCase()
												.includes(
														$scope.searchText
																.toLowerCase())
										|| ($scope.assetChronoData[i].assignedPerson != undefined
												&& $scope.assetChronoData[i].assignedPerson != null ? $scope.assetChronoData[i].assignedPerson
												: "").toLowerCase()
												.includes(
														$scope.searchText
																.toLowerCase())
										|| ($scope.assetChronoData[i].assignmentDept != undefined
												&& $scope.assetChronoData[i].assignmentDept != null ? $scope.assetChronoData[i].assignmentDept
												: "").toLowerCase()
												.includes(
														$scope.searchText
																.toLowerCase())
										|| ($scope.assetChronoData[i].department != undefined
												&& $scope.assetChronoData[i].department != null ? $scope.assetChronoData[i].department.deptName
												: "").toLowerCase()
												.includes(
														$scope.searchText
																.toLowerCase())
										|| ($scope.assetChronoData[i].fromTime != undefined
												&& $scope.assetChronoData[i].fromTime != null ? $scope.assetChronoData[i].fromTime
												: "").toLowerCase()
												.includes(
														$scope.searchText
																.toLowerCase())
										|| ($scope.assetChronoData[i].toTime != undefined
												&& $scope.assetChronoData[i].toTime != null ? $scope.assetChronoData[i].toTime
												: "").toLowerCase()
												.includes(
														$scope.searchText
																.toLowerCase()))

								{
									$scope.assetChronoDataFilterList
											.push($scope.assetChronoData[i]); 
									 
								}

							}
							
						}

						$scope.totalItems = $scope.assetChronoDataFilterList.length;
						$scope.currentPage = 1;
						$scope.getTotalPages();
						$scope.getAssignedTagIds($scope.assetChronoDataFilterList);
						
						if ($scope.assetChronoData.length == 0
								|| $scope.assetChronoDataFilterList.length == 0) {
							$scope.assetChronoNoData = true;

						} else {
							$scope.assetChronoNoData = false;

							var begin = (($scope.currentPage - 1) * $scope.numPerPage), end = begin
									+ $scope.numPerPage;

							$scope.assetChronoViewData = $scope.assetChronoDataFilterList
									.slice(begin, end);
							
							// ---

							
							$scope.assetChronoNoData = false;

					if ($scope.assetChronoDataFilterList.length > $scope.numPerPage){
						$scope.assetChronoViewData = $scope.assetChronoDataFilterList
								.slice(0, $scope.numPerPage);
					$scope.PaginationTab=true; // pagination tab
					}
					else {
						$scope.assetChronoViewData = $scope.assetChronoDataFilterList;
						$scope.PaginationTab=false; // pagination tab
					}

						
							
						
							
						}
					}// kumar 02/Jul filter option <-
					// Assigned person dropdown (dept API call)
					$scope.getStaffDetails = function() {
						if (!$rootScope.checkNetconnection()) { 
							//$rootScope.alertDialogChecknet();
							return;
									 }
					$http({
						method : 'GET',
						url : './staff',

					})
							.then(
									function(response) {
										if (response != null
												&& response.data != null
												&& response.data.length != 0
												&& response.data != "BAD_REQUEST"
												&& response.data.data != undefined
												&& response.data.data.length != 0) {

											$scope.empViewData = [];

											for (var i = 0; i < response.data.data.length; i++) {
												if (response.data.data[i].active == true) {
													$scope.empViewData
															.push(response.data.data[i].empId);
												}
											}

										} else {
											$scope.noList = true;
										}
									},function(error) {
										$rootScope.hideloading('#loadingBar');
										$rootScope.fnHttpError(error);
									});
					}
					$scope.getDepartmentDetails = function() {
						if (!$rootScope.checkNetconnection()) { 
							//$rootScope.alertDialogChecknet();
							return;
									 }
					// data for department dropdown (dept API call)
					$http({
						method : 'GET',
						url : './department',

					})
							.then(
									function(response) {
										if (response != null
												&& response.data != null
												&& response.data.length != 0
												&& response.data != "BAD_REQUEST"
												&& response.data != undefined
												&& response.data.length != 0) {

											$scope.departmentList = [];

											for (var i = 0; i < response.data.length; i++) {
												if (response.data[i].active == true) {
													$scope.departmentList
															.push(response.data[i].deptName);
												}
											}

										} else {
											$scope.noList = true;
										}
									},function(error) {
										$rootScope.fnHttpError(error);
									});
					}
					var reA = /[^a-zA-Z]/g;
					var reN = /[^0-9]/g;

					function sortAlphaNum(a1, b1) {
						var a = a1.assetId;
						var b = b1.assetId;
						var aA = a.replace(reA, "");
						var bA = b.replace(reA, "");
						if (aA === bA) {
							var aN = parseInt(a.replace(reN, ""), 10);
							var bN = parseInt(b.replace(reN, ""), 10);
							return aN === bN ? 0 : aN > bN ? 1 : -1;
						} else {
							return aA > bA ? 1 : -1;
						}
					}
					// asset chrono view all API call and display data in table
					$scope.getAssetChronoData = function() {
						 
						
						if (!$rootScope.checkNetconnection()) { 
							$rootScope.alertDialogChecknet();
							return;
									 }
						$rootScope.showloading('#loadingBar');
						$http({
							method : 'GET',
							url : './assetChrono',

						})
								.then(
										function success(response) {
											$rootScope.hideloading('#loadingBar');
											if (response != null
													&& response.data != null
													&& response.data.length != 0
													&& response.data != "BAD_REQUEST"
													&& response.data.data != undefined
													&& response.data.data.length != 0) {

												$scope.assetChronoData.length = 0;
												response.data.data
												.sort(sortAlphaNum);
												for (var i = 0; i < response.data.data.length; i++) {
													var toTime ="",  assignedPerson ="",  deptName = "", fromTime ="", assignedDate ="",validityDate ="";
													if (response.data.data[i].active == true) {
														// $scope.assetChronoData.push(response.data.data[i]);
														

														// $scope.assetNonChronoData.push(response.data.data[i]);
												
														if (response.data.data[i].assignmentStatus ==  "ASSIGNED"){
															// .
															var assetAssignmentDetails =response.data.data[i].assetAssignmentDetails;
															var currentAssetAssignment;
															for (var key in assetAssignmentDetails){ 
																currentAssetAssignment = assetAssignmentDetails[key]
																break;
															}
																if(currentAssetAssignment.fromTime != undefined && currentAssetAssignment.fromTime != null && currentAssetAssignment.fromTime != "" ){
																// response.data.data[i].fromTime
																// =
																// response.data[i].createdDate.substring(0,
																// response.data[i].createdDate.length-9);
																// fromTime =
																// currentAssetAssignment.fromTime;
																fromTime =	moment(currentAssetAssignment.fromTime,"YYYY-MMM-DD HH:mm").format("MMM DD, YYYY HH:mm");
																 
															}
																if(currentAssetAssignment.date != undefined && currentAssetAssignment.date != null && currentAssetAssignment.date != ""){
																	// response.data.data[i].fromTime
																	// =
																	// response.data[i].createdDate.substring(0,
																	// response.data[i].createdDate.length-9);
																	assignedDate = moment(currentAssetAssignment.date,"YYYY-MMM-DD").format("MMM DD, YYYY");
																}	
															if(currentAssetAssignment.toTime != undefined && currentAssetAssignment.toTime != null &&currentAssetAssignment.toTime != "" ){
																toTime =	moment(currentAssetAssignment.toTime,"YYYY-MMM-DD HH:mm").format("MMM DD, YYYY HH:mm");
																
																// 
													
															
															}
															if(currentAssetAssignment.assignmentType == "PERSON" ){
																 assignedPerson = currentAssetAssignment.empId
																}
															if(currentAssetAssignment.assignmentType == "DEPT"){
																 deptName = currentAssetAssignment.deptName
																}
															}
													if(response.data.data[i].validityDate != undefined && response.data.data[i].validityDate != null && 
															response.data.data[i].validityDate != "" 
															){
														// response.data.data[i].calibrationRecord.calibrationExpiryDate
														// =
														// response.data.data[i].calibrationRecord.calibrationExpiryDate.substring(0,(response.data.data[i].calibrationRecord.calibrationExpiryDate.length-9));
														validityDate = moment(response.data.data[i].validityDate,"YYYY-MMM-DD").format("MMM DD, YYYY");
														
														}
													
												
													$scope.assetChronoData.push( {
														'assetId': response.data.data[i].assetId ,
														'assetType':  response.data.data[i].assetType,
														'subType':  response.data.data[i].subType,
														'type':  response.data.data[i].type,
														'manufacturer': response.data.data[i].manufacturer,
														'rangeValue':  response.data.data[i].rangeValue,
														'validityDate':  validityDate,
														'validityDateExpiry':  response.data.data[i].validityDate,
														'assignedTagMacId':  response.data.data[i].assignedTagMacId,
														'assignedStatus':  response.data.data[i].assignedStatus,
														'assignedPerson':   assignedPerson,
														 'deptName' :deptName,
														'fromTime': fromTime,
														'toTime' : toTime,
														'date': assignedDate
														
														
														});
														
													}
												}
												$scope.assetIdTable.length	= 0;
												for(var k=0; k< $scope.assetChronoData.length; k++){
													if($scope.assetChronoData[k].assetId != undefined && $scope.assetChronoData[k].assetId != '' && $scope.assetChronoData[k].assetId !=null){
														$scope.assetIdTable.push($scope.assetChronoData[k].assetId);
													}
												 
														
												}
												
												$scope.getAssignedTagIds($scope.assetChronoData);
												
												if ($scope.assetIdTable.length == 0)
													$scope.assetIdTable.push("No asset found ");

												if ($scope.assetChronoData.length == 0) {
													$scope.assetChronoNoData = true;
													return;
												} else {
													$scope.assetChronoNoData = false;
												}

												if ($scope.assetChronoData.length > 10) {
													$scope.PaginationTab = true;
												} else {
													$scope.PaginationTab = false;
												}
												$scope.totalItems = $scope.assetChronoData.length;
 												
												$scope.getTotalPages();

												$scope.currentPage = 1;
												if ($scope.assetChronoData.length > $scope.numPerPage) {
													$scope.assetChronoViewData = $scope.assetChronoData
															.slice(	0,$scope.numPerPage);
													$scope.hasPrevious = true;
													$scope.isFirstPage = true;
													$scope.hasNext = false;
													$scope.isLastPage = false;
												} else {
													$scope.assetChronoViewData = $scope.assetChronoData;
												}
											}else{
												$scope.assetChronoNoData = true;
											}
										},
										function error(response) {
											$rootScope.hideloading('#loadingBar');
											$rootScope.fnHttpError(response);
										});
					}

					// $scope.getAssetChronoData();

					$scope.fetchAsset = function() {
						
						if (!$rootScope.checkNetconnection()) { 
							$rootScope.alertDialogChecknet();
							return;
									 }  
						/*
						 * var fetchAsset =
						 * document.getElementById("idInputfetchAssetId"); if
						 * (fetchAsset.value.length == 0) {
						 * $rootScope.hideloading('#loadingBar');
						 * $scope.fetchAssetError = true;
						 * $scope.fetchAsset_error_msg = "Please enter Asset ID ";
						 * $timeout(function() { $scope.fetchAssetError = false; },
						 * 2000);
						 * 
						 * return; }
						 */
						$rootScope.showloading('#loadingBar');
						$scope.promise = $http
								.get('./assetChrono/' + $scope.fetchAssetId)
								.then(
										function(result) {
											$rootScope.hideloading('#loadingBar');
											$scope.fetchAssetIdAPIData = result;
											if (result.data.status == "success") {
												
												if (result.data.data[0].assignmentStatus == "NOT_ASSIGNED") {

													$scope.assetChronoAssignReturnFetchScreen = false;
													$scope.assetChronoAssignScreen = true;
													$scope.assetChronoReturnScreen = false;

													$scope.authen.assetId = result.data.data[0].assetId;
													$scope.authen.assetType = result.data.data[0].assetType;
													$scope.authen.subType = result.data.data[0].subType;
													$scope.authen.rangeValue = result.data.data[0].rangeValue;
													$scope.authen.manufacturer = result.data.data[0].manufacturer;
													$scope.authen.validityDate = result.data.data[0].validityDate;
													$scope.authen.type = result.data.data[0].type;

													$scope.authen.assignmentStatus = "PERSON";
													$scope.authen.assignedPerson = $scope.empViewData[0];
													if ($scope.authen.assignmentStatus == "PERSON") {
														$scope.showAssignedPerson  = true;
														$scope.showAssignmentDepartment = false;
														
														$scope.assignedPersonReadonly = false;
														$scope.assignmentDepartmentReadonly = true;
													}

													else if ($scope.authen.assignmentStatus == "DEPT") {
														$scope.showAssignedPerson  = false;
														$scope.showAssignmentDepartment = true;
														
														$scope.assignedPersonReadonly = true;
														$scope.assignmentDepartmentReadonly = false;
													}
												
										/*
										 * var startTime = ("0" +
										 * today.getHours()).slice(-2) + ":" +
										 * ("0" + today.getMinutes()).slice(-2);
										 * var endTime = ("0" +
										 * (today.getHours() + 2)) .slice(-2) +
										 * ":" + ("0" +
										 * today.getMinutes()).slice(-2);
										 */
													var startTime = moment(new Date()).format('YYYY-MMM-DD HH:mm');
													var endTime = moment(new Date()).format('YYYY-MMM-DD HH:mm');
													endTime =  moment(new Date()).format('YYYY-MMM-DD ')+("0" + (today.getHours() + 2))
													.slice(-2)
													+ ":"
													+ ("0" + today.getMinutes()).slice(-2);
											$("#stime").val(startTime);
											$("#etime").val(endTime);
											$scope.authen.stime = startTime;
											$scope.authen.etime = endTime;
											//$scope.authen.validityDate = moment(new Date()).format('YYYY-MMM-DD');
											$scope.authen.validityDate = moment(new Date()).format('YYYY-MMM-DD');
											$("#datetimepicker4").val(moment(new Date()).format('YYYY-MMM-DD'));
											
											// $scope.authen.date = moment(new
											// Date()).format("YYYY-MMM-DD");

												} else if(result.data.data[0].assignmentStatus == "ASSIGNED"){
													var assetAssignmentDetails =result.data.data[0].assetAssignmentDetails;
													var currentAssetAssignment;
													for (var key in assetAssignmentDetails){ 
														currentAssetAssignment = assetAssignmentDetails[key]
														break;
													}
													
													if(currentAssetAssignment.assignmentType == "PERSON"
														|| currentAssetAssignment.assignmentType == "DEPT") {

													$scope.assetChronoAssignReturnFetchScreen = false;
													$scope.assetChronoAssignScreen = false;
													$scope.assetChronoReturnScreen = true;

													$scope.authen.assetId = result.data.data[0].assetId;
													$scope.authen.assignmentStatus = result.data.data[0].assignmentStatus;
													
													
													$("#id_assetIdReturnAsset").val($scope.authen.assetId);
													$("#id_assignmentStatusReturnAsset").val($scope.authen.assignmentStatus);

													
													if (currentAssetAssignment.assignmentType == "PERSON") {
														$scope.authen.assignedTo = currentAssetAssignment.empId;
													} else if (currentAssetAssignment.assignmentType == "DEPT") {
														$scope.authen.assignedTo = currentAssetAssignment.deptName;
													}
													$("#id_assignedToReturnAsset").val($scope.authen.assignedTo);
												}
											}

											} else if (result.data.status == "error") {

												$rootScope.showAlertDialog(result.data.message);
											}

										},
										function(error) {
											$rootScope.hideloading('#loadingBar');
											
											$rootScope.fnHttpError(error);
											
										});						

					}

					$scope.assignmentStatusValue = function() {

						if ($scope.authen.assignmentStatus == "PERSON") {
							$scope.showAssignedPerson  = true;
							$scope.showAssignmentDepartment = false;
							
							$scope.assignedPersonReadonly = false;
							$scope.assignmentDepartmentReadonly = true;
							$scope.authen.assignedPerson = $scope.empViewData[0]; 
							$scope.authen.assignmentDepartment = "";
						}

						else if ($scope.authen.assignmentStatus == "DEPT") {
							$scope.showAssignedPerson  = false;
							$scope.showAssignmentDepartment = true;
							
							$scope.assignedPersonReadonly = true;
							$scope.assignmentDepartmentReadonly = false;
							$scope.authen.assignmentDepartment = $scope.departmentList[0]; 
							$scope.authen.assignedPerson = "";
						}

					}
					$scope.assignAssetChrono = function() {

						$("#assignAssetChronoForm")
								.validate(
										{
											// Rules for form validation
											rules : {

												assignmentStatus : {
													required : true
												},
												date : {
													required : true
												},
												stime : {
													required : true
												},
												etime : {
													required : true
												}
											},
											// Messages for form validation
											messages : {
												assignmentStatus : {
													required : 'Please select assignment status',
												},
												date : {
													required : 'Please enter date',
												},
												stime : {
													required : 'Please enter start time '
												},
												etime : {
													required : 'Please enter end time'
												}
											},

											// Do not change code below
											errorPlacement : function(error,
													element) {
												error.insertAfter(element
														.parent());
											}
										});
						if ($scope.assignAssetChronoForm.$valid) {
							
							if ($scope.authen.assignmentStatus == "PERSON") {
								var assignAssetChronoParameter = {
									"assetId" : $scope.authen.assetId,
									// "date" : $scope.authen.date,
									"fromTime" : $scope.authen.stime,
									"toTime" : $scope.authen.etime,
									"assignmentType" : $scope.authen.assignmentStatus,
									"empId" : $scope.authen.assignedPerson
									
								};
							} else if ($scope.authen.assignmentStatus == "DEPT") {

								var assignAssetChronoParameter = {
										"assetId" : $scope.authen.assetId,
										// "date" : $scope.authen.date,
										"fromTime" : $scope.authen.stime,
										"toTime" : $scope.authen.etime,
										"assignmentType" : $scope.authen.assignmentStatus,
										"deptName" : $scope.authen.assignmentDepartment
									
								};
							}
							if (!$rootScope.checkNetconnection()) { 
								$rootScope.alertDialogChecknet();
								return;
										 }
							if( $scope.authen.stime >= $scope.authen.etime)
							{
							
							$rootScope.showAlertDialog("Warning : End time should be later than Start time");
							return;
							}
							$rootScope.showloading('#loadingBar');
							$scope.promise = $http
									.put('./assignAssetChrono',
											assignAssetChronoParameter)
									.then(
											function(result) {
												$rootScope
												.hideloading('#loadingBar');
												if (result.data.status == "success") {

													$scope.assetChronoAssignScreen = false;
													$scope.assetChronoAssignReturnFetchScreen = true;
													$scope.assetChronoReturnScreen = true;
													$rootScope.showAlertDialog(result.data.message);
													$scope.cancelAssignAssetChrono();

												} else if (result.data.status == "error") {
													$rootScope.showAlertDialog(result.data.message);
													}

											},
											function(error) {
												$rootScope
												.hideloading('#loadingBar');
												$rootScope.fnHttpError(error);
											});
						}

					}

					$scope.cancelAssignAssetChrono = function() {
						// $('#idInputfetchAssetId').val("");
						// $scope.fetchAssetId = $scope.assetIdTable[0];
						$scope.authen.assetId="";
						$scope.authen.assetType="";
						$scope.authen.type= $scope.chronoAssetType[0];
						$scope.authen.subType="";
						$scope.authen.rangeValue="";
						$scope.authen.manufacturer="";
						$scope.authen.validityDate="";
						$scope.authen.assignedPerson="";
						// $scope.authen.date="";
						$scope.authen.stime="";
						$scope.authen.etime="";
						
						$scope.assetChronoAssignReturnFetchScreen = true;
						$scope.assetChronoAssignScreen = false;
						$scope.assetChronoReturnScreen = false;
					}

					$scope.returnAssetChrono = function() {
						
						// console.log($scope.fetchAssetIdAPIData)
						var assetAssignmentDetails =$scope.fetchAssetIdAPIData.data.data[0].assetAssignmentDetails;
						var currentAssetAssignment;
						for (var key in assetAssignmentDetails){ 
							currentAssetAssignment = assetAssignmentDetails[key]
							break;
						}
						
						if (currentAssetAssignment.assignmentType == "DEPT") {
							
							var returnAssetChronoParameter = {
								"assetId" : $scope.authen.assetId,
								"assignmentType" : currentAssetAssignment.assignmentType,
								"deptName" : $scope.authen.assignedTo
							};
						} else if (currentAssetAssignment.assignmentType == "PERSON") {
							var returnAssetChronoParameter = {
								"assetId" : $scope.authen.assetId,
								"assignmentType" : currentAssetAssignment.assignmentType,
								"empId" : $scope.authen.assignedTo
							};
						}
						if (!$rootScope.checkNetconnection()) { 
						 	$rootScope.alertDialogChecknet();
							return; }
						$rootScope.showloading('#loadingBar');
						$scope.promise = $http.post('./returnAssetChrono',
								returnAssetChronoParameter).then(
								function(result) {
									$rootScope.hideloading('#loadingBar');
									if (result.data.status == "success") {
										$rootScope.showAlertDialog(result.data.message);
										$scope.cancelreturnAssetChrono();
									} else if (result.data.status == "error") {
										$rootScope.showAlertDialog(result.data.message);
										
									}

								}, function(error) {
									$rootScope.hideloading('#loadingBar');
									$rootScope.fnHttpError(error);
								});
					}

					$scope.cancelreturnAssetChrono = function() {
						$scope.assetChronoAssignReturnFetchScreen = true;
						$scope.assetChronoAssignScreen = false;
						$scope.assetChronoReturnScreen = false;
					}
					$scope.editAssetValue = function() {
						$scope.assetChronoEditSelectScreen = false;
						$scope.assetChronoEditUpdateScreen = true;
						$scope.searchButton = false;
					}
					$scope.updateAssetChrono = function() {
							
							
							var assetId = document.getElementById("id_assetIdEditAsset");
							if (assetId.value.length == 0) {
								
								$scope.editAssetIdError = true;
								$scope.editAssetId_error_msg = "Please enter the Asset Id";
								$timeout(function() {
									$scope.editAssetIdError = false;
								}, 2000);
								return;
							}
								var assetType = document.getElementById("id_assetTypeEditAsset");
								if (assetType.value.length == 0) {
									
									$scope.editTypeError = true;
									$scope.editType_error_msg = "Please enter Type";
									$timeout(function() {
										$scope.editTypeError = false;
									}, 2000);
									return;
								}
								var subType = document.getElementById("id_subTypeEditAsset");
								if (subType.value.length == 0) {
									
									$scope.editSubTypeError = true;
									$scope.editSubType_error_msg = "Please enter Subtype";
									$timeout(function() {
										$scope.editSubTypeError = false;
									}, 2000);
									return;
								}
								/*var rangeValue = document.getElementById("id_rangeEditAsset");
								if (rangeValue.value.length == 0) {
									 $rootScope.hideloading('#loadingBar');
									$scope.editRangeError = true;
									$scope.editRange_error_msg = "Please enter Range Value";
									$timeout(function() {
										$scope.editRangeError = false;
									}, 2000);
									return;
								}*/
								var manufacturer = document.getElementById("id_manufacturerEditAsset");
								if (manufacturer.value.length == 0) {
									 
									$scope.editManufacturerError = true;
									$scope.editManufacturer_error_msg = "Please enter Manufacturer";
									$timeout(function() {
										$scope.editManufacturerError = false;
									}, 2000);
									return;
								}
								var expiryDate = document.getElementById("id_validityDateEditAsset");
								if (expiryDate.value.length == 0) {
									 
									$scope.editExpiryDateError = true;
									$scope.editExpiryDate_error_msg = "Please enter Expiry Date";
									$timeout(function() {
										$scope.editExpiryDateError = false;
									}, 2000);
									return;
								}
								
							//$scope.authen.validityDate = moment($scope.authen.validityDate,"YYYY-MMM-DD").format("MMM DD, YYYY"); 
							var validityDateEditAsset =  moment(expiryDate.value,"YYYY-MMM-DD").format("YYYY-MMM-DD");
							if ($scope.validityDateBeforeEdit != validityDateEditAsset ) {
								if (moment(validityDateEditAsset, 'YYYY-MMM-DD')<= moment(moment(new Date(), 'YYYY-MMM-DD').format('YYYY-MMM-DD'), 'YYYY-MMM-DD'))
									 {
									 $scope.editExpiryDateError = true;
										$scope.editExpiryDate_error_msg = "Please enter Expiry Date  greater than today's date";
										$timeout(function() {
											$scope.editExpiryDateError = false;
										}, 2000);
										return;
									 }
							}
							
							var editAssetChronoParameter = {
								"assetId" : $scope.authen.assetId,
								"assetType" : $scope.authen.assetType,
								"type" :$scope.authen.type,
								"subType" : $scope.authen.subType,
								"rangeValue" : $scope.authen.rangeValue,
								"manufacturer" : $scope.authen.manufacturer,
								"validityDate" : validityDateEditAsset

							};
							if (!$rootScope.checkNetconnection()) { 
							 	$rootScope.alertDialogChecknet();
								return; } 
							$rootScope.showloading('#loadingBar');
							$scope.promise = $http
									.put('./assetChrono',
											editAssetChronoParameter)
									.then(
											function success(result) {
												$rootScope.hideloading('#loadingBar');
												
												if (result.data.status == "success") {
													$scope.authen.validityDate = moment($scope.authen.validityDate,"YYYY-MMM-DD").format("MMM DD, YYYY"); 
													$scope.assetChronoViewData[$scope.selectedRow]=JSON.parse(JSON.stringify($scope.authen));
													$rootScope.showAlertDialog(result.data.message);
													$scope.cancelEditAssetChrono();

												} else if (result.data.status == "error") {
													$rootScope.showAlertDialog(result.data.message);
													}

											},
											function error(result) {
												$rootScope.hideloading('#loadingBar');
												$rootScope.fnHttpError(result);
												$scope.cancelEditAssetChrono();
												
												
											});
						
						
					}
					$scope.deleteAssetValue = function() {

						var name = document
								.getElementById("id_assetIdEditAsset");

						if (name.value.length == 0) {
							$scope.nameError = true;
							$scope.name_error_msg = "Please enter the Asset ID";
							$timeout(function() {
								$scope.nameError = false;

							}, 2000);

							return;
						}

						if ($scope.editAssetChronoForm.$valid) {
							$
									.confirm({
										title : $rootScope.MSGBOX_TITLE,
										content : 'Are you sure you want to delete this record?',
										type : 'blue',
										columnClass : 'medium',
										autoClose : false,
										icon : 'fa fa-info-circle',
										buttons : {
											yes : {
												text : 'Yes',
												btnClass : 'btn-blue',
												action : function() {
													
													var assetId = $scope.authen.assetId
													data = {
														"assetId" : assetId
													}
													if (!$rootScope.checkNetconnection()) { 
														$rootScope.alertDialogChecknet();
														return;
																 }
													$rootScope.showloading('#loadingBar');
													$http(
															{
																method : 'DELETE',
																url : './assetChrono',
																headers : {
																	'Content-Type' : 'application/json'
																},
																data : data,

															})
															.then(
																	function success(response) {
																		
																		if (response != null
																				&& response.data != null
																				&& response.data != "BAD_REQUEST") {
																			$rootScope
																					.hideloading('#loadingBar');
																			$scope.deletedAsset = response.data;
																			$rootScope.showAlertDialog(response.data.message);
																			$scope.getAssetChronoData();
																			$scope.cancelEdit();

																		} else {
																			$rootScope.hideloading('#loadingBar');
																			$rootScope.showAlertDialog(response.data.message);
																			$timeout(
																					function() {
																						$scope.cancelEdit();
																					},
																					2000);
																		}
																	},
																	function error(response) {
																		 $rootScope.hideloading('#loadingBar');
																		 $rootScope.fnHttpError(response);
																		
																	});
												}
											},
											no : function() {

											}
										}
									});
						}

					}
					

					$scope.cancelEdit = function() {
						$scope.selectedRow = -1;
						$scope.trackMacId = '';
					}
					$scope.cancelEditAssetChrono = function() {
						$scope.authen = {};
						$scope.selectedRow = -1;
						$scope.assetChronoEditSelectScreen = true;
						$scope.assetChronoEditUpdateScreen = false;
						$scope.searchButton = true;
					}

					
					$scope.rowHighilited = function(row) {
						$scope.selectedRow = row;
						// $scope.authen = $scope.assetChronoData[row];
						 $scope.authen = JSON.parse(JSON.stringify($scope.assetChronoViewData[row])); 
						 $scope.authen.validityDate =  moment($scope.authen.validityDate,"MMM DD, YYYY").format("YYYY-MMM-DD"); 
						 $scope.validityDateBeforeEdit = $scope.authen.validityDate;
 							
							if($scope.authen.assignedStatus=='Assigned'){
								if($scope.authen.assignedTagMacId !=undefined 
										&& $scope.authen.assignedTagMacId !=''){
									$scope.trackMacId = $scope.authen.assignedTagMacId ; 
								}else 
									$scope.trackMacId = ''; 
							}else 
								$scope.trackMacId = ''; 
					}

					// pagination button

					$scope.currentPage = 1;
					$scope.numPerPage = 10;
					$scope.maxSize = 3;
					$scope.totalItems = 0;
					$scope.assetChronoViewData = [];
					$scope.assetChronoDataFilterList = [];
					$scope
					.$watch(
					'currentPage + numPerPage',
					function() {
					var begin = (($scope.currentPage - 1) * $scope.numPerPage), end = begin
					+ $scope.numPerPage;
					// pagination for expire soon
					if ($scope.showCompareDateSelection == true){
						$scope.assetChronoViewData = $scope.finalassetChronoData
						.slice(begin, end);				
						$timeout(function() {
							$scope.getBgColorViewAllTable(); 
						}, 100);
					}
					// for other screens
					else{
						if ($scope.searchText == '') 
							$scope.assetChronoViewData = $scope.assetChronoData
							.slice(begin, end);
						else
							$scope.assetChronoViewData = $scope.assetChronoDataFilterList
							.slice(begin, end);
	
						}

					$scope.enablePageButtons();
					
					});

					$scope.currentPage = 1;
					$scope.maxSize = 3;
					$scope.numPerPage = 10;
					$scope.totalItems = 0;
					$scope.totalPages = 0;

					$scope.hasNext = true;
					$scope.hasPrevious = true;
					$scope.isFirstPage = true;
					$scope.isLastPage = true;
					$scope.serial = 1;

					$scope
							.$watch(
									"currentPage",
									function(newVal, oldVal) {
										if (newVal !== oldVal) {

											if (newVal > $scope.totalPages) {
												$scope.currentPage = $scope.totalPages;
											} else {
												$scope.currentPage = newVal;
											}

											$scope.serial = (($scope.currentPage - 1) * $scope.numPerPage) + 1;
											$scope.enablePageButtons();
										}
									});

					$scope.enablePageButtons = function() {
						$scope.serial = (($scope.currentPage - 1) * $scope.numPerPage) + 1;
						$scope.selectedRow = -1;
						if ($scope.totalPages <= 1) {
							$scope.hasPrevious = true;
							$scope.isFirstPage = true;
							$scope.hasNext = true;
							$scope.isLastPage = true;

						} else if ($scope.totalPages > 1
								&& $scope.currentPage == $scope.totalPages) {

							$scope.hasPrevious = false;
							$scope.isFirstPage = false;
							$scope.hasNext = true;
							$scope.isLastPage = true;
						} else if ($scope.currentPage != 1
								&& $scope.currentPage < $scope.totalPages) {

							$scope.hasPrevious = false;
							$scope.isFirstPage = false;
							$scope.hasNext = false;
							$scope.isLastPage = false;

						} else {
							$scope.hasPrevious = true;
							$scope.isFirstPage = true;
							$scope.hasNext = false;
							$scope.isLastPage = false;
						}
						 
					};

					$scope.getTotalPages = function() {

						if (($scope.totalItems % $scope.numPerPage) > 0)
							$scope.totalPages = parseInt($scope.totalItems
									/ $scope.numPerPage) + 1;
						else
							$scope.totalPages = parseInt($scope.totalItems
									/ $scope.numPerPage);

						$scope.enablePageButtons(); 
					};
					
					$scope.getAssignedTagIds = function(assetList) { 
				
						$scope.assignedTagMacId = '';
						
						if(assetList!=undefined && assetList!=null){
							for(var i=0; i<assetList.length; i++){
								 if(assetList[i].assignedStatus=='Assigned'){
										if(assetList[i].assignedTagMacId !=undefined 
												&& assetList[i].assignedTagMacId !=''){
											if($scope.assignedTagMacId=='')
												$scope.assignedTagMacId = assetList[i].assignedTagMacId;
											else
												$scope.assignedTagMacId = $scope.assignedTagMacId+","+assetList[i].assignedTagMacId;
												
										}
									}
							 } 
						}
						 
					}

					$scope.goFirstPage = function() {
						$scope.currentPage = 1;
						$scope.enablePageButtons();
						
						

					};

					$scope.goPreviousPage = function() {
						if ($scope.currentPage > 1)
							$scope.currentPage = $scope.currentPage - 1;
						$scope.enablePageButtons();
						
						
				};

					$scope.goNextPage = function() {
						if ($scope.currentPage <= $scope.totalPages)
							$scope.currentPage = $scope.currentPage + 1;
						$scope.enablePageButtons();
						
						
					};

					$scope.goLastPage = function() {
						$scope.currentPage = $scope.totalPages;
						$scope.enablePageButtons();
						
						
					};

					// add asset chrono

					$scope.addAssetChrono = function() {
						
						
						var assetId = document.getElementById("assetId");
						if (assetId.value.length == 0) {
							$scope.addAssetIdError = true;
							$scope.addAssetId_error_msg = "Please enter the Asset Id";
							$timeout(function() {
								$scope.addAssetIdError = false;
							}, 2000);
							return;
						}
							var assetType = document.getElementById("id_assetTypeAddAsset");
							if (assetType.value.length == 0) {
								 $scope.addTypeError = true;
								$scope.addType_error_msg = "Please enter Type";
								$timeout(function() {
									$scope.addTypeError = false;
								}, 2000);
								return;
							}
							var subType = document.getElementById("id_subTypeAddAsset");
							if (subType.value.length == 0) {
								 $scope.addSubTypeError = true;
								$scope.addSubType_error_msg = "Please enter Subtype";
								$timeout(function() {
									$scope.addSubTypeError = false;
								}, 2000);
								return;
							}
							/*var rangeValue = document.getElementById("id_rangeAddAsset");
							if (rangeValue.value.length == 0) {
								 $rootScope.hideloading('#loadingBar');
								$scope.addRangeError = true;
								$scope.addRange_error_msg = "Please enter Range Value";
								$timeout(function() {
									$scope.addRangeError = false;
								}, 2000);
								return;
							}*/
							var manufacturer = document.getElementById("id_manufacturerAddAsset");
							if (manufacturer.value.length == 0) {
								 $scope.addManufacturerError = true;
								$scope.addManufacturer_error_msg = "Please enter Manufacturer";
								$timeout(function() {
									$scope.addManufacturerError = false;
								}, 2000);
								return;
							}
							
							var expiryDate = document.getElementById("id_validityDateAddAsset");
							if (expiryDate.value.length == 0) {
								$scope.expiryDateError = true;
								$scope.expiryDate_error_msg = "Please enter Expiry Date";
								$timeout(function() {
									$scope.expiryDateError = false;
								}, 2000);
								return;
							}
							var addAssetChronoParameter = {
								"assetId" : $scope.authen.assetId,
								"assetType" : $scope.authen.assetType,
								"type": $scope.authen.type,
								"subType" : $scope.authen.subType,
								"rangeValue" : $scope.authen.range,
								"manufacturer" : $scope.authen.manufacturer,
								"validityDate" : $scope.authen.validityDate
						
							};
							if (!$rootScope.checkNetconnection()) { 
								$rootScope.alertDialogChecknet();
								return;
								} 
							$rootScope.showloading('#loadingBar');
							$scope.promise = $http
									.post('./assetChrono',
											addAssetChronoParameter)
									.then(
											function(result) {
												$rootScope.hideloading('#loadingBar');
												if (result.data.status == "success") {
													$rootScope.showAlertDialog(result.data.message);
													$scope.cancelAssetChrono();
													
												} else if (result.data.status == "error") {
													$rootScope.showAlertDialog(result.data.message);
												}

											},
											function(error) {
												$rootScope.hideloading('#loadingBar');
												$rootScope.fnHttpError(error);
											});
						
					}
					$scope.cancelAssetChrono = function() {
						
						$scope.authen.assetId = "";
						$scope.authen.assetType = "";
						$scope.authen.subType = "";
						$scope.authen.range = "";
						$scope.authen.manufacturer = "";
						$scope.authen.type = $scope.chronoAssetType[0];
						$scope.authen.validityDate = moment(new Date()).format('YYYY-MMM-DD');
						$("#datetimepicker4").val(moment(new Date()).format('YYYY-MMM-DD'));
						
					}
					 // asset chrono expire soon data in table
					$scope.getAssetChronoExpiryData = function() {
					$scope.DAY_ARRAY = [];
					$scope.FINAL_DAY_ARRAY = [];
					$scope.finalassetChronoData = [];

					if ($scope.assetChronoData.length == 0) {
					$scope.assetChronoNoData = true;
					return;
					} else {
					$scope.assetChronoNoData = false;
					function convert(str) {
					var date = new Date(str)
					           // var now_utc = new
								// Date(date.toISOString().slice(0, 10));
					           var mnth = ("0" + (date.getMonth() + 1)).slice(-2);
					           var day = ("0" + date.getDate()).slice(-2);
					           return [date.getFullYear(), mnth, day].join("-");
					}
					var getDatesBetweenDates = (startDate, endDate) => {
					           let dates = []
					               // to avoid
					// modifying the
					// original date
					           const theDate = new Date(startDate);
					           const theendDate = new Date(endDate);

					           while (theDate < theendDate) {
					               dates = [...dates, new Date(theDate)]
					               theDate.setDate(theDate.getDate() + 1)
					           }
					           dates = [...dates, endDate]
					           return dates
					       }
					
					function formatDate1(date) {
					var mS = [ 'Jan', 'Feb', 'Mar', 'Apr', 'May', 'June','Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec' ];
					var sp_date = date.split("-");
					return   (sp_date[0] + "-" + mS[parseInt(sp_date[1]) - 1]+ "-"+  sp_date[2])
					}

					$scope.DAY_ARRAY = getDatesBetweenDates( new Date($scope.startDateExpiry),new Date($scope.endDateExpiry));

					for (var da = 0; da < $scope.DAY_ARRAY.length; da++) {
					$scope.FINAL_DAY_ARRAY[da] = formatDate1(convert($scope.DAY_ARRAY[da].toString()));
					}

					for (var i = 0; i < $scope.assetChronoData.length; i++) {
					if ($scope.FINAL_DAY_ARRAY.includes($scope.assetChronoData[i].validityDateExpiry))
					{
					var dateObj = $scope.assetChronoData[i];
					$scope.finalassetChronoData.push(dateObj);
					}
					}
					}
					// to sort based on validity date
					$scope.finalassetChronoData.sort($scope.sortBasedOnValidityDateFunction);
					
					if ($scope.finalassetChronoData.length > 10) {
					$scope.PaginationTab = true;
					} else {
					$scope.PaginationTab = false;
					}
					$scope.totalItems = $scope.finalassetChronoData.length;
					$scope.getTotalPages();
					$scope.currentPage = 1;

					$scope.getAssignedTagIds($scope.finalassetChronoData);
					
					if ($scope.finalassetChronoData.length > $scope.numPerPage) {
					$scope.assetChronoViewData = $scope.finalassetChronoData.slice( 0,$scope.numPerPage);
					$scope.hasPrevious = true;
					$scope.isFirstPage = true;
					$scope.hasNext = false;
					$scope.isLastPage = false;
					} else {
					$scope.assetChronoViewData = $scope.finalassetChronoData;
					}					
					if ($scope.showCompareDateSelection == true){
						$timeout(function() {
							$scope.getBgColorViewAllTable(); 
						}, 100);
					}
					
					
					if ($scope.assetChronoViewData.length == 0) {
					$scope.assetChronoNoData = true;
					return;
					} else {
					$scope.assetChronoNoData = false;
					}

					} 
					$scope.getBgColorViewAllTable = function() {
						// to give bg-color for table row based on validity date
						
						var selectedRowCSS_Gray = {'background': '#696969', 'color': '#ffffff' };
						var selectedRowCSS_Red = {'background': '#ff0000', 'color': '#ffffff' };
						var selectedRowCSS3_Orange = {'background': '#ff8b00', 'color': '#ffffff' };
						var selectedRowCSS4_Green = {'background': '#00ff00', 'color': '#000000' };
						
						for (var i = 0; i < $scope.assetChronoViewData.length; i++) {						
						var validityDateExpiry =  moment($scope.assetChronoViewData[i].validityDate);
						var currentDay = moment(new Date()).format('MMM DD, YYYY');
						
						if (validityDateExpiry != undefined && validityDateExpiry != null ) {

							var diff = moment(currentDay) - validityDateExpiry;

							 if ( diff == 0) {
									$("#idViewAllTable > tbody > tr:nth-child(" + (i + 1) + ")").css(selectedRowCSS_Gray);
							}else  if ( diff>0) {
								$("#idViewAllTable > tbody > tr:nth-child(" + (i + 1) + ")").css(selectedRowCSS_Red);
							} 
							 // next 7 days in orange
							else if ( diff <= -86400000 && diff >= -604800000) { 
								$("#idViewAllTable > tbody > tr:nth-child(" + (i + 1) + ")").css(selectedRowCSS3_Orange);
							} 
							else   {
								$("#idViewAllTable > tbody > tr:nth-child(" + (i + 1) + ")").css(selectedRowCSS4_Green);
							}

						}
						}
						 
						}

					$scope.getBgColorExpiryTable = function() {
						// to give bg-color for table row based on validity date
						
						var selectedRowCSS_Gray = {'background': '#696969', 'color': '#ffffff' };
						var selectedRowCSS_Red = {'background': '#ff0000', 'color': '#ffffff' };
						var selectedRowCSS3_Orange = {'background': '#ff8b00', 'color': '#ffffff' };
						var selectedRowCSS4_Green = {'background': '#00ff00', 'color': '#000000' };
						
						for (var i = 0; i < $scope.assetChronoViewData.length; i++) {						
						var validityDateExpiry =  moment($scope.assetChronoViewData[i].validityDate);
						var currentDay = moment(new Date()).format('MMM DD, YYYY');
						
						if (validityDateExpiry != undefined && validityDateExpiry != null ) {

							var diff = moment(currentDay) - validityDateExpiry;

							 if ( diff == 0) {
									$("#idExpiryTable > tbody > tr:nth-child(" + (i + 1) + ")").css(selectedRowCSS_Red);
							}else  if ( diff>0) {
								$("#idExpiryTable > tbody > tr:nth-child(" + (i + 1) + ")").css(selectedRowCSS_Gray);
							} 
							 // next 7 days in orange
							else if ( diff <= -86400000 && diff >= -604800000) { 
								$("#idExpiryTable > tbody > tr:nth-child(" + (i + 1) + ")").css(selectedRowCSS3_Orange);
							} 
							else   {
								$("#idExpiryTable > tbody > tr:nth-child(" + (i + 1) + ")").css(selectedRowCSS4_Green);
							}

						}
						}
						 
						}
					$scope.displayChronoViewScreen = function() {
						$scope.assetChronoViewData = [];
						$scope.assetChronoDataFilterList = [];
						$scope.showSearchIcon = true;
						$scope.showDownloadIcon = true;
						$scope.showMultipleSearch = false;
						$scope.searchText = "";
						$scope.selectedRow = -1;
						$scope.currentPage = 1;
						$scope.searchButton = true;
						$scope.authen.type = $scope.chronoAssetType[0];
						$scope.assetChronoViewAllScreen = true;
						$scope.assetChronoEditSelectScreen = false;
						$scope.assetChronoEditUpdateScreen = false;
						$scope.assetChronoAddScreen = false;
						$scope.assetChronoAssignReturnFetchScreen = false;
						$scope.assetChronoAssignScreen = false;
						$scope.assetChronoReturnScreen = false;
						//$scope.returnChronoFetchScreen = false;
						
						
						$scope.showCompareDateSelection = false;
						
						
						$scope.searchButton = true;
						$("#idChronoView").addClass("active");
						$("#idChronoAdd").removeClass("active");
						$("#idChronoEdit").removeClass("active");
						$("#idChronoAssign").removeClass("active");
						$("#idChronoExpire").removeClass("active");
						$scope.getAssetChronoData();
						};
						$scope.displayChronoAddScreen = function() {
						$scope.cancelAssetChrono();
						$scope.showMultipleSearch = false;
						$scope.showSearchIcon = false;
						$scope.showDownloadIcon = false;
						$scope.assetChronoViewAllScreen = false;
						$scope.assetChronoEditSelectScreen = false;
						$scope.assetChronoEditUpdateScreen = false;
						$scope.searchButton = false;
						$scope.assetChronoAddScreen = true;
						$scope.assetChronoAssignReturnFetchScreen = false;
						$scope.assetChronoAssignScreen = false;
						$scope.assetChronoReturnScreen = false;
						//$scope.returnChronoFetchScreen = false;
						
						$scope.showCompareDateSelection = false;
						
						
						$("#idChronoView").removeClass("active");
						$("#idChronoAdd").addClass("active");
						$("#idChronoEdit").removeClass("active");
						$("#idChronoAssign").removeClass("active");
						$("#idChronoExpire").removeClass("active");

						$('#id_validityDateAddAsset').val(moment(new Date()).format("YYYY-MMM-DD"));
						$scope.authen.validityDate = moment(new Date()).format("YYYY-MMM-DD");

						};
						$scope.displayChronoEditScreen = function() {
						$scope.assetChronoViewData = [];
						$scope.assetChronoDataFilterList = [];
						$scope.showSearchIcon = true;
						$scope.showDownloadIcon = false;
						$scope.showMultipleSearch = false;
						$scope.searchText = "";
						$scope.selectedRow = -1;
						$scope.currentPage = 1;
						$scope.searchButton = true;
						$scope.assetChronoViewAllScreen = false;
						$scope.assetChronoEditSelectScreen = true;
						$scope.assetChronoEditUpdateScreen = false;
						$scope.assetChronoAddScreen = false;
						$scope.assetChronoAssignReturnFetchScreen = false;
						$scope.assetChronoAssignScreen = false;
						$scope.assetChronoReturnScreen = false;
						//$scope.returnChronoFetchScreen = false;
						
						
						$scope.showCompareDateSelection = false;
						
						
						$("#idChronoView").removeClass("active");
						$("#idChronoAdd").removeClass("active");
						$("#idChronoEdit").addClass("active");
						$("#idChronoAssign").removeClass("active");
						$("#idChronoExpire").removeClass("active");
						
						$scope.getAssetChronoData();
						};
						$scope.displayChronoAssignScreen = function() {
						// $('#idInputfetchAssetId').val("");
							$scope.showMultipleSearch = false;
							$scope.showSearchIcon = false;
							$scope.showDownloadIcon = false;
							$scope.showMultipleSearch = false;
						$scope.assetChronoViewAllScreen = false;
						$scope.assetChronoEditSelectScreen = false;
						$scope.assetChronoEditUpdateScreen = false;
						$scope.assetChronoAddScreen = false;
						$scope.assetChronoAssignReturnFetchScreen = true;
						$scope.assetChronoAssignScreen = false;
						$scope.searchButton = false;
						//$scope.returnChronoFetchScreen = false;
						$scope.showCompareDateSelection = false;
						
						
						$("#idChronoView").removeClass("active");
						$("#idChronoAdd").removeClass("active");
						$("#idChronoEdit").removeClass("active");
						$("#idChronoAssign").addClass("active");
						$("#idChronoExpire").removeClass("active");

						// $('#id_validityDateAddAsset').val(moment(new
						// Date()).format("YYYY-MMM-DD"));
						// $scope.authen.date = moment(new
						// Date()).format("YYYY-MMM-DD");
						$scope.getAssetChronoData();
						$scope.fetchAssetId = $scope.assetIdTable[0];

						};
						


						// to display enabled sub menus
						if ($scope.user.capabilityDetails != undefined
						&& $scope.user.capabilityDetails != null) {
						if ($scope.user.capabilityDetails.ACVA != 0) {
						$scope.displayChronoViewScreen();
						}
						if ($scope.user.capabilityDetails.ACVA != 1) {
						$scope.displayChronoAddScreen();
						}
						if ($scope.user.capabilityDetails.ACVA != 1
						&& $scope.user.capabilityDetails.ACAD != 1) {
						$scope.displayChronoEditScreen();
						}
						if ($scope.user.capabilityDetails.ACVA != 1
						&& $scope.user.capabilityDetails.ACAD != 1
						&& $scope.user.capabilityDetails.ACED != 1) {
						$scope.displayChronoAssignScreen();
						}
					
						if ($scope.user.capabilityDetails.ACVA == 1
						&& $scope.user.capabilityDetails.ACAD == 1
						&& $scope.user.capabilityDetails.ACED == 1
						&& $scope.user.capabilityDetails.ACAR != 1) {
						return;
						} else {
						$scope.getDepartmentDetails();
						$scope.getStaffDetails();
						     
						}
						}
						// Compare date
						$scope.fnExpiryDateFilter = function() {
							$scope.showMultipleSearch = false;
							$scope.searchButton = false;
							$scope.showSearchIcon = false;
							$scope.showDownloadIcon = false;
							$scope.showCompareDateSelection = true;
							// $scope.showCompareDate = false; // show when custom
															// dates are equal
							
							$scope.varCompareDate = "Equal";
							$scope.dateRange = "Today";
							$scope.disableCompareDate = false;
							$scope.startDateExpiry =  moment(new Date()).format("MMM DD, YYYY");
							$scope.endDateExpiry = moment(new Date()).format("MMM DD, YYYY");
							$scope.getAssetChronoExpiryData();
							//$scope.$apply();						
						}
						// Compare date
						$scope.fnCompareDateChange= function() {
							if ($scope.varCompareDate == "Greater"){
								//toastr.error('Greater.', "EI4.0");
								const tomorrow = new Date();
								tomorrow.setDate(tomorrow.getDate() + 1);
								$scope.startDateExpiry =  moment(tomorrow).format("MMM DD, YYYY");
								$scope.endDateExpiry = moment().add(2, 'year').endOf('year').format('MMM DD, YYYY');
								
							}
							else if ($scope.varCompareDate == "Less"){
								const yesterday = new Date();
								yesterday.setDate(yesterday.getDate() - 1);
								$scope.endDateExpiry =  moment(yesterday).format("MMM DD, YYYY");
								$scope.startDateExpiry  = moment().subtract(2, 'year').endOf('year').format('MMM DD, YYYY');
								

							}
							else if ($scope.varCompareDate == "Equal"){
								$scope.startDateExpiry =  moment(new Date()).format("MMM DD, YYYY");
								$scope.endDateExpiry = moment(new Date()).format("MMM DD, YYYY");
								
							}
							$scope.getAssetChronoExpiryData();
							//$scope.$apply();	
						}
						// multisearch code starts
						$scope.multipleSearch = function() {
							$scope.showMultipleSearch = true;
							$scope.searchButton = false
							$scope.showDownloadIcon = false;
							$scope.showCompareDateSelection = false;
							$scope.firstMultiSearchDropDown = "All";
							$scope.secondMultiSearchDropDown = "All";
							$scope.varANDORDropDown = "AND";
							$scope.firstSearchText = "";
							$scope.secondSearchText = "";
						}
						// Compare date
						$scope.fnCloseCompareDateView = function() {
							$scope.showMultipleSearch = false;
							$scope.searchButton = true;
							$scope.showSearchIcon = true;
							$scope.showDownloadIcon = true;
							$scope.showCompareDateSelection = false;
							$scope.showDownloadIcon = true;
							$scope.fnDisplayAllrecords();
							var selectedRowCSS_white = {'background': '#ffffff', 'color': '#000000' };
							for (var i = 0; i < $scope.assetChronoViewData.length; i++) {						
									$("#idViewAllTable > tbody > tr:nth-child(" + (i + 1) + ")").css(selectedRowCSS_white);
							}
							//$scope.$apply();	
						}
						$scope.closeMultiSearchView = function() {
							$scope.showMultipleSearch = false;
							$scope.searchButton = true;
							$scope.showDownloadIcon = true;
							$scope.showCompareDateSelection = false;
							$scope.fnDisplayAllrecords();
						}
						$scope.fnDisplayAllrecords = function() {
							$scope.assetChronoDataFilterList = $scope.assetChronoData;
							$scope.totalItems = $scope.assetChronoDataFilterList.length;
							$scope.currentPage = 1;
							$scope.getTotalPages();
							
							$scope.getAssignedTagIds($scope.assetChronoDataFilterList);
							
							 if($scope.assetChronoData.length == 0 || $scope.assetChronoDataFilterList.length == 0 ){
									$scope.assetChronoNoData = true;

								}else{
									$scope.assetChronoNoData = false;

							if ($scope.assetChronoDataFilterList.length > $scope.numPerPage){								
								 $scope.assetChronoViewData = JSON.parse(JSON.stringify($scope.assetChronoDataFilterList.slice(0, $scope.numPerPage))); 
							$scope.PaginationTab=true;
							}
							else {
								$scope.assetChronoViewData = JSON.parse(JSON.stringify($scope.assetChronoDataFilterList)); 
								$scope.PaginationTab=false;
							}
						}
						}
						
						
						$scope.fnfirstSearchANDData = function() {
							
							$scope.firstSearchANDDataArray = [];			
						for (var i = 0; i < $scope.assetChronoData.length; i++) {
							// to check all
							if ($scope.firstMultiSearchDropDown == "All"){
							if ($scope.firstSearchText == "" || $scope.firstSearchText == undefined) {
								
								$scope.firstSearchANDDataArray.push($scope.assetChronoData[i]);
								
							} else{
								if (($scope.assetChronoData[i].assetId != undefined
										&& $scope.assetChronoData[i].assetId != null ? $scope.assetChronoData[i].assetId
										: "").toLowerCase().includes(
										$scope.firstSearchText.toLowerCase())
										|| ($scope.assetChronoData[i].assetType != undefined
												&& $scope.assetChronoData[i].assetType != null ? $scope.assetChronoData[i].assetType
												: "").toLowerCase()
												.includes(
														$scope.firstSearchText
																.toLowerCase())
										|| ($scope.assetChronoData[i].type != undefined
												&& $scope.assetChronoData[i].type != null ? $scope.assetChronoData[i].type
												: "").toLowerCase()
												.includes(
														$scope.firstSearchText
																.toLowerCase())						
										|| ($scope.assetChronoData[i].subType != undefined
												&& $scope.assetChronoData[i].subType != null ? $scope.assetChronoData[i].subType
												: "").toLowerCase()
												.includes(
														$scope.firstSearchText
																.toLowerCase())
										|| ($scope.assetChronoData[i].rangeValue != undefined
												&& $scope.assetChronoData[i].rangeValue != null ? $scope.assetChronoData[i].rangeValue
												: "").toLowerCase()
												.includes(
														$scope.firstSearchText
																.toLowerCase())
										|| ($scope.assetChronoData[i].manufacturer != undefined
												&& $scope.assetChronoData[i].manufacturer != null ? $scope.assetChronoData[i].manufacturer
												: "").toLowerCase()
												.includes(
														$scope.firstSearchText
																.toLowerCase())
										|| ($scope.assetChronoData[i].validityDate != undefined
												&& $scope.assetChronoData[i].validityDate != null ? $scope.assetChronoData[i].validityDate
												: "").toLowerCase()
												.includes(
														$scope.firstSearchText
																.toLowerCase())										
										|| ($scope.assetChronoData[i].assignedPerson != undefined
												&& $scope.assetChronoData[i].assignedPerson != null ? $scope.assetChronoData[i].assignedPerson
												: "").toLowerCase()
												.includes(
														$scope.firstSearchText
																.toLowerCase())											
										|| ($scope.assetChronoData[i].deptName != undefined
												&& $scope.assetChronoData[i].deptName != null ? $scope.assetChronoData[i].deptName
												: "").toLowerCase()
												.includes(
														$scope.firstSearchText
																.toLowerCase())
										|| ($scope.assetChronoData[i].fromTime != undefined
												&& $scope.assetChronoData[i].fromTime != null ? $scope.assetChronoData[i].fromTime
												: "").toLowerCase()
												.includes(
														$scope.firstSearchText
																.toLowerCase())
										|| ($scope.assetChronoData[i].toTime != undefined
												&& $scope.assetChronoData[i].toTime != null ? $scope.assetChronoData[i].toTime
												: "").toLowerCase()
												.includes(
														$scope.firstSearchText
																.toLowerCase())){
									$scope.firstSearchANDDataArray
											.push($scope.assetChronoData[i]);

								}

							}
							}
							// to check other than all
							else{
								if($scope.firstMultiSearchDropDown == "assetId"){
									if ($scope.firstSearchText	== "" || $scope.firstSearchText == undefined) {						
										$scope.firstSearchANDDataArray.push($scope.assetChronoData[i]);						
									}
									else if((($scope.assetChronoData[i].assetId).toString() != undefined&& ($scope.assetChronoData[i].assetId).toString() != null ?
											($scope.assetChronoData[i].assetId).toString(): "").toLowerCase().includes($scope.firstSearchText.toLowerCase()))					
										$scope.firstSearchANDDataArray.push($scope.assetChronoData[i]);
									
								}else if($scope.firstMultiSearchDropDown == "assetType"){
									if ($scope.firstSearchText	== "" || $scope.firstSearchText == undefined) {						
										$scope.firstSearchANDDataArray.push($scope.assetChronoData[i]);						
									}
									else if(($scope.assetChronoData[i].type != undefined
												&& $scope.assetChronoData[i].type != null ? $scope.assetChronoData[i].type: "").toLowerCase()
												.includes($scope.firstSearchText.toLowerCase()))					
											$scope.firstSearchANDDataArray.push($scope.assetChronoData[i]);
									
								}else if($scope.firstMultiSearchDropDown == "subType"){
									if ($scope.firstSearchText	== "" || $scope.firstSearchText == undefined) {						
											$scope.firstSearchANDDataArray.push($scope.assetChronoData[i]);						
									}
									else if(($scope.assetChronoData[i].subType != undefined
													&& $scope.assetChronoData[i].subType != null ? $scope.assetChronoData[i].subType: "").toLowerCase()
													.includes($scope.firstSearchText.toLowerCase()))					
												$scope.firstSearchANDDataArray.push($scope.assetChronoData[i]);
									
								}else if($scope.firstMultiSearchDropDown == "range"){
									if ($scope.firstSearchText	== "" || $scope.firstSearchText == undefined) {						
										$scope.firstSearchANDDataArray.push($scope.assetChronoData[i]);						
								}
									else if(($scope.assetChronoData[i].rangeValue != undefined
												&& $scope.assetChronoData[i].rangeValue != null ? $scope.assetChronoData[i].rangeValue
												: "")
												.toLowerCase()
												.includes(
														$scope.firstSearchText
																.toLowerCase()))					
											$scope.firstSearchANDDataArray.push($scope.assetChronoData[i]);
							}else if($scope.firstMultiSearchDropDown == "type"){
								if ($scope.firstSearchText	== "" || $scope.firstSearchText == undefined) {						
									$scope.firstSearchANDDataArray.push($scope.assetChronoData[i]);						
							}
							else if(($scope.assetChronoData[i].assetType != undefined
									&& $scope.assetChronoData[i].assetType != null ? $scope.assetChronoData[i].assetType
											: "").toLowerCase().includes($scope.firstSearchText.toLowerCase()))					
										$scope.firstSearchANDDataArray.push($scope.assetChronoData[i]);
							
							}else if($scope.firstMultiSearchDropDown == "manufacturer"){
							if ($scope.firstSearchText	== "" || $scope.firstSearchText == undefined) {						
								$scope.firstSearchANDDataArray.push($scope.assetChronoData[i]);						
							}
								else if(($scope.assetChronoData[i].manufacturer != undefined
								&& $scope.assetChronoData[i].manufacturer != null ? $scope.assetChronoData[i].manufacturer
										: "").toLowerCase().includes($scope.firstSearchText.toLowerCase()))					
									$scope.firstSearchANDDataArray.push($scope.assetChronoData[i]);
						
						}else if($scope.firstMultiSearchDropDown == "validityDate"){
								if ($scope.firstSearchText	== "" || $scope.firstSearchText == undefined) {						
									$scope.firstSearchANDDataArray.push($scope.assetChronoData[i]);						
								}
								else if(($scope.assetChronoData[i].validityDate != undefined
										&& $scope.assetChronoData[i].validityDate != null ? $scope.assetChronoData[i].validityDate: "").toLowerCase()
									.includes($scope.firstSearchText.toLowerCase()))					
								$scope.firstSearchANDDataArray.push($scope.assetChronoData[i]);
					
						}else if($scope.firstMultiSearchDropDown == "assignedTo"){	
							if ($scope.firstSearchText	== "" || $scope.firstSearchText == undefined) {						
								$scope.firstSearchANDDataArray.push($scope.assetChronoData[i]);						
							}
							else if(($scope.assetChronoData[i].assignedPerson != undefined
									&& $scope.assetChronoData[i].assignedPerson != null ? $scope.assetChronoData[i].assignedPerson
											: "").toLowerCase()
											.includes(
													$scope.firstSearchText
															.toLowerCase())											
									|| ($scope.assetChronoData[i].deptName != undefined
											&& $scope.assetChronoData[i].deptName != null ? $scope.assetChronoData[i].deptName
											: "").toLowerCase()
											.includes(
													$scope.firstSearchText
															.toLowerCase()))					
							$scope.firstSearchANDDataArray.push($scope.assetChronoData[i]);
														
						
						}else if($scope.firstMultiSearchDropDown == "fromTime"){
								if ($scope.firstSearchText	== "" || $scope.firstSearchText == undefined) {						
									$scope.firstSearchANDDataArray.push($scope.assetChronoData[i]);						
								}
								else if(($scope.assetChronoData[i].shiftDetails.fromTime != undefined
												&& $scope.assetChronoData[i].fromTime != null ? $scope.assetChronoData[i].fromTime
												: "")
												.toLowerCase()
												.includes(
														$scope.firstSearchText
																.toLowerCase()))					
								$scope.firstSearchANDDataArray.push($scope.assetChronoData[i]);
					
							}else if($scope.firstMultiSearchDropDown == "toTime"){
								if ($scope.firstSearchText	== "" || $scope.firstSearchText == undefined) {						
									$scope.firstSearchANDDataArray.push($scope.assetChronoData[i]);						
								}
								else if(($scope.assetChronoData[i].toTime != undefined
												&& $scope.assetChronoData[i].toTime != null ? $scope.assetChronoData[i].toTime
												: "")
										 		.toLowerCase()
												.includes(
														$scope.firstSearchText
																.toLowerCase()))					
								$scope.firstSearchANDDataArray.push($scope.assetChronoData[i]);
					
							}					
								
							}
					}
					}
						
						
						$scope.multiSearchChanged = function() {
							$scope.firstSearchANDDataArray = [];
							$scope.fnfirstSearchANDData();
							//console.log($scope.firstSearchANDDataArray);
							
							if($scope.varANDORDropDown == "AND"){			
								$scope.disableSecondFilterText = false;
								$scope.disableSecondFilterDropDown = false;
								$scope.assetChronoViewData=[];
								$scope.assetChronoDataFilterList=[];

								for (var i = 0; i < $scope.firstSearchANDDataArray.length; i++) {
									if ($scope.secondMultiSearchDropDown == "All"){
										
										if ($scope.secondSearchText == "" || $scope.secondSearchText == undefined) {
											$scope.assetChronoDataFilterList
													.push($scope.firstSearchANDDataArray[i]);
										} else {
											if ((($scope.firstSearchANDDataArray[i].assetId).toString() != undefined&& ($scope.firstSearchANDDataArray[i].assetId).toString() != null ?
													($scope.firstSearchANDDataArray[i].assetId).toString(): "").toLowerCase().includes($scope.secondSearchText.toLowerCase())
													|| ($scope.firstSearchANDDataArray[i].assetType != undefined
															&& $scope.firstSearchANDDataArray[i].assetType != null ? $scope.firstSearchANDDataArray[i].assetType: "").toLowerCase()
															.includes($scope.secondSearchText.toLowerCase())
													|| ($scope.firstSearchANDDataArray[i].subType != undefined
															&& $scope.firstSearchANDDataArray[i].subType != null ? $scope.firstSearchANDDataArray[i].subType
															: "")
															.toLowerCase()
															.includes(
																	$scope.secondSearchText
																			.toLowerCase())
													|| ($scope.firstSearchANDDataArray[i].rangeValue != undefined
															&& $scope.firstSearchANDDataArray[i].rangeValue != null ? $scope.firstSearchANDDataArray[i].rangeValue
															: "")
															.toLowerCase()
															.includes(
																	$scope.secondSearchText
																			.toLowerCase())
													|| ($scope.firstSearchANDDataArray[i].type != undefined
															&& $scope.firstSearchANDDataArray[i].type != null ? $scope.firstSearchANDDataArray[i].type
															: "")
															.toLowerCase()
															.includes(
																	$scope.secondSearchText
																			.toLowerCase())
													|| ($scope.firstSearchANDDataArray[i].manufacturer != undefined
															&& $scope.firstSearchANDDataArray[i].manufacturer != null ? $scope.firstSearchANDDataArray[i].manufacturer
															: "")
															.toLowerCase()
															.includes(
																	$scope.secondSearchText
																			.toLowerCase())
													|| ($scope.firstSearchANDDataArray[i].validityDate != undefined
															&& $scope.firstSearchANDDataArray[i].validityDate != null ? $scope.firstSearchANDDataArray[i].validityDate: "")
															.toLowerCase().includes($scope.secondSearchText.toLowerCase())
													|| ($scope.firstSearchANDDataArray[i].assignedPerson != undefined
														&& $scope.firstSearchANDDataArray[i].assignedPerson != null ? $scope.firstSearchANDDataArray[i].assignedPerson: "")
														.toLowerCase().includes($scope.secondSearchText.toLowerCase())											
													|| ($scope.firstSearchANDDataArray[i].deptName != undefined
														&& $scope.firstSearchANDDataArray[i].deptName != null ? $scope.firstSearchANDDataArray[i].deptName: "")
														.toLowerCase().includes($scope.secondSearchText.toLowerCase())
													|| ($scope.firstSearchANDDataArray[i].fromTime != undefined
															&& $scope.firstSearchANDDataArray[i].fromTime != null ? $scope.firstSearchANDDataArray[i].fromTime
															: "")
															.toLowerCase()
															.includes(
																	$scope.secondSearchText
																			.toLowerCase())
													|| ($scope.firstSearchANDDataArray[i].toTime != undefined
															&& $scope.firstSearchANDDataArray[i].toTime != null ? $scope.firstSearchANDDataArray[i].toTime
															: "")
													 		.toLowerCase()
															.includes(
																	$scope.secondSearchText
																			.toLowerCase())) {
												$scope.assetChronoDataFilterList
														.push($scope.firstSearchANDDataArray[i]);

											}
										}	
									}else{

										if($scope.secondMultiSearchDropDown == "assetId"){
											if ($scope.secondSearchText	== "" || $scope.secondSearchText == undefined) {						
												$scope.assetChronoDataFilterList.push($scope.firstSearchANDDataArray[i]);						
											}
											else if((($scope.firstSearchANDDataArray[i].assetId).toString() != undefined&& ($scope.firstSearchANDDataArray[i].assetId).toString() != null ?
													($scope.firstSearchANDDataArray[i].assetId).toString(): "").toLowerCase().includes($scope.secondSearchText.toLowerCase()))					
												$scope.assetChronoDataFilterList.push($scope.firstSearchANDDataArray[i]);
											
										}else if($scope.secondMultiSearchDropDown == "assetType"){
											if ($scope.secondSearchText	== "" || $scope.secondSearchText == undefined) {						
												$scope.assetChronoDataFilterList.push($scope.firstSearchANDDataArray[i]);						
											}
											else if(($scope.firstSearchANDDataArray[i].type != undefined
														&& $scope.firstSearchANDDataArray[i].type != null ? $scope.firstSearchANDDataArray[i].type: "").toLowerCase()
														.includes($scope.secondSearchText.toLowerCase()))					
													$scope.assetChronoDataFilterList.push($scope.firstSearchANDDataArray[i]);
											
										}else if($scope.secondMultiSearchDropDown == "subType"){
											if ($scope.secondSearchText	== "" || $scope.secondSearchText == undefined) {						
													$scope.assetChronoDataFilterList.push($scope.firstSearchANDDataArray[i]);						
											}
											else if(($scope.firstSearchANDDataArray[i].subType != undefined
															&& $scope.firstSearchANDDataArray[i].subType != null ? $scope.firstSearchANDDataArray[i].subType: "").toLowerCase()
															.includes($scope.secondSearchText.toLowerCase()))					
														$scope.assetChronoDataFilterList.push($scope.firstSearchANDDataArray[i]);
											
										}else if($scope.secondMultiSearchDropDown == "range"){
											if ($scope.secondSearchText	== "" || $scope.secondSearchText == undefined) {						
												$scope.assetChronoDataFilterList.push($scope.firstSearchANDDataArray[i]);						
										}
											else if(($scope.firstSearchANDDataArray[i].rangeValue != undefined
														&& $scope.firstSearchANDDataArray[i].rangeValue != null ? $scope.firstSearchANDDataArray[i].rangeValue
														: "")
														.toLowerCase()
														.includes(
																$scope.secondSearchText
																		.toLowerCase()))					
													$scope.assetChronoDataFilterList.push($scope.firstSearchANDDataArray[i]);
									}else if($scope.secondMultiSearchDropDown == "type"){
										if ($scope.secondSearchText	== "" || $scope.secondSearchText == undefined) {						
											$scope.assetChronoDataFilterList.push($scope.firstSearchANDDataArray[i]);						
									}
									else if(($scope.firstSearchANDDataArray[i].assetType != undefined
											&& $scope.firstSearchANDDataArray[i].assetType != null ? $scope.firstSearchANDDataArray[i].assetType
													: "").toLowerCase().includes($scope.secondSearchText.toLowerCase()))					
												$scope.assetChronoDataFilterList.push($scope.firstSearchANDDataArray[i]);
									
									}else if($scope.secondMultiSearchDropDown == "manufacturer"){
									if ($scope.secondSearchText	== "" || $scope.secondSearchText == undefined) {						
										$scope.assetChronoDataFilterList.push($scope.firstSearchANDDataArray[i]);						
									}
										else if(($scope.firstSearchANDDataArray[i].manufacturer != undefined
										&& $scope.firstSearchANDDataArray[i].manufacturer != null ? $scope.firstSearchANDDataArray[i].manufacturer
												: "").toLowerCase().includes($scope.secondSearchText.toLowerCase()))					
											$scope.assetChronoDataFilterList.push($scope.firstSearchANDDataArray[i]);
								
								}else if($scope.secondMultiSearchDropDown == "validityDate"){
										if ($scope.secondSearchText	== "" || $scope.secondSearchText == undefined) {						
											$scope.assetChronoDataFilterList.push($scope.firstSearchANDDataArray[i]);						
										}
										else if(($scope.firstSearchANDDataArray[i].validityDate != undefined
												&& $scope.firstSearchANDDataArray[i].validityDate != null ? $scope.firstSearchANDDataArray[i].validityDate: "").toLowerCase()
											.includes($scope.secondSearchText.toLowerCase()))					
										$scope.assetChronoDataFilterList.push($scope.firstSearchANDDataArray[i]);
							
									}else if($scope.secondMultiSearchDropDown == "assignedTo"){	
										if ($scope.secondSearchText	== "" || $scope.secondSearchText == undefined) {						
											$scope.assetChronoDataFilterList.push($scope.firstSearchANDDataArray[i]);						
										}
										else if(($scope.firstSearchANDDataArray[i].assignedPerson != undefined
												&& $scope.firstSearchANDDataArray[i].assignedPerson != null ? $scope.firstSearchANDDataArray[i].assignedPerson
														: "").toLowerCase()
														.includes(
																$scope.secondSearchText
																		.toLowerCase())											
												|| ($scope.firstSearchANDDataArray[i].deptName != undefined
														&& $scope.firstSearchANDDataArray[i].deptName != null ? $scope.firstSearchANDDataArray[i].deptName
														: "").toLowerCase()
														.includes(
																$scope.secondSearchText
																		.toLowerCase()))					
											$scope.assetChronoDataFilterList.push($scope.firstSearchANDDataArray[i]);	
																	
									
									}else if($scope.secondMultiSearchDropDown == "fromTime"){
										if ($scope.secondSearchText	== "" || $scope.secondSearchText == undefined) {						
											$scope.assetChronoDataFilterList.push($scope.firstSearchANDDataArray[i]);						
										}
										else if(($scope.firstSearchANDDataArray[i].fromTime != undefined
														&& $scope.firstSearchANDDataArray[i].fromTime != null ? $scope.firstSearchANDDataArray[i].fromTime
														: "")
														.toLowerCase()
														.includes(
																$scope.secondSearchText
																		.toLowerCase()))					
										$scope.assetChronoDataFilterList.push($scope.firstSearchANDDataArray[i]);
							
									}else if($scope.secondMultiSearchDropDown == "toTime"){
										if ($scope.secondSearchText	== "" || $scope.secondSearchText == undefined) {						
											$scope.assetChronoDataFilterList.push($scope.firstSearchANDDataArray[i]);						
										}
										else if(($scope.firstSearchANDDataArray[i].toTime != undefined
														&& $scope.firstSearchANDDataArray[i].toTime != null ? $scope.firstSearchANDDataArray[i].toTime
														: "")
												 		.toLowerCase()
														.includes(
																$scope.secondSearchText
																		.toLowerCase()))					
										$scope.assetChronoDataFilterList.push($scope.firstSearchANDDataArray[i]);
							
									}					
										
									
										
									}
								}

								$scope.totalItems = $scope.assetChronoDataFilterList.length;
								$scope.currentPage = 1;
								$scope.getTotalPages();
								$scope.getAssignedTagIds($scope.assetChronoDataFilterList);
								
								 if($scope.assetChronoData.length == 0 || $scope.assetChronoDataFilterList.length == 0 ){
										$scope.assetChronoNoData = true;

									}else{
										$scope.assetChronoNoData = false;

								if ($scope.assetChronoDataFilterList.length > $scope.numPerPage){
									$scope.assetChronoViewData = $scope.assetChronoDataFilterList
											.slice(0, $scope.numPerPage);
								$scope.PaginationTab=true;
								}
								else {
									$scope.assetChronoViewData = $scope.assetChronoDataFilterList;
									$scope.PaginationTab=false;
								}
							}	
								
							}else if($scope.varANDORDropDown == "OR"){
								
								if ($scope.firstMultiSearchDropDown == "All" && ($scope.firstSearchText == "" || $scope.firstSearchText == undefined)){
									$scope.disableSecondFilterText = true;
									$scope.disableSecondFilterDropDown = true;
									$scope.secondMultiSearchDropDown = "All"
									$scope.secondSearchText = ""
									
								}else{
									$scope.disableSecondFilterText = false;
									$scope.disableSecondFilterDropDown = false;
								}
								
								// $scope.assetChronoViewData.length = 0;
								// $scope.assetChronoDataFilterList.length = 0;
								$scope.assetChronoViewData = []
								$scope.assetChronoDataFilterList = [];
								$scope.secondSearchORDataArray = [];
								for (var i = 0; i < $scope.assetChronoData.length; i++) {
									if ($scope.secondMultiSearchDropDown == "All"){
										if ($scope.secondSearchText == "" || $scope.secondSearchText == undefined) {
											$scope.secondSearchORDataArray
													.push($scope.assetChronoData[i]);
											
											
										} else {
											if ((($scope.assetChronoData[i].assetId).toString() != undefined&& ($scope.assetChronoData[i].assetId).toString() != null ?
													($scope.assetChronoData[i].assetId).toString(): "").toLowerCase().includes($scope.secondSearchText.toLowerCase())
													|| ($scope.assetChronoData[i].assetType != undefined
															&& $scope.assetChronoData[i].assetType != null ? $scope.assetChronoData[i].assetType: "").toLowerCase()
															.includes($scope.secondSearchText.toLowerCase())
													|| ($scope.assetChronoData[i].subType != undefined
															&& $scope.assetChronoData[i].subType != null ? $scope.assetChronoData[i].subType
															: "")
															.toLowerCase()
															.includes(
																	$scope.secondSearchText
																			.toLowerCase())
													|| ($scope.assetChronoData[i].rangeValue != undefined
															&& $scope.assetChronoData[i].rangeValue != null ? $scope.assetChronoData[i].rangeValue
															: "")
															.toLowerCase()
															.includes(
																	$scope.secondSearchText
																			.toLowerCase())
													|| ($scope.assetChronoData[i].type != undefined
															&& $scope.assetChronoData[i].type != null ? $scope.assetChronoData[i].type
															: "")
															.toLowerCase()
															.includes(
																	$scope.secondSearchText
																			.toLowerCase())
													|| ($scope.assetChronoData[i].manufacturer != undefined
															&& $scope.assetChronoData[i].manufacturer != null ? $scope.assetChronoData[i].manufacturer
															: "")
															.toLowerCase()
															.includes(
																	$scope.secondSearchText
																			.toLowerCase())
													|| ($scope.assetChronoData[i].validityDate != undefined
															&& $scope.assetChronoData[i].validityDate != null ? $scope.assetChronoData[i].validityDate
															: "")
															.toLowerCase()
															.includes(
																	$scope.secondSearchText
																			.toLowerCase())
													|| ($scope.assetChronoData[i].assignedPerson != undefined
														&& $scope.assetChronoData[i].assignedPerson != null ? $scope.assetChronoData[i].assignedPerson: "")
														.toLowerCase().includes($scope.secondSearchText.toLowerCase())											
													|| ($scope.assetChronoData[i].deptName != undefined
														&& $scope.assetChronoData[i].deptName != null ? $scope.assetChronoData[i].deptName: "")
														.toLowerCase().includes($scope.secondSearchText.toLowerCase())
													|| ($scope.assetChronoData[i].fromTime != undefined
															&& $scope.assetChronoData[i].fromTime != null ? $scope.assetChronoData[i].fromTime
															: "")
															.toLowerCase()
															.includes(
																	$scope.secondSearchText
																			.toLowerCase())
													|| ($scope.assetChronoData[i].toTime != undefined
															&& $scope.assetChronoData[i].toTime != null ? $scope.assetChronoData[i].toTime
															: "")
													 		.toLowerCase()
															.includes(
																	$scope.secondSearchText
																			.toLowerCase())) {
												$scope.secondSearchORDataArray
														.push($scope.assetChronoData[i]);
												

											}
										}	
									}else{

										// to check other than all in second
										// search
										if($scope.secondMultiSearchDropDown == "assetId"){
											if ($scope.secondSearchText	== "" || $scope.secondSearchText == undefined) {						
												$scope.secondSearchORDataArray.push($scope.assetChronoData[i]);						
											}
											else if((($scope.assetChronoData[i].assetId).toString() != undefined&& ($scope.assetChronoData[i].assetId).toString() != null ?
													($scope.assetChronoData[i].assetId).toString(): "").toLowerCase().includes($scope.secondSearchText.toLowerCase()))					
												$scope.secondSearchORDataArray.push($scope.assetChronoData[i]);
											
										}else if($scope.secondMultiSearchDropDown == "assetType"){
											if ($scope.secondSearchText	== "" || $scope.secondSearchText == undefined) {						
												$scope.secondSearchORDataArray.push($scope.assetChronoData[i]);						
											}
											else if(($scope.assetChronoData[i].type != undefined
														&& $scope.assetChronoData[i].type != null ? $scope.assetChronoData[i].type: "").toLowerCase()
														.includes($scope.secondSearchText.toLowerCase()))					
													$scope.secondSearchORDataArray.push($scope.assetChronoData[i]);
											
										}else if($scope.secondMultiSearchDropDown == "subType"){
											if ($scope.secondSearchText	== "" || $scope.secondSearchText == undefined) {						
													$scope.secondSearchORDataArray.push($scope.assetChronoData[i]);						
											}
											else if(($scope.assetChronoData[i].subType != undefined
															&& $scope.assetChronoData[i].subType != null ? $scope.assetChronoData[i].subType: "").toLowerCase()
															.includes($scope.secondSearchText.toLowerCase()))					
														$scope.secondSearchORDataArray.push($scope.assetChronoData[i]);
											
										}else if($scope.secondMultiSearchDropDown == "range"){
											if ($scope.secondSearchText	== "" || $scope.secondSearchText == undefined) {						
												$scope.secondSearchORDataArray.push($scope.assetChronoData[i]);						
										}
											else if(($scope.assetChronoData[i].rangeValue != undefined
														&& $scope.assetChronoData[i].rangeValue != null ? $scope.assetChronoData[i].rangeValue
														: "")
														.toLowerCase()
														.includes(
																$scope.secondSearchText
																		.toLowerCase()))					
													$scope.secondSearchORDataArray.push($scope.assetChronoData[i]);
									}else if($scope.secondMultiSearchDropDown == "type"){
										if ($scope.secondSearchText	== "" || $scope.secondSearchText == undefined) {						
											$scope.secondSearchORDataArray.push($scope.assetChronoData[i]);						
									}
									else if(($scope.assetChronoData[i].assetType != undefined
											&& $scope.assetChronoData[i].assetType != null ? $scope.assetChronoData[i].assetType
													: "").toLowerCase().includes($scope.secondSearchText.toLowerCase()))					
												$scope.secondSearchORDataArray.push($scope.assetChronoData[i]);
									
									}else if($scope.secondMultiSearchDropDown == "manufacturer"){
									if ($scope.secondSearchText	== "" || $scope.secondSearchText == undefined) {						
										$scope.secondSearchORDataArray.push($scope.assetChronoData[i]);						
									}
										else if(($scope.assetChronoData[i].manufacturer != undefined
										&& $scope.assetChronoData[i].manufacturer != null ? $scope.assetChronoData[i].manufacturer
												: "").toLowerCase().includes($scope.secondSearchText.toLowerCase()))					
											$scope.secondSearchORDataArray.push($scope.assetChronoData[i]);
								
								}else if($scope.secondMultiSearchDropDown == "validityDate"){
										if ($scope.secondSearchText	== "" || $scope.secondSearchText == undefined) {						
											$scope.secondSearchORDataArray.push($scope.assetChronoData[i]);						
										}
										else if(($scope.assetChronoData[i].validityDate != undefined
												&& $scope.assetChronoData[i].validityDate != null ? $scope.assetChronoData[i].validityDate: "").toLowerCase()
											.includes($scope.secondSearchText.toLowerCase()))					
										$scope.secondSearchORDataArray.push($scope.assetChronoData[i]);
							
									}else if($scope.secondMultiSearchDropDown == "assignedTo"){	
										if ($scope.secondSearchText	== "" || $scope.secondSearchText == undefined) {						
											$scope.secondSearchORDataArray.push($scope.assetChronoData[i]);							
										}
										else if(($scope.assetChronoData[i].assignedPerson != undefined
												&& $scope.assetChronoData[i].assignedPerson != null ? $scope.assetChronoData[i].assignedPerson
														: "").toLowerCase()
														.includes(
																$scope.secondSearchText
																		.toLowerCase())											
												|| ($scope.assetChronoData[i].deptName != undefined
														&& $scope.assetChronoData[i].deptName != null ? $scope.assetChronoData[i].deptName
														: "").toLowerCase()
														.includes(
																$scope.secondSearchText
																		.toLowerCase()))					
											$scope.secondSearchORDataArray.push($scope.assetChronoData[i]);	
																	
									
									}else if($scope.secondMultiSearchDropDown == "fromTime"){
										if ($scope.secondSearchText	== "" || $scope.secondSearchText == undefined) {						
											$scope.secondSearchORDataArray.push($scope.assetChronoData[i]);						
										}
										else if(($scope.assetChronoData[i].fromTime != undefined
														&& $scope.assetChronoData[i].fromTime != null ? $scope.assetChronoData[i].fromTime
														: "")
														.toLowerCase()
														.includes(
																$scope.secondSearchText
																		.toLowerCase()))					
										$scope.secondSearchORDataArray.push($scope.assetChronoData[i]);
							
									}else if($scope.secondMultiSearchDropDown == "toTime"){
										if ($scope.secondSearchText	== "" || $scope.secondSearchText == undefined) {						
											$scope.secondSearchORDataArray.push($scope.assetChronoData[i]);						
										}
										else if(($scope.assetChronoData[i].toTime != undefined
														&& $scope.assetChronoData[i].toTime != null ? $scope.assetChronoData[i].toTime
														: "")
												 		.toLowerCase()
														.includes(
																$scope.secondSearchText
																		.toLowerCase()))					
										$scope.secondSearchORDataArray.push($scope.assetChronoData[i]);
							
									}					
									
										
									}
								}
								// to concat first and second search arrays
								for (var i = 0; i <  $scope.firstSearchANDDataArray.length; i++) {
									if (!$scope.secondSearchORDataArray.includes( $scope.firstSearchANDDataArray[i]))
										$scope.secondSearchORDataArray.push( $scope.firstSearchANDDataArray[i]);
								}							
								$scope.assetChronoDataFilterList = $scope.secondSearchORDataArray;
								// $scope.assetChronoDataFilterList =
								// ($scope.firstSearchANDDataArray).concat($scope.secondSearchORDataArray);
								
								// for pagination
								$scope.totalItems = $scope.assetChronoDataFilterList.length;
								$scope.currentPage = 1;
								$scope.getTotalPages();
								$scope.getAssignedTagIds($scope.assetChronoDataFilterList);
								
								 if($scope.assetChronoData.length == 0 || $scope.assetChronoDataFilterList.length == 0 ){
										$scope.assetChronoNoData = true;

									}else{
										$scope.assetChronoNoData = false;

								if ($scope.assetChronoDataFilterList.length > $scope.numPerPage){								
									 $scope.assetChronoViewData = JSON.parse(JSON.stringify($scope.assetChronoDataFilterList.slice(0, $scope.numPerPage))); 
								$scope.PaginationTab=true;
								}
								else {
									$scope.assetChronoViewData = JSON.parse(JSON.stringify($scope.assetChronoDataFilterList)); 
									$scope.PaginationTab=false;
								}
							}	
							}
								
							
						}
						
						
						function getAllKeys(keys, obj, path) {
							for (key in obj) {
								var currpath = path + '/' + key;

								if (typeof (obj[key]) == 'object'
										|| (obj[key] instanceof Array))
									getAllKeys(keys, obj[key], currpath);
								else
									keys.push({
										'key' : key,
										"path" : currpath,
										"value" : obj[key]
									});
							}
						}
						
						//EXPORT code
						$scope.exportDataFn = function(){
							$scope.exportArrayData = [];
							$scope.exportArrayDataForSort = [];
						//console.log($scope.assetChronoData);
						//$scope.exportArrayDataForSort = $scope.assetChronoData;
						$scope.exportArrayDataForSort = $scope.assetChronoData.slice();
						$scope.exportArrayDataForSort.sort($rootScope.GetSortOrderByDate("validityDate", true));
						
						for(var i=0; i<$scope.exportArrayDataForSort.length; i++){
							$scope.exportArrayData.push({					
								'S.No':i+1,
								'Asset ID':$scope.exportArrayDataForSort[i].assetId,
								'Type': $scope.exportArrayDataForSort[i].type,
								'Sub Type':$scope.exportArrayDataForSort[i].subType,
								'Range':$scope.exportArrayDataForSort[i].rangeValue,
								'Asset Type':$scope.exportArrayDataForSort[i].assetType,
								'Manufacturer':$scope.exportArrayDataForSort[i].manufacturer,
								'Expiry Date':$scope.exportArrayDataForSort[i].validityDate,
								'Assigned To':$scope.exportArrayDataForSort[i].assignedPerson,
								'From Time':$scope.exportArrayDataForSort[i].fromTime,
								'To Time':$scope.exportArrayDataForSort[i].toTime
								
							});	
						}
						
						
						}
						//EXPORT code
						
						$scope.exportExcelData=function(){
							
							if (!$rootScope.checkNetconnection()) { 
								$rootScope.alertDialogChecknet();
								return;
								}
						   	 else{
						   		$rootScope.exportExcelDataFlag = true;
						   		var filename  = "Asset_Chrono_Details";
						   		$rootScope.exportXLSPDFFileName(filename);
							
							var headerArray=[];
							var detailarray=[];
							var descarray=[];
							
							 detailarray=['Report Generated by','Report Generated Date & Time','Report'];
							descarray=[$scope.user.userName, $rootScope.exportDate+ ", "+$rootScope.exportTime1,"Asset Chrono Detail"];
							
							for(var i=0; i<detailarray.length; i++){
								headerArray.push( {
									'Details': detailarray[i],				
									'Description':descarray[i] ,				
								});		    
							}
							
							$scope.exportDataFn();
							
							//var export_file_name = "SKU_DETAIL_"	+ "_"+ exportDate + "_"	+ exportTime1 + ".xlsx";
							
							$rootScope.exportAsExcelFn($scope.exportArrayData,headerArray);
							
							
							}
						
						}
						
						
						$scope.exportPDFData=function(){
							if (!$rootScope.checkNetconnection()) { 
								$rootScope.alertDialogChecknet();
								return;
								}
						   	 else{
							function getTarget(e) {
								  return e.target || e.srcElement;			  
								}
							var filename  = "Asset_Chrono_Detail";
							$rootScope.exportXLSPDFFileName(filename);
							
							$scope.exportDataFn();
							var columns= [], keyNames =[];
							getAllKeys(keyNames,$scope.exportArrayData, '');
							for (var i = 0; i < keyNames.length; i++) {
								if (!columns.includes(keyNames[i]['key']))
									columns.push(keyNames[i]['key']);
								
								}
							
							var rowsToDisplay =[]; var rows =[];
							
							for (var i = 0; i < $scope.exportArrayData.length; i++) {
								rowsToDisplay =[ $scope.exportArrayData[i]["S.No"],$scope.exportArrayData[i]["Asset ID"],
									$scope.exportArrayData[i]["Type"],$scope.exportArrayData[i]["Sub Type"],
									$scope.exportArrayData[i]["Range"],$scope.exportArrayData[i]["Asset Type"],
									$scope.exportArrayData[i]["Manufacturer"],$scope.exportArrayData[i]["Expiry Date"],
										$scope.exportArrayData[i]["Assigned To"],$scope.exportArrayData[i]["From Time"],
										$scope.exportArrayData[i]["To Time"]];
								 rows.push(rowsToDisplay);
							}
							
							$rootScope.exportPDFfn(columns,rows, 7); //7-Expiry Date Index
								
						   	 }	

					}

				});
