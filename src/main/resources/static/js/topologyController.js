app
		.controller(
				'topologyController',
				function($scope, $http, $rootScope, $window, $timeout,
						$location, $filter) {
					$scope.user = JSON.parse($window.sessionStorage
							.getItem("user"));
					if ($scope.user == null) {
						$location.path('/login');
						return;
					}
					$('#map').css('height', ($(window).height() - 135));

					/*
					 * $scope.user = JSON.parse($window.sessionStorage
					 * .getItem("user")); if ($scope.user != null) {
					 * $rootScope.$broadcast('updatemenu', $scope.user.role); if
					 * ($scope.user.capabilityDetails.ZOM.ZCL == 1) {
					 * $("#zomZC").show(); } else { $("#zomZC").hide(); } if
					 * ($scope.user.capabilityDetails.ZOM.ZAP == 1) {
					 * $("#zomAP").show(); } else { $("#zomAP").hide(); } if
					 * ($scope.user.capabilityDetails.ZOM.ZZN == 1) {
					 * $("#zomZON").show(); } else { $("#zomZON").hide(); } }
					 * else { $location.path('/home'); }
					 * 
					 * if ($scope.user.role == "admin") { $scope.serviceprovider =
					 * $window.sessionStorage .getItem("serviceProvider"); }
					 * else { $scope.serviceprovider =
					 * $scope.user.serviceProvider; } if
					 * ($rootScope.checkNetconnection() == false) {
					 * $rootScope.hideloading('#loadingBar'); return; }
					 * 
					 * $scope.org = $window.sessionStorage.getItem("org");
					 * $scope.$on('updateorg', function(event, data) {
					 * 
					 * $scope.org = data; map.closePopup(); });
					 */

					var maxZoomValue = 18;
					var minZoomValue = 0;
					var defZoomValue = 8;

					$scope.success = false;
					$scope.error = false;
					$scope.loading = false;
					$scope.isOutlineView = true;
					$scope.isOutlineDataView = true;
					$scope.isWallEdit = false;
					$scope.isPartitionEdit = false; 
					$scope.drawLine = false;
					$scope.drawaRectangle = false;
					$scope.drawPolygon = false;
					
					$scope.outlineList = [];
					$scope.doorsList = [];
					$scope.wallList = [];
					$scope.partitionList = [];
					$scope.pathwayList=[];

					$scope.doorWallType = 'Normal';
					$scope.wallWallType = 'Normal'; 
					$scope.partitionWallType = 'Normal';
					
					
					$("#campusDetailsDiv").show();
					$("#selectedId").hide();
					$("#selectedDoor").hide();
					$("#selectedWall").hide();
					$("#selectedPartition").hide(); 								
					$("#selectedPathway").hide();
					
					
					 $scope.ext ="";
					 $scope.jsonContent = new Array();
					  $scope.alert_success_msg="";
					  $scope.alert_error_msg="";
					  $scope.error= false;
					

					
					// $('#myModal').modal('show');
					$scope.selectedMenu = 'All';
					$scope.selectedMode = 'All';
					document.getElementById("idCkAllMenu").checked =true ;
					// $scope.$on('topologyMenu', function(event, data) {
					
					$scope.topologyMenu = function() {
						// $scope.selectedMenu = data;
						$scope.drawControl.remove();
						$scope.drawControlEdit.addTo(map);
						$scope.drawLine = false;
						$scope.drawaRectangle = false;
						$scope.drawPolygon = false;
						$scope.selectedMode = $scope.selectedMenu;

						if ($scope.selectedMenu === "All") {
							$scope.drawLine = false;
							$scope.drawaRectangle = false;
							$scope.drawPolygon = false;
						} else if ($scope.selectedMenu === "Building") {
							$scope.drawLine = false;
							$scope.drawaRectangle = true;
							$scope.drawPolygon = true;
						} else if ($scope.selectedMenu === "Pathway") {
							$scope.drawLine = false;
							$scope.drawaRectangle = true;
							$scope.drawPolygon = true;
						} else if ($scope.selectedMenu === "Door") {
							$scope.drawLine = true;
							$scope.drawaRectangle = false;
							$scope.drawPolygon = false;
						} else if ($scope.selectedMenu === "Partition") {
							$scope.drawLine = true;
							$scope.drawaRectangle = true;
							$scope.drawPolygon = true;
						} else if ($scope.selectedMenu === "Wall") {
							$scope.drawLine = true;
							$scope.drawaRectangle = true;
							$scope.drawPolygon = true;
						} else if ($scope.selectedMenu === "Export") {
						console.log("Topology Export");
						} else if (data === "Import") {
							console.log("Topology Import");	
						}
						$scope.addmapDrawOptions();
						
						map.eachLayer(function(layer) {
							if (layer.editor !== undefined) {
								layer.editor.editLayer
										.clearLayers()
							} 
						});
						
						$scope.isEdit  = false;
						$scope.isDoorEdit = false;
						$scope.isWallEdit = false;
						$scope.isPartitionEdit = false;
						$scope.isPathwayEdit = false;
						
						$("#campusDetailsDiv").hide();
						$("#selectedId").hide();
						$("#selectedDoor").hide();
						$("#selectedWall").hide();
						$("#selectedPartition").hide(); 								
						$("#selectedPathway").hide();
						$("#idExportMapDetail").hide();
						$("#idImportMapDetail").hide();
						$("#idImportZoneDetail").hide();
						

					// });
					}
					
					
					var map = L.map('map', {
						editable : true,
						maxZoom : maxZoomValue,
						minZoom : minZoomValue,
						zoom : defZoomValue,
						crs : L.CRS.Simple,
						zoomSnap : 0.01,
						zoomDelta: 0.25,
						scrollWheelZoom: false,  
						smoothWheelZoom: true,   
						smoothSensitivity: 1   // zoom speed. default is 1
					}).setView([ 0, 0 ], 14);

					var ZoomViewer = L.Control
							.extend({
								options: {
								  position: 'topleft' 
								},
								onAdd : function() {

									var container = L.DomUtil.create('div');
									var gauge = L.DomUtil.create('div');
									container.style.width = '100px';
									container.style.background = 'rgba(255,255,255,0.5)';
									container.style.textAlign = 'center';
									map.on('zoomstart zoom zoomend', function(
											ev) {
										gauge.innerHTML = 'Zoom level: '
												+((map.getZoom()-minZoomValue)+1).toFixed(2);
									})
									container.appendChild(gauge);

									return container;
								}
							});

					(new ZoomViewer).addTo(map);
					map.zoomControl.setPosition('topleft');
					L.control.bigImage({position: 'topright', downloadTitle:'Save', 
						printControlTitle:'Save Map As Image', printControlLabel:'&#128190'}).addTo(map);

					window.addEventListener('resize', function(event) {
						// get the width of the screen after the resize event
						var width = document.documentElement.clientWidth;
						// tablets are between 768 and 922 pixels wide
						// phones are less than 768 pixels wide
						if (width < 768) {
							// set the zoom level to 10
							// map.setZoom(10);
						} else {
							// set the zoom level to 8
							// map.setZoom(8);
						}
					});

					
					$scope.wallTypes = [ "Normal", "Thin", "Thick" ];
					$scope.wallType = "Normal";
					$scope.placeTypes = [ "Building", "Parking", "Park",
							"Greenspace", "Openyard" ];
					$scope.pathwayTypes = [ "All", "People", "Vehicle" ];
					
					$scope.pathwayType = "All";
					$scope.placeType = "Building";
					$scope.outlinesArea = 0;
					$scope.campusId = 1;
					$scope.treeData = [];
					$scope.loadTreeView = function() {

						var options = {
							bootstrap2 : false,
							showTags : true,
							levels : 5,
							data : $scope.treeData,
							onNodeExpanded : nodeExpand,
							onNodeCollapsed : nodeCollapse,
							onNodeSelected : nodeSelect,
							onNodeUnselected : nodeUnselect
						};

						$('#treeview').treeview(options);
					};
					var expandedNodes = [];
					var tree = [];
					function nodeExpand(event, data) {
						expandedNodes.push(data.nodeId);
						var requestObject = []
						requestObject.push(data.text);

						var parent, dummy = data;
						while ((parent = $('#treeview').treeview('getParent',
								dummy.nodeId))["nodeId"] != undefined) {
							requestObject.push(parent.text);
							dummy = parent;
						}

					}
					function nodeCollapse(event, data) {
						var index = expandedNodes.indexOf(data.nodeId);
						if (index > -1)
							expandedNodes.splice(index, 1);
					}

					function nodeSelect(event, data) {
						if (data.state.expanded == true)
							$('#treeview').treeview('collapseNode', data.nodeId);
						else
							$('#treeview').treeview('expandNode', data.nodeId);
						 
						if (data.nodeId == 0) {
							$("#campusDetailsDiv").show();
							$("#selectedId").hide(); 
							$("#selectedDoor").hide();
							$("#selectedWall").hide();
							$("#selectedPartition").hide(); 
						    $("#selectedPathway").hide();
							$("#idExportMapDetail").hide();
							$("#idImportMapDetail").hide();
							$("#idImportZoneDetail").hide();

						} else {
							$("#campusDetailsDiv").hide();
							$("#selectedId").show();
							$("#selectedDoor").hide();
							$("#selectedWall").hide();
							$("#selectedPartition").hide(); 
							$("#selectedPathway").hide();
							$("#idExportMapDetail").hide();
							$("#idImportMapDetail").hide();
							$("#idImportZoneDetail").hide();
							$scope.selectOutline(data.nodeId - 1)
						}
					}

					function nodeUnselect(event, data) {
					}

					var command = L.control({
						position : 'topright'
					});

					command.onAdd = function(map) {
						var div = L.DomUtil.create('div', 'command');

						div.innerHTML = '<form>Grid (1X1M) <input id="command" type="checkbox" checked/></form>';
						return div;
					};

					command.addTo(map);
					
					// add the event handler
					function handleCommand() {
						// alert("Clicked, checked = " + this.checked);
						$scope.isCheckedGrid = this.checked;
						if (this.checked == true) {
							$scope.grid.onAdd(map)
						} else {
							$scope.grid.onRemove(map);
						}
					} 
					 
					document.getElementById("command").addEventListener(
							"click", handleCommand, false);

					/*
					 * $scope.showApCheckbox = L.control({position:
					 * 'topright'});
					 * 
					 * $scope.showApCheckbox.onAdd = function (map) { var div =
					 * L.DomUtil.create('div', 'command'); div.innerHTML = '<form ><span
					 * id="labelid">Show Aps</span><input id="showApCheckbox"
					 * type="checkbox" checked/></form>'; } else{ div.innerHTML = '<form>Show
					 * APs <input id="showApCheckbox" type="checkbox" checked/></form>'; }
					 * 
					 * return div; }; $scope.showApCheckbox.addTo(map);
					 * 
					 * 
					 * if($scope.serviceprovider=="quuppa"){
					 * $("#labelid").text("Show Locators"); }
					 */
					$scope.campusGridArray = {};
					function loadMap() {

						var height = $scope.mapHeight;
						var width = $scope.mapWidth;
						$scope.editableLayers = new L.FeatureGroup();
						map.addLayer($scope.editableLayers);
						
						$scope.campusGridArray = {};

						for (var i = 0; i < height; i++) {

							for (var j = 0; j < width; j++) {

								var campusArray = [];
								campusArray.push({
									"x" : i,
									"y" : j
								});
								campusArray.push({
									"x" : i,
									"y" : (j + 1)
								});
								campusArray.push({
									"x" : (i + 1),
									"y" : (j + 1)
								});
								campusArray.push({
									"x" : (i + 1),
									"y" : j
								});

								$scope.campusGridArray[i + '_' + j] = campusArray;
							}

						}

						console.log($scope.campusGridArray);

						map.setZoom(defZoomValue);
						var southWest = map.unproject([ 0, height ],
								defZoomValue);
						var northEast = map.unproject([ width, 0 ],
								defZoomValue);
						map.setMaxBounds(new L.LatLngBounds(southWest,
								northEast));

						map.fitBounds(new L.LatLngBounds(southWest, northEast));

						if ($scope.campusRect != undefined
								|| $scope.campusRect != null) {
							$scope.campusRect.onRemove(map);
						}

						var bounds1 = [
								[ pixelsToLatLng(parseFloat(0), parseFloat(0)) ],
								[ pixelsToLatLng(parseFloat(0),
										parseFloat(height)) ],
								[ pixelsToLatLng(parseFloat(width),
										parseFloat(height)) ],
								[ pixelsToLatLng(parseFloat(width),
										parseFloat(0)) ] ];

						$scope.campusRect = L.rectangle(bounds1, {
							color : $scope.mapColor,
							weight : 2,
							fillColor : $scope.mapColor,
							fillOpacity : 0.8
						});
						$scope.campusRect.addTo(map);
						$scope.campusRect.addTo($scope.editableLayers);

						if ($scope.marker != undefined || $scope.marker != null) {
							$scope.marker.onRemove(map);
						}

						$scope.marker = new L.Marker(
								pixelsToLatLng(0, 0),
								{
									icon : new L.DivIcon(
											{
												className : 'my-div-icon',
												html : '<div> <img class="staringImage" src="./assets/img/red_circle.png"/> '
											})
								});
						$scope.marker.addTo(map);

						$scope.mapDetails = {};
						$scope.mapDetails.width_m = width;
						$scope.mapDetails.height_m = height;
						$scope.mapDetails.height = height;
						$scope.mapDetails.width = width;
						$scope.mapDetails.mapType = 'custom';

						if ($scope.grid != undefined || $scope.grid != null) {
							$scope.grid.onRemove(map);
						}

						if ($scope.marker1 != undefined
								|| $scope.marker1 != null) {
							$scope.marker1.onRemove(map);
						}

						$scope.grid = L.grid(
								{
									options : {
										position : 'topright',
										bounds : new L.LatLngBounds(southWest,
												northEast),
										mapDetails : $scope.mapDetails,
										defZoomValue : defZoomValue,
										gridSize : 1
									}
								}).addTo(map);

						$scope.marker1 = new L.Marker(
								pixelsToLatLng(-35, -5),
								{
									icon : new L.DivIcon(
											{
												className : 'my-div-icon',
												html : '<span class="staringlabel">(0,0)</span>'
											})
								});
						$scope.marker1.addTo(map);

						$timeout(function() {
							map.setView([ $scope.campusRect.getCenter().lat,
									$scope.campusRect.getCenter().lng ], map
									.getZoom());

							getAllOutlinedetails();

						}, 500);
					}

					// loadMap();

					function isNumberKey(evt) {
						var charCode = (evt.which) ? evt.which : evt.keyCode;
						if (charCode != 46 && charCode > 31
								&& (charCode < 48 || charCode > 57))
							return false;

						return true;
					}

					function updateCampusDetails() {
						$http({
							method : 'PUT',
							url : './campusDetails',
							data : {
								"campusId" : $scope.campusId,
								"colorCode" : $scope.mapColor,
								"height" : $scope.mapHeight,
								"width" : $scope.mapWidth,
								"name" : $scope.campusName
							}
						})
								.then(
										function success(response) {
											$rootScope
													.hideloading('#loadingBar');
											if (response != null
													&& response.data != null
													&& response.data != "BAD_REQUEST") {

												$timeout(function() {
													loadMap();
												}, 500);
											}
										},
										function error(response) {
											$rootScope.hideloading('#loadingBar');
											$rootScope.fnHttpError(response);
										});
					}
					 
					function getWallWeight(val){
						if(val=='Normal'){
							return 3;
						}else if(val=='Thin'){
							return 2; 
						}else if(val=='Thick'){
							return 4;  
						}else 
							return 3;
					}
					
					function getCampusDetails() {

						$http({
							method : 'GET',
							url : './campusDetails'
						})
								.then(
										function success(response) {
											$rootScope
													.hideloading('#loadingBar');
											if (response != null
													&& response.data != null
													&& response.data != "BAD_REQUEST") {
												$scope.campusDetails = response.data.data;

												$scope.mapHeight = $scope.campusDetails.height;
												$scope.mapWidth = $scope.campusDetails.width;
												$scope.map_height = $scope.campusDetails.height;
												$scope.map_width = $scope.campusDetails.width;
												$scope.mapColor = $scope.campusDetails.colorCode;
												$scope.campusId = $scope.campusDetails.campusId;
												$scope.campusName = $scope.campusDetails.name;

												$scope.treeData = [ {
													text : "Campus"
												} ];

												$("#campusDetailsDiv").show();
												$("#selectedId").hide();
												$("#selectedDoor").hide();
												$("#idExportMapDetail").hide();
												$("#idImportMapDetail").hide();
												$("#idImportZoneDetail").hide();
												$scope.loadTreeView();

												$timeout(function() {
													loadMap();
												}, 500);
											}
										},
										function error(response) {
											$rootScope.hideloading('#loadingBar');
											$rootScope.fnHttpError(response);
										});
					}

					getCampusDetails();

					function latLngToPixels(latlng) {
						var pixelArr = [];
						for (var i = 0; i < latlng[0].length; i++) {

							var latlngArr = latlng[0][i];

							var pt = map.project(
									[ latlngArr.lat, latlngArr.lng ],
									defZoomValue);

							pixelArr.push({
								"x" : (parseFloat(pt.x)).toFixed(2),
								"y" : (parseFloat(pt.y)).toFixed(2),
							});

						}
						return pixelArr;
					}

					function latLngToPixels1(latlng) {
						var pixelArr = [];
						for (var i = 0; i < latlng.length; i++) {

							var latlngArr = latlng[i];

							var pt = map.project(
									[ latlngArr.lat, latlngArr.lng ],
									defZoomValue);

							pixelArr.push({
								"x" : (parseFloat(pt.x)).toFixed(2),
								"y" : (parseFloat(pt.y)).toFixed(2),
							});

						}
						return pixelArr;
					}

					function roundVal(latlng) {
						var pixelArr = [];
						for (var i = 0; i < latlng.length; i++) {

							var pt = latlng[i];

							pixelArr.push({
								"x" : (parseFloat(pt.x)).toFixed(),
								"y" : (parseFloat(pt.y)).toFixed(),
							});

						}
						return pixelArr;
					}

					function editlatLngToPixels(latlng) {
						var pixelArr = [];
						for (var i = 0; i < latlng[0].length; i++) {

							var latlngArr = latlng[0][i];
							var pt = pt = map.project([ latlngArr.lat,
									latlngArr.lng ], defZoomValue);
 
							pixelArr.push({
								"x" : pt.x.toFixed(),
								"y" : pt.y.toFixed(),
							});

						}
						return pixelArr;
					}

					function pixelsToLatLng(x, y) {
						return map.unproject([ x, y ], defZoomValue);
					}

					$scope.getQuuppaAccessPoints = function() {
						if ($scope.isCheckedShowAp || $scope.isAPView) {
							for (var i = 0; i < $scope.apList.length; i++) {
								$scope.addLocatorAPViews(i);
							}
						}
					}

					var selectedRowCSS = {
						'background' : '#cccccc',
						'color' : '#000000',
						'font-weight' : 'bold'
					};
					var unSelectedRowCSS = {
						'background' : '#ffffff',
						'color' : '#000000',
						'font-weight' : 'normal'
					};

					$scope.ppm = "";

					function isInsidePolygon(point, vs) {
						// ray-casting algorithm based on
						// http://www.ecse.rpi.edu/Homepages/wrf/Research/Short_Notes/pnpoly.html

						var x = point[0], y = point[1];

						var inside = false;
						for (var i = 0, j = vs.length - 1; i < vs.length; j = i++) {
							var xi = vs[i][0], yi = vs[i][1];
							var xj = vs[j][0], yj = vs[j][1];

							var intersect = ((yi > y) != (yj > y))
									&& (x < (xj - xi) * (y - yi) / (yj - yi)
											+ xi);
							if (intersect)
								inside = !inside;
						}

						return inside;
					}
					 

					function checkDoorOverlap(outlinesId, points) {
						var array = [];
 
						var isDoorOverLap = false; 
						 
						for (var i = 0; i < $scope.outlineList.length; i++) {
							var id = $scope.outlineList[i].outlinesId;
							if (id != '' && id == outlinesId) {
								var vertices = $scope.outlineList[i].coordinateList;
								var bounds1 = pointsToArray(vertices);
								  
								for (var k = 0; k < points.length; k++) { 
									var pt = [points[k].x, points[k].y];
									
									var isInside = isInsidePolygon(pt, bounds1);
									if(isInside){
										isDoorOverLap = true;
									}else
									{
										isDoorOverLap = false;
										break;
									}
								}
								
								 
								} 
						}
						 
						return isDoorOverLap;
					}
					
					function checkOutlineOverlap(outlinesId, points) {

						var array =  pointsToArray(points);
 
						var poly1 = turf.polygon([ array ]);

						for (var i = 0; i < $scope.outlineList.length; i++) {
							var id = $scope.outlineList[i].outlinesId;
							if (id != '' && id != outlinesId) {
								var vertices = $scope.outlineList[i].coordinateList;
								var bounds1 = pointsToArray(vertices); 

								var poly2 = turf.polygon([ bounds1 ]);
								if (turf.booleanOverlap(poly1, poly2)
										|| turf.booleanContains(poly1, poly2)
										|| turf.booleanContains(poly2, poly1)) {
									return false;
								}
							}

						}
						return true;
					}
					
					
					function checkWallInsideBuilding(outlinesId, points) {

						var array = pointsToArray(points);  

						var poly1 = turf.polygon([ array ]);

						for (var i = 0; i < $scope.outlineList.length; i++) {
							var id = $scope.outlineList[i].outlinesId;
							if (id != '' && id == outlinesId) {
								var vertices = $scope.outlineList[i].coordinateList;
								var bounds1 = pointsToArray(vertices); 

								var poly2 = turf.polygon([ bounds1 ]);
								if (turf.booleanContains(poly1, poly2)
										|| turf.booleanContains(poly2, poly1)) {
									return true;
								}
							}

						}
						return false;
					}

					
					function getBoundsArray(points) {
						var bounds = [];
						for (var i = 0; i < points.length; i++) {

							bounds.push(pixelsToLatLng(
											parseFloat(points[i].x),
											parseFloat(points[i].y)));

						}
						return bounds; 
					}
					
					function isInsideCampus(points)
					{
						
						for (var i = 0; i < points.length; i++) {
							var point = points[i];
							if (!isInsideRect(0, 0,
									$scope.mapWidth,
									$scope.mapHeight,
									point.x, point.y)) {
								return false; 
							}
						}
						
						return true;
					}
					
					function isInsideRect(x1, y1, x2, y2, x, y) {
						if (x > x1 && x < x2 && y > y1 && y < y2)
							return true;

						return false;
					}
					
					function pointsToArray(points) {
						var array = [];

						for (var k = 0; k < points.length; k++) {
							array.push([ parseFloat(points[k].x),
								parseFloat(points[k].y) ]);
						}
						array.push([ parseFloat(points[0].x),
							parseFloat(points[0].y) ]);

						return array;
					}

					$scope.show_alert_error = false;
					$scope.saveCampusDetails = function() {
						var width = $('.Width').val();
						var height = $('.Height').val();
						var cname = $('.campusName').val().trim();

						if (cname == "") {

							$scope.error = true;
							$scope.alert_error_msg = 'Enter Campus Name';
							$timeout(function() {
								$scope.error = false;
							}, 1500);

							return false;
						}
						if (!width) {

							$scope.error = true;
							$scope.alert_error_msg = 'Enter Campus Width';
							$timeout(function() {
								$scope.error = false;
							}, 1500);

							return false;
						}else if (parseFloat(width)<$scope.mapWidth){
							$scope.error = true;
							$scope.alert_error_msg = 'Campus Width must be greater than previous Width ('+$scope.mapWidth+')';
							$timeout(function() {
								$scope.error = false;
							}, 1500);

							return false;
						}
						
						if (!height) {

							$scope.error = true;
							$scope.alert_error_msg = 'Enter Campus Height';
							$timeout(function() {
								$scope.error = false;
							}, 1500);

							return false;
						}else if (parseFloat(height)<$scope.mapHeight){
							$scope.error = true;
							$scope.alert_error_msg = 'Campus Height must be greater than previous Height ('+$scope.mapHeight+')';
							$timeout(function() {
								$scope.error = false;
							}, 1500);

							return false;
						}

						
						$scope.mapHeight = parseFloat(height);
						$scope.mapWidth = parseFloat(width);
						$scope.map_height = $scope.mapHeight;
						$scope.map_width = $scope.mapWidth;
						
						updateCampusDetails();

						// $('#myModal').modal('hide');
					}

					function reload() {
						
						map.eachLayer(function(layer) {
							if (layer.menuType !== undefined) {
								layer.remove();  
							}
						});
						
						getAllOutlinedetails();
					}

					$scope.refreshBtn = L.control({
						position : 'topright'
					});
					$scope.refreshBtn.onAdd = function(map) {

						this._div = L.DomUtil.create('div', 'mainMenu');
						this._div.innerHTML += ' <img class="reloadImage" id="refreshBtn" src="./assets/img/reload.jpg"/>';
						return this._div;
					};

					$scope.refreshBtn.addTo(map);
					document.getElementById("refreshBtn").addEventListener(
							"click", reload, false);
					$scope.addmapDrawOptions = function() {
						var options = {
							position : 'topright',
							draw : {
								polygon : $scope.drawPolygon,
								polyline : $scope.drawLine,
								circlemarker : false,
								circle : false,
								marker : false,
								rectangle : $scope.drawaRectangle
							},
							edit : false

						};
						$scope.drawControl = new L.Control.Draw(options);
						$scope.drawControlEdit = new L.Control.Draw({
							draw : false
						});
						map.addControl($scope.drawControl);
					}
					$scope.addmapDrawOptions();
					
					function afterRender(result) {
					      return result;
					    }

					    function afterExport(result) {
					      return result;
					    }
 

					// $rootScope.$broadcast('selectTopologyMenu',
					// $scope.selectedMenu);
					
					map
							.on(
									L.Draw.Event.CREATED,
									function(e) {

										var type = e.layerType, layer = e.layer;

										var isInsideMap = true;
										var isOutlineOverlap = true;
										
										if ($scope.selectedMenu == "Door") {

											var points = latLngToPixels1(layer.getLatLngs()); 
											isInsideMap = isInsideCampus(points);
											 
											if (isInsideMap) {
												
											   isOutlineOverlap =
													  checkDoorOverlap(
														 $scope.outlinesId, points);  
												 
											}

											if (isInsideMap && isOutlineOverlap) {
 
												$("#campusDetailsDiv").hide();
												$("#selectedId").hide();
												$("#selectedDoor").show();
												$("#selectedWall").hide();
												$("#selectedPartition").hide(); 
												$("#selectedPathway").hide(); 
												$("#idExportMapDetail").hide();
												$("#idImportMapDetail").hide();
												$("#idImportZoneDetail").hide();
												layer.outlinesId = $scope.outlinesId; 
												
												
												$scope.doorColorCode = '#3388ff'; 
												$scope.doorName = "Door ";
												$scope.doorVertices = points;
												$scope.doorId = '';
												$scope.doorWallType = 'Normal'; 
												
												
												$("#doorEditBtn")
												.text(
														'Save');
												$scope.isDoorEdit = true;
												$("#btnDoorDelete").hide();
										
												$scope.editableLayers
												.addLayer(layer); 
												
												$scope.$apply();

												layer.outlinesId = $scope.outlinesId;
												layer.menuType = "Door";
												layer.doorId = "";
												layer.doorName = $scope.doorName; 
												layer.doorColorCode = $scope.doorColorCode; 
												layer.geometryType = "Line"; 
												layer.coordinateList =points;
												layer.doorWallType = $scope.doorWallType; 
												map.addLayer(layer);

												layer.enableEdit();
												
												$scope.drawControl.remove();
												$scope.drawControlEdit
														.addTo(map);
												
											} else {
												
												$scope.map_error_msg ="Door should be inside the selected building";
												
												$scope.showOutlineOverlapError();
												layer.remove();
											}

										} else if ($scope.selectedMenu == "Building" ) {

											var points = latLngToPixels(layer
													.getLatLngs());

											isInsideMap = isInsideCampus(points);
											  
											if (isInsideMap) {
												isOutlineOverlap = checkOutlineOverlap(
														e.layer.outlinesId,
														points);
											}

											if (isInsideMap && isOutlineOverlap) {

												$("#campusDetailsDiv").hide();
												$("#selectedWall").hide();
												$("#selectedDoor").hide();
												$("#selectedId").show();
												$("#selectedPartition").hide(); 
												$("#selectedPathway").hide(); 
												$("#idExportMapDetail").hide();
												$("#idImportMapDetail").hide();
												$("#idImportZoneDetail").hide();
												if ($scope.selectedRow != null) {
													$scope.cancelEditOutline();
												}

												$scope.drawControl.remove();
												$scope.drawControlEdit
														.addTo(map);

												$scope.editableLayers
														.addLayer(layer);

												$timeout(
														function() {
															$scope.noList = false;
															$scope.outlineList
																	.push({
																		"outlinesId" : "",
																		"name" : "Building"
																				+ ($scope.outlineList.length + 1),
																		"coordinateList" : latLngToPixels(layer
																				.getLatLngs()),
																		"type" : type,
																		"colorCode" : '#3388ff',
																		"wallWidth" : 2

																	})

															$("#selectedId")
																	.show();

															$scope.selectedRow = $scope.outlineList.length - 1;
															$scope.outlinesId = '';
															$scope.colorCode = '#3388ff';
															$scope.wallWidth = 2;
															$scope.outlineName = "Building"
																	+ ($scope.outlineList.length);
															$scope.outlineVertices = latLngToPixels(layer
																	.getLatLngs());
															
															// $scope.getGridList();
															

															$scope.geometryType = type;

															$("#outlineEditBtn")
																	.text(
																			'Save');
															$scope.isEdit = true;
															$("#btnDelete")
																	.hide();
															$(
																	"#outlineDimension")
																	.val(
																			JSON
																					.stringify($scope.outlineVertices));

															$scope.outlinesArea = $scope
																	.area(editlatLngToPixels(layer
																			.getLatLngs()));
															$scope.$apply();

															layer.outlinesId = "";
															layer.id = $scope.selectedRow;
															layer.name = $scope.outlineName;
															layer.outlinesArea = $scope.outlinesArea;
															layer.colorCode = $scope.colorCode;
															layer.wallType = $scope.wallType;
															layer.placeType = $scope.placeType;
															layer.geometryType = $scope.geometryType;
															layer.outlinesArea = $scope.outlinesArea;
															layer.coordinateList = latLngToPixels(layer
																	.getLatLngs());
															layer.menuType = "Building";
															map.addLayer(layer);

															layer.enableEdit();

														}, 500);

											} else {
												$scope.map_error_msg = "Building should not overlap with other Building";
												$scope.showOutlineOverlapError();
												layer.remove();
											}

										} else if ($scope.selectedMenu == "Wall" ) {

											if($scope.outlinesId==undefined || $scope.outlinesId==null || $scope.outlinesId=="")
											{
												$scope.map_error_msg = "Please select building";
												$scope.showOutlineOverlapError();
												layer.remove();
												return;
											}
											
											var points;
											if(type=='polyline'){
												points = latLngToPixels1(layer
														.getLatLngs());
											}else
											{
												 points = latLngToPixels(layer
															.getLatLngs());
											}
											   
											isInsideMap = isInsideCampus(points);
											  
											if (isInsideMap) {
												if(type=='polyline')
													isOutlineOverlap = checkDoorOverlap(
															$scope.outlinesId,
															points);
												else
													isOutlineOverlap = checkWallInsideBuilding(
															$scope.outlinesId,
															points);
											}

											if (isInsideMap && isOutlineOverlap) {

												$("#campusDetailsDiv").hide();
												$("#selectedId").hide(); 
												$("#selectedWall").show();
												$("#selectedDoor").hide();
												$("#selectedPartition").hide(); 
												$("#selectedPathway").hide(); 				
												$("#idExportMapDetail").hide();
												$("#idImportMapDetail").hide();
												$("#idImportZoneDetail").hide();
												/*
												 * if ($scope.selectedRow !=
												 * null) {
												 * $scope.cancelEditWall(); }
												 */

												$scope.drawControl.remove();
												$scope.drawControlEdit
														.addTo(map);

												$scope.editableLayers
														.addLayer(layer);

												  
 
												$scope.colorCode = '#3388ff';
												$scope.wallColorCode = '#3388ff'; 
												$scope.wallWidth = 4;
												$scope.wallId = "";
												$scope.wallName = "Wall";
												$scope.wallVertices = points;
												$scope.wallWallType = 'Normal'; 

												$scope.geometryTypeWall = type;

												$("#wallEditBtn").text('Save');
												$scope.isWallEdit = true;
												$("#btnWallDelete").hide(); 

												 
												$scope.$apply();

												layer.outlinesId = $scope.outlinesId;
												layer.wallId = $scope.wallId;
												layer.name = $scope.wallName; 
												layer.colorCode = $scope.wallColorCode;  
												layer.geometryType = $scope.geometryTypeWall; 
												layer.coordinateList = $scope.wallVertices;
												layer.menuType = $scope.selectedMenu;
												layer.wallWallType = $scope.wallWallType;
												map.addLayer(layer);

												layer.enableEdit(); 
											


											} else {
												$scope.map_error_msg = "Wall should be inside the selected building";
												$scope.showOutlineOverlapError();
												layer.remove();
											}

										}else  if ($scope.selectedMenu == "Partition") {


											if($scope.outlinesId==undefined || $scope.outlinesId==null || $scope.outlinesId=="")
											{
												$scope.map_error_msg = "Please select building";
												$scope.showOutlineOverlapError();
												layer.remove();
												return;
											}
											
											var points;
											if(type=='polyline'){
												points = latLngToPixels1(layer
														.getLatLngs());
											}else
											{
												 points = latLngToPixels(layer
															.getLatLngs());
											}
											  
											 
											isInsideMap = isInsideCampus(points);
											 
											if (isInsideMap) {
												if(type=='polyline')
													isOutlineOverlap = checkDoorOverlap(
															$scope.outlinesId,
															points);
												else
													isOutlineOverlap = checkWallInsideBuilding(
															$scope.outlinesId,
															points);
											}

											if (isInsideMap && isOutlineOverlap) {

												$("#campusDetailsDiv").hide();
												$("#selectedId").hide(); 
												$("#selectedWall").hide();
												$("#selectedDoor").hide();
												$("#selectedPartition").show(); 
       											$("#selectedPathway").hide(); 
       											$("#idExportMapDetail").hide();
       											$("#idImportMapDetail").hide();
       											$("#idImportZoneDetail").hide();
												/*
												 * if ($scope.selectedRow !=
												 * null) {
												 * $scope.cancelEditWall(); }
												 */

												$scope.drawControl.remove();
												$scope.drawControlEdit.addTo(map);

												$scope.editableLayers.addLayer(layer);

												  
 
												$scope.colorCode = '#3388ff';
												$scope.partitionColorCode = '#3388ff'; 
												$scope.partitionWidth = 4;
												$scope.partitionId = "";
												$scope.partitionName = "Partition";
												$scope.partitionWallType = 'Normal';
												$scope.partitionVertices = points;

												$scope.geometryTypePartition = type;

												$("#partitionEditBtn").text('Save');
												$scope.isPartitionEdit = true;
												$("#btnPartitionDelete").hide(); 

												 
												$scope.$apply();

												layer.outlinesId = $scope.outlinesId;
												layer.partitionId = $scope.partitionId;
												layer.partitionName = $scope.partitionName; 
												layer.colorCode = $scope.partitionColorCode;  
												layer.geometryType = $scope.geometryTypePartition; 
												layer.coordinateList = $scope.partitionVertices;
												layer.menuType = $scope.selectedMenu;
												layer.partitionWallType = $scope.partitionWallType;
												map.addLayer(layer);

												layer.enableEdit(); 
											
												

											} else {
												
												$scope.map_error_msg = "Partition should be inside the selected building";
												$scope.showOutlineOverlapError();
												layer.remove();
											}

										
										}else  if ($scope.selectedMenu == "Pathway") {
											
											var points = latLngToPixels(layer
													.getLatLngs());
											isInsideMap = isInsideCampus(points);
											  
											if (isInsideMap) {

												$("#campusDetailsDiv").hide();
												$("#selectedId").hide(); 
												$("#selectedWall").hide();
												$("#selectedDoor").hide();
												$("#selectedPartition").hide(); 
												$("#selectedPathway").show(); 
												$("#idExportMapDetail").hide();
												$("#idImportMapDetail").hide();
												$("#idImportZoneDetail").hide();
												/*
												 * if ($scope.selectedRow !=
												 * null) {
												 * $scope.cancelEditWall(); }
												 */

												$scope.drawControl.remove();
												$scope.drawControlEdit
														.addTo(map);

												$scope.editableLayers
														.addLayer(layer);

												  
 
												$scope.colorCode = '#3388ff';
												$scope.pathwayColorCode = '#3388ff'; 
												$scope.pathwayWidth = 4;
												$scope.pathwayId = "";
												$scope.pathwayName = "pathway";
												$scope.pathwayVertices = points;
												$scope.pathwayType = "All";
												$scope.geometryTypePathway = type;
												$scope.selectedPatternName = ''; 
												$scope.selectedPatternImage = ''; 
												
												$("#pathwayEditBtn").text('Save');
												$scope.isPathwayEdit = true;
												$("#btnPathwayDelete").hide(); 
												$("#selectedPattern").hide();

												 
												$scope.$apply();
												layer.campusId=$scope.campusId;
 												layer.pathwayId = $scope.pathwayId;
												layer.pathwayName = $scope.pathwayName; 
												layer.colorCode = $scope.pathwayColorCode;  
												layer.geometryType = $scope.geometryTypePathway; 
												layer.coordinateList = $scope.pathwayVertices;
												layer.menuType = $scope.selectedMenu;
												layer.pathwayType = $scope.pathwayType;
												map.addLayer(layer);
												
												layer.enableEdit(); 
											
												

											} else {
												$scope.map_error_msg = "Pathway should be inside the campus"; 
												$scope.showOutlineOverlapError();
												layer.remove();
											}

										
										}
									});

					map
							.on(
									'draw:drawstart',
									function(e) {
										map.closePopup();
										if ($scope.isEdit) {
											var r;
											$
													.confirm({
														title : $rootScope.MSGBOX_TITLE,
														content : 'Are you sure you want to discard your changes?',
														type : 'blue',
														columnClass : 'medium',
														autoClose : false,
														icon : 'fa fa-info-circle',
														buttons : {
															yes : {
																text : 'Yes',
																btnClass : 'btn-blue',
																action : function() {
																	r = true;
																}
															},
															no : function() {
																r = false;

															}
														}
													});
											if (r === false) {
												return false;
											} else {
												$scope.isDrawEvent = false;
												$scope.cancelEditOutline();
											}
										} else {
											$scope.isDrawEvent = true;
											
											 
										}
									});
					
					map.on('draw:drawvertex', function(e) {
						if ($scope.selectedMenu == "Door" || $scope.selectedMenu == "Wall" ||  $scope.selectedMenu=="Partition" ||  $scope.selectedMenu=="Pathway") {
							const layerIds = Object.keys(e.layers._layers);
							if( $scope.selectedMenu!="Pathway"&&($scope.outlinesId==undefined || $scope.outlinesId==null || $scope.outlinesId==""))
							{
								const firstVertex = e.layers._layers[layerIds[0]]._icon; 
								
								if(firstVertex !=undefined && firstVertex!=null )
									requestAnimationFrame(() => firstVertex.click());
						        $scope.map_error_msg = "Please select building"; 
								$scope.showOutlineOverlapError();
								e.layers.remove();
								return;
							
							}
								
						
							if (layerIds.length >= 2) {
								const secondVertex = e.layers._layers[layerIds[1]]._icon; 
								
								if(secondVertex !=undefined && secondVertex!=null )
									requestAnimationFrame(() => secondVertex.click());
							}
						}
					});


					map.on('draw:drawstop', function(e) {
						$scope.isDrawEvent = false;
					});

					map.on('draw:toolbarclosed', function(e) {
						$scope.isDrawEvent = false;
					});

					map
							.on(
									'editable:vertex:dragend',
									function(e) {

										if ($(".leaflet-vertex-icon").length != 0) {
											for (var i = 0; i < $(".leaflet-vertex-icon").length; i++) {
												$(".leaflet-vertex-icon")[i].style.background = "#fff"
											}
										}

										

										var isInsideMap = true;
										var isOutlineOverlap = true;
										var type = e.layer.geometryType, layer = e.layer;
										
										if ($scope.selectedMenu == "Door") {
 
 											
											var points = latLngToPixels1(layer
													.getLatLngs());
											isInsideMap = isInsideCampus(points);
										  
											if (isInsideMap) {
												
												   isOutlineOverlap =
														  checkDoorOverlap(
															 $scope.outlinesId, points);  
													 
												}

												if (isInsideMap && isOutlineOverlap) {
	 
													$("#campusDetailsDiv").hide();
													$("#selectedId").hide();
													$("#selectedDoor").show();
													$("#idExportMapDetail").hide();
													$("#idImportMapDetail").hide();
													$("#idImportZoneDetail").hide();
													layer.outlinesId = $scope.outlinesId; 
													
													
													 
													$scope.doorVertices = points;
													
													if($scope.doorId=="")
														$("#doorEditBtn").text('Save');
													else 
														$("#doorEditBtn").text('Update');
													
													$scope.isDoorEdit = true;
													$("#btnDoorDelete").hide();
											
													$scope.editableLayers
													.addLayer(layer); 
													
													$scope.$apply();

													layer.outlinesId = $scope.outlinesId;
													layer.menuType = "Door";
													if($scope.doorId=="")
														layer.doorId = "";
													else 
														layer.doorId = $scope.doorId;
													
													layer.doorName = $scope.doorName; 
													layer.doorColorCode = $scope.doorColorCode; 
													layer.geometryType = "Line"; 
													layer.doorWallType = $scope.doorWallType;
													layer.coordinateList =points;

													map.addLayer(layer);

													layer.enableEdit();
													
													$scope.drawControl.remove();
													$scope.drawControlEdit
															.addTo(map);
													
												} else {
													$scope.map_error_msg ="Door should be inside the selected building"; 
													layer.remove();
													$scope.showOutlineOverlapError();
													var bounds = getBoundsArray($scope.doorVertices); 
													
													var line = L
													.polyline(
															bounds,
															{
																color : $scope.doorColorCode,
																weight : getWallWeight($scope.doorWallType),
																fillColor : $scope.doorColorCode,
																fillOpacity : 0
															})
													.addTo(map)
													.addTo($scope.editableLayers)
													.on('click', onDoorClick)
													.on('mouseover', mouseOverPopup);
													 
													
													line.outlinesId = $scope.outlinesId;
													line.menuType = "Door";
													line.doorId = $scope.doorId;
													
													line.doorName = $scope.doorName; 
													line.doorColorCode = $scope.doorColorCode; 
													line.geometryType = "Line"; 
													line.coordinateList = bounds;
													line.doorWallType = $scope.doorWallType;
													line.enableEdit(); 
												}

										}   else if ($scope.selectedMenu == "Building") {

 
											var outlineVertices = latLngToPixels(e.layer._latlngs);
											isInsideMap = isInsideCampus(outlineVertices); 
											
											if (isInsideMap) {
												isOutlineOverlap = checkOutlineOverlap(
														e.layer.outlinesId,
														outlineVertices);
											}

											if (isInsideMap && isOutlineOverlap) {
												console.log('Drag: Inside');
												map.closePopup();

												$scope.selectedRow = e.layer.id;

												if (e.layer.outlinesId != undefined) {
													$scope.outlinesId = e.layer.outlinesId;
												} else {
													$scope.outlinesId = '';
												}

												$scope.outlineName = e.layer.name;
												$scope.outlineVertices = outlineVertices;
												$scope.outlineMapId = $scope.mapId;
												$scope.geometryType = e.layer.geometryType;
												$scope.outlinesArea = $scope
														.area(editlatLngToPixels(e.layer._latlngs));

												$scope.$apply();
												if (!$scope.isEdit) {
													$("#btnDelete").show();
												}
											} else {

												if (e.layer.colorCode != undefined) {
													$scope.colorCode = e.layer.colorCode;
													$scope.opacity = 0.8;
												} else {
													$scope.colorCode = '#3388ff';
													$scope.opacity = 0.5;
												}
												e.layer.remove();
												$scope.map_error_msg = "Building should not overlap with other Building";
												$scope.showOutlineOverlapError();

												var bounds = getBoundsArray($scope.outlineVertices); 
												var layer = null;
												if ($scope.geometryType == "polygon") {
													  
													layer = L
															.polygon(
																	bounds,
																	{
																		color : $scope.colorCode,
																		weight : getWallWeight($scope.wallType),
																		fillColor : $scope.colorCode,
																		fillOpacity : 0.8
																	}); 

												} else {

													layer = L
															.rectangle(
																	bounds,
																	{
																		color : $scope.colorCode,
																		weight : getWallWeight($scope.wallType),
																		fillColor : $scope.colorCode,
																		fillOpacity : 0.8
																	});
 

												}
												

												layer.addTo(map)
													.addTo($scope.editableLayers)
													.on('click', onClick)
													.on('mouseover', mouseOverPopup);
														
												$scope.outlinesArea = $scope
												.area(editlatLngToPixels(layer
														.getLatLngs()));
												layer.id = $scope.selectedRow;
												layer.menuType = "Building";
												layer.outlinesId = $scope.outlinesId;
												layer.name = $scope.outlineName;
												layer.colorCode = $scope.colorCode;
												layer.outlinesArea = $scope.outlinesArea; 
												layer.wallType = $scope.wallType;
												layer.placeType = $scope.placeType;
												layer.geometryType = $scope.geometryType;
												layer.coordinateList = bounds;
												layer.enableEdit();
											}
										}else if ($scope.selectedMenu == "Wall") {

  
											var outlineVertices;
											if(type=='polyline'){
												outlineVertices = latLngToPixels1(e.layer._latlngs);
											}else
											{
												outlineVertices = latLngToPixels(e.layer._latlngs);
											}
											 
											isInsideMap = isInsideCampus(outlineVertices);
											
											if (isInsideMap) {
												if(type=='polyline')
													isOutlineOverlap = checkDoorOverlap(
															$scope.outlinesId,
															outlineVertices);
												else
													isOutlineOverlap = checkWallInsideBuilding(
															$scope.outlinesId, outlineVertices);
											}

											if (isInsideMap && isOutlineOverlap) { 
												 
												$scope.wallVertices = outlineVertices;  

												$scope.$apply();
												if (!$scope.isDoorEdit) {
													$("#btnDoorDelete").show();
												}
											} else {

												if (e.layer.colorCode != undefined) {
													$scope.wallColorCode = e.layer.colorCode;
													$scope.opacity = 0.8;
												} else {
													$scope.wallColorCode = '#3388ff';
													$scope.opacity = 0.5;
												}
												e.layer.remove();
												$scope.map_error_msg = "Wall should be inside the selected building";
												$scope.showOutlineOverlapError();
												var bounds = getBoundsArray($scope.wallVertices);  
												var layer = null;
												if (type == "polyline") { 
													
													layer = L
													.polyline(
															bounds,
															{
																color : $scope.wallColorCode,
																weight : getWallWeight($scope.wallWallType),
																fillColor : $scope.wallColorCode,
																fillOpacity : 0
															}); 
												}else if (type == "polygon") { 
													 
													layer = L
															.polygon(
																	bounds,
																	{
																		color : $scope.wallColorCode,
																		weight : getWallWeight($scope.wallWallType),
																		fillColor : $scope.wallColorCode,
																		fillOpacity : 0
																	});
													 

												} else {
 
													layer = L
															.rectangle(
																	bounds,
																	{
																		color : $scope.wallColorCode,
																		weight : getWallWeight($scope.wallWallType),
																		fillColor : $scope.wallColorCode,
																		fillOpacity : 0 
																	});
 

												} 


												layer.addTo(map)
													.addTo($scope.editableLayers)
													.on('click', onClickWall)
													.on('mouseover', mouseOverPopup);
														
												layer.wallId = $scope.wallId;
												layer.name = $scope.wallName; 
												layer.colorCode = $scope.wallColorCode;  
												layer.geometryType = $scope.geometryTypeWall; 
												layer.coordinateList = bounds;
												layer.menuType = $scope.selectedMenu;
												layer.wallWallType = $scope.wallWallType;
												
												layer.enableEdit();
											}
										}else if ($scope.selectedMenu=="Partition") {
											
											var partitionVertices;
											if(type=='polyline'){
												partitionVertices = latLngToPixels1(e.layer._latlngs);
											}else
											{
												partitionVertices = latLngToPixels(e.layer._latlngs);
											}
											
											isInsideMap = isInsideCampus(partitionVertices);
											
											if (isInsideMap) {
												if(type=='polyline')
													isOutlineOverlap = checkDoorOverlap(
															$scope.outlinesId,
															partitionVertices);
												else
													isOutlineOverlap = checkWallInsideBuilding(
															$scope.outlinesId, partitionVertices);
											}

											if (isInsideMap && isOutlineOverlap) { 
												map.closePopup(); 
												
												$scope.partitionVertices = partitionVertices;  

												$scope.$apply();
												if (!$scope.isPartitionEdit) {
													$("#btnPartitionDelete").show();
												}
											} else {


												if (e.layer.colorCode != undefined) {
													$scope.partitionColorCode = e.layer.colorCode;
													$scope.opacity = 0.8;
												} else {
													$scope.partitionColorCode = '#3388ff';
													$scope.opacity = 0.5;
												}
												e.layer.remove();

												$scope.map_error_msg = "Partition should be inside the selected building";
												$scope.showOutlineOverlapError();
												
												
												var bounds = getBoundsArray($scope.partitionVertices);  
												 
												var layer = null;
												
												if (type == "polyline") { 
													
													layer = L.polyline(bounds,
															{
																color : $scope.partitionColorCode,
																weight : getWallWeight($scope.partitionWallType),
																fillColor : $scope.partitionColorCode,
																fillOpacity : 0
															});
													 
													
													
												}else if (type == "polygon") {
													
													layer = L.polygon(bounds,
																	{
																		color : $scope.partitionColorCode,
																		weight : getWallWeight($scope.partitionWallType),
																		fillColor : $scope.partitionColorCode,
																		fillOpacity : 0
																	}); 

												} else {
 
													layer= L.rectangle(bounds,
																	{
																		color : $scope.partitionColorCode,
																		weight : getWallWeight($scope.partitionWallType),
																		fillColor : $scope.partitionColorCode,
																		fillOpacity : 0 
																	}); 
												}
												

												layer.addTo(map)
													.addTo($scope.editableLayers)
													.on('click', onClickPartition)
													.on('mouseover', mouseOverPopup);
												
												layer.outlinesId = $scope.outlinesId;
												layer.partitionId = $scope.partitionId;
												layer.partitionName = $scope.partitionName; 
												layer.colorCode = $scope.partitionColorCode;  
												layer.geometryType = $scope.geometryTypePartition; 
												layer.coordinateList = $scope.partitionVertices;
												layer.menuType = $scope.selectedMenu;
												layer.partitionWallType = $scope.partitionWallType;
												layer.enableEdit();
											
											}
										}else if ($scope.selectedMenu=="Pathway") {
											
											var pathwayVertices = latLngToPixels(e.layer._latlngs);
											 
											isInsideMap = isInsideCampus(pathwayVertices);
										 

											if (isInsideMap) { 
												map.closePopup(); 
												
												$scope.pathwayVertices = pathwayVertices;  

												$scope.$apply();
												if (!$scope.isPathwayEdit) {
													$("#btnPathwayDelete").show();
												}
											} else {


												if (e.layer.colorCode != undefined) {
													$scope.pathwayColorCode = e.layer.colorCode;
													$scope.opacity = 0.8;
												} else {
													$scope.pathwayColorCode = '#3388ff';
													$scope.opacity = 0.5;
												}
												e.layer.remove();
												$scope.map_error_msg = "Pathway should be inside the campus";
												$scope.showOutlineOverlapError();
												
												
												var bounds = getBoundsArray($scope.pathwayVertices);  
												 var theme = {
															color : $scope.pathwayColorCode,
															weight : 3,
															fillColor : $scope.pathwayColorCode, 
															fill : 'url('+getPathwayPattern($scope.selectedPatternName)+')' 
															
														};
												var layer = null;
											  if (type == "polygon") { 
													layer = L.polygon(bounds, theme); 
												} else { 
													layer= L.rectangle(bounds, theme); 
												}
											  

											  	layer.addTo(map)
											  		.addTo($scope.editableLayers)
											  		.on('click', onClickPathway)
											  		.on('mouseover', mouseOverPopup);
												
												layer.campusId=$scope.campusId;
												layer.pathwayId = $scope.pathwayId;
												layer.pathwayName = $scope.pathwayName; 
												layer.colorCode = $scope.pathwayColorCode;  
												layer.geometryType = $scope.geometryTypePathway; 
												layer.coordinateList = $scope.partitionVertices;
												layer.menuType = $scope.selectedMenu;
												layer.pathwayPattern = $scope.selectedPatternName;
												layer.pathwayType = $scope.pathwayType;
												
												
												layer.enableEdit();
											
											}
										}
									});

					map
							.on(
									'editable:dragend',
									function(e) {
  
										var isInsideMap = true;
										var isOutlineOverlap = true;
										var type = e.layer.geometryType, layer = e.layer;
											
											if ($scope.selectedMenu == "Door") {
												
												var points = latLngToPixels1(e.layer._latlngs);
												isInsideMap = isInsideCampus(points); 
												
												if (isInsideMap) {  
													isOutlineOverlap =
														  checkDoorOverlap(
															 $scope.outlinesId, points);  
													
												}
												if (isInsideMap && isOutlineOverlap) {
													$scope.doorVertices = points; 
													$scope.$apply();
												}else
												{
													if (e.layer.colorCode != undefined) {
														$scope.colorCode = e.layer.colorCode;
														$scope.opacity = 0.8;
													} else {
														$scope.colorCode = '#3388ff';
														$scope.opacity = 0.5;
													}
													e.layer.remove();
													$scope.map_error_msg ="Door should be inside the selected building";
													$scope.showOutlineOverlapError();
													var bounds =  getBoundsArray($scope.doorVertices);  
												 	
													var line = L
													.polyline(
															bounds,
															{
																color : $scope.doorColorCode,
																weight : getWallWeight($scope.doorWallType),
																fillColor : $scope.doorColorCode,
																fillOpacity : 0
															})
													.addTo(map)
													.addTo($scope.editableLayers)
													.on('click', onDoorClick)
													.on('mouseover', mouseOverPopup);
													 
													
													line.outlinesId = $scope.outlinesId;
													line.menuType = "Door";
													line.doorId = $scope.doorId;
													
													line.doorName = $scope.doorName; 
													line.doorColorCode = $scope.doorColorCode; 
													line.geometryType = "Line"; 
													line.coordinateList = bounds;
													line.doorWallType = $scope.doorWallType;
													line.enableEdit(); 
													
												}
												
											}else if ($scope.selectedMenu == "Building") {
											 
												var points = latLngToPixels(e.layer._latlngs); 
												isInsideMap = isInsideCampus(points); 

											if (isInsideMap) {
												isOutlineOverlap = checkOutlineOverlap(
														e.layer.outlinesId,
														points);
											}

											if (isInsideMap && isOutlineOverlap) {

												$scope.selectedRow = e.layer.id;
												if (e.layer.outlinesId != undefined) {
													$scope.outlinesId = e.layer.outlinesId;
												} else {
													$scope.outlinesId = '';
												}
												// $scope.outlineClassification
												// =
												// e.layer.classification;
												$scope.outlineName = e.layer.name;
												// $scope.outlineRestrictions =
												// e.layer.restrictions;
												$scope.outlineVertices = points;
												$scope.outlineMapId = $scope.mapId;
												$scope.geometryType = e.layer.geometryType;
												$scope.outlinesArea = $scope
														.area(editlatLngToPixels(e.layer._latlngs));

												// $("#outlineDimension").val(JSON.stringify($scope.outlineVertices));
												$scope.$apply();
												if (!$scope.isEdit) {
													$("#btnDelete").show();
												}
											} else {
												if (e.layer.colorCode != undefined) {
													$scope.colorCode = e.layer.colorCode;
													$scope.opacity = 0.8;
												} else {
													$scope.colorCode = '#3388ff';
													$scope.opacity = 0.5;
												}
												e.layer.remove();
												$scope.map_error_msg = "Building should not overlap with other Building";
												$scope.showOutlineOverlapError(); 
												var bounds =  getBoundsArray($scope.outlineVertices);  
												var layer = null;
												var theme = {
														color : $scope.colorCode,
														weight : getWallWeight($scope.wallType),
														fillColor : $scope.colorCode,
														fillOpacity : $scope.opacity
													};
												
												if ($scope.geometryType == "polygon") { 
													layer = L.polygon(bounds, theme); 
												} else { 
													layer = L.rectangle(bounds, theme);  
												}
												

												layer.addTo(map)
												.addTo($scope.editableLayers)
												.on('click', onClick)
												.on('mouseover', mouseOverPopup);
														
												$scope.outlinesArea = $scope
												.area(editlatLngToPixels(layer
														.getLatLngs()));

												layer.id = $scope.selectedRow;
												layer.outlinesId = $scope.outlinesId;
												layer.name = $scope.outlineName;
												layer.colorCode = $scope.colorCode;
												layer.outlinesArea = $scope.outlinesArea;
												layer.menuType = "Building";
												layer.wallType = $scope.wallType;
												layer.placeType = $scope.placeType;
												layer.geometryType = $scope.geometryType;
												layer.coordinateList = bounds;
		
												layer.enableEdit();

											} 
											
									}else if ($scope.selectedMenu == "Wall") {

										  
										var outlineVertices;
										if(type=='polyline'){
											outlineVertices = latLngToPixels1(e.layer._latlngs);
										}else
										{
											outlineVertices = latLngToPixels(e.layer._latlngs);
										}
										

										isInsideMap = isInsideCampus(outlineVertices); 
										
										if (isInsideMap) {
											if(type=='polyline')
												isOutlineOverlap = checkDoorOverlap(
														$scope.outlinesId, outlineVertices);
											else
												isOutlineOverlap = checkWallInsideBuilding(
														$scope.outlinesId, outlineVertices);
										}

										if (isInsideMap && isOutlineOverlap) { 
											map.closePopup(); 
											
											$scope.wallVertices = outlineVertices;  

											$scope.$apply();
											if (!$scope.isDoorEdit) {
												$("#btnDoorDelete").show();
											}
										} else {

											if (e.layer.colorCode != undefined) {
												$scope.wallColorCode = e.layer.colorCode;
												$scope.opacity = 0.8;
											} else {
												$scope.wallColorCode = '#3388ff';
												$scope.opacity = 0.5;
											}
											e.layer.remove();
											$scope.map_error_msg = "Wall should be inside the selected building";
											$scope.showOutlineOverlapError();
											var bounds = getBoundsArray($scope.wallVertices);  
											var layer= null;
											var theme = {
													color : $scope.wallColorCode,
													weight : getWallWeight($scope.wallWallType),
													fillColor : $scope.wallColorCode,
													fillOpacity : 0
												};
											
											if (type == "polyline") {  
												layer = L.polyline(bounds, theme);  
											}else if (type == "polygon") { 
												layer = L.polygon( bounds, theme); 
											} else {
												layer = L.rectangle( bounds, theme);  
											} 


											layer.addTo(map)
											.addTo($scope.editableLayers)
											.on('click', onClickWall)
											.on('mouseover', mouseOverPopup);
													
											layer.wallId = $scope.wallId;
											layer.name = $scope.wallName; 
											layer.colorCode = $scope.wallColorCode;  
											layer.geometryType = $scope.geometryTypeWall;  
											layer.menuType = $scope.selectedMenu;
											layer.coordinateList = bounds;
											layer.wallWallType = $scope.wallWallType;
											layer.enableEdit();
										}
									}else if ($scope.selectedMenu=="Partition") {
										
										var partitionVertices;
										if(type=='polyline'){
											partitionVertices = latLngToPixels1(e.layer._latlngs);
										}else
										{
											partitionVertices = latLngToPixels(e.layer._latlngs);
										}
										

										isInsideMap = isInsideCampus(partitionVertices); 
										
										if (isInsideMap) {
											if(type=='polyline')
												isOutlineOverlap = checkDoorOverlap(
														$scope.outlinesId,
														partitionVertices);
											else
												isOutlineOverlap = checkWallInsideBuilding(
														$scope.outlinesId, partitionVertices);
										}

										if (isInsideMap && isOutlineOverlap) { 
											map.closePopup(); 
											
											$scope.partitionVertices = partitionVertices;  

											$scope.$apply();
											if (!$scope.isPartitionEdit) {
												$("#btnPartitionDelete").show();
											}
										} else {


											if (e.layer.colorCode != undefined) {
												$scope.partitionColorCode = e.layer.colorCode;
												$scope.opacity = 0.8;
											} else {
												$scope.partitionColorCode = '#3388ff';
												$scope.opacity = 0.5;
											}
											e.layer.remove();
											$scope.map_error_msg = "Partition should be inside the selected building"; 
											$scope.showOutlineOverlapError();
											
											
											var bounds = getBoundsArray($scope.partitionVertices);    
											var layer = null;
											var theme = {
													color : $scope.partitionColorCode,
													weight : getWallWeight($scope.partitionWallType),
													fillColor : $scope.partitionColorCode,
													fillOpacity : 0
												};
											
											if (type == "polyline") {  
												layer = L.polyline(bounds, theme); 
											}else if (type == "polygon") { 
												layer = L.polygon(bounds, theme);  
											} else { 
												layer= L.rectangle(bounds, theme); 
											}
											

											layer.addTo(map)
												.addTo($scope.editableLayers)
												.on('click', onClickPartition)
												.on('mouseover', mouseOverPopup);
											
											layer.outlinesId = $scope.outlinesId;
											layer.partitionId = $scope.partitionId;
											layer.partitionName = $scope.partitionName; 
											layer.colorCode = $scope.partitionColorCode;  
											layer.geometryType = $scope.geometryTypePartition; 
											layer.coordinateList = $scope.partitionVertices;
											layer.menuType = $scope.selectedMenu;
											layer.partitionWallType = $scope.partitionWallType;
											layer.enableEdit();
										
										}
									}else if ($scope.selectedMenu=="Pathway") {
										
										
										var pathwayVertices = latLngToPixels(e.layer._latlngs); 
										isInsideMap = isInsideCampus(pathwayVertices); 										 

										if (isInsideMap) { 
											map.closePopup(); 
											
											$scope.pathwayVertices = pathwayVertices;  

											$scope.$apply();
											if (!$scope.isPathwayEdit) {
												$("#btnPathwayDelete").show();
											}
										} else {


											if (e.layer.colorCode != undefined) {
												$scope.pathwayColorCode = e.layer.colorCode;
												$scope.opacity = 0.8;
											} else {
												$scope.pathwayColorCode = '#3388ff';
												$scope.opacity = 0.5;
											}
											e.layer.remove();
											$scope.map_error_msg = "Pathway should be inside the campus";
											$scope.showOutlineOverlapError();
											
											
											var bounds = getBoundsArray($scope.pathwayVertices);  
											var theme = {
													color : $scope.pathwayColorCode,
													weight : 3,
													fillColor : $scope.pathwayColorCode,
													fill : 'url('+getPathwayPattern($scope.selectedPatternName)+')' 
												};
											
											var layer = null;
											if (type == "polygon") { 
												layer = L.polygon(bounds, theme);  
											} else { 
												layer= L.rectangle(bounds, theme); 
											}
											 

										 	layer.addTo(map)
										 		.addTo($scope.editableLayers)
										 		.on('click', onClickPathway)
										 		.on('mouseover', mouseOverPopup);
											
											layer.campusId=$scope.campusId;
											layer.pathwayId = $scope.pathwayId;
											layer.pathwayName = $scope.pathwayName; 
											layer.colorCode = $scope.pathwayColorCode;  
											layer.geometryType = $scope.geometryTypePathway; 
											layer.coordinateList = $scope.pathwayVertices;
											layer.menuType = $scope.selectedMenu;
											layer.pathwayType = $scope.pathwayType;
											layer.pathwayPattern = $scope.selectedPatternName;
											layer.enableEdit();
										
										}
									}

									});

					$scope.selectedRow = null;
					$scope.map_error_msg ="Outline should not overlap with other outlines";
					$("#outlineOverlapAlert").alert("close");
					$scope.showOutlineOverlapError = function() {
						// $scope.alert_error_msg = 'Outline should not overlap
						// with other outlines.';
						 	
						$("#outlineOverlapAlert").fadeIn();
						$timeout(function() {
							$("#outlineOverlapAlert").fadeOut(300);
						}, 1500);
					}

					$scope.popup = L.popup({
						maxWidth : 400
					});

					var mouseroverid = null;
					function mouseOverPopup(e) { 
						if ($scope.isEdit || $scope.isWallEdit || $scope.isPartitionEdit 
								|| $scope.isDoorEdit|| $scope.isPathwayEdit)
							return;
 
						if (!$scope.isDrawEvent) {

							if (mouseroverid != e.sourceTarget.id) {
								var menuType = e.sourceTarget.menuType;
								var name = "";
								if(menuType=='Door')
									 name = e.sourceTarget.doorName;
								else if(menuType=='Pathway')
									 name = e.sourceTarget.pathwayName;
							   else if(menuType=='Partition')
									 name = e.sourceTarget.partitionName;
								else
								   name = e.sourceTarget.name; 
 
								
								if (name == undefined || name == null || name == "")
									name = '-'; 

								var popupContent = "<strong>Name :" + name
										+ "</strong>";
								$scope.popup.setLatLng(e.sourceTarget._bounds
										.getCenter());
								$scope.popup.setContent(popupContent);
								map.openPopup($scope.popup);
								mouseroverid = e.sourceTarget.id;
							}
						}
					}

					$scope.area = function(points) {
						var area = 0, i, j, point1, point2;

						var length = points.length;

						for (i = 0, j = length - 1; i < length; j = i, i += 1) {
							point1 = points[i];
							point2 = points[j];
							area += point1.x * point2.y;
							area -= point1.y * point2.x;
						}
						area /= 2;

						return Math.abs(area).toFixed(3);
					};
					
					$scope.pathwayVerticeChangeFunction = function(singleVer, index, name,
							pathwayVertices)
					{
						if (name == "xVal") {
							singleVer.x = $("#pathwayXVal" + index + "").val();
						} else {
							singleVer.y = $("#pathwayYVal" + index + "").val();

						}

						pathwayVertices[index] = singleVer;
						
						
						if($scope.geometryTypePathway == "rectangle") {
							
							if(index==0)
							{ 
								if (name == "xVal") {
									var val = pathwayVertices[1];
									val.x = singleVer.x;
									pathwayVertices[1] = val;
								}else
								{

									var val = pathwayVertices[3];
									val.y = singleVer.y;
									pathwayVertices[3] = val;
								} 
							
							}else if(index==1){
								
								if (name == "xVal") {
									var val = pathwayVertices[0];
									val.x = singleVer.x;
									pathwayVertices[0] = val;
								}else
								{
									var val = pathwayVertices[2];
									val.y = singleVer.y;
									pathwayVertices[2] = val;
								}
								
								 
							}else if(index==2){
								if (name == "xVal") {
									var val = pathwayVertices[3]; 
									val.x = singleVer.x;
									pathwayVertices[3] = val;
								}else
								{
									var val = pathwayVertices[1]; 
									val.y = singleVer.y;
									pathwayVertices[1] = val;
								}
								
								 
							}else if(index==3){
								
								if (name == "xVal") {
									var val = pathwayVertices[2];
									val.x = singleVer.x;
									pathwayVertices[2] = val;
								}else
								{
									var val = pathwayVertices[0];
									val.y = singleVer.y;
									pathwayVertices[0] = val;
								}
								
							} 
							
						}
						

						var isInsideMap = isInsideCampus(pathwayVertices); 		
						var isOutlineOverlap = true;
 
						map.eachLayer(function(layer) {
							if (layer.menuType !== undefined && layer.menuType=="Pathway") { 
								if (layer.pathwayId !== undefined
										&& layer.pathwayId == $scope.pathwayId ) {
									layer.remove(); 
								}  
							}							 
						});
						
						 
						 
						if (!isInsideMap) { 
							$scope.map_error_msg = "Pathway should be inside the Campus";
							$scope.showOutlineOverlapError();  
							
						}else
						{
							$scope.pathwayVertices = pathwayVertices;
						}
						 
						var bounds = getBoundsArray($scope.pathwayVertices);  
 
						var layer= null;
						var theme = {
								color : $scope.pathwayColorCode,
								weight : 3,
								fillColor : $scope.pathwayColorCode,
								fill : 'url('+getPathwayPattern($scope.selectedPatternName)+')' 
							};
						if($scope.geometryTypePathway == "polyline")
						{ 
							layer = L.polyline(bounds,theme); 
						}else if ($scope.geometryTypePathway == "polygon") { 
							layer = L.polygon(bounds, theme);  
						} else { 
							layer = L.rectangle(bounds, theme);  
						}
						

						layer.addTo(map)
							.addTo($scope.editableLayers)
							.on('click', onClickPathway)
							.on('mouseover', mouseOverPopup);
						
						layer.pathwayId = $scope.pathwayId;
						layer.pathwayName = $scope.pathwayName; 
						layer.colorCode = $scope.pathwayColorCode;  
						layer.geometryType = $scope.geometryTypePathway; 
						layer.coordinateList = bounds;
						layer.menuType = $scope.selectedMenu;
						layer.pathwayPattern = $scope.selectedPatternName;
						layer.pathwayType = $scope.pathwayType;
						
						layer.enableEdit();
						
						$(".leaflet-vertex-icon")[index].style.background = "red"
						
					}
					
					$scope.partitionVerticeChangeFunction = function(singleVer, index, name,
							partitionVertices)
					{
						if (name == "xVal") {
							singleVer.x = $("#partitionXVal" + index + "").val();
						} else {
							singleVer.y = $("#partitionYVal" + index + "").val();

						}

						partitionVertices[index] = singleVer;
						
						
						if($scope.geometryTypePartition == "rectangle") {
							
							if(index==0)
							{ 
								if (name == "xVal") {
									var val = partitionVertices[1];
									val.x = singleVer.x;
									partitionVertices[1] = val;
								}else
								{

									var val = partitionVertices[3];
									val.y = singleVer.y;
									partitionVertices[3] = val;
								} 
							
							}else if(index==1){
								
								if (name == "xVal") {
									var val = partitionVertices[0];
									val.x = singleVer.x;
									partitionVertices[0] = val;
								}else
								{
									var val = partitionVertices[2];
									val.y = singleVer.y;
									partitionVertices[2] = val;
								}
								
								 
							}else if(index==2){
								if (name == "xVal") {
									var val = partitionVertices[3]; 
									val.x = singleVer.x;
									partitionVertices[3] = val;
								}else
								{
									var val = partitionVertices[1]; 
									val.y = singleVer.y;
									partitionVertices[1] = val;
								}
								
								 
							}else if(index==3){
								
								if (name == "xVal") {
									var val = partitionVertices[2];
									val.x = singleVer.x;
									partitionVertices[2] = val;
								}else
								{
									var val = partitionVertices[0];
									val.y = singleVer.y;
									partitionVertices[0] = val;
								} 
							}  
						}
						

					    var isInsideMap = isInsideCampus(partitionVertices); 	
						var isOutlineOverlap = true;
 
						map.eachLayer(function(layer) {
							if (layer.menuType !== undefined && layer.menuType=="Partition") { 
								if (layer.partitionId !== undefined
										&& layer.partitionId == $scope.partitionId ) {
									layer.remove(); 
								}  
							}							 
						});
						
						
						
						if (isInsideMap) {
							if($scope.geometryTypePartition=='polyline')
								isOutlineOverlap = checkDoorOverlap(
										$scope.outlinesId,
										partitionVertices);
							else
								isOutlineOverlap = checkWallInsideBuilding(
										$scope.outlinesId, partitionVertices);
						}
						
						 
						if (!isInsideMap  ||  !isOutlineOverlap) { 
							$scope.map_error_msg = "Partition should be inside the selected building";
							$scope.showOutlineOverlapError();  
							
						}else
						{
							$scope.partitionVertices = partitionVertices;
						}
						 
						var bounds  = getBoundsArray($scope.partitionVertices);  
						var theme = {
								color : $scope.partitionColorCode,
								weight : getWallWeight($scope.partitionWallType),
								fillColor : $scope.partitionColorCode,
								fillOpacity : 0
							};
						
						var layer= null;
						if($scope.geometryTypePartition == "polyline")
						{ 
							layer = L.polyline(bounds,theme); 
						}else if ($scope.geometryTypePartition == "polygon") { 
							layer = L.polygon(bounds, theme);  
						} else { 
							layer = L.rectangle(bounds, theme);  
						}
						

						layer.addTo(map)
							.addTo($scope.editableLayers)
							.on('click', onClickPartition)
							.on('mouseover', mouseOverPopup);
						
						layer.partitionId = $scope.partitionId;
						layer.partitionName = $scope.partitionName; 
						layer.colorCode = $scope.partitionColorCode;  
						layer.geometryType = $scope.geometryTypePartition; 
						layer.coordinateList = bounds;
						layer.menuType = $scope.selectedMenu;
						layer.partitionWallType = $scope.partitionWallType;
						
						layer.enableEdit();
						
						$(".leaflet-vertex-icon")[index].style.background = "red"
						
					}
					
					$scope.wallVerticeChangeFunction = function(singleVer, index, name,
							wallVertices)
					{
						if (name == "xVal") {
							singleVer.x = $("#wallXVal" + index + "").val();
						} else {
							singleVer.y = $("#wallYVal" + index + "").val();

						}

						wallVertices[index] = singleVer;
						
						if($scope.geometryTypeWall == "rectangle") {
							
							if(index==0)
							{ 
								if (name == "xVal") {
									var val = wallVertices[1];
									val.x = singleVer.x;
									wallVertices[1] = val;
								}else
								{

									var val = wallVertices[3];
									val.y = singleVer.y;
									wallVertices[3] = val;
								} 
							
							}else if(index==1){
								
								if (name == "xVal") {
									var val = wallVertices[0];
									val.x = singleVer.x;
									wallVertices[0] = val;
								}else
								{
									var val = wallVertices[2];
									val.y = singleVer.y;
									wallVertices[2] = val;
								}
								
								 
							}else if(index==2){
								if (name == "xVal") {
									var val = wallVertices[3]; 
									val.x = singleVer.x;
									wallVertices[3] = val;
								}else
								{
									var val = wallVertices[1]; 
									val.y = singleVer.y;
									wallVertices[1] = val;
								}
								
								 
							}else if(index==3){
								
								if (name == "xVal") {
									var val = wallVertices[2];
									val.x = singleVer.x;
									wallVertices[2] = val;
								}else
								{
									var val = wallVertices[0];
									val.y = singleVer.y;
									wallVertices[0] = val;
								}
								
							} 
							
						}


					    var isInsideMap = isInsideCampus(wallVertices); 	
						var isOutlineOverlap = true;
  
						
						map.eachLayer(function(layer) {
							if (layer.menuType !== undefined && layer.menuType=="Wall") { 
								if (layer.wallId !== undefined
										&& (layer.wallId == '' || layer.wallId ==$scope.wallId)) {
									layer.remove(); 
								}  
							}							 
						});
						
						
						
						if (isInsideMap) {
							if($scope.geometryTypeWall=='polyline')
								isOutlineOverlap = checkDoorOverlap(
										$scope.outlinesId,
										wallVertices);
							else
								isOutlineOverlap = checkWallInsideBuilding(
										$scope.outlinesId, wallVertices);
						}
						
						 
						if (!isInsideMap  ||  !isOutlineOverlap) { 
							$scope.map_error_msg = "Wall should be inside the selected building";
							$scope.showOutlineOverlapError();  
							
						}else
						{
							$scope.wallVertices = wallVertices;
						}
						
					 
						var bounds = getBoundsArray($scope.wallVertices);  
						var theme = {
								color : $scope.wallColorCode,
								weight : getWallWeight($scope.wallWallType),
								fillColor : $scope.wallColorCode,
								fillOpacity : 0
							};
						
						var layer= null;
						if($scope.geometryTypeWall == "polyline")
						{ 
							layer = L.polyline(bounds, theme); 
						}else if ($scope.geometryTypeWall == "polygon") { 
							layer = L.polygon(bounds, theme);  
						} else { 
							layer = L.rectangle(bounds, theme); 

						}
						

						layer.addTo(map)
							.addTo($scope.editableLayers)
							.on('click', onClickWall)
							.on('mouseover', mouseOverPopup);
						
						layer.wallId = $scope.wallId;
						layer.name = $scope.wallName; 
						layer.colorCode = $scope.wallColorCode;  
						layer.geometryType = $scope.geometryTypeWall; 
						layer.coordinateList = bounds;
						layer.menuType = $scope.selectedMenu;
						layer.wallWallType = $scope.wallWallType;
						
						layer.enableEdit();
						
						$(".leaflet-vertex-icon")[index].style.background = "red"
						
					}
					
					
					$scope.doorVerticeChangeFunction = function(singleVer, index, name,
							outlineVertices)
					{
						if (name == "xVal") {
							singleVer.x = $("#doorXVal" + index + "").val();
						} else {
							singleVer.y = $("#doorYVal" + index + "").val();

						}

						outlineVertices[index] = singleVer;

						var isInsideMap = isInsideCampus(outlineVertices); 	
						var isOutlineOverlap = true;

						  
						map.eachLayer(function(layer) {
							if (layer.menuType !== undefined && layer.menuType=="Door") {
								var dId = layer.doorId;
								if (layer.doorId !== undefined
										&& (layer.doorId == '' || dId==$scope.doorId)) {
									layer.remove(); 
								}  
							}							 
						});
						
						
						if (isInsideMap) {  
							isOutlineOverlap =
								  checkDoorOverlap(
									 $scope.outlinesId, outlineVertices);  
							
						}
						if (!isInsideMap  ||  !isOutlineOverlap) { 
							 
							$scope.map_error_msg ="Door should be inside the selected building";
							$scope.showOutlineOverlapError();  
							
						}else
						{
							$scope.doorVertices = outlineVertices;
						}
						
						 
						var bounds = getBoundsArray($scope.doorVertices);  
						 
						var layer = L
						.polyline(bounds,
								{
									color : $scope.doorColorCode,
									weight : getWallWeight($scope.doorWallType),
									fillColor : $scope.doorColorCode,
									fillOpacity : 0.5
								})
						.addTo(map)
						.addTo($scope.editableLayers)
						.on('click', onDoorClick)
						.on('mouseover', mouseOverPopup);

				
						layer.outlinesId = $scope.outlinesId;
						layer.menuType = "Door";
						layer.doorId = $scope.doorId;
						layer.doorName = $scope.doorName; 
						layer.doorColorCode = $scope.doorColorCode; 
						layer.geometryType = $scope.geometryTypeWall; 
						layer.coordinateList = bounds;
						layer.doorWallType = $scope.doorWallType;
						layer.enableEdit();
						
					}
					
					$scope.myFunct = function(singleVer, index, name, outlineVertices) {
						
						if (name == "xVal") {
							singleVer.x = $("#xVal" + index + "").val();
						} else {
							singleVer.y = $("#yVal" + index + "").val(); 
						}

                        outlineVertices[index] = singleVer;
						 
						if($scope.geometryType == "rectangle") {
							
							if(index==0)
							{ 
								if (name == "xVal") {
									var val = outlineVertices[1];
									val.x = singleVer.x;
									outlineVertices[1] = val;
								}else
								{

									var val = outlineVertices[3];
									val.y = singleVer.y;
									outlineVertices[3] = val;
								} 
							
							}else if(index==1){
								
								if (name == "xVal") {
									var val = outlineVertices[0];
									val.x = singleVer.x;
									outlineVertices[0] = val;
								}else
								{
									var val = outlineVertices[2];
									val.y = singleVer.y;
									outlineVertices[2] = val;
								}
								
								 
							}else if(index==2){
								if (name == "xVal") {
									var val = outlineVertices[3]; 
									val.x = singleVer.x;
									outlineVertices[3] = val;
								}else
								{
									var val = outlineVertices[1]; 
									val.y = singleVer.y;
									outlineVertices[1] = val;
								}
								
								 
							}else if(index==3){
								
								if (name == "xVal") {
									var val = outlineVertices[2];
									val.x = singleVer.x;
									 outlineVertices[2] = val;
								}else
								{
									var val = outlineVertices[0];
									val.y = singleVer.y;
									 outlineVertices[0] = val;
								}
								
							} 
							
						}
 
						

						var isInsideMap = isInsideCampus(outlineVertices); 
						var isOutlineOverlap = true;
 
						if (isInsideMap) {
							isOutlineOverlap = checkOutlineOverlap(
									$scope.outlinesId, outlineVertices);
						}

						map.eachLayer(function(layer) {

							if (layer.id !== undefined) {
								if (layer.id == $scope.selectedRow) {
									layer.remove();

								}
							}
						});
						if (isInsideMap && isOutlineOverlap) {
							$scope.outlineVertices = outlineVertices; 
						} else {

							$scope.map_error_msg ="Building should not overlap with other Building";
							$scope.showOutlineOverlapError();
							$scope.outlineVertices = $scope.orignalOutlineVertices;
						}

						var vertics = $scope.outlineVertices;
						var bounds = getBoundsArray($scope.outlineVertices); 
						
						var layer = null;
						var theme = {
								color : $scope.colorCode,
								weight : getWallWeight($scope.wallType),
								fillColor : $scope.colorCode,
								fillOpacity : 0.8
							};
						if ($scope.geometryType == "polygon") { 
							layer = L.polygon(bounds, theme); 
						} else { 
							layer = L.rectangle(bounds,theme); 

						}
						
						layer.addTo(map)
							.addTo($scope.editableLayers)
							.on('click', onClick)
							.on('mouseover', mouseOverPopup);
						
						layer.id = $scope.selectedRow;
						layer.outlinesId = $scope.outlinesId;
						layer.name = $scope.outlineName; 
						layer.coordinateList = bounds;
						layer.colorCode = $scope.colorCode;
						layer.geometryType = $scope.geometryType;
						layer.wallType = $scope.wallType; 
						layer.menuType = "Building";
						layer.enableEdit();
						
						$(".leaflet-vertex-icon")[index].style.background = "red"

					}
					var selectId = null;
					$scope.editOutline = function() {

						if (!$scope.isEdit) {
							$scope.drawControl.remove();
							$scope.drawControlEdit.addTo(map);

							$("#btnDelete").hide();
							$("#outlineEditBtn").text('Update');
							if (selectId != null) {
								map.eachLayer(function(layer) {
									if (selectId == layer.id) {

										layer.enableEdit();

										$scope.outlineName = $("#outlinename")
												.val();

									} else {
										if (layer.editor !== undefined) {
											layer.editor.editLayer
													.clearLayers()
										}
									}

								});
							} else {
								map
										.eachLayer(function(layer) {
											if (layer.id !== undefined) {
												if ($scope.selectedRow != null
														&& $scope.selectedRow == layer.id) {

													layer.enableEdit();
													map.closePopup();
												} else {
													if (layer.editor !== undefined) {
														layer.editor.editLayer
																.clearLayers()
													}
												}
											}

										});
							}
							$scope.isEdit = !$scope.isEdit;
						} else {
							if ($scope.outlineName == undefined
									|| $scope.outlineName == '') {
								$scope.error = true;
								$scope.alert_error_msg = 'Enter Area name';
								$timeout(function() {
									$scope.error = false;
								}, 1500);
								return;
							} else {
								$scope.saveOutlines();
							}
						}

					}

					
					// return true if overlap
					function checkGridOverlap(gridPoints) {

						var array = [];

						for (var k = 0; k < gridPoints.length; k++) {
							array.push([ parseInt(gridPoints[k].x),
									parseInt(gridPoints[k].y) ]);
						}
						array.push([ parseInt(gridPoints[0].x),
								parseInt(gridPoints[0].y) ]);

						var poly1 = turf.polygon([ array ]);

						var vertices = $scope.outlineVertices;
						var bounds1 = [];
						for (var j = 0; j < vertices.length; j++) {
							bounds1.push([ parseInt(vertices[j].x),
									parseInt(vertices[j].y) ]);
						}

						bounds1.push([ parseInt(vertices[0].x),
								parseInt(vertices[0].y) ]);

						var poly2 = turf.polygon([ bounds1 ]);
						if (turf.booleanOverlap(poly1, poly2)
								|| turf.booleanContains(poly1, poly2)
								|| turf.booleanContains(poly2, poly1)) {
							return true;
						}

						return false;
					}

					$scope.getGridList = function() {
						var gridList = [];

						for ( var gridItems in $scope.campusGridArray) {
							if (checkGridOverlap($scope.campusGridArray[gridItems])) {
								gridList.push(gridItems);
							}
						}

						console.log(gridList);
						return gridList;
					}
					
					$scope.deleteOutline = function() {
						if ($scope.outlinesId != '') {
							var outlineData = {
								"outlinesId" : $scope.outlinesId
							};
							$
									.confirm({
										title : $rootScope.MSGBOX_TITLE,
										content : 'Are you sure you want to delete this item?',
										type : 'blue',
										columnClass : 'medium',
										autoClose : false,
										icon : 'fa fa-info-circle',
										buttons : {
											yes : {
												text : 'Yes',
												btnClass : 'btn-blue',
												action : function() {
													$rootScope
															.showloading('#loadingBar');
													$http(
															{
																method : 'DELETE',
																url : './outlineDetails',
																headers : {
																	'Content-Type' : 'application/json'
																},
																data : outlineData
															})
															.then(
																	function success(
																			response) {
																		$rootScope
																				.hideloading('#loadingBar');
																		if (response != null
																				&& response.data != null
																				&& response.data != "BAD_REQUEST") {

																			if (response.data.status == 'success') {

																				$scope.alert_success_msg = 'Outline Deleted successfully.';

																				$timeout(
																						function() {
																							refreshOutlinedetails();
																						},
																						500);
																			} else {
																				$scope.error = true;
																				$scope.alert_error_msg = response.data.message;
																				$timeout(
																						function() {
																							$scope.error = false;
																						},
																						1500);
																			}
																		}
																	},
																	function error(response) {
																		$rootScope.hideloading('#loadingBar');
																		$rootScope.fnHttpError(response);
																	});
												}
											},
											no : function() {

											}
										}
									});

						}
					}
					
					$scope.editDoor = function() {

						
					 
						/*
						 * $("#campusDetailsDiv").hide();
						 * $("#selectedId").hide(); $("#selectedDoor").show();
						 */
						
						if (!$scope.isDoorEdit) {
							$scope.drawControl.remove();
							$scope.drawControlEdit.addTo(map);

							$("#btnDoorDelete").hide();
							$("#doorEditBtn").text('Update');
							if ($scope.doorId!="" && $scope.doorId != null) {
								map.eachLayer(function(layer) {
									if ($scope.doorId == layer.doorId) {

										layer.enableEdit();

										$scope.doorName = $("#doorName")
												.val();

									} else {
										if (layer.editor !== undefined) {
											layer.editor.editLayer
													.clearLayers()
										}
									}

								});
							}  else
							{
								map.eachLayer(function(layer) {
									if (layer.editor !== undefined) {
										layer.editor.editLayer
												.clearLayers()
									}

								});
							}
							$scope.isDoorEdit = !$scope.isDoorEdit;
						} else {
							if ($scope.doorName == undefined
									|| $scope.doorName == '') {
								$scope.error = true;
								$scope.alert_error_msg = 'Enter Door name';
								$timeout(function() {
									$scope.error = false;
								}, 1500);
								return;
							} else {
								$scope.saveDoors();
							}
						}

					}

					$scope.saveDoors = function()
					{
						 
						
						var doorData = {
								"outlinesId" : $scope.outlinesId,
								"name" : $scope.doorName,
								"colorCode" : $scope.doorColorCode,
								"coordinates" : $scope.doorVertices ,
								"wallType" : $scope.doorWallType 
							};
						
						if ($scope.doorId == '') {
							$http({
								method : 'POST',
								url : './doorsDetails' ,
								data : doorData
							}).then(
								function success(response) {
									$rootScope.hideloading('#loadingBar');
									if (response != null
											&& response.data != null
											&& response.data != "BAD_REQUEST") {
		
										if (response.data.status == 'success') {
											
											$scope.refreshDoors();
										}
									}
								},
								function error(response) {
									$rootScope.hideloading('#loadingBar');
									$rootScope.fnHttpError(response);
								});
						}else
						{
							doorData.id= $scope.doorId;
							 
							$http({
								method : 'PUT',
								url : './doorsDetails' ,
								data : doorData
							}).then(
								function success(response) {
									$rootScope.hideloading('#loadingBar');
									if (response != null
											&& response.data != null
											&& response.data != "BAD_REQUEST") {
		
										if (response.data.status == 'success') {
											
											$scope.refreshDoors(); 
										}
									}
								},
								function error(response) {
									$rootScope.hideloading('#loadingBar');
									$rootScope.fnHttpError(response);
								});
						}
					}
					
					$scope.saveWalls = function()
					{
						 
						
						var wallData = {
								"outlinesId" : $scope.outlinesId,
								"name" : $scope.wallName,
								"colorCode" : $scope.wallColorCode,
								"geometryType": $scope.geometryTypeWall,
								"coordinates" : $scope.wallVertices ,
								"wallType" : $scope.wallWallType  
							};
						
						if ($scope.wallId == '') {
							$http({
								method : 'POST',
								url : './wallsDetails' ,
								data : wallData
							}).then(
								function success(response) {
									$rootScope.hideloading('#loadingBar');
									if (response != null
											&& response.data != null
											&& response.data != "BAD_REQUEST") {
		
										if (response.data.status == 'success') { 
											$scope.refreshWalls(); 
										}
									}
								},
								function error(response) {
									$rootScope.hideloading('#loadingBar');
									$rootScope.fnHttpError(response);
								});
						}else
						{
							wallData.id= $scope.wallId;
							 
							$http({
								method : 'PUT',
								url : './wallsDetails' ,
								data : wallData
							}).then(
								function success(response) {
									$rootScope.hideloading('#loadingBar');
									if (response != null
											&& response.data != null
											&& response.data != "BAD_REQUEST") {
		
										if (response.data.status == 'success') { 
											$scope.refreshWalls();
										}
									}
								},
								function error(response) {
									$rootScope.hideloading('#loadingBar');
									$rootScope.fnHttpError(response);
								});
						}
					}
					
					
					$scope.refreshDoors = function ()
					{
						map.eachLayer(function(layer) {
							if (layer.menuType !== undefined && layer.menuType=="Door") {
								layer.remove();  
							}
						});    
						$scope.isDoorEdit = false;
						$scope.drawControl.remove();
						$scope.addmapDrawOptions()
						$("#selectedDoor").hide();
						$scope.getAllDoors();
					}
					
					$scope.refreshWalls = function ()
					{
						map.eachLayer(function(layer) {
							if (layer.menuType !== undefined && layer.menuType=="Wall") {
								layer.remove();  
							}
						});
						$scope.isWallEdit = false;
						$("#selectedWall").hide();
						$scope.drawControl.remove();
						$scope.addmapDrawOptions()
						$scope.getAllWalls();
					}
					
					
					$scope.refreshPartition = function ()
					{
						map.eachLayer(function(layer) {
							if (layer.menuType !== undefined && layer.menuType=="Partition") {
								layer.remove();  
							}
						});
						$scope.isPartitionEdit = false;
						$("#selectedPartition").hide();
						$scope.drawControl.remove();
						$scope.addmapDrawOptions()
						$scope.getAllPartitions();
					}
					
					$scope.savePartition = function()
					{
						 
						
						var partitionsData = {
								"outlinesId" : $scope.outlinesId,
								"name" : $scope.partitionName,
								"colorCode" : $scope.partitionColorCode,
								"geometryType": $scope.geometryTypePartition,
								"coordinates" : $scope.partitionVertices ,
								"wallType" : $scope.partitionWallType   
							};
						
						if ($scope.partitionId == '') {
							$http({
								method : 'POST',
								url : './partitionsDetails' ,
								data : partitionsData
							}).then(
								function success(response) {
									$rootScope.hideloading('#loadingBar');
									if (response != null
											&& response.data != null
											&& response.data != "BAD_REQUEST") {
		
										if (response.data.status == 'success') { 
											$scope.refreshPartition(); 
										}
									}
								},
								function error(response) {
									$rootScope.hideloading('#loadingBar');
									$rootScope.fnHttpError(response);
								});
						}else
						{
							partitionsData.id= $scope.partitionId;
							 
							$http({
								method : 'PUT',
								url : './partitionsDetails' ,
								data : partitionsData
							}).then(
								function success(response) {
									$rootScope.hideloading('#loadingBar');
									if (response != null
											&& response.data != null
											&& response.data != "BAD_REQUEST") {
		
										if (response.data.status == 'success') { 
											$scope.refreshPartition();
										}
									}
								},
								function error(response) {
									$rootScope.hideloading('#loadingBar');
									$rootScope.fnHttpError(response);
								});
						}
					}
					
					$scope.deletePartition = function() {
						if ($scope.partitionId != '') {
							var data = {
								"id" : $scope.partitionId
							};
							$
									.confirm({
										title : $rootScope.MSGBOX_TITLE,
										content : 'Are you sure you want to delete this item?',
										type : 'blue',
										columnClass : 'medium',
										autoClose : false,
										icon : 'fa fa-info-circle',
										buttons : {
											yes : {
												text : 'Yes',
												btnClass : 'btn-blue',
												action : function() {
													$rootScope
															.showloading('#loadingBar');
													$http(
															{
																method : 'DELETE',
																url : './partitionsDetails',
																headers : {
																	'Content-Type' : 'application/json'
																},
																data : data
															})
															.then(
																	function success(
																			response) {
																		$rootScope
																				.hideloading('#loadingBar');
																		if (response != null
																				&& response.data != null
																				&& response.data != "BAD_REQUEST") {

																			if (response.data.status == 'success') {

																				$scope.alert_success_msg = 'Wall Deleted successfully.';

																				$timeout(
																						function() {
																							
																							$scope.refreshPartition();
																						},
																						500);
																			} else {
																				$scope.error = true;
																				$scope.alert_error_msg = response.data.message;
																				$timeout(
																						function() {
																							$scope.error = false;
																						},
																						1500);
																			}
																		}
																	},
																	function error(response) {
																		$rootScope.hideloading('#loadingBar');
																		$rootScope.fnHttpError(response);
																	});
												}
											},
											no : function() {

											}
										}
									});

						}else
						{
							$scope.error = true;
							$scope.alert_error_msg = "Please select the Partition";
							$timeout(
									function() {
										$scope.error = false;
									},
									1500);
						}
					}
					$scope.deletePathway = function() {
						if ($scope.pathwayId != '') {
							var data = {
								"pathwayId" : $scope.pathwayId
							};
							$
									.confirm({
										title : $rootScope.MSGBOX_TITLE,
										content : 'Are you sure you want to delete this item?',
										type : 'blue',
										columnClass : 'medium',
										autoClose : false,
										icon : 'fa fa-info-circle',
										buttons : {
											yes : {
												text : 'Yes',
												btnClass : 'btn-blue',
												action : function() {
													$rootScope
															.showloading('#loadingBar');
													$http(
															{
																method : 'DELETE',
																url : './pathWay',
																headers : {
																	'Content-Type' : 'application/json'
																},
																data : data
															})
															.then(
																	function success(
																			response) {
																		$rootScope
																				.hideloading('#loadingBar');
																		if (response != null
																				&& response.data != null
																				&& response.data != "BAD_REQUEST") {

																			if (response.data.status == 'success') {

																				$scope.alert_success_msg = 'Wall Deleted successfully.';

																				$timeout(
																						function() {
																							
																							$scope.refreshPathway();
																						},
																						500);
																			} else {
																				$scope.error = true;
																				$scope.alert_error_msg = response.data.message;
																				$timeout(
																						function() {
																							$scope.error = false;
																						},
																						1500);
																			}
																		}
																	},
																	function error(response) {
																		$rootScope.hideloading('#loadingBar');
																		$rootScope.fnHttpError(response);
																	});
												}
											},
											no : function() {

											}
										}
									});

						}else
						{
							$scope.error = true;
							$scope.alert_error_msg = "Please select the Pathway";
							$timeout(
									function() {
										$scope.error = false;
									},
									1500);
						}
					}
					$scope.refreshPathway = function ()
					{
						map.eachLayer(function(layer) {
							if (layer.menuType !== undefined && layer.menuType=="Pathway") {
								layer.remove();  
							}
						});
						$scope.isPathwayEdit = false;
						$("#selectedPathway").hide();
						$scope.drawControl.remove();
						$scope.addmapDrawOptions()
						$scope.getAllPathways();
					}
					
					$scope.savePathway = function()
					{
						 
						
						var pathwaysData = {
								"campusId" : $scope.campusId,
								"name" : $scope.pathwayName,
								"colorCode" : $scope.pathwayColorCode,
								"geometryType": $scope.geometryTypePathway,
								"coordinates" : $scope.pathwayVertices, 
								"pathwayPattern" : $scope.selectedPatternName, 
								"pathwayType" : $scope.pathwayType 
							};
						
						if ($scope.pathwayId == '') {
							$http({
								method : 'POST',
								url : './pathWay' ,
								data : pathwaysData
							}).then(
								function success(response) {
									$rootScope.hideloading('#loadingBar');
									if (response != null
											&& response.data != null
											&& response.data != "BAD_REQUEST") {
		
										if (response.data.status == 'success') { 
											$scope.refreshPathway(); 
										} else if (response.data.status == 'error'){
											$rootScope.alertErrorDialog(response.data.message);
										}
									}
								},
								function error(response) {
									$rootScope.hideloading('#loadingBar');
									$rootScope.fnHttpError(response);
								});
						}else
						{
							pathwaysData.id= $scope.pathwayId;
							 
							$http({
								method : 'PUT',
								url : './pathWay' ,
								data : pathwaysData
							}).then(
								function success(response) {
									$rootScope.hideloading('#loadingBar');
									if (response != null
											&& response.data != null
											&& response.data != "BAD_REQUEST") {
		
										if (response.data.status == 'success') { 
											$scope.refreshPathway();
										}
									}
								},
								function error(response) {
									$rootScope.hideloading('#loadingBar');
									$rootScope.fnHttpError(response);
								});
						}
					}
					$scope.saveOutlines = function() {

						if ($scope.geometryType == "") {
							$scope.geometryType = "polygon";
						}

						var outlineData = {
							"name" : $scope.outlineName,
							"geometryType" : $scope.geometryType,
							"coordinateList" : $scope.outlineVertices,
							"placeType" : $scope.placeType,
							"outlinesArea" : $scope.outlinesArea,
							"bgColor" : $scope.colorCode,
							"wallType" : $scope.wallType
						};

						console.log(outlineData);

						$scope.isEdit = false;
						$scope.success = false;

						if ($scope.outlinesId == '') {
							$http({
								method : 'POST',
								url : './outlineDetails/' + $scope.campusId,
								data : outlineData
							})
									.then(
											function success(response) {
												$rootScope
														.hideloading('#loadingBar');
												if (response != null
														&& response.data != null
														&& response.data != "BAD_REQUEST") {

													if (response.data.status == 'success') {

														// $scope.insertOutline(outlineData);
														$scope.outlineList[$scope.selectedRow].id = ''
																+ $scope.selectedRow;
														$scope.alert_success_msg = 'Outline added successfully.';

														$timeout(
																function() {
																	refreshOutlinedetails();
																}, 500);
													} else {
														$scope.error = true;
														$scope.alert_error_msg = response.data.message;
														$timeout(
																function() {
																	$scope.error = false;
																}, 1500);
													}
												}
											},
											function error(response) {
												$rootScope.hideloading('#loadingBar');
												$rootScope.fnHttpError(response);
											});

						} else {

							outlineData.outlinesId = $scope.outlinesId;

							$http({
								method : 'PUT',
								url : './outlineDetails/' + $scope.campusId,
								data : outlineData
							})
									.then(
											function success(response) {
												$rootScope
														.hideloading('#loadingBar');
												if (response != null
														&& response.data != null
														&& response.data != "BAD_REQUEST") {

													if (response.data.status == 'success') {

														$scope.alert_success_msg = 'Outline Updated successfully.';

														$timeout(
																function() {
																	refreshOutlinedetails();
																}, 500);
													} else {
														$scope.error = true;
														$scope.alert_error_msg = response.data.message;
														$timeout(
																function() {
																	$scope.error = false;
																}, 1500);
													}

												}
											},
											function error(response) {
												$rootScope.hideloading('#loadingBar');
												$rootScope.fnHttpError(response);
											});

						}

					}

					function refreshOutlinedetails() {

						$scope.showOutlineList = true;

						$scope.outlineList[$scope.selectedRow].name = $scope.outlineName;
						$scope.outlineList[$scope.selectedRow].colorCode = $scope.colorCode;
						$scope.outlineList[$scope.selectedRow].geometryType = $scope.geometryType;
						$scope.outlineList[$scope.selectedRow].coordinateList = $scope.outlineVertices;
						$scope.outlineList[$scope.selectedRow].placeType = $scope.placeType;
						$scope.outlineList[$scope.selectedRow].wallType = $scope.wallType;
						$scope.outlineList[$scope.selectedRow].outlinesArea = $scope.outlinesArea;
						$scope.outlineList[$scope.selectedRow].wallWidth = parseInt($scope.wallWidth);

						$scope.selectedRow = null;
						$scope.cancelEditOutline();
						$timeout(function() {
							$scope.success = false;
							$scope.drawOutlineDetails();

							$scope.outlineType = "Tracking Outline";
							$scope.wallType = "Normal";
							getAllOutlinedetails();
						}, 1000);
					}

					function getAllOutlinedetails() {
						map.eachLayer(function(layer) {
							if (layer.id !== undefined) {
								layer.remove();
							}
						});

						$http({
							method : 'GET',
							url : './outlineDetails/' + $scope.campusId
						})
								.then(
										function success(response) {
											$rootScope
													.hideloading('#loadingBar');
											if (response != null
													&& response.data != null
													&& response.data != "BAD_REQUEST") {

												$scope.outlineList.length = 0;
												if (response.data.status == 'success') {
													for (var i = 0; i < response.data.data.length; i++) {
														if (response.data.data[i].active)
															$scope.outlineList
																	.push(response.data.data[i]);
													}
													var item = $scope.treeData[0];

													var itemNode = [];

													if ($scope.outlineList != null
															&& $scope.outlineList.length > 0) {

														for (var i = 0; i < $scope.outlineList.length; i++) {

															itemNode[i] = {
																text : $scope.outlineList[i].name
															};
														}

													}

													item.nodes = itemNode;

													$scope.treeData[0] = item;

													$scope.loadTreeView();

													if ($scope.outlineList.length != 0) {
														for (var i = 0; i < $scope.outlineList.length; i++) {
															if ($scope.outlineList[i].active) {
																$scope.outlineList[i].area = $scope
																		.area($scope.outlineList[i].coordinateList);
																var bounds = getBoundsArray($scope.outlineList[i].coordinateList); ;

																$scope.colorCode = $scope.outlineList[i].bgColor;
																var layer = null;
																var theme = {
																		color : $scope.colorCode,
																		weight : getWallWeight($scope.outlineList[i].wallType),
																		fillColor : $scope.colorCode,
																		fillOpacity : 0.5
																	};
																
																if ($scope.outlineList[i].geometryType == undefined
																		|| $scope.outlineList[i].geometryType == null
																		|| $scope.outlineList[i].geometryType == "polygon"
																		|| $scope.outlineList[i].geometryType == "") {
 
																	$scope.outlineList[i].geometryType = "polygon";
																	layer = L.polygon(bounds, theme); 
																} else { 
																	layer = L.rectangle(bounds, theme); 
																}
																

																layer.addTo(map)
																	.addTo($scope.editableLayers)
																	.on('click', onClick)
																	.on('mouseover', mouseOverPopup);
																
																$scope.outlinesArea = $scope
																.area(editlatLngToPixels(layer
																		.getLatLngs()));
																
																layer.id = i;
																layer.outlinesId = $scope.outlineList[i].outlinesId;
																layer.name = $scope.outlineList[i].name;
																layer.outlinesArea = $scope.outlinesArea;
																layer.colorCode = $scope.colorCode; 
																layer.menuType = "Building";
																layer.wallType = $scope.outlineList[i].wallType;
																layer.palceType = $scope.outlineList[i].placeType;
																layer.geometryType = $scope.outlineList[i].geometryType;
																layer.coordinateList = bounds;

															}
														}
													} else {
														$scope.noList = true;
														$scope.valueList = false;
														$scope.errorMsg = "No outlines details are available";
														$timeout(
																function() {
																	$scope.error = false;
																}, 1500);
													}

													$scope.getAllDoors();
													$scope.getAllWalls(); 
													$scope.getAllPartitions();
													$scope.getAllPathways();
												} else {
													$scope.error = true;
													$scope.alert_error_msg = response.data.message;
													$timeout(function() {
														$scope.error = false;
													}, 1500);
												}

											}
										},
										function error(response) {
											$rootScope.hideloading('#loadingBar');
											$rootScope.fnHttpError(response);
										});
					}
					
					
					$scope.getAllPathways = function()
					{ 
						 
						 
						$scope.isPathwayEdit = false; 
						
						$http({
							method : 'GET',
							url : './pathWay'
						})
								.then(
										function success(response) {
											$rootScope
											.hideloading('#loadingBar');
									if (response != null
											&& response.data != null
											&& response.data != "BAD_REQUEST") {

										
										if (response.data.status == 'success') {
											$scope.addPathwayIntoMap(response.data.data);
										}
									}
										},
										function error(response) {
											$rootScope.hideloading('#loadingBar');
											$rootScope.fnHttpError(response);
										});
					
					
					}
					
					$scope.addPathwayIntoMap = function(pathWayData)
					{
						$scope.pathwayList.length = 0;
						for (var i = 0; i < pathWayData.length; i++) {
							if (pathWayData[i].active)
								$scope.pathwayList
										.push(pathWayData[i]);
						}
						
						if ($scope.pathwayList.length != 0) {
							for (var i = 0; i < $scope.pathwayList.length; i++) {
								
								var bounds = getBoundsArray($scope.pathwayList[i].coordinates);  
								var layer = null;
								var theme = {
										color : $scope.pathwayList[i].colorCode,
										weight : 3,
										fillColor : $scope.pathwayList[i].colorCode,
										fill : 'url('+getPathwayPattern($scope.pathwayList[i].pathwayPattern)+')' 
									};
								
								if( $scope.pathwayList[i].geometryType=="polygon")
								{ 
									layer= L.polygon(bounds, theme);  
								}else  
								{
									layer = L.rectangle(bounds, theme);
								}
								   
							 	layer.addTo(map)
							 		.addTo($scope.editableLayers)
							 		.on('click', onClickPathway)
							 		.on('mouseover', mouseOverPopup);
								
								layer.menuType = "Pathway";
								layer.campusId=$scope.pathwayList[i].campusDetails.campusId;
								layer.pathwayId = $scope.pathwayList[i].id;
								layer.pathwayName = $scope.pathwayList[i].name; 
								layer.colorCode = $scope.pathwayList[i].colorCode; 
								layer.geometryType = $scope.pathwayList[i].geometryType; 
								layer.pathwayType = $scope.pathwayList[i].pathwayType;
								layer.pathwayPattern = $scope.pathwayList[i].pathwayPattern;
								layer.coordinateList = bounds; 
								
								 
							}
						}
						
					
					}
					$scope.getAllPartitions = function()
					{ 
						 
						$scope.isPartitionEdit = false; 
						
						$http({
							method : 'GET',
							url : './partitionsDetails'
						})
								.then(
										function success(response) {
											$rootScope
											.hideloading('#loadingBar');
									if (response != null
											&& response.data != null
											&& response.data != "BAD_REQUEST") {

										
										if (response.data.status == 'success') {
											$scope.addPartitionIntoMap( response.data.data);
										}
									}
										},
										function error(response) {
											$rootScope.hideloading('#loadingBar');
											$rootScope.fnHttpError(response);
										});
					
					
					}
					
					$scope.addPartitionIntoMap = function(partitionData)	{
						$scope.partitionList.length = 0;
						for (var i = 0; i < partitionData.length; i++) {
							if (partitionData[i].active)
								$scope.partitionList
										.push(partitionData[i]);
						}
						
						if ($scope.partitionList.length != 0) {
							for (var i = 0; i < $scope.partitionList.length; i++) {
								
								var bounds =  getBoundsArray($scope.partitionList[i].coordinates);   
								var layer = null;
								var theme = {
										color : $scope.partitionList[i].colorCode,
										weight : getWallWeight($scope.partitionList[i].wallType),
										fillColor : $scope.partitionList[i].colorCode,
										fillOpacity : 0
									};
								
								if( $scope.partitionList[i].geometryType=="polyline")
								{  
									 layer = L.polyline(bounds, theme);
								}else if( $scope.partitionList[i].geometryType=="polygon")
								{ 
									layer= L.polygon(bounds, theme);  
								}else  
								{
									layer = L.rectangle(bounds, theme);
								}
								  

								layer.addTo(map)
									.addTo($scope.editableLayers)
									.on('click', onClickPartition)
									.on('mouseover', mouseOverPopup);
								
								layer.outlinesId = $scope.partitionList[i].outlinesDetails.outlinesId;
								layer.menuType = "Partition";
								layer.partitionId = $scope.partitionList[i].id;
								layer.partitionName = $scope.partitionList[i].name; 
								layer.colorCode = $scope.partitionList[i].colorCode; 
								layer.geometryType = $scope.partitionList[i].geometryType;
								layer.partitionWallType = $scope.partitionList[i].wallType;  
								layer.coordinateList = bounds; 
								
								 
							}
						}
						
					
					}
					$scope.getAllWalls = function()
					{ 
						$scope.isWallEdit = false; 
						
						$http({
							method : 'GET',
							url : './wallsDetails'
						})
								.then(
										function success(response) {
											$rootScope
											.hideloading('#loadingBar');
									if (response != null
											&& response.data != null
											&& response.data != "BAD_REQUEST") {

										
										if (response.data.status == 'success') {
											$scope.addWallIntoMap( response.data.data);
											
										}
									}
										},
										function error(response) {
											$rootScope.hideloading('#loadingBar');
											$rootScope.fnHttpError(response);
										});
					
					}
					$scope.addWallIntoMap = function(wallData){
						$scope.wallList.length = 0;
						for (var i = 0; i < wallData.length; i++) {
							if (wallData[i].active)
								$scope.wallList
										.push(wallData[i]);
						}
						
						if ($scope.wallList.length != 0) {
							for (var i = 0; i < $scope.wallList.length; i++) {
								
								var bounds = getBoundsArray($scope.wallList[i].coordinates);  
								var layer = null;
								var theme = {
										color : $scope.wallList[i].colorCode,
										weight : getWallWeight($scope.wallList[i].wallType),
										fillColor : $scope.wallList[i].colorCode,
										fillOpacity : 0
									};
								if( $scope.wallList[i].geometryType=="polyline")
								{ 
									 layer = L.polyline(bounds,theme);
								}else if( $scope.wallList[i].geometryType=="polygon")
								{ 
									layer= L.polygon(bounds,theme); 
									
								}else  
								{
									layer = L.rectangle(bounds, theme);
								}
								  

								layer.addTo(map)
									.addTo($scope.editableLayers)
									.on('click', onClickWall)
									.on('mouseover', mouseOverPopup);
								
								layer.outlinesId = $scope.wallList[i].outlinesDetails.outlinesId;
								layer.menuType = "Wall";
								layer.wallId = $scope.wallList[i].id;
								layer.name = $scope.wallList[i].name; 
								layer.colorCode = $scope.wallList[i].colorCode; 
								layer.geometryType = $scope.wallList[i].geometryType; 
								layer.wallWallType = $scope.wallList[i].wallType;  
								layer.coordinateList = bounds; 
								
								 
							}
						}
						
					
					}
					$scope.getAllDoors = function()
					{
						
						
						$http({
							method : 'GET',
							url : './doorsDetails'
						})
								.then(
										function success(response) {
											$rootScope
											.hideloading('#loadingBar');
									if (response != null
											&& response.data != null
											&& response.data != "BAD_REQUEST") {
										if (response.data.status == 'success') {
										$scope.addDoorIntoMap( response.data.data);
										}
									}
										},
										function error(response) {
											$rootScope.hideloading('#loadingBar');
											$rootScope.fnHttpError(response);
										});
					}
					$scope.addDoorIntoMap = function(doorData)
					{


						$scope.doorsList.length = 0;
						
							for (var i = 0; i < doorData.length; i++) {
								if (doorData[i].active)
									$scope.doorsList
											.push(doorData[i]);
							}
							
							if ($scope.doorsList.length != 0) {
								for (var i = 0; i < $scope.doorsList.length; i++) {
									var bounds = getBoundsArray($scope.doorsList[i].coordinates);    
									var layer = L
											.polyline(
													bounds,
													{
														color : $scope.doorsList[i].colorCode,
														weight : getWallWeight($scope.doorsList[i].wallType),
														fillColor : $scope.doorsList[i].colorCode,
														fillOpacity : 0.5
													})
											.addTo(map)
											.addTo($scope.editableLayers)
											.on('click', onDoorClick)
											.on('mouseover', mouseOverPopup);

									 
									
									layer.outlinesId = $scope.doorsList[i].outlinesDetails.outlinesId;
									layer.menuType = "Door";
									layer.doorId = $scope.doorsList[i].id;
									layer.doorName = $scope.doorsList[i].name; 
									layer.doorColorCode = $scope.doorsList[i].colorCode; 
									layer.doorWallType = $scope.doorsList[i].wallType;  
									layer.geometryType = "Line"; 
									layer.coordinateList = bounds;
									
									 
								}
							}
							
						
					
					}
					
					$scope.getTheme = function()
					{
						var theme = {
							color : $scope.doorsList[i].colorCode,
							weight : getWallWeight($scope.doorsList[i].wallType),
							fillColor : $scope.doorsList[i].colorCode,
							fillOpacity : 0.5
						};
						
						return theme;
					}
					
					$scope.cancelPartitionEdit = function()
					{
						$scope.drawControlEdit.remove();
						$scope.drawControl.addTo(map);
						$scope.isPartitionEdit = false; 
						$("#selectedPartition").hide();
						
						map.closePopup();
						map.eachLayer(function(layer) {

							if (layer.menuType !== undefined && layer.menuType=="Partition") { 
								if (layer.partitionId !== undefined
										&& ( layer.partitionId == '' || layer.partitionId == $scope.partitionId) ) {
									layer.remove(); 
								} else
								{
									if (layer.partitionId == $scope.partitionId)
									{
										if (layer.editor !== undefined) {
											layer.editor.editLayer.clearLayers()
										}
									}
									
								}  
							}

						});
						
						if ($scope.partitionId !="") { 
							
							 var i = getPosByPartitionId($scope.partitionId);  
							var bounds = getBoundsArray($scope.partitionList[i].coordinates);  
							var layer = null;
							var theme= {
									color : $scope.partitionList[i].colorCode, 
									weight : getWallWeight($scope.partitionList[i].wallType),
									fillColor : $scope.partitionList[i].colorCode,
									fillOpacity : 0
								};
							if( $scope.partitionList[i].geometryType=="polyline")
							{  
								 layer = L.polyline(bounds,theme);
							}else if( $scope.partitionList[i].geometryType=="polygon")
							{ 
								layer= L.polygon(bounds, theme);  
							}else  
							{
								layer = L.rectangle(bounds, theme);
							}
							  

							layer.addTo(map)
								.addTo($scope.editableLayers)
								.on('click', onClickPartition)
								.on('mouseover', mouseOverPopup);
							
							layer.outlinesId = $scope.partitionList[i].outlinesDetails.outlinesId;
							layer.menuType = "Partition";
							layer.partitionId = $scope.partitionList[i].id;
							layer.partitionName = $scope.partitionList[i].name; 
							layer.colorCode = $scope.partitionList[i].colorCode; 
							layer.geometryType = $scope.partitionList[i].geometryType; 
							layer.partitionWallType = $scope.partitionList[i].wallType; 
							layer.coordinateList = bounds;   
							 
						}
					}
					
					$scope.cancelPathwayEdit = function()
					{
						$scope.drawControlEdit.remove();
						$scope.drawControl.addTo(map);
						$scope.isPathwayEdit = false; 
						$("#selectedPathway").hide();
						
						map.closePopup();
						map.eachLayer(function(layer) {

							if (layer.menuType !== undefined && layer.menuType=="Pathway") { 
								if (layer.pathwayId !== undefined
										&& ( layer.pathwayId == '' || layer.pathwayId == $scope.pathwayId )) {
									layer.remove(); 
								} else
								{
									if (layer.pathwayId == $scope.pathwayId)
									{
										if (layer.editor !== undefined) {
											layer.editor.editLayer.clearLayers()
										}
									}
									
								}  
							}

						});
						
						if ($scope.pathwayId !="") { 
													
							 var i = getPosByPathwayId($scope.pathwayId);
							 
 
							var bounds = getBoundsArray($scope.pathwayList[i].coordinates);  
							var layer = null;
							var theme = {
									color : $scope.pathwayList[i].colorCode,
									weight : 3, 
									fillColor : $scope.pathwayList[i].colorCode,
									fill : 'url('+getPathwayPattern($scope.pathwayList[i].pathwayPattern)+')' 
									
								};
							
							if( $scope.pathwayList[i].geometryType=="polygon")
							{ 
								layer= L.polygon(bounds, theme);  
							}else  
							{
								layer = L.rectangle(bounds, theme);
							}
							  

						 	layer.addTo(map)
						 		.addTo($scope.editableLayers)
						 		.on('click', onClickPathway)
						 		.on('mouseover', mouseOverPopup);
							
							layer.menuType = "Pathway";
							layer.campusId=$scope.pathwayList[i].campusDetails.campusId;
							layer.pathwayId = $scope.pathwayList[i].id;
							layer.pathwayName = $scope.pathwayList[i].name; 
							layer.colorCode = $scope.pathwayList[i].colorCode; 
							layer.geometryType = $scope.pathwayList[i].geometryType; 
							layer.pathwayType = $scope.pathwayList[i].pathwayType; 
							layer.pathwayPattern = $scope.pathwayList[i].pathwayPattern; 
							layer.coordinateList = bounds;  
													 
						}
					}
					
					$scope.cancelWallEdit  = function() {
						$scope.drawControlEdit.remove();
						$scope.drawControl.addTo(map);
						$scope.isWallEdit = false; 
						$("#selectedWall").hide();
						
						map.closePopup();
						map.eachLayer(function(layer) {

							if (layer.menuType !== undefined && layer.menuType=="Wall") { 
								if (layer.wallId !== undefined
										&& ( layer.wallId == '' || layer.wallId == $scope.wallId )) {
									layer.remove(); 
								} else
								{
									if (layer.wallId == $scope.wallId)
									{
										if (layer.editor !== undefined) {
											layer.editor.editLayer.clearLayers()
										}
									}
									
								}  
							}

						});
						
						if ($scope.wallId !="") { 
							
							var i = getPosByWallId($scope.wallId);
							 
							
							var bounds = getBoundsArray($scope.wallList[i].coordinates);   
							var theme = {
									color : $scope.wallList[i].colorCode,
									weight : getWallWeight($scope.wallList[i].wallType),
									fillColor : $scope.wallList[i].colorCode,
									fillOpacity : 0
								};
							
							var layer = null;
							if( $scope.wallList[i].geometryType=="polyline")
							{  
								 layer = L.polyline(bounds, theme);
							}else if( $scope.wallList[i].geometryType=="polygon")
							{ 
								layer= L.polygon(bounds, theme);  
							}else  
							{
								layer = L.rectangle(bounds,theme);
							}
							  

							layer.addTo(map)
								.addTo($scope.editableLayers)
								.on('click', onClickWall)
								.on('mouseover', mouseOverPopup);
							
							layer.outlinesId = $scope.wallList[i].outlinesDetails.outlinesId;
							layer.menuType = "Wall";
							layer.wallId = $scope.wallList[i].id;
							layer.name = $scope.wallList[i].name; 
							layer.colorCode = $scope.wallList[i].colorCode; 
							layer.geometryType = $scope.wallList[i].geometryType; 
							layer.wallWallType = $scope.wallList[i].wallType; 
							layer.coordinateList = bounds;   
						
						}
						
					}
					
					
					$scope.cancelDoorEdit  = function() {
						
						
						$scope.drawControlEdit.remove();
						$scope.drawControl.addTo(map);
						$scope.isDoorEdit = false; 
						$("#selectedDoor").hide();
						
						map.closePopup();
						map.eachLayer(function(layer) {

							if (layer.menuType !== undefined && layer.menuType=="Door") { 
								if (layer.doorId !== undefined
										&& (layer.doorId == '' || layer.doorId == $scope.doorId )) {
									layer.remove(); 
								} else {
									if (layer.editor !== undefined) {
										layer.editor.editLayer.clearLayers()
									}
								}  
							}

						});
								
						if ($scope.doorId !="") {
							
							
							var i = getPosByDoorId($scope.doorId);
							
							var bounds = getBoundsArray($scope.doorsList[i].coordinates);   
							 
							var layer = L
									.polyline(
											bounds,
											{
												color : $scope.doorsList[i].colorCode,
												weight : getWallWeight($scope.doorsList[i].wallType),
												fillColor : $scope.doorsList[i].colorCode,
												fillOpacity : 0.5
											})
									.addTo(map)
									.addTo($scope.editableLayers)
									.on('click', onDoorClick)
									.on('mouseover', mouseOverPopup);
 
							layer.outlinesId = $scope.doorsList[i].outlinesDetails.outlinesId;
							layer.menuType = "Door";
							layer.doorId = $scope.doorsList[i].id;
							layer.doorName = $scope.doorsList[i].name; 
							layer.doorColorCode = $scope.doorsList[i].colorCode; 
							layer.doorWallType = $scope.doorsList[i].wallType; 
							layer.geometryType = "Line"; 
							layer.coordinateList = bounds;
						} 
					}
  
					$scope.editWall = function() {

						 
						/*
						 * $("#campusDetailsDiv").hide();
						 * $("#selectedId").hide(); $("#selectedDoor").show();
						 */
						
						if (!$scope.isWallEdit) {
							$scope.drawControl.remove();
							$scope.drawControlEdit.addTo(map);

							$("#btnWallDelete").hide();
							$("#wallEditBtn").text('Update');
							if ($scope.wallId!="" && $scope.wallId != null) {
								map.eachLayer(function(layer) {
									if ($scope.wallId == layer.wallId) {

										layer.enableEdit();

										$scope.wallName = $("#wallName")
												.val();

									} else {
										if (layer.editor !== undefined) {
											layer.editor.editLayer
													.clearLayers()
										}
									}

								});
							}  else
							{
								map.eachLayer(function(layer) {
									if (layer.editor !== undefined) {
										layer.editor.editLayer
												.clearLayers()
									}

								});
							}
							$scope.isWallEdit = !$scope.isWallEdit;
						} else {
							if ($scope.wallName == undefined
									|| $scope.wallName == '') {
								$scope.error = true;
								$scope.alert_error_msg = 'Enter Wall name';
								$timeout(function() {
									$scope.error = false;
								}, 1500);
								return;
							} else {
								$scope.saveWalls();
							}
						}

					}
					
					$scope.editPartition = function() {

						 
						/*
						 * $("#campusDetailsDiv").hide();
						 * $("#selectedId").hide(); $("#selectedDoor").show();
						 */
						
						if (!$scope.isPartitionEdit) {
							$scope.drawControl.remove();
							$scope.drawControlEdit.addTo(map);

							$("#btnPartitionDelete").hide();
							$("#partitionEditBtn").text('Update');
							if ($scope.partitionId!="" && $scope.partitionId != null) {
								map.eachLayer(function(layer) {
									if ($scope.partitionId == layer.partitionId) {

										layer.enableEdit();

										$scope.partitionName = $("#partitionName")
												.val();

									} else {
										if (layer.editor !== undefined) {
											layer.editor.editLayer
													.clearLayers()
										}
									}

								});
							}  else
							{
								map.eachLayer(function(layer) {
									if (layer.editor !== undefined) {
										layer.editor.editLayer
												.clearLayers()
									}

								});
							}
							$scope.isPartitionEdit = !$scope.isPartitionEdit;
						} else {
							if ($scope.partitionName == undefined
									|| $scope.partitionName == '') {
								$scope.error = true;
								$scope.alert_error_msg = 'Enter Wall name';
								$timeout(function() {
									$scope.error = false;
								}, 1500);
								return;
							} else {
								$scope.savePartition();
							}
						}

					}
					$scope.editPathway = function() {

						 
						/*
						 * $("#campusDetailsDiv").hide();
						 * $("#selectedId").hide(); $("#selectedDoor").show();
						 */
						
						if (!$scope.isPathwayEdit) {
							$scope.drawControl.remove();
							$scope.drawControlEdit.addTo(map);

							$("#btnPathwayDelete").hide();
							$("#pathwayEditBtn").text('Update');
							if ($scope.pathwayId!="" && $scope.pathwayId != null) {
								map.eachLayer(function(layer) {
									if ($scope.pathwayId == layer.pathwayId) {

										layer.enableEdit();

										$scope.pathwayName = $("#pathwayName")
												.val();

									} else {
										if (layer.editor !== undefined) {
											layer.editor.editLayer
													.clearLayers()
										}
									}

								});
							}  else
							{
								map.eachLayer(function(layer) {
									if (layer.editor !== undefined) {
										layer.editor.editLayer
												.clearLayers()
									}

								});
							}
							$scope.isPathwayEdit = !$scope.isPathwayEdit;
						} else {
							if ($scope.pathwayName == undefined
									|| $scope.pathwayName == '') {
								$scope.error = true;
								$scope.alert_error_msg = 'Enter Pathway name';
								$timeout(function() {
									$scope.error = false;
								}, 1500);
								return;
							} else {
								$scope.savePathway();
							}
						}

					}
					
					$scope.deleteDoor = function() {
						if ($scope.doorId != '') {
							var wallsData = {
								"id" : $scope.doorId
							};
							$
									.confirm({
										title : $rootScope.MSGBOX_TITLE,
										content : 'Are you sure you want to delete this item?',
										type : 'blue',
										columnClass : 'medium',
										autoClose : false,
										icon : 'fa fa-info-circle',
										buttons : {
											yes : {
												text : 'Yes',
												btnClass : 'btn-blue',
												action : function() {
													$rootScope
															.showloading('#loadingBar');
													$http(
															{
																method : 'DELETE',
																url : './doorsDetails',
																headers : {
																	'Content-Type' : 'application/json'
																},
																data : wallsData
															})
															.then(
																	function success(
																			response) {
																		$rootScope.hideloading('#loadingBar');
																		if (response != null
																				&& response.data != null
																				&& response.data != "BAD_REQUEST") {

																			if (response.data.status == 'success') {

																				$scope.alert_success_msg = 'Door Deleted successfully.';

																				$timeout(
																						function() {
																							
																							$scope.refreshDoors();
																						},
																						500);
																			} else {
																				$scope.error = true;
																				$scope.alert_error_msg = response.data.message;
																				$timeout(
																						function() {
																							$scope.error = false;
																						},
																						1500);
																			}
																		}
																	},
																	function error(response) {
																		$rootScope.hideloading('#loadingBar');
																		$rootScope.fnHttpError(response);
																	});
												}
											},
											no : function() {

											}
										}
									});

						}else
						{
							$scope.error = true;
							$scope.alert_error_msg = "Please select the Door";
							$timeout(
									function() {
										$scope.error = false;
									},
									1500);
						}
					}
					
					$scope.deleteWall = function() {
						if ($scope.wallId != '') {
							var wallsData = {
								"id" : $scope.wallId
							};
							$
									.confirm({
										title : $rootScope.MSGBOX_TITLE,
										content : 'Are you sure you want to delete this item?',
										type : 'blue',
										columnClass : 'medium',
										autoClose : false,
										icon : 'fa fa-info-circle',
										buttons : {
											yes : {
												text : 'Yes',
												btnClass : 'btn-blue',
												action : function() {
													$rootScope
															.showloading('#loadingBar');
													$http(
															{
																method : 'DELETE',
																url : './wallsDetails',
																headers : {
																	'Content-Type' : 'application/json'
																},
																data : wallsData
															})
															.then(
																	function success(
																			response) {
																		$rootScope
																				.hideloading('#loadingBar');
																		if (response != null
																				&& response.data != null
																				&& response.data != "BAD_REQUEST") {

																			if (response.data.status == 'success') {

																				$scope.alert_success_msg = 'Wall Deleted successfully.';

																				$timeout(
																						function() {
																							
																							$scope.refreshWalls();
																						},
																						500);
																			} else {
																				$scope.error = true;
																				$scope.alert_error_msg = response.data.message;
																				$timeout(
																						function() {
																							$scope.error = false;
																						},
																						1500);
																			}
																		}
																	},
																	function error(response) {
																		$rootScope.hideloading('#loadingBar');
																		$rootScope.fnHttpError(response);
																	});
												}
											},
											no : function() {

											}
										}
									});

						}else
						{
							$scope.error = true;
							$scope.alert_error_msg = "Please select the Wall";
							$timeout(
									function() {
										$scope.error = false;
									},
									1500);
						}
					}
					
					$scope.cancelEditOutline = function() {
 

						$scope.drawControlEdit.remove();
						$scope.drawControl.addTo(map);
 
						$scope.isEdit = false;

						$("#selectedId").hide();

						map.closePopup();
						map.eachLayer(function(layer) {

							if (layer.menuType !== undefined && layer.menuType=="Building") {  
								if (layer.outlinesId !== undefined
										&& (layer.outlinesId == '' || layer.outlinesId == $scope.outlinesId)) {
									layer.remove();
									if(layer.outlinesId == '')
										$scope.outlineList.splice(layer.id, 1);
								} 
							}

						});

						if ($scope.selectedRow != null) {
							var outline = $scope.outlineList[$scope.selectedRow];
							// outline.vertices = JSON.parse(
							// $("#outlineDimension").val());
							if (outline != undefined) {
								var bounds = getBoundsArray(outline.coordinateList);   
								var layer = null;
								var theme = {
										color : outline.bgColor,
										weight : 2,
										fillColor : outline.bgColor,
										fillOpacity : 0.5
									};
								if (outline.geometryType == "polygon") { 
									layer = L.polygon(bounds, theme);  
								} else { 
									layer = L.rectangle(bounds, theme); 
								}
								
								layer.addTo(map).addTo($scope.editableLayers)
								.on('click', onClick)
								.on('mouseover', mouseOverPopup);

								$scope.outlinesArea = $scope
										.area(editlatLngToPixels(layer
												.getLatLngs()));

								layer.id = $scope.selectedRow;
								layer.outlinesId = outline.outlinesId;
								layer.name = outline.name;
								layer.outlinesArea = $scope.outlinesArea;
								layer.colorCode = outline.bgColor;
								layer.wallType = outline.wallType;
								layer.placeType = outline.placeType;
								layer.geometryType = outline.geometryType;
								layer.menuType = "Building";
								layer.coordinateList = bounds;
							}
						}

						$scope.outlinesId = '';
						$scope.outlineName = '';
						$scope.selectedRow = null;
						selectId = null;
						$scope.isDrawEvent = false;
					}

					$scope.drawOutlineDetails = function() {

						map.eachLayer(function(layer) {
							if (layer.id !== undefined) {
								layer.remove();
							}
						});

						if ($scope.outlineList.length != 0) {
							for (var i = 0; i < $scope.outlineList.length; i++) {

								
								var bounds =  getBoundsArray($scope.outlineList[i].coordinateList);  
								var layer = null;
								var theme = {
										color : $scope.outlineList[i].colorCode,
										weight : $scope.outlineList[i].wallWidth,
										fillColor : $scope.outlineList[i].colorCode,
										fillOpacity : 0.5
									};
								
								if ($scope.outlineList[i].geometryType == undefined
										|| $scope.outlineList[i].geometryType == null
										|| $scope.outlineList[i].geometryType == "polygon"
										|| $scope.outlineList[i].geometryType == "") {
									 
									$scope.outlineList[i].geometryType = "polygon";
									layer = L.polygon(bounds, theme); 

								} else {  
									layer = L.rectangle(bounds, theme);  
								}
								
								layer.addTo(map).addTo($scope.editableLayers)
									.on('click', onClick)
									.on('mouseover', mouseOverPopup);
								
								$scope.outlinesArea = $scope.area(editlatLngToPixels(layer
												.getLatLngs())); 

								$scope.outlineList[i].area =  $scope.outlinesArea;
								
								layer.id = i;
								layer.outlinesId = $scope.outlineList[i].outlinesId;
								layer.name = $scope.outlineList[i].outlineName;
								layer.outlinesArea = $scope.outlinesArea;
								layer.colorCode = $scope.outlineList[i].colorCode;
								layer.menuType = "Building"; 
								layer.wallType = $scope.outlineList[i].wallType;
								layer.placeType = $scope.outlineList[i].placeType;
								layer.geometryType = $scope.outlineList[i].geometryType;
								layer.coordinateList = bounds;

							}
						} else {
							$scope.noList = true;
							$scope.valueList = false;
							$scope.errorMsg = "No outlines details are available";
						}

					}
					
					function onClickPathway(e)
					{

						if ($scope.selectedMenu == "Pathway") {
							if ($scope.isPathwayEdit) {
								var r;
								$
										.confirm({
											title : $rootScope.MSGBOX_TITLE,
											content : 'Are you sure you want to discard your changes?',
											type : 'blue',
											columnClass : 'medium',
											autoClose : false,
											icon : 'fa fa-info-circle',
											buttons : {
												yes : {
													text : 'Yes',
													btnClass : 'btn-blue',
													action : function() {
														r = true;
													}
												},
												no : function() {
													r = false;

												}
											}
										});
								if (r === false) {
									return false;
								} else {
									$scope.cancelPathwayEdit();
								}
							}else{
								$("#selectedId").hide();
								$("#selectedDoor").hide();
								$("#pathwayEditBtn").text('Edit');
								$scope.isWallEdit = false;
								$("#btnPathwayDelete").show();
								$("#campusDetailsDiv").hide(); 
								$("#selectedWall").hide();
								$("#selectedPathway").show(); 
								$("#idExportMapDetail").hide();
								$("#idImportMapDetail").hide();
								$("#idImportZoneDetail").hide();
								  
								$scope.pathwayColorCode = e.sourceTarget.colorCode; 
								$scope.pathwayName = e.sourceTarget.pathwayName;
								$scope.pathwayVertices = latLngToPixels1(e.sourceTarget.coordinateList);
								$scope.pathwayId = e.sourceTarget.pathwayId;
								$scope.campusId = e.sourceTarget.campusId;
								$scope.pathwayType = e.sourceTarget.pathwayType;
								$scope.geometryTypePathway = e.sourceTarget.geometryType;  
								$scope.selectedPatternName = e.sourceTarget.pathwayPattern;   
								setPatternValues();
								
								$scope.$apply();
								
								var pos = getPosByOutlineId($scope.outlinesId);

								$('#treeview').treeview('selectNode', pos + 1);
							}
						}
					
					}
					function onClickPartition(e)
					{

						if ($scope.selectedMenu == "Partition") {
							if ($scope.isPartitionEdit) {
								var r;
								$
										.confirm({
											title : $rootScope.MSGBOX_TITLE,
											content : 'Are you sure you want to discard your changes?',
											type : 'blue',
											columnClass : 'medium',
											autoClose : false,
											icon : 'fa fa-info-circle',
											buttons : {
												yes : {
													text : 'Yes',
													btnClass : 'btn-blue',
													action : function() {
														r = true;
													}
												},
												no : function() {
													r = false;

												}
											}
										});
								if (r === false) {
									return false;
								} else {
									$scope.cancelPartitionEdit();
								}
							}else{
								$("#selectedId").hide();
								$("#selectedDoor").hide();
								$("#partitionEditBtn").text('Edit');
								$scope.isWallEdit = false;
								$("#btnPartitionDelete").show();
								$("#campusDetailsDiv").hide(); 
								$("#selectedWall").hide();
								$("#selectedPartition").show(); 
								$("#selectedPathway").hide();
								$("#idExportMapDetail").hide();
								$("#idImportMapDetail").hide();
								$("#idImportZoneDetail").hide();  
								$scope.partitionColorCode = e.sourceTarget.colorCode; 
								$scope.partitionName = e.sourceTarget.partitionName;
								$scope.partitionVertices = latLngToPixels1(e.sourceTarget.coordinateList);
								$scope.partitionId = e.sourceTarget.partitionId;
								$scope.outlinesId = e.sourceTarget.outlinesId;
								$scope.geometryTypePartition = e.sourceTarget.geometryType;
								$scope.partitionWallType = e.sourceTarget.partitionWallType;
								    
								
								$scope.$apply();
								
								var pos = getPosByOutlineId($scope.outlinesId);

								$('#treeview').treeview('selectNode', pos + 1);
							}
						}
					
					}
					
					function onClickWall(e) {
						if ($scope.selectedMenu == "Wall") {
							if ($scope.isWallEdit) {
								var r;
								$
										.confirm({
											title : $rootScope.MSGBOX_TITLE,
											content : 'Are you sure you want to discard your changes?',
											type : 'blue',
											columnClass : 'medium',
											autoClose : false,
											icon : 'fa fa-info-circle',
											buttons : {
												yes : {
													text : 'Yes',
													btnClass : 'btn-blue',
													action : function() {
														r = true;
													}
												},
												no : function() {
													r = false;

												}
											}
										});
								if (r === false) {
									return false;
								} else {
									$scope.cancelWallEdit();
								}
							}else{
								$("#selectedId").hide();
								$("#selectedDoor").hide();
								$("#wallEditBtn").text('Edit');
								$scope.isWallEdit = false;
								$("#btnWallDelete").show();
								$("#campusDetailsDiv").hide();
								$("#btnDoorDelete").hide();
								$("#selectedWall").show();
								$("#selectedPartition").hide(); 
		   					    $("#selectedPathway").hide();
								$("#idExportMapDetail").hide();
								$("#idImportMapDetail").hide();
								$("#idImportZoneDetail").hide();
								  
								$scope.wallColorCode = e.sourceTarget.colorCode; 
								$scope.wallName = e.sourceTarget.name;
								$scope.wallVertices = latLngToPixels1(e.sourceTarget.coordinateList);
								$scope.wallId = e.sourceTarget.wallId;
								$scope.outlinesId = e.sourceTarget.outlinesId;
								$scope.geometryTypeWall = e.sourceTarget.geometryType;
								$scope.wallWallType = e.sourceTarget.wallWallType;
								     
								$scope.$apply();
								
								var pos = getPosByOutlineId($scope.outlinesId);

								$('#treeview').treeview('selectNode', pos + 1);
							}
						}
					}
					
					function onDoorClick(e) {
						if ($scope.selectedMenu == "Door") {
							
							if ($scope.isDoorEdit) {
								var r;
								$
										.confirm({
											title : $rootScope.MSGBOX_TITLE,
											content : 'Are you sure you want to discard your changes?',
											type : 'blue',
											columnClass : 'medium',
											autoClose : false,
											icon : 'fa fa-info-circle',
											buttons : {
												yes : {
													text : 'Yes',
													btnClass : 'btn-blue',
													action : function() {
														r = true;
													}
												},
												no : function() {
													r = false;

												}
											}
										});
								if (r === false) {
									return false;
								} else {
									$scope.cancelDoorEdit();
								}
							}else{

								$scope.isDoorEdit = false;
								$("#selectedId").hide();
								$("#selectedDoor").show();
								$("#doorEditBtn").text('Edit');
								$("#btnDoorDelete").show();
								$("#campusDetailsDiv").hide();
								$("#selectedPartition").hide(); 								
								$("#selectedPathway").hide();
								
								
								$scope.doorColorCode = e.sourceTarget.doorColorCode; 
								$scope.doorName = e.sourceTarget.doorName;
								$scope.doorVertices = latLngToPixels1(e.sourceTarget.coordinateList);
								$scope.doorId = e.sourceTarget.doorId;
								$scope.outlinesId = e.sourceTarget.outlinesId;
								$scope.doorWallType = e.sourceTarget.doorWallType;
								    
								
								$scope.$apply();
								
								var pos = getPosByOutlineId($scope.outlinesId);

								$('#treeview').treeview('selectNode', pos + 1); 
								 
							}
						}
					}
					
					function getPosByOutlineId(outlinesId)
					{
					
						for (var i = 0; i < $scope.outlineList.length; i++) {
							var id = $scope.outlineList[i].outlinesId;
							if (id != '' && id == outlinesId) {
								return i;
							}
						}
						
						return -1;
					}
					
					function getPosByDoorId(doorId)
					{
					
						for (var i = 0; i < $scope.doorsList.length; i++) {
							var id = $scope.doorsList[i].id;
							if (id != '' && id == doorId) {
								return i;
							}
						}
						
						return -1;
					}
					
					function getPosByWallId(wallId)
					{
					
						for (var i = 0; i < $scope.wallList.length; i++) {
							var id = $scope.wallList[i].id;
							if (id != '' && id == wallId) {
								return i;
							}
						}
						
						return -1;
					}
					 
					function getPosByPathwayId(pathwayId)
					{
					
						for (var i = 0; i < $scope.pathwayList.length; i++) {
							var id = $scope.pathwayList[i].id;
							if (id != '' && id == pathwayId) {
								return i;
							}
						}
						
						return -1;
					}
					
					function getPosByPartitionId(partitionId)
					{
					
						for (var i = 0; i < $scope.partitionList.length; i++) {
							var id = $scope.partitionList[i].id;
							if (id != '' && id == partitionId) {
								return i;
							}
						}
						
						return -1;
					}
					
					
					
					
					function onClick(e) {

						if ($scope.selectedMenu == "Building") {
							  
							 
							if ($scope.isEdit) {
								var r;
								$
										.confirm({
											title : $rootScope.MSGBOX_TITLE,
											content : 'Are you sure you want to discard your changes?',
											type : 'blue',
											columnClass : 'medium',
											autoClose : false,
											icon : 'fa fa-info-circle',
											buttons : {
												yes : {
													text : 'Yes',
													btnClass : 'btn-blue',
													action : function() {
														r = true;
													}
												},
												no : function() {
													r = false;
	
												}
											}
										});
								if (r === false) {
									return false;
								} else {
									$scope.cancelEditOutline();
								}
							}
							$("#selectedId").show();
							$("#outlineEditBtn").text('Edit');
							$scope.isEdit = false;
							$("#btnDelete").show();
							$("#campusDetailsDiv").hide(); 
							$("#selectedDoor").hide();
							$("#selectedWall").hide();
							$("#selectedPartition").hide(); 								
							$("#selectedPathway").hide();
							$("#idExportMapDetail").hide();
							$("#idImportMapDetail").hide();
							$("#idImportZoneDetail").hide();
							// $("#outlinename").val(e.sourceTarget.name);
							// $("#classification").val(e.sourceTarget.classification);
	
							var arrpoint = latLngToPixels(e.sourceTarget._latlngs);
							$scope.outlinesId = e.sourceTarget.outlinesId
							$scope.outlineVertices = arrpoint;
							$scope.outlinesArea = e.sourceTarget.outlinesArea;
	
							$scope.orignalOutlineVertices = [];
							for (var j = 0; j < $scope.outlineVertices.length; j++) {
								$scope.orignalOutlineVertices.push({
									'x' : $scope.outlineVertices[j].x,
									'y' : $scope.outlineVertices[j].y
								});
							}
							$("#outlinesId").val(e.sourceTarget.outlinesId);
	
							$scope.outlineName = e.sourceTarget.name;
							$scope.colorCode = e.sourceTarget.colorCode;
							$scope.wallType = e.sourceTarget.wallType;
							$scope.placeType = e.sourceTarget.placeType;
							$scope.geometryType = e.sourceTarget.geometryType;
							$scope.coordinateList = e.sourceTarget.coordinateList;
							$scope.$apply();
	
							map
									.eachLayer(function(layer) {
										if (layer.id !== undefined && layer.menuType == "Building") {
											if (e.sourceTarget.id == layer.id) {
												layer.setStyle({
													fillColor : layer.colorCode,
													fillOpacity : 0.8,
													color : layer.colorCode,
													weight : 2
												});
											} else {
												if (Object.keys(layer.options).length != 0) {
	
													layer
															.setStyle({
																fillColor : layer.colorCode,
																fillOpacity : 0.5,
																color : layer.colorcode,
																weight : 1
															});
												}
											}
										}
									});
							selectId = e.sourceTarget.id;
							$scope.selectedRow = e.sourceTarget.id;
							$('#treeview').treeview('selectNode',
									e.sourceTarget.id + 1);
						}

					}

					$scope.selectOutline = function(index) {

						if ($scope.isEdit) {
							var r;
							$
									.confirm({
										title : $rootScope.MSGBOX_TITLE,
										content : 'Are you sure you want to discard your changes?',
										type : 'blue',
										columnClass : 'medium',
										autoClose : false,
										icon : 'fa fa-info-circle',
										buttons : {
											yes : {
												text : 'Yes',
												btnClass : 'btn-blue',
												action : function() {
													r = true;
												}
											},
											no : function() {
												r = false;

											}
										}
									});
							if (r === false) {
								return false;
							} else {
								$scope.cancelEditOutline();
							}
						}

						if ($scope.selectedMenu == "Door") {

							$scope.selectedRow = index;
							$("#selectedId").hide();
							$scope.outlinesId = $scope.outlineList[$scope.selectedRow].outlinesId;
						}else
							{
							
							
						$("#selectedId").show();

						$scope.selectedRow = index;

						$scope.outlinesId = $scope.outlineList[$scope.selectedRow].outlinesId;
						$scope.outlineName = $scope.outlineList[$scope.selectedRow].name;
						$scope.outlinesArea = $scope.outlineList[$scope.selectedRow].outlinesArea;
						$scope.colorCode = $scope.outlineList[$scope.selectedRow].bgColor;
						$scope.wallType = $scope.outlineList[$scope.selectedRow].wallType;
						$scope.outlineType = $scope.outlineList[$scope.selectedRow].outlineType;
						$scope.placeType = $scope.outlineList[$scope.selectedRow].placeType;
						$scope.geometryType = $scope.outlineList[$scope.selectedRow].geometryType;
						$scope.outlineVertices =  $scope.outlineList[$scope.selectedRow].coordinateList ;

						$scope.wallWidth = $scope.outlineList[$scope.selectedRow].wallWidth;
						$scope.$apply();
						$scope.orignalOutlineVertices = [];
						for (var j = 0; j < $scope.outlineVertices.length; j++) {
							$scope.orignalOutlineVertices.push({
								'x' : $scope.outlineVertices[j].x,
								'y' : $scope.outlineVertices[j].y
							});
						}
						if ($scope.outlinesId == '') {
							$("#outlineEditBtn").text('Save');
							$scope.isEdit = true;
							// $("#btnDelete").hide();
						} else {
							$("#outlineEditBtn").text('Edit');
							$scope.isEdit = false;
							$("#btnDelete").show();
						}
						map
								.eachLayer(function(layer) {
									if (layer.id !== undefined && layer.menuType == "Building") {
										if (index == layer.id) {
											layer.setStyle({
												fillColor : layer.colorCode,
												fillOpacity : 0.8,
												color : layer.colorCode,
												weight : 2
											});
										} else {
											if (Object.keys(layer.options).length != 0) {
												if (layer.setStyle !== undefined) {
													layer
															.setStyle({
																fillColor : layer.colorCode,
																fillOpacity : 0.5,
																color : layer.colorCode,
																weight : 1
															});
												}
											}
										}
									}
								});
						map.closePopup();
						if ($scope.outlineVertices.length > 0) {
							map.panTo(pixelsToLatLng(
									parseFloat($scope.outlineVertices[0].x),
									parseFloat($scope.outlineVertices[0].y)));

							/*
							 * map.flyTo(pixelsToLatLng(parseFloat($scope.outlineVertices[0].x)
							 * ,parseFloat($scope.outlineVertices[0].y)),
							 * map.getZoom());
							 */
						}
							}

					}

					function GetSortOrder(prop) {
						return function(a, b) {
							if (a[prop].toLowerCase() > b[prop].toLowerCase()) {
								return 1;
							} else if (a[prop].toLowerCase() < b[prop]
									.toLowerCase()) {
								return -1;
							}
							return 0;
						}
					}
					
					
					
					$('.selected-items-box').bind('click', function(e){
						  e.stopPropagation();
						  $('.multiple-select-wrapper .list').toggle('slideDown');
						});

						$('.multiple-select-wrapper .list').bind('click', function(e){
							e.stopPropagation();
						});

						$(document).bind('click', function(){
							$('.multiple-select-wrapper .list').slideUp();	
						});
						  
						$scope.pathwayPatterns = [
							{name:'Line-Cross', img:'dist/img/path/image.gif'}, 
						    {name:'Diamond', img:'dist/img/path/diamond.gif'},
							{ name:'Square', img:'dist/img/path/square.gif'} ,
							{ name:'Polygon', img:'dist/img/path/polygon.gif'} 
						];
						

						$("#selectedPattern").hide(); 
						$("#pathPattern li").click(function() {

							if(this.id!==undefined && this.id!=null){
								$scope.selectedPatternName = $scope.pathwayPatterns[this.id-1].name; 
								setPatternValues();
						    		  
							} 
					});
						
						function getPathwayPattern(pathwayPattern) {
							for (var i = 0; i < $scope.pathwayPatterns.length; i++) {
								if($scope.pathwayPatterns[i].name==pathwayPattern)
								{
									return $scope.pathwayPatterns[i].img;
								}
							}
							
							return $scope.pathwayPatterns[0].img;
						}
						
						function setPatternValues()
						{
							$scope.selectedPatternImage = getPathwayPattern($scope.selectedPatternName); 
							 
				    		$('#selectedPatternName').text($scope.selectedPatternName); 
							$("#selectedPatternImage").attr("src", $scope.selectedPatternImage);
							$('.multiple-select-wrapper .list').slideUp();	 
							$("#selectedPattern").show();
						}
						
						/*
						 * var ws;
						 * 
						 * $scope.connect = function () { var username =
						 * document.getElementById("username").value;
						 * 
						 * var host = document.location.host; var pathname =
						 * document.location.pathname;
						 * 
						 * ws = new WebSocket("ws://" +host + pathname + "chat/" +
						 * username);
						 * 
						 * ws.onmessage = function(event) {
						 * console.log(event.data); }; }
						 * 
						 * $scope.send = function () { var content = "Test"; var
						 * json = JSON.stringify({ "content":content });
						 * 
						 * ws.send(json); }
						 */
						
						$scope.fnAllClick = function() {
								document.getElementById("idCkAllMenu").checked = true;
								document.getElementById("idCkBuildingMenu").checked = false;
								document.getElementById("idCkDoorMenu").checked = false;
								document.getElementById("idCkWallMenu").checked = false;
								document.getElementById("idCkPartitionMenu").checked = false;
								document.getElementById("idCkPathwayMenu").checked = false;
								document.getElementById("idCkImportMenu").checked = false;
								document.getElementById("idCkExportMenu").checked = false;
								document.getElementById("idCkImportZoneMenu").checked = false;
								
								$scope.selectedMenu = 'All';
								// $rootScope.$broadcast('selectTopologyMenu',
								// $scope.selectedMenu);
								$scope.topologyMenu();
					}
						$scope.fnBuildingClick = function() {
							document.getElementById("idCkAllMenu").checked =false ;
							document.getElementById("idCkBuildingMenu").checked =true ;
							document.getElementById("idCkDoorMenu").checked = false;
							document.getElementById("idCkWallMenu").checked = false;
							document.getElementById("idCkPartitionMenu").checked = false;
							document.getElementById("idCkPathwayMenu").checked = false;
							document.getElementById("idCkImportMenu").checked = false;
							document.getElementById("idCkExportMenu").checked = false;
							document.getElementById("idCkImportZoneMenu").checked = false;
							$scope.selectedMenu = 'Building';
							// $rootScope.$broadcast('selectTopologyMenu',
							// $scope.selectedMenu);
							$scope.topologyMenu();
				}

						$scope.fnDoorClick = function() {
							document.getElementById("idCkAllMenu").checked =false ;
							document.getElementById("idCkBuildingMenu").checked = false;
							document.getElementById("idCkDoorMenu").checked = true ;
							document.getElementById("idCkWallMenu").checked = false;
							document.getElementById("idCkPartitionMenu").checked = false;
							document.getElementById("idCkPathwayMenu").checked = false;
							document.getElementById("idCkImportMenu").checked = false;
							document.getElementById("idCkExportMenu").checked = false;
							document.getElementById("idCkImportZoneMenu").checked = false;
							$scope.selectedMenu = 'Door';
							// $rootScope.$broadcast('selectTopologyMenu',
							// $scope.selectedMenu);
							$scope.topologyMenu();
				}
						$scope.fnWallClick = function() {
							document.getElementById("idCkAllMenu").checked =false ;
							document.getElementById("idCkBuildingMenu").checked = false;
							document.getElementById("idCkDoorMenu").checked =  false;
							document.getElementById("idCkWallMenu").checked = true ;
							document.getElementById("idCkPartitionMenu").checked = false;
							document.getElementById("idCkPathwayMenu").checked = false;
							document.getElementById("idCkImportMenu").checked = false;
							document.getElementById("idCkExportMenu").checked = false;
							document.getElementById("idCkImportZoneMenu").checked = false;
							$scope.selectedMenu = 'Wall';
							// $rootScope.$broadcast('selectTopologyMenu',
							// $scope.selectedMenu);
							$scope.topologyMenu();
				}
						$scope.fnPartitionClick = function() {
							document.getElementById("idCkAllMenu").checked =false ;
							document.getElementById("idCkBuildingMenu").checked = false;
							document.getElementById("idCkDoorMenu").checked =  false;
							document.getElementById("idCkWallMenu").checked =  false;
							document.getElementById("idCkPartitionMenu").checked = true ;
							document.getElementById("idCkPathwayMenu").checked = false;
							document.getElementById("idCkImportMenu").checked = false;
							document.getElementById("idCkExportMenu").checked = false;
							document.getElementById("idCkImportZoneMenu").checked = false;
							$scope.selectedMenu = 'Partition';
							// $rootScope.$broadcast('selectTopologyMenu',
							// $scope.selectedMenu);
							$scope.topologyMenu();
				}
						$scope.fnPathwayClick = function() {
							document.getElementById("idCkAllMenu").checked =false ;
							document.getElementById("idCkBuildingMenu").checked = false;
							document.getElementById("idCkDoorMenu").checked =  false;
							document.getElementById("idCkWallMenu").checked =  false;
							document.getElementById("idCkPartitionMenu").checked = false;
							document.getElementById("idCkPathwayMenu").checked = true ;
							document.getElementById("idCkImportMenu").checked = false;
							document.getElementById("idCkExportMenu").checked = false;
							document.getElementById("idCkImportZoneMenu").checked = false;
							$scope.selectedMenu = 'Pathway';
							// $rootScope.$broadcast('selectTopologyMenu',
							// $scope.selectedMenu);
							$scope.topologyMenu();
				}
						$scope.fnImportClick = function() {
							document.getElementById("idCkAllMenu").checked =false ;
							document.getElementById("idCkBuildingMenu").checked = false;
							document.getElementById("idCkDoorMenu").checked =  false;
							document.getElementById("idCkWallMenu").checked =  false;
							document.getElementById("idCkPartitionMenu").checked = false;
							document.getElementById("idCkPathwayMenu").checked = false;
							document.getElementById("idCkImportMenu").checked = true ;
							document.getElementById("idCkExportMenu").checked = false;
							document.getElementById("idCkImportZoneMenu").checked = false;
							$scope.topologyMenu();
							$("#campusDetailsDiv").hide();
							$("#selectedId").hide();
							$("#selectedDoor").hide();
							$("#selectedWall").hide();
							$("#selectedPartition").hide(); 								
							$("#selectedPathway").hide();
							$("#idImportMapDetail").show();
							$("#idExportMapDetail").hide();
							$("#idImportZoneDetail").hide();
							$scope.selectedMode = "Import";
	
				}
						$scope.fnExportClick = function() {
							document.getElementById("idCkAllMenu").checked =false ;
							document.getElementById("idCkBuildingMenu").checked = false;
							document.getElementById("idCkDoorMenu").checked =  false;
							document.getElementById("idCkWallMenu").checked =  false;
							document.getElementById("idCkPartitionMenu").checked = false;
							document.getElementById("idCkPathwayMenu").checked = false;
							document.getElementById("idCkImportMenu").checked = false;
							document.getElementById("idCkExportMenu").checked =true ;
							document.getElementById("idCkImportZoneMenu").checked = false;
							$scope.topologyMenu();
							$("#campusDetailsDiv").hide();
							$("#selectedId").hide();
							$("#selectedDoor").hide();
							$("#selectedWall").hide();
							$("#selectedPartition").hide(); 								
							$("#selectedPathway").hide();
							$("#idImportMapDetail").hide();
							$("#idExportMapDetail").show();
							$("#idImportZoneDetail").hide();
							$scope.selectedMode = "Export";
		
				}
						$scope.fnImportZoneClick = function() {
							document.getElementById("idCkAllMenu").checked =false ;
							document.getElementById("idCkBuildingMenu").checked = false;
							document.getElementById("idCkDoorMenu").checked =  false;
							document.getElementById("idCkWallMenu").checked =  false;
							document.getElementById("idCkPartitionMenu").checked = false;
							document.getElementById("idCkPathwayMenu").checked = false;
							document.getElementById("idCkImportMenu").checked = false;
							document.getElementById("idCkExportMenu").checked =false ;
							document.getElementById("idCkImportZoneMenu").checked =true ;
							
							$scope.topologyMenu();
							$("#campusDetailsDiv").hide();
							$("#selectedId").hide();
							$("#selectedDoor").hide();
							$("#selectedWall").hide();
							$("#selectedPartition").hide(); 								
							$("#selectedPathway").hide();
							$("#idImportMapDetail").hide();
							$("#idExportMapDetail").hide();
							$("#idImportZoneDetail").show();
							$scope.selectedMode = "Import Zone";
		
				}	
						$scope.exportData = function() {
							
							var door_list = [];
							var outline_list = [];
							var pathway_list = [];
							var partition_list = [];
							var wall_list = [];
							// door list
							if($scope.doorsList.length>0){
								
								for (var i = 0; i < $scope.doorsList.length; i++) {
									
									var data = {};
									if($scope.doorsList[i].active == true){
										data.id = $scope.doorsList[i].id;
										data.colorCode = $scope.doorsList[i].colorCode;
										data.name = $scope.doorsList[i].name;
										data.outlinesId = $scope.doorsList[i].outlinesDetails.outlinesId;
										data.wallType = $scope.doorsList[i].wallType;
										data.coordinates = $scope.doorsList[i].coordinates;
										door_list.push(data);	
									}
									
								} 
							}
							// outline list
							
								if($scope.outlineList.length>0){
								
								for (var i = 0; i < $scope.outlineList.length; i++) {
									
									var data = {};
									if($scope.outlineList[i].active == true){
									data.outlinesId = $scope.outlineList[i].outlinesId;
									data.campusId =$scope.outlineList[i].campusDetails.campusId;
									data.name = $scope.outlineList[i].campusDetails.name;
									data.placeType = $scope.outlineList[i].placeType;
									data.wallType = $scope.outlineList[i].wallType;
									data.outlinesArea = $scope.outlineList[i].outlinesArea;
									data.bgColor = $scope.outlineList[i].bgColor;
									data.geometryType = $scope.outlineList[i].geometryType;
									data.coordinateList = $scope.outlineList[i].coordinateList;
									outline_list.push(data);
								} 
								}
							}
								// pathway list
								
								if($scope.pathwayList.length>0){
								
								for (var i = 0; i < $scope.pathwayList.length; i++) {
									
									var data = {};
									if($scope.pathwayList[i].active == true){
									data.id = $scope.pathwayList[i].id;								
									data.campusId = $scope.pathwayList[i].campusDetails.campusId;
									data.name = $scope.pathwayList[i].name;
									data.colorCode = $scope.pathwayList[i].colorCode;
									data.pathwayPattern = $scope.pathwayList[i].pathwayPattern;
									data.pathwayType = $scope.pathwayList[i].pathwayType;
									data.geometryType = $scope.pathwayList[i].geometryType;
									data.coordinates = $scope.pathwayList[i].coordinates;
									pathway_list.push(data);
								} 
								}
							}
								
								// partition list
								
								if($scope.partitionList.length>0){
								
								for (var i = 0; i < $scope.partitionList.length; i++) {
									
									var data = {};
									if($scope.partitionList[i].active == true){
									data.id = $scope.partitionList[i].id;								
									data.outlinesId = $scope.partitionList[i].outlinesDetails.outlinesId;
									data.name = $scope.partitionList[i].name;
									data.colorCode = $scope.partitionList[i].colorCode;
									data.wallType = $scope.partitionList[i].wallType;									
									data.geometryType = $scope.partitionList[i].geometryType;
									data.coordinates = $scope.partitionList[i].coordinates;
									partition_list.push(data);
								} 
								}
							}
									// wall list
								
								if($scope.wallList.length>0){
								
								for (var i = 0; i < $scope.wallList.length; i++) {
									
									var data = {};
									if($scope.wallList[i].active == true){
									data.id = $scope.wallList[i].id;								
									data.outlinesId = $scope.wallList[i].outlinesDetails.outlinesId;
									data.name = $scope.wallList[i].name;
									data.colorCode = $scope.wallList[i].colorCode;
									data.wallType = $scope.wallList[i].wallType;									
									data.geometryType = $scope.wallList[i].geometryType;
									data.coordinates = $scope.wallList[i].coordinates;
									wall_list.push(data);
								} 
								}
							}
							var jsonDataforExport = {
									  "version": "version 1.0",
									  "type": "topology",
									  "campus": {
									    "width": $scope.mapWidth,
									    "height": $scope.mapHeight,
									    "name": $scope.campusName,
									    "colorCode": $scope.mapColor
									  },
									  "outline": outline_list,									 
									  "door": door_list,
									  "pathway": pathway_list,
									  "partition": partition_list,
									  "wall": wall_list,
									};
						    let dataStr = JSON.stringify(jsonDataforExport);
						    let dataUri = 'data:application/json;charset=utf-8,'+ encodeURIComponent(dataStr);
						    let exportFileDefaultName = 'topologyEditor.json';
						    let linkElement = document.createElement('a');
						    linkElement.setAttribute('href', dataUri);
						    linkElement.setAttribute('download', exportFileDefaultName);
						    linkElement.click();
						}
						
						
						/*
						 * var formData = new FormData();
						 * 
						 * $scope.getTheFiles = function(element) {
						 * 
						 * $scope.loading = true; $scope.$apply(function($scope) {
						 * 
						 * for (var i = 0; i < element.files.length; i++) {
						 * 
						 * fileName = element.files[0].name; var ext =
						 * fileName.substring(fileName.lastIndexOf('.') + 1);
						 * $scope.loading = false; if(ext.toLowerCase() ==
						 * 'json') { $scope.error= false;
						 * 
						 * var fileReader = new FileReader(); fileReader.onload =
						 * function(e) { try{ $scope.jsonContent =
						 * JSON.parse(e.target.result); }catch (err){ $.alert({
						 * title: $rootScope.MSGBOX_TITLE, content: 'JSON format
						 * error', icon: 'fa fa-info-circle', });
						 * 
						 * $scope.$apply(); } }
						 * fileReader.readAsText(files.item(0)); } else {
						 * 
						 * $('input[type=file]').val(''); $scope.error = true;
						 * $scope.alert_error_msg = "Please upload correct File
						 * Type, File extension should be .json"; $timeout(
						 * function() { $scope.error= false; }, 3000); return
						 * true; } } }); }
						 */
						
						$scope.getJsondata = function() {
							$scope.jsonContent = [];
							$scope.ext ="";
							var files = document.getElementById('upfile').files;
							var jsonfileName = document.getElementById('upfile').value;
							$scope.ext = jsonfileName.substring(jsonfileName.lastIndexOf('.') + 1); 
                             if (files.length <= 0) {
     						    return false;
     						  }     						  
                               if($scope.ext.toLowerCase() == 'json'){
										  $scope.errorImport= false;	
										  var fr = new FileReader();										 					 
										  fr.onload = function(e) { 	          								
	          									 $scope.jsonContent = JSON.parse(e.target.result);	
	          								}
	          								fr.readAsText(files.item(0));	          							 
	          				  }/*
								 * else{ $('input[type=file]').val('');
								 * $scope.errorImport = true;
								 * $scope.alert_error_import_msg = "Please
								 * Select JSON File"; $timeout(function() {
								 * $scope.errorImport= false; }, 500); }
								 */
						}
						
						$scope.importData = function() {			
					
		   			 var fileVal = $('#upfile').val(); 
			        if(fileVal=='' || $scope.ext.toLowerCase() != 'json') 
			        { 
			            $scope.errorImport= true;
						$scope.alert_error_import_msg="Please select a JSON file";
	
						$timeout(
							function() {
								$scope.errorImport= false
	
							}, 5000);
						return;

			        }
			        if ($scope.jsonContent.type != "topology")
			    	{
			    	$scope.errorImport= true;
					$scope.alert_error_import_msg="Please select Topology JSON file";

					$timeout(
						function() {
							$scope.errorImport= false

						}, 5000);
					return;
			    	}
						
				if($scope.uploadFiles.$valid){
					 $rootScope.showloading('#loadingBar');
                     if ($rootScope.checkNetconnection() == false) { 
                    	 $rootScope.hideloading('#loadingBar');
							$rootScope.alertErrorDialog('Please check the Internet Connection');   
				           return;
				            } 
			// if ($window.sessionStorage.getItem("serviceProvider") ==
			// "quuppa") {
				   // var str = $scope.vtype;
				      // var res = str.toUpperCase();

                        /*
						 * $.confirm({ title: $rootScope.MSGBOX_TITLE, content:
						 * 'Do you want to upload the File '+fileName+'?',
						 * confirmButton: 'OK', cancelButton: 'Cancel',
						 * confirmButtonClass: 'btn-ok', cancelButtonClass:
						 * 'btn-cancel', autoClose:false, icon: 'fa
						 * fa-question-circle', theme: 'hololight',
						 */
                        // confirm: function(){
               
					  
						var request = {

							method : 'POST',
							url : './importCampusDetails',
							data :  $scope.jsonContent ,
							headers : {
								'Content-Type' : 'application/json'
							}
						};
						console.log(request)
						$http(request)
								.success(function(response) {
									$rootScope.hideloading('#loadingBar');
									
									if (response.status == "error") {
										
										$('input[type=file]').val('');
										$rootScope.alertErrorDialog(response.message);
 
										
									} else {
										
										$('input[type=file]').val('');

										$rootScope.alertDialog('File imported successfully !'); 
    									getCampusDetails();

									}

								})
								.error(function(response) {
									$rootScope.hideloading('#loadingBar');
									$rootScope.fnHttpError(response);
									$('input[type=file]').val('');
									}); 
						
				   }
				
				}
						$scope.getZoneJsondata = function() {
							$scope.jsonContent = [];
							$scope.ext ="";
							var files = document.getElementById('idZoneFile').files;
							var jsonfileName = document.getElementById('idZoneFile').value;
							$scope.ext = jsonfileName.substring(jsonfileName.lastIndexOf('.') + 1); 
                             if (files.length <= 0) {
     						    return false;
     						  }     						  
                               if($scope.ext.toLowerCase() == 'json'){
										  $scope.errorImport= false;	
										  var fr = new FileReader();										 					 
										  fr.onload = function(e) { 	          								
	          									 $scope.jsonContent = JSON.parse(e.target.result);	
	          								}
	          								fr.readAsText(files.item(0));	          							 
	          				  }
						}
						$scope.importZoneData = function() {			
							
				   			 var fileVal = $('#idZoneFile').val(); 
					        if(fileVal=='' || $scope.ext.toLowerCase() != 'json') 
					        { 
					            $scope.errorImport= true;
								$scope.alert_error_import_msg="Please select a JSON file";
			
								$timeout(
									function() {
										$scope.errorImport= false
			
									}, 5000);
								return;

					        } 			
					        
					    if ($scope.jsonContent.type != "zone")
					    	{
					    	$scope.errorImport= true;
							$scope.alert_error_import_msg="Please select Zone Detail JSON file";
		
							$timeout(
								function() {
									$scope.errorImport= false
		
								}, 5000);
							return;
					    	}
					   
						if($scope.uploadZoneFiles.$valid){
		                     if ($rootScope.checkNetconnection() == false) { 
									$rootScope.alertErrorDialog('Please check the Internet Connection');   
									return;
						     } 
							 $rootScope.showloading('#loadingBar');
							  
								var request = {

									method : 'POST',
									url : './importZoneDetails',
									data :  $scope.jsonContent ,
									headers : {
										'Content-Type' : 'application/json'
									}
								};
								console.log(request)
								$http(request)
										.success(function(response) {
											$rootScope.hideloading('#loadingBar');
											
											if (response.status == "error") {
												
												$('input[type=file]').val('');
												$rootScope.alertErrorDialog(response.message);
		 
												
											} else {
												
												$('input[type=file]').val('');

												$rootScope.alertDialog('Zone Details imported successfully! Please check in Zone Management screen'); 
											}

										})
										.error(function(response) {
											$rootScope.hideloading('#loadingBar');
											$rootScope.fnHttpError(response);
											$('input[type=file]').val('');
											}); 
								
						   }
						
						}						
						
				});
