app.controller(
		'accountCreateController',
		function($scope, $http, $rootScope, $window, $location,$timeout) {
			$scope.user = JSON.parse($window.sessionStorage
					.getItem("user"));
			if ($scope.user == null) {
				$location.path('/login');
				return;
			}
	        let menudata;
	        
	        $scope.systemUserType =["SystemAdmin", "Manager", "SuperUser", "User"];
		 $scope.prof="Manager";
			// Set the default value of inputType
		   $scope.inputType = 'password';
		   
		   $(document).ready(function() {
			   
				$('[data-toggle="tooltip"]').tooltip();
			});
		  
			
		  var userName = document.querySelector('#numberField');

		//userName.addEventListener('input', restrictNumber);
		function restrictNumber (e) {  
		  var newValue = this.value.replace(new RegExp(/[^\d]/,'ig'), "");
		  this.value = newValue;
		}
		//kumar 14-Jun show password with eye icon
		$scope.pwdEyeButtonClick = function() {
			$scope.toggleEyeButton('#id_password',"#id_passwordToggle svg");
		}			 
		//kumar 14-Jun show password with eye icon
		$scope.retypePwdEyeButtonClick = function() {
			$scope.toggleEyeButton('#id_retypePassword',"#id_RetypePwdToggle svg");
		}			 
		//kumar 14-Jun show password with eye icon
		$scope.toggleEyeButton = function (pwdEditCtrl, eyeButton){
			const passwordCtl = document.querySelector(pwdEditCtrl);
			
			if(passwordCtl.getAttribute('type') == "password"){
		       	passwordCtl.setAttribute('type', 'text');
		        //$("#togglePassword svg").removeClass("fa-eye-slash").addClass("fa-eye");
			}else if(passwordCtl.getAttribute('type') == "text"){
				passwordCtl.setAttribute('type', 'password');
		       	//$("#togglePassword svg").removeClass("fa-eye").addClass("fa-eye-slash");
			}
			$(eyeButton).toggleClass("fa-eye fa-eye-slash");
		 
		}
		$("#phoneNumber").keypress(
				function(e) {
					// if the letter is not digit then display error
					// and don't type anything
					
					if (e.which != 8 && e.which != 0
							&& (e.which < 48 || e.which > 57)) {
						
						return false;
					}
				});
		$("#fullName").keypress(
				function(e) {
					// if the letter is not alphabets then display error
					// and don't type anything
					$return = ((e.which > 64) && (e.which < 91)) 
					|| ((e.which> 96) && (e.which < 123)) 
					||(e.which == 8) || (e.which == 32)  
					|| (e.which >= 48 && e.which <= 57)
					if (!$return)
										
					{
						
						return false;
					}
				});
		
		
		$scope.cancel = function (){
			
			$scope.authen.fullName ="";
			$scope.authen.mailId ="";
			$scope.authen.password="";
		    $scope.authen.retypePassword="";
		    $scope.authen.phoneNumber="";
		    $scope. authen.countryCode = $rootScope.countryCodes[0];
		    $scope.authen.userType = $scope.systemUserType[3];
		    //$scope.modelerror = false;
			 
		}

		if($scope.authen==undefined){ $scope.authen = {};}
		$scope.authen.userType = $scope.systemUserType[3];
		if($scope.authen==undefined){ $scope.authen = {};}
		$scope.authen.countryCode =$rootScope.countryCodes[0];

	    $scope.authenticate = function (){
	   
	    	var displayName = document.getElementById("fullName");
			if (displayName.value.length == 0) {
				 $scope.accCreateNameError = true;
				$scope.accCreateName_error_msg = "Please enter Name";
				$timeout(function() {
					$scope.accCreateNameError = false;
				}, 2000);
				return;
			}
			var emailAccCreate = document.getElementById("mailId");
			if (emailAccCreate.value.length == 0) {
				 $scope.accCreateEmailError = true;
				$scope.accCreateEmail_error_msg = "Please enter Email";
				$timeout(function() {
					$scope.accCreateEmailError = false;
				}, 2000);
				return;
			}
			var mailformat = /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/ ;
			if (!mailformat.test(emailAccCreate.value)) {
				 $scope.accCreateEmailError = true;
				$scope.accCreateEmail_error_msg = "Please enter a valid Email Id";
				$timeout(function() {
					$scope.accCreateEmailError = false;
				}, 2000);
				return;
			}
			var accCreatePassword = document.getElementById("id_password");
			if (accCreatePassword.value.length == 0) {
				$scope.accCreatePasswordError = true;
				$scope.accCreatePassword_error_msg = "Please enter Password";
				$timeout(function() {
					$scope.accCreatePasswordError = false;
				}, 2000);
				return;
			}
			var accCreateRetypePWD = document.getElementById("id_retypePassword");
			if (accCreateRetypePWD.value.length == 0) {
				 $scope.accCreateRetypePWDError = true;
				$scope.accCreateRetypePWD_error_msg = "Please  retype Password";
				$timeout(function() {
					$scope.accCreateRetypePWDError = false;
				}, 2000);
				return;
			}
			var phnoErr = document.getElementById("phoneNumber");
			if (phnoErr.value.length == 0) {
				$scope.accCreatePhNoError = true;
				$scope.accCreatePhNo_error_msg = "Please enter phone number";
				$timeout(function() {
					$scope.accCreatePhNoError = false;
				}, 2000);
				return;
			}
			if(phnoErr.value.length > 0 && phnoErr.value.length < 10 ){
				$scope.accCreatePhNoError = true;
				$scope.accCreatePhNo_error_msg = "Please enter valid 10 digit phone number";
				$timeout(function() {
					$scope.accCreatePhNoError = false;
				}, 2000);
				return;
			
			}
	       	
	      		if($scope.authen.password.length>=8 ){
	      		 if(/[ !@#$%^&*()_+\-=\[\]{};':"\\|,.<>\/?]/.test($scope.authen.password)){
	      			 if($scope.authen.password ==  $scope.authen.retypePassword){
	      			 
	   	     $scope.phNo = $scope.authen.countryCode+$scope.authen.phoneNumber;
	   	     
	   	     
	   	     var regParameter={
	   					
	   						"emailID": $scope.authen.mailId,
	   						  "displayName": $scope.authen.fullName,
	   						  "password": $scope.authen.password,
	   						  "userType": $scope.authen.userType,
	   						  "type": "dashboard",
	   						  "mobileNumber": $scope.phNo 
	   				};

	   	  if (!$rootScope.checkNetconnection()) { 
				$rootScope.alertDialogChecknet();
				return;
				}
	   	  
	   	     $rootScope.showloading('#loadingBar');
	   		 	$scope.promise= $http.post('./registration',regParameter)
	   	          .then(function(result) {
	   	 			 
	   	        	  if (result != null
	   							&& result.data != null
	   							&& result.data != "BAD_REQUEST") {
	   	        	  
	   		        		$rootScope.hideloading('#loadingBar');
	   		        		$rootScope.showAlertDialog(result.data.message) ;
	   		        		$scope.cancel();
	   		        		 
	                        $timeout(function() {
	   						
	   				        //$location.path("/login"); 
	                        
	   						 }, 3000); 
	   		        	}else {
	   				    
	   		        		$rootScope.hideloading('#loadingBar');
	   		        		$rootScope.showAlertDialog(result.data.message) ;
	   		        		
	   		        	}
	   		        	 
	   		          }, function(error) {
							$rootScope.hideloading('#loadingBar');
							$rootScope.fnHttpError(error);
						});
	      		 }else{
	      			$rootScope.showAlertDialog("Passwords mismatch") ;
	      			}
	      		}else
	      			{
	      			$rootScope.showAlertDialog("Password must contain special character") ;
	      			
	      			}
	      	 }
	      	 else{
	      		$rootScope.showAlertDialog("Please enter a password greater than or equal to 8 characters") ;  		
	      		
	      	 }
	      	 
	   	
	   	
	       }
                     /* $http.get('page.json')
							.success(function(data) {
								menudata = data;
								  let tree = new Tree('.treemenu', {
							        data: [{ id: '-1', text: 'All', children: menudata }],
							        closeDepth: 2,
							        loaded: function () {
							            console.log(this.selectedNodes)
							            console.log(this.values)
							        },
							        onChange: function () {
							            //console.log(this.values);
										let frequentdata=this.selectedNodes;
                                     let tree1 = new Tree('.frequent', {
								        data: [{ id: '-1', text: 'All', children: frequentdata }],
								        closeDepth: 2,
								        loaded: function () {
								            console.log(this.selectedNodes)
								            console.log(this.values)
								        },
								        onChange: function () {
								            console.log(this.values);
								        }
								    })
							        }
							    })
								
							})
							.error(
									function(response) {
										if (response.status == 403) {
									     $.alert({
			                             title: $rootScope.MSGBOX_TITLE,
                                         content: 'Session timed-out or Invalid. Please logout and login again',
                                         icon: 'fa fa-exclamation-triangle',
                                          });											
                                     }
									});*/
									

		});