app
		.controller( 'skuController',
				function($scope, $http, $rootScope, $window, $location,
						$timeout) {
			
			$scope.user = JSON.parse($window.sessionStorage
					.getItem("user"));
			if ($scope.user == null) {
				$location.path('/login');
				return;
			}
					$scope.ext = "";
					$scope.exportArrayData = [];
					$scope.skuDetailData = new Array();
					$scope.skuDetailDispatchData = new Array();
					$scope.skuDetailViewData = new Array();
					$scope.skuDetailFilterList = new Array();
					$scope.assignedTagMacId = '';
					$scope.trackMacId = '';
					$scope.searchText = "";
					$scope.searchButton = true;
					$scope.showMultipleSearch = true;
					$scope.showDownloadIcon  = true;	
					$scope.showExportCard = false;
					$scope.showCompareDateSelection = false;
					$scope.keyNames= new Array();
					// Compare date
					$('#idDateRangeButton')
							.daterangepicker(
									{
										ranges : {
											'Today' : [ moment(), moment() ],
											'Next 7 Days' : [
													moment().add(6, 'days'),
													moment() ],
											'Next 14 Days' : [
													moment().add(13, 'days'),
													moment() ],
											'Next Month' : [
													moment().add(1, 'month')
															.startOf('month'),
													moment().add(1, 'month')
															.endOf('month') ],
										},
										startDate : moment(),
										endDate : moment()
									})

			        $scope.showLocateBtn = false;
					
					if ($scope.user.capabilityDetails != undefined
							&& $scope.user.capabilityDetails != null) {

						if ($scope.user.capabilityDetails.SDET == 1) {
							$scope.showSKUDetailBtn = true;
						} else {
							$scope.showSKUImportBtn = false;
						}
						if ($scope.user.capabilityDetails.SIMP == 1) {
							$scope.showSKUImportBtn = true;
						} else {
							$scope.showSKUDetailBtn = false;
						}
						if ($scope.user.capabilityDetails.SDIS == 1) {
							$scope.showSKUDispatchBtn = true;
							$scope.showLocateBtn = false;
						} else {
							$scope.showSKUDispatchBtn = false;
						}
						if ($scope.user.capabilityDetails.SKLV == 1) {
                            $scope.showLocateBtn = true;
                        }
					}
					
					//sorting fn
					var $sortable = $('.sortable');

					$sortable.on('click', function() {
						if ($scope.showMultipleSearch || $scope.showCompareDateSelection){
							return;
						}
						var $this = $(this);

						var asc = $this.hasClass('asc');
						var desc = $this.hasClass('desc');
						$sortable.removeClass('asc').removeClass('desc');
						var isAsc;
						if (desc || (!asc && !desc)) {
							$this.addClass('asc');
							isAsc = true;
						} else {
							$this.addClass('desc');
							isAsc = false;
						}

						var id = this.id;
						$scope.sortType = id;
						$scope.tableSortOrder(id, isAsc)

					});
					function GetSortOrder(prop) {
						return function(a, b) {
							if (a[prop].toLowerCase() > b[prop].toLowerCase()) {
								return 1;
							} else if (a[prop].toLowerCase() < b[prop]
									.toLowerCase()) {
								return -1;
							}
							return 0;
						}
					}
					function GetSortOrderNum(prop) {
						return function(a, b) {
							if (a[prop] > b[prop]) {
								return 1;
							} else if (a[prop]< b[prop]) {
								return -1;
							}
							return 0;
						}
					}
										
					$scope.tableSortType = '';
					$scope.tableSortisAsc = '';
					$scope.sortType = '';

					$scope.tableSortOrder = function(type, isAsc) {
						if (type == 'idSKUTableUPCCode')
							$scope.skuDetailData.sort(GetSortOrder("upcCode"));
						else if (type == 'idSKUTableSKUNo')
							$scope.skuDetailData.sort(GetSortOrder("skuNo"));						
						else if (type == 'idSKUTableBatchNo')
							$scope.skuDetailData.sort(GetSortOrder("batchNo"));
						else if (type == 'idSKUTableDescription')
							$scope.skuDetailData.sort(GetSortOrder("description"));
						else if (type == 'idSKUTableTotalQty')
							$scope.skuDetailData.sort(GetSortOrderNum("quantity"));
						else if (type == 'idSKUTableAvailQty')
							$scope.skuDetailData.sort(GetSortOrderNum("availableQuantity"));
						else if (type == 'idSKUTableExpiryDate')
							$scope.skuDetailData.sort($rootScope.GetSortOrderByDate("expiryDate", isAsc));
						
						if (!isAsc && type!='idSKUTableExpiryDate') {
							$scope.skuDetailData.reverse();
						}

						$scope.tableSortType = type;
						$scope.tableSortisAsc = isAsc;

						$timeout(function() {
						}, 300);
						
						if ($scope.searchText == '') {
							if ($scope.currentPage == 1)
								$scope.filterOption();// kumar 02/Jul filter
							// option
							else {
								var begin = (($scope.currentPage - 1) * $scope.numPerPage), end = begin
										+ $scope.numPerPage;

								$scope.skuDetailViewData = $scope.skuDetailData.slice(
										begin, end);
							}
						} else {
							var nCurrentPage = $scope.currentPage;
							var begin;
							$scope.filterOption();// kumar 02/Jul filter
							// option

							if (nCurrentPage == $scope.currentPage) {
										begin = (($scope.currentPage - 1) * $scope.numPerPage),
										end = begin + $scope.numPerPage;
							} else {
										begin = ((nCurrentPage - 1) * $scope.numPerPage),
										end = begin + $scope.numPerPage;
							}
							$scope.currentPage = nCurrentPage;
							$scope.getTotalPages();
							$scope.skuDetailViewData = $scope.skuDetailFilterList
									.slice(begin, end);

						}	
						/*$timeout(function() {
							var selectedRowCSS_Gray = {'background': '#ffffff', 'color': '#000000' };
							var selectedRowCSS_Gray1 = {'background': '#dee2e6', 'color': '#000000' };
							
							for (var i = 0; i < $scope.skuDetailViewData.length; i++) {
								if(i%2==0)
									$("#idSKUDetailTable > tbody > tr:nth-child(" + (i + 1) + ")").css(selectedRowCSS_Gray);
								else
									$("#idSKUDetailTable > tbody > tr:nth-child(" + (i + 1) + ")").css(selectedRowCSS_Gray1);
							}
						}, 100);*/
					}


					/*$scope.fnSearchOption = function() {
						$scope.fnFilterOption();
					}*/

					/*$scope.fnFilterOption = function() {

						$scope.skuDetailFilterList = [];

						for (var i = 0; i < $scope.skuDetailData.length; i++) {
							if ($scope.searchText == '') {

								$scope.skuDetailFilterList
										.push($scope.skuDetailData[i]);
							} else {
								if (($scope.skuDetailData[i].skuNo != undefined
										&& $scope.skuDetailData[i].skuNo != null ? $scope.skuDetailData[i].skuNo
										: "").toLowerCase().includes(
										$scope.searchText.toLowerCase())
										|| ($scope.skuDetailData[i].batchNo != undefined
												&& $scope.skuDetailData[i].batchNo != null ? $scope.skuDetailData[i].batchNo
												: "").toLowerCase()
												.includes(
														$scope.searchText
																.toLowerCase())
																|| ($scope.skuDetailData[i].upcCode != undefined
												&& $scope.skuDetailData[i].upcCode != null ? $scope.skuDetailData[i].upcCode
												: "").toLowerCase()
												.includes(
														$scope.searchText
																.toLowerCase())
										|| ($scope.skuDetailData[i].description != undefined
												&& $scope.skuDetailData[i].description != null ? $scope.skuDetailData[i].description
												: "").toLowerCase()
												.includes(
														$scope.searchText
																.toLowerCase())
										|| (($scope.skuDetailData[i].quantity).toString() != undefined
												&& ($scope.skuDetailData[i].quantity).toString() != null ? ($scope.skuDetailData[i].quantity).toString()
												: "").toLowerCase()
												.includes(
														$scope.searchText
																.toLowerCase())
										|| (($scope.skuDetailData[i].availableQuantity).toString() != undefined
												&& ($scope.skuDetailData[i].availableQuantity).toString() != null ? ($scope.skuDetailData[i].availableQuantity).toString()
												: "").toLowerCase()
												.includes(
														$scope.searchText
																.toLowerCase())
										|| ($scope.skuDetailData[i].transaction != undefined
												&& $scope.skuDetailData[i].transaction != null ? $scope.skuDetailData[i].transaction
												: "").toLowerCase()
												.includes(
														$scope.searchText
																.toLowerCase())
										|| ($scope.skuDetailData[i].expiryDate != undefined
												&& $scope.skuDetailData[i].expiryDate != null ? $scope.skuDetailData[i].expiryDate
												: "").toLowerCase()
												.includes(
														$scope.searchText
																.toLowerCase()))

								{
									$scope.skuDetailFilterList
											.push($scope.skuDetailData[i]);

								}

							}

						}

						$scope.totalItems = $scope.skuDetailFilterList.length;
						$scope.currentPage = 1;
						$scope.getTotalPages();
						$scope.getAssignedTagIds($scope.skuDetailFilterList);

						if ($scope.skuDetailData.length == 0
								|| $scope.skuDetailFilterList.length == 0) {
							$scope.skuDetailNoData = true;

						} else {
							$scope.skuDetailNoData = false;

							var begin = (($scope.currentPage - 1) * $scope.numPerPage), end = begin
									+ $scope.numPerPage;

							$scope.skuDetailViewData = $scope.skuDetailFilterList
									.slice(begin, end);

							// ---

							$scope.skuDetailNoData = false;

							if ($scope.skuDetailFilterList.length > $scope.numPerPage) {
								$scope.skuDetailViewData = $scope.skuDetailFilterList
										.slice(0, $scope.numPerPage);
								$scope.PaginationTab = true; // pagination
																// tab
							} else {
								$scope.skuDetailViewData = $scope.skuDetailFilterList;
								$scope.PaginationTab = false; // pagination
																// tab
							}

						}
					}*/
					$scope.getJsondata = function() {
						$scope.jsonContent = [];
						$scope.ext = "";
						var files = document.getElementById('idUPfile').files;
						var jsonfileName = document.getElementById('idUPfile').value;
						$scope.SelectedFile = jsonfileName;
						$scope.ext = jsonfileName.substring(jsonfileName
								.lastIndexOf('.') + 1);
						if (files.length <= 0) {
							return false;
						}
						if ($scope.ext.toLowerCase() == 'json') {
							$scope.errorImport = false;
							var fr = new FileReader();
							fr.onload = function(e) {
								try {
									$scope.jsonContent = JSON
											.parse(e.target.result);
								} catch (e) {
									$rootScope.alertErrorDialog(e); // error in
									// the above
									// string
									// (in this
									// case,
									// yes)!
									$('input[type=file]').val('');
								}
							}
							fr.readAsText(files.item(0));
						} else if ($scope.ext.toLowerCase() == 'csv') {

							$scope.errorImport = false;
							var fr = new FileReader();
							fr.onload = function(e) {
								try {
									var csv = e.target.result;
									var headerCSV, dataCSV, headerCSVlength = [];
									$scope.parsedCSVfile = Papa.parse(csv, {
										complete : function(results) {
											headerCSV = results.data[0];
											dataCSV = results.data;
											headerCSVlength = headerCSV;
										}
									});
									/*
									 * jquery_csv library code. it doesn't
									 * support tab separated file. but papa
									 * parse support both comma and tab
									 * separated file var data =
									 * $.csv.toArrays(csv); const result = [];
									 * const obj = {} for (let i = 1; i <
									 * data.length; i++) { for (let k = 0; k <
									 * data[i].length; k++) { obj[data[0][k]] =
									 * data[i][k]; }
									 * 
									 * result.push(obj); } console.log(result);
									 */

								} catch (e) {
									$rootScope.alertErrorDialog(e); // error in
									// the above
									// string
									// (in this
									// case,
									// yes)!
									$('input[type=file]').val('');
								}
							}
							fr.readAsText(files.item(0));

							/*
							 * $scope.errorImport= false; var fr = new
							 * FileReader(); fr.onload = function(e) { try { var
							 * csv = e.target.result; var data =
							 * $.csv.toArrays(csv); const result = []; const obj = {}
							 * for (let i = 1; i < data.length; i++) { for (let
							 * k = 0; k < data[i].length; k++) { obj[data[0][k]] =
							 * data[i][k]; }
							 * 
							 * result.push(obj); } console.log(result); }
							 * catch(e) { $rootScope.alertErrorDialog(e); //
							 * error in the above string (in this case, yes)!
							 * $('input[type=file]').val(''); } }
							 * fr.readAsText(files.item(0));
							 */
						}
					}
					$scope.importSKUDetailFile = function() {

						var fileVal = $('#upfile').val();
						if (fileVal == '' || $scope.ext.toLowerCase() != 'json') {
							$rootScope
									.alertErrorDialog("Please select a JSON file");
							return;
						}
						if ($scope.SKUImportForm.$valid) {
							if (!$rootScope.checkNetconnection()) { 
								//$rootScope.hideloading('#loadingBar');
							 	$rootScope.alertDialogChecknet();
								return; 
								}
							$rootScope.showloading('#loadingBar');
							var request = {

								method : 'POST',
								url : './skuDetails',
								data : $scope.jsonContent,
								headers : {
									'Content-Type' : 'application/json'
								}
							};
							

							$http(request)
									.success(
											function(response) {
												$rootScope
														.hideloading('#loadingBar');

												$('input[type=file]').val('');
												if (response.status == "error") {
													$rootScope
															.alertErrorDialog(response.message);
												} else {
													$rootScope
															.alertDialog('File imported successfully !');
												}

											})
									.error(
											function(response) {

												$rootScope.hideloading('#loadingBar');
												$rootScope.fnHttpError(response);
											

												$('input[type=file]').val('');

											});

						}
					}

					$scope.trackTag = function() {
						if ($scope.assignedTagMacId != undefined
								&& $scope.assignedTagMacId != null
								&& $scope.assignedTagMacId != '') {
							$rootScope.trackingMacId = $scope.assignedTagMacId;
							$rootScope.showFullView();
							var currentMenuName = $window.sessionStorage.getItem("menuName");
							if ($scope.user.role == "DemoUser")
							{
								$window.sessionStorage.setItem("menuName", "shopFloorAnalysis");
								$window.sessionStorage.setItem("demoMenuName", "skuLiveView");
								$location.path('/shopFloorAnalysis');
							}else{
								$window.sessionStorage.setItem("menuName",
										"skuLiveView");
								$location.path('/skuLiveView');
							}
							$rootScope.$broadcast('sourcePreviousScreen',currentMenuName);
						} else {
							toastr.error('No assigned tags to track.', "EI4.0");
						}

					}

					$scope.trackSingleTag = function() {
						if ($scope.trackMacId != undefined
								&& $scope.trackMacId != null
								&& $scope.trackMacId != '') {
							$rootScope.trackingMacId = $scope.trackMacId;
							$rootScope.showFullView();
							var currentMenuName = $window.sessionStorage.getItem("menuName");
							if ($scope.user.role == "DemoUser")
							{
								$window.sessionStorage.setItem("menuName", "shopFloorAnalysis");
								$window.sessionStorage.setItem("demoMenuName", "skuLiveView");
								$location.path('/shopFloorAnalysis');
							}else{
								$window.sessionStorage.setItem("menuName",
										"skuLiveView");
								$location.path('/skuLiveView');
							}
							$rootScope.$broadcast('sourcePreviousScreen',currentMenuName);
						} else {
							toastr.error('No assigned tags to track.', "EI4.0");
						}
					}
					$scope.rowHighilited = function(row) {
						$scope.selectedRow = row;

						$scope.authen = JSON.parse(JSON
								.stringify($scope.skuDetailViewData[row]));

						if ($scope.authen.status == 'Valid') {
							if ($scope.authen.assignedTagMacId != undefined
									&& $scope.authen.assignedTagMacId != '') {
								$scope.trackMacId = $scope.authen.assignedTagMacId;
							} else
								$scope.trackMacId = '';
						} else
							$scope.trackMacId = '';
					}
					var reA = /[^a-zA-Z]/g;
					var reN = /[^0-9]/g;

					function sortAlphaNum(a1, b1) {
						var a = a1.upcCode;
						var b = b1.upcCode;
						var aA = a.replace(reA, "");
						var bA = b.replace(reA, "");
						if (aA === bA) {
							var aN = parseInt(a.replace(reN, ""), 10);
							var bN = parseInt(b.replace(reN, ""), 10);
							return aN === bN ? 0 : aN > bN ? 1 : -1;
						} else {
							return aA > bA ? 1 : -1;
						}
					}
					// SKU Details view all API call and display data in table

					$scope.getSKUDetailData = function() {

						if (!$rootScope.checkNetconnection()) { 
							$rootScope.alertDialogChecknet();
							return; 
							}
						$rootScope.showloading('#loadingBar');
						$http({
							method : 'GET',
							url : './skuDetails',

						})
								.then(
										function success(response) {
											$rootScope
													.hideloading('#loadingBar');
											if (response != null
													&& response.data != null
													&& response.data.length != 0
													&& response.data != "BAD_REQUEST"
													&& response.data.data != undefined
													&& response.data.data.length != 0) {
												
												$scope.skuDetailDispatchData = [];
												$scope.skuDetailDataValid = [];
												$scope.skuDetailData = [];
												response.data.data
														.sort(sortAlphaNum);
												for (var i = 0; i < response.data.data.length; i++) {
													var varExpiryDate = "", varTransaction = "";
													if (response.data.data[i].skuAllocationData != undefined
															&& response.data.data[i].skuAllocationData.status == "Valid") {

														if (response.data.data[i].skuAllocationData.expiryDate != undefined 
																&& response.data.data[i].skuAllocationData.expiryDate != null 
																&& response.data.data[i].skuAllocationData.expiryDate != "" ) {
															varExpiryDate = moment(
																	response.data.data[i].skuAllocationData.expiryDate,"YYYY-MMM-DD")
																	.format(
																			"MMM DD, YYYY");

														}
														varTransaction = "False";
														if (response.data.data[i].skuAllocationData.transaction == true) {
															varTransaction = "True";
														}

														$scope.skuDetailDataValid
																.push({
																	'upcCode' : response.data.data[i].skuAllocationData.upcCode,
																	'skuNo' : response.data.data[i].skuAllocationData.skuNo,
																	'batchNo' : response.data.data[i].skuAllocationData.batchNo,
																	'description' : response.data.data[i].skuAllocationData.description,
																	'quantity' : response.data.data[i].skuAllocationData.quantity,
																	'availableQuantity' : response.data.data[i].skuAllocationData.availableQuantity,
																	'transaction' : varTransaction,
																	'skuExpiryDate':  response.data.data[i].skuAllocationData.expiryDate,
																	'expiryDate' : varExpiryDate,
																	'assignedTagMacId' : response.data.data[i].assignedTagMacId,
																	'tagId' : response.data.data[i].skuAllocationData.tagId,
																	'status' : response.data.data[i].skuAllocationData.status
																});

													}
													else if (response.data.data[i].skuAllocationData != undefined
															&& response.data.data[i].skuAllocationData.status == "Completed"){

														if (response.data.data[i].skuAllocationData.expiryDate != undefined 
																&& response.data.data[i].skuAllocationData.expiryDate != null 
																&& response.data.data[i].skuAllocationData.expiryDate != "" ) {
															varExpiryDate = moment(
																	response.data.data[i].skuAllocationData.expiryDate,"YYYY-MMM-DD")
																	.format(
																			"MMM DD, YYYY");

														}
														varTransaction = "False";
														if (response.data.data[i].skuAllocationData.transaction == true) {
															varTransaction = "True";
														}

														$scope.skuDetailDispatchData
																.push({
																	'upcCode' : response.data.data[i].skuAllocationData.upcCode,
																	'skuNo' : response.data.data[i].skuAllocationData.skuNo,
																	'batchNo' : response.data.data[i].skuAllocationData.batchNo,
																	'description' : response.data.data[i].skuAllocationData.description,
																	'quantity' : response.data.data[i].skuAllocationData.quantity,
																	'availableQuantity' : response.data.data[i].skuAllocationData.availableQuantity,
																	'transaction' : varTransaction,
																	'skuExpiryDate':  response.data.data[i].skuAllocationData.expiryDate,
																	'expiryDate' : varExpiryDate,
																	'assignedTagMacId' : response.data.data[i].assignedTagMacId,
																	'tagId' : response.data.data[i].skuAllocationData.tagId,
																	'status' : response.data.data[i].skuAllocationData.status
																});

													}
												}
												
												if($scope.skuTabName == "DispatchPage"){
													$scope.skuDetailData =  [];
													$scope.skuDetailData = $scope.skuDetailDispatchData	;
													
												}else if($scope.skuTabName == "DetailPage"){
													$scope.skuDetailData =  [];
													$scope.skuDetailData = $scope.skuDetailDataValid;
													
												}
													
												$scope.getAssignedTagIds($scope.skuDetailData);

												if ($scope.skuDetailData.length == 0) {
													
													$scope.skuDetailViewData = $scope.skuDetailData;
													$scope.skuDetailNoData = true;
													$scope.PaginationTab  = false;
													return;
												} else {
													$scope.skuDetailNoData = false;
												}

												if ($scope.skuDetailData.length > 10) {
													$scope.PaginationTab = true;
												} else {
													$scope.PaginationTab = false;
												}
												$scope.totalItems = $scope.skuDetailData.length;
												$scope.getTotalPages();
												$scope.currentPage = 1;
												
												
												if ($scope.skuDetailData.length > $scope.numPerPage) {
													$scope.skuDetailViewData = $scope.skuDetailData
															.slice(
																	0,
																	$scope.numPerPage);
													$scope.hasPrevious = true;
													$scope.isFirstPage = true;
													$scope.hasNext = false;
													$scope.isLastPage = false;
												} else {
													$scope.skuDetailViewData = $scope.skuDetailData;
												}
											} else {
												$scope.skuDetailNoData = true;
											}
										},
										function error(response) {
											$rootScope.hideloading('#loadingBar');
											$rootScope.fnHttpError(response);
										});
					}
					// Compare date
					$('#idDateRangeButton').on('apply.daterangepicker', function(ev, picker) {
					$('#idDateRangeButton').val(picker.startDate.format('MMM DD, YYYY') + ' - ' + picker.endDate.format('MMM DD, YYYY'));
					// alert(picker.endDate.format('MMMM D, YYYY') + ' - ' +
					// picker.startDate.format('MMMM D, YYYY'));

					if(picker.chosenLabel == "Custom Range" || picker.chosenLabel == "Next Month" ) {
						$scope.startDateExpiry =  picker.startDate.format('MMM DD, YYYY');
						$scope.endDateExpiry = picker.endDate.format('MMM DD, YYYY');
						$scope.disableCompareDate = true;
						if(picker.chosenLabel == "Custom Range" && ($scope.startDateExpiry == $scope.endDateExpiry)){
							$scope.disableCompareDate = false;
							// toastr.info("Dates are same","EI40")
						}
						if (picker.chosenLabel == "Next Month"){
							$scope.dateRange = picker.chosenLabel;
						} else {
							$scope.dateRange = picker.startDate.format('MMM DD, YYYY') + ' - ' + picker.endDate.format('MMM DD, YYYY');
						}
					}else{
						$scope.startDateExpiry =   picker.endDate.format('MMM DD, YYYY');
						$scope.endDateExpiry = picker.startDate.format('MMM DD, YYYY');
						// $scope.dateRange = picker.endDate.format('MMM D,
						// YYYY') +
						// ' - ' + picker.startDate.format('MMM D, YYYY');
						$scope.disableCompareDate = true;
						if(picker.chosenLabel == "Today" && ($scope.startDateExpiry == $scope.endDateExpiry)){
							$scope.disableCompareDate = false;
							$scope.varCompareDate = "Equal";
						}
						$scope.dateRange = picker.chosenLabel;

					}
					// toastr.success($scope.dateRange,"EI40")
					$scope.getSKUExpiryDetailRecord();
					$scope.$apply();
					});			
					
					$scope.sortBasedOnExpiryDateFunction = function(a,b) {  
					    var dateA = new Date(a.expiryDate).getTime();
					    var dateB = new Date(b.expiryDate).getTime();
					    return dateA > dateB ? 1 : -1;  
					} 
					 // sku record expire soon data in table
					$scope.getSKUExpiryDetailRecord = function() {
					$scope.DAY_ARRAY = [];
					$scope.FINAL_DAY_ARRAY = [];
					$scope.finalskuDetailData = [];
					
					if ($scope.skuDetailData.length == 0) {
					$scope.skuDetailNoData = true;
					return;
					} else {
						$scope.skuDetailNoData = false;
						function convert(str) {
							var date = new Date(str)
					           // var now_utc = new
								// Date(date.toISOString().slice(0, 10));
					           var mnth = ("0" + (date.getMonth() + 1)).slice(-2);
					           var day = ("0" + date.getDate()).slice(-2);
					           return [date.getFullYear(), mnth, day].join("-");
						}
						var getDatesBetweenDates = (startDate, endDate) => {
					           let dates = []
					               // to avoid
					// modifying the
					// original date
					           const theDate = new Date(startDate);
					           const theendDate = new Date(endDate);

					           while (theDate < theendDate) {
					               dates = [...dates, new Date(theDate)]
					               theDate.setDate(theDate.getDate() + 1)
					           }
					           dates = [...dates, endDate]
					           return dates
					       }
					
						function formatDate1(date) {
							var mS = [ 'Jan', 'Feb', 'Mar', 'Apr', 'May', 'June','Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec' ];
							var sp_date = date.split("-");
							return   (sp_date[0] + "-" + mS[parseInt(sp_date[1]) - 1]+ "-"+  sp_date[2])
						}

						$scope.DAY_ARRAY = getDatesBetweenDates( new Date($scope.startDateExpiry),new Date($scope.endDateExpiry));

						for (var da = 0; da < $scope.DAY_ARRAY.length; da++) {
							$scope.FINAL_DAY_ARRAY[da] = formatDate1(convert($scope.DAY_ARRAY[da].toString()));
						}

						for (var i = 0; i < $scope.skuDetailData.length; i++) {
							if ($scope.FINAL_DAY_ARRAY.includes($scope.skuDetailData[i].skuExpiryDate))	{
								var dateObj = $scope.skuDetailData[i];
								$scope.finalskuDetailData.push(dateObj);
							}
						}
					}
					// to sort based on validity date
					$scope.finalskuDetailData.sort($scope.sortBasedOnExpiryDateFunction);
					
					if ($scope.finalskuDetailData.length > 10) {
						$scope.PaginationTab = true;
					} else {
						$scope.PaginationTab = false;
					}
					$scope.totalItems = $scope.finalskuDetailData.length;
					$scope.getTotalPages();
					$scope.currentPage = 1;

					$scope.getAssignedTagIds($scope.finalskuDetailData);
					
					if ($scope.finalskuDetailData.length > $scope.numPerPage) {
						$scope.skuDetailViewData = $scope.finalskuDetailData.slice( 0,$scope.numPerPage);
						$scope.hasPrevious = true;
						$scope.isFirstPage = true;
						$scope.hasNext = false;
						$scope.isLastPage = false;
					} else {
						$scope.skuDetailViewData = $scope.finalskuDetailData;
					}					
				
					$timeout(function() {
						$scope.getBgColorExpiryTable(); 
					}, 100);
					
					
					if ($scope.skuDetailViewData.length == 0) {
					$scope.skuDetailNoData = true;
					return;
					} else {
					$scope.skuDetailNoData = false;
					}

					} 

					$scope.getBgColorExpiryTable = function() {
						// to give bg-color for table row based on validity date
						
						var selectedRowCSS_Gray = {'background': '#696969', 'color': '#ffffff' };
						var selectedRowCSS_Red = {'background': '#ff0000', 'color': '#ffffff' };
						var selectedRowCSS3_Orange = {'background': '#ff8b00', 'color': '#ffffff' };
						var selectedRowCSS4_Green = {'background': '#00ff00', 'color': '#000000' };
						
 
						for (var i = 0; i < $scope.skuDetailViewData.length; i++) {						
						var validityDateExpiry =  moment($scope.skuDetailViewData[i].expiryDate);
						var currentDay = moment(new Date()).format('MMM DD, YYYY');
						
						if (validityDateExpiry != undefined && validityDateExpiry != null ) {

							var diff = moment(currentDay) - validityDateExpiry;

							 if ( diff == 0) {
									$("#idSKUDetailTable > tbody > tr:nth-child(" + (i + 1) + ")").css(selectedRowCSS_Gray);
							}else  if ( diff>0) {
								$("#idSKUDetailTable > tbody > tr:nth-child(" + (i + 1) + ")").css(selectedRowCSS_Red);
							} 
							 // next 7 days in orange
							else if ( diff <= -86400000 && diff >= -604800000) { 
								$("#idSKUDetailTable > tbody > tr:nth-child(" + (i + 1) + ")").css(selectedRowCSS3_Orange);
							} 
							else   {
								$("#idSKUDetailTable > tbody > tr:nth-child(" + (i + 1) + ")").css(selectedRowCSS4_Green);
							}

						}
						}
						 
						}					
                    // Compare date
					$scope.fnCompareDateChange= function() {
						if ($scope.varCompareDate == "Greater"){
							//toastr.error('Greater.', "EI4.0");
							const tomorrow = new Date();
							tomorrow.setDate(tomorrow.getDate() + 1);
							$scope.startDateExpiry =  moment(tomorrow).format("MMM DD, YYYY");
							$scope.endDateExpiry = moment().add(2, 'year').endOf('year').format('MMM DD, YYYY');
							
						}
						else if ($scope.varCompareDate == "Less"){
							const yesterday = new Date();
							yesterday.setDate(yesterday.getDate() - 1);
							$scope.endDateExpiry =  moment(yesterday).format("MMM DD, YYYY");
							$scope.startDateExpiry  = moment().subtract(2, 'year').endOf('year').format('MMM DD, YYYY');
							

						}
						else if ($scope.varCompareDate == "Equal"){
							$scope.startDateExpiry =  moment(new Date()).format("MMM DD, YYYY");
							$scope.endDateExpiry = moment(new Date()).format("MMM DD, YYYY");
							
						}
						$scope.getSKUExpiryDetailRecord();
							
					}
					// Compare date
					$scope.fnExpiryDateFilter = function() {
						$scope.showMultipleSearch = false;
						$scope.searchButton = false;
						$scope.showSearchIcon = false;
						$scope.showExportCard = false;
						$scope.showCompareDateSelection = true;
						$("#idSKUTableUPCCode").removeClass("sortable")
						$("#idSKUTableSKUNo").removeClass("sortable")
						$("#idSKUTableBatchNo").removeClass("sortable")
						$("#idSKUTableDescription").removeClass("sortable")
						$("#idSKUTableTotalQty").removeClass("sortable")
						$("#idSKUTableAvailQty").removeClass("sortable")
						$("#idSKUTableExpiryDate").removeClass("sortable")
						$("#idSKUTableExpiryDate").removeClass("sortable")
						
						


						// $scope.showCompareDate = false; // show when custom
														// dates are equal
						
						$scope.varCompareDate = "Equal";
						$scope.dateRange = "Today";
						$scope.disableCompareDate = false;
						$scope.startDateExpiry =  moment(new Date()).format("MMM DD, YYYY");
						$scope.endDateExpiry = moment(new Date()).format("MMM DD, YYYY");
						$scope.getSKUExpiryDetailRecord();
						//$scope.$apply();						
					}
					// multisearch code starts
					$scope.fnMultipleSearch = function() {
						
						$("#idSKUTableUPCCode").removeClass("sortable")
						$("#idSKUTableSKUNo").removeClass("sortable")
						$("#idSKUTableBatchNo").removeClass("sortable")
						$("#idSKUTableDescription").removeClass("sortable")
						$("#idSKUTableTotalQty").removeClass("sortable")
						$("#idSKUTableAvailQty").removeClass("sortable")
						$("#idSKUTableExpiryDate").removeClass("sortable")
						$scope.showMultipleSearch = true;
						$scope.searchButton = false;
						$scope.showCompareDateSelection = false;
						$scope.firstMultiSearchDropDown = "All";
						$scope.secondMultiSearchDropDown = "All"
						$scope.varCompareQuantityFirst = "Greater";
						$scope.varCompareQuantitySecond = "Greater";
						$scope.varANDORDropDown = "AND";
						$scope.firstSearchText = "";
						$scope.secondSearchText = "";
						$scope.showDownloadIcon  = false;	
						$scope.showExportCard = false;
					}
					
					$scope.fnCloseCompareDateView = function() {
						$scope.showMultipleSearch = false;
						$scope.searchButton = true;
						$scope.showSearchIcon = true;
						$scope.showCompareDateSelection = false;
						$("#idSKUTableUPCCode").addClass("sortable")
						$("#idSKUTableSKUNo").addClass("sortable")
						$("#idSKUTableBatchNo").addClass("sortable")
						$("#idSKUTableDescription").addClass("sortable")
						$("#idSKUTableTotalQty").addClass("sortable")
						$("#idSKUTableAvailQty").addClass("sortable")
						$("#idSKUTableExpiryDate").addClass("sortable")

						$scope.fnDisplayAllrecords();
						var selectedRowCSS_white = {'background': '#ffffff', 'color': '#000000' };
						for (var i = 0; i < $scope.skuDetailViewData.length; i++) {						
								$("#idSKUDetailTable > tbody > tr:nth-child(" + (i + 1) + ")").css(selectedRowCSS_white);
						}
						//$scope.$apply();	
					}
					$scope.fnCloseMultiSearchView = function() {
						$scope.showDownloadIcon = true; 
						$scope.showMultipleSearch = false;
						$scope.searchButton = true;
						$scope.showCompareDateSelection = false;
						$("#idSKUTableUPCCode").addClass("sortable")
						$("#idSKUTableSKUNo").addClass("sortable")
						$("#idSKUTableBatchNo").addClass("sortable")
						$("#idSKUTableDescription").addClass("sortable")
						$("#idSKUTableTotalQty").addClass("sortable")
						$("#idSKUTableAvailQty").addClass("sortable")
						$("#idSKUTableExpiryDate").addClass("sortable")
						$scope.fnDisplayAllrecords();
					}
					$scope.fnDisplayAllrecords = function() {
						$scope.skuDetailFilterList = $scope.skuDetailData;
						$scope.totalItems = $scope.skuDetailFilterList.length;
						$scope.currentPage = 1;
						$scope.getTotalPages();

						$scope.getAssignedTagIds($scope.skuDetailFilterList);

						if ($scope.skuDetailData.length == 0
								|| $scope.skuDetailFilterList.length == 0) {
							$scope.skuDetailNoData = true;

						} else {
							$scope.skuDetailNoData = false;

							if ($scope.skuDetailFilterList.length > $scope.numPerPage) {
								$scope.skuDetailViewData = JSON.parse(JSON
										.stringify($scope.skuDetailFilterList
												.slice(0, $scope.numPerPage)));
								$scope.PaginationTab = true;
							} else {
								$scope.skuDetailViewData = JSON.parse(JSON
										.stringify($scope.skuDetailFilterList));
								$scope.PaginationTab = false;
							}
						}
					}
					$scope.cancelEdit = function() {
						$scope.selectedRow = -1;
						$scope.trackMacId = '';
					}

					$scope.displaySKUDetailMenuBtn = function() {
						$("#idSKUImportBtn").removeClass("active");
						$("#idSKUDetailBtn").addClass("active");
						$("#idSKUDispatchBtn").removeClass("active");
						
						$scope.showSKUDetailScreen = true;
						$scope.showLocateBtn = true;
						$scope.showDownloadIcon  = true;
						$scope.showImportSKUScreen = false;
						$scope.showSearchIcon = true;
						$scope.showMultipleSearch = false;
						$scope.searchButton = true;
						$scope.searchText = "";	
						$scope.showCompareDateSelection = false;
						$scope.showCardSKUDispatchView = false;
						$scope.skuTabName = "DetailPage";					
						$scope.getSKUDetailData();
					};
					$scope.displaySKUImportMenuBtn = function() {
						$("#idSKUImportBtn").addClass("active");
						$("#idSKUDetailBtn").removeClass("active");
						$("#idSKUDispatchBtn").removeClass("active");
						$scope.showImportSKUScreen = true;
						$scope.showSKUDetailScreen = false;
						$scope.showSearchIcon = false;
						$scope.showMultipleSearch = false;
						$scope.searchButton = false;
						$scope.showCompareDateSelection = false;
						$scope.showCardSKUDispatchView = false;
						$scope.showDownloadIcon  = false;
						$scope.showExportCard = false;
						$scope.skuTabName = "ImportPage";		
						
					};
					$scope.displaySKUDispatchMenuBtn = function() {
						$("#idSKUImportBtn").removeClass("active");
						$("#idSKUDetailBtn").removeClass("active");
						$("#idSKUDispatchBtn").addClass("active");
						
						$scope.showImportSKUScreen = false;
						$scope.showSKUDetailScreen = true;
						$scope.showLocateBtn = false;
						$scope.showSearchIcon = false;
						$scope.showMultipleSearch = false;
						$scope.showCompareDateSelection = false;
						$scope.showCardSKUDispatchView = false;
						$scope.showDownloadIcon  = false;
						$scope.showExportCard = false;
						
						$scope.searchButton = true;
						$scope.searchText = "";	
						$scope.skuTabName = "DispatchPage";			
						$scope.getSKUDetailData();
						
					};
					
					if ($scope.user.capabilityDetails != undefined
							&& $scope.user.capabilityDetails != null) {
						if ($scope.user.capabilityDetails.SDET != 0) {
							$scope.displaySKUDetailMenuBtn();
							}
							if ($scope.user.capabilityDetails.SDET != 1) {
								$scope.displaySKUImportMenuBtn();
							}
							if ($scope.user.capabilityDetails.SDET != 1
							&& $scope.user.capabilityDetails.SIMP != 1) {
								$scope.displaySKUDispatchMenuBtn();
							}

					}
					$scope.getAssignedTagIds = function(skuList) {

						$scope.assignedTagMacId = '';

						if (skuList != undefined && skuList != null) {
							for (var i = 0; i < skuList.length; i++) {
								if (skuList[i].status == 'Valid') {
									if (skuList[i].assignedTagMacId != undefined
											&& skuList[i].assignedTagMacId != '') {
										if ($scope.assignedTagMacId == '')
											$scope.assignedTagMacId = skuList[i].assignedTagMacId;
										else
											$scope.assignedTagMacId = $scope.assignedTagMacId
													+ ","
													+ skuList[i].assignedTagMacId;

									}
								}
							}
						}

					}
					
					// multisearch code starts
					$scope.multipleSearch = function() {
						$scope.showMultipleSearch = true;
						$scope.searchButton = false
						$scope.firstMultiSearchDropDown = "All";
						$scope.secondMultiSearchDropDown = "All";
						$scope.varANDORDropDown = "AND";
						$scope.firstSearchText = "";
						$scope.secondSearchText = "";
					}
					$scope.closeMultiSearchView = function() {
						$scope.showMultipleSearch = false;
						$scope.searchButton = true
						
						$scope.skuDetailFilterList = $scope.skuDetailData;
						$scope.totalItems = $scope.skuDetailFilterList.length;
						$scope.currentPage = 1;
						$scope.getTotalPages();
						
						$scope.getAssignedTagIds($scope.skuDetailFilterList);
						
						 if($scope.skuDetailData.length == 0 || $scope.skuDetailFilterList.length == 0 ){
								$scope.skuDetailNoData = true;

							}else{
								$scope.skuDetailNoData = false;

						if ($scope.skuDetailFilterList.length > $scope.numPerPage){								
							 $scope.skuDetailViewData = JSON.parse(JSON.stringify($scope.skuDetailFilterList.slice(0, $scope.numPerPage))); 
						$scope.PaginationTab=true;
						}
						else {
							$scope.skuDetailViewData = JSON.parse(JSON.stringify($scope.skuDetailFilterList)); 
							$scope.PaginationTab=false;
						}
					}
					}
					
					
					$scope.fnfirstSearchANDData = function() {
						
						$scope.firstSearchANDDataArray = [];			
					for (var i = 0; i < $scope.skuDetailData.length; i++) {
						// to check all
						if ($scope.firstMultiSearchDropDown == "All"){
						if ($scope.firstSearchText == "" || $scope.firstSearchText == undefined) {
							
							$scope.firstSearchANDDataArray.push($scope.skuDetailData[i]);
							
						} else{
							if ((($scope.skuDetailData[i].skuNo).toString() != undefined
									&& ($scope.skuDetailData[i].skuNo).toString() != null ? ($scope.skuDetailData[i].skuNo).toString()
									: "").toLowerCase().includes(
									$scope.firstSearchText.toLowerCase())
									|| ($scope.skuDetailData[i].upcCode != undefined
											&& $scope.skuDetailData[i].upcCode != null ? $scope.skuDetailData[i].upcCode
											: "").toLowerCase()
											.includes(
													$scope.firstSearchText
															.toLowerCase())															
									|| ($scope.skuDetailData[i].batchNo != undefined
											&& $scope.skuDetailData[i].batchNo != null ? $scope.skuDetailData[i].batchNo
											: "").toLowerCase()
											.includes(
													$scope.firstSearchText
															.toLowerCase())															
									|| ($scope.skuDetailData[i].description != undefined
											&& $scope.skuDetailData[i].description != null ? $scope.skuDetailData[i].description
											: "").toLowerCase()
											.includes(
													$scope.firstSearchText
															.toLowerCase())	
									|| (($scope.skuDetailData[i].quantity).toString() != undefined
											&& ($scope.skuDetailData[i].quantity).toString() != null ? ($scope.skuDetailData[i].quantity).toString()
											: "").toLowerCase()
											.includes(
													$scope.firstSearchText
															.toLowerCase())
									|| (($scope.skuDetailData[i].availableQuantity).toString() != undefined
											&& ($scope.skuDetailData[i].availableQuantity).toString() != null ? ($scope.skuDetailData[i].availableQuantity).toString()
											: "").toLowerCase()
											.includes(
													$scope.firstSearchText
															.toLowerCase())
									|| ($scope.skuDetailData[i].transaction != undefined
											&& $scope.skuDetailData[i].transaction != null ? $scope.skuDetailData[i].transaction
											: "").toLowerCase()
											.includes(
													$scope.firstSearchText
															.toLowerCase())
									|| ($scope.skuDetailData[i].expiryDate != undefined
											&& $scope.skuDetailData[i].expiryDate != null ? $scope.skuDetailData[i].expiryDate
											: "").toLowerCase()
											.includes(
													$scope.firstSearchText
															.toLowerCase())){
								$scope.firstSearchANDDataArray
										.push($scope.skuDetailData[i]);

							}

						}
						}
						// to check other than all
						else{
							$scope.showCompareQuantityFirst = false;
							if($scope.firstMultiSearchDropDown == "skuNo"){								
								if ($scope.firstSearchText	== "" || $scope.firstSearchText == undefined) {						
									$scope.firstSearchANDDataArray.push($scope.skuDetailData[i]);						
								}
								else if((($scope.skuDetailData[i].skuNo).toString() != undefined&& ($scope.skuDetailData[i].skuNo).toString() != null ?
										($scope.skuDetailData[i].skuNo).toString(): "").toLowerCase().includes($scope.firstSearchText.toLowerCase()))					
									$scope.firstSearchANDDataArray.push($scope.skuDetailData[i]);
								
							}else	if($scope.firstMultiSearchDropDown == "upcCode"){
								if ($scope.firstSearchText	== "" || $scope.firstSearchText == undefined) {						
									$scope.firstSearchANDDataArray.push($scope.skuDetailData[i]);						
								}
								else if((($scope.skuDetailData[i].upcCode).toString() != undefined&& ($scope.skuDetailData[i].upcCode).toString() != null ?
										($scope.skuDetailData[i].upcCode).toString(): "").toLowerCase().includes($scope.firstSearchText.toLowerCase()))					
									$scope.firstSearchANDDataArray.push($scope.skuDetailData[i]);
								
							}else if($scope.firstMultiSearchDropDown == "batchNo"){
								if ($scope.firstSearchText	== "" || $scope.firstSearchText == undefined) {						
									$scope.firstSearchANDDataArray.push($scope.skuDetailData[i]);						
								}
								else if(($scope.skuDetailData[i].batchNo != undefined
											&& $scope.skuDetailData[i].batchNo != null ? $scope.skuDetailData[i].batchNo: "").toLowerCase()
											.includes($scope.firstSearchText.toLowerCase()))					
										$scope.firstSearchANDDataArray.push($scope.skuDetailData[i]);
								
							}else if($scope.firstMultiSearchDropDown == "description"){
								if ($scope.firstSearchText	== "" || $scope.firstSearchText == undefined) {						
										$scope.firstSearchANDDataArray.push($scope.skuDetailData[i]);						
								}
								else if(($scope.skuDetailData[i].description != undefined
												&& $scope.skuDetailData[i].description != null ? $scope.skuDetailData[i].description: "").toLowerCase()
												.includes($scope.firstSearchText.toLowerCase()))					
											$scope.firstSearchANDDataArray.push($scope.skuDetailData[i]);
								
							}else if($scope.firstMultiSearchDropDown == "quantity"){
								$scope.showCompareQuantityFirst = true;
								
									if ($scope.firstSearchText	== "" || $scope.firstSearchText == undefined) {						
										$scope.firstSearchANDDataArray.push($scope.skuDetailData[i]);						
								}
									else if($scope.varCompareQuantityFirst == "Greater"){
										if($scope.skuDetailData[i].quantity != undefined && $scope.skuDetailData[i].quantity != null){
											if($scope.skuDetailData[i].quantity > $scope.firstSearchText)
												$scope.firstSearchANDDataArray.push($scope.skuDetailData[i]);
										}
											
									}else if($scope.varCompareQuantityFirst == "Less"){
										if($scope.skuDetailData[i].quantity != undefined && $scope.skuDetailData[i].quantity != null){
											if($scope.skuDetailData[i].quantity < $scope.firstSearchText)
												$scope.firstSearchANDDataArray.push($scope.skuDetailData[i]);
										}
											
									}else if($scope.varCompareQuantityFirst == "Equal"){
										if($scope.skuDetailData[i].quantity != undefined && $scope.skuDetailData[i].quantity != null){
											if($scope.skuDetailData[i].quantity == $scope.firstSearchText)
												$scope.firstSearchANDDataArray.push($scope.skuDetailData[i]);
										}
											
									}			
							
						}else if($scope.firstMultiSearchDropDown == "availableQuantity"){
							$scope.showCompareQuantityFirst = true;
							if ($scope.firstSearchText	== "" || $scope.firstSearchText == undefined) {						
								$scope.firstSearchANDDataArray.push($scope.skuDetailData[i]);						
						}
							$scope.showCompareQuantityFirst = true;
							
							if ($scope.firstSearchText	== "" || $scope.firstSearchText == undefined) {						
								$scope.firstSearchANDDataArray.push($scope.skuDetailData[i]);						
						}
							else if($scope.varCompareQuantityFirst == "Greater"){
								if($scope.skuDetailData[i].availableQuantity != undefined && $scope.skuDetailData[i].availableQuantity != null){
									if($scope.skuDetailData[i].availableQuantity > $scope.firstSearchText)
										$scope.firstSearchANDDataArray.push($scope.skuDetailData[i]);
								}
									
							}else if($scope.varCompareQuantityFirst == "Less"){
								if($scope.skuDetailData[i].availableQuantity != undefined && $scope.skuDetailData[i].availableQuantity != null){
									if($scope.skuDetailData[i].availableQuantity < $scope.firstSearchText)
										$scope.firstSearchANDDataArray.push($scope.skuDetailData[i]);
								}
									
							}else if($scope.varCompareQuantityFirst == "Equal"){
								if($scope.skuDetailData[i].availableQuantity != undefined && $scope.skuDetailData[i].availableQuantity != null){
									if($scope.skuDetailData[i].availableQuantity == $scope.firstSearchText)
										$scope.firstSearchANDDataArray.push($scope.skuDetailData[i]);
								}
									
							}	
						
						}else if($scope.firstMultiSearchDropDown == "transaction"){
						if ($scope.firstSearchText	== "" || $scope.firstSearchText == undefined) {						
							$scope.firstSearchANDDataArray.push($scope.skuDetailData[i]);						
						}
							else if(($scope.skuDetailData[i].transaction != undefined
							&& $scope.skuDetailData[i].transaction != null ? $scope.skuDetailData[i].transaction
									: "").toLowerCase().includes($scope.firstSearchText.toLowerCase()))					
								$scope.firstSearchANDDataArray.push($scope.skuDetailData[i]);
					
					}else if($scope.firstMultiSearchDropDown == "expiryDate"){
							if ($scope.firstSearchText	== "" || $scope.firstSearchText == undefined) {						
								$scope.firstSearchANDDataArray.push($scope.skuDetailData[i]);						
							}
							else if(($scope.skuDetailData[i].expiryDate != undefined
									&& $scope.skuDetailData[i].expiryDate != null ? $scope.skuDetailData[i].expiryDate: "").toLowerCase()
								.includes($scope.firstSearchText.toLowerCase()))					
							$scope.firstSearchANDDataArray.push($scope.skuDetailData[i]);
				
					}
				}
				}
					}	
					$scope.multiSearchChanged = function() {
						$scope.firstSearchANDDataArray = [];
						$scope.fnfirstSearchANDData();						
						
						if($scope.varANDORDropDown == "AND"){			
							$scope.disableSecondFilterText = false;
							$scope.disableSecondFilterDropDown = false;
							$scope.skuDetailViewData=[];
							$scope.skuDetailFilterList=[];

							for (var i = 0; i < $scope.firstSearchANDDataArray.length; i++) {
								if ($scope.secondMultiSearchDropDown == "All"){
									
									if ($scope.secondSearchText == "" || $scope.secondSearchText == undefined) {
										$scope.skuDetailFilterList
												.push($scope.firstSearchANDDataArray[i]);
									} else {
										if ((($scope.firstSearchANDDataArray[i].skuNo).toString() != undefined&& ($scope.firstSearchANDDataArray[i].skuNo).toString() != null ?
												($scope.firstSearchANDDataArray[i].skuNo).toString(): "").toLowerCase().includes($scope.secondSearchText.toLowerCase())
												
												|| ($scope.firstSearchANDDataArray[i].upcCode != undefined
														&& $scope.firstSearchANDDataArray[i].upcCode != null ? $scope.firstSearchANDDataArray[i].upcCode: "").toLowerCase()
														.includes($scope.secondSearchText.toLowerCase())
												|| ($scope.firstSearchANDDataArray[i].batchNo != undefined
														&& $scope.firstSearchANDDataArray[i].batchNo != null ? $scope.firstSearchANDDataArray[i].batchNo: "").toLowerCase()
														.includes($scope.secondSearchText.toLowerCase())
												|| ($scope.firstSearchANDDataArray[i].description != undefined
														&& $scope.firstSearchANDDataArray[i].description != null ? $scope.firstSearchANDDataArray[i].description
														: "")
														.toLowerCase()
														.includes(
																$scope.secondSearchText
																		.toLowerCase())
												|| (($scope.firstSearchANDDataArray[i].quantity).toString() != undefined
														&& ($scope.firstSearchANDDataArray[i].quantity).toString() != null ? ($scope.firstSearchANDDataArray[i].quantity).toString()
														: "")
														.toLowerCase()
														.includes(
																$scope.secondSearchText
																		.toLowerCase())
												|| (($scope.firstSearchANDDataArray[i].availableQuantity).toString() != undefined
														&& ($scope.firstSearchANDDataArray[i].availableQuantity).toString() != null ? ($scope.firstSearchANDDataArray[i].availableQuantity).toString()
														: "")
														.toLowerCase()
														.includes(
																$scope.secondSearchText
																		.toLowerCase())
												|| ($scope.firstSearchANDDataArray[i].transaction != undefined
														&& $scope.firstSearchANDDataArray[i].transaction != null ? $scope.firstSearchANDDataArray[i].transaction
														: "")
														.toLowerCase()
														.includes(
																$scope.secondSearchText
																		.toLowerCase())
												|| ($scope.firstSearchANDDataArray[i].expiryDate != undefined
														&& $scope.firstSearchANDDataArray[i].expiryDate != null ? $scope.firstSearchANDDataArray[i].expiryDate: "")
														.toLowerCase().includes($scope.secondSearchText.toLowerCase())) {
											$scope.skuDetailFilterList
													.push($scope.firstSearchANDDataArray[i]);

										}
									}	
								}else{
									$scope.showCompareQuantitySecond = false;
									if($scope.secondMultiSearchDropDown == "upcCode"){
										if ($scope.secondSearchText	== "" || $scope.secondSearchText == undefined) {						
											$scope.skuDetailFilterList.push($scope.firstSearchANDDataArray[i]);						
										}
										else if((($scope.firstSearchANDDataArray[i].upcCode).toString() != undefined && ($scope.firstSearchANDDataArray[i].upcCode).toString() != null ?
												($scope.firstSearchANDDataArray[i].upcCode).toString(): "").toLowerCase().includes($scope.secondSearchText.toLowerCase()))					
											$scope.skuDetailFilterList.push($scope.firstSearchANDDataArray[i]);
										
									}else if($scope.secondMultiSearchDropDown == "skuNo"){
										if ($scope.secondSearchText	== "" || $scope.secondSearchText == undefined) {						
											$scope.skuDetailFilterList.push($scope.firstSearchANDDataArray[i]);						
										}
										else if((($scope.firstSearchANDDataArray[i].skuNo).toString() != undefined&& ($scope.firstSearchANDDataArray[i].skuNo).toString() != null ?
												($scope.firstSearchANDDataArray[i].skuNo).toString(): "").toLowerCase().includes($scope.secondSearchText.toLowerCase()))					
											$scope.skuDetailFilterList.push($scope.firstSearchANDDataArray[i]);
										
									}else if($scope.secondMultiSearchDropDown == "batchNo"){
										if ($scope.secondSearchText	== "" || $scope.secondSearchText == undefined) {						
											$scope.skuDetailFilterList.push($scope.firstSearchANDDataArray[i]);						
										}
										else if(($scope.firstSearchANDDataArray[i].batchNo != undefined
													&& $scope.firstSearchANDDataArray[i].batchNo != null ? $scope.firstSearchANDDataArray[i].batchNo: "").toLowerCase()
													.includes($scope.secondSearchText.toLowerCase()))					
												$scope.skuDetailFilterList.push($scope.firstSearchANDDataArray[i]);
										
									}else if($scope.secondMultiSearchDropDown == "description"){
										if ($scope.secondSearchText	== "" || $scope.secondSearchText == undefined) {						
												$scope.skuDetailFilterList.push($scope.firstSearchANDDataArray[i]);						
										}
										else if(($scope.firstSearchANDDataArray[i].description != undefined
														&& $scope.firstSearchANDDataArray[i].description != null ? $scope.firstSearchANDDataArray[i].description: "").toLowerCase()
														.includes($scope.secondSearchText.toLowerCase()))					
													$scope.skuDetailFilterList.push($scope.firstSearchANDDataArray[i]);
										
									}else if($scope.secondMultiSearchDropDown == "quantity"){
										$scope.showCompareQuantitySecond = true;
										if ($scope.secondSearchText	== "" || $scope.secondSearchText == undefined) {						
											$scope.skuDetailFilterList.push($scope.firstSearchANDDataArray[i]);						
									}else if($scope.varCompareQuantitySecond == "Greater"){
											if($scope.firstSearchANDDataArray[i].quantity != undefined && $scope.firstSearchANDDataArray[i].quantity != null){
												if($scope.firstSearchANDDataArray[i].quantity > parseFloat($scope.secondSearchText))
													$scope.skuDetailFilterList.push($scope.firstSearchANDDataArray[i]);
											}
												
										}else if($scope.varCompareQuantitySecond == "Less"){
											if($scope.firstSearchANDDataArray[i].quantity != undefined && $scope.firstSearchANDDataArray[i].quantity != null){
												if($scope.firstSearchANDDataArray[i].quantity < parseFloat($scope.secondSearchText))
													$scope.skuDetailFilterList.push($scope.firstSearchANDDataArray[i]);
											}
												
										}else if($scope.varCompareQuantitySecond == "Equal"){
											if($scope.firstSearchANDDataArray[i].quantity != undefined && $scope.firstSearchANDDataArray[i].quantity != null){
												if($scope.firstSearchANDDataArray[i].quantity == parseFloat($scope.secondSearchText))
													$scope.skuDetailFilterList.push($scope.firstSearchANDDataArray[i]);
											}
												
										}		
										
								}else if($scope.secondMultiSearchDropDown == "availableQuantity"){
									$scope.showCompareQuantitySecond = true;
									if ($scope.secondSearchText	== "" || $scope.secondSearchText == undefined) {						
										$scope.skuDetailFilterList.push($scope.firstSearchANDDataArray[i]);						
								}
									else if($scope.varCompareQuantitySecond == "Greater"){
										if($scope.firstSearchANDDataArray[i].availableQuantity != undefined && $scope.firstSearchANDDataArray[i].availableQuantity != null){
											if($scope.firstSearchANDDataArray[i].availableQuantity > parseFloat($scope.secondSearchText))
												$scope.skuDetailFilterList.push($scope.firstSearchANDDataArray[i]);
										}
											
									}else if($scope.varCompareQuantitySecond == "Less"){
										if($scope.firstSearchANDDataArray[i].availableQuantity != undefined && $scope.firstSearchANDDataArray[i].availableQuantity != null){
											if($scope.firstSearchANDDataArray[i].availableQuantity < parseFloat($scope.secondSearchText))
												$scope.skuDetailFilterList.push($scope.firstSearchANDDataArray[i]);
										}
											
									}else if($scope.varCompareQuantitySecond == "Equal"){
										if($scope.firstSearchANDDataArray[i].availableQuantity != undefined && $scope.firstSearchANDDataArray[i].availableQuantity != null){
											if($scope.firstSearchANDDataArray[i].availableQuantity == parseFloat($scope.secondSearchText))
												$scope.skuDetailFilterList.push($scope.firstSearchANDDataArray[i]);
										}
											
									}	
								
								}else if($scope.secondMultiSearchDropDown == "transaction"){
								if ($scope.secondSearchText	== "" || $scope.secondSearchText == undefined) {						
									$scope.skuDetailFilterList.push($scope.firstSearchANDDataArray[i]);						
								}
									else if(($scope.firstSearchANDDataArray[i].transaction != undefined
									&& $scope.firstSearchANDDataArray[i].transaction != null ? $scope.firstSearchANDDataArray[i].transaction
											: "").toLowerCase().includes($scope.secondSearchText.toLowerCase()))					
										$scope.skuDetailFilterList.push($scope.firstSearchANDDataArray[i]);
							
							}else if($scope.secondMultiSearchDropDown == "expiryDate"){
									if ($scope.secondSearchText	== "" || $scope.secondSearchText == undefined) {						
										$scope.skuDetailFilterList.push($scope.firstSearchANDDataArray[i]);						
									}
									else if(($scope.firstSearchANDDataArray[i].expiryDate != undefined
											&& $scope.firstSearchANDDataArray[i].expiryDate != null ? $scope.firstSearchANDDataArray[i].expiryDate: "").toLowerCase()
										.includes($scope.secondSearchText.toLowerCase()))					
									$scope.skuDetailFilterList.push($scope.firstSearchANDDataArray[i]);
						
								}
							}
							}
							$scope.totalItems = $scope.skuDetailFilterList.length;
							$scope.currentPage = 1;
							$scope.getTotalPages();
							$scope.getAssignedTagIds($scope.skuDetailFilterList);
							
							 if($scope.skuDetailData.length == 0 || $scope.skuDetailFilterList.length == 0 ){
									$scope.skuDetailNoData = true;

								}else{
									$scope.skuDetailNoData = false;

							if ($scope.skuDetailFilterList.length > $scope.numPerPage){
								$scope.skuDetailViewData = $scope.skuDetailFilterList
										.slice(0, $scope.numPerPage);
							$scope.PaginationTab=true;
							}
							else {
								$scope.skuDetailViewData = $scope.skuDetailFilterList;
								$scope.PaginationTab=false;
							}
						}	
							
						
						}else if($scope.varANDORDropDown == "OR"){
							
							if ($scope.firstMultiSearchDropDown == "All" && ($scope.firstSearchText == "" || $scope.firstSearchText == undefined)){
								$scope.disableSecondFilterText = true;
								$scope.disableSecondFilterDropDown = true;
								$scope.secondMultiSearchDropDown = "All"
								$scope.secondSearchText = ""
								
							}else{
								$scope.disableSecondFilterText = false;
								$scope.disableSecondFilterDropDown = false;
							}
							
							// $scope.skuDetailViewData.length = 0;
							// $scope.skuDetailFilterList.length = 0;
							$scope.skuDetailViewData = []
							$scope.skuDetailFilterList = [];
							$scope.secondSearchORDataArray = [];
							for (var i = 0; i < $scope.skuDetailData.length; i++) {
								if ($scope.secondMultiSearchDropDown == "All"){
									if ($scope.secondSearchText == "" || $scope.secondSearchText == undefined) {
										$scope.secondSearchORDataArray
												.push($scope.skuDetailData[i]);
										
										
									} else {
										if ((($scope.skuDetailData[i].skuNo).toString() != undefined&& ($scope.skuDetailData[i].skuNo).toString() != null ?
												($scope.skuDetailData[i].skuNo).toString(): "").toLowerCase().includes($scope.secondSearchText.toLowerCase())
												|| ($scope.skuDetailData[i].upcCode != undefined
														&& $scope.skuDetailData[i].upcCode != null ? $scope.skuDetailData[i].upcCode: "").toLowerCase()
														.includes($scope.secondSearchText.toLowerCase())
												|| ($scope.skuDetailData[i].batchNo != undefined
														&& $scope.skuDetailData[i].batchNo != null ? $scope.skuDetailData[i].batchNo: "").toLowerCase()
														.includes($scope.secondSearchText.toLowerCase())
												|| ($scope.skuDetailData[i].description != undefined
														&& $scope.skuDetailData[i].description != null ? $scope.skuDetailData[i].description
														: "")
														.toLowerCase()
														.includes(
																$scope.secondSearchText
																		.toLowerCase())
												|| (($scope.skuDetailData[i].quantity).toString() != undefined
														&& ($scope.skuDetailData[i].quantity).toString() != null ? ($scope.skuDetailData[i].quantity).toString()
														: "")
														.toLowerCase()
														.includes(
																$scope.secondSearchText
																		.toLowerCase())
												|| (($scope.skuDetailData[i].availableQuantity).toString() != undefined
														&& ($scope.skuDetailData[i].availableQuantity).toString() != null ? ($scope.skuDetailData[i].availableQuantity).toString()
														: "")
														.toLowerCase()
														.includes(
																$scope.secondSearchText
																		.toLowerCase())
												|| ($scope.skuDetailData[i].transaction != undefined
														&& $scope.skuDetailData[i].transaction != null ? $scope.skuDetailData[i].transaction
														: "")
														.toLowerCase()
														.includes(
																$scope.secondSearchText
																		.toLowerCase())
												|| ($scope.skuDetailData[i].expiryDate != undefined
														&& $scope.skuDetailData[i].expiryDate != null ? $scope.skuDetailData[i].expiryDate
														: "")
														.toLowerCase()
														.includes(
																$scope.secondSearchText
																		.toLowerCase())) {
											$scope.secondSearchORDataArray
													.push($scope.skuDetailData[i]);
											

										}
									}	
								}else{

									// to check other than all in second
									// search
									$scope.showCompareQuantitySecond = false;
									if($scope.secondMultiSearchDropDown == "upcCode"){
										if ($scope.secondSearchText	== "" || $scope.secondSearchText == undefined) {						
											$scope.secondSearchORDataArray.push($scope.skuDetailData[i]);						
										}
										else if((($scope.skuDetailData[i].upcCode).toString() != undefined&& ($scope.skuDetailData[i].upcCode).toString() != null ?
												($scope.skuDetailData[i].upcCode).toString(): "").toLowerCase().includes($scope.secondSearchText.toLowerCase()))					
											$scope.secondSearchORDataArray.push($scope.skuDetailData[i]);
										
									}else if($scope.secondMultiSearchDropDown == "skuNo"){
										if ($scope.secondSearchText	== "" || $scope.secondSearchText == undefined) {						
											$scope.secondSearchORDataArray.push($scope.skuDetailData[i]);						
										}
										else if((($scope.skuDetailData[i].skuNo).toString() != undefined&& ($scope.skuDetailData[i].skuNo).toString() != null ?
												($scope.skuDetailData[i].skuNo).toString(): "").toLowerCase().includes($scope.secondSearchText.toLowerCase()))					
											$scope.secondSearchORDataArray.push($scope.skuDetailData[i]);
										
									}else if($scope.secondMultiSearchDropDown == "batchNo"){
										if ($scope.secondSearchText	== "" || $scope.secondSearchText == undefined) {						
											$scope.secondSearchORDataArray.push($scope.skuDetailData[i]);						
										}
										else if(($scope.skuDetailData[i].batchNo != undefined
													&& $scope.skuDetailData[i].batchNo != null ? $scope.skuDetailData[i].batchNo: "").toLowerCase()
													.includes($scope.secondSearchText.toLowerCase()))					
												$scope.secondSearchORDataArray.push($scope.skuDetailData[i]);
										
									}else if($scope.secondMultiSearchDropDown == "description"){
										if ($scope.secondSearchText	== "" || $scope.secondSearchText == undefined) {						
												$scope.secondSearchORDataArray.push($scope.skuDetailData[i]);						
										}
										else if(($scope.skuDetailData[i].description != undefined
														&& $scope.skuDetailData[i].description != null ? $scope.skuDetailData[i].description: "").toLowerCase()
														.includes($scope.secondSearchText.toLowerCase()))					
													$scope.secondSearchORDataArray.push($scope.skuDetailData[i]);
										
									}else if($scope.secondMultiSearchDropDown == "quantity"){										
								
										$scope.showCompareQuantitySecond = true;
										if ($scope.secondSearchText	== "" || $scope.secondSearchText == undefined) {						
											$scope.secondSearchORDataArray.push($scope.skuDetailData[i]);						
									}
										else if($scope.varCompareQuantitySecond == "Greater"){
											if($scope.skuDetailData[i].quantity != undefined && $scope.skuDetailData[i].quantity != null){
												if($scope.skuDetailData[i].quantity > parseFloat($scope.secondSearchText))
													$scope.secondSearchORDataArray.push($scope.skuDetailData[i]);
											}
												
										}else if($scope.varCompareQuantitySecond == "Less"){
											if($scope.skuDetailData[i].quantity != undefined && $scope.skuDetailData[i].quantity != null){
												if($scope.skuDetailData[i].quantity < parseFloat($scope.secondSearchText))
													$scope.secondSearchORDataArray.push($scope.skuDetailData[i]);
											}
												
										}else if($scope.varCompareQuantitySecond == "Equal"){
											if($scope.skuDetailData[i].quantity != undefined && $scope.skuDetailData[i].quantity != null){
												if($scope.skuDetailData[i].quantity == parseFloat($scope.secondSearchText))
													$scope.secondSearchORDataArray.push($scope.skuDetailData[i]);
											}
												
										}	
									
									}else if($scope.secondMultiSearchDropDown == "availableQuantity"){										
								
										$scope.showCompareQuantitySecond = true;
										if ($scope.secondSearchText	== "" || $scope.secondSearchText == undefined) {						
											$scope.secondSearchORDataArray.push($scope.skuDetailData[i]);						
									}
										else if($scope.varCompareQuantitySecond == "Greater"){
											if($scope.skuDetailData[i].availableQuantity != undefined && $scope.skuDetailData[i].availableQuantity != null){
												if($scope.skuDetailData[i].availableQuantity > parseFloat($scope.secondSearchText))
													$scope.secondSearchORDataArray.push($scope.skuDetailData[i]);
											}
												
										}else if($scope.varCompareQuantitySecond == "Less"){
											if($scope.skuDetailData[i].availableQuantity != undefined && $scope.skuDetailData[i].availableQuantity != null){
												if($scope.skuDetailData[i].availableQuantity < parseFloat($scope.secondSearchText))
													$scope.secondSearchORDataArray.push($scope.skuDetailData[i]);
											}
												
										}else if($scope.varCompareQuantitySecond == "Equal"){
											if($scope.skuDetailData[i].availableQuantity != undefined && $scope.skuDetailData[i].availableQuantity != null){
												if($scope.skuDetailData[i].availableQuantity == parseFloat($scope.secondSearchText))
													$scope.secondSearchORDataArray.push($scope.skuDetailData[i]);
											}
												
										}	
									
									}else if($scope.secondMultiSearchDropDown == "transaction"){
								if ($scope.secondSearchText	== "" || $scope.secondSearchText == undefined) {						
									$scope.secondSearchORDataArray.push($scope.skuDetailData[i]);						
								}
									else if(($scope.skuDetailData[i].transaction != undefined
									&& $scope.skuDetailData[i].transaction != null ? $scope.skuDetailData[i].transaction
											: "").toLowerCase().includes($scope.secondSearchText.toLowerCase()))					
										$scope.secondSearchORDataArray.push($scope.skuDetailData[i]);
							
							}else if($scope.secondMultiSearchDropDown == "expiryDate"){
									if ($scope.secondSearchText	== "" || $scope.secondSearchText == undefined) {						
										$scope.secondSearchORDataArray.push($scope.skuDetailData[i]);						
									}
									else if(($scope.skuDetailData[i].expiryDate != undefined
											&& $scope.skuDetailData[i].expiryDate != null ? $scope.skuDetailData[i].expiryDate: "").toLowerCase()
										.includes($scope.secondSearchText.toLowerCase()))					
									$scope.secondSearchORDataArray.push($scope.skuDetailData[i]);
						
								}
									
								}
							}
							// to concat first and second search arrays
							for (var i = 0; i <  $scope.firstSearchANDDataArray.length; i++) {
								if (!$scope.secondSearchORDataArray.includes( $scope.firstSearchANDDataArray[i]))
									$scope.secondSearchORDataArray.push( $scope.firstSearchANDDataArray[i]);
							}							
							$scope.skuDetailFilterList = $scope.secondSearchORDataArray;
							// $scope.skuDetailFilterList =
							// ($scope.firstSearchANDDataArray).concat($scope.secondSearchORDataArray);
							
							// for pagination
							$scope.totalItems = $scope.skuDetailFilterList.length;
							$scope.currentPage = 1;
							$scope.getTotalPages();
							$scope.getAssignedTagIds($scope.skuDetailFilterList);
							
							 if($scope.skuDetailData.length == 0 || $scope.skuDetailFilterList.length == 0 ){
									$scope.skuDetailNoData = true;

								}else{
									$scope.skuDetailNoData = false;

							if ($scope.skuDetailFilterList.length > $scope.numPerPage){								
								 $scope.skuDetailViewData = JSON.parse(JSON.stringify($scope.skuDetailFilterList.slice(0, $scope.numPerPage))); 
							$scope.PaginationTab=true;
							}
							else {
								$scope.skuDetailViewData = JSON.parse(JSON.stringify($scope.skuDetailFilterList)); 
								$scope.PaginationTab=false;
							}
						}	
						}
						
					}
					
					
					//search and pagination code---- 
					$scope.fnSearchOption = function() {
						$scope.filterOption();
						}

						$scope.filterOption = function() {
						var indexArray = [];
						$scope.skuDetailViewData = [];
						$scope.skuDetailFilterList = [];	
						$scope.keyNames = ["upcCode", "skuNo", "batchNo", "description", "quantity","availableQuantity", "transaction", "expiryDate"];
						
						for (var i = 0; i < $scope.skuDetailData.length; i++) {
						if ($scope.searchText == '') {
						$scope.skuDetailFilterList
						.push($scope.skuDetailData[i]);
						} else {
						var obj = {};
						obj = $scope.skuDetailData[i];
						for(var x = 0; x < $scope.keyNames.length; x++){
						var tableText = JSON.stringify($scope.skuDetailData[i][$scope.keyNames[x]]);

						if ((tableText != undefined && tableText != null ? tableText : "").toLowerCase()
						.includes($scope.searchText.toLowerCase()))
						{
						$scope.skuDetailFilterList.push($scope.skuDetailData[i]);
						break;
						}
						}
						}
						}
						// pagination code
						$scope.totalItems = $scope.skuDetailFilterList.length;
						$scope.currentPage = 1;
						$scope.getTotalPages();

						if ($scope.skuDetailFilterList.length == 0) {
						$scope.skuDetailNoData = true;						
						
						} else {
						$scope.skuDetailNoData = false;						
						
						$scope.searchButton = true;
						if ($scope.skuDetailFilterList.length > $scope.numPerPage) {
							$scope.skuDetailViewData = $scope.skuDetailFilterList
									.slice(0, $scope.numPerPage);
							$scope.PaginationTab = true;
						} else {
							$scope.skuDetailViewData = $scope.skuDetailFilterList;
							$scope.PaginationTab = false;
						}
						}


						}
						// pagination button

						$scope.currentPage = 1;
						$scope.numPerPage = 10;
						//$scope.maxSize = 3;
						$scope.totalItems = 0;
						$scope.skuDetailViewData = [];
						$scope.skuDetailFilterList = [];
						$scope
						.$watch(
						'currentPage + numPerPage',
						function() {
						var begin = (($scope.currentPage - 1) * $scope.numPerPage), end = begin + $scope.numPerPage;
						
						if($scope.searchText == undefined || $scope.searchText == "")
							
						$scope.skuDetailViewData = $scope.skuDetailData
						.slice(begin, end);
						else
						$scope.skuDetailViewData = $scope.skuDetailFilterList
						.slice(begin, end);



						$scope.enablePageButtons();

						});

						$scope.currentPage = 1;
						//$scope.maxSize = 3;
						$scope.numPerPage = 10;
						$scope.totalItems = 0;
						$scope.totalPages = 0;

						$scope.hasNext = true;
						$scope.hasPrevious = true;
						$scope.isFirstPage = true;
						$scope.isLastPage = true;
						$scope.serial = 1;

						$scope
						.$watch(
						"currentPage",
						function(newVal, oldVal) {
						if (newVal !== oldVal) {

						if (newVal > $scope.totalPages) {
						$scope.currentPage = $scope.totalPages;
						} else {
						$scope.currentPage = newVal;
						}

						$scope.serial = (($scope.currentPage - 1) * $scope.numPerPage) + 1;
						$scope.enablePageButtons();
						}
						});

						$scope.enablePageButtons = function() {
						$scope.serial = (($scope.currentPage - 1) * $scope.numPerPage) + 1;
						$scope.selectedRow = -1;
						if ($scope.totalPages <= 1) {
						$scope.hasPrevious = true;
						$scope.isFirstPage = true;
						$scope.hasNext = true;
						$scope.isLastPage = true;

						} else if ($scope.totalPages > 1
						&& $scope.currentPage == $scope.totalPages) {

						$scope.hasPrevious = false;
						$scope.isFirstPage = false;
						$scope.hasNext = true;
						$scope.isLastPage = true;
						} else if ($scope.currentPage != 1
						&& $scope.currentPage < $scope.totalPages) {

						$scope.hasPrevious = false;
						$scope.isFirstPage = false;
						$scope.hasNext = false;
						$scope.isLastPage = false;

						} else {
						$scope.hasPrevious = true;
						$scope.isFirstPage = true;
						$scope.hasNext = false;
						$scope.isLastPage = false;
						}

						};

						$scope.getTotalPages = function() {
						if (($scope.totalItems % $scope.numPerPage) > 0)
						$scope.totalPages = parseInt($scope.totalItems
						/ $scope.numPerPage) + 1;
						else
						$scope.totalPages = parseInt($scope.totalItems
						/ $scope.numPerPage);

						$scope.enablePageButtons();
						};

						$scope.goFirstPage = function() {
						$scope.currentPage = 1;
						$scope.enablePageButtons();
						};

						$scope.goPreviousPage = function() {
						if ($scope.currentPage > 1)
						$scope.currentPage = $scope.currentPage - 1;
						$scope.enablePageButtons();
						};

						$scope.goNextPage = function() {
						if ($scope.currentPage <= $scope.totalPages)
						$scope.currentPage = $scope.currentPage + 1;
						$scope.enablePageButtons();
						};

						$scope.goLastPage = function() {
						$scope.currentPage = $scope.totalPages;
						$scope.enablePageButtons();
						};
						//EXPORT code
						$scope.exportDataFn = function(){
							$scope.exportArrayData = [];
							$scope.exportArrayDataForSort = [];
						//console.log($scope.skuDetailData);
						$scope.exportArrayDataForSort = $scope.skuDetailData.slice();
						$scope.exportArrayDataForSort.sort($rootScope.GetSortOrderByDate("expiryDate", true));
						if($scope.allAndLslmDropDown == "All"){
						for(var i=0; i<$scope.exportArrayDataForSort.length; i++){
							$scope.exportArrayData.push({					
								'S.No':i+1,
								'UPC Code':$scope.exportArrayDataForSort[i].upcCode,
								'SKU No': $scope.exportArrayDataForSort[i].skuNo,
								'Batch No':$scope.exportArrayDataForSort[i].batchNo,
								'Description':$scope.exportArrayDataForSort[i].description,
								'Total Quantity':$scope.exportArrayDataForSort[i].quantity,
								'Available Quantity':$scope.exportArrayDataForSort[i].availableQuantity,
								'Transaction':$scope.exportArrayDataForSort[i].transaction,
								'Expiry Date':$scope.exportArrayDataForSort[i].expiryDate
								
							});	
						}
						}
						else if($scope.allAndLslmDropDown == "LSLM"){
							var j =0;
							for(var i=0; i<$scope.exportArrayDataForSort.length; i++){
								if( $scope.exportArrayDataForSort[i].skuExpiryDate != null && $scope.exportArrayDataForSort[i].skuExpiryDate != ""){
									j=j+1;
								$scope.exportArrayData.push({					
									'S.No':j,
									'UPC Code':$scope.exportArrayDataForSort[i].upcCode,
									'SKU No': $scope.exportArrayDataForSort[i].skuNo,
									'Batch No':$scope.exportArrayDataForSort[i].batchNo,
									'Description':$scope.exportArrayDataForSort[i].description,
									'Total Quantity':$scope.exportArrayDataForSort[i].quantity,
									'Available Quantity':$scope.exportArrayDataForSort[i].availableQuantity,
									'Transaction':$scope.exportArrayDataForSort[i].transaction,
									'Expiry Date':$scope.exportArrayDataForSort[i].expiryDate
									
								});	
								}
							}
							
						}
						}
						//EXPORT code
						
						$scope.exportExcelData=function(){
														
							if (!$rootScope.checkNetconnection()) { 
								$rootScope.alertDialogChecknet();
								return;
								}
						   	 else{
						   		$rootScope.exportExcelDataFlag = true;
						   		var filename  = "SKU_Detail";
						   		$rootScope.exportXLSPDFFileName(filename);
							
							var headerArray=[];
							var detailarray=[];
							var descarray=[];
							if($scope.allAndLslmDropDown == "All"){
							 detailarray=['Report Generated by','Report Generated Date & Time','Report','Reported data'];
							descarray=[$scope.user.userName, $rootScope.exportDate+ ", "+$rootScope.exportTime1,"SKU Detail","All Data"];
							}
							else if($scope.allAndLslmDropDown == "LSLM"){
								detailarray=['Report Generated by','Report Generated Date & Time','Report','Reported data'];
								descarray=[$scope.user.userName, $rootScope.exportDate+ ", "+$rootScope.exportTime1,"SKU Detail","LSLM Only"];
							}
							
							for(var i=0; i<detailarray.length; i++){
								headerArray.push( {
									'Details': detailarray[i],				
									'Description':descarray[i] ,				
								});		    
							}
							
							$scope.exportDataFn();
							
							//var export_file_name = "SKU_DETAIL_"	+ "_"+ exportDate + "_"	+ exportTime1 + ".xlsx";
							
							$rootScope.exportAsExcelFn($scope.exportArrayData,headerArray);
							
							
							}
						
						}
						
						
						$scope.exportPDFData=function(){
							if (!$rootScope.checkNetconnection()) { 
								$rootScope.alertDialogChecknet();
								return;
								}
						   	 else{
						   		$rootScope.SKUPDFDownloadFlag = true;
							function getTarget(e) {
								  return e.target || e.srcElement;			  
								}
							var filename  = "SKU_Detail";
							$rootScope.exportXLSPDFFileName(filename);
							
							if($scope.allAndLslmDropDown == "All"){
								$rootScope.allDropDownSet = true;
								
								}else if($scope.allAndLslmDropDown == "LSLM"){
									$rootScope.LslmDropDownSet = true;
									
								}
							$scope.exportDataFn();
							var columns= [], keyNames =[];
							getAllKeys(keyNames,$scope.exportArrayData, '');
							for (var i = 0; i < keyNames.length; i++) {
								if (!columns.includes(keyNames[i]['key']))
									columns.push(keyNames[i]['key']);
								
								}
							
							var rowsToDisplay =[]; var rows =[];
							
							for (var i = 0; i < $scope.exportArrayData.length; i++) {
								rowsToDisplay =[ $scope.exportArrayData[i]["S.No"],$scope.exportArrayData[i]["UPC Code"],
									$scope.exportArrayData[i]["SKU No"],$scope.exportArrayData[i]["Batch No"],
									$scope.exportArrayData[i]["Description"],$scope.exportArrayData[i]["Total Quantity"],
									$scope.exportArrayData[i]["Available Quantity"],$scope.exportArrayData[i]["Transaction"],
										$scope.exportArrayData[i]["Expiry Date"] ];
								 rows.push(rowsToDisplay);
							}
							
							$rootScope.exportPDFfn(columns,rows, 8); //8-Expiry Date Index
								
						   	 }	

					}
						function getAllKeys(keys, obj, path) {
							for (key in obj) {
								var currpath = path + '/' + key;

								if (typeof (obj[key]) == 'object'
										|| (obj[key] instanceof Array))
									getAllKeys(keys, obj[key], currpath);
								else
									keys.push({
										'key' : key,
										"path" : currpath,
										"value" : obj[key]
									});
							}
						}
						$scope.fnExportButtons = function(){
							$scope.showExportCard = true;
							$scope.allAndLslmDropDown = "All";
						}
						$scope.fnCloseExportView = function() {
							$scope.showExportCard = false;
							$scope.showMultipleSearch = false;
							$scope.searchButton = true;
							$scope.showCompareDateSelection = false;
							
						}
				});
