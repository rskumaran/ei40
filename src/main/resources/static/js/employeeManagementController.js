app
		.controller(
				'employeeManagementController',
				function($scope, $http, $window, $rootScope, $location,
						$timeout) {
					$scope.user = JSON.parse($window.sessionStorage
							.getItem("user"));
					if ($scope.user == null) {
						$location.path('/login');
						return;
					}
					if ($scope.authen == undefined) {
						$scope.authen = {};
					}
					if ($scope.addEmployee == undefined) {
						$scope.addEmployee = {};
					}
					if ($scope.multiEmployee == undefined) {
						$scope.multiEmployee = {};
					}
					$scope.employeeStatus =["Working", "Left", "Blocked"];
					$scope.addEmployee.countryCode = $rootScope.countryCodes[0];
					$scope.addEmployee.status = $scope.employeeStatus[0];

					$scope.employeeViewScreen = true;
					$scope.searchButton = true;

					$scope.assignedTagMacId = '';
					$scope.trackMacId = '';

					$scope.empList = new Array();
					$scope.empViewData = new Array();
					$scope.employeeFilterList = new Array();
					$scope.firstSearchANDDataArray = new Array();
					$scope.secondSearchORDataArray = new Array();

					$scope.empList = [];
					$scope.empViewData = [];
					$scope.employeeFilterList = [];
					$scope.searchText = "";
					$scope.firstSearchText = "";
					$scope.secondSearchText = "";
					
					$scope.jsonContent = [];
					$scope.jsonMultipleEmployees = new Array();
					$scope.csvContent =[];
					$scope.ext ="";
					//$rootScope.isEmpManagement = false ;
					//$rootScope.isEmpManagementEdit = false ;
					
					//$scope.locationInfo ="";
					
					$scope.keyNames = new Array(); // for bulk employees
					$scope.jsonDataArray = new Array(); // for bulk employees
					var multipleEmployees = new Array();
					$scope.errorArray =new Array(); // error detail array for multiple material upload
					
					var inputfieldRegex = /^[\x20-\x7E]+$/;
					var nameRegex = /^[a-zA-Z0-9]*$/;
					var empPhoneRegex = /^[0-9+]*$/;
					
					$('.select2').select2(); // searchable dropdown  in fetch screen intialization
					
					$scope.showLocateBtn = false;
					$scope.user = JSON.parse($window.sessionStorage
							.getItem("user"));
					if ($scope.user.capabilityDetails != undefined
							&& $scope.user.capabilityDetails != null) {
						if ($scope.user.capabilityDetails.PEVA == 1) {
							$scope.showViewAllBtn = true;
						} else {
							$scope.showViewAllBtn = false;
						}
						if ($scope.user.capabilityDetails.PEAD == 1) {
							$scope.showAddBtn = true;
						} else {
							$scope.showAddBtn = false;
						}
						if ($scope.user.capabilityDetails.PMEA == 1) {
							$scope.showMultiAddBtn = true;
						} else {
							$scope.showMultiAddBtn = false;
						}
						
						if ($scope.user.capabilityDetails.PEED == 1) {
							$scope.showEditBtn = true;
						} else {
							$scope.showEditBtn = false;
						}
						if ($scope.user.capabilityDetails.PLVW == 1) {
							$scope.showLocateBtn = true;
						}
					}
					
					$scope.editEmployeeValue = function() {
						$scope.searchButton = false;
						$scope.employeeEditScreen = false;
						$scope.employeeEditUpdateScreen = true;
						
						
					}

					$scope.updateEmployee = function() {
						$scope.employeeEditScreen = true;
						$scope.employeeEditUpdateScreen = false;
						$scope.searchButton = true;
					}

					// row highlighting
					$scope.rowHighilited = function(row) {
						$scope.selectedRow = row;
						
						$scope.authen = JSON.parse(JSON
								.stringify($scope.empViewData[row]));
						var phoneNo = $scope.empViewData[row].phoneNumber;
						var cCode = "";
						var phone = "";
						if ((phoneNo.length == 12) || (phoneNo.length == 13)) {
							cCode = phoneNo.split(phoneNo.slice(-10))[0];
							phone = phoneNo.substr(phoneNo.length - 10);
						} else if (phoneNo == "+1") {
							cCode = "+1";
							phone = "";
						} else if (phoneNo == "+91") {
							cCode = "+1";
							phone = "";
						} else if (phoneNo == "") {
							cCode = "+1";
							phone = "";
						} else {
							cCode = phoneNo.split(phoneNo.slice(-10))[0];
							phone = phoneNo.substr(phoneNo.length - 10);
							;
						}
						$scope.authen.countryCode = cCode;
						$scope.authen.phoneNumber = phone;

						if ($scope.locationList
								.indexOf($scope.authen.location1) < 0)
							$scope.authen.location1 = $scope.locationList[0];
						if ($scope.locationList
								.indexOf($scope.authen.location2) < 0)
							$scope.authen.location2 = $scope.locationList[0];
						if ($scope.locationList
								.indexOf($scope.authen.location3) < 0)
							$scope.authen.location3 = $scope.locationList[0];
						if ($scope.shiftList
								.indexOf($scope.authen.shiftDetails.shiftName) < 0)
							$scope.authen.shiftDetails.shiftName = $scope.shiftList[0];

						if ($scope.employeeViewScreen) {
							$scope.assignedStatus = $scope.authen.assignedStatus;
							if ($scope.assignedStatus == 'Assigned') {
								$scope.trackMacId = $scope.authen.assignedTagMacId;
							} else {
								$scope.trackMacId = '';

							}
						} else {
							$scope.trackMacId = '';
						}

					}

					$scope.cancelEdit = function() {
						$scope.selectedRow = -1;
						$scope.trackMacId = '';
					}

					// kumar 05-Jul-21 ->
					$scope.fnEmpSearchOption = function() {
						$scope.fnSearchFilterOption();
					}

					$scope.trackTag = function() {
						if ($scope.assignedTagMacId != undefined
								&& $scope.assignedTagMacId != null
								&& $scope.assignedTagMacId != '') {
							$rootScope.showFullView();
							$rootScope.trackingMacId = $scope.assignedTagMacId;
							var currentMenuName = $window.sessionStorage.getItem("menuName");
							if ($scope.user.role == "DemoUser")
							{
								$window.sessionStorage.setItem("menuName", "shopFloorAnalysis");
								$window.sessionStorage.setItem("demoMenuName", "peopleLiveView");
								$location.path('/shopFloorAnalysis');
							}else{
								$window.sessionStorage.setItem("menuName", "peopleLiveView");
								$location.path('/peopleLiveView');
							}
							$rootScope.$broadcast('sourcePreviousScreen',currentMenuName);
						} else {
							toastr.error('No assigned tags to track.', "EI4.0");
						}

					}

					$scope.trackSingleTag = function() {
						if ($scope.trackMacId != undefined
								&& $scope.trackMacId != null
								&& $scope.trackMacId != '') {
							$rootScope.trackingMacId = $scope.trackMacId;
							$rootScope.showFullView();
							var currentMenuName = $window.sessionStorage.getItem("menuName");
							if ($scope.user.role == "DemoUser")
							{
								$window.sessionStorage.setItem("menuName", "shopFloorAnalysis");
								$window.sessionStorage.setItem("demoMenuName", "peopleLiveView");
								$location.path('/shopFloorAnalysis');
							}else{
								$window.sessionStorage.setItem("menuName", "peopleLiveView");
								$location.path('/peopleLiveView');
							}
							$rootScope.$broadcast('sourcePreviousScreen',currentMenuName);
						} else {
							toastr.error('No assigned tags to track.', "EI4.0");
						}
					}

					$scope.getAssignedTagIds = function(assetList) {

						$scope.assignedTagMacId = '';

						if (assetList != undefined && assetList != null) {
							for (var i = 0; i < assetList.length; i++) {
								if (assetList[i].assignedStatus == 'Assigned') {
									if (assetList[i].assignedTagMacId != undefined
											&& assetList[i].assignedTagMacId != '') {
										if ($scope.assignedTagMacId == '')
											$scope.assignedTagMacId = assetList[i].assignedTagMacId;
										else
											$scope.assignedTagMacId = $scope.assignedTagMacId
													+ ","
													+ assetList[i].assignedTagMacId;

									}
								}
							}
						}

					}

					$scope.fnSearchFilterOption = function() {
						$scope.empViewData = [];
						$scope.employeeFilterList = [];

						for (var i = 0; i < $scope.empList.length; i++) {
							if ($scope.searchText == '') {
								$scope.employeeFilterList
										.push($scope.empList[i]);
							} else {
								if ($scope.empList[i].empId
										.toLowerCase()
										.includes(
												$scope.searchText.toLowerCase())
										|| ($scope.empList[i].firstName != undefined
												&& $scope.empList[i].firstName != null ? $scope.empList[i].firstName
												: "").toLowerCase()
												.includes(
														$scope.searchText
																.toLowerCase())
										|| ($scope.empList[i].lastName != undefined
												&& $scope.empList[i].lastName != null ? $scope.empList[i].lastName
												: "").toLowerCase()
												.includes(
														$scope.searchText
																.toLowerCase())
										|| ($scope.empList[i].phoneNumber != undefined
												&& $scope.empList[i].phoneNumber != null ? $scope.empList[i].phoneNumber
												: "").toLowerCase()
												.includes(
														$scope.searchText
																.toLowerCase())
										|| ($scope.empList[i].location1 != undefined
												&& $scope.empList[i].location1 != null ? $scope.empList[i].location1
												: "").toLowerCase()
												.includes(
														$scope.searchText
																.toLowerCase())
										|| ($scope.empList[i].department.deptName != undefined
												&& $scope.empList[i].department.deptName != null ? $scope.empList[i].department.deptName
												: "").toLowerCase()
												.includes(
														$scope.searchText
																.toLowerCase())
										|| ($scope.empList[i].shiftDetails.shiftName != undefined
												&& $scope.empList[i].shiftDetails.shiftName != null ? $scope.empList[i].shiftDetails.shiftName
												: "").toLowerCase()
												.includes(
														$scope.searchText
																.toLowerCase())
										|| ($scope.empList[i].location2 != undefined
												&& $scope.empList[i].location2 != null ? $scope.empList[i].location2
												: "").toLowerCase()
												.includes(
														$scope.searchText
																.toLowerCase())
										|| ($scope.empList[i].status != undefined
												&& $scope.empList[i].status != null ? $scope.empList[i].status
												: "").toLowerCase()
												.includes(
														$scope.searchText
																.toLowerCase())) {
									$scope.employeeFilterList
											.push($scope.empList[i]);

								}
							}
						}

						$scope.totalItems = $scope.employeeFilterList.length;
						$scope.currentPage = 1;
						$scope.getTotalPages();
						$scope.getAssignedTagIds($scope.employeeFilterList);

						if ($scope.empList.length == 0
								|| $scope.employeeFilterList.length == 0) {
							$scope.empNoData = true;

						} else {
							$scope.empNoData = false;

							if ($scope.employeeFilterList.length > $scope.numPerPage) {
								$scope.empViewData = $scope.employeeFilterList
										.slice(0, $scope.numPerPage);
								$scope.PaginationTab = true;
							} else {
								$scope.empViewData = $scope.employeeFilterList;
								$scope.PaginationTab = false;
							}
						}

					}// kumar 05/Jul filter option <-

					// kumar 05/Jul sort based on EmpID option <-
					var reA = /[^a-zA-Z]/g;
					var reN = /[^0-9]/g;

					function sortAlphaNum(a1, b1) {
						var a = a1.empId;
						var b = b1.empId;
						var aA = a.replace(reA, "");
						var bA = b.replace(reA, "");
						if (aA === bA) {
							var aN = parseInt(a.replace(reN, ""), 10);
							var bN = parseInt(b.replace(reN, ""), 10);
							return aN === bN ? 0 : aN > bN ? 1 : -1;
						} else {
							return aA > bA ? 1 : -1;
						}
					}

					// data for Primary location dropdown (dept API call)
					$scope.getZoneDetails = function() {
						if (!$rootScope.checkNetconnection()) { 
						 	//$rootScope.alertDialogChecknet();
							return; }
						$http({
							method : 'GET',
							url : './zone',

						})
								.then(
										function(response) {
											if (response != null
													&& response.data != null
													&& response.data.length != 0
													&& response.data != "BAD_REQUEST"
													&& response.data.data != undefined
													&& response.data.data.length != 0) {

												$scope.locationList = [];

												for (var i = 0; i < response.data.data.length; i++) {
													if (response.data.data[i].active == true) {
														$scope.locationList
																.push(response.data.data[i].name);
													}
												}

											} else {
												$scope.noList = true;
											}
										},function(error) {
											$rootScope.hideloading('#loadingBar');
											$rootScope.fnHttpError(error);
										});
					}

					// data for department dropdown (dept API call)
					$scope.getDepartmentDetails = function() {
						if (!$rootScope.checkNetconnection()) { 
						 	//$rootScope.alertDialogChecknet();
							return; }
						
						$http({
							method : 'GET',
							url : './department',

						})
								.then(
										function(response) {
											if (response != null
													&& response.data != null
													&& response.data.length != 0
													&& response.data != "BAD_REQUEST"
													&& response.data != undefined
													&& response.data.length != 0) {

												$scope.departmentList = [];

												for (var i = 0; i < response.data.length; i++) {
													if (response.data[i].active == true) {
														$scope.departmentList
																.push(response.data[i].deptName);
													}
												}
											} else {
												$scope.noList = true;
											}
										},function(error) {
											$rootScope.hideloading('#loadingBar');
											$rootScope.fnHttpError(error);
										});

					}

					// data for Shift dropdown (shift api call)
					$scope.getShiftDetails = function() {
						if (!$rootScope.checkNetconnection()) { 
						 	//$rootScope.alertDialogChecknet();
							return; }
						
						$http({
							method : 'GET',
							url : './shift',
						})
								.then(
										function(response) {
											if (response != null
													&& response.data != null
													&& response.data.length != 0
													&& response.data != "BAD_REQUEST"
													&& response.data.data != undefined
													&& response.data.data.length != 0) {
												$scope.shiftList = [];

												for (var i = 0; i < response.data.data.length; i++) {
													if (response.data.data[i].active == true) {
														$scope.shiftList
																.push(response.data.data[i].shiftName);
													}
												}

											} else {
												$scope.noList = true;
											}
										},function(error) {
											$rootScope.hideloading('#loadingBar');
											$rootScope.fnHttpError(error);
											
										});
					}
					// Anand View all employee table API call

					$scope.getEmpViewAllData = function() {
						if (!$rootScope.checkNetconnection()) { 
						 	$rootScope.alertDialogChecknet();
							return; }
						$scope.empList = [];
						$rootScope.showloading('#loadingBar');
						
						$http({
							method : 'GET',
							url : './staff',

						})
								.then(
										function success(response) {
											$rootScope
													.hideloading('#loadingBar');
											if (response != null
													&& response.data != null
													&& response.data.length != 0
													&& response.data != "BAD_REQUEST"
													&& response.data.data != undefined
													&& response.data.data.length != 0) {

												$scope.empViewData.length = 0;

												for (var i = 0; i < response.data.data.length; i++) {
													if (response.data.data[i].active == true) {
														$scope.empList
																.push(response.data.data[i]);
													}
												}
												$scope.empList
														.sort(sortAlphaNum);
												$scope.totalItems = $scope.empList.length;
												$scope.getAssignedTagIds($scope.empList);

												if ($scope.empList.length == 0) {
													$scope.empNoData = true;
													$scope.empMainView = false;
												} else {
													$scope.empNoData = false;
													$scope.empMainView = true;
												}
												$scope.getTotalPages();
												$scope.fnSearchFilterOption();

											} else {
												$scope.empNoData = true;
											}
										},
										function error(response) {
											$rootScope.hideloading('#loadingBar');
											$rootScope.fnHttpError(response);
											
											/*
											$rootScope.hideloading('#loadingBar');
											if (response.status == 403) {
												$
														.alert({
															title : $rootScope.MSGBOX_TITLE,
															content : 'Session timed-out or Invalid. Please logout and login again',
															type : 'blue',
															columnClass : 'small',
															autoClose : false,
															icon : 'fa fa-info-circle',
														});
											}
											else{
												toastr.error(response,"EI4.0")
											}
										*/
	
										});
					}

					// $scope.getEmpViewAllData();

					// View all employee table ends here
					// Anand emp Id validation
					// , #id_empIdEditEmployee, #id_firstNameAddEmployee, #id_firstNameEditEmployee, #id_lastNameAddEmployee, #id_lastNameEditEmployee
					$(document).on("keypress", "#id_empIdAddEmployee, #id_empIdEditEmployee, #id_firstNameAddEmployee, #id_firstNameEditEmployee, #id_lastNameAddEmployee, #id_lastNameEditEmployee",function(e) {
										// if the letter is not alphabets then display error
										// and don't type anything
										$return = ((e.which > 64 || e.keyCode > 64) && (e.which < 91 || e.keyCode < 91)) 
										|| ((e.which> 96 || e.keyCode > 96) && (e.which < 123 || e.keyCode < 123)) 
										||(e.which == 8|| e.keyCode == 8) || (e.which == 32|| e.keyCode == 32)  
										|| ((e.which >= 48 || e.keyCode >= 48)&& (e.which <= 57|| e.keyCode <= 57))
										if (!$return)
															
										{
											
											return false;
										}
									});

					// Anand Ph number validation
				
							$(document).on("keypress", "#id_phoneNumberAddEmployee, #id_phoneNumberEditEmployee",
									function(e) {
										// if the letter is not digit then
										// display error
										// and don't type anything

										if ((e.which != 8 || e.keyCode != 8)
												&& (e.which != 0 || e.keyCode !=0)
												&& ((e.which < 48|| e.keyCode < 48) || (e.which > 57 || e.keyCode > 57))) {

											return false;
										}
									});

					// Anand 2-6-21 add employee API call and validation
					$scope.addEmployeeFn = function() {
						
							var empId = document.getElementById("id_empIdAddEmployee");
							if (empId.value.length == 0) {
								 $scope.addEmpIdError = true;
								$scope.addEmpId_error_msg = "Please enter the Employee Id";
								$timeout(function() {
									$scope.addEmpIdError = false;
								}, 2000);
								return;
							}
							var fname = document.getElementById("id_firstNameAddEmployee");
							if (fname.value.length == 0) {
								$scope.addEmpFnameError = true;
								$scope.addEmpFname_error_msg = "Please enter the First Name";
								$timeout(function() {
									$scope.addEmpFnameError = false;
								}, 2000);
								return;
							}
							var lname = document.getElementById("id_lastNameAddEmployee");
							if (lname.value.length == 0) {
								 $scope.addEmpLnameError = true;
								$scope.addEmpLname_error_msg = "Please enter the Last Name";
								$timeout(function() {
									$scope.addEmpLnameError = false;
								}, 2000);
								return;
							}
							var phnoErr = document.getElementById("id_phoneNumberAddEmployee");
							if(phnoErr.value.length > 0 && phnoErr.value.length < 10 ){

								 $scope.phNoError = true;
								$scope.phNo_error_msg = "Please enter valid 10 digit phone number";
								$timeout(function() {
									$scope.phNoError = false;
								}, 2000);
								return;
							
							}
							if (phnoErr.value.length == 0) {
								$scope.phNo = "";
							}
							else if(phnoErr.value.length > 0 && phnoErr.value.length == 10){
							$scope.phNo = $scope.addEmployee.countryCode+ $scope.addEmployee.phoneNumber;
							}
							
							for(var i=0; i < $scope.locationList.length; i++){
								if($scope.addEmployee.primaryLocation == $scope.locationList[i].zoneName){
									var primaryLocationZoneId = $scope.locationList[i].zoneId;
									break;
								}
							}
						
							var addEmployeeParameter = {
								"empId" : $scope.addEmployee.empId,
								"lastName" : $scope.addEmployee.lastName,
								"firstName" : $scope.addEmployee.firstName,
								"department" : $scope.addEmployee.dept,
								"location1" : $scope.addEmployee.primaryLocation,
								"location2" : $scope.addEmployee.secondaryLocation,
								"location3" : $scope.addEmployee.tertiaryLocation,
								"phoneNumber" : $scope.phNo,
								"status" : "Working",
								"shiftDetails" : $scope.addEmployee.shift
							};
							if (!$rootScope.checkNetconnection()) {
}
							$rootScope.showloading('#loadingBar');
							$scope.promise = $http
									.post('./staff', addEmployeeParameter)
									.then(
											function(result) {

												if (result.data.status == "success") {
													$rootScope.hideloading('#loadingBar');
													$rootScope.showAlertDialog(result.data.message);
													$scope.cancelAddEmployee();

												} else if (result.data.status == "error") {
													$rootScope.hideloading('#loadingBar');
													$rootScope.showAlertDialog(result.data.message);
													
												}

											},
											function(error) {
												$rootScope.hideloading('#loadingBar');
												$rootScope.fnHttpError(error);
											});
						
					}

					$scope.cancelAddEmployee = function() {

						$scope.addEmployee = {};
						$scope.selectedRow = -1;
						$scope.addEmployee.empId = "";
						$scope.addEmployee.firstName = "";
						$scope.addEmployee.lastName = "";
						/*$('#id_empIdAddEmployee').val("");
						$('#id_firstNameAddEmployee').val("");
						$('#id_lastNameAddEmployee').val("");
						$('#phoneNumber').val("");*/

						if ($scope.departmentList != undefined
								&& $scope.departmentList != null
								&& $scope.departmentList != "") {
							$scope.addEmployee.dept = $scope.departmentList[0];
						}
						if ($scope.locationList != undefined
								&& $scope.locationList != null
								&& $scope.locationList != "") {
							$scope.addEmployee.primaryLocation = $scope.locationList[0];
							$scope.addEmployee.secondaryLocation = $scope.locationList[0];
							$scope.addEmployee.tertiaryLocation = $scope.locationList[0];
						}
						if ($scope.shiftList != undefined
								&& $scope.shiftList != null
								&& $scope.shiftList != "") {
							$scope.addEmployee.shift = $scope.shiftList[0];
						}
						$scope.addEmployee.countryCode = $rootScope.countryCodes[0];
						$scope.addEmployee.status = $scope.employeeStatus[0];

					}
					$scope.addMultipleEmployee = function() {
						
						var mulEmpId = document.getElementById("id_multipleEmployeeEmployeeID");
						if (mulEmpId.value == "Select an option" ) {
							$scope.addMulEmpIdError = true;
							$scope.addMulEmpId_error_msg = "Please select Employee Id";
							$timeout(function() {
								$scope.addMulEmpIdError= false;
							}, 2000);
							return;
						}
						var mulDept = document.getElementById("id_multipleEmployeeDepatment");
						if (mulDept.value == "Select an option" ) {
							$scope.addMulDeptError = true;
							$scope.addMulDept_error_msg = "Please select Department";
							$timeout(function() {
								$scope.addMulDeptError= false;
							}, 2000);
							return;
						}
						var mulFname = document.getElementById("id_multipleEmployeeFirstName");
						if (mulFname.value == "Select an option" ) {
							$scope.addMulFnameError = true;
							$scope.addMulFname_error_msg = "Please select First Name";
							$timeout(function() {
								$scope.addMulFnameError= false;
							}, 2000);
							return;
						}
						var mulLname = document.getElementById("id_multipleEmployeeLastname");
						if (mulLname.value == "Select an option" ) {
							$scope.addMullNameError = true;
							$scope.addMullName_error_msg = "Please select Last Name";
							$timeout(function() {
								$scope.addMullNameError= false;
							}, 2000);
							return;
						}
						var mulPrimaryLoc = document.getElementById("id_multipleEmployeePrimaryLocation");
						if (mulPrimaryLoc.value == "Select an option" ) {
							$scope.addMulPrimaryLocError = true;
							$scope.addMulPrimaryLoc_error_msg = "Please select Primary Location";
							$timeout(function() {
								$scope.addMulPrimaryLocError= false;
							}, 2000);
							return;
						}
						var mulSecLoc = document.getElementById("id_multipleEmployeeSecondaryLocation");
						if (mulSecLoc.value == "Select an option" ) {
							$scope.addMulSecondaryLocError = true;
							$scope.addMulSecondaryLoc_error_msg = "Please select Secondary Location";
							$timeout(function() {
								$scope.addMulSecondaryLocError= false;
							}, 2000);
							return;
						}
						var mulTerLoc = document.getElementById("id_multipleEmployeeTertiaryLocation");
						if (mulTerLoc.value == "Select an option" ) {
							$scope.addMulTertiaryLocError = true;
							$scope.addMulTertiaryLoc_error_msg = "Please select Tertiary Location";
							$timeout(function() {
								$scope.addMulTertiaryLocError= false;
							}, 2000);
							return;
						}
						var mulPhno = document.getElementById("id_multipleEmployeePhNo");
						if (mulPhno.value == "Select an option" ) {
							$scope.addMulPhnoError = true;
							$scope.addMulPhno_error_msg = "Please select Phone Number" ;
							$timeout(function() {
								$scope.addMulPhnoError= false;
							}, 2000);
							return;
						}
						var mulShift = document.getElementById("id_multipleEmployeeShift");
						if (mulShift.value == "Select an option" ) {
							$scope.addMulShiftError = true;
							$scope.addMulShift_error_msg = "Please select Shift" ;
							$timeout(function() {
								$scope.addMulShiftError= false;
							}, 2000);
							return;
						}
						/*var mulStatus = document.getElementById("id_multipleEmployeeStatus");
						if (mulStatus.value == "Select an option" ) {
							$scope.addMulStatusError = true;
							$scope.addMulStatus_error_msg = "Please select Status" ;
							$timeout(function() {
								$scope.addMulStatusError= false;
							}, 2000);
							return;
						}*/
						if($scope.multiEmployeeAddScreen){
							
							if ($scope.multiEmployee.employeeID != undefined && $scope.multiEmployee.depatment != undefined &&
									$scope.multiEmployee.employeeID == $scope.multiEmployee.depatment) {
								 
								$rootScope.hideloading('#loadingBar');
								$rootScope.showAlertDialog("Select different field for Employee ID and Department");
						          return;
								
							}
							else if ($scope.multiEmployee.employeeID != undefined && $scope.multiEmployee.fName != undefined &&
									$scope.multiEmployee.employeeID == $scope.multiEmployee.fName) {
								 
								$rootScope.hideloading('#loadingBar');	
								$rootScope.showAlertDialog("Select different field for Employee ID and First Name");
						         return;
								
							}
							else if ($scope.multiEmployee.employeeID != undefined && $scope.multiEmployee.lName != undefined &&
									$scope.multiEmployee.employeeID == $scope.multiEmployee.lName) {
								 
								$rootScope.hideloading('#loadingBar');	
								$rootScope.showAlertDialog("Select different field for Employee ID and Last Name");
						         return;
								
							}
							else if ($scope.multiEmployee.employeeID != undefined && $scope.multiEmployee.primaryLocation != undefined &&
									$scope.multiEmployee.employeeID == $scope.multiEmployee.primaryLocation) {
								 
								$rootScope.hideloading('#loadingBar');	
								$rootScope.showAlertDialog("Select different field for Employee ID and Primary Location");
						         return;
								
							}
							
							else if ($scope.multiEmployee.employeeID != undefined && $scope.multiEmployee.secondaryLocation != undefined &&
									$scope.multiEmployee.employeeID == $scope.multiEmployee.secondaryLocation) {
								 
								$rootScope.hideloading('#loadingBar');	
								$rootScope.showAlertDialog("Select different field for Employee ID and Secondary Location");
						         return;
								
							}
							else if ($scope.multiEmployee.employeeID != undefined && $scope.multiEmployee.tertiaryLoc != undefined &&
									$scope.multiEmployee.employeeID == $scope.multiEmployee.tertiaryLoc) {
								 
								$rootScope.hideloading('#loadingBar');	
								$rootScope.showAlertDialog("Select different field for Employee ID and Tertiary Location");
						         return;
								
							}
							else if ($scope.multiEmployee.employeeID != undefined && $scope.multiEmployee.phNo != undefined &&
									$scope.multiEmployee.employeeID == $scope.multiEmployee.phNo) {
								 
								$rootScope.hideloading('#loadingBar');	
								$rootScope.showAlertDialog("Select different field for Employee ID and Phone Number");
						         return;
								
							}
							else if ($scope.multiEmployee.employeeID != undefined && $scope.multiEmployee.shift != undefined &&
									$scope.multiEmployee.employeeID == $scope.multiEmployee.shift) {
								 
								$rootScope.hideloading('#loadingBar');	
								$rootScope.showAlertDialog("Select different field for Employee ID and Shift");
						         return;
								
							}
							/*else if ($scope.multiEmployee.employeeID != undefined && $scope.multiEmployee.status != undefined &&
									$scope.multiEmployee.employeeID == $scope.multiEmployee.status) {
								 
								$rootScope.hideloading('#loadingBar');	
								$rootScope.showAlertDialog("Select different field for Employee ID and Status");
						         return;
								
							}*/
							else if ($scope.multiEmployee.depatment != undefined && $scope.multiEmployee.fName != undefined &&
									$scope.multiEmployee.depatment == $scope.multiEmployee.fName) {
								 
								$rootScope.hideloading('#loadingBar');	
								$rootScope.showAlertDialog("Select different field for Department and First Name");
						         return;
								
							}
							else if ($scope.multiEmployee.depatment != undefined && $scope.multiEmployee.lName != undefined &&
									$scope.multiEmployee.depatment == $scope.multiEmployee.lName) {
								 
								$rootScope.hideloading('#loadingBar');	
								$rootScope.showAlertDialog("Select different field for Department and Last Name");
						         return;
								
							}
							else if ($scope.multiEmployee.depatment != undefined && $scope.multiEmployee.primaryLocation != undefined &&
									$scope.multiEmployee.depatment == $scope.multiEmployee.primaryLocation) {
								 
								$rootScope.hideloading('#loadingBar');	
								$rootScope.showAlertDialog("Select different field for Department and Primary Location");
						         return;
								
							}
							else if ($scope.multiEmployee.depatment != undefined && $scope.multiEmployee.secondaryLocation != undefined &&
									$scope.multiEmployee.depatment == $scope.multiEmployee.secondaryLocation) {
								 
								$rootScope.hideloading('#loadingBar');	
								$rootScope.showAlertDialog("Select different field for Department and Secondary Location");
						         return;
								
							}
							else if ($scope.multiEmployee.depatment != undefined && $scope.multiEmployee.tertiaryLoc != undefined &&
									$scope.multiEmployee.depatment == $scope.multiEmployee.tertiaryLoc) {
								 
								$rootScope.hideloading('#loadingBar');	
								$rootScope.showAlertDialog("Select different field for Department and Tertiary Location");
						         return;
								
							}
							else if ($scope.multiEmployee.depatment != undefined && $scope.multiEmployee.phNo != undefined &&
									$scope.multiEmployee.depatment == $scope.multiEmployee.phNo) {
								 
								$rootScope.hideloading('#loadingBar');	
								$rootScope.showAlertDialog("Select different field for Department and Phone Number");
						         return;
								
							}
							else if ($scope.multiEmployee.depatment != undefined && $scope.multiEmployee.shift != undefined &&
									$scope.multiEmployee.depatment == $scope.multiEmployee.shift) {
								 
								$rootScope.hideloading('#loadingBar');	
								$rootScope.showAlertDialog("Select different field for Department and Shift");
						         return;
								
							}
							/*else if ($scope.multiEmployee.depatment != undefined && $scope.multiEmployee.status != undefined &&
									$scope.multiEmployee.depatment == $scope.multiEmployee.status) {
								 
								$rootScope.hideloading('#loadingBar');	
								$rootScope.showAlertDialog("Select different field for Department and Status");
						         return;
								
							}*/
							else if ($scope.multiEmployee.fName != undefined && $scope.multiEmployee.lName != undefined &&
									$scope.multiEmployee.fName == $scope.multiEmployee.lName) {
								 
								$rootScope.hideloading('#loadingBar');	
								$rootScope.showAlertDialog("Select different field for First Name and Last Name");
						         return;
								
							}
							else if ($scope.multiEmployee.fName != undefined && $scope.multiEmployee.primaryLocation != undefined &&
									$scope.multiEmployee.fName == $scope.multiEmployee.primaryLocation) {
								 
								$rootScope.hideloading('#loadingBar');	
								$rootScope.showAlertDialog("Select different field for First Name and Primary Location");
						         return;
								
							}
							else if ($scope.multiEmployee.fName != undefined && $scope.multiEmployee.secondaryLocation != undefined &&
									$scope.multiEmployee.fName == $scope.multiEmployee.secondaryLocation) {
								 
								$rootScope.hideloading('#loadingBar');	
								$rootScope.showAlertDialog("Select different field for First Name and Secondary Location");
						         return;
								
							}
							else if ($scope.multiEmployee.fName != undefined && $scope.multiEmployee.tertiaryLoc != undefined &&
									$scope.multiEmployee.fName == $scope.multiEmployee.tertiaryLoc) {
								 
								$rootScope.hideloading('#loadingBar');	
								$rootScope.showAlertDialog("Select different field for First Name and Tertiary Location");
						         return;
								
							}
							else if ($scope.multiEmployee.fName != undefined && $scope.multiEmployee.phNo != undefined &&
									$scope.multiEmployee.fName == $scope.multiEmployee.phNo) {
								 
								$rootScope.hideloading('#loadingBar');	
								$rootScope.showAlertDialog("Select different field for First Name and Phone Number");
						         return;
								
							}
							else if ($scope.multiEmployee.fName != undefined && $scope.multiEmployee.shift != undefined &&
									$scope.multiEmployee.fName == $scope.multiEmployee.shift) {
								 
								$rootScope.hideloading('#loadingBar');	
								$rootScope.showAlertDialog("Select different field for First Name and Shift");
						         return;
								
							}
							/*else if ($scope.multiEmployee.fName != undefined && $scope.multiEmployee.status != undefined &&
									$scope.multiEmployee.fName == $scope.multiEmployee.status) {
								 
								$rootScope.hideloading('#loadingBar');	
								$rootScope.showAlertDialog("Select different field for First Name and Status");
						         return;
								
							}*/
							else if ($scope.multiEmployee.lName != undefined && $scope.multiEmployee.primaryLocation != undefined &&
									$scope.multiEmployee.lName == $scope.multiEmployee.primaryLocation) {
								 
								$rootScope.hideloading('#loadingBar');	
								$rootScope.showAlertDialog("Select different field for Last Name and Primary Location");
						         return;
								
							}
							else if ($scope.multiEmployee.lName != undefined && $scope.multiEmployee.secondaryLocation != undefined &&
									$scope.multiEmployee.lName == $scope.multiEmployee.secondaryLocation) {
								 
								$rootScope.hideloading('#loadingBar');	
								$rootScope.showAlertDialog("Select different field for Last Name and Secondary Location");
						         return;
								
							}
							else if ($scope.multiEmployee.lName != undefined && $scope.multiEmployee.tertiaryLoc != undefined &&
									$scope.multiEmployee.lName == $scope.multiEmployee.tertiaryLoc) {
								 
								$rootScope.hideloading('#loadingBar');	
								$rootScope.showAlertDialog("Select different field for Last Name and Tertiary Location");
						         return;
								
							}
							else if ($scope.multiEmployee.lName != undefined && $scope.multiEmployee.phNo != undefined &&
									$scope.multiEmployee.lName == $scope.multiEmployee.phNo) {
								 
								$rootScope.hideloading('#loadingBar');	
								$rootScope.showAlertDialog("Select different field for Last Name and Phone Number");
						         return;
								
							}
							else if ($scope.multiEmployee.lName != undefined && $scope.multiEmployee.shift != undefined &&
									$scope.multiEmployee.lName == $scope.multiEmployee.shift) {
								 
								$rootScope.hideloading('#loadingBar');	
								$rootScope.showAlertDialog("Select different field for Last Name and Shift");
						         return;
								
							}
							/*else if ($scope.multiEmployee.lName != undefined && $scope.multiEmployee.status != undefined &&
									$scope.multiEmployee.lName == $scope.multiEmployee.status) {
								 
								$rootScope.hideloading('#loadingBar');	
								$rootScope.showAlertDialog("Select different field for Last Name and Status");
						         return;
								
							}*/
							else if ($scope.multiEmployee.primaryLocation != undefined && $scope.multiEmployee.secondaryLocation != undefined &&
									$scope.multiEmployee.primaryLocation == $scope.multiEmployee.secondaryLocation) {
								 
								$rootScope.hideloading('#loadingBar');	
								$rootScope.showAlertDialog("Select different field for Primary Location and Secondary Location");
						         return;
								
							}
							else if ($scope.multiEmployee.primaryLocation != undefined && $scope.multiEmployee.tertiaryLoc != undefined &&
									$scope.multiEmployee.primaryLocation == $scope.multiEmployee.tertiaryLoc) {
								 
								$rootScope.hideloading('#loadingBar');	
								$rootScope.showAlertDialog("Select different field for Primary Location and Tertiary Location");
						         return;
								
							}
							else if ($scope.multiEmployee.primaryLocation != undefined && $scope.multiEmployee.phNo != undefined &&
									$scope.multiEmployee.primaryLocation == $scope.multiEmployee.phNo) {
								 
								$rootScope.hideloading('#loadingBar');	
								$rootScope.showAlertDialog("Select different field for Primary Location and Phone Number");
						         return;
								
							}
							else if ($scope.multiEmployee.primaryLocation != undefined && $scope.multiEmployee.shift != undefined &&
									$scope.multiEmployee.primaryLocation == $scope.multiEmployee.shift) {
								 
								$rootScope.hideloading('#loadingBar');	
								$rootScope.showAlertDialog("Select different field for Primary Location and Shift");
						         return;
								
							}
							/*else if ($scope.multiEmployee.primaryLocation != undefined && $scope.multiEmployee.status != undefined &&
									$scope.multiEmployee.primaryLocation == $scope.multiEmployee.status) {
								 
								$rootScope.hideloading('#loadingBar');	
								$rootScope.showAlertDialog("Select different field for Primary Location and Status");
						         return;
								
							}*/
							
							else if ($scope.multiEmployee.secondaryLocation != undefined && $scope.multiEmployee.tertiaryLoc != undefined &&
									$scope.multiEmployee.secondaryLocation == $scope.multiEmployee.tertiaryLoc) {
								 
								$rootScope.hideloading('#loadingBar');	
								$rootScope.showAlertDialog("Select different field for Secondary Location and Tertiary Location");
						         return;
								
							}
							else if ($scope.multiEmployee.secondaryLocation != undefined && $scope.multiEmployee.phNo != undefined &&
									$scope.multiEmployee.secondaryLocation == $scope.multiEmployee.phNo) {
								 
								$rootScope.hideloading('#loadingBar');	
								$rootScope.showAlertDialog("Select different field for Secondary Location and Phone Number");
						         return;
								
							}
							else if ($scope.multiEmployee.secondaryLocation != undefined && $scope.multiEmployee.shift != undefined &&
									$scope.multiEmployee.secondaryLocation == $scope.multiEmployee.shift) {
								 
								$rootScope.hideloading('#loadingBar');	
								$rootScope.showAlertDialog("Select different field for Secondary Location and Shift");
						         return;
								
							}
							/*else if ($scope.multiEmployee.secondaryLocation != undefined && $scope.multiEmployee.status != undefined &&
									$scope.multiEmployee.secondaryLocation == $scope.multiEmployee.status) {
								 
								$rootScope.hideloading('#loadingBar');	
								$rootScope.showAlertDialog("Select different field for Secondary Location and Status");
						         return;
								
							}*/
							
							else if ($scope.multiEmployee.tertiaryLoc != undefined && $scope.multiEmployee.phNo != undefined &&
									$scope.multiEmployee.tertiaryLoc == $scope.multiEmployee.phNo) {
								 
								$rootScope.hideloading('#loadingBar');	
								$rootScope.showAlertDialog("Select different field for Tertiary Location and Phone Number");
						         return;
								
							}
							else if ($scope.multiEmployee.tertiaryLoc != undefined && $scope.multiEmployee.shift != undefined &&
									$scope.multiEmployee.tertiaryLoc == $scope.multiEmployee.shift) {
								 
								$rootScope.hideloading('#loadingBar');	
								$rootScope.showAlertDialog("Select different field for Tertiary Location and Shift");
						         return;
								
							}
							/*else if ($scope.multiEmployee.tertiaryLoc != undefined && $scope.multiEmployee.status != undefined &&
									$scope.multiEmployee.tertiaryLoc == $scope.multiEmployee.status) {
								 
								$rootScope.hideloading('#loadingBar');	
								$rootScope.showAlertDialog("Select different field for Tertiary Location and Status");
						         return;
								
							}*/
							else if ($scope.multiEmployee.phNo != undefined && $scope.multiEmployee.shift != undefined &&
									$scope.multiEmployee.phNo == $scope.multiEmployee.shift) {
								 
								$rootScope.hideloading('#loadingBar');	
								$rootScope.showAlertDialog("Select different field for Phone Number and Shift");
						         return;
								
							}
							/*else if ($scope.multiEmployee.phNo != undefined && $scope.multiEmployee.status != undefined &&
									$scope.multiEmployee.phNo == $scope.multiEmployee.status) {
								 
								$rootScope.hideloading('#loadingBar');	
								$rootScope.showAlertDialog("Select different field for Phone Number and Status");
						         return;
								
							}
							else if ($scope.multiEmployee.shift != undefined && $scope.multiEmployee.status != undefined &&
									$scope.multiEmployee.shift == $scope.multiEmployee.status) {
								 
								$rootScope.hideloading('#loadingBar');	
								$rootScope.showAlertDialog("Select different field for Shift and Status");
						         return;
								
							}*/
							else {
								$scope.Upload();
							}
							
						}
					}
					$scope.cancelMultipleEmployee= function() {

						$("#id_multipleEmployeeEmployeeID").val("Select an option"); 
						$("#id_multipleEmployeeDepatment").val("Select an option");
						$("#id_multipleEmployeeFirstName").val("Select an option");
						$("#id_multipleEmployeeLastname").val("Select an option");
						$("#id_multipleEmployeePrimaryLocation").val("Select an option");
						$("#id_multipleEmployeeSecondaryLocation").val("Select an option");
						$("#id_multipleEmployeeTertiaryLocation").val("Select an option");
						$("#id_multipleEmployeePhNo").val("Select an option");
						$("#id_multipleEmployeeShift").val("Select an option");
						//$("#id_multipleEmployeeStatus").val("Select an option");
						
						$scope.multiEmployeeAddScreen = false;
						$scope.employeeMultipleAddFileUploadScreen = true;
					
					}
					$scope.fnGetDictionaryLength = function(dict) {
						var nCount = 0;
						for (i in dict)
							++nCount;
						return nCount;
					}
					
					$scope.fnSelectZoneEditEmp  = function(locationInfo) {
						$rootScope.selectedRowOnZoneDetail =  {};
						
						$rootScope.selectedRowOnZoneDetail.selectedRow =$scope.selectedRow;
						$rootScope.selectedRowOnZoneDetail.pageNum =$scope.currentPage;
						$rootScope.selectedRowOnZoneDetail.employeeDetail = $scope.empList;
						
						$rootScope.locationInfo = locationInfo;
						if ($scope.fnGetDictionaryLength($rootScope.ZoneSelection) == 0)
						{
			
						if ($rootScope.locationInfo == 'tertiaryLoc'){
							$rootScope.ZoneSelection.selectedZone = $scope.authen.location3;
						}
						else if ($rootScope.locationInfo == 'secondaryLoc'){
							$rootScope.ZoneSelection.selectedZone = $scope.authen.location2;
						}
						else if($rootScope.locationInfo == 'primaryLoc'){
							$rootScope.ZoneSelection.selectedZone = $scope.authen.location1;
						}
						
						$rootScope.ZoneSelection.zoneSelect = "true";
						
						 $rootScope.ZoneSelection.empId = $scope.authen.empId;
						 $rootScope.ZoneSelection.dept = $scope.authen.department.deptName;
						 $rootScope.ZoneSelection.firstName = $scope.authen.firstName;
						 $rootScope.ZoneSelection.lastName =  $scope.authen.lastName;
						 $rootScope.ZoneSelection.primaryLocation = $scope.authen.location1;
						 $rootScope.ZoneSelection.secondaryLocation = $scope.authen.location2;
						 $rootScope.ZoneSelection.tertiaryLocation =$scope.authen.location3;
						 $rootScope.ZoneSelection.shift = $scope.authen.shiftDetails.shiftName;
						 $rootScope.ZoneSelection.countryCode = $scope.authen.countryCode;
					     $rootScope.ZoneSelection.phoneNumber = $scope.authen.phoneNumber;
						 $rootScope.ZoneSelection.shift = $scope.authen.shiftDetails.shiftName;
						 $rootScope.ZoneSelection.status = $scope.authen.status;
						
						
						//$rootScope.ZoneSelection.locationList = $scope.locationList;
						$rootScope.ZoneSelection.zoneDetails = $scope.locationList;
						$rootScope.isEmpManagementEdit = true ;
						$rootScope.isEmpManagement = false ;
						$window.sessionStorage.setItem("menuName","zoneManagement");
						$location.path('/zoneManagement');
						//locationList
						}
					}
					
					$scope.fnSelectZoneEmp  = function(location) {
						$rootScope.locationInfo = location;
						if ($scope.fnGetDictionaryLength($rootScope.ZoneSelection) == 0)
						{
							if ($rootScope.locationInfo == 'tertiaryLocation'){
								$rootScope.ZoneSelection.selectedZone = $scope.addEmployee.tertiaryLocation;
							}
							else if ($rootScope.locationInfo == 'secondaryLocation'){
								$rootScope.ZoneSelection.selectedZone = $scope.addEmployee.secondaryLocation;
							}
							else if($rootScope.locationInfo == 'primaryLocation'){
								$rootScope.ZoneSelection.selectedZone = $scope.addEmployee.primaryLocation;
							}

						 $rootScope.ZoneSelection.zoneSelect = "true";
						
						 $rootScope.ZoneSelection.empId = $scope.addEmployee.empId;
						 $rootScope.ZoneSelection.dept = $scope.addEmployee.dept;
						 $rootScope.ZoneSelection.firstName = $scope.addEmployee.firstName;
						 $rootScope.ZoneSelection.lastName =  $scope.addEmployee.lastName;
						 $rootScope.ZoneSelection.primaryLocation = $scope.addEmployee.primaryLocation;
						 $rootScope.ZoneSelection.secondaryLocation = $scope.addEmployee.secondaryLocation;
						 $rootScope.ZoneSelection.tertiaryLocation = $scope.addEmployee.tertiaryLocation ;
						 $rootScope.ZoneSelection.shift = $scope.addEmployee.shift;
						 $rootScope.ZoneSelection.countryCode = $scope.addEmployee.countryCode;
					     $rootScope.ZoneSelection.phoneNumber = $scope.addEmployee.phoneNumber;
						 $rootScope.ZoneSelection.shift = $scope.addEmployee.shift;
						 
						
						//$rootScope.ZoneSelection.locationList = $scope.locationList;
						$rootScope.ZoneSelection.zoneDetails = $scope.locationList;
						$rootScope.isEmpManagement = true ;
						$rootScope.isEmpManagementEdit = false ;
						$window.sessionStorage.setItem("menuName","zoneManagement");
						$location.path('/zoneManagement');
						//locationList
						}
					}
					
					$scope.cancelEmployee = function() {
						// var
						// formID=document.getElementById("addEmpForm").reset();
						$scope.authen = {};
						$scope.selectedRow = -1;
						$scope.employeeEditScreen = true;
						$scope.employeeEditUpdateScreen = false;
						$scope.searchButton = true;
					}

					// edit employee
					$scope.updateEmployee = function() {
							
				
							
							var empId = document.getElementById("id_empIdEditEmployee");
							if (empId.value.length == 0) {
								 $scope.editEmpIdError = true;
								$scope.editEmpId_error_msg = "Please enter the Employee Id";
								$timeout(function() {
									$scope.editEmpIdError = false;
								}, 2000);
								return;
							}
							var fname = document.getElementById("id_firstNameEditEmployee");
							if (fname.value.length == 0) {
								 $scope.editEmpFnameError = true;
								$scope.editEmpFname_error_msg = "Please enter the First Name";
								$timeout(function() {
									$scope.editEmpFnameError = false;
								}, 2000);
								return;
							}
							var lname = document.getElementById("id_lastNameEditEmployee");
							if (lname.value.length == 0) {
								 $scope.editEmpLnameError = true;
								$scope.editEmpLname_error_msg = "Please enter the Last Name";
								$timeout(function() {
									$scope.editEmpLnameError = false;
								}, 2000);
								return;
							}
							var phnoErr = document.getElementById("id_phoneNumberEditEmployee");
							if(phnoErr.value.length > 0 && phnoErr.value.length < 10 ){
								$scope.phNoError = true;
								$scope.phNo_error_msg = "Please enter valid 10 digit phone number";
								$timeout(function() {
									$scope.phNoError = false;
								}, 2000);
								return;
							
							}
							
							if (phnoErr.value.length == 0) {
								$scope.phNo = "";
							}
							else if(phnoErr.value.length > 0 && phnoErr.value.length == 10){
							$scope.phNo = $scope.authen.countryCode + $scope.authen.phoneNumber;
							}
							var editEmployeeParameter = {
								"empId" : $scope.authen.empId,
								"lastName" : $scope.authen.lastName,
								"firstName" : $scope.authen.firstName,
								"department" : $scope.authen.department.deptName,
								"location1" : $scope.authen.location1,
								"location2" : $scope.authen.location2,
								"location3" : $scope.authen.location3,
								"phoneNumber" : $scope.phNo,
								"status" : $scope.authen.status,
								"shiftDetails" : $scope.authen.shiftDetails.shiftName
							};
							$scope.empViewData[$scope.selectedRow] = JSON
									.parse(JSON.stringify($scope.authen));
							$scope.empViewData[$scope.selectedRow].phoneNumber = $scope.phNo;
							if (!$rootScope.checkNetconnection()) { 
							 	$rootScope.alertDialogChecknet();
								return; 
							}
							$rootScope.showloading('#loadingBar');
							$scope.promise = $http
									.put('./staff', editEmployeeParameter)
									.then(
											function(result) {
												$rootScope.hideloading('#loadingBar');
												if (result.data.status == "success") {
													$rootScope.showAlertDialog(result.data.message);
													$scope.cancelEmployee();
													$scope.getEmpViewAllData()
												} else if (result.data.status == "error") {
													$rootScope.showAlertDialog(result.data.message);
													
												}

											},
											function(error) {
												$rootScope.hideloading('#loadingBar');
												$rootScope.fnHttpError(error);
											});
						
					}

					// pagination button

					$scope.currentPage = 1;
					$scope.numPerPage = 10;
					$scope.maxSize = 3;
					$scope.totalItems = 0;

					$scope
							.$watch(
									'currentPage + numPerPage',
									function() {
										var begin = (($scope.currentPage - 1) * $scope.numPerPage), end = begin
												+ $scope.numPerPage;

										if ($scope.searchText == '')
											$scope.empViewData = $scope.empList
													.slice(begin, end);
										else
											$scope.empViewData = $scope.employeeFilterList
													.slice(begin, end);

										$scope.enablePageButtons();

									});

					$scope.currentPage = 1;
					$scope.maxSize = 3;
					$scope.numPerPage = 10;
					$scope.totalItems = 0;
					$scope.totalPages = 0;

					$scope.hasNext = true;
					$scope.hasPrevious = true;
					$scope.isFirstPage = true;
					$scope.isLastPage = true;
					$scope.serial = 1;

					$scope
							.$watch(
									"currentPage",
									function(newVal, oldVal) {
										if (newVal !== oldVal) {

											if (newVal > $scope.totalPages) {
												$scope.currentPage = $scope.totalPages;
											} else {
												$scope.currentPage = newVal;
											}

											$scope.serial = (($scope.currentPage - 1) * $scope.numPerPage) + 1;
											$scope.enablePageButtons();
										}
									});

					$scope.enablePageButtons = function() {
						$scope.serial = (($scope.currentPage - 1) * $scope.numPerPage) + 1;
						$scope.selectedRow = -1;
						if ($scope.totalPages <= 1) {
							$scope.hasPrevious = true;
							$scope.isFirstPage = true;
							$scope.hasNext = true;
							$scope.isLastPage = true;

						} else if ($scope.totalPages > 1
								&& $scope.currentPage == $scope.totalPages) {

							$scope.hasPrevious = false;
							$scope.isFirstPage = false;
							$scope.hasNext = true;
							$scope.isLastPage = true;
						} else if ($scope.currentPage != 1
								&& $scope.currentPage < $scope.totalPages) {

							$scope.hasPrevious = false;
							$scope.isFirstPage = false;
							$scope.hasNext = false;
							$scope.isLastPage = false;

						} else {
							$scope.hasPrevious = true;
							$scope.isFirstPage = true;
							$scope.hasNext = false;
							$scope.isLastPage = false;
						}

					};

					$scope.getTotalPages = function() {

						if (($scope.totalItems % $scope.numPerPage) > 0)
							$scope.totalPages = parseInt($scope.totalItems
									/ $scope.numPerPage) + 1;
						else
							$scope.totalPages = parseInt($scope.totalItems
									/ $scope.numPerPage);

						$scope.enablePageButtons();

					};

					$scope.goFirstPage = function() {
						$scope.currentPage = 1;
						$scope.enablePageButtons();
					};

					$scope.goPreviousPage = function() {
						if ($scope.currentPage > 1)
							$scope.currentPage = $scope.currentPage - 1;
						$scope.enablePageButtons();
					};

					$scope.goNextPage = function() {
						if ($scope.currentPage <= $scope.totalPages)
							$scope.currentPage = $scope.currentPage + 1;
						$scope.enablePageButtons();
					};

					$scope.goLastPage = function() {
						$scope.currentPage = $scope.totalPages;
						$scope.enablePageButtons();
					};
					$scope.displayEmployeeViewScreen = function() {
						if($rootScope.isBackFromZone != true)
						//$scope.empList = [];
						$scope.empViewData = [];
						$scope.empList.length = 0;
						$scope.empViewData.length = 0;
						$scope.searchText = "";
						$scope.trackMacId = '';

						$scope.employeeViewScreen = true;
						$scope.employeeEditScreen = false;
						$scope.employeeEditUpdateScreen = false;
						$scope.employeeAddScreen = false;
						$scope.showSearchIcon = true;
						$scope.showMultipleSearch = false;
						$scope.employeeMultipleAddFileUploadScreen = false;
						$scope.multiEmployeeAddScreen = false;
						$scope.selectedRow = -1;
						$scope.currentPage = 1;
						$scope.searchButton = true;
						$("#idEmployeeView").addClass("active");
						$("#idEmployeeAdd").removeClass("active");
						$("#idEmployeeEdit").removeClass("active");
						$("#idEmployeeMultipleAdd").removeClass("active");
						$scope.getEmpViewAllData();
						
						
						
						
						
					};
					$scope.displayEmployeeAddScreen = function() {
						
						if($rootScope.ZoneSelection.zoneSelect == false){
							
						}
						$scope.showMultipleSearch = false;
						$scope.showSearchIcon = false;
						$scope.cancelAddEmployee();
						$scope.employeeViewScreen = false;
						$scope.employeeEditScreen = false;
						$scope.employeeEditUpdateScreen = false;
						$scope.employeeAddScreen = true;
						$scope.searchButton = false;
						$scope.employeeMultipleAddFileUploadScreen = false;
						$scope.multiEmployeeAddScreen = false;
						$scope.trackMacId = '';

						
						$timeout(function() {
							$("#idEmployeeView").removeClass("active");
							$("#idEmployeeAdd").addClass("active");
							$("#idEmployeeEdit").removeClass("active");
							$("#idEmployeeMultipleAdd").removeClass("active"); 
						}, 100);
						
						if ($rootScope.ZoneSelection.zoneSelect != undefined && $rootScope.ZoneSelection.zoneSelect == "true" && $rootScope.isEmpManagement == true && $rootScope.isEmpManagementEdit == false){
					 		$rootScope.ZoneSelection.zoneSelect = "false";
					 		$rootScope.isBackFromZone = false;
					 		$rootScope.isEmpManagement = false;
							$scope.addEmployee.empId = $rootScope.ZoneSelection.empId;
							$scope.addEmployee.dept = $rootScope.ZoneSelection.dept;
							$scope.addEmployee.firstName = $rootScope.ZoneSelection.firstName;
							$scope.addEmployee.lastName = $rootScope.ZoneSelection.lastName;
							
							$scope.addEmployee.shift = $rootScope.ZoneSelection.shift;
							$scope.addEmployee.countryCode = $rootScope.ZoneSelection.countryCode;
							$scope.addEmployee.phoneNumber = $rootScope.ZoneSelection.phoneNumber;
							$scope.addEmployee.shift = $rootScope.ZoneSelection.shift;
							var primaryLocation = $rootScope.ZoneSelection.primaryLocation;
							var secondaryLocation = $rootScope.ZoneSelection.secondaryLocation;
							var tertiaryLocation = $rootScope.ZoneSelection.tertiaryLocation;
							
							
							
							
							//if($rootScope.ZoneSelection.selectedZone != undefined && $rootScope.ZoneSelection.selectedZone != null)
							//{
							if($rootScope.locationInfo == 'primaryLocation'){
								$rootScope.locationInfo ="";
							$scope.addEmployee.primaryLocation = $rootScope.ZoneSelection.selectedZone;
							//
							$scope.addEmployee.secondaryLocation = $rootScope.ZoneSelection.secondaryLocation;
							$scope.addEmployee.tertiaryLocation = $rootScope.ZoneSelection.tertiaryLocation;
							}
							//}else
							//{
							//	$scope.addEmployee.primaryLocation = primaryLocation;
							//}
							//if($rootScope.ZoneSelection.selectedZone != undefined && $rootScope.ZoneSelection.selectedZone != null)
							//{
							if ($rootScope.locationInfo == 'secondaryLocation'){
								$rootScope.locationInfo ="";
							$scope.addEmployee.secondaryLocation = $rootScope.ZoneSelection.selectedZone;
							$scope.addEmployee.primaryLocation = $rootScope.ZoneSelection.primaryLocation;
							//$scope.addEmployee.secondaryLocation = $rootScope.ZoneSelection.secondaryLocation;
							$scope.addEmployee.tertiaryLocation = $rootScope.ZoneSelection.tertiaryLocation;
							}
							//}else
							//{
							//	$scope.addEmployee.secondaryLocation = secondaryLocation;
							//}
							//if($rootScope.ZoneSelection.selectedZone != undefined && $rootScope.ZoneSelection.selectedZone != null)
							//{
							 if ($rootScope.locationInfo == 'tertiaryLocation'){
								$rootScope.locationInfo ="";
								$scope.addEmployee.tertiaryLocation = $rootScope.ZoneSelection.selectedZone;
								$scope.addEmployee.primaryLocation = $rootScope.ZoneSelection.primaryLocation;
								$scope.addEmployee.secondaryLocation = $rootScope.ZoneSelection.secondaryLocation;
								//$scope.addEmployee.tertiaryLocation = $rootScope.ZoneSelection.tertiaryLocation;
								}
							//}else
							//{
							//	$scope.addEmployee.tertiaryLocation = tertiaryLocation;
							//}
							$scope.zoneDetails = $rootScope.ZoneSelection.zoneDetails;
							$rootScope.ZoneSelection = {};
							$rootScope.setPageName("Employee Management");
			 	}else
		 		{	 $scope.getZoneDetails();
					  		/*$scope.tagPeopleViewScreen = false;
							$scope.employeeEditScreen = false;
							$scope.employeeEditUpdateScreen = false;
							$scope.tagSKUReturnScreen = false;*/
							}
					};
					$scope.displayEmployeeMultipleAddScreen = function() {
						$scope.showMultipleSearch = false;
						$scope.showSearchIcon = false;
						$scope.employeeViewScreen = false;
						$scope.employeeEditScreen = false;
						$scope.employeeEditUpdateScreen = false;
						$scope.employeeAddScreen = false;
						$scope.searchButton = false;
						
						$scope.trackMacId = '';
						if(!$scope.multiEmployeeAddScreen)
							$scope.employeeMultipleAddFileUploadScreen = true;
						
						$("#idEmployeeView").removeClass("active");
						$("#idEmployeeMultipleAdd").addClass("active");
						$("#idEmployeeAdd").removeClass("active");
						$("#idEmployeeEdit").removeClass("active");
						
					};
					
					$scope.displayEmployeeEditScreen = function() {
						$scope.empList = [];
						$scope.empViewData = [];
						//$scope.empList.length = 0;
						$scope.empViewData.length = 0;
						$scope.searchText = "";
						$scope.showSearchIcon = true;
						$scope.showMultipleSearch = false;
						$scope.employeeViewScreen = false;
						$scope.employeeEditScreen = true;
						$scope.employeeEditUpdateScreen = false;
						$scope.employeeAddScreen = false;
						$scope.employeeMultipleAddFileUploadScreen = false;
						$scope.multiEmployeeAddScreen = false;
						$scope.selectedRow = -1;
						$scope.currentPage = 1;
						$scope.searchButton = true;
						$scope.trackMacId = '';

						$("#idEmployeeView").removeClass("active");
						$("#idEmployeeAdd").removeClass("active");
						$("#idEmployeeEdit").addClass("active");
						$("#idEmployeeMultipleAdd").removeClass("active");
						$scope.getEmpViewAllData();
						

					};

					// to display enabled sub menus

					if ($scope.user.capabilityDetails != undefined
							&& $scope.user.capabilityDetails != null) {
						
						if($rootScope.isBackFromZone == true)
						{
							$rootScope.isBackFromZone = false;
							if($rootScope.isEmpManagement == true){
								$scope.displayEmployeeAddScreen();
							}else if($rootScope.isEmpManagementEdit == true){
								//$scope.displayEmployeeEditScreen();
								//$scope.editEmployeeValue();
								
								
								$scope.searchButton = false;
								$scope.employeeEditScreen = false;
								$scope.employeeEditUpdateScreen = true;
								$scope.employeeAddScreen = false;
								$scope.employeeMultipleAddFileUploadScreen = false;
								$scope.multiEmployeeAddScreen = false;
								$scope.showMultipleSearch = false;
								$scope.employeeViewScreen = false;
								
								$timeout(function() {
									$("#idEmployeeView").removeClass("active");
									$("#idEmployeeAdd").removeClass
									("active");
									$("#idEmployeeEdit").addClass("active");
									$("#idEmployeeMultipleAdd").removeClass("active"); 
								}, 100);
								
								if ($rootScope.ZoneSelection.zoneSelect != undefined && $rootScope.ZoneSelection.zoneSelect == "true" && $rootScope.isEmpManagement == false && $rootScope.isEmpManagementEdit == true){
							 		$rootScope.ZoneSelection.zoneSelect = "false";
							 		$rootScope.isBackFromZone = false;
							 		$rootScope.isEmpManagementEdit = false;
							 		$scope.authen = {};
							 		$scope.authen.department ={};
							 		$scope.authen.shiftDetails ={};
									$scope.authen.empId = $rootScope.ZoneSelection.empId;
									$scope.authen.firstName = $rootScope.ZoneSelection.firstName;
									$scope.authen.lastName = $rootScope.ZoneSelection.lastName;
									$scope.authen.countryCode = $rootScope.ZoneSelection.countryCode;
									$scope.authen.phoneNumber = $rootScope.ZoneSelection.phoneNumber;
									$scope.authen.department.deptName = $rootScope.ZoneSelection.dept;
									$scope.authen.shiftDetails.shiftName = $rootScope.ZoneSelection.shift;
									$scope.authen.status = $rootScope.ZoneSelection.status;
									
									var primaryLocation = $rootScope.ZoneSelection.primaryLocation;
									var secondaryLocation = $rootScope.ZoneSelection.secondaryLocation;
									var tertiaryLocation = $rootScope.ZoneSelection.tertiaryLocation;
									
									if($rootScope.selectedRowOnZoneDetail != undefined && $rootScope.selectedRowOnZoneDetail != null){
										$scope.selectedRow= $rootScope.selectedRowOnZoneDetail.selectedRow;
										$scope.currentPage = $rootScope.selectedRowOnZoneDetail.pageNum ;
										$scope.empList = $rootScope.selectedRowOnZoneDetail.employeeDetail ;
										
									}else{
										$scope.selectedRow= -1;
										$scope.currentPage = 1;
										$scope.empList = [];
									}
									
									$scope.employeeFilterList = $scope.empList;
									$scope.totalItems = $scope.employeeFilterList.length;
									$scope.currentPage = 1;
									$scope.getTotalPages();
									$scope.getAssignedTagIds($scope.employeeFilterList);
									if ($scope.empList.length == 0
											|| $scope.employeeFilterList.length == 0) {
										$scope.empNoData = true;

									} else {
										$scope.empNoData = false;

										if ($scope.employeeFilterList.length > $scope.numPerPage) {
											$scope.empViewData = JSON.parse(JSON
													.stringify($scope.employeeFilterList
															.slice(0, $scope.numPerPage)));
											$scope.PaginationTab = true;
										} else {
											$scope.empViewData = JSON.parse(JSON
													.stringify($scope.employeeFilterList));
											$scope.PaginationTab = false;
										}
									
									
								}
									
									$rootScope.selectedRowOnZoneDetail = null;
									
									//if($rootScope.ZoneSelection.selectedZone != undefined && $rootScope.ZoneSelection.selectedZone != null)
									//{
										if($rootScope.locationInfo == 'primaryLoc'){
										$rootScope.locationInfo ="";
									$scope.authen.location1 = $rootScope.ZoneSelection.selectedZone;
									//
									$scope.authen.location2 = $rootScope.ZoneSelection.secondaryLocation;
									$scope.authen.location3 = $rootScope.ZoneSelection.tertiaryLocation;
									}
									//}else
									//{
									//	$scope.authen.location1  = primaryLocation;
									//}
									//if($rootScope.ZoneSelection.selectedZone != undefined && $rootScope.ZoneSelection.selectedZone != null)
									//{
									
									if ($rootScope.locationInfo == 'secondaryLoc'){
										$rootScope.locationInfo ="";
									$scope.authen.location2 = $rootScope.ZoneSelection.selectedZone;
									$scope.authen.location1 = $rootScope.ZoneSelection.primaryLocation;
									//$scope.addEmployee.secondaryLocation = $rootScope.ZoneSelection.secondaryLocation;
									$scope.authen.location3 = $rootScope.ZoneSelection.tertiaryLocation;
									}
									//}else
									//{
									//	$scope.authen.location2 = secondaryLocation;
									//}
									//if($rootScope.ZoneSelection.selectedZone != undefined && $rootScope.ZoneSelection.selectedZone != null)
									//{
									 if ($rootScope.locationInfo == 'tertiaryLoc'){
										$rootScope.locationInfo ="";
										$scope.authen.location3 = $rootScope.ZoneSelection.selectedZone;
										$scope.authen.location1 = $rootScope.ZoneSelection.primaryLocation;
										$scope.authen.location2 = $rootScope.ZoneSelection.secondaryLocation;
										//$scope.addEmployee.tertiaryLocation = $rootScope.ZoneSelection.tertiaryLocation;
										}
									//}else
									//{
									//	$scope.$scope.authen.location3 = tertiaryLocation;
									//}
									$scope.zoneDetails = $rootScope.ZoneSelection.zoneDetails;
									$rootScope.ZoneSelection = {};
									$rootScope.setPageName("Employee Management");
					 	}else
				 		{	 $scope.getZoneDetails();
							  		$scope.tagPeopleViewScreen = false;
									$scope.employeeEditScreen = false;
									$scope.employeeEditUpdateScreen = false;
									$scope.tagSKUReturnScreen = false;
									}
							}
							
						}else{ 
							if ($scope.user.capabilityDetails.PEVA == 1) {
								$scope.displayEmployeeViewScreen();
							}else if ($scope.user.capabilityDetails.PEAD == 1  ) {
								$rootScope.isBackFromZone = false;
								$scope.displayEmployeeAddScreen();
							}else if ($scope.user.capabilityDetails.PMEA == 1 ) {
								$scope.displayEmployeeMultipleAddScreen();
							}else if ($scope.user.capabilityDetails.PEED == 1) {
								$scope.displayEmployeeEditScreen();
							} 
						}
						
						if ($scope.user.capabilityDetails.PEAD == 1
								||  $scope.user.capabilityDetails.PEED == 1 ) {
							$scope.getZoneDetails();
							$scope.getDepartmentDetails();
							$scope.getShiftDetails();
						}  
					}
					$scope.getJsondata = function() {
						$scope.jsonContent = [];
						$scope.csvContent =[];
						$scope.ext = "";
						var files = document.getElementById('upfile').files;
						var jsonfileName = document.getElementById('upfile').value;
						$scope.SelectedFile = jsonfileName;
						$scope.ext = jsonfileName.substring(jsonfileName.lastIndexOf('.') + 1); 
		                 if (files.length <= 0) {
							    return false;
							  }     						  
		                   if($scope.ext.toLowerCase() == 'json'){
		                	   		  $scope.keyNames =[];
		                	   		$scope.jsonDataArray.length = 0;
									  $scope.errorImport= false;	
									  var fr = new FileReader();										 					 
									  fr.onload = function(e) { 
										  try {
											  multipleEmployees = new Array(); 
											  
		  									 $scope.jsonContent = JSON.parse(e.target.result);
		  									//console.log($scope.jsonContent);
		  									 getAllKeys($scope.jsonDataArray,$scope.jsonContent, '');
		  									for (var i = 0; i < $scope.jsonDataArray.length; i++) {
												if (!$scope.keyNames.includes($scope.jsonDataArray[i]['key']))
													$scope.keyNames.push($scope.jsonDataArray[i]['key']);
												
		  									}
		  								//console.log($scope.keyNames);
		  									
											
		  									
										  } catch(e) {
											  $rootScope.alertErrorDialog(e); // error in the above string (in this case, yes)!
											  $('input[type=file]').val('');
										    }
										 // $scope.keyNames = headerCSV;
		  								}
									 //$scope.sendInsertData(multipleMaterials);
		  								fr.readAsText(files.item(0));	          							 
		  				  }else if($scope.ext.toLowerCase() == 'csv'){
		  					$scope.jsonDataArray = [];
		  					$scope.keyNames =[];
		  					$scope.csvContent =[];
		    					$scope.errorImport= false;	
		  					  var fr = new FileReader();										 					 
		  					  fr.onload = function(e) { 
		  						  try {
		  							  var csv = e.target.result;
		  							$scope.csvContent = csv;
		  							var headerCSV,dataCSV,headerCSVlength=[];
		  							$scope.parsedCSVfile = Papa.parse(csv, {
		  								header: true,
		  								complete: function(results) {
											 headerCSV = results.data[0];
											 dataCSV = results.data;
											headerCSVlength =headerCSV;
											},
											header: true,
											skipEmptyLines: true,
		  							
										});
		  							//$scope.jsonDataArray=dataCSV;
		  							 getAllKeys($scope.jsonDataArray,dataCSV, '');
		  							for (var i = 0; i < $scope.jsonDataArray.length; i++) {
										if (!$scope.keyNames.includes($scope.jsonDataArray[i]['key']))
											$scope.keyNames.push($scope.jsonDataArray[i]['key']);
										
  									}
		  							//console.log($scope.jsonDataArray);
		  							
		  					          
		  						  } catch(e) {
		  							  $rootScope.alertErrorDialog(e); // error in the above string (in this case, yes)!
		  							  $('input[type=file]').val('');
		  						    }
		  						 
		  						//$scope.keyNames = headerCSV;
		  						}
		  						fr.readAsText(files.item(0));
		  					
		  				  }
					}
					$scope.importEmployeeDetailFile  = function(){
						 var fileVal = $('#upfile').val(); 
					        if(fileVal=='' || ($scope.ext.toLowerCase() != 'csv' && $scope.ext.toLowerCase() != 'json')) 
					        { 
					        	$rootScope.alertErrorDialog("Please select a valid CSV or JSON file");
								return;
					        }
					        else if($scope.ext.toLowerCase() == 'csv' || $scope.ext.toLowerCase() == 'json' ){
					        	$scope.employeeMultipleAddFileUploadScreen = false;
					        	$scope.multiEmployeeAddScreen = true;
					        	$scope.files = document.getElementById('upfile').files;
					        	$scope.jsonfileName = document.getElementById('upfile').value;
							$timeout(function() {
								$("#id_multipleEmployeeEmployeeID").val("Select an option"); 
								$("#id_multipleEmployeeDepatment").val("Select an option");
								$("#id_multipleEmployeeFirstName").val("Select an option");
								$("#id_multipleEmployeeLastname").val("Select an option");
								$("#id_multipleEmployeePrimaryLocation").val("Select an option");
								$("#id_multipleEmployeeSecondaryLocation").val("Select an option");
								$("#id_multipleEmployeeTertiaryLocation").val("Select an option");
								$("#id_multipleEmployeePhNo").val("Select an option");
								$("#id_multipleEmployeeShift").val("Select an option");
								//$("#id_multipleEmployeeStatus").val("Select an option");
							}, 200);
				        }
						
					}
					function getAllKeys(keys, obj, path) {
						for (key in obj) {
							var currpath = path + '/' + key;

							if (typeof (obj[key]) == 'object'
									|| (obj[key] instanceof Array))
								getAllKeys(keys, obj[key], currpath);
							else
								keys.push({
									'key' : key,
									"path" : currpath,
									"value" : obj[key]
								});
						}
					}
					$scope.Upload = function(){
						//$scope.jsonContent = [];
						$scope.ext ="";
						var files = $scope.files;
						var jsonfileName = $scope.jsonfileName;
						$scope.SelectedFile = jsonfileName;
						$scope.ext = jsonfileName.substring(jsonfileName.lastIndexOf('.') + 1); 
						 var fr = new FileReader();
		                 if (files.length <= 0) {
							    return false;
							  }     	
		                 if($scope.ext.toLowerCase() == 'csv'){
		                	
		                		var headerCSV,dataCSV,headerCSVlength=[];
	  							$scope.parsedCSVfile = Papa.parse($scope.csvContent, {
									complete: function(results) {
										 headerCSV = results.data[0];
										 dataCSV = results.data;
										headerCSVlength =headerCSV;
										
									},
									quoteChar: '"',
									header: true,
									skipEmptyLines: true
									
									});
	  							//console.log($scope.csvContent);

			                	   multipleEmployees = new Array(); 
			                	   $scope.jsonMultipleEmployees =[];
			                	   
			                	   var localEmpDetail = {};
									for (var i = 0; i < $scope.jsonDataArray.length; i++) {

										var json = $scope.jsonDataArray[i];
										if (json['key'] == $scope.multiEmployee.employeeID) {
											localEmpDetail.empId = json['value'];
										} else if (json['key'] == $scope.multiEmployee.depatment) {
											localEmpDetail.department = json['value'];
										} else if (json['key'] == $scope.multiEmployee.fName) {
											localEmpDetail.firstName = json['value'];
										} else if (json['key'] == $scope.multiEmployee.lName) {
											localEmpDetail.lastName = json['value'];
										} else if (json['key'] == $scope.multiEmployee.primaryLocation) {
											localEmpDetail.location1 = json['value'];
										} else if (json['key'] == $scope.multiEmployee.secondaryLocation) {
											localEmpDetail.location2 = json['value'];
										} else if (json['key'] == $scope.multiEmployee.tertiaryLoc) {
											localEmpDetail.location3 = json['value'];
										}
										else if (json['key'] == $scope.multiEmployee.phNo) {
											localEmpDetail.phoneNumber = json['value'];
										}
										else if (json['key'] == $scope.multiEmployee.shift) {
											localEmpDetail.shiftDetails = json['value'];
										}
										if ($scope.multiEmployee.employeeID != undefined
												&& $scope.multiEmployee.depatment != undefined
												&& $scope.multiEmployee.fName != undefined
												&& $scope.multiEmployee.lName != undefined
												&& $scope.multiEmployee.primaryLocation != undefined
												&& $scope.multiEmployee.secondaryLocation != undefined
												&& $scope.multiEmployee.tertiaryLoc != undefined
												&& $scope.multiEmployee.phNo != undefined
												&& $scope.multiEmployee.shift != undefined) {
											if (localEmpDetail.hasOwnProperty('empId')
													&& localEmpDetail.hasOwnProperty('department')
													&& localEmpDetail.hasOwnProperty('firstName')
													&& localEmpDetail.hasOwnProperty('lastName')
													&& localEmpDetail.hasOwnProperty('location1')
													&& localEmpDetail.hasOwnProperty('location2')
													&& localEmpDetail.hasOwnProperty('location3')
													&& localEmpDetail.hasOwnProperty('phoneNumber')
													&& localEmpDetail.hasOwnProperty('shiftDetails')) {
												
												localEmpDetail.status ="Working";
												$scope.jsonMultipleEmployees.push(localEmpDetail);
												localEmpDetail = {};

											}
										}
									}
									$scope.multipleEmployeeValidation();
			                	  // console.log($scope.jsonMultipleEmployees);
			                	   
				                   $scope.sendInsertData($scope.jsonMultipleEmployees);
										fr.readAsText(files.item(0));
			                  
		                 
	  							
		                 }
		                 else if($scope.ext.toLowerCase() == 'json'){
		                	
		                	  
		                	   $scope.jsonMultipleEmployees =[];
		                	   var localEmpDetail = {};
		                	  
								for (var i = 0; i < $scope.jsonDataArray.length; i++) {
								    
									var json = $scope.jsonDataArray[i];

									if (json['key'] == $scope.multiEmployee.employeeID) {
										localEmpDetail.empId = json['value'];
									} else if (json['key'] == $scope.multiEmployee.depatment) {
										localEmpDetail.department = json['value'];
									} else if (json['key'] == $scope.multiEmployee.fName) {
										localEmpDetail.firstName = json['value'];
									} else if (json['key'] == $scope.multiEmployee.lName) {
										localEmpDetail.lastName = json['value'];
									} else if (json['key'] == $scope.multiEmployee.primaryLocation) {
										localEmpDetail.location1 = json['value'];
									} else if (json['key'] == $scope.multiEmployee.secondaryLocation) {
										localEmpDetail.location2 = json['value'];
									} else if (json['key'] == $scope.multiEmployee.tertiaryLoc) {
										localEmpDetail.location3 = json['value'];
									}
									else if (json['key'] == $scope.multiEmployee.phNo) {
										localEmpDetail.phoneNumber = json['value'];
									}
									else if (json['key'] == $scope.multiEmployee.shift) {
										localEmpDetail.shiftDetails = json['value'];
									}
									if ($scope.multiEmployee.employeeID != undefined
											&& $scope.multiEmployee.depatment != undefined
											&& $scope.multiEmployee.fName != undefined
											&& $scope.multiEmployee.lName != undefined
											&& $scope.multiEmployee.primaryLocation != undefined
											&& $scope.multiEmployee.secondaryLocation != undefined
											&& $scope.multiEmployee.tertiaryLoc != undefined
											&& $scope.multiEmployee.phNo != undefined
											&& $scope.multiEmployee.shift != undefined
											) {
										if (localEmpDetail.hasOwnProperty('empId')
												&& localEmpDetail.hasOwnProperty('department')
												&& localEmpDetail.hasOwnProperty('firstName')
												&& localEmpDetail.hasOwnProperty('lastName')
												&& localEmpDetail.hasOwnProperty('location1')
												&& localEmpDetail.hasOwnProperty('location2')
												&& localEmpDetail.hasOwnProperty('location3')
												&& localEmpDetail.hasOwnProperty('phoneNumber')
												&& localEmpDetail.hasOwnProperty('shiftDetails')) {
											localEmpDetail.status ="Working";
											$scope.jsonMultipleEmployees.push(localEmpDetail);
											localEmpDetail = {};

										}
									}
								}
								$scope.multipleEmployeeValidation();
		                	  // console.log($scope.jsonMultipleEmployees);
		                	   
			                   $scope.sendInsertData($scope.jsonMultipleEmployees);
									fr.readAsText(files.item(0));
		                 
		                   }
		                   
					}
					$scope.bulkTagCloseBtnClik = function() {
						$('#bulkEmpUpload_modal').modal('hide');
						$scope.errorArray = [];
					};
		                 $scope.sendInsertData = function(data) {
								if (!$rootScope.checkNetconnection()) { 
									$rootScope.alertDialogChecknet();
									return; } 
								if ($scope.errorArray.length > 0) {

									$('#bulkEmpUpload_modal').modal('show');

								} else if (data.length > 0) {
									 
								$rootScope.showloading('#loadingBar');
										$http({
										method : 'POST',
										data : data,
										url : './multipleStaffs',
										
										})
										.then(
												function(result) {
													$rootScope.hideloading('#loadingBar');
													if (result.data.status == "success") {
														$rootScope.showAlertDialog(result.data.message);
														$scope.cancelMultipleEmployee();
														
													} else if (result.data.status == "error") {
														$rootScope.showAlertDialog(result.data.message);
														
													}				
													},
												function(error) {
																$rootScope.hideloading('#loadingBar');
																$rootScope.fnHttpError(error);
													});
										
								 }else {
										//alert('Invalid Data.');
									 $rootScope.showAlertDialog("Invalid Data");
										
										}
								
							};
					
					
					// multisearch code starts
					$scope.multipleSearch = function() {
						$scope.showMultipleSearch = true;
						$scope.searchButton = false
						$scope.firstMultiSearchDropDown = "All";
						$scope.secondMultiSearchDropDown = "All";
						$scope.varANDORDropDown = "AND";
						$scope.firstSearchText = "";
						$scope.secondSearchText = "";
					}
					$scope.closeMultiSearchView = function() {
						if($scope.showMultipleSearch){
							
							$scope.showMultipleSearch = false;
							$scope.searchButton = true;
							$scope.employeeFilterList = $scope.empList;
							$scope.totalItems = $scope.employeeFilterList.length;
							$scope.currentPage = 1;
							$scope.getTotalPages();
							$scope.getAssignedTagIds($scope.employeeFilterList);
							if ($scope.empList.length == 0
									|| $scope.employeeFilterList.length == 0) {
								$scope.empNoData = true;

							} else {
								$scope.empNoData = false;

								if ($scope.employeeFilterList.length > $scope.numPerPage) {
									$scope.empViewData = JSON.parse(JSON
											.stringify($scope.employeeFilterList
													.slice(0, $scope.numPerPage)));
									$scope.PaginationTab = true;
								} else {
									$scope.empViewData = JSON.parse(JSON
											.stringify($scope.employeeFilterList));
									$scope.PaginationTab = false;
								}
							
							
						}
						}
					}

					$scope.fnfirstSearchANDData = function() {

						$scope.firstSearchANDDataArray = [];
						for (var i = 0; i < $scope.empList.length; i++) {
							// to check all
							if ($scope.firstMultiSearchDropDown == "All") {
								if ($scope.firstSearchText == ""
										|| $scope.firstSearchText == undefined) {

									$scope.firstSearchANDDataArray
											.push($scope.empList[i]);

								} else {

									if ((($scope.empList[i].empId).toString() != undefined
											&& ($scope.empList[i].empId)
													.toString() != null ? ($scope.empList[i].empId)
											.toString()
											: "").toLowerCase().includes(
											$scope.firstSearchText
													.toLowerCase())
											|| ($scope.empList[i].firstName != undefined
													&& $scope.empList[i].firstName != null ? $scope.empList[i].firstName
													: "")
													.toLowerCase()
													.includes(
															$scope.firstSearchText
																	.toLowerCase())
											|| ($scope.empList[i].lastName != undefined
													&& $scope.empList[i].lastName != null ? $scope.empList[i].lastName
													: "")
													.toLowerCase()
													.includes(
															$scope.firstSearchText
																	.toLowerCase())
											|| ($scope.empList[i].phoneNumber != undefined
													&& $scope.empList[i].phoneNumber != null ? $scope.empList[i].phoneNumber
													: "")
													.toLowerCase()
													.includes(
															$scope.firstSearchText
																	.toLowerCase())
											|| ($scope.empList[i].location1 != undefined
													&& $scope.empList[i].location1 != null ? $scope.empList[i].location1
													: "")
													.toLowerCase()
													.includes(
															$scope.firstSearchText
																	.toLowerCase())
											|| ($scope.empList[i].department.deptName != undefined
													&& $scope.empList[i].department.deptName != null ? $scope.empList[i].department.deptName
													: "")
													.toLowerCase()
													.includes(
															$scope.firstSearchText
																	.toLowerCase())
											|| ($scope.empList[i].shiftDetails.shiftName != undefined
													&& $scope.empList[i].shiftDetails.shiftName != null ? $scope.empList[i].shiftDetails.shiftName
													: "")
													.toLowerCase()
													.includes(
															$scope.firstSearchText
																	.toLowerCase())
											|| ($scope.empList[i].location2 != undefined
													&& $scope.empList[i].location2 != null ? $scope.empList[i].location2
													: "")
													.toLowerCase()
													.includes(
															$scope.firstSearchText
																	.toLowerCase())
											|| ($scope.empList[i].status != undefined
													&& $scope.empList[i].status != null ? $scope.empList[i].status
													: "")
													.toLowerCase()
													.includes(
															$scope.firstSearchText
																	.toLowerCase())) {
										$scope.firstSearchANDDataArray
												.push($scope.empList[i]);

									}
								}
							}
							// to check other than all
							else {
								if ($scope.firstMultiSearchDropDown == "empId") {
									if ($scope.firstSearchText == ""
											|| $scope.firstSearchText == undefined) {
										$scope.firstSearchANDDataArray
												.push($scope.empList[i]);
									} else if ((($scope.empList[i].empId)
											.toString() != undefined
											&& ($scope.empList[i].empId)
													.toString() != null ? ($scope.empList[i].empId)
											.toString()
											: "").toLowerCase().includes(
											$scope.firstSearchText
													.toLowerCase()))
										$scope.firstSearchANDDataArray
												.push($scope.empList[i]);

								} else if ($scope.firstMultiSearchDropDown == "firstName") {
									if ($scope.firstSearchText == ""
											|| $scope.firstSearchText == undefined) {
										$scope.firstSearchANDDataArray
												.push($scope.empList[i]);
									} else if (($scope.empList[i].firstName != undefined
											&& $scope.empList[i].firstName != null ? $scope.empList[i].firstName
											: "").toLowerCase().includes(
											$scope.firstSearchText
													.toLowerCase()))
										$scope.firstSearchANDDataArray
												.push($scope.empList[i]);

								} else if ($scope.firstMultiSearchDropDown == "lastName") {
									if ($scope.firstSearchText == ""
											|| $scope.firstSearchText == undefined) {
										$scope.firstSearchANDDataArray
												.push($scope.empList[i]);
									} else if (($scope.empList[i].lastName != undefined
											&& $scope.empList[i].lastName != null ? $scope.empList[i].lastName
											: "").toLowerCase().includes(
											$scope.firstSearchText
													.toLowerCase()))
										$scope.firstSearchANDDataArray
												.push($scope.empList[i]);

								} else if ($scope.firstMultiSearchDropDown == "Department") {
									if ($scope.firstSearchText == ""
											|| $scope.firstSearchText == undefined) {
										$scope.firstSearchANDDataArray
												.push($scope.empList[i]);
									} else if (($scope.empList[i].department.deptName != undefined
											&& $scope.empList[i].department.deptName != null ? $scope.empList[i].department.deptName
											: "").toLowerCase().includes(
											$scope.firstSearchText
													.toLowerCase()))
										$scope.firstSearchANDDataArray
												.push($scope.empList[i]);
								} else if ($scope.firstMultiSearchDropDown == "primaryLocation") {
									if ($scope.firstSearchText == ""
											|| $scope.firstSearchText == undefined) {
										$scope.firstSearchANDDataArray
												.push($scope.empList[i]);
									} else if (($scope.empList[i].location1 != undefined
											&& $scope.empList[i].location1 != null ? $scope.empList[i].location1
											: "").toLowerCase().includes(
											$scope.firstSearchText
													.toLowerCase()))
										$scope.firstSearchANDDataArray
												.push($scope.empList[i]);

								} else if ($scope.firstMultiSearchDropDown == "secondaryLocation") {
									if ($scope.firstSearchText == ""
											|| $scope.firstSearchText == undefined) {
										$scope.firstSearchANDDataArray
												.push($scope.empList[i]);
									} else if (($scope.empList[i].location2 != undefined
											&& $scope.empList[i].location2 != null ? $scope.empList[i].location2
											: "").toLowerCase().includes(
											$scope.firstSearchText
													.toLowerCase()))
										$scope.firstSearchANDDataArray
												.push($scope.empList[i]);

								} else if ($scope.firstMultiSearchDropDown == "phoneNo") {
									if ($scope.firstSearchText == ""
											|| $scope.firstSearchText == undefined) {
										$scope.firstSearchANDDataArray
												.push($scope.empList[i]);
									} else if (($scope.empList[i].phoneNumber != undefined
											&& $scope.empList[i].phoneNumber != null ? $scope.empList[i].phoneNumber
											: "").toLowerCase().includes(
											$scope.firstSearchText
													.toLowerCase()))
										$scope.firstSearchANDDataArray
												.push($scope.empList[i]);

								} else if ($scope.firstMultiSearchDropDown == "Shift") {
									if ($scope.firstSearchText == ""
											|| $scope.firstSearchText == undefined) {
										$scope.firstSearchANDDataArray
												.push($scope.empList[i]);
									} else if (($scope.empList[i].shiftDetails.shiftName != undefined
											&& $scope.empList[i].shiftDetails.shiftName != null ? $scope.empList[i].shiftDetails.shiftName
											: "").toLowerCase().includes(
											$scope.firstSearchText
													.toLowerCase()))
										$scope.firstSearchANDDataArray
												.push($scope.empList[i]);

								} else if ($scope.firstMultiSearchDropDown == "Status") {
									if ($scope.firstSearchText == ""
											|| $scope.firstSearchText == undefined) {
										$scope.firstSearchANDDataArray
												.push($scope.empList[i]);
									} else if (($scope.empList[i].status != undefined
											&& $scope.empList[i].status != null ? $scope.empList[i].status
											: "").toLowerCase().includes(
											$scope.firstSearchText
													.toLowerCase()))
										$scope.firstSearchANDDataArray
												.push($scope.empList[i]);

								}

							}
						}
					}

					$scope.multiSearchChanged = function() {
						$scope.firstSearchANDDataArray = [];
						$scope.fnfirstSearchANDData();
						//console.log($scope.firstSearchANDDataArray);

						if ($scope.varANDORDropDown == "AND") {
							$scope.disableSecondFilterText = false;
							$scope.disableSecondFilterDropDown = false;
							$scope.empViewData.length = 0;
							$scope.employeeFilterList.length = 0;

							for (var i = 0; i < $scope.firstSearchANDDataArray.length; i++) {
								if ($scope.secondMultiSearchDropDown == "All") {

									if ($scope.secondSearchText == ""
											|| $scope.secondSearchText == undefined) {
										$scope.employeeFilterList
												.push($scope.firstSearchANDDataArray[i]);
									} else {
										if ((($scope.firstSearchANDDataArray[i].empId)
												.toString() != undefined
												&& ($scope.firstSearchANDDataArray[i].empId)
														.toString() != null ? ($scope.firstSearchANDDataArray[i].empId)
												.toString()
												: "").toLowerCase().includes(
												$scope.secondSearchText
														.toLowerCase())
												|| ($scope.firstSearchANDDataArray[i].firstName != undefined
														&& $scope.firstSearchANDDataArray[i].firstName != null ? $scope.firstSearchANDDataArray[i].firstName
														: "")
														.toLowerCase()
														.includes(
																$scope.secondSearchText
																		.toLowerCase())
												|| ($scope.firstSearchANDDataArray[i].lastName != undefined
														&& $scope.firstSearchANDDataArray[i].lastName != null ? $scope.firstSearchANDDataArray[i].lastName
														: "")
														.toLowerCase()
														.includes(
																$scope.secondSearchText
																		.toLowerCase())
												|| ($scope.firstSearchANDDataArray[i].phoneNumber != undefined
														&& $scope.firstSearchANDDataArray[i].phoneNumber != null ? $scope.firstSearchANDDataArray[i].phoneNumber
														: "")
														.toLowerCase()
														.includes(
																$scope.secondSearchText
																		.toLowerCase())
												|| ($scope.firstSearchANDDataArray[i].location1 != undefined
														&& $scope.firstSearchANDDataArray[i].location1 != null ? $scope.firstSearchANDDataArray[i].location1
														: "")
														.toLowerCase()
														.includes(
																$scope.secondSearchText
																		.toLowerCase())
												|| ($scope.firstSearchANDDataArray[i].department.deptName != undefined
														&& $scope.firstSearchANDDataArray[i].department.deptName != null ? $scope.firstSearchANDDataArray[i].department.deptName
														: "")
														.toLowerCase()
														.includes(
																$scope.secondSearchText
																		.toLowerCase())
												|| ($scope.firstSearchANDDataArray[i].shiftDetails.shiftName != undefined
														&& $scope.firstSearchANDDataArray[i].shiftDetails.shiftName != null ? $scope.firstSearchANDDataArray[i].shiftDetails.shiftName
														: "")
														.toLowerCase()
														.includes(
																$scope.secondSearchText
																		.toLowerCase())
												|| ($scope.firstSearchANDDataArray[i].location2 != undefined
														&& $scope.firstSearchANDDataArray[i].location2 != null ? $scope.firstSearchANDDataArray[i].location2
														: "")
														.toLowerCase()
														.includes(
																$scope.secondSearchText
																		.toLowerCase())
												|| ($scope.firstSearchANDDataArray[i].status != undefined
														&& $scope.firstSearchANDDataArray[i].status != null ? $scope.firstSearchANDDataArray[i].status
														: "")
														.toLowerCase()
														.includes(
																$scope.secondSearchText
																		.toLowerCase())) {
											$scope.employeeFilterList
													.push($scope.firstSearchANDDataArray[i]);

										}
									}
								} else {

									if ($scope.secondMultiSearchDropDown == "empId") {
										if ($scope.secondSearchText == ""
												|| $scope.secondSearchText == undefined) {
											$scope.employeeFilterList
													.push($scope.firstSearchANDDataArray[i]);
										} else if ((($scope.firstSearchANDDataArray[i].empId)
												.toString() != undefined
												&& ($scope.firstSearchANDDataArray[i].empId)
														.toString() != null ? ($scope.firstSearchANDDataArray[i].empId)
												.toString()
												: "").toLowerCase().includes(
												$scope.secondSearchText
														.toLowerCase()))
											$scope.employeeFilterList
													.push($scope.firstSearchANDDataArray[i]);

									} else if ($scope.secondMultiSearchDropDown == "firstName") {
										if ($scope.secondSearchText == ""
												|| $scope.secondSearchText == undefined) {
											$scope.employeeFilterList
													.push($scope.firstSearchANDDataArray[i]);
										} else if (($scope.firstSearchANDDataArray[i].firstName != undefined
												&& $scope.firstSearchANDDataArray[i].firstName != null ? $scope.firstSearchANDDataArray[i].firstName
												: "").toLowerCase().includes(
												$scope.secondSearchText
														.toLowerCase()))
											$scope.employeeFilterList
													.push($scope.firstSearchANDDataArray[i]);

									} else if ($scope.secondMultiSearchDropDown == "lastName") {
										if ($scope.secondSearchText == ""
												|| $scope.secondSearchText == undefined) {
											$scope.employeeFilterList
													.push($scope.firstSearchANDDataArray[i]);
										} else if (($scope.firstSearchANDDataArray[i].lastName != undefined
												&& $scope.firstSearchANDDataArray[i].lastName != null ? $scope.firstSearchANDDataArray[i].lastName
												: "").toLowerCase().includes(
												$scope.secondSearchText
														.toLowerCase()))
											$scope.employeeFilterList
													.push($scope.firstSearchANDDataArray[i]);

									} else if ($scope.secondMultiSearchDropDown == "Department") {
										if ($scope.secondSearchText == ""
												|| $scope.secondSearchText == undefined) {
											$scope.employeeFilterList
													.push($scope.firstSearchANDDataArray[i]);
										} else if (($scope.firstSearchANDDataArray[i].department.deptName != undefined
												&& $scope.firstSearchANDDataArray[i].department.deptName != null ? $scope.firstSearchANDDataArray[i].department.deptName
												: "").toLowerCase().includes(
												$scope.secondSearchText
														.toLowerCase()))
											$scope.employeeFilterList
													.push($scope.firstSearchANDDataArray[i]);
									} else if ($scope.secondMultiSearchDropDown == "primaryLocation") {
										if ($scope.secondSearchText == ""
												|| $scope.secondSearchText == undefined) {
											$scope.employeeFilterList
													.push($scope.firstSearchANDDataArray[i]);
										} else if (($scope.firstSearchANDDataArray[i].location1 != undefined
												&& $scope.firstSearchANDDataArray[i].location1 != null ? $scope.firstSearchANDDataArray[i].location1
												: "").toLowerCase().includes(
												$scope.secondSearchText
														.toLowerCase()))
											$scope.employeeFilterList
													.push($scope.firstSearchANDDataArray[i]);

									} else if ($scope.secondMultiSearchDropDown == "secondaryLocation") {
										if ($scope.secondSearchText == ""
												|| $scope.secondSearchText == undefined) {
											$scope.employeeFilterList
													.push($scope.firstSearchANDDataArray[i]);
										} else if (($scope.firstSearchANDDataArray[i].location2 != undefined
												&& $scope.firstSearchANDDataArray[i].location2 != null ? $scope.firstSearchANDDataArray[i].location2
												: "").toLowerCase().includes(
												$scope.secondSearchText
														.toLowerCase()))
											$scope.employeeFilterList
													.push($scope.firstSearchANDDataArray[i]);

									} else if ($scope.secondMultiSearchDropDown == "phoneNo") {
										if ($scope.secondSearchText == ""
												|| $scope.secondSearchText == undefined) {
											$scope.employeeFilterList
													.push($scope.firstSearchANDDataArray[i]);
										} else if (($scope.firstSearchANDDataArray[i].phoneNumber != undefined
												&& $scope.firstSearchANDDataArray[i].phoneNumber != null ? $scope.firstSearchANDDataArray[i].phoneNumber
												: "").toLowerCase().includes(
												$scope.secondSearchText
														.toLowerCase()))
											$scope.employeeFilterList
													.push($scope.firstSearchANDDataArray[i]);

									} else if ($scope.secondMultiSearchDropDown == "Shift") {
										if ($scope.secondSearchText == ""
												|| $scope.secondSearchText == undefined) {
											$scope.employeeFilterList
													.push($scope.firstSearchANDDataArray[i]);
										} else if (($scope.firstSearchANDDataArray[i].shiftDetails.shiftName != undefined
												&& $scope.firstSearchANDDataArray[i].shiftDetails.shiftName != null ? $scope.firstSearchANDDataArray[i].shiftDetails.shiftName
												: "").toLowerCase().includes(
												$scope.secondSearchText
														.toLowerCase()))
											$scope.employeeFilterList
													.push($scope.firstSearchANDDataArray[i]);

									} else if ($scope.secondMultiSearchDropDown == "Status") {
										if ($scope.secondSearchText == ""
												|| $scope.secondSearchText == undefined) {
											$scope.employeeFilterList
													.push($scope.firstSearchANDDataArray[i]);
										} else if (($scope.firstSearchANDDataArray[i].status != undefined
												&& $scope.firstSearchANDDataArray[i].status != null ? $scope.firstSearchANDDataArray[i].status
												: "").toLowerCase().includes(
												$scope.secondSearchText
														.toLowerCase()))
											$scope.employeeFilterList
													.push($scope.firstSearchANDDataArray[i]);

									}

								}
							}

							$scope.totalItems = $scope.employeeFilterList.length;
							$scope.currentPage = 1;
							$scope.getTotalPages();
							$scope.getAssignedTagIds($scope.employeeFilterList);

							if ($scope.empList.length == 0
									|| $scope.employeeFilterList.length == 0) {
								$scope.empNoData = true;

							} else {
								$scope.empNoData = false;

								if ($scope.employeeFilterList.length > $scope.numPerPage) {
									$scope.empViewData = $scope.employeeFilterList
											.slice(0, $scope.numPerPage);
									$scope.PaginationTab = true;
								} else {
									$scope.empViewData = $scope.employeeFilterList;
									$scope.PaginationTab = false;
								}
							}

						} else if ($scope.varANDORDropDown == "OR") {

							if ($scope.firstMultiSearchDropDown == "All"
									&& ($scope.firstSearchText == "" || $scope.firstSearchText == undefined)) {
								$scope.disableSecondFilterText = true;
								$scope.disableSecondFilterDropDown = true;
								$scope.secondMultiSearchDropDown = "All"
								$scope.secondSearchText = ""

							} else {
								$scope.disableSecondFilterText = false;
								$scope.disableSecondFilterDropDown = false;
							}

							// $scope.empViewData.length = 0;
							// $scope.employeeFilterList.length = 0;
							$scope.empViewData = []
							$scope.employeeFilterList = [];
							$scope.secondSearchORDataArray = [];
							for (var i = 0; i < $scope.empList.length; i++) {
								if ($scope.secondMultiSearchDropDown == "All") {
									if ($scope.secondSearchText == ""
											|| $scope.secondSearchText == undefined) {
										$scope.secondSearchORDataArray
												.push($scope.empList[i]);

									} else {
										if ((($scope.empList[i].empId)
												.toString() != undefined
												&& ($scope.empList[i].empId)
														.toString() != null ? ($scope.empList[i].empId)
												.toString()
												: "").toLowerCase().includes(
												$scope.secondSearchText
														.toLowerCase())
												|| ($scope.empList[i].firstName != undefined
														&& $scope.empList[i].firstName != null ? $scope.empList[i].firstName
														: "")
														.toLowerCase()
														.includes(
																$scope.secondSearchText
																		.toLowerCase())
												|| ($scope.empList[i].lastName != undefined
														&& $scope.empList[i].lastName != null ? $scope.empList[i].lastName
														: "")
														.toLowerCase()
														.includes(
																$scope.secondSearchText
																		.toLowerCase())
												|| ($scope.empList[i].phoneNumber != undefined
														&& $scope.empList[i].phoneNumber != null ? $scope.empList[i].phoneNumber
														: "")
														.toLowerCase()
														.includes(
																$scope.secondSearchText
																		.toLowerCase())
												|| ($scope.empList[i].location1 != undefined
														&& $scope.empList[i].location1 != null ? $scope.empList[i].location1
														: "")
														.toLowerCase()
														.includes(
																$scope.secondSearchText
																		.toLowerCase())
												|| ($scope.empList[i].department.deptName != undefined
														&& $scope.empList[i].department.deptName != null ? $scope.empList[i].department.deptName
														: "")
														.toLowerCase()
														.includes(
																$scope.secondSearchText
																		.toLowerCase())
												|| ($scope.empList[i].shiftDetails.shiftName != undefined
														&& $scope.empList[i].shiftDetails.shiftName != null ? $scope.empList[i].shiftDetails.shiftName
														: "")
														.toLowerCase()
														.includes(
																$scope.secondSearchText
																		.toLowerCase())
												|| ($scope.empList[i].location2 != undefined
														&& $scope.empList[i].location2 != null ? $scope.empList[i].location2
														: "")
														.toLowerCase()
														.includes(
																$scope.secondSearchText
																		.toLowerCase())
												|| ($scope.empList[i].status != undefined
														&& $scope.empList[i].status != null ? $scope.empList[i].status
														: "")
														.toLowerCase()
														.includes(
																$scope.secondSearchText
																		.toLowerCase())) {
											$scope.secondSearchORDataArray
													.push($scope.empList[i]);

										}
									}
								} else {

									// to check other than all in second search
									if ($scope.secondMultiSearchDropDown == "empId") {
										if ($scope.secondSearchText == ""
												|| $scope.secondSearchText == undefined) {
											$scope.secondSearchORDataArray
													.push($scope.empList[i]);
										} else if ((($scope.empList[i].empId)
												.toString() != undefined
												&& ($scope.empList[i].empId)
														.toString() != null ? ($scope.empList[i].empId)
												.toString()
												: "").toLowerCase().includes(
												$scope.secondSearchText
														.toLowerCase()))
											$scope.secondSearchORDataArray
													.push($scope.empList[i]);

									} else if ($scope.secondMultiSearchDropDown == "firstName") {
										if ($scope.secondSearchText == ""
												|| $scope.secondSearchText == undefined) {
											$scope.secondSearchORDataArray
													.push($scope.empList[i]);
										} else if (($scope.empList[i].firstName != undefined
												&& $scope.empList[i].firstName != null ? $scope.empList[i].firstName
												: "").toLowerCase().includes(
												$scope.secondSearchText
														.toLowerCase()))
											$scope.secondSearchORDataArray
													.push($scope.empList[i]);

									} else if ($scope.secondMultiSearchDropDown == "lastName") {
										if ($scope.secondSearchText == ""
												|| $scope.secondSearchText == undefined) {
											$scope.secondSearchORDataArray
													.push($scope.empList[i]);
										} else if (($scope.empList[i].lastName != undefined
												&& $scope.empList[i].lastName != null ? $scope.empList[i].lastName
												: "").toLowerCase().includes(
												$scope.secondSearchText
														.toLowerCase()))
											$scope.secondSearchORDataArray
													.push($scope.empList[i]);

									} else if ($scope.secondMultiSearchDropDown == "Department") {
										if ($scope.secondSearchText == ""
												|| $scope.secondSearchText == undefined) {
											$scope.secondSearchORDataArray
													.push($scope.empList[i]);
										} else if (($scope.empList[i].department.deptName != undefined
												&& $scope.empList[i].department.deptName != null ? $scope.empList[i].department.deptName
												: "").toLowerCase().includes(
												$scope.secondSearchText
														.toLowerCase()))
											$scope.secondSearchORDataArray
													.push($scope.empList[i]);
									} else if ($scope.secondMultiSearchDropDown == "primaryLocation") {
										if ($scope.secondSearchText == ""
												|| $scope.secondSearchText == undefined) {
											$scope.secondSearchORDataArray
													.push($scope.empList[i]);
										} else if (($scope.empList[i].location1 != undefined
												&& $scope.empList[i].location1 != null ? $scope.empList[i].location1
												: "").toLowerCase().includes(
												$scope.secondSearchText
														.toLowerCase()))
											$scope.secondSearchORDataArray
													.push($scope.empList[i]);

									} else if ($scope.secondMultiSearchDropDown == "secondaryLocation") {
										if ($scope.secondSearchText == ""
												|| $scope.secondSearchText == undefined) {
											$scope.secondSearchORDataArray
													.push($scope.empList[i]);
										} else if (($scope.empList[i].location2 != undefined
												&& $scope.empList[i].location2 != null ? $scope.empList[i].location2
												: "").toLowerCase().includes(
												$scope.secondSearchText
														.toLowerCase()))
											$scope.secondSearchORDataArray
													.push($scope.empList[i]);

									} else if ($scope.secondMultiSearchDropDown == "phoneNo") {
										if ($scope.secondSearchText == ""
												|| $scope.secondSearchText == undefined) {
											$scope.secondSearchORDataArray
													.push($scope.empList[i]);
										} else if (($scope.empList[i].phoneNumber != undefined
												&& $scope.empList[i].phoneNumber != null ? $scope.empList[i].phoneNumber
												: "").toLowerCase().includes(
												$scope.secondSearchText
														.toLowerCase()))
											$scope.secondSearchORDataArray
													.push($scope.empList[i]);

									} else if ($scope.secondMultiSearchDropDown == "Shift") {
										if ($scope.secondSearchText == ""
												|| $scope.secondSearchText == undefined) {
											$scope.secondSearchORDataArray
													.push($scope.empList[i]);
										} else if (($scope.empList[i].shiftDetails.shiftName != undefined
												&& $scope.empList[i].shiftDetails.shiftName != null ? $scope.empList[i].shiftDetails.shiftName
												: "").toLowerCase().includes(
												$scope.secondSearchText
														.toLowerCase()))
											$scope.secondSearchORDataArray
													.push($scope.empList[i]);

									} else if ($scope.secondMultiSearchDropDown == "Status") {
										if ($scope.secondSearchText == ""
												|| $scope.secondSearchText == undefined) {
											$scope.secondSearchORDataArray
													.push($scope.empList[i]);
										} else if (($scope.empList[i].status != undefined
												&& $scope.empList[i].status != null ? $scope.empList[i].status
												: "").toLowerCase().includes(
												$scope.secondSearchText
														.toLowerCase()))
											$scope.secondSearchORDataArray
													.push($scope.empList[i]);

									}

								}
							}
							// to concat first and second search arrays
							for (var i = 0; i < $scope.firstSearchANDDataArray.length; i++) {
								if (!$scope.secondSearchORDataArray
										.includes($scope.firstSearchANDDataArray[i]))
									$scope.secondSearchORDataArray
											.push($scope.firstSearchANDDataArray[i]);
							}
							$scope.employeeFilterList = $scope.secondSearchORDataArray;
							// $scope.employeeFilterList =
							// ($scope.firstSearchANDDataArray).concat($scope.secondSearchORDataArray);

							// for pagination
							$scope.totalItems = $scope.employeeFilterList.length;
							$scope.currentPage = 1;
							$scope.getTotalPages();

							$scope.getAssignedTagIds($scope.employeeFilterList);
							if ($scope.empList.length == 0
									|| $scope.employeeFilterList.length == 0) {
								$scope.empNoData = true;

							} else {
								$scope.empNoData = false;

								if ($scope.employeeFilterList.length > $scope.numPerPage) {
									$scope.empViewData = JSON
											.parse(JSON
													.stringify($scope.employeeFilterList
															.slice(
																	0,
																	$scope.numPerPage)));
									$scope.PaginationTab = true;
								} else {
									$scope.empViewData = JSON
											.parse(JSON
													.stringify($scope.employeeFilterList));
									$scope.PaginationTab = false;
								}
							}
						}

					}
					
					// Multiple Employee validation  in uploaded file 
					$scope.multipleEmployeeValidation  = function() {
						$scope.errorArray = [];
						var cCode ="";
						for (var i = 0; i < $scope.jsonMultipleEmployees.length; i++) {
							var recordCount = i + 1;
							if ($scope.jsonMultipleEmployees[i].empId.length == 0) {
								$scope.errorArray
										.push({

											'recordcount' : recordCount,
											'errorField' : "Employee ID : "
													+ " " + "***",
											'errorDescription' : "Employee ID should not be empty. upto 12 characters allowed"
										});

							}
							if (!nameRegex.test($scope.jsonMultipleEmployees[i].empId)) {
								$scope.errorArray
										.push({

											'recordcount' : recordCount,
											'errorField' : "Employee ID : "
													+ " "
													+ $scope.jsonMultipleEmployees[i].empId,
											'errorDescription' : "Only Only alpha numeric characters are allowed "
										});

							}
							if ($scope.jsonMultipleEmployees[i].empId.length > 12) {
								$scope.errorArray
										.push({

											'recordcount' : recordCount,
											'errorField' : "Employee ID : "
													+ " "
													+ $scope.jsonMultipleEmployees[i].empId,
											'errorDescription' : "Exceeds limit. upto 12 characters allowed"
										});

							}
							if ($scope.jsonMultipleEmployees[i].firstName.length == 0) {
								$scope.errorArray
										.push({

											'recordcount' : recordCount,
											'errorField' : "First Name : "
													+ " " + "***",
											'errorDescription' : "First Name should not be empty. upto 16 characters allowed"
										});

							}
							if (!nameRegex.test($scope.jsonMultipleEmployees[i].firstName)) {
								$scope.errorArray
										.push({

											'recordcount' : recordCount,
											'errorField' : "First Name  : "
													+ " "
													+ $scope.jsonMultipleEmployees[i].firstName,
											'errorDescription' : "Only alpha numeric characters are allowed "
										});

							}
							if ($scope.jsonMultipleEmployees[i].firstName.length > 16) {
								$scope.errorArray
										.push({

											'recordcount' : recordCount,
											'errorField' : "First Name : "
													+ " "
													+ $scope.jsonMultipleEmployees[i].firstName,
											'errorDescription' : "Exceeds limit. upto 16 characters allowed"
										});

							}
							if ($scope.jsonMultipleEmployees[i].lastName.length == 0) {
								$scope.errorArray
										.push({

											'recordcount' : recordCount,
											'errorField' : "Last Name : "
													+ " " + "***",
											'errorDescription' : "Last Name should not be empty. upto 16 characters allowed"
										});

							}
							if (!nameRegex.test($scope.jsonMultipleEmployees[i].lastName)) {
								$scope.errorArray
										.push({

											'recordcount' : recordCount,
											'errorField' : "Last Name  : "
													+ " "
													+ $scope.jsonMultipleEmployees[i].lastName,
											'errorDescription' : "Only alpha numeric characters are allowed "
										});

							}
							if ($scope.jsonMultipleEmployees[i].lastName.length > 16) {
								$scope.errorArray
										.push({

											'recordcount' : recordCount,
											'errorField' : "Last Name : "
													+ " "
													+ $scope.jsonMultipleEmployees[i].lastName,
											'errorDescription' : "Exceeds limit. upto 16 characters allowed"
										});

							}
							if ($scope.jsonMultipleEmployees[i].lastName.length > 16) {
								$scope.errorArray
										.push({

											'recordcount' : recordCount,
											'errorField' : "Last Name : "
													+ " "
													+ $scope.jsonMultipleEmployees[i].lastName,
											'errorDescription' : "Exceeds limit. upto 16 characters allowed"
										});

							}
							if ($scope.jsonMultipleEmployees[i].department.length == 0) {
								$scope.errorArray
										.push({

											'recordcount' : recordCount,
											'errorField' : "Department : "
													+ " " + "***",
											'errorDescription' : "Department should not be empty. upto 30 characters allowed"
										});

							}
							if (!inputfieldRegex.test($scope.jsonMultipleEmployees[i].department)) {
								$scope.errorArray
										.push({

											'recordcount' : recordCount,
											'errorField' : "Department : "
													+ " "
													+ $scope.jsonMultipleEmployees[i].department,
											'errorDescription' : "Only Printable characters are allowed "
										});

							}
							
							if ($scope.jsonMultipleEmployees[i].location1.length == 0) {
								$scope.errorArray
										.push({

											'recordcount' : recordCount,
											'errorField' : "Primary Location : "
													+ " " + "***",
											'errorDescription' : "Primary Location should not be empty. upto 40 characters allowed"
										});

							}
							if (!inputfieldRegex.test($scope.jsonMultipleEmployees[i].location1)) {
								$scope.errorArray
										.push({

											'recordcount' : recordCount,
											'errorField' : "Primary location  : "
													+ " "
													+ $scope.jsonMultipleEmployees[i].location1,
											'errorDescription' : "Only Printable characters are allowed "
										});

							}
							if ($scope.jsonMultipleEmployees[i].location1.length > 40) {
								$scope.errorArray
										.push({

											'recordcount' : recordCount,
											'errorField' : "Primary location : "
													+ " "
													+ $scope.jsonMultipleEmployees[i].location1,
											'errorDescription' : "Exceeds limit. upto 40 characters allowed"
										});

							}
							if ($scope.jsonMultipleEmployees[i].location2.length == 0) {
								$scope.errorArray
										.push({

											'recordcount' : recordCount,
											'errorField' : "Secondary Location : "
													+ " " + "***",
											'errorDescription' : "Secondary Location should not be empty. upto 40 characters allowed"
										});

							}
							if (!inputfieldRegex.test($scope.jsonMultipleEmployees[i].location2)) {
								$scope.errorArray
										.push({

											'recordcount' : recordCount,
											'errorField' : "Secondary Location  : "
													+ " "
													+ $scope.jsonMultipleEmployees[i].location2,
											'errorDescription' : "Only Printable characters are allowed "
										});

							}
							if ($scope.jsonMultipleEmployees[i].location2.length > 40) {
								$scope.errorArray
										.push({

											'recordcount' : recordCount,
											'errorField' : "Secondary Location : "
													+ " "
													+ $scope.jsonMultipleEmployees[i].location2,
											'errorDescription' : "Exceeds limit. upto 40 characters allowed"
										});

							}
							if ($scope.jsonMultipleEmployees[i].location3.length == 0) {
								$scope.errorArray
										.push({

											'recordcount' : recordCount,
											'errorField' : "Tertiary Location : "
													+ " " + "***",
											'errorDescription' : "Tertiary Location should not be empty. upto 40 characters allowed"
										});

							}
							if (!inputfieldRegex.test($scope.jsonMultipleEmployees[i].location3)) {
								$scope.errorArray
										.push({

											'recordcount' : recordCount,
											'errorField' : "Tertiary Location  : "
													+ " "
													+ $scope.jsonMultipleEmployees[i].location3,
											'errorDescription' : "Only Printable characters are allowed "
										});

							}
							if ($scope.jsonMultipleEmployees[i].location3.length > 40) {
								$scope.errorArray
										.push({

											'recordcount' : recordCount,
											'errorField' : "Tertiary Location : "
													+ " "
													+ $scope.jsonMultipleEmployees[i].location3,
											'errorDescription' : "Exceeds limit. upto 40 characters allowed"
										});

							}
							
							
							
							if($scope.jsonMultipleEmployees[i].phoneNumber.length != 0 && $scope.jsonMultipleEmployees[i].phoneNumber.length != 12 && $scope.jsonMultipleEmployees[i].phoneNumber.length != 13){
								//invalid phone number
								$scope.errorArray
								.push({

								'recordcount' : recordCount,
								'errorField' : "Phone Number : "
								+ " "
								+ $scope.jsonMultipleEmployees[i].phoneNumber,
								'errorDescription' : "Proper country code followed by a valid 10 digit phone number required"
								});
								}
							
							
							if($scope.jsonMultipleEmployees[i].phoneNumber.length != 0 &&($scope.jsonMultipleEmployees[i].phoneNumber.length == 12) || ($scope.jsonMultipleEmployees[i].phoneNumber.length == 13))
							{
							 cCode = $scope.jsonMultipleEmployees[i].phoneNumber.split($scope.jsonMultipleEmployees[i].phoneNumber.slice(-10))[0];
							if((cCode != "+1") && (cCode != "+91")){
							
							$scope.errorArray
							.push({

							'recordcount' : recordCount,
							'errorField' : "Phone Number : "
							+ " "
							+ $scope.jsonMultipleEmployees[i].phoneNumber.length,
							'errorDescription' : "Proper country code followed by a valid 10 digit phone number required"
							});
							
							}
							}

							if (!empPhoneRegex
									.test($scope.jsonMultipleEmployees[i].phoneNumber)) {
								$scope.errorArray
										.push({

											'recordcount' : recordCount,
											'errorField' : "Phone number : "
													+ " "
													+ $scope.jsonMultipleEmployees[i].phoneNumber,
											'errorDescription' : "Only numeric values are allowed without any space, hyphen or bracket"
										});

							}
							if ($scope.jsonMultipleEmployees[i].shiftDetails.length == 0) {
								$scope.errorArray
										.push({

											'recordcount' : recordCount,
											'errorField' : "Shift : "
													+ " " + "***",
											'errorDescription' : "Shift should not be empty. upto 40 characters allowed"
										});

							}
							if (!inputfieldRegex.test($scope.jsonMultipleEmployees[i].shiftDetails)) {
								$scope.errorArray
										.push({

											'recordcount' : recordCount,
											'errorField' : "Shift  : "
													+ " "
													+ $scope.jsonMultipleEmployees[i].shiftDetails,
											'errorDescription' : "Only Printable characters are allowed "
										});

							}
							if ($scope.jsonMultipleEmployees[i].shiftDetails.length > 40) {
								$scope.errorArray
										.push({

											'recordcount' : recordCount,
											'errorField' : "Shift : "
													+ " "
													+ $scope.jsonMultipleEmployees[i].shiftDetails,
											'errorDescription' : "Exceeds limit. upto 40 characters allowed"
										});

							}
							
						}
					};

				});
