app
	.controller(
		'calibrationController',
		function($scope, $http, $rootScope, $window, $location,$timeout) {
			$scope.user = JSON.parse($window.sessionStorage
					.getItem("user"));
			if ($scope.user == null) {
				$location.path('/login');
				return;
			}
			 // Timepicker
  			$scope.add=true;
			$scope.update=false;
	     	$scope.delete=false;
            $scope.calData= new Array();
            $scope.calViewData= new Array();
            $scope.calDataFilterList = new Array();
			$scope.searchText = "";
            $scope.addCal={};

       // Date picker
    $('#reservationdate').datetimepicker({
        format: 'YYYY-MMM-DD'
    });
    $scope.addCal.calibrationExpiryDate = moment(new Date()).format('YYYY-MMM-DD');
    $scope.searchOption = function() {
		$scope.filterOption();
	}
    
    $scope.filterOption = function() {

		$scope.calViewData = [];
		$scope.calDataFilterList = [];

		for (var i = 0; i < $scope.calData.length; i++) {
			
			if ($scope.searchText == '') {
				$scope.calDataFilterList
						.push($scope.calData[i]);
			} else {
				if ($scope.calData[i].calibrationId.toLowerCase().includes($scope.searchText.toLowerCase())
				|| $scope.calData[i].asset.toLowerCase().includes($scope.searchText.toLowerCase())
				|| $scope.calData[i].calibrationExpiryDate.toLowerCase().includes($scope.searchText.toLowerCase()) ) {
					
					$scope.calDataFilterList.push($scope.calData[i]);

				}

			}
			
		}

		$scope.totalItems = $scope.calDataFilterList.length;
		$scope.currentPage = 1;
		$scope.getTotalPages();

		if ($scope.calData.length == 0
				|| $scope.calDataFilterList.length == 0) {
			$scope.calibrationNoData = true;

		} else {
			
			$scope.calibrationNoData = false;

	if ($scope.calDataFilterList.length > $scope.numPerPage){
		$scope.calViewData = $scope.calDataFilterList
				.slice(0, $scope.numPerPage);
	$scope.cal_tabNavi=true; // pagination tab
	}
	else {
		$scope.calViewData = $scope.calDataFilterList;
		$scope.cal_tabNavi=false; // pagination tab
	}

		
			
		}
	}
 // input field validation 
	
	$("#calibrationId, #calasset").keypress(
			function(e) {
				// if the letter is not alphabets then display error
				// and don't type anything
				$return = ((e.which > 64) && (e.which < 91)) || ((e.which> 96) && (e.which < 123)) ||(e.which == 8) || (e.which == 32)  || (e.which >= 48 && e.which <= 57)
				if (!$return)
									
				{
					
					return false;
				}
			});	
	
    

			$scope.getCalibrationData = function() {
									
				if (!$rootScope.checkNetconnection()) { 
				 	$rootScope.alertDialogChecknet();
					return; }
						 $rootScope.showloading('#loadingBar');
						$http({
							method : 'GET',
							url : './calibrationRecord',
							
						})
								.then(
										function(response) {
											$rootScope.hideloading('#loadingBar');
											if (response != null
													&& response.data.data != null
													&& response.data.datalength != 0
													&& response.data.data != "BAD_REQUEST"
													&& response.data.data != undefined
													&& response.data.data.length != 0) {
												

												$scope.calData.length = 0;

												for (var i = 0; i < response.data.data.length; i++) {
													if (response.data.data[i].active == true) {
														$scope.calData
																.push(response.data.data[i]);
													}
												}
												if ($scope.calData.length == 0) {
													$scope.calibrationNoData = true;
													return;
												} else {
													$scope.calibrationNoData = false;
												}
												if($scope.calData.length > 10){
													$scope.cal_tabNavi=true; // pagination tab
												}else{
													$scope.cal_tabNavi=false; // pagination tab
												}
												$scope.totalItems=$scope.calData.length;
												$scope.currentPage=1;
												
												$scope.getTotalPages();
							
									if ($scope.calData.length > $scope.numPerPage)
										$scope.calViewData =$scope.calData
										.slice(0, $scope.numPerPage);
										else
									$scope.calViewData = $scope.calData;
												

											} else{
												
												$scope.calibrationNoData = true;
												
											}
										},function(error) {
											$rootScope.hideloading('#loadingBar');
											$rootScope.fnHttpError(error);
										});
					}
                
				  $scope.getCalibrationData();
		
				var formData = new FormData();

					$scope.getTheFiles = function(element) {
						$scope.loading = true;
						$scope.$apply(function($scope) {
								
							for (var i = 0; i < element.files.length; i++) {
								

                                  fileName = element.files[0].name;
                                  var ext = fileName.substring(fileName.lastIndexOf('.') + 1);  
                                
									  $scope.error= false;
                                      formData.append('file', element.files[i]);
          							

 							
                                 } 
 						});
					}
		
			      $scope.addCalibration = function() {
			    		var calibrationId = document.getElementById("calibrationId");
						var calasset = document.getElementById("calasset");
						var calibrationExpiryDate = document.getElementById("calibrationExpiryDate");
						var upfile = document.getElementById("upfile");

						if (calibrationId.value.length == 0) {
							$scope.calIDError = true;
							$scope.calID_error_msg = "Please enter the Id";
							$timeout(function() {
								$scope.calIDError = false;
							}, 2000);
							return;
						}else if (calasset.value.length == 0) {
							$scope.calassetError = true;
							$scope.calasset_error_msg = "Please enter the asset";
							$timeout(function() {
								$scope.calassetError = false;
							}, 2000);
							return;
						}else if (calibrationExpiryDate.value.length == 0) {
							$scope.exDateError = true;
							$scope.exDate_error_msg = "Please select date";
							$timeout(function() {
								$scope.exDateError = false;
							}, 2000);
							return;
						}
						

						
						/*else if (upfile.value.length == 0) {
							$scope.upfileError = true;
							$scope.upfile_error_msg = "Please select file";
							$timeout(function() {
								$scope.upfileError = false;
							}, 2000);
							return;
						}*/


						 /*
							 * var fileVal = $('#upfile').val(); if(fileVal=='') {
							 * $scope.error= true;
							 * $scope.alert_error_msg="Please select a file";
							 * 
							 * $timeout( function() { $scope.error= false },
							 * 5000); return; }
							 */


						if ($scope.addCalForm.$valid) {
							
							
							formData.append('calibrationId', $scope.addCal.calibrationId);
							formData.append('asset',$scope.addCal.asset);
							formData.append('calibrationExpiryDate', $scope.addCal.calibrationExpiryDate);
							if (!$rootScope.checkNetconnection()) { 
								$rootScope.alertDialogChecknet();
								return;
								}
                                                     
							$rootScope.showloading('#loadingBar');
			
							$http({
								method : 'POST',
								url : './calibrationRecord',
								data : formData,
								headers: {
								'Content-Type':undefined
								}
								
								
							})
									.then(
											function(response) {
												if (response != null
														&& response.data != null
														&& response.data != "BAD_REQUEST") {
													$rootScope.hideloading('#loadingBar');
													$scope.cal = response.data;
													$rootScope.showAlertDialog(response.data.message);
													formData.delete("file");
													formData.delete("calibrationId");
													formData.delete("asset");
													formData.delete("calibrationExpiryDate");

													$timeout(
															function() {
																 $scope.getCalibrationData();
																 $scope.cancelCalibration();
															}, 2000);
												} else {
													$rootScope.hideloading('#loadingBar');
													$rootScope.showAlertDialog(response.data.message);
													$timeout(function() {
														$scope.cancelCalibration();
													}, 2000);
												}
											},function(error) {
												$rootScope.hideloading('#loadingBar');
												$rootScope.fnHttpError(error);
											});
						}

				
					}
					
					 
                 $scope.rowHighilited = function(row) {
	
	                    $scope.selectedRow = row;
	
		                $scope.addCal.calibrationId = $scope.calViewData[row].calibrationId;
						$scope.addCal.asset = $scope.calViewData[row].asset;
						$scope.addCal.calibrationExpiryDate = $scope.calViewData[row].calibrationExpiryDate;
						
						$scope.calID=true;
						$scope.update=true;
						$scope.delete=true;
						$scope.add=false;
						
						}
				 $scope.updateCalibration = function() {
					 
					 var calibrationId = document.getElementById("calibrationId");
						var calasset = document.getElementById("calasset");
						var calibrationExpiryDate = document.getElementById("calibrationExpiryDate");
						var upfile = document.getElementById("upfile");

						if (calibrationId.value.length == 0) {
							$scope.calIDError = true;
							$scope.calID_error_msg = "Please enter the Id";
							$timeout(function() {
								$scope.calIDError = false;
							}, 2000);
							return;
						}else if (calasset.value.length == 0) {
							$scope.calassetError = true;
							$scope.calasset_error_msg = "Please enter the asset";
							$timeout(function() {
								$scope.calassetError = false;
							}, 2000);
							return;
						}else if (calibrationExpiryDate.value.length == 0) {
							$scope.exDateError = true;
							$scope.exDate_error_msg = "Please select date";
							$timeout(function() {
								$scope.exDateError = false;
							}, 2000);
							return;
						}
						/* while adding cal record file is optional
						 else if (upfile.value.length == 0) {
							$scope.upfileError = true;
							$scope.upfile_error_msg = "Please select file";
							$timeout(function() {
								$scope.upfileError = false;
							}, 2000);
							return;
						}*/
						if (!$rootScope.checkNetconnection()) { 
						 	$rootScope.alertDialogChecknet();
							return; }
						if ($scope.addCalForm.$valid) {
							
							formData.append('calibrationId', $scope.addCal.calibrationId);
							formData.append('asset',$scope.addCal.asset);
							formData.append('calibrationExpiryDate', $scope.addCal.calibrationExpiryDate);
							if (!$rootScope.checkNetconnection()) { 
								$rootScope.alertDialogChecknet();
								return;
								}
							$rootScope.showloading('#loadingBar');
			
							$http({
								method : 'PUT',
								url : './calibrationRecord',
								data : formData,
								headers: {
								'Content-Type':undefined
								}
								
							})
									.then(
											function(response) {
												if (response != null
														&& response.data != null
														&& response.data != "BAD_REQUEST") {
													$rootScope.hideloading('#loadingBar');
													$scope.cal = response.data;
													$rootScope.showAlertDialog(response.data.message);
													formData.delete("file");
													formData.delete("calibrationId");
													formData.delete("asset");
													formData.delete("calibrationExpiryDate");
													$timeout(
															function() {
																 $scope.getCalibrationData();
																 $scope.cancelCalibration();

															}, 2000);
												} else {
													$rootScope.hideloading('#loadingBar');
													$rootScope.showAlertDialog(response.data.message);
													$timeout(function() {
														$scope.cancelCalibration();
													}, 2000);
												}
											},function(error) {
												$rootScope.hideloading('#loadingBar');
												$rootScope.fnHttpError(error);
											});
						}

				
					}
									
				 $scope.deleteCalibration = function() {	
					 if (!$rootScope.checkNetconnection()) { 
						 	$rootScope.alertDialogChecknet();
							return; }

						if ($scope.addCalForm.$valid) {
								$.confirm({
														title : $rootScope.MSGBOX_TITLE,
														content : 'Are you sure you want to delete this item?',
														type: 'blue',
														columnClass:'medium',
														autoClose : false,
                         							    icon: 'fa fa-info-circle',
													    buttons: {
														yes: {
														text: 'Yes', 
														btnClass: 'btn-blue',
														action : function() {

															 btnClass: 'btn-blue',
															
															 //var calId = $scope.addCal.calibrationId
														     data = {
															 "calibrationId" : $scope.addCal.calibrationId 
														    }
															$rootScope.showloading('#loadingBar');
															$http({
																method : 'DELETE',
																url : './calibrationRecord',
																headers: {
																'Content-Type':'application/json'
																},
																data : data,
																
															})
																	.then(
																			function(response) {
																				if (response != null
																						&& response.data != null
																						&& response.data != "BAD_REQUEST") {
																					$rootScope.hideloading('#loadingBar');
																					$scope.cal = response.data;
																					$rootScope.showAlertDialog(response.data.message);
																					$timeout(
																							function() {
																								 $scope.getCalibrationData();
																						        $scope.cancelCalibration();
																							}, 2000);
																				} else {
																					$rootScope.hideloading('#loadingBar');
																					$rootScope.showAlertDialog(response.data.message);
																					$timeout(function() {
																						$scope.cancelCalibration();
																					}, 2000);
																				}
																			},function(error) {
																				$rootScope.hideloading('#loadingBar');
																				$rootScope.fnHttpError(error);
																			});
																			}
																						},
														no : function() {
														  },
														}
														
													});
				
						}

				
					}
				 $scope.cancelCalibration = function() {	
	                    $scope.addCal={};
						$scope.calID=false;
						$scope.update=false;
						$scope.delete=false;
						$scope.add=true;
					 $scope.addCal.calibrationExpiryDate = moment(new Date()).format('YYYY-MMM-DD');
					 $scope.getTotalPages();
					}
// pagination button
		
		$scope.currentPage = 1;
		$scope.numPerPage = 10;
		$scope.maxSize = 3;
		$scope.totalItems = 0;
		 $scope.calViewData=[];
		 $scope.calDataFilterList=[];
		 
		$scope
				.$watch(
						'currentPage + numPerPage',
						function() {
							var begin = (($scope.currentPage - 1) * $scope.numPerPage), end = begin
									+ $scope.numPerPage;
							if ($scope.searchText == '') 
								$scope.calViewData = $scope.calData
										.slice(begin, end);
							else
								$scope.calViewData = $scope.calDataFilterList
								.slice(begin, end);

						});

		$scope.currentPage = 1;

		$scope.maxSize = 3;

		$scope.numPerPage = 10;
		$scope.totalItems = 0;
		$scope.totalPages = 0;

		$scope.hasNext = true;
		$scope.hasPrevious = true;
		$scope.isFirstPage = true;
		$scope.isLastPage = true;
		$scope.serial = 1;

		$scope
				.$watch(
						"currentPage",
						function(newVal, oldVal) {
							if (newVal !== oldVal) {

								if (newVal > $scope.totalPages) {
									$scope.currentPage = $scope.totalPages;
								} else {
									$scope.currentPage = newVal;
								}

								$scope.serial = (($scope.currentPage - 1) * $scope.numPerPage) + 1;

							}
						});

		$scope.enablePageButtons = function() {
			$scope.serial = (($scope.currentPage - 1) * $scope.numPerPage) + 1;
			$scope.selectedRow = -1;
			if ($scope.totalPages <= 1) {
				$scope.hasPrevious = true;
				$scope.isFirstPage = true;
				$scope.hasNext = true;
				$scope.isLastPage = true;

			} else if ($scope.totalPages > 1
					&& $scope.currentPage == $scope.totalPages) {

				$scope.hasPrevious = false;
				$scope.isFirstPage = false;
				$scope.hasNext = true;
				$scope.isLastPage = true;
			} else if ($scope.currentPage != 1
					&& $scope.currentPage < $scope.totalPages) {

				$scope.hasPrevious = false;
				$scope.isFirstPage = false;
				$scope.hasNext = false;
				$scope.isLastPage = false;

			} else {
				$scope.hasPrevious = true;
				$scope.isFirstPage = true;
				$scope.hasNext = false;
				$scope.isLastPage = false;
			}
		};

		$scope.getTotalPages = function() {

			if (($scope.totalItems % $scope.numPerPage) > 0)
				$scope.totalPages = parseInt($scope.totalItems
						/ $scope.numPerPage) + 1;
			else
				$scope.totalPages = parseInt($scope.totalItems
						/ $scope.numPerPage);

			$scope.enablePageButtons();

		};

		$scope.goFirstPage = function() {
			$scope.currentPage = 1;
			$scope.enablePageButtons();
		};

		$scope.goPreviousPage = function() {
			if ($scope.currentPage > 1)
				$scope.currentPage = $scope.currentPage - 1;
			$scope.enablePageButtons();
		};

		$scope.goNextPage = function() {
			if ($scope.currentPage <= $scope.totalPages)
				$scope.currentPage = $scope.currentPage + 1;
			$scope.enablePageButtons();
		};

		$scope.goLastPage = function() {
			$scope.currentPage = $scope.totalPages;
			$scope.enablePageButtons();
		};				
										
					
		});
