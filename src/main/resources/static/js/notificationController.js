app
		.controller(
				'notificationController',
				function($scope, $http, $window, $rootScope, $location,
						$timeout) {
					$scope.user = JSON.parse($window.sessionStorage
							.getItem("user"));
					if ($scope.user == null) {
						$location.path('/login');
						return;
					}
					$scope.readNotificationCount = "0";

					$scope.loadata = true;
					$scope.searchButton = true;
					$scope.notificationViewData = [];
					$scope.notificationViewDataDummy = [];
					$scope.notificationData = [];
					$scope.notificationFilterList = [];
					$scope.notificationModalViewData = [];
					$scope.isFirstTime = '0';

					// View all notification table API call
					$scope.getNotificationAPIData = function() {

						if (!$rootScope.checkNetconnection()) { 				
						 	$rootScope.alertDialogChecknet();
							return; 
							}
						$rootScope.showloading('#loadingBar');
						$http(
								{
									method : 'POST',
									url : './notifications',
									data : {
										"emailId" : $scope.user.emailID,
										"notificationId" : $scope.readNotificationCount,
										"noOfRecords" : "0",
										"isFirstTime" : $scope.isFirstTime
									}

								})
								.then(
										
										function(response) {
											$rootScope.hideloading('#loadingBar');		
											if (response != null
													&& response.data != null
													&& response.data.length != 0
													&& response.data != "BAD_REQUEST"
													&& response.data.data != undefined
													&& response.data.data.length != 0) {

												$scope.notificationData.length = 0;
												$scope.loadata = true;
												$scope.isFirstTime = '1';
												let minNotificationId = response.data.data[0].id;
												for (var i = 0; i < response.data.data.length; i++) {
													// if($rootScope.notificationType
													// ==
													// response.data.data[i].type
													// ){
													$scope.notificationData
															.push(response.data.data[i]);

													if (response.data.data[i].id < minNotificationId) {
														minNotificationId = response.data.data[i].id;
													}
													// }

												}
												$scope.notificationViewData = $scope.notificationViewData
														.concat($scope.notificationData);
												$scope.notificationViewDataDummy = $scope.notificationViewDataDummy
														.concat($scope.notificationData);

												$scope.readNotificationCount = minNotificationId;

											} else {
												$rootScope
														.alertDialog('ERROR : No Data Available');
											}
										},
										function(error) {
											$rootScope.hideloading('#loadingBar');		
											$rootScope.fnHttpError(error);

										});
					}

					$scope.getNotificationAPIData();

					$scope.searchOption = function() {
						$scope.filterOption();
					}

					$scope.filterOption = function() {

						$scope.notificationViewData=[];						// $scope.notificationFilterList.length = 0;
						for (var i = 0; i < $scope.notificationViewDataDummy.length; i++) {
							if ($scope.searchText == '') {
								$scope.notificationViewData
										.push($scope.notificationViewDataDummy[i]);
							} else {
								if ((($scope.notificationViewDataDummy[i].id)
										.toString() != undefined
										&& ($scope.notificationViewDataDummy[i].id)
												.toString() != null ? ($scope.notificationViewDataDummy[i].id)
										.toString()
										: "").toLowerCase().includes(
										$scope.searchText.toLowerCase())
										|| ($scope.notificationViewDataDummy[i].type != undefined
												&& $scope.notificationViewDataDummy[i].type != null ? $scope.notificationViewDataDummy[i].type
												: "").toLowerCase()
												.includes(
														$scope.searchText
																.toLowerCase())
										|| ($scope.notificationViewDataDummy[i].message != undefined
												&& $scope.notificationViewDataDummy[i].message != null ? $scope.notificationViewDataDummy[i].message
												: "").toLowerCase()
												.includes(
														$scope.searchText
																.toLowerCase())
										|| ($scope.notificationViewDataDummy[i].notifyTime != undefined
												&& $scope.notificationViewDataDummy[i].notifyTime != null ? $scope.notificationViewDataDummy[i].notifyTime
												: "").toLowerCase()
												.includes(
														$scope.searchText
																.toLowerCase())) {
									$scope.notificationViewData
											.push($scope.notificationViewDataDummy[i]);

								}
							}
							// }
						}

						if ($scope.notificationViewData.length == 0) {
							$scope.notificationNoData = true;
						} else {
							$scope.notificationNoData = false;
						}
					}

					$scope.refresh = function() {
						$scope.notificationViewData.length = 0;
						$scope.notificationData.length = 0;
						$scope.notificationNoData = false;
						$scope.searchText = ''
						$scope.loadata = true;

						$scope.readNotificationCount = "0";
						$scope.getNotificationAPIData();

					};

					$scope.scrollNotification = function() {
						if ($scope.notificationViewData.length == 0) //dont scroll if data is not available
							return;
						
						var table = document
								.querySelector('#idNotificationTable');
						var line = 0;
						var rows = table
								.querySelectorAll('#idNotificationTableRow');
						rows[line].scrollIntoView({
							behavior : 'smooth',
							block : 'center'
						});
					};

					$scope.loadMore = function getData(tableState) {
						if ($scope.loadata) {
							$rootScope.showloading('#loadingBar');
							$timeout(function() {

								// if we reset (like after a search or an order)
								if (tableState.pagination.start != 0) {
									// we load more
									$scope.getNotificationAPIData();

								}
							}, 1000);
						}
					};

					$scope.rowHighilited = function(row) {
						$('#notificationTableView').empty();
						$scope.notificationTableHeaders = [];
						$scope.dataObjKeys = [];
						$scope.zoneNameObjKeys = [];

						$scope.notificationModalViewData.length = 0;
						$scope.notificationID = "";
						$scope.notificationType = "";
						$scope.notificationSource = "";
						$scope.notificationMessage = "";
						$scope.notificationMessageObj = "";
						$scope.notificationNotifyTime = "";

						for (var i = 0; i < $scope.notificationViewData.length; i++) {
							if (row == i)
								$scope.notificationModalViewData
										.push($scope.notificationViewData[i]);
						}
						$scope.notificationID = $scope.notificationModalViewData[0].id;
						$scope.notificationType = $scope.notificationModalViewData[0].type;
						$scope.notificationSource = $scope.notificationModalViewData[0].source;
						$scope.notificationMessage = $scope.notificationModalViewData[0].message;
						$scope.notificationMessageObj = $scope.notificationModalViewData[0].messageObj;
						$scope.notificationNotifyTime = $scope
								.localizeDateStr($scope.notificationModalViewData[0].notifyTime);

						if ($scope.notificationMessageObj.type == "crowdedZone"
								|| $scope.notificationMessageObj.type == "tags"
								|| $scope.notificationMessageObj.type == "locator") {
							$scope.dataObjKeys = [];
							var dataObj = $scope.notificationMessageObj.data[0]
							$scope.dataObjKeys = Object.keys(dataObj);
							for (var k = 0; k < $scope.dataObjKeys.length; k++) {
								$scope.notificationTableHeaders
										.push($scope.dataObjKeys[k]);
							}

						} else if ($scope.notificationMessageObj.type == "lateTag"
								|| $scope.notificationMessageObj.type == "restrictedZoneVisitor") {
							$scope.dataObjKeys = [];

							var zoneNameObj = $scope.notificationMessageObj.tagData;
							$scope.zoneNameObjKeys = Object.keys(zoneNameObj);

							var x = $scope.zoneNameObjKeys[0];
							var dataObj = $scope.notificationMessageObj.tagData[x][0];

							$scope.dataObjKeys = Object.keys(dataObj);
							$scope.dataObjKeys.push('Zone Name');
							for (var k = 0; k < $scope.dataObjKeys.length; k++) {
								$scope.notificationTableHeaders
										.push($scope.dataObjKeys[k]);
							}

						} else if ($scope.notificationMessageObj.type == "lateAsset") {

							$scope.zoneNameObjKeys = [ 'Chrono', 'NonChrono' ];
							var x = $scope.zoneNameObjKeys[0];
							var dataObj = $scope.notificationMessageObj.tagData[x][0];

							$scope.dataObjKeys = Object.keys(dataObj);
							$scope.dataObjKeys.push('Asset Profile');
							for (var k = 0; k < $scope.dataObjKeys.length; k++) {
								$scope.notificationTableHeaders
										.push($scope.dataObjKeys[k]);
							}

						}
						// to change the header-names
						/*
						 * $scope.notificationTableHeaders =
						 * JSON.parse(JSON.stringify($scope.notificationTableHeaders).split('"name"').join('"Name"'));
						 * $scope.notificationTableHeaders =
						 * JSON.parse(JSON.stringify($scope.notificationTableHeaders).split('"id"').join('"Id"'));
						 * $scope.notificationTableHeaders =
						 * JSON.parse(JSON.stringify($scope.notificationTableHeaders).split('"status"').join('"Status"'));
						 * $scope.notificationTableHeaders =
						 * JSON.parse(JSON.stringify($scope.notificationTableHeaders).split('"time"').join('"Time"'));
						 * $scope.notificationTableHeaders =
						 * JSON.parse(JSON.stringify($scope.notificationTableHeaders).split('"tagName"').join('"Tag
						 * Name"')); $scope.notificationTableHeaders =
						 * JSON.parse(JSON.stringify($scope.notificationTableHeaders).split('"zoneName"').join('"Zone
						 * Name"')); $scope.notificationTableHeaders =
						 * JSON.parse(JSON.stringify($scope.notificationTableHeaders).split('"tagStatus"').join('"Tag
						 * Status"')); $scope.notificationTableHeaders =
						 * JSON.parse(JSON.stringify($scope.notificationTableHeaders).split('"percentage"').join('"Percentage"'));
						 * $scope.notificationTableHeaders =
						 * JSON.parse(JSON.stringify($scope.notificationTableHeaders).split('"assignedId"').join('Assigned
						 * Id')); $scope.notificationTableHeaders =
						 * JSON.parse(JSON.stringify($scope.notificationTableHeaders).split('"uid"').join('"UId"'));
						 * $scope.notificationTableHeaders =
						 * JSON.parse(JSON.stringify($scope.notificationTableHeaders).split('"type"').join('"Type"'));
						 * $scope.notificationTableHeaders =
						 * JSON.parse(JSON.stringify($scope.notificationTableHeaders).split('"macId"').join('"MacId"'));
						 */
						$scope.notificationTableHeadersName = {
							"name" : "Name",
							"id" : "ID",
							"status" : "Status",
							"time" : "Time",
							"tagName" : "Tag Name",
							"zoneName" : "Zone Name",
							"tagStatus" : "Tag Status",
							"percentage" : "Percentage",
							"assignedId" : "Assigned ID",
							"notificationCount" : "Count",
							"uid" : "User ID",
							"type" : "Type",
							"macId" : "MAC ID"
						};

						$('#idNotificationmodal').modal('show');

						// to create a table view for message details
						var notificationTable = document.createElement('table');
						notificationTable.setAttribute('id',
								'notificationTable'); // table id.
						notificationTable.className = "tableWidth";
						notificationTable.className = "table table-bordered";

						var tr = notificationTable.insertRow(-1);
						for (var h = 0; h < $scope.notificationTableHeaders.length; h++) {
							var th = document.createElement('th');
							th.className = "thcss";
							th.innerHTML = $scope.notificationTableHeadersName[$scope.notificationTableHeaders[h]] == undefined ? $scope.notificationTableHeaders[h]
									: $scope.notificationTableHeadersName[$scope.notificationTableHeaders[h]];
							tr.appendChild(th);
						}

						var div = document
								.getElementById('notificationTableView');
						div.appendChild(notificationTable); // add the TABLE to
						// the container.

						if ($scope.notificationMessageObj.type == "crowdedZone"
								|| $scope.notificationMessageObj.type == "tags"
								|| $scope.notificationMessageObj.type == "locator") {
							for (var k = 0; k < $scope.notificationMessageObj.data.length; k++) {
								$scope
										.addnotificationTableRow($scope.notificationMessageObj.data[k]);
							}
						} else if ($scope.notificationMessageObj.type == "lateTag"
								|| $scope.notificationMessageObj.type == "restrictedZoneVisitor") {
							for (var k = 0; k < $scope.zoneNameObjKeys.length; k++) {
								var x = $scope.zoneNameObjKeys[k]
								for (var j = 0; j < $scope.notificationMessageObj.tagData[x].length; j++) {
									$scope.notificationMessageObj.tagData[x][j]['Zone Name'] = x;
									$scope
											.addnotificationTableRow($scope.notificationMessageObj.tagData[x][j]);

								}
							}
						} else if ($scope.notificationMessageObj.type == "lateAsset") {
							for (var k = 0; k < $scope.zoneNameObjKeys.length; k++) {
								var x = $scope.zoneNameObjKeys[k]
								for (var j = 0; j < $scope.notificationMessageObj.tagData[x].length; j++) {
									$scope.notificationMessageObj.tagData[x][j]['Asset Profile'] = x;
									$scope
											.addnotificationTableRow($scope.notificationMessageObj.tagData[x][j]);

								}
							}
						}

					}
					// now, add a new to the TABLE.
					$scope.addnotificationTableRow = function(listItem) {

						var notificationTab = document
								.getElementById('notificationTable');

						var rowCnt = notificationTab.rows.length; // table row
						var tr = notificationTab.insertRow(rowCnt);
						tr = notificationTab.insertRow(rowCnt);

						for (var c = 0; c < $scope.notificationTableHeaders.length; c++) {
							var td = document.createElement('td'); // table
							// definition.
							td = tr.insertCell(c);

							var node = document.createElement('div');
							node.style.marginLeft = '5px';
							node.style.marginRight = '5px';

							node.innerHTML = listItem[$scope.notificationTableHeaders[c]] == undefined ? "-"
									: listItem[$scope.notificationTableHeaders[c]];
							td.appendChild(node);
						}
					}

					$scope.notificationModalCloseBtnClik = function() {
						$('#idNotificationmodal').modal('hide');
					};

					$scope.localizeDateStr = function(date_to_convert_str) {
						return date_to_convert_str.slice(0, 20);
					}

				});