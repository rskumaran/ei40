app
		.controller(
				'warehouseMaterialController',
				function($scope, $http, $window, $rootScope, $location,
						$timeout) {
					$scope.user = JSON.parse($window.sessionStorage
							.getItem("user"));
					if ($scope.user == null) {
						$location.path('/login');
						return;
					}
					
					$scope.currentMenuName = $window.sessionStorage
							.getItem("menuName");
					
 
					$scope.assignedTagMacId = '';
					$scope.trackMacId = '';
					if ($scope.authen == undefined) {
						$scope.authen = {};
					}
					$scope.showSearchIcon = true;
					$scope.searchButton = true;
					//$scope.checkTagPeopleId = {};
					$scope.tagData = new Array();
					//$scope.tagDetail= new Array();
					$scope.skuDetail = new Array();
					$scope.tagDataFilterList = new Array();	
					$scope.verificationTableStock = [];	
					$scope.verificationTableDispatch = [];
					$scope.searchText = "";	
					$scope.warehouseTabName = "";
					$rootScope.isWarehouseMaterial = false ;
					var selectedRowCSS = {'background': '#4da3ff', 'color': '#ffffff', 'font-weight': 'bold'};
					var unSelectedRowCSS = {'background': '#ffffff', 'color': '#000000', 'font-weight': 'normal'};
					// to enable/disable sub menus
					$scope.user = JSON.parse($window.sessionStorage
							.getItem("user"));
					
					
					$scope.showLocateBtn = false;
					
					if ($scope.user.capabilityDetails != undefined
							&& $scope.user.capabilityDetails != null) {

						if ($scope.user.capabilityDetails.HMVA == 1) {
							$scope.showMaterialBtn = true;
						} else {
							$scope.showMaterialBtn = false;
						}
						if ($scope.user.capabilityDetails.HMDP == 1) {
							$scope.showDispatchBtn = true;
						} else {
							$scope.showDispatchBtn = false;
						}
						if ($scope.user.capabilityDetails.HMRP == 1) {
							$scope.showReciptBtn = true;
						} else {
							$scope.showReciptBtn = false;
						}
						if ($scope.user.capabilityDetails.HMDS == 1) {
							$scope.showDispatchMaterialBtn = true;
						} else {
							$scope.showDispatchMaterialBtn = false;
						}
						
						if ($scope.user.capabilityDetails.HKLV == 1) {
							$scope.showLocateBtn = true;
						}

					}
					
					
					$("#trackSingleTagBtn").hide(); 
					$("#canceltrackSingleTagBtn").hide(); 
					
					$scope.trackTag = function() {
						if($scope.assignedTagMacId!=undefined && $scope.assignedTagMacId!=null 
								&& $scope.assignedTagMacId!='')
						{
							
							$rootScope.trackingMacId = $scope.assignedTagMacId;
							$rootScope.showFullView();
							var currentMenuName = $window.sessionStorage.getItem("menuName");
                            if ($scope.user.role == "DemoUser")
                            {
                                $window.sessionStorage.setItem("menuName", "shopFloorAnalysis");
                                $window.sessionStorage.setItem("demoMenuName", "warehouseLiveView");
                                $location.path('/shopFloorAnalysis');
                            }else{
								$window.sessionStorage.setItem("menuName", "warehouseLiveView");
								$location.path('/warehouseLiveView'); 
                            }
                            $rootScope.$broadcast('sourcePreviousScreen',currentMenuName);
						}else{
							toastr.error('No assigned tags to track.', "EI4.0");  
						}
						 
					}
					
					$scope.trackSingleTag = function() {
						if ($scope.trackMacId != undefined
								&& $scope.trackMacId != null
								&& $scope.trackMacId != '') {
							
							$rootScope.trackingMacId = $scope.trackMacId;
							$rootScope.showFullView();
							var currentMenuName = $window.sessionStorage.getItem("menuName");
                            if ($scope.user.role == "DemoUser")
                            {
                                $window.sessionStorage.setItem("menuName", "shopFloorAnalysis");
                                $window.sessionStorage.setItem("demoMenuName", "warehouseLiveView");
                                $location.path('/shopFloorAnalysis');
                            }else{
								$window.sessionStorage.setItem("menuName",
										"warehouseLiveView");
								$location.path('/warehouseLiveView');
                            }
                            $rootScope.$broadcast('sourcePreviousScreen',currentMenuName);
						} else {
							toastr.error('No assigned tags to track.', "EI4.0");
						}
					}
					
					$scope.selectedRow = -1;
					$scope.cancelEdit = function() {
						var rowCount = ($('#materialTable tr').length);
						for (var i = 0; i < rowCount; i++) {
							$("#materialTable > tbody > tr:nth-child(" + (i+1) + ")").css(unSelectedRowCSS);
						}
						$scope.selectedRow = -1;
						$scope.trackMacId = '';
						$("#trackSingleTagBtn").hide(); 
						$("#canceltrackSingleTagBtn").hide();  
					}
					
					
					$scope.rowHighilited = function(row) {
						$scope.selectedRow = row;
						// $scope.authen = $scope.assetChronoData[row];
						 $scope.authen = JSON.parse(JSON.stringify($scope.assetChronoViewData[row])); 
 
 							
							if($scope.authen.status=='Valid'){
								if($scope.authen.mac_id !=undefined && $scope.authen.mac_id != null  
										&& $scope.authen.mac_id !=''){
									$scope.trackMacId = $scope.authen.mac_id ; 
								}else 
									$scope.trackMacId = ''; 
							}else 
								$scope.trackMacId = '';  
							
							
					}
					
					$scope.searchOption = function() {						
						$scope.filterOption();
					}

					$scope.filterOption = function() {
						var indexArray = [];
						$scope.tagViewData = [];
						$scope.tagDataFilterList = [];
						
						 
					
					for (var i = 0; i < $scope.tagData.length; i++) {
						if ($scope.searchText == '') {
							
							$scope.tagDataFilterList
									.push($scope.tagData[i]);
						} else {
							var obj = {};
							obj = $scope.tagData[i];
							 for(var x = 0; x < $scope.uniqueheaderList.length; x++){								
								 var tableText = JSON.stringify($scope.tagData[i][$scope.uniqueheaderList[x].displayHeader]);	 							
							
							if ((tableText != undefined && tableText != null ? tableText : "").toLowerCase()
											.includes($scope.searchText.toLowerCase()))								
								{
									$scope.tagDataFilterList.push($scope.tagData[i]);
									break;									 
								}	
						 }
							
					}						
						
				} 
					$scope.fntoCreateTableID();
					
						$scope.totalItems = $scope.tagDataFilterList.length;
						$scope.currentPage = 1;
						$scope.getTotalPages();
						 $scope.getAssignedTagIds($scope.tagDataFilterList);
						if (	$scope.tagDataFilterList.length == 0) {
							$scope.tagNoData = true;
							$scope.no_data_found = "No data found.";

						} else {
							$scope.tagNoData = false;

							if ($scope.tagDataFilterList.length > $scope.numPerPage) {
								$scope.tagViewData = $scope.tagDataFilterList
										.slice(0, $scope.numPerPage);
								$scope.PaginationTab = true;
							} else {
								$scope.tagViewData = $scope.tagDataFilterList;
								$scope.PaginationTab = false;
							}
						}

						 for (var k = 0; k < $scope.tagViewData.length; k++) {
						  $scope.addMaterialTableRow(k); 
						  }	
						
					}// kumar 02/Jul filter option <-
					var reA = /[^a-zA-Z]/g;
					var reN = /[^0-9]/g;
					function sortAlphaNum(a1, b1) {
						var a = a1.tagId;
						var b = b1.tagId;
						var aA = a.replace(reA, "");
						var bA = b.replace(reA, "");
						if (aA === bA) {
							var aN = parseInt(a.replace(reN, ""), 10);
							var bN = parseInt(b.replace(reN, ""), 10);
							return aN === bN ? 0 : aN > bN ? 1 : -1;
						} else {
							return aA > bA ? 1 : -1;
						}
					}
					
					$scope.multiSelectModalStockOKBtnClick = function() {
						$rootScope.materialwarehouseApiFlag = 2;
					$scope.uniqueheaderList=[];
					$rootScope.warehouseSaveDisplayFieldArray = [];
					var values = '';
					var flag = 0;
					
					$("#multiselectlistStock" + " #multivaltoStock").find("option").each(function(){
						 flag = 1;
					values = $(this).html();
					// $scope.uniqueheaderList.push(values)
					 
				    for(var i=0; i<$scope.verificationTableStock.length; i++){
				    	if($scope.verificationTableStock[i].displayHeader == values)
				    	var actualHeader = $scope.verificationTableStock[i].actualHeader
				    }
					$scope.uniqueheaderList.push({					
						'actualHeader': actualHeader,											
						'displayHeader': values											
					});						

					$rootScope.warehouseSaveDisplayFieldArray.push({					
						'actualHeader': actualHeader,											
						'displayHeader': values											
					});
			
				});
					$scope.fncreateTable();						
					$('#idStockMultiselectmodal').modal('hide');	
					}
					$scope.multiSelectModalStockCancelBtnClick = function() {
						$scope.fncreateTable();						
						$('#idStockMultiselectmodal').modal('hide');	
					}					
						
					//Multi Select Modal Dispatch OK Btn Click 
					$scope.multiSelectModalDispatchOKBtnClick = function() {
					$rootScope.dispatchwarehouseApiFlag = 2;
					$scope.uniqueheaderList=[];
					$rootScope.dispatchSaveDisplayFieldArray = [];
					var values = '';
					var flag = 0;
					
					$("#multiselectlistDispatch" + " #multivaltoDispatch").find("option").each(function(){
						 flag = 1;
					values = $(this).html();
					// $scope.uniqueheaderList.push(values)
					 
				    for(var i=0; i<$scope.verificationTableDispatch.length; i++){
				    	if($scope.verificationTableDispatch[i].displayHeader == values)
				    	var actualHeader = $scope.verificationTableDispatch[i].actualHeader
				    }
					$scope.uniqueheaderList.push({					
						'actualHeader': actualHeader,											
						'displayHeader': values											
					});						

					$rootScope.dispatchSaveDisplayFieldArray.push({					
						'actualHeader': actualHeader,											
						'displayHeader': values											
					});
			
				});
					$scope.fncreateTable();						
					$('#idDispatchMultiselectmodal').modal('hide');	
			}
					
					//Multi Select Modal Dispatch CANCEL Btn Click
					$scope.multiSelectModalDispatchCancelBtnClick = function() {
						$scope.fncreateTable();						
						$('#idDispatchMultiselectmodal').modal('hide');	
					}					
					
					
						$scope.fngetHeaderList = function() {
							
							$scope.tagData = [];
							 $scope.verificationTableStock = [];
							 $scope.verificationTableDispatch = [];
							var url =  ""
							 
							if($scope.warehouseTabName == "Material"){
								url =  './wareHouse/valid'
							}else if($scope.warehouseTabName == "Dispatch"){
								url =  './wareHouse/completed'
							}
							if (!$rootScope.checkNetconnection()) { 				
							 	$rootScope.alertDialogChecknet();
								return; 
								}
							$rootScope.showloading('#loadingBar');
							$http({
								method : 'GET',
								url : url,

							})
									.then(
											function(response) {
												$rootScope
												.hideloading('#loadingBar');
												if (response != null
														&& response.data != null
														&& response.data.length != 0
														&& response.data != "BAD_REQUEST"
														&& response.data != undefined
														&& response.data.data.length != 0) {
												
													$scope.tagData = [];
													// to change the header
													// names
													response.data.data = JSON.parse(JSON.stringify(response.data.data).split('"issue_date":').join('"Issue Date":'));
													response.data.data = JSON.parse(JSON.stringify(response.data.data).split('"dispatch_date":').join('"Dispatch Date":'));
													response.data.data = JSON.parse(JSON.stringify(response.data.data).split('"arrival_date":').join('"Arrival Date":'));
													response.data.data = JSON.parse(JSON.stringify(response.data.data).split('"created_date":').join('"Created Date":'));													
													
													response.data.data = JSON.parse(JSON.stringify(response.data.data).split('"manufacture_date":').join('"Manufacture Date":'));													
													response.data.data = JSON.parse(JSON.stringify(response.data.data).split('"modified_date":').join('"Modified Date":'));
													response.data.data = JSON.parse(JSON.stringify(response.data.data).split('"expiry_date":').join('"Expiry Date":'));
													response.data.data = JSON.parse(JSON.stringify(response.data.data).split('"issue_date":').join('"Issue Date":'));
													response.data.data = JSON.parse(JSON.stringify(response.data.data).split('"arrival_date":').join('"Arrival Date":'));
													
													for (var i = 0; i < response.data.data.length; i++) {
														

														if(response.data.data[i]["Issue Date"] != undefined && response.data.data[i]["Issue Date"] != null &&  response.data.data[i]["Issue Date"] != "null")
															response.data.data[i]["Issue Date"] = moment(response.data.data[i]["Issue Date"]).format("MMM DD, YYYY");
														else 
															response.data.data[i]["Issue Date"] = "";
														if(response.data.data[i]["Dispatch Date"] != undefined && response.data.data[i]["Dispatch Date"] != null &&  response.data.data[i]["Dispatch Date"] != "null")
															response.data.data[i]["Dispatch Date"] = moment(response.data.data[i]["Dispatch Date"]).format("MMM DD, YYYY");
														else 
															response.data.data[i]["Dispatch Date"] = "";
														if(response.data.data[i]["Arrival Date"] != undefined && response.data.data[i]["Arrival Date"] != null &&  response.data.data[i]["Arrival Date"] != "null")
															response.data.data[i]["Arrival Date"] = moment(response.data.data[i]["Arrival Date"]).format("MMM DD, YYYY");
														else 
															response.data.data[i]["Arrival Date"] = "";
														if(response.data.data[i]["Created Date"] != undefined && response.data.data[i]["Created Date"] != null &&  response.data.data[i]["Created Date"] != "null")
															response.data.data[i]["Created Date"] = moment(response.data.data[i]["Created Date"]).format("MMM DD, YYYY");
														else 
																response.data.data[i]["Created Date"] = "";
														if(response.data.data[i]["Modified Date"] != undefined && response.data.data[i]["Modified Date"] != null &&  response.data.data[i]["Modified Date"] != "null")
															response.data.data[i]["Modified Date"] = moment(response.data.data[i]["Modified Date"]).format("MMM DD, YYYY");
														else 
															response.data.data[i]["Modified Date"] = "";
														if(response.data.data[i]["Manufacture Date"] != undefined && response.data.data[i]["Manufacture Date"] != null &&  response.data.data[i]["Manufacture Date"] != "null")
															response.data.data[i]["Manufacture Date"] = moment(response.data.data[i]["Manufacture Date"],"DD/MM/YYYY").format("MMM DD, YYYY");
														else 
															response.data.data[i]["Manufacture Date"] = "";
														if(response.data.data[i]["Date of Manufacture"] != undefined && response.data.data[i]["Date of Manufacture"] != null &&  response.data.data[i]["Date of Manufacture"] != "null")
															response.data.data[i]["Date of Manufacture"] = moment(response.data.data[i]["Date of Manufacture"],"DD/MM/YYYY").format("MMM DD, YYYY");
														else 
															response.data.data[i]["Date of Manufacture"] = "";
														if(response.data.data[i]["Purchase Date"] != undefined && response.data.data[i]["Purchase Date"] != null &&   response.data.data[i]["Purchase Date"] != "null")
															response.data.data[i]["Purchase Date"] = moment(response.data.data[i]["Purchase Date"],"DD/MM/YYYY").format("MMM DD, YYYY");
														else 
															response.data.data[i]["Purchase Date"] = ""; 
														if(response.data.data[i]["Expiry Date"] != undefined && response.data.data[i]["Expiry Date"] != null &&   response.data.data[i]["Expiry Date"] != "null")
															response.data.data[i]["Expiry Date"] = moment(response.data.data[i]["Expiry Date"],"DD/MM/YYYY").format("MMM DD, YYYY");
														else 
															response.data.data[i]["Expiry Date"] = "";
														
															$scope.tagData.push(response.data.data[i]);															
													}
													
													$scope.fncreateHeaderAndTable();

												} else {
													$scope.showMultiSelectSetting = false;
													$('#idTableView').empty();
													$scope.tagNoData = true;
													$scope.no_data_found = "No data found.";
												}
											},
											function error(response) {
												$rootScope
														.hideloading('#loadingBar');
												$rootScope.fnHttpError(response);
											});
						}
						
						
						$scope.fncreateHeaderAndTable = function() {
							var headerListfromAPI = [];
							var headerListAfterConcat = [];
							$scope.uniqueheaderList = [];
							$scope.uniqueheaderListfromAPI = [];
							$scope.uniqueheaderListForStock = [];
							$scope.uniqueheaderListForDispatch = [];
							
						// $('#tableOptions').empty();
							
							for(var i=0; i<$scope.tagData.length; i++){						
								var jsonObj = $scope.tagData[i];
								headerListfromAPI = Object.keys(jsonObj);					
								
								if(headerListfromAPI!=null ) {
									headerListAfterConcat = headerListAfterConcat.concat(headerListfromAPI)
								}						
							}
							$scope.uniqueheaderListfromAPI = [...new Set(headerListAfterConcat)];
							
							var removeHeaderItems = ["mac_id", "zone_id", "tag_id", "status" ]
							for(var i = 0; i < removeHeaderItems.length; i++){						
								const index = $scope.uniqueheaderListfromAPI.indexOf(removeHeaderItems[i]);
								if (index > -1) {
									$scope.uniqueheaderListfromAPI.splice(index, 1);
								}							
								}
							
							if($scope.warehouseTabName == "Material"){								
							const index = $scope.uniqueheaderListfromAPI.indexOf("Dispatch Date");
							if (index > -1) {
								$scope.uniqueheaderListfromAPI.splice(index, 1);
							}							
							}
							
							
							$scope.uniqueheaderListfromAPI.sort();
							for (var i = 0; i < $scope.uniqueheaderListfromAPI.length; i++) {								
									$scope.uniqueheaderListForStock.push($scope.uniqueheaderListfromAPI[i]);															
							}
							//$scope.uniqueheaderListForStock = $scope.uniqueheaderListfromAPI;
							$scope.uniqueheaderListForDispatch = $scope.uniqueheaderListfromAPI;
							if($scope.warehouseTabName == "Material"){
								if($rootScope.renamedFieldListStock.length != 0){
							for(var i = 0; i < $rootScope.renamedFieldListStock.length; i++){						
								const index = $scope.uniqueheaderListForStock.indexOf($rootScope.renamedFieldListStock[i]);
								if (index > -1) {
									$scope.uniqueheaderListForStock.splice(index, 1);
								}							
								}
								}
							}else if($scope.warehouseTabName == "Dispatch"){
								if($rootScope.renamedFieldListDispatch.length != 0){
									for(var i = 0; i < $rootScope.renamedFieldListDispatch.length; i++){						
										const index = $scope.uniqueheaderListForDispatch.indexOf($rootScope.renamedFieldListDispatch[i]);
										if (index > -1) {
											$scope.uniqueheaderListForDispatch.splice(index, 1);
										}							
										}
								}
									
							}
							
							if($scope.warehouseTabName == "Material"){
								if($rootScope.materialwarehouseApiFlag == 1){
									for(var i=0; i<$scope.uniqueheaderListfromAPI.length; i++){
										$scope.uniqueheaderList.push({					
											'actualHeader': $scope.uniqueheaderListfromAPI[i],											
											'displayHeader': $scope.uniqueheaderListfromAPI[i]											
										});
									}
									}else if($rootScope.materialwarehouseApiFlag == 2){
																 
										for(var i=0; i<$rootScope.warehouseSaveDisplayFieldArray.length; i++){
											var appendvalue = '';
									    	$scope.uniqueheaderList.push({					
												'actualHeader': $rootScope.warehouseSaveDisplayFieldArray[i].actualHeader,											
												'displayHeader': $rootScope.warehouseSaveDisplayFieldArray[i].displayHeader											
											});
									    	$('#multiselectlistStock').find('#multivaltoStock option[value="'+ $scope.uniqueheaderList[i].displayHeader+'"]').remove();
											    appendvalue += '<option value="'+$scope.uniqueheaderList[i].displayHeader+'">'+$scope.uniqueheaderList[i].displayHeader+'</option>';															 
											    $('#multiselectlistStock').find('#multivaltoStock').append(appendvalue);	
									    }		
									    }								
									
									for(var i=0; i<$scope.uniqueheaderListfromAPI.length; i++){
										$scope.verificationTableStock.push({					
											'actualHeader': $scope.uniqueheaderListfromAPI[i],											
											'displayHeader': $scope.uniqueheaderListfromAPI[i]											
										});
									}
							}else if($scope.warehouseTabName == "Dispatch"){
								if($rootScope.dispatchwarehouseApiFlag == 1){
									for(var i=0; i<$scope.uniqueheaderListfromAPI.length; i++){
										$scope.uniqueheaderList.push({					
											'actualHeader': $scope.uniqueheaderListfromAPI[i],											
											'displayHeader': $scope.uniqueheaderListfromAPI[i]											
										});
									}
									}else if($rootScope.dispatchwarehouseApiFlag == 2){
																 
										for(var i=0; i<$rootScope.dispatchSaveDisplayFieldArray.length; i++){
											var appendvalue = '';
									    	$scope.uniqueheaderList.push({					
												'actualHeader': $rootScope.dispatchSaveDisplayFieldArray[i].actualHeader,											
												'displayHeader': $rootScope.dispatchSaveDisplayFieldArray[i].displayHeader											
											});
									    	$('#multiselectlistDispatch').find('#multivaltoDispatch option[value="'+ $scope.uniqueheaderList[i].displayHeader+'"]').remove();
											    appendvalue += '<option value="'+$scope.uniqueheaderList[i].displayHeader+'">'+$scope.uniqueheaderList[i].displayHeader+'</option>';															 
											    $('#multiselectlistDispatch').find('#multivaltoDispatch').append(appendvalue);	
									    }		
									    }								
									
									for(var i=0; i<$scope.uniqueheaderListfromAPI.length; i++){
										$scope.verificationTableDispatch.push({					
											'actualHeader': $scope.uniqueheaderListfromAPI[i],											
											'displayHeader': $scope.uniqueheaderListfromAPI[i]											
										});
									}	
							}	
							
						
							$scope.fncreateTable();
						}
						$scope.fncreateTable = function() {							
							
						
							$scope.fntoCreateTableID();
							
							if($scope.warehouseTabName == "Material" && $rootScope.materialwarehouseApiFlag == 2 && $rootScope.warehouseSaveDisplayFieldArray.length == 0){
								$scope.tagNoData = true;
								$scope.no_data_found = "Please select display fields.";	
								return;
							}else if($scope.warehouseTabName == "Dispatch" && $rootScope.dispatchwarehouseApiFlag == 2 && $rootScope.dispatchSaveDisplayFieldArray.length == 0){
								$scope.tagNoData = true;
								$scope.no_data_found = "Please select display fields.";		
								return;
							}else if ($scope.tagData.length == 0) {
								$scope.tagNoData = true;
								$scope.no_data_found = "No data found.";
								return;
							}else {
								$scope.tagNoData = false;
							}
						
						if ($scope.tagData.length > 10) {
							$scope.PaginationTab = true;
						} else {
							$scope.PaginationTab = false;
						}
						$scope.totalItems = $scope.tagData.length;
						$scope.getTotalPages();					
						$scope.currentPage = 1;
						 $scope.getAssignedTagIds($scope.tagData);
						
						if ($scope.tagData.length > $scope.numPerPage) {
							$scope.tagViewData = $scope.tagData
									.slice(
											0,
											$scope.numPerPage);
							$scope.hasPrevious = true;
							$scope.isFirstPage = true;
							$scope.hasNext = false;
							$scope.isLastPage = false;
						} else {
							$scope.tagViewData = $scope.tagData;
						}
						
						for (var k = 0; k < $scope.tagViewData.length; k++) {
							$scope.addMaterialTableRow(k);
						}				
						
						}
						
						
						$scope.fntoCreateTableID = function(){
							// to create a dynamic table
							$('#idTableView').empty();
							$scope.materialTableHeaders = [];	
							if($scope.uniqueheaderList != undefined){
								for(var i=0; i<$scope.uniqueheaderList.length; i++){
									$scope.materialTableHeaders.push({					
										'actualHeader': $scope.uniqueheaderList[i].actualHeader,											
										'displayHeader': $scope.uniqueheaderList[i].displayHeader											
									});
								}	
							}	
							// to add serial no to table starts here
						    $scope.materialTableHeaders.push ({
													'actualHeader' : "SNo",
													'displayHeader' : "S. No",
													});						    
						    var fromIndex;
							for (var x = 0; x < $scope.materialTableHeaders.length; x++) {
								if($scope.materialTableHeaders[x].actualHeader == "SNo")
									 fromIndex = x;									
								}
						    
						    var element = $scope.materialTableHeaders[fromIndex];
						    $scope.materialTableHeaders.splice(fromIndex, 1);
						    $scope.materialTableHeaders.splice(0, 0, element);
						 // to add serial no to table ends here

							var materialTable = document.createElement('table');
							materialTable.setAttribute('id','materialTable'); // table
																				// id.
							materialTable.className = "tableWidth";
							materialTable.className = "table table-bordered";

							var tr = materialTable.insertRow(-1);
							if($scope.materialTableHeaders != undefined && $scope.materialTableHeaders.length != null &&
									$scope.materialTableHeaders.length != 0 ){
								for (var h = 0; h < $scope.materialTableHeaders.length; h++) {
									var th = document.createElement('th');
									th.className = "thcss";
									th.innerHTML = $scope.materialTableHeaders[h].displayHeader;
									th.width = "200px";
									tr.appendChild(th);
								}

							}
							
							var div = document
									.getElementById('idTableView');
							div.appendChild(materialTable); // add the TABLE to
						}
						
						// now, add a new row to the TABLE.
						$scope.addMaterialTableRow = function(pos) {
							var listItem =  $scope.tagViewData[pos];
							
							// to add serial to table starts here
							listItem.SNo = pos + 1							
							
							var materialTab = document
									.getElementById('materialTable');

							var rowCnt = materialTab.rows.length; // table row
							var tr = materialTab.insertRow(rowCnt);	
							
							var rowClickHandler = function(row) {
							      return function() {

							    	  $scope.selectedRow = row;
										 
										 $scope.authen = $scope.tagViewData[row]; 
				  
											if($scope.authen.status=='Valid'){
												if($scope.authen.mac_id !=undefined && $scope.authen.mac_id != null  
														&& $scope.authen.mac_id !=''){
													$scope.trackMacId = $scope.authen.mac_id ; 
												}else 
													$scope.trackMacId = ''; 
											}else 
												$scope.trackMacId = ''; 
											var rowCount = ($('#materialTable tr').length);
											for (var i = 0; i < rowCount; i++) {
												$("#materialTable > tbody > tr:nth-child(" + (i+1) + ")").css(unSelectedRowCSS);
											}
											$("#materialTable > tbody > tr:nth-child(" + ($scope.selectedRow + 2) + ")").css(selectedRowCSS);
										
											if($scope.selectedRow>=0&&$scope.trackMacId!='')
											{
												$("#trackSingleTagBtn").show(); 
												$("#canceltrackSingleTagBtn").show(); 
											}else
											{
												$("#trackSingleTagBtn").hide(); 
												$("#canceltrackSingleTagBtn").hide(); 
											}
											
							      };
							    };
							    tr.onclick = rowClickHandler(pos);
							  
							for (var c = 0; c < $scope.materialTableHeaders.length; c++) {
								var td = document.createElement('td'); // table
								// definition.
								td = tr.insertCell(c);
								
								var node = document.createElement('div');
								node.style.marginLeft = '5px';
								node.style.marginRight = '5px';
								node.style.width = '200px';

								node.innerHTML = listItem[$scope.materialTableHeaders[c].actualHeader];
								if(node.innerHTML == "undefined" || node.innerHTML == null || node.innerHTML == "null" )
									node.innerHTML = " "
								td.appendChild(node);
							}
						}					
						
					
					// pagination button
					$scope.currentPage = 1;
					$scope.currentPage = 1;
					$scope.numPerPage = 10;
					$scope.maxSize = 3;
					$scope.totalItems = 0;
					$scope.tagDataFilterList = [];
					$scope.tagViewData = [];
					$scope
							.$watch(
									'currentPage + numPerPage',
									function() {
										var begin = (($scope.currentPage - 1) * $scope.numPerPage), end = begin
												+ $scope.numPerPage;
										if ($scope.searchText == '') 
											$scope.tagViewData = $scope.tagData
													.slice(begin, end);
										else
											$scope.tagViewData = $scope.tagDataFilterList
													.slice(begin, end);
										
										$scope.fntoCreateTableID();
										  for (var k = 0; k < $scope.tagViewData.length; k++) {
										  $scope.addMaterialTableRow(k); 
										  }
										 
										$scope.enablePageButtons();
									});

					$scope.currentPage = 1;
					$scope.maxSize = 3;
					$scope.numPerPage = 10;
					$scope.totalItems = 0;
					$scope.totalPages = 0;

					$scope.hasNext = true;
					$scope.hasPrevious = true;
					$scope.isFirstPage = true;
					$scope.isLastPage = true;
					$scope.serial = 1;

					$scope
							.$watch(
									"currentPage",
									function(newVal, oldVal) {
										if (newVal !== oldVal) {

											if (newVal > $scope.totalPages) {
												$scope.currentPage = $scope.totalPages;
											} else {
												$scope.currentPage = newVal;
											}

											$scope.serial = (($scope.currentPage - 1) * $scope.numPerPage) + 1;
											$scope.enablePageButtons();
										}
									});

					$scope.enablePageButtons = function() {
						$scope.serial = (($scope.currentPage - 1) * $scope.numPerPage) + 1;
						$scope.selectedRow = -1;
						if ($scope.totalPages <= 1) {
							$scope.hasPrevious = true;
							$scope.isFirstPage = true;
							$scope.hasNext = true;
							$scope.isLastPage = true;

						} else if ($scope.totalPages > 1
								&& $scope.currentPage == $scope.totalPages) {

							$scope.hasPrevious = false;
							$scope.isFirstPage = false;
							$scope.hasNext = true;
							$scope.isLastPage = true;
						} else if ($scope.currentPage != 1
								&& $scope.currentPage < $scope.totalPages) {

							$scope.hasPrevious = false;
							$scope.isFirstPage = false;
							$scope.hasNext = false;
							$scope.isLastPage = false;

						} else {
							$scope.hasPrevious = true;
							$scope.isFirstPage = true;
							$scope.hasNext = false;
							$scope.isLastPage = false;
						}
 
					};

					$scope.getTotalPages = function() {

						if (($scope.totalItems % $scope.numPerPage) > 0)
							$scope.totalPages = parseInt($scope.totalItems
									/ $scope.numPerPage) + 1;
						else
							$scope.totalPages = parseInt($scope.totalItems
									/ $scope.numPerPage);
						$scope.enablePageButtons();

					};

					$scope.getAssignedTagIds = function(assetList) {

						$scope.assignedTagMacId = '';

						if (assetList != undefined && assetList != null) {
							for (var i = 0; i < assetList.length; i++) {
								if (assetList[i].status == 'Valid') {
									if (assetList[i].mac_id != undefined && assetList[i].mac_id != null
											&& assetList[i].mac_id != '') {
										if ($scope.assignedTagMacId == '')
											$scope.assignedTagMacId = assetList[i].mac_id;
										else
											$scope.assignedTagMacId = $scope.assignedTagMacId
													+ ","
													+ assetList[i].mac_id;

									}
								}
							}
						}

					}
					
					$scope.goFirstPage = function() {
						$scope.currentPage = 1;
						$scope.enablePageButtons();
					};

					$scope.goPreviousPage = function() {
						if ($scope.currentPage > 1)
							$scope.currentPage = $scope.currentPage - 1;
						$scope.enablePageButtons();
					};

					$scope.goNextPage = function() {
						if ($scope.currentPage <= $scope.totalPages)
							$scope.currentPage = $scope.currentPage + 1;
						$scope.enablePageButtons();
					};

					$scope.goLastPage = function() {
						$scope.currentPage = $scope.totalPages;
						$scope.enablePageButtons();
					};

					$scope.displayMaterialViewScreen = function() {
						
						$scope.tagDataFilterList = [];
						$scope.tagViewData = [];
						$scope.searchText = "";
						$scope.selectedRow = -1;
						$scope.currentPage = 1;
						$scope.searchButton = true;
						$scope.showSearchIcon = true;
						$scope.wareHouseMaterialViewScreen = true;	
						document.getElementById("id_CardGeneral").style.width = '100%';
						$scope.showMultipleSearch = false;
						 $scope.showCardSKUdetail = false;
						$scope.fetchScreen= false;
						$scope.showMultiSelectSetting = true;
						$scope.showtrackIcon = true;
						$scope.assignScreen= false;
						$scope.tagSKUReturnScreen = false;
						$scope.warehouseTabName = "Material";
						$("#idMaterialView").addClass("active");
						$("#idDispatchView").removeClass("active");
						$("#idReceipt").removeClass("active");
						$("#idDispatchMaterial").removeClass("active");
						
						$scope.fngetHeaderList();
						// $scope.getTagData();
			 		
					};
					$scope.displayDispatchViewScreen = function() {
						
						$scope.tagDataFilterList = [];
						$scope.tagViewData = [];
						$scope.searchText = "";
						$scope.selectedRow = -1;
						$scope.currentPage = 1;
						$scope.searchButton = true;
						$scope.wareHouseMaterialViewScreen = true;
						document.getElementById("id_CardGeneral").style.width = '100%';
						$scope.showSearchIcon = true;
						$scope.showMultipleSearch = false;
						$scope.fetchScreen= false;
						$scope.assignScreen= false;
						$scope.tagSKUReturnScreen = false;
						$scope.showCardSKUdetail = false;
						$scope.showMultiSelectSetting = true;
						$scope.showtrackIcon = false;
						$scope.warehouseTabName = "Dispatch";
						$("#idMaterialView").removeClass("active");
						$("#idDispatchView").addClass("active"); 
						$("#idReceipt").removeClass("active");
						$("#idDispatchMaterial").removeClass("active");
						$scope.fngetHeaderList();
						// $scope.getTagData();
		 		
					};
					$scope.showSKUDetail = function(){
						$scope.skuDetail = [];
						
						if($scope.skuNo == undefined || $scope.skuNo == null|| $scope.skuNo == ""   )
							{
							$rootScope.showAlertDialog("Please enter SKU Number to show its details");
							return;
						
							}
						else{
							if (!$rootScope.checkNetconnection()) { 
							 	$rootScope.alertDialogChecknet();
								return; 
								}
							$rootScope.showloading('#loadingBar');	
							$http({
								method : 'GET',
								url : './wareHouseData/'+$scope.skuNo,

							})
									.then(
											function(response) {
												$rootScope.hideloading('#loadingBar');
												if (response != null
														&& response.data != null
														&& response.data.length != 0
														&& response.data != "BAD_REQUEST"
														&& response.data != undefined
														&& response.data.length != 0) {													
													//$scope.skuDetail = response.data.wareHouseData;
													//console.log($scope.skuDetail["Storage Area"]);
													
													$('#SKUDetailsTable').empty();
													if(response.data.status == "success"){
														$scope.showCardSKUdetail = true;													
														
														
														if(response.data.wareHouseData != undefined && response.data.wareHouseData != null ){
															if(response.data.wareHouseData["Issue Date"] != undefined && response.data.wareHouseData["Issue Date"] != null &&  response.data.wareHouseData["Issue Date"] != "null")
																response.data.wareHouseData["Issue Date"] = moment(response.data.wareHouseData["Issue Date"]).format("MMM DD, YYYY");
															
															if(response.data.wareHouseData["Dispatch Date"] != undefined && response.data.wareHouseData["Dispatch Date"] != null &&  response.data.wareHouseData["Dispatch Date"] != "null")
																response.data.wareHouseData["Dispatch Date"] = moment(response.data.wareHouseData["Dispatch Date"]).format("MMM DD, YYYY");
															
															if(response.data.wareHouseData["Arrival Date"] != undefined && response.data.wareHouseData["Arrival Date"] != null &&  response.data.wareHouseData["Arrival Date"] != "null")
																response.data.wareHouseData["Arrival Date"] = moment(response.data.wareHouseData["Arrival Date"], "dd-mm-yyyy").format("MMM DD, YYYY");
															
															if(response.data.wareHouseData["Created Date"] != undefined && response.data.wareHouseData["Created Date"] != null &&  response.data.wareHouseData["Created Date"] != "null")
																response.data.wareHouseData["Created Date"] = moment(response.data.wareHouseData["Created Date"]).format("MMM DD, YYYY");
															
															if(response.data.wareHouseData["Manufacture Date"] != undefined && response.data.wareHouseData["Manufacture Date"] != null &&  response.data.wareHouseData["Manufacture Date"] != "null")
																response.data.wareHouseData["Manufacture Date"] = moment(response.data.wareHouseData["Manufacture Date"],"DD/MM/YYYY").format("MMM DD, YYYY");
															
															if(response.data.wareHouseData["Date of Manufacture"] != undefined && response.data.wareHouseData["Date of Manufacture"] != null &&  response.data.wareHouseData["Date of Manufacture"] != "null")
																response.data.wareHouseData["Date of Manufacture"] = moment(response.data.wareHouseData["Date of Manufacture"],"DD/MM/YYYY").format("MMM DD, YYYY");
															
															if(response.data.wareHouseData["Modified Date"] != undefined && response.data.wareHouseData["Modified Date"] != null &&  response.data.wareHouseData["Modified Date"] != "null")
																response.data.wareHouseData["Modified Date"] = moment(response.data.wareHouseData["Modified Date"]).format("MMM DD, YYYY");
														
															if(response.data.wareHouseData["Purchase Date"] != undefined && response.data.wareHouseData["Purchase Date"] != null &&   response.data.wareHouseData["Purchase Date"] != "null")
																response.data.wareHouseData["Purchase Date"] = moment(response.data.wareHouseData["Purchase Date"],"DD/MM/YYYY").format("MMM DD, YYYY");
															
															if(response.data.wareHouseData["Expiry Date"] != undefined && response.data.wareHouseData["Expiry Date"] != null &&   response.data.wareHouseData["Expiry Date"] != "null")
																response.data.wareHouseData["Expiry Date"] = moment(response.data.wareHouseData["Expiry Date"],"DD/MM/YYYY").format("MMM DD, YYYY");
															
														}															
															$scope.skuDetail.push(response.data.wareHouseData);															
														
															//console.log($scope.skuDetail);
														
														
														if( $scope.skuDetail[0]["Storage Area"] !=null && $scope.skuDetail[0]["Storage Area"] != undefined && $scope.skuDetail[0]["Storage Area"]!= "")
														$scope.skuZoneName= $scope.skuDetail[0]["Storage Area"];
														skuDetailsTable();
														
													}
													
													else {
														$rootScope.showAlertDialog(response.data.message);
														return;
													
													}
													
												} else {
													toastr.error('No SKU Detail available',"EI4.0")	
												}
											},function error(response) {
												$rootScope.hideloading('#loadingBar');			
												$rootScope.fnHttpError(response);
												});
							
							
						}
						 
					};
					$scope.displayReceiptScreen = function() {
						 $scope.showCardSKUdetail = false;
						 document.getElementById("id_CardGeneral").style.width = '50%';
						 document.getElementById("SKUNoDetailsView").style.width = '49%';
						 $("#checktagIdSelect").val("");
						 $scope.showMultiSelectSetting = false;
						 $scope.showtrackIcon = false;
						 $scope.fetchScreen = false;
						 $scope.assignScreen = true;
						
						 $scope.showMultipleSearch = false;
					 		$scope.showSearchIcon = false;
					 		$scope.searchButton = false;
					 		$("#idMaterialView").removeClass("active");
							$("#idDispatchView").removeClass("active"); 
							$("#idReceipt").addClass("active");
							$("#idDispatchMaterial").removeClass("active");
						 if ($rootScope.ZoneSelection.zoneSelect != undefined && $rootScope.ZoneSelection.zoneSelect == "true"){
						 		$rootScope.ZoneSelection.zoneSelect = "false";
						 		$rootScope.isBackFromZone = false;
								$scope.authen.tagId = $rootScope.ZoneSelection.tagId;
								$scope.skuNo = $rootScope.ZoneSelection.skuNo;
								$scope.skuZoneName = $rootScope.ZoneSelection.selectedZone;
								$scope.zoneDetails = $rootScope.ZoneSelection.zoneDetails;
								$rootScope.ZoneSelection = {};
								$rootScope.setPageName("Warehouse Material");
				 	}else
			 		{	 $scope.getZoneDetails();
						  		$scope.wareHouseMaterialViewScreen = false;
								$scope.viewscreen = false;
								$scope.addscreen = false;
								$scope.tagSKUReturnScreen = false;
								}
					};
					$scope.displayDispatchMaterialScreen = function() {
						$scope.wareHouseMaterialViewScreen = false;
						$scope.viewscreen = false;
						$scope.addscreen = false;
						$scope.showSearchIcon = false;
						$scope.fetchScreen = true;
						document.getElementById("id_CardGeneral").style.width = '100%';
						$scope.assignScreen = false;
						$scope.tagSKUReturnScreen = false;
						$scope.searchButton = false;
						$scope.showMultipleSearch = false;
						$scope.showMultiSelectSetting = false;
						$scope.showtrackIcon = false;
						$scope.showCardSKUdetail = false;
						
						$("#idMaterialView").removeClass("active");
						$("#idDispatchView").removeClass("active"); 
						$("#idReceipt").removeClass("active");
						$("#idDispatchMaterial").addClass("active");
						//$scope.getTagData();
					};
					
					$scope.fnGetDictionaryLength = function(dict) {
						var nCount = 0;
						for (i in dict)
							++nCount;
						return nCount;
					}
					$scope.fnSelectZoneFromUI = function() {
						if ($scope.fnGetDictionaryLength($rootScope.ZoneSelection) == 0)
						{
							$rootScope.ZoneSelection.zoneSelect = "true";
							$rootScope.ZoneSelection.tagId = $scope.authen.tagId;
							$rootScope.ZoneSelection.skuNo = $scope.skuNo  ;
							//$rootScope.ZoneSelection.locationList = $scope.locationList;
							$rootScope.ZoneSelection.zoneDetails = $scope.zoneDetails;
							$rootScope.ZoneSelection.selectedZone = $scope.skuZoneName;
							$rootScope.isWarehouseMaterial = true ;
							$window.sessionStorage.setItem("menuName","zoneManagement");
							$location.path('/zoneManagement');
						//locationList
						}
					}
					$scope.cancelSKUTagAssign = function() {
						 $('#checktagIdSelect').val("");
						$scope.fetchScreen = false;
						$scope.tagSKUReturnScreen = false;
						$scope.assignScreen = true;
						$scope.checkTagMacId = "";
						$scope.authen.tagId= "";
						$scope.skuNo= "";
						if ($scope.zoneDetails.length > 0)
							$scope.skuZoneName = $scope.zoneDetails[0].zoneName;
						
						$scope.skuBatchNo = "";
						$scope.skuDescription = "";
						$rootScope.ZoneSelection = {};
						$scope.skuDetail = [];
						$('#SKUDetailsTable').empty();
						$scope.showCardSKUdetail = false;
					}
					$scope.assignSKUTag = function() {

						

						var varSKUNo = document
						.getElementById("id_skuNoAssignTag");
				if (varSKUNo.value.length == 0) {

					$scope.skuNoError = true;
					$scope.skuNo_error_msg = "Please Enter SKU No";
					$timeout(function() {
						$scope.skuNoError = false;
					}, 2000);
					return;
				}
				var tagIdSKU = document.getElementById("id_tagIdAssignTag");
				if (tagIdSKU.value.length == 0) {

					$scope.tagIdError = true;
					$scope.tagId_error_msg = "Please Enter Tag ID";
					$timeout(function() {
						$scope.tagIdError = false;
					}, 2000);
					return;
				}
						
						for(var i=0; i < $scope.zoneDetails.length; i++){
							if($scope.skuZoneName == $scope.zoneDetails[i].zoneName){
								var skuZoneId = $scope.zoneDetails[i].zoneId;
								break;
							}
						}
						if (!$rootScope.checkNetconnection()) {
							$rootScope.alertDialogChecknet();
							return;
						}
						var checkTagParameter = {
								"tagId" :  $scope.authen.tagId
							};

							$scope.promise = $http
									.post('./checkTag', checkTagParameter)
									.then(
											function(result) {

												if (result.data.status == "success") {
													if (result.data.tagStatus == "free") {

														skuZoneId = skuZoneId.toString();  
														$rootScope.showloading('#loadingBar');
														
															var assignTagParameter = {
																"tagId" : $scope.authen.tagId,
																"skuNo" : $scope.skuNo,
																"zoneId": skuZoneId
															}
															if (!$rootScope.checkNetconnection()) {
																return;
															}
															$scope.promise = $http
															.post('./assignWareHouse', assignTagParameter)
															.then(
																	function(result) {
																		$rootScope
																				.hideloading('#loadingBar');
																		if (result.data.status == "success") {
																			$rootScope.showAlertDialog(result.data.message);
																			$scope.cancelSKUTagAssign();

																		} else if (result.data.status == "error") {
																			$rootScope.showAlertDialog(result.data.message);
																		}

																	},
																	function(error) {
																		$rootScope
																				.hideloading('#loadingBar');
																		$rootScope.fnHttpError(error);
																	});
													
														
													}else if ((result.data.tagStatus == "Assigned")
															|| (result.data.tagStatus == "assigned")) {
														$rootScope.hideloading('#loadingBar');
														$rootScope.showAlertDialog(result.data.message) ;
													}else {
														$rootScope.hideloading('#loadingBar');
														$rootScope.showAlertDialog(result.data.message) ;
													}
												}else if (result.data.status == "error") {
													$rootScope.showAlertDialog(result.data.message) ;
												}
						
											},
											function(error) {
												$rootScope
														.hideloading('#loadingBar');
												$rootScope.fnHttpError(error);
											});
						
						
						
					}
					
					$scope.cancelReturnSKUTag = function() {
						 $('#checktagIdSelect').val("");
						$scope.tagSKUReturnScreen = false;
						$scope.fetchScreen = true;
						
					}
					$scope.returnSKUTag = function() {
						
						

						/*
						for(var i=0; i < $scope.tagDetail.length ; i++){
							if($scope.tagDetail[i].status == "Assigned"){
								if($scope.tagDetail[i].type == "WareHouse"){
							
								if($scope.tagReturnTagID == $scope.tagDetail[i].tagId){
									var tagReturnTagMACID = $scope.tagDetail[i].macId;
									var tagReturnType = $scope.tagDetail[i].type;
									break;
								}
								}	
							}
						}*/
						
						
						var returnTagParameter = {
							"macId" : $scope.tagReturnMacID,
							"peopleId" : $scope.tagReturnSKUNo,
							 "type" : $scope.tagReturnType
						};
						
						if ($rootScope.checkNetconnection() == false) {
							$rootScope.alertDialogChecknet();
							return;
						}
						$rootScope.showloading('#loadingBar');
						$scope.promise = $http
								.post('./returnTag', returnTagParameter)
								.then(
										function(result) {
											$rootScope
													.hideloading('#loadingBar');
											if (result.data.status == "success") {

												$rootScope.showAlertDialog(result.data.message) ;
												$scope.cancelReturnSKUTag();
											} else if (result.data.status == "error") {
												$rootScope.showAlertDialog(result.data.message) ;
											}

										},
										function(error) {
											$rootScope
													.hideloading('#loadingBar');
											$rootScope.fnHttpError(error);
										});

					}
					
					// fetch button function triggered when enter key pressed
					$("#checktagIdSelect").keyup(function(event) {
					    if (event.keyCode === 13) {
					        $("#id_fetchButtonWarehouseTag").click();
					    }
					});

					$scope.fetchTag = function() {
						
						
						
						var tagIdWM = document.getElementById("checktagIdSelect");
						if (tagIdWM.value.length == 0) {
							// $rootScope.hideloading('#loadingBar');
							$scope.fetchTagError = true;
							$scope.fetchTag_error_msg = "Please enter Tag Id";
							$timeout(function() {
								$scope.fetchTagError = false;
							}, 2000);
							return;
						}
						
						
						// var fetchTag = document.getElementById("checktagIdSelect"); 
						 if ( $scope.fieldName.checktagId == undefined || $scope.fieldName.checktagId == "" || $scope.fieldName.checktagId == null ) {
							 $rootScope.hideloading('#loadingBar');
							 $scope.fetchTagError = true;
							 $scope.fetchTag_error_msg = "Please enter Tag ID ";
							 $timeout(function() { 
								 $scope.fetchTagError = false;
						  }, 2000);
						  
						  return; 
						  }
						
						var checkTagParameter = {
							"tagId" :  $scope.fieldName.checktagId
						};
						 if ($rootScope.checkNetconnection() == false) {
							 $rootScope.alertDialogChecknet();
							 return;
						 }
						 $rootScope.showloading('#loadingBar');
						$scope.promise = $http
								.post('./checkTag', checkTagParameter)
								.then(
										function(result) {

											$rootScope
													.hideloading('#loadingBar');
											// to change the header
											// names
											
											
											if (result.data.status == "success") {
												if (result.data.tagStatus == "free") {
													$rootScope.showAlertDialog(result.data.message) ;
												} else if ((result.data.tagStatus == "Assigned")
														|| (result.data.tagStatus == "assigned")) {
													if (result.data.tag.type == "WareHouse"	) {
														var resData = JSON.stringify(result.data);
														result.data = JSON.parse(resData); 
														//console.log(result.data.warehouseDetails);
														result.data.warehouseDetails = JSON.parse(JSON.stringify(result.data.warehouseDetails).split('"issue_date":').join('"Issue Date":'));
														result.data.warehouseDetails = JSON.parse(JSON.stringify(result.data.warehouseDetails).split('"dispatch_date":').join('"Dispatch Date":'));
														result.data.warehouseDetails = JSON.parse(JSON.stringify(result.data.warehouseDetails).split('"arrival_date":').join('"Arrival Date":'));
														result.data.warehouseDetails = JSON.parse(JSON.stringify(result.data.warehouseDetails).split('"created_date":').join('"Created Date":'));													
														
														result.data.warehouseDetails = JSON.parse(JSON.stringify(result.data.warehouseDetails).split('"manufacture_date":').join('"Manufacture Date":'));													
														result.data.warehouseDetails = JSON.parse(JSON.stringify(result.data.warehouseDetails).split('"modified_date":').join('"Modified Date":'));
														result.data.warehouseDetails = JSON.parse(JSON.stringify(result.data.warehouseDetails).split('"expiry_date":').join('"Expiry Date":'));
														result.data.warehouseDetails = JSON.parse(JSON.stringify(result.data.warehouseDetails).split('"issue_date":').join('"Issue Date":'));
														result.data.warehouseDetails = JSON.parse(JSON.stringify(result.data.warehouseDetails).split('"arrival_date":').join('"Arrival Date":'));
														
													
														
														if(result.data.warehouseDetails != undefined && result.data.warehouseDetails != null ){
															if(result.data.warehouseDetails["Issue Date"] != undefined && result.data.warehouseDetails["Issue Date"] != null &&  result.data.warehouseDetails["Issue Date"] != "null")
																result.data.warehouseDetails["Issue Date"] = moment(result.data.warehouseDetails["Issue Date"]).format("MMM DD, YYYY");
															else 
																result.data.warehouseDetails["Issue Date"] = "";
															if(result.data.warehouseDetails["Dispatch Date"] != undefined && result.data.warehouseDetails["Dispatch Date"] != null &&  result.data.warehouseDetails["Dispatch Date"] != "null")
																result.data.warehouseDetails["Dispatch Date"] = moment(result.data.warehouseDetails["Dispatch Date"]).format("MMM DD, YYYY");
															else 
																result.data.warehouseDetails["Dispatch Date"] = "";
															if(result.data.warehouseDetails["Arrival Date"] != undefined && result.data.warehouseDetails["Arrival Date"] != null &&  result.data.warehouseDetails["Arrival Date"] != "null")
																result.data.warehouseDetails["Arrival Date"] = moment(result.data.warehouseDetails["Arrival Date"]).format("MMM DD, YYYY");
															else 
																result.data.warehouseDetails["Arrival Date"] = "";
															if(result.data.warehouseDetails["Created Date"] != undefined && result.data.warehouseDetails["Created Date"] != null &&  result.data.warehouseDetails["Created Date"] != "null")
																result.data.warehouseDetails["Created Date"] = moment(result.data.warehouseDetails["Created Date"]).format("MMM DD, YYYY");
															else 
																	result.data.warehouseDetails["Created Date"] = "";
															if(result.data.warehouseDetails["Manufacture Date"] != undefined && result.data.warehouseDetails["Manufacture Date"] != null &&  result.data.warehouseDetails["Manufacture Date"] != "null")
																result.data.warehouseDetails["Manufacture Date"] = moment(result.data.warehouseDetails["Manufacture Date"],"DD/MM/YYYY").format("MMM DD, YYYY");
															else 
																result.data.warehouseDetails["Manufacture Date"] = "";
															if(result.data.warehouseDetails["Date of Manufacture"] != undefined && result.data.warehouseDetails["Date of Manufacture"] != null &&  result.data.warehouseDetails["Date of Manufacture"] != "null")
																result.data.warehouseDetails["Date of Manufacture"] = moment(result.data.warehouseDetails["Date of Manufacture"],"DD/MM/YYYY").format("MMM DD, YYYY");
															else 
																result.data.warehouseDetails["Date of Manufacture"] = "";
															if(result.data.warehouseDetails["Modified Date"] != undefined && result.data.warehouseDetails["Modified Date"] != null &&  result.data.warehouseDetails["Modified Date"] != "null")
																result.data.warehouseDetails["Modified Date"] = moment(result.data.warehouseDetails["Modified Date"]).format("MMM DD, YYYY");
															else 
																result.data.warehouseDetails["Modified Date"] = "";
															if(result.data.warehouseDetails["Purchase Date"] != undefined && result.data.warehouseDetails["Purchase Date"] != null &&   result.data.warehouseDetails["Purchase Date"] != "null")
																result.data.warehouseDetails["Purchase Date"] = moment(result.data.warehouseDetails["Purchase Date"],"DD/MM/YYYY").format("MMM DD, YYYY");
															else 
																result.data.warehouseDetails["Purchase Date"] = "";
															if(result.data.warehouseDetails["Expiry Date"] != undefined && result.data.warehouseDetails["Expiry Date"] != null &&   result.data.warehouseDetails["Expiry Date"] != "null")
																result.data.warehouseDetails["Expiry Date"] = moment(result.data.warehouseDetails["Expiry Date"],"DD/MM/YYYY").format("MMM DD, YYYY");
															else 
																result.data.warehouseDetails["Expiry Date"] = "";
															
														}
														
														var varSKUIssueDate = "", varSKUExpiryDate = "";
														$scope.showReturnTagSKUExpiryDate = false;
														$scope.assignScreen = false;
														$scope.fetchScreen = false;
														$scope.tagSKUReturnScreen = true;
														$scope.tagReturnTagID = result.data.tag.tagId;
														$scope.tagReturnMacID = result.data.tag.macId;
														$scope.tagReturnType = result.data.tag.type;
														
														$scope.tagReturnSKUNo = result.data.tag.assignmentId;
														
														if (result.data.warehouseDetails['Issue Date'] != undefined
																&& result.data.warehouseDetails['Issue Date'] != null)
															varSKUIssueDate = moment(
																	result.data.warehouseDetails['Issue Date'])
																	.format(
																			"MMM DD, YYYY");
														
														$scope.tagReturnSKUNo = result.data.warehouseDetails['SKU No'];
														$scope.tagReturnBatchNo = result.data.warehouseDetails['Batch No'];
														$scope.tagReturnSKUDescription = result.data.warehouseDetails['Description'];
														
														$scope.tagReturnSKUIssueDate = varSKUIssueDate;
														
														$scope.tagReturnSKUExpiryDate = varSKUExpiryDate;
													} else {
														$rootScope.showAlertDialog(result.data.message) ;
													}
													
													
													// code for dynamic table in return screen 

													
													$('#idTableViewRetScreen').empty();
													  var ViewRetScreenTableDiv = document.getElementById("idTableViewRetScreen");
													  ViewRetScreenTableDiv.innerHTML = '';
													  
													  var ViewRetScreentable = document.createElement('TABLE'); 
													  ViewRetScreentable.className += "table table-bordered tbodyWidth ";
													  
													  var tableBody = document.createElement('TBODY');
													 
													  ViewRetScreentable.appendChild(tableBody);
													  $scope.materialAssignedDataObj = JSON.parse(JSON.stringify(result.data.warehouseDetails));
														  var keyNames =  Object.keys($scope.materialAssignedDataObj);
														  
														  var removeHeaderItems = ["mac_id", "zone_id", "tag_id", "status", "SKU No" ]
															for(var i = 0; i < removeHeaderItems.length; i++){						
																const index = keyNames.indexOf(removeHeaderItems[i]);
																if (index > -1) {
																	keyNames.splice(index, 1);
																}							
																}
													  
													  for (var i = 0; i < keyNames.length; i++) {
													    var tr = document.createElement('TR');
													    tableBody.appendChild(tr);

													    for (var j = 0; j < 2; j++) {
													      var td = document.createElement('TD');
													      td.width = '300';
													      if(j==0)
													    	  td.appendChild(document.createTextNode(keyNames[i]+":"));
													      else
													      {
													    	  if($scope.materialAssignedDataObj[''+keyNames[i]]==null||$scope.materialAssignedDataObj[''+keyNames[i]]==''||
															    			  $scope.materialAssignedDataObj[''+keyNames[i]]=='null')
															    		  td.appendChild(document.createTextNode(''));
															    	  else						    		  
															    		  td.appendChild(document.createTextNode($scope.materialAssignedDataObj[''+keyNames[i]]));
															      	
													    	}
													      tr.appendChild(td);
													    }
													  }
													  ViewRetScreenTableDiv.appendChild(ViewRetScreentable);
													
													
													//code ends here dynamic table

												} else {
													$rootScope.showAlertDialog(result.data.message) ;
												}

											} else if (result.data.status == "error") {
												$rootScope.showAlertDialog(result.data.message) ;
											}

										},
										function(error) {
											$rootScope
													.hideloading('#loadingBar');
											$rootScope.fnHttpError(error);
										});

					}
					// allowable zone list API
					$scope.getZoneDetails = function() {
						if (!$rootScope.checkNetconnection()) { 				
						 	//$rootScope.alertDialogChecknet();
							return; 
							}
						$rootScope.showloading('#loadingBar');	
					$http({
						method : 'GET',
						url : './zone',

					})
							.then(
									function(response) {
										$rootScope.hideloading('#loadingBar');	
										if (response != null
												&& response.data != null
												&& response.data.length != 0
												&& response.data != "BAD_REQUEST"
												&& response.data.data != undefined
												&& response.data.data.length != 0) {

											//$scope.locationList = [];
											$scope.zoneDetails = [];

											
											for (var i = 0; i < response.data.data.length; i++) {
												if (response.data.data[i].active == true) {
													if(response.data.data[i].zoneType == "Trackingzone"){
													$scope.zoneDetails.push ({
														'zoneName' : response.data.data[i].name,
														'zoneId' : response.data.data[i].id
														});
												}
													
												}
											}	
											
											if ($scope.zoneDetails.length > 0)
												$scope.skuZoneName = $scope.zoneDetails[0].zoneName;
											//$scope.fnCreateTreeWithZoneNme();
										} else {
											toastr.error('No zones are available',"EI4.0")	
										}
									},function error(response) {
										$rootScope.hideloading('#loadingBar');			
										$rootScope.fnHttpError(response);
										});
					}
					
				
					if ($rootScope.LocateItem == true)
						{
							if ($scope.user.capabilityDetails.HMDS == 1) {
								$scope.displayMaterialViewScreen();
							}
							$rootScope.LocateItem = false;
						}
					// to display enabled sub menus
					else if ($scope.user.capabilityDetails != undefined
							&& $scope.user.capabilityDetails != null) {
						
						if ($scope.user.capabilityDetails.HMRP != 0) {
							$scope.displayReceiptScreen();
						}
						if ($scope.user.capabilityDetails.HMRP != 1) {
							$scope.displayDispatchMaterialScreen();
						}
						if ($scope.user.capabilityDetails.HMRP != 1
								&& $scope.user.capabilityDetails.HMDS != 1) {
							$scope.displayMaterialViewScreen();
						}
						if ($scope.user.capabilityDetails.HMRP != 1
								&& $scope.user.capabilityDetails.HMDS != 1
								&& $scope.user.capabilityDetails.HMVA != 1) {
							$scope.displayDispatchViewScreen();
						}

					}
					// multisearch code starts
					$scope.multipleSearch = function() {
						$scope.showMultipleSearch = true;
						$scope.searchButton = false
						$scope.firstMultiSearchDropDown = "All";
						$scope.secondMultiSearchDropDown = "All";
						$scope.varANDORDropDown = "AND";
						$scope.firstSearchText = "";
						$scope.secondSearchText = "";
					}
					$scope.closeMultiSearchView = function() {
						$scope.showMultipleSearch = false;
						$scope.searchButton = true
						
						// for pagination
						$scope.fntoCreateTableID();
						$scope.tagDataFilterList = $scope.tagData;
						$scope.totalItems = $scope.tagDataFilterList.length;
						$scope.currentPage = 1;
						$scope.getTotalPages();

						$scope.getAssignedTagIds($scope.tagDataFilterList);
						if ($scope.tagData.length == 0
								|| $scope.tagDataFilterList.length == 0) {
							$scope.tagNoData = true;
							$scope.no_data_found = "No data found.";

						} else {
							$scope.tagNoData = false;

							if ($scope.tagDataFilterList.length > $scope.numPerPage) {
								$scope.tagViewData =  $scope.tagDataFilterList.slice(0, $scope.numPerPage);
								$scope.PaginationTab = true;
							} else {
								$scope.tagViewData =  $scope.tagDataFilterList;
								$scope.PaginationTab = false;
							}
						}
						for (var k = 0; k < $scope.tagViewData.length; k++) {
							  $scope.addMaterialTableRow(k); 
						}
						
					}
					
					$scope.fnfirstSearchANDData = function() {
						
						$scope.firstSearchANDDataArray = [];						
						
						for (var i = 0; i < $scope.tagData.length; i++) {
							if ($scope.firstMultiSearchDropDown == "All") {
								if ($scope.firstSearchText == ""
									|| $scope.firstSearchText == undefined) {
								
								$scope.firstSearchANDDataArray
										.push($scope.tagData[i]);
							} else {
								var obj = {};
								obj = $scope.tagData[i];
								 for(var x = 0; x < $scope.uniqueheaderList.length; x++){								
									 var tableText = JSON.stringify($scope.tagData[i][$scope.uniqueheaderList[x].actualHeader]);								
								
								if ((tableText != undefined && tableText != null ? tableText : "").toLowerCase()
												.includes($scope.firstSearchText.toLowerCase()))								
									{
										$scope.firstSearchANDDataArray.push($scope.tagData[i]);										
										break;									 
									}	
							 }
								
						}
							}
							// to check other than all
							else {
								var firstDropDownValue = $scope.firstMultiSearchDropDown;
								 for(var x = 0; x < $scope.uniqueheaderList.length; x++){			
									 if( $scope.uniqueheaderList[x].displayHeader == firstDropDownValue )
										var actualHeaderfirstDropDownValue = $scope.uniqueheaderList[x].actualHeader;
								 }
									if ($scope.firstSearchText == "" || $scope.firstSearchText == undefined) 
										$scope.firstSearchANDDataArray.push($scope.tagData[i]);
									else{
									var obj = $scope.tagData[i];
									
									// var tableText =
									// JSON.stringify($scope.tagData[i][actualHeaderfirstDropDownValue]);
									var tableText = $scope.tagData[i][actualHeaderfirstDropDownValue];	
									if((tableText != undefined && tableText != null ? tableText : "").toLowerCase()
													.includes($scope.firstSearchText.toLowerCase()))								
										{
											$scope.firstSearchANDDataArray.push($scope.tagData[i]);										
											//break;									 
										}		
								

								} 

							}
							
					} 
						
					}

					$scope.multiSearchChanged = function() {
						
						$scope.fnfirstSearchANDData();
					// console.log($scope.firstSearchANDDataArray);

						if ($scope.varANDORDropDown == "AND") {
							$scope.disableSecondFilterText = false;
							$scope.disableSecondFilterDropDown = false;
							$scope.tagViewData = [];
							$scope.tagDataFilterList = [];

							for (var i = 0; i < $scope.firstSearchANDDataArray.length; i++) {
								if ($scope.secondMultiSearchDropDown == "All") {

									if ($scope.secondSearchText == ""
											|| $scope.secondSearchText == undefined) {
										$scope.tagDataFilterList
												.push($scope.firstSearchANDDataArray[i]);
									} else {

										var obj = $scope.tagData[i];
										 for(var x = 0; x < $scope.uniqueheaderList.length; x++){								
											 var tableText = JSON.stringify($scope.firstSearchANDDataArray[i][$scope.uniqueheaderList[x].actualHeader]);								
										
										if ((tableText != undefined && tableText != null ? tableText : "").toLowerCase()
														.includes($scope.secondSearchText.toLowerCase()))								
											{													
												$scope.tagDataFilterList.push($scope.firstSearchANDDataArray[i]);
												break;									 
											}	
									 }	
									}
								} else {

									var secondDropDownValue = $scope.secondMultiSearchDropDown;	
									for(var x = 0; x < $scope.uniqueheaderList.length; x++){			
										 if( $scope.uniqueheaderList[x].displayHeader == secondDropDownValue )
											var actualHeadersecondDropDownValue = $scope.uniqueheaderList[x].actualHeader;
									 }
										if ($scope.secondSearchText == "" || $scope.secondSearchText == undefined) 
											
										$scope.tagDataFilterList.push($scope.firstSearchANDDataArray[i]);
										else{
										var obj = $scope.tagData[i];
										
										// var tableText =
										// JSON.stringify($scope.tagData[i][actualHeadersecondDropDownValue]);
										var tableText = $scope.tagData[i][actualHeadersecondDropDownValue];	
										if((tableText != undefined && tableText != null ? tableText : "").toLowerCase()
														.includes($scope.secondSearchText.toLowerCase()))								
											{
											$scope.tagDataFilterList.push($scope.firstSearchANDDataArray[i]);									
												// break;
											}
									} 

								}
							}
							
							
							$scope.fntoCreateTableID();
							
							$scope.totalItems = $scope.tagDataFilterList.length;
							$scope.currentPage = 1;
							$scope.getTotalPages();
						 	$scope.getAssignedTagIds($scope.tagDataFilterList);

							if ($scope.tagDataFilterList.length == 0) {
								$scope.tagNoData = true;
								$scope.no_data_found = "No data found.";

							} else {
								$scope.tagNoData = false;

								
								if ($scope.tagDataFilterList.length > $scope.numPerPage) {
									$scope.tagViewData = $scope.tagDataFilterList
											.slice(0, $scope.numPerPage);
									$scope.PaginationTab = true;
								} else {
									$scope.tagViewData = $scope.tagDataFilterList;
									$scope.PaginationTab = false;
								}
							}
							for (var k = 0; k < $scope.tagViewData.length; k++) {
								  $scope.addMaterialTableRow(k); 
								  }	
							
						} else if ($scope.varANDORDropDown == "OR") {

							if ($scope.firstMultiSearchDropDown == "All"
									&& ($scope.firstSearchText == "" || $scope.firstSearchText == undefined)) {
								$scope.disableSecondFilterText = true;
								$scope.disableSecondFilterDropDown = true;
								$scope.secondMultiSearchDropDown = "All"
								$scope.secondSearchText = ""

							} else {
								$scope.disableSecondFilterText = false;
								$scope.disableSecondFilterDropDown = false;
							}
							
							$scope.tagViewData = []
							$scope.tagDataFilterList = [];
							$scope.secondSearchORDataArray = [];
							for (var i = 0; i < $scope.tagData.length; i++) {
								if ($scope.secondMultiSearchDropDown == "All") {
									if ($scope.secondSearchText == ""
											|| $scope.secondSearchText == undefined) {
										$scope.secondSearchORDataArray
												.push($scope.tagData[i]);

									} else {
										var obj = $scope.tagData[i];
										 for(var x = 0; x < $scope.uniqueheaderList.length; x++){								
											 var tableText = JSON.stringify($scope.tagData[i][$scope.uniqueheaderList[x].actualHeader]);								
										
										if ((tableText != undefined && tableText != null ? tableText : "").toLowerCase()
														.includes($scope.secondSearchText.toLowerCase()))								
											{													
											$scope.secondSearchORDataArray.push($scope.tagData[i]);
												break;									 
											}	
									 }
									}
								} else {

									// to check other than all in second search
									var secondDropDownValue = $scope.secondMultiSearchDropDown;	
									for(var x = 0; x < $scope.uniqueheaderList.length; x++){			
										 if( $scope.uniqueheaderList[x].displayHeader == secondDropDownValue )
											var actualHeadersecondDropDownValue = $scope.uniqueheaderList[x].actualHeader;
									 }
										if ($scope.secondSearchText == "" || $scope.secondSearchText == undefined) 
											
										$scope.secondSearchORDataArray.push($scope.tagData[i]);
										else{
										var obj = $scope.tagData[i];
										
										// var tableText =
										// JSON.stringify($scope.tagData[i][actualHeadersecondDropDownValue]);
										var tableText = $scope.tagData[i][actualHeadersecondDropDownValue];	
										if((tableText != undefined && tableText != null ? tableText : "").toLowerCase()
														.includes($scope.secondSearchText.toLowerCase()))								
											{
											$scope.secondSearchORDataArray.push($scope.tagData[i]);									
												// break;
											}
									}

								}
							}
							// to concat first and second search arrays
							for (var i = 0; i < $scope.firstSearchANDDataArray.length; i++) {
								if (!$scope.secondSearchORDataArray
										.includes($scope.firstSearchANDDataArray[i]))
									$scope.secondSearchORDataArray
											.push($scope.firstSearchANDDataArray[i]);
							}
							$scope.tagDataFilterList = $scope.secondSearchORDataArray;
							// $scope.tagDataFilterList =
							// ($scope.firstSearchANDDataArray).concat($scope.secondSearchORDataArray);

							// for pagination
							$scope.fntoCreateTableID();
							
							$scope.totalItems = $scope.tagDataFilterList.length;
							$scope.currentPage = 1;
							$scope.getTotalPages();

							$scope.getAssignedTagIds($scope.tagDataFilterList);
							if ($scope.tagData.length == 0
									|| $scope.tagDataFilterList.length == 0) {
								$scope.tagNoData = true;
								$scope.no_data_found = "No data found.";

							} else {
								$scope.tagNoData = false;

								if ($scope.tagDataFilterList.length > $scope.numPerPage) {
									$scope.tagViewData =  $scope.tagDataFilterList.slice(0, $scope.numPerPage);
									$scope.PaginationTab = true;
								} else {
									$scope.tagViewData =  $scope.tagDataFilterList;
									$scope.PaginationTab = false;
								}
							}
							for (var k = 0; k < $scope.tagViewData.length; k++) {
								  $scope.addMaterialTableRow(k); 
							}
						}
					

					}
					$scope.multiSelectSetting = function() {
						if($scope.warehouseTabName == "Material"){
							$('#idStockMultiselectmodal').modal('show');						
							$scope.renameScreenStock = false;
							assign_btn_action('multiselectlistStock');		
							
						}else if($scope.warehouseTabName == "Dispatch"){
							$('#idDispatchMultiselectmodal').modal('show');
							$scope.renameScreenDispatch = false;
							assign_btn_action('multiselectlistDispatch');	
						}	
					}
					
					/*
					 * $(document).ready(function(){
					 * assign_btn_action('multiselectlistStock');
					 * 
					 * });
					 * 
					 * $(document).ready(function(){
					 * assign_btn_action('multiselectlistDispatch');
					 * 
					 * });
					 */
					
					function move_remove_all(where,list_id)
					{
						if($scope.warehouseTabName == "Material"){
							
							if (where == 'right')
							{
								$('#' + list_id + ' #multivalfromStock').find('option').each(function(){
									var optionval = '<option value="'+$(this).html()+'">'+$(this).html()+'</option>';
									$('#' + list_id).find('#multivaltoStock').append(optionval);
								});
								$('#' + list_id + ' #multivalfromStock').html('');
							}
							else if (where == 'left')
							{
								$('#' + list_id + ' #multivaltoStock').find('option').each(function(){
									var optionval = '<option value="'+$(this).html()+'">'+$(this).html()+'</option>';
									$('#' + list_id).find('#multivalfromStock').append(optionval);
								});
								$('#' + list_id + ' #multivaltoStock').html('');
							}	
						}else if($scope.warehouseTabName == "Dispatch"){
							if (where == 'right')
							{
								$('#' + list_id + ' #multivalfromDispatch').find('option').each(function(){
									var optionval = '<option value="'+$(this).html()+'">'+$(this).html()+'</option>';
									$('#' + list_id).find('#multivaltoDispatch').append(optionval);
								});
								$('#' + list_id + ' #multivalfromDispatch').html('');
							}
							else if (where == 'left')
							{
								$('#' + list_id + ' #multivaltoDispatch').find('option').each(function(){
									var optionval = '<option value="'+$(this).html()+'">'+$(this).html()+'</option>';
									$('#' + list_id).find('#multivalfromDispatch').append(optionval);
								});
								$('#' + list_id + ' #multivaltoDispatch').html('');
							}
						}	
						
						
					}
					function top_bottom(where,list_id)
					{
						if($scope.warehouseTabName == "Material"){
								var selected = $('#' + list_id + ' #multivaltoStock').val();
								var appendvalue = '';
								for (i=0;i<selected.length;i++)
								{
									appendvalue += '<option value="'+selected[i]+'">'+selected[i]+'</option>';
									$('#' + list_id + ' #multivaltoStock option[value="'+selected[i]+'"]').remove();
								}
								if (where == 'top')
								{
									$('#' + list_id + ' #multivaltoStock').prepend(appendvalue);
								}
								else if (where == 'bottom')
								{
									$('#' + list_id + ' #multivaltoStock').append(appendvalue);
								}
							
						}else if($scope.warehouseTabName == "Dispatch"){
								var selected = $('#' + list_id + ' #multivaltoDispatch').val();
								var appendvalue = '';
								for (i=0;i<selected.length;i++)
								{
									appendvalue += '<option value="'+selected[i]+'">'+selected[i]+'</option>';
									$('#' + list_id + ' #multivaltoDispatch option[value="'+selected[i]+'"]').remove();
								}
								if (where == 'top')
								{
									$('#' + list_id + ' #multivaltoDispatch').prepend(appendvalue);
								}
								else if (where == 'bottom')
								{
									$('#' + list_id + ' #multivaltoDispatch').append(appendvalue);
								}	
						}	
						
						 
					};
					function up_down(where,list_id)
					{
					if($scope.warehouseTabName == "Material"){
								var $op = $('#' + list_id + ' #multivaltoStock option:selected');
								if($op.length)
								{
									if(where == 'up')
									{
										$op.first().prev().before($op)
									}
									else if (where == 'down')
									{
										$op.last().next().after($op);
									}	
								}		
							
						}else if($scope.warehouseTabName == "Dispatch"){
								var $op = $('#' + list_id + ' #multivaltoDispatch option:selected');
								if($op.length)
								{
									if(where == 'up')
									{
										$op.first().prev().before($op)
									}
									else if (where == 'down')
									{
										$op.last().next().after($op);
									}	
								}	
						}
					}
					function move_remove(where,list_id)
					{
						if($scope.warehouseTabName == "Material"){
							if (where == 'right')
							{
								var selected = $('#' + list_id).find('#multivalfromStock').val();
								var appendvalue = ''
								for (i=0;i<selected.length;i++)
								{
									appendvalue += '<option value="'+selected[i]+'">'+selected[i]+'</option>';
									$('#' + list_id).find('#multivalfromStock option[value="'+selected[i]+'"]').remove();
								}
								$('#' + list_id).find('#multivaltoStock').append(appendvalue);
							}
							else if (where == 'left')
							{
								var selected = $('#' + list_id).find('#multivaltoStock').val();
								var appendvalue = ''
								for (i=0;i<selected.length;i++)
								{
									appendvalue += '<option value="'+selected[i]+'">'+selected[i]+'</option>';
									$('#' + list_id).find('#multivaltoStock option[value="'+selected[i]+'"]').remove();
								}
								$('#' + list_id).find('#multivalfromStock').append(appendvalue);			
							}	
							
						}else if($scope.warehouseTabName == "Dispatch"){
							if (where == 'right')
							{
								var selected = $('#' + list_id).find('#multivalfromDispatch').val();
								var appendvalue = ''
								for (i=0;i<selected.length;i++)
								{
									appendvalue += '<option value="'+selected[i]+'">'+selected[i]+'</option>';
									$('#' + list_id).find('#multivalfromDispatch option[value="'+selected[i]+'"]').remove();
								}
								$('#' + list_id).find('#multivaltoDispatch').append(appendvalue);
							}
							else if (where == 'left')
							{
								var selected = $('#' + list_id).find('#multivaltoDispatch').val();
								var appendvalue = ''
								for (i=0;i<selected.length;i++)
								{
									appendvalue += '<option value="'+selected[i]+'">'+selected[i]+'</option>';
									$('#' + list_id).find('#multivaltoDispatch option[value="'+selected[i]+'"]').remove();
								}
								$('#' + list_id).find('#multivalfromDispatch').append(appendvalue);			
							}
						}	
						
						
					}
					function assign_btn_action(list_id)
					{
						if($scope.warehouseTabName == "Material"){
							$("#" + list_id).find("#top_btn")[ 0 ].onclick = function() {
								top_bottom('top',list_id);							
							};
							$("#" + list_id).find("#bottom_btn")[ 0 ].onclick = function() {
								top_bottom('bottom',list_id);							
							};
							$("#" + list_id).find("#up_btn")[ 0 ].onclick = function() {
								up_down('up',list_id);							
							};
							$("#" + list_id).find("#down_btn")[ 0 ].onclick = function() {
								up_down('down',list_id);							
							};
							$("#" + list_id).find("#move_btn")[ 0 ].onclick = function() {
								move_remove('right',list_id);							
							};
							$("#" + list_id).find("#remove_btn")[ 0 ].onclick = function() {
								move_remove('left',list_id);							
							};
							$("#" + list_id).find("#move_all_btn")[ 0 ].onclick = function() {
								move_remove_all('right',list_id);							
							};
							$("#" + list_id).find("#remove_all_btn")[ 0 ].onclick = function() {
								move_remove_all('left',list_id);							
							};
							
						}else if($scope.warehouseTabName == "Dispatch"){
							$("#" + list_id).find("#top_btn_dispatch")[ 0 ].onclick = function() {
								top_bottom('top',list_id);							
							};
							$("#" + list_id).find("#bottom_btn_dispatch")[ 0 ].onclick = function() {
								top_bottom('bottom',list_id);							
							};
							$("#" + list_id).find("#up_btn_dispatch")[ 0 ].onclick = function() {
								up_down('up',list_id);							
							};
							$("#" + list_id).find("#down_btn_dispatch")[ 0 ].onclick = function() {
								up_down('down',list_id);							
							};
							$("#" + list_id).find("#move_btn_dispatch")[ 0 ].onclick = function() {
								move_remove('right',list_id);							
							};
							$("#" + list_id).find("#remove_btn_dispatch")[ 0 ].onclick = function() {
								move_remove('left',list_id);							
							};
							$("#" + list_id).find("#move_all_btn_dispatch")[ 0 ].onclick = function() {
								move_remove_all('right',list_id);							
							};
							$("#" + list_id).find("#remove_all_btn_dispatch")[ 0 ].onclick = function() {
								move_remove_all('left',list_id);							
							};
						}	
						
						
						
					}
					function getvalue(list_id)
					{
						var values = '';
						$("#" + list_id + " #multivaltoStock").find("option").each(function(){
							values += $(this).html() + ',';
						});
						values = values.substr(0,values.lastIndexOf(','));	
						return values;
						
					}
					//rename code for stock multi select
					$('#multivaltoStock').on('click', 'option', function() {
						// $('#multivaltoStock').html($(this).text());
						 $scope.selectedDisplayValuestock = ""
						 $scope.selectedDisplayValuestock = $('#multiselectlistStock').find('#multivaltoStock').val();
						  if($scope.selectedDisplayValuestock.length == 1){	
							  $("#id_RenameTextStock").val("");
							  $scope.renameScreenStock = true; 
							  $scope.$apply();
						  }else{
							  $scope.renameScreenStock = false;    
							  $scope.$apply();
						  }						
						 
						});
					$('#multivalfromStock').on('click', 'option', function() {						
							  $scope.renameScreenStock = false;    
							  $scope.$apply();						
						});
					  $scope.fnRenameTextStock = function() {						  
							var appendvaluestock = ''							
								appendvaluestock += '<option value="'+$scope.renameTextStock+'">'+$scope.renameTextStock+'</option>';
								$('#multiselectlistStock').find('#multivaltoStock option[value="'+ $scope.selectedDisplayValuestock+'"]').remove();						 
							    $('#multiselectlistStock').find('#multivaltoStock').append(appendvaluestock);	
							    
							    
							    for(var i=0; i<$scope.verificationTableStock.length; i++){
							    	if($scope.verificationTableStock[i].displayHeader == $scope.selectedDisplayValuestock[0])
							    		$scope.verificationTableStock[i].displayHeader = $scope.renameTextStock;
							    }
							    $rootScope.renamedFieldListStock.push($scope.selectedDisplayValuestock[0]);	
							    //console.log($scope.verificationTableStock)
							    //$scope.renamedFieldListStock.sort();
							    $scope.renameScreenStock = false;  
					  }
					  
					  
					//rename code for dispatch multi select
						$('#multivaltoDispatch').on('click', 'option', function() {
							// $('#multivaltoStock').html($(this).text());
							 $scope.selectedDisplayValueDispatch = ""
							 $scope.selectedDisplayValueDispatch = $('#multiselectlistDispatch').find('#multivaltoDispatch').val();
							  if($scope.selectedDisplayValueDispatch.length == 1){	
								  $("#id_RenameTextDispatch").val("");
								  $scope.renameScreenDispatch = true; 
								  $scope.$apply();
							  }else{
								  $scope.renameScreenDispatch = false;    
								  $scope.$apply();
							  }						
							 
							});
						$('#multivalfromDispatch').on('click', 'option', function() {						
								  $scope.renameScreenDispatch = false;    
								  $scope.$apply();						
							});
						  $scope.fnRenameTextDispatch = function() {						  
								var appendvalue = ''							
									appendvalue += '<option value="'+$scope.renameTextDispatch+'">'+$scope.renameTextDispatch+'</option>';
									$('#multiselectlistDispatch').find('#multivaltoDispatch option[value="'+ $scope.selectedDisplayValueDispatch+'"]').remove();						 
								    $('#multiselectlistDispatch').find('#multivaltoDispatch').append(appendvalue);	
								    
								    
								    for(var i=0; i<$scope.verificationTableDispatch.length; i++){
								    	if($scope.verificationTableDispatch[i].displayHeader == $scope.selectedDisplayValueDispatch[0])
								    		$scope.verificationTableDispatch[i].displayHeader = $scope.renameTextDispatch;
								    }
								    $rootScope.renamedFieldListDispatch.push($scope.selectedDisplayValueDispatch[0]);
								    //console.log($scope.verificationTableDispatch)
								    //$scope.verificationTableDispatch.sort();
								    $scope.renameScreenDispatch = false;  
						  }
					  
					  
					  /*
						 * $scope.getTagData = function() {
						 * 
						 * $rootScope.showloading('#loadingBar'); if
						 * ($rootScope.checkNetconnection() == false) {
						 * $rootScope.hideloading('#loadingBar'); $ .alert({
						 * title : $rootScope.MSGBOX_TITLE, content : 'Please
						 * check the Internet Connection', icon : 'fa
						 * fa-info-circle', }); return; } $http({ method :
						 * 'GET', url : './tag',
						 *  }) .then( function(response) { $rootScope
						 * .hideloading('#loadingBar'); if (response != null &&
						 * response.data != null && response.data.length != 0 &&
						 * response.data != "BAD_REQUEST" && response.data !=
						 * undefined && response.data.length != 0) {
						 * 
						 * $scope.tagDetail =[]; response.data
						 * .sort(sortAlphaNum); for (var i = 0; i <
						 * response.data.length; i++) { var fromTime = "",
						 * toTime = "", assignmentDate = "";
						 * 
						 * if (response.data[i].fromTime != null &&
						 * response.data[i].fromTime != "" &&
						 * response.data[i].fromTime != undefined) { fromTime =
						 * response.data[i].fromTime; } if
						 * (response.data[i].toTime != null &&
						 * response.data[i].toTime != "" &&
						 * response.data[i].toTime != undefined) { toTime =
						 * response.data[i].toTime; } if
						 * (response.data[i].createdDate != null &&
						 * response.data[i].createdDate != "" &&
						 * response.data[i].createdDate != undefined) {
						 * response.data[i].createdDate =
						 * response.data[i].createdDate .substring( 0,
						 * response.data[i].createdDate.length - 9); var
						 * createdDate = moment( response.data[i].createdDate)
						 * .format( "MMM DD, YYYY"); } if
						 * (response.data[i].modifiedDate != null &&
						 * response.data[i].modifiedDate != "" &&
						 * response.data[i].modifiedDate != undefined) {
						 * response.data[i].modifiedDate =
						 * response.data[i].modifiedDate .substring( 0,
						 * response.data[i].modifiedDate.length - 9); var
						 * modifiedDate = moment( response.data[i].modifiedDate)
						 * .format( "MMM DD, YYYY"); } if
						 * (response.data[i].assignmentDate != null &&
						 * response.data[i].assignmentDate != "" &&
						 * response.data[i].assignmentDate != undefined) {
						 * response.data[i].assignmentDate =
						 * response.data[i].assignmentDate .substring( 0,
						 * response.data[i].assignmentDate.length - 9);
						 * assignmentDate = moment(
						 * response.data[i].assignmentDate) .format( "MMM DD,
						 * YYYY"); } var tagType = response.data[i].type if
						 * (response.data[i].type == "Asset Chrono" ||
						 * response.data[i].type == "Asset NonChrono" ||
						 * response.data[i].type == "Materials") tagType =
						 * response.data[i].subType
						 * 
						 * var skuTagType = response.data[i].type; var tagStatus =
						 * response.data[i].status; if (skuTagType ==
						 * "WareHouse"|| tagStatus == "Free" || (tagStatus ==
						 * "Lost" && (skuTagType == "WareHouse" ))) {
						 * $scope.tagDetail .push({ 'tagId' :
						 * response.data[i].tagId, 'groupId' :
						 * response.data[i].groupId, 'macId' :
						 * response.data[i].macId, 'manufacturer' :
						 * response.data[i].manufacturer, 'status' :
						 * response.data[i].status, 'assignmentId' :
						 * response.data[i].assignmentId, 'assignmentDate' :
						 * assignmentDate, 'fromTime' : fromTime, 'toTime' :
						 * toTime, 'createdDate' : createdDate, 'modifiedDate' :
						 * modifiedDate, 'major' : response.data[i].major,
						 * 'minor' : response.data[i].minor, 'type' : tagType,
						 * 'restart' : response.data[i].restart }); } }
						 * 
						 * if ($scope.tagDetail.length == 0) { $scope.tagNoData =
						 * true; return; } else { $scope.tagNoData = false; }
						 * 
						 * 
						 *  } else { $scope.tagNoData = true; } }, function
						 * error(response) { $rootScope
						 * .hideloading('#loadingBar'); if (error.data.status ==
						 * 403) { $rootScope .sessionTimeoutDialog(); } else {
						 * $rootScope .showAlertDialog(result.data.message);
						 *  }
						 * 
						 * });
						 *  }
						 */
					function skuDetailsTable() {
						
						
						  var SKUDetailsTableTableDiv = document.getElementById("SKUDetailsTable");
						  SKUDetailsTableTableDiv.innerHTML = '';
						  
						  var table = document.createElement('TABLE'); 
						  table.className += "table table-bordered tbodyWidth ";
						  
						  var tableBody = document.createElement('TBODY');
						 
						  table.appendChild(tableBody);
						  
							  var keyNames =  Object.keys($scope.skuDetail[0]);
						  
						  for (var i = 0; i < keyNames.length; i++) {
						    var tr = document.createElement('TR');
						    tableBody.appendChild(tr);

						    for (var j = 0; j < 2; j++) {
						      var td = document.createElement('TD');
						      td.width = '75';
						      if(j==0)
						    	  td.appendChild(document.createTextNode(keyNames[i]+":"));
						      else
						      {
						    	  if($scope.skuDetail[0][''+keyNames[i]]==null||$scope.skuDetail[0][''+keyNames[i]]==''||
								    			  $scope.skuDetail[0][''+keyNames[i]]=='null')
								    		  td.appendChild(document.createTextNode(''));
								    	  else						    		  
								    		  td.appendChild(document.createTextNode($scope.skuDetail[0][''+keyNames[i]]));
								      	
						    	}
						      tr.appendChild(td);
						    }
						  }
						  SKUDetailsTableTableDiv.appendChild(table);
						}

					
				});