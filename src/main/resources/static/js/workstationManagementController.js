app
		.controller(
				'workstationManagementController',
				function($scope, $http, $window, $rootScope, $location,
						$timeout) {
					$scope.user = JSON.parse($window.sessionStorage
							.getItem("user"));
					if ($scope.user == null) {
						$location.path('/login');
						return;
					}
					if ($scope.authen == undefined) {
						$scope.authen = {};
					}

					$scope.workstationEditScreen = true;
					$scope.workstationUpdateScreen = false;
					$scope.uniqueheaderList = [ "stationId", "stationType",
							"inbound", "outbound", "workArea", "storage",
							"inLimit", "outLimit" ];
					$scope.showSearchBar = true;
					$scope.wrkStnViewData = [];
					$scope.wrkStnData = [];
					$scope.wrkStnFilterList = [];
					$scope.zoneListActual = [];
					$scope.operatorList = [];
					var menudata = [];
					let selectedOperator = 0;
					$scope.selectedOperator = [];
					$scope.selectedOperatorEditWS = [];
					$scope.clearTreeFlag = false;
					$('.select2').select2();
					$scope.user = JSON.parse($window.sessionStorage
							.getItem("user"));
					// if ($scope.user != null) {
					// $rootScope.$broadcast('updatemenu', $scope.user.role);
					if ($scope.user.capabilityDetails != undefined
							&& $scope.user.capabilityDetails != null) {
						if ($scope.user.capabilityDetails.WWVA == 1) {
							$scope.showViewAllBtn = true;
						} else {
							$scope.showViewAllBtn = false;
						}
						if ($scope.user.capabilityDetails.WWAD == 1) {
							$scope.showAddBtn = true;
						} else {
							$scope.showAddBtn = false;
						}
						if ($scope.user.capabilityDetails.WWED == 1) {
							$scope.showEditBtn = true;
						} else {
							$scope.showEditBtn = false;
						}

					}

					// Search bar code

					$scope.searchOption = function() {
						$scope.filterOption();
					}

					$scope.filterOption = function() {

						$scope.wrkStnViewData = [];
						$scope.wrkStnFilterList = [];

						for (var i = 0; i < $scope.wrkStnData.length; i++) {

							if ($scope.searchText == '') {
								$scope.wrkStnFilterList
										.push($scope.wrkStnData[i]);
							} else {
								if (($scope.wrkStnData[i].stationId != undefined
										&& $scope.wrkStnData[i].stationId != null ? $scope.wrkStnData[i].stationId
										: "").toLowerCase().includes(
										$scope.searchText.toLowerCase())
										|| ($scope.wrkStnData[i].stationType != undefined
												&& $scope.wrkStnData[i].stationType != null ? $scope.wrkStnData[i].stationType
												: "").toLowerCase()
												.includes(
														$scope.searchText
																.toLowerCase())
										|| ($scope.wrkStnData[i].inbound != undefined
												&& $scope.wrkStnData[i].inbound != null ? $scope.wrkStnData[i].inbound
												: "").toLowerCase()
												.includes(
														$scope.searchText
																.toLowerCase())
										|| ($scope.wrkStnData[i].outbound != undefined
												&& $scope.wrkStnData[i].outbound != null ? $scope.wrkStnData[i].outbound
												: "").toLowerCase()
												.includes(
														$scope.searchText
																.toLowerCase())
										|| ($scope.wrkStnData[i].workArea != undefined
												&& $scope.wrkStnData[i].workArea != null ? $scope.wrkStnData[i].workArea
												: "").toLowerCase()
												.includes(
														$scope.searchText
																.toLowerCase())
										|| ($scope.wrkStnData[i].storage != undefined
												&& $scope.wrkStnData[i].storage != null ? $scope.wrkStnData[i].storage
												: "").toLowerCase()
												.includes(
														$scope.searchText
																.toLowerCase())
										|| (($scope.wrkStnData[i].inLimit)
												.toString() != undefined
												&& ($scope.wrkStnData[i].inLimit)
														.toString() != null ? ($scope.wrkStnData[i].inLimit)
												.toString()
												: "").toLowerCase()
												.includes(
														$scope.searchText
																.toLowerCase())
										|| (($scope.wrkStnData[i].outLimit)
												.toString() != undefined
												&& ($scope.wrkStnData[i].outLimit)
														.toString() != null ? ($scope.wrkStnData[i].outLimit)
												.toString()
												: "").toLowerCase()
												.includes(
														$scope.searchText
																.toLowerCase())) {
									$scope.wrkStnFilterList
											.push($scope.wrkStnData[i]);

								}

							}
							// }
						}

						$scope.totalItems = $scope.wrkStnFilterList.length;
						$scope.currentPage = 1;
						$scope.getTotalPages();

						if ($scope.wrkStnData.length == 0
								|| $scope.wrkStnFilterList.length == 0) {
							$scope.wrkStnViewData = [];
							$scope.workStationNoData = true;

						} else {
							$scope.workStationNoData = false;

							var begin = (($scope.currentPage - 1) * $scope.numPerPage), end = begin
									+ $scope.numPerPage;

							$scope.wrkStnViewData = $scope.wrkStnFilterList
									.slice(begin, end);

						}
					}

					// --------- Search bar code ends here-------------
					$scope.editWorkStationValue = function() {
						$scope.workstationEditScreen = false;
						$scope.workstationUpdateScreen = true;
						$scope.fnEditTreeWithOperatorList();
					}
					/*
					 * $scope.updateWorkStation = function() { $scope.workstationEditScreen =
					 * true; $scope.workstationUpdateScreen = false; }
					 */

					// row highlighting
					$scope.rowHighilited = function(row) {
						
						$rootScope.ZoneSelection = null;
						$scope.selectedRow = row;
						$scope.editWS = JSON.parse(JSON
								.stringify($scope.wrkStnViewData[row]));

						if ($scope.zoneList.indexOf($scope.editWS.storage) < 0)
							$scope.editWS.inbound = $scope.locationList[0];
						if ($scope.zoneList.indexOf($scope.editWS.workArea) < 0)
							$scope.editWS.outbound = $scope.locationList[0];
						if ($scope.zoneList.indexOf($scope.editWS.outbound) < 0)
							$scope.editWS.workArea = $scope.locationList[0];
						if ($scope.zoneList.indexOf($scope.editWS.inbound) < 0)
							$scope.editWS.storage = $scope.shiftList[0];

						if ($scope.editWS.outbound != undefined
								&& $scope.editWS.outbound != null
								&& $scope.editWS.outbound != '') {
							$scope.trackOutboundId = $scope.editWS.outbound;
						} else {
							$scope.trackOutboundId = '';
						}

						/*
						 * $scope.selectedRow = row; $scope.authen =
						 * JSON.parse(JSON.stringify($scope.empViewData[row]));
						 */

					}
					$scope.cancelEdit = function() {
						$scope.selectedRow = -1;
					}

					$scope.trackOutboundId = '';
					$scope.cancelWorkStation = function() {
						$scope.selectedRow = -1;
						$scope.trackOutboundId = '';
					}

					$scope.trackOutbound = function() {
						if ($scope.trackOutboundId != undefined
								&& $scope.trackOutboundId != null
								&& $scope.trackOutboundId != '') {
							$rootScope.showFullView();
							$rootScope.trackOutboundId = $scope.trackOutboundId;
							var currentMenuName = $window.sessionStorage
									.getItem("menuName");
							if ($scope.user.role == "DemoUser") {
								$window.sessionStorage.setItem("menuName",
										"shopFloorAnalysis");
								$window.sessionStorage.setItem("demoMenuName",
										"workorderLiveView");
								$location.path('/shopFloorAnalysis');
							} else {
								$window.sessionStorage.setItem("menuName",
										"workorderLiveView");
								$location.path('/workorderLiveView');
							}
							$rootScope.$broadcast('sourcePreviousScreen',
									currentMenuName);
						} else {
							toastr.error('No assigned tags to track.', "EI4.0");
						}
					}

					// operator list API
					$scope.getStaffDetails = function() {
						$rootScope.showloading('#loadingBar');
						$http({
							method : 'GET',
							url : './staff',

						})
								.then(

										function(response) {
											$rootScope
													.hideloading('#loadingBar');
											if (response != null
													&& response.data != null
													&& response.data.length != 0
													&& response.data != "BAD_REQUEST"
													&& response.data.data != undefined
													&& response.data.data.length != 0) {

												$scope.operatorList.length = 0;

												for (var i = 0; i < response.data.data.length; i++) {
													if (response.data.data[i].active == true) {

														$scope.operatorList
																.push({
																	"id" : (response.data.data[i].empId),
																	"text" : (response.data.data[i].firstName
																			+ " " + response.data.data[i].lastName)
																});
													}
												}

											} else {

											}

											menudata = JSON
													.parse(JSON
															.stringify($scope.operatorList));
											
											
											if($scope.workstationUpdateScreen)
												$scope.editWorkStationValue();
											else
												$scope
											.fnCreateTreeWithOperatorList();
												
										},
										function(error) {
											$rootScope
													.hideloading('#loadingBar');
											$rootScope.fnHttpError(error);
										});
					}

					$scope.fnCreateTreeWithOperatorList = function() {
						let frequentdata;
						let menuIdArray = [];
						
						let tree = new Tree('.frequent', {
							data : [ {
								id : -1,
								text : 'All',
								children : menudata
							} ],
							closeDepth : 2,
							loaded : function() {
								if($scope.clearTreeFlag){
									this.values = [];
									$scope.clearTreeFlag = false;
								}									
								else{
									this.values = $scope.selectedOperator;										
								}
									
								
								// console.log(this.selectedNodes)
								// console.log(this.values)
							},
							onChange : function() {
								frequentdata = this.selectedNodes;
								$scope.selectedOperator = this.values;
								// console.log(this.values);

							}
						})
					}

					$scope.fnEditTreeWithOperatorList = function() {

						
						if(menudata !=undefined && menudata != null && menudata.length > 0){
							
							if($rootScope.ZoneSelection == undefined || $rootScope.ZoneSelection == null ){
								for (var i = 0; i < $scope.wrkStnViewData.length; i++) {
									if ($scope.wrkStnViewData[i].stationId == $scope.editWS.stationId)
										$scope.selectedOperatorEditWS = $scope.wrkStnViewData[i].operatorList;
								}
							}
							

							let frequentdata;
							let menuIdArray = [];

							let tree = new Tree('.frequentEditWS', {
								data : [ {
									id : -1,
									text : 'All',
									children : menudata
								} ],
								closeDepth : 2,
								loaded : function() {
									this.values = $scope.selectedOperatorEditWS;
									
								},
								onChange : function() {
									frequentdata = this.selectedNodes;
									$scope.selectedOperatorEditWS = this.values;
									// console.log(this.values);

								}
							})
						}
						

					}

					// zone list API
					$scope.getZoneDetails = function() {
						$rootScope.showloading('#loadingBar');
						$http({
							method : 'GET',
							url : './zone',

						})
								.then(
										function(response) {
											$rootScope
													.hideloading('#loadingBar');
											if (response != null
													&& response.data != null
													&& response.data.length != 0
													&& response.data != "BAD_REQUEST"
													&& response.data.data != undefined
													&& response.data.data.length != 0) {

												$scope.zoneList = [];

												for (var i = 0; i < response.data.data.length; i++) {
													if (response.data.data[i].active == true) {
														$scope.zoneList
																.push(response.data.data[i].name);
														$scope.zoneListActual
																.push(response.data.data[i]);
													}
												}

											} else {

											}
										},
										function(error) {
											$rootScope
													.hideloading('#loadingBar');
											$rootScope.fnHttpError(error);
										});
					}

					// Anand View all workstation table API call

					$scope.getWrkStnViewData = function() {

						if (!$rootScope.checkNetconnection()) {
							$rootScope.alertDialogChecknet();
							return;
						}
						$rootScope.showloading('#loadingBar');
						$http({
							method : 'GET',
							url : './workStation',

						})
								.then(

										function(response) {
											$rootScope
													.hideloading('#loadingBar');
											if (response != null
													&& response.data != null
													&& response.data.length != 0
													&& response.data != "BAD_REQUEST"
													&& response.data.data != undefined
													&& response.data.data.length != 0) {

												$scope.wrkStnData.length = 0;

												for (var i = 0; i < response.data.data.length; i++) {
													if (response.data.data[i].active == true) {
														/*
														 * $scope.wrkStnData
														 * .push(response.data.data[i]);
														 */

														$scope.wrkStnData
																.push({
																	'stationId' : response.data.data[i].stationId,
																	'stationType' : response.data.data[i].stationType,
																	'inbound' : response.data.data[i].inbound.name,
																	'outbound' : response.data.data[i].outbound.name,
																	'storage' : response.data.data[i].storage.name,
																	'workArea' : response.data.data[i].workArea.name,
																	'inLimit' : response.data.data[i].inLimit,
																	'outLimit' : response.data.data[i].outLimit,
																	'operatorList' : response.data.data[i].operatorList
																});
													}
												}
												if ($scope.wrkStnData.length == 0) {
													$scope.workStationNoData = true;
													return;
												} else {
													$scope.workStationNoData = false;

												}

												$scope.totalItems = $scope.wrkStnData.length;
												$scope.getTotalPages();

												$scope.currentPage = 1;
												if ($scope.wrkStnData.length > $scope.numPerPage) {
													$scope.wrkStnViewData = $scope.wrkStnData
															.slice(
																	0,
																	$scope.numPerPage);
													$scope.PaginationTab = true;
												} else {
													$scope.wrkStnViewData = $scope.wrkStnData;
													$scope.PaginationTab = false;
												}

											} else {
												$scope.workStationNoData = true;
											}
										},
										function error(response) {
											$rootScope
													.hideloading('#loadingBar');
											$rootScope.fnHttpError(response);
										});
					}

					// $scope.getWrkStnViewData();

					// Delete workstation record

					$scope.deleteWorkStation = function() {
						var name = document
								.getElementById("id_stationId_editWS");

						if (name.value.length == 0) {
							$scope.nameError = true;
							$scope.name_error_msg = "Please enter the Work station ID";
							$timeout(function() {
								$scope.nameError = false;

							}, 2000);

							return;
						}

						if ($scope.workstationEditForm.$valid) {
							$
									.confirm({
										title : $rootScope.MSGBOX_TITLE,
										content : 'Are you sure you want to delete this record?',
										type : 'blue',
										columnClass : 'medium',
										autoClose : false,
										icon : 'fa fa-info-circle',
										buttons : {
											yes : {
												text : 'Yes',
												btnClass : 'btn-blue',
												action : function() {

													if (!$rootScope
															.checkNetconnection()) {
														$rootScope
																.alertDialogChecknet();
														return;
													}

													var stationId = $scope.editWS.stationId
													data = {
														"stationId" : stationId
													}
													// var index =
													// $scope.wrkStnViewData.indexOf();
													$scope.wrkStnViewData
															.splice(
																	$scope.selectedRow,
																	1);
													$rootScope
															.showloading('#loadingBar');
													$http(
															{
																method : 'DELETE',
																url : './workStation',
																headers : {
																	'Content-Type' : 'application/json'
																},
																data : data,

															})
															.then(

																	function(
																			response) {
																		$rootScope
																				.hideloading('#loadingBar');
																		if (response != null
																				&& response.data != null
																				&& response.data != "BAD_REQUEST") {

																			$scope.deletedStation = response.data;
																			$rootScope
																					.showAlertDialog(response.data.message);
																			// $scope.getWrkStnViewData();
																			$scope
																					.cancelWorkStationedit();

																		} else {
																			$rootScope
																					.showAlertDialog(response.data.message);
																			$timeout(
																					function() {
																						$scope
																								.cancelWorkStationedit();
																					},
																					2000);
																		}
																	},
																	function(
																			error) {
																		$rootScope
																				.hideloading('#loadingBar');
																		$rootScope
																				.fnHttpError(error);
																	});
												}
											},
											no : function() {

											}
										}
									});
						}

					}

					// Delete workstation record ends here

					// Anand station Id validation
					$("#id_stationId_editWS").keypress(
							function(e) {
								// if the letter is not alphabets then display
								// error
								// and don't type anything
								$return = ((e.which > 64) && (e.which < 91))
										|| ((e.which > 96) && (e.which < 123))
										|| (e.which == 8) || (e.which == 32)
										|| (e.which >= 48 && e.which <= 57)
								if (!$return)

								{
									// display error message

									return false;
								}
							});
					$("#id_stationId").keypress(
							function(e) {
								// if the letter is not alphabets then display
								// error
								// and don't type anything
								$return = ((e.which > 64) && (e.which < 91))
										|| ((e.which > 96) && (e.which < 123))
										|| (e.which == 8) || (e.which == 32)
										|| (e.which >= 48 && e.which <= 57)
								if (!$return)

								{
									// display error message

									return false;
								}
							});
					// Anand station type validation
					$("#id_stationType_editWS").keypress(
							function(e) {
								// if the letter is not alphabets then display
								// error
								// and don't type anything
								$return = ((e.which > 64) && (e.which < 91))
										|| ((e.which > 96) && (e.which < 123))
										|| (e.which == 8) || (e.which == 32)
										|| (e.which >= 48 && e.which <= 57)
								if (!$return)

								{
									// display error message

									return false;
								}
							});
					$("#id_stationType").keypress(
							function(e) {
								// if the letter is not alphabets then display
								// error
								// and don't type anything
								$return = ((e.which > 64) && (e.which < 91))
										|| ((e.which > 96) && (e.which < 123))
										|| (e.which == 8) || (e.which == 32)
										|| (e.which >= 48 && e.which <= 57)
								if (!$return)

								{
									// display error message

									return false;
								}
							});

					// Anand Ph number validation
					$("#id_inLimit_editWS").keypress(
							function(e) {
								// if the letter is not digit then display error
								// and don't type anything

								if (e.which != 8 && e.which != 0
										&& (e.which < 48 || e.which > 57)) {

									return false;
								}
							});
					$("#id_outLimit_editWS").keypress(
							function(e) {
								// if the letter is not digit then display error
								// and don't type anything

								if (e.which != 8 && e.which != 0
										&& (e.which < 48 || e.which > 57)) {

									return false;
								}
							});
					$("#id_inLimit").keypress(
							function(e) {
								// if the letter is not digit then display error
								// and don't type anything

								if (e.which != 8 && e.which != 0
										&& (e.which < 48 || e.which > 57)) {

									return false;
								}
							});
					$("#id_outLimit").keypress(
							function(e) {
								// if the letter is not digit then display error
								// and don't type anything

								if (e.which != 8 && e.which != 0
										&& (e.which < 48 || e.which > 57)) {

									return false;
								}
							});
					$scope.fnGetDictionaryLength = function(dict) {
						var nCount = 0;
						for (i in dict)
							++nCount;
						return nCount;
					}
					// Anand select inbound out bound in add workstation
						
					$scope.fnSelectZoneWorkStation = function(location) {
						$rootScope.locationInfo = location;
					
						if ($scope.fnGetDictionaryLength($rootScope.ZoneSelection) == 0)
						{
							if ($rootScope.locationInfo == 'inBound'){
								$rootScope.ZoneSelection.selectedZone = $scope.authen.inbound;
							}
							else if ($rootScope.locationInfo == 'workArea'){
								$rootScope.ZoneSelection.selectedZone = $scope.authen.workArea;
							}
							else if($rootScope.locationInfo == 'outBound'){
								$rootScope.ZoneSelection.selectedZone = $scope.authen.outbound;
							}
							else if($rootScope.locationInfo == 'shippingArea'){
								$rootScope.ZoneSelection.selectedZone = $scope.authen.storage;
							}

						 $rootScope.ZoneSelection.zoneSelect = "true";
						
						 $rootScope.ZoneSelection.stationId = $scope.authen.stationId;
						 $rootScope.ZoneSelection.stationType = $scope.authen.stationType;
						 $rootScope.ZoneSelection.inbound = $scope.authen.inbound;
						 $rootScope.ZoneSelection.workArea =  $scope.authen.workArea;
						 $rootScope.ZoneSelection.outbound = $scope.authen.outbound;
						 $rootScope.ZoneSelection.storage = $scope.authen.storage;
						 $rootScope.ZoneSelection.inLimit = $scope.authen.inLimit ;
						 $rootScope.ZoneSelection.outLimit = $scope.authen.outLimit;
						$rootScope.ZoneSelection.selectedOperator = $scope.selectedOperator;
						
						//$rootScope.ZoneSelection.locationList = $scope.locationList;
						$rootScope.ZoneSelection.zoneDetails = $scope.locationList;
						$rootScope.isWorkStation = true ;
						$rootScope.isWorkStationEdit = false ;
						$window.sessionStorage.setItem("menuName","zoneManagement");
						$location.path('/zoneManagement');
						//locationList
						}
					}
					
					$scope.fnSelectZoneWorkStationEdit  = function(locationInfo) {
						$rootScope.selectedRowOnZoneDetail =  {};
						 $rootScope.ZoneSelection = {};
						$rootScope.selectedRowOnZoneDetail.selectedRow =$scope.selectedRow;
						$rootScope.selectedRowOnZoneDetail.pageNum =$scope.currentPage;
						$rootScope.selectedRowOnZoneDetail.wrkStnDataDetail = $scope.wrkStnData;
						
						$rootScope.locationInfo = locationInfo;
						if ($scope.fnGetDictionaryLength($rootScope.ZoneSelection) == 0)
						{
							
							if ($rootScope.locationInfo == 'inBound'){
								$rootScope.ZoneSelection.selectedZone = $scope.editWS.inbound;
							}
							else if ($rootScope.locationInfo == 'workArea'){
								$rootScope.ZoneSelection.selectedZone = $scope.editWS.workArea;
							}
							else if($rootScope.locationInfo == 'outBound'){
								$rootScope.ZoneSelection.selectedZone = $scope.editWS.outbound;
							}
							else if($rootScope.locationInfo == 'shippingArea'){
								$rootScope.ZoneSelection.selectedZone = $scope.editWS.storage;
							}
							
						$rootScope.ZoneSelection.zoneSelect = "true";
						
						 $rootScope.ZoneSelection.stationId = $scope.editWS.stationId;
						 $rootScope.ZoneSelection.stationType = $scope.editWS.stationType;
						 $rootScope.ZoneSelection.inbound = $scope.editWS.inbound;
						 $rootScope.ZoneSelection.workArea =  $scope.editWS.workArea;
						 $rootScope.ZoneSelection.outbound = $scope.editWS.outbound;
						 $rootScope.ZoneSelection.storage = $scope.editWS.storage;
						 $rootScope.ZoneSelection.inLimit = $scope.editWS.inLimit ;
						 $rootScope.ZoneSelection.outLimit = $scope.editWS.outLimit;
						 $rootScope.ZoneSelection.selectedOperator = $scope.selectedOperatorEditWS;
						
						//$rootScope.ZoneSelection.locationList = $scope.locationList;
						$rootScope.ZoneSelection.zoneDetails = $scope.locationList;
						$rootScope.isWorkStation = false ;
						$rootScope.isWorkStationEdit = true ;
						$window.sessionStorage.setItem("menuName","zoneManagement");
						$location.path('/zoneManagement');
						//locationList
						
						
						}
					}
					
					// Anand 2-6-21 add work station validation
					$scope.addWorkStation = function() {

						var stationId = document.getElementById("id_stationId");
						if (stationId.value.length == 0) {
							// $rootScope.hideloading('#loadingBar');
							$scope.addWSstationIdError = true;
							$scope.addWSstationId_error_msg = "Please enter Station Id";
							$timeout(function() {
								$scope.addWSstationIdError = false;
							}, 2000);
							return;
						}
						var stationType = document
								.getElementById("id_stationType");
						if (stationType.value.length == 0) {
							// $rootScope.hideloading('#loadingBar');
							$scope.addWSstationTypeError = true;
							$scope.addWSstationType_error_msg = "Please enter Station Type";
							$timeout(function() {
								$scope.addWSstationTypeError = false;
							}, 2000);
							return;
						}
						var inLimit = document.getElementById("id_inLimit");
						if (inLimit.value.length == 0) {
							// $rootScope.hideloading('#loadingBar');
							$scope.addWSstationInlimitError = true;
							$scope.addWSstationInlimit_error_msg = "Please enter Input Limit";
							$timeout(function() {
								$scope.addWSstationInlimitError = false;
							}, 2000);
							return;
						}
						var outLimit = document.getElementById("id_outLimit");
						if (outLimit.value.length == 0) {
							// $rootScope.hideloading('#loadingBar');
							$scope.addWSstationOutlimitError = true;
							$scope.addWSstationOutlimit_error_msg = "Please enter Output Limit";
							$timeout(function() {
								$scope.addWSstationOutlimitError = false;
							}, 2000);
							return;
						}

						/*
						 * if($scope.selectedOperator.length == 0){
						 * $rootScope.hideloading('#loadingBar'); $.alert({
						 * title : $rootScope.MSGBOX_TITLE, content : "Please
						 * Select a Operator ", type : 'blue', columnClass :
						 * 'small', autoClose : false, icon : 'fa
						 * fa-info-circle', }); return }
						 */

						if ($rootScope.checkNetconnection() == false) {
							// $rootScope.hideloading('#loadingBar');
							$rootScope.alertDialogChecknet();
							return;
						}
						$rootScope.showloading('#loadingBar');

						var inBoundID = 0, storageID = 0, outboundID = 0, workAreaID = 0;
						for (var i = 0; i < $scope.zoneListActual.length; i++) {
							if ($scope.authen.inbound == $scope.zoneListActual[i].name) {
								inBoundID = $scope.zoneListActual[i].id;
								break;
							}
						}
						for (var i = 0; i < $scope.zoneListActual.length; i++) {
							if ($scope.authen.outbound == $scope.zoneListActual[i].name) {
								outboundID = $scope.zoneListActual[i].id;
								break;
							}
						}
						for (var i = 0; i < $scope.zoneListActual.length; i++) {
							if ($scope.authen.workArea == $scope.zoneListActual[i].name) {
								workAreaID = $scope.zoneListActual[i].id;
								break;
							}
						}
						for (var i = 0; i < $scope.zoneListActual.length; i++) {
							if ($scope.authen.storage == $scope.zoneListActual[i].name) {
								storageID = $scope.zoneListActual[i].id;
								break;
							}
						}
						var addWorkStationParameter = {

							"stationId" : $scope.authen.stationId,
							"stationType" : $scope.authen.stationType,
							"inbound" : inBoundID,
							"outbound" : outboundID,
							"workArea" : workAreaID,
							"storage" : storageID,
							"inLimit" : $scope.authen.inLimit,
							"outLimit" : $scope.authen.outLimit,
							"zone" : "Zone",
							"operatorList" : $scope.selectedOperator
						// operator list should be in following format
						// ["test01","test02"] selected
						};
						$scope.promise = $http
								.post('./workStation', addWorkStationParameter)
								.then(
										function(result) {
											$rootScope
													.hideloading('#loadingBar');
											if (result.data.status == "success") {
												$rootScope
														.showAlertDialog(result.data.message);
												$scope.cancelWorkStationAdd();
												/*
												 * $timeout(function() {
												 * 
												 * //$location.path("/login");
												 *  }, 1500);
												 */
											} else if (result.data.status == "error") {
												$rootScope
														.showAlertDialog(result.data.message);
											}

										},
										function(error) {
											$rootScope
													.hideloading('#loadingBar');
											$rootScope.fnHttpError(error);
										});

					}

					$scope.cancelWorkStationAdd = function() {
						$scope.clearTreeFlag = true;
						$scope.fnCreateTreeWithOperatorList();
						$scope.authen = {};
						$('#id_stationId').val("");
						$('#id_stationType').val("");
						$('#id_inLimit').val("");
						$('#id_outLimit').val("");
						if($scope.zoneList != undefined || $scope.zoneList != null){
							if ($scope.zoneList.indexOf($scope.authen.inbound) < 0)
								$scope.authen.inbound = $scope.zoneList[0];
							if ($scope.zoneList.indexOf($scope.authen.outbound) < 0)
								$scope.authen.outbound = $scope.zoneList[0];
							if ($scope.zoneList.indexOf($scope.authen.workArea) < 0)
								$scope.authen.workArea = $scope.zoneList[0];
							if ($scope.zoneList.indexOf($scope.authen.storage) < 0)
								$scope.authen.storage = $scope.zoneList[0];
						}
						

					}
					// edit workstation.
					// Anand 6-8-21 edit work station validation
					$scope.updateWorkStation = function() {

						var stationId = document
								.getElementById("id_stationId_editWS");
						if (stationId.value.length == 0) {
							// $rootScope.hideloading('#loadingBar');
							$scope.editWSstationIdError = true;
							$scope.editWSstationId_error_msg = "Please enter Station Id";
							$timeout(function() {
								$scope.editWSstationIdError = false;
							}, 2000);
							return;
						}
						var stationType = document
								.getElementById("id_stationType_editWS");
						if (stationType.value.length == 0) {
							// $rootScope.hideloading('#loadingBar');
							$scope.editWSstationTypeError = true;
							$scope.editWSstationType_error_msg = "Please enter Station Type";
							$timeout(function() {
								$scope.editWSstationTypeError = false;
							}, 2000);
							return;
						}

						var inLimit = document
								.getElementById("id_inLimit_editWS");
						if (inLimit.value.length == 0) {
							// $rootScope.hideloading('#loadingBar');
							$scope.editWSstationInlimitError = true;
							$scope.editWSstationInlimit_error_msg = "Please enter InLimit";
							$timeout(function() {
								$scope.editWSstationInlimitError = false;
							}, 2000);
							return;
						}
						var outLimit = document
								.getElementById("id_outLimit_editWS");
						if (outLimit.value.length == 0) {
							// $rootScope.hideloading('#loadingBar');
							$scope.editWSstationOutlimitError = true;
							$scope.editWSstationOutlimit_error_msg = "Please enter OutLimit";
							$timeout(function() {
								$scope.editWSstationOutlimitError = false;
							}, 2000);
							return;
						}

						/*
						 * if($scope.selectedOperatorEditWS.length == 0){
						 * $rootScope.hideloading('#loadingBar'); $.alert({
						 * title : $rootScope.MSGBOX_TITLE, content : "Please
						 * Select a Operator ", type : 'blue', columnClass :
						 * 'small', autoClose : false, icon : 'fa
						 * fa-info-circle', }); return }
						 */

						if ($rootScope.checkNetconnection() == false) {
							$rootScope.alertDialogChecknet();
							return;
						}
						$rootScope.showloading('#loadingBar');

						var inBoundID = 0, storageID = 0, outboundID = 0, workAreaID = 0;
						var inBoundName = "", storageName = "", outboundName = "", workAreaName = "";
						for (var i = 0; i < $scope.zoneListActual.length; i++) {
							if ($scope.editWS.inbound == $scope.zoneListActual[i].name) {
								inBoundID = $scope.zoneListActual[i].id;
								inBoundName = $scope.zoneListActual[i].name;
								break;
							}
						}
						for (var i = 0; i < $scope.zoneListActual.length; i++) {
							if ($scope.editWS.outbound == $scope.zoneListActual[i].name) {
								outboundID = $scope.zoneListActual[i].id;
								outboundName = $scope.zoneListActual[i].name;
								break;
							}
						}
						for (var i = 0; i < $scope.zoneListActual.length; i++) {
							if ($scope.editWS.workArea == $scope.zoneListActual[i].name) {
								workAreaID = $scope.zoneListActual[i].id;
								workAreaName = $scope.zoneListActual[i].name;
								break;
							}
						}
						for (var i = 0; i < $scope.zoneListActual.length; i++) {
							if ($scope.editWS.storage == $scope.zoneListActual[i].name) {
								storageID = $scope.zoneListActual[i].id;
								storageName = $scope.zoneListActual[i].name;
								break;
							}
						}
						var editWorkStationParameter = {

							"stationId" : $scope.editWS.stationId,
							"stationType" : $scope.editWS.stationType,
							"inbound" : inBoundID,
							"outbound" : outboundID,
							"workArea" : workAreaID,
							"storage" : storageID,
							"inLimit" : $scope.editWS.inLimit,
							"outLimit" : $scope.editWS.outLimit,
							"zone" : "Zone",
							"operatorList" : $scope.selectedOperatorEditWS
						// operator list should be in following format
						// ["test01","test02"] selected
						};
						// to add tree view list in editWS
						$scope.editWS.operatorList = $scope.selectedOperatorEditWS;
						$scope.wrkStnViewData[$scope.selectedRow] = JSON
								.parse(JSON.stringify($scope.editWS));
						// console.log($scope.wrkStnViewData);

						$scope.promise = $http
								.put('./workStation', editWorkStationParameter)
								.then(

										function(result) {
											$rootScope
													.hideloading('#loadingBar');
											if (result.data.status == "success") {
												$rootScope
														.showAlertDialog(result.data.message);
												$scope.cancelWorkStationedit();

											} else if (result.data.status == "error") {
												$rootScope
														.showAlertDialog(result.data.message);
											}

										},
										function(error) {
											$rootScope
													.hideloading('#loadingBar');
											$rootScope.fnHttpError(error);
										});

					}

					// ----
					$scope.cancelWorkStationedit = function() {
						$scope.editWS = {};
						$scope.selectedRow = -1;
						$scope.workstationEditScreen = true;
						$scope.workstationUpdateScreen = false;

					}
					// pagination button

					$scope.currentPage = 1;
					$scope.numPerPage = 10;
					$scope.maxSize = 3;
					$scope.totalItems = 0;
					$scope.wrkStnFilterList = [];
					$scope.wrkStnViewData = [];
					$scope
							.$watch(
									'currentPage + numPerPage',
									function() {
										var begin = (($scope.currentPage - 1) * $scope.numPerPage), end = begin
												+ $scope.numPerPage;

										if ($scope.filterValue == "All")
											$scope.wrkStnViewData = $scope.wrkStnData
													.slice(begin, end);
										else
											$scope.wrkStnViewData = $scope.wrkStnData
													.slice(begin, end);

									});

					$scope.currentPage = 1;

					$scope.maxSize = 3;

					$scope.numPerPage = 10;
					$scope.totalItems = 0;
					$scope.totalPages = 0;

					$scope.hasNext = true;
					$scope.hasPrevious = true;
					$scope.isFirstPage = true;
					$scope.isLastPage = true;
					$scope.serial = 1;

					$scope
							.$watch(
									"currentPage",
									function(newVal, oldVal) {
										if (newVal !== oldVal) {

											if (newVal > $scope.totalPages) {
												$scope.currentPage = $scope.totalPages;
											} else {
												$scope.currentPage = newVal;
											}

											$scope.serial = (($scope.currentPage - 1) * $scope.numPerPage) + 1;

										}
									});

					$scope.enablePageButtons = function() {
						$scope.serial = (($scope.currentPage - 1) * $scope.numPerPage) + 1;
						$scope.selectedRow = -1;
						if ($scope.totalPages <= 1) {
							$scope.hasPrevious = true;
							$scope.isFirstPage = true;
							$scope.hasNext = true;
							$scope.isLastPage = true;

						} else if ($scope.totalPages > 1
								&& $scope.currentPage == $scope.totalPages) {

							$scope.hasPrevious = false;
							$scope.isFirstPage = false;
							$scope.hasNext = true;
							$scope.isLastPage = true;
						} else if ($scope.currentPage != 1
								&& $scope.currentPage < $scope.totalPages) {

							$scope.hasPrevious = false;
							$scope.isFirstPage = false;
							$scope.hasNext = false;
							$scope.isLastPage = false;

						} else {
							$scope.hasPrevious = true;
							$scope.isFirstPage = true;
							$scope.hasNext = false;
							$scope.isLastPage = false;
						}
					};

					$scope.getTotalPages = function() {

						if (($scope.totalItems % $scope.numPerPage) > 0)
							$scope.totalPages = parseInt($scope.totalItems
									/ $scope.numPerPage) + 1;
						else
							$scope.totalPages = parseInt($scope.totalItems
									/ $scope.numPerPage);

						$scope.enablePageButtons();

					};

					$scope.goFirstPage = function() {
						$scope.currentPage = 1;
						$scope.enablePageButtons();
					};

					$scope.goPreviousPage = function() {
						if ($scope.currentPage > 1)
							$scope.currentPage = $scope.currentPage - 1;
						$scope.enablePageButtons();
					};

					$scope.goNextPage = function() {
						if ($scope.currentPage <= $scope.totalPages)
							$scope.currentPage = $scope.currentPage + 1;
						$scope.enablePageButtons();
					};

					$scope.goLastPage = function() {
						$scope.currentPage = $scope.totalPages;
						$scope.enablePageButtons();
					};
					$scope.displayWorkStationViewScreen = function() {
						$scope.workstationViewScreen = true;
						$scope.workstationEditScreen = false;
						$scope.workstationUpdateScreen = false;
						$scope.workstationAddScreen = false;
						$("#idWorkStationView").addClass("active");
						$("#idWorkStationAdd").removeClass("active");
						$("#idWorkStationEdit").removeClass("active");
						$scope.getWrkStnViewData();
						$scope.showSearchBar = true;
						$scope.showSearchIcon = true;
						$scope.showMultipleSearch = false;
					};
					$scope.displayWorkStationAddScreen = function() {
						$scope.workstationViewScreen = false;
						$scope.workstationEditScreen = false;
						$scope.workstationUpdateScreen = false;
						$scope.showSearchBar = false;
						$scope.workstationAddScreen = true;
						$scope.showSearchIcon = false;
						$scope.showMultipleSearch = false;
						$scope.cancelWorkStationAdd();
						
						$timeout(function() {
							$("#idWorkStationView").removeClass("active");
							$("#idWorkStationAdd").addClass("active");
							$("#idWorkStationEdit").removeClass("active");
							}, 100);
						
						
						if ($rootScope.ZoneSelection.zoneSelect != undefined && $rootScope.ZoneSelection.zoneSelect == "true" && $rootScope.isWorkStation == true && $rootScope.isWorkStationEdit == false){
					 		$rootScope.ZoneSelection.zoneSelect = "false";
					 		$rootScope.isBackFromZone = false;
					 		$rootScope.isWorkStation = false;
					 		
					 		$scope.authen.stationId = $rootScope.ZoneSelection.stationId ;
					 		$scope.authen.stationType =$rootScope.ZoneSelection.stationType;
					 		$scope.authen.inLimit = $rootScope.ZoneSelection.inLimit  ;
					 		$scope.authen.outLimit = $rootScope.ZoneSelection.outLimit ;
					 		$scope.selectedOperator =  $rootScope.ZoneSelection.selectedOperator ;
					 		// $scope.fnCreateTreeWithOperatorList();
					 		var inbound =  $rootScope.ZoneSelection.inbound;
					 		var workArea =  $rootScope.ZoneSelection.workArea;
					 		var outbound = $rootScope.ZoneSelection.outbound  ;
					 		var storage = $rootScope.ZoneSelection.storage ;
					 		
							if($rootScope.locationInfo == 'inBound'){
								$rootScope.locationInfo ="";
								$scope.authen.inbound = $rootScope.ZoneSelection.selectedZone;
								$scope.authen.workArea = $rootScope.ZoneSelection.workArea;
								$scope.authen.outbound = $rootScope.ZoneSelection.outbound;
								$scope.authen.storage = $rootScope.ZoneSelection.storage;
							}
							
							if ($rootScope.locationInfo == 'workArea'){
								$rootScope.locationInfo ="";
								$scope.authen.inbound =  $rootScope.ZoneSelection.inbound;
								$scope.authen.workArea = $rootScope.ZoneSelection.selectedZone;
								$scope.authen.outbound = $rootScope.ZoneSelection.outbound;
								$scope.authen.storage = $rootScope.ZoneSelection.storage;
							}
							
							 if ($rootScope.locationInfo == 'outBound'){
								$rootScope.locationInfo ="";
								$scope.authen.inbound =  $rootScope.ZoneSelection.inbound;
								$scope.authen.workArea =  $rootScope.ZoneSelection.workArea;
								$scope.authen.outbound = $rootScope.ZoneSelection.selectedZone;
								$scope.authen.storage = $rootScope.ZoneSelection.storage;
								}
							 if ($rootScope.locationInfo == 'shippingArea'){
									$rootScope.locationInfo ="";
									$scope.authen.inbound =  $rootScope.ZoneSelection.inbound;
									$scope.authen.workArea =  $rootScope.ZoneSelection.workArea;
									$scope.authen.outbound = $rootScope.ZoneSelection.outbound;
									$scope.authen.storage = $rootScope.ZoneSelection.selectedZone;
									}
							
							$scope.zoneDetails = $rootScope.ZoneSelection.zoneDetails;
							$rootScope.ZoneSelection = {};
							$rootScope.setPageName("Work Station");
			 	}else
		 		{	 $scope.getZoneDetails();
					  		
							}
						
						

					};
					$scope.displayWorkStationEditScreen = function() {
						$scope.workstationViewScreen = false;
						$scope.workstationEditScreen = true;
						$scope.workstationUpdateScreen = false;
						$scope.workstationAddScreen = false;
						$scope.showSearchBar = true;
						$("#idWorkStationView").removeClass("active");
						$("#idWorkStationAdd").removeClass("active");
						$("#idWorkStationEdit").addClass("active");
						$scope.showSearchIcon = true;
						$scope.showMultipleSearch = false;
						$scope.getWrkStnViewData();
					};
					// to display enabled sub menus
					if ($scope.user.capabilityDetails != undefined
							&& $scope.user.capabilityDetails != null) {
						
						if ($scope.user.capabilityDetails.WWAD == 1 ||  $scope.user.capabilityDetails.WWED == 1 ) {
							$scope.getZoneDetails();
							$scope.getStaffDetails();
						}  
						
						if($rootScope.isBackFromZone == true)
						{
							$rootScope.isBackFromZone = false;
							if($rootScope.isWorkStation == true){
								$scope.displayWorkStationAddScreen();
							}else if($rootScope.isWorkStationEdit == true){
								  // for time being until edit screen code developed
								
								$scope.searchButton = false;
								$scope.workstationEditScreen = false;
								$scope.workstationUpdateScreen = true;
								$scope.workstationViewScreen = false;
								$scope.workstationAddScreen = false;
								$scope.showMultipleSearch = false;
								$timeout(function() {
								$("#idWorkStationView").removeClass("active");
								$("#idWorkStationAdd").removeClass("active");
								$("#idWorkStationEdit").addClass("active");
								}, 100);
								
								
								
								if ($rootScope.ZoneSelection.zoneSelect != undefined && $rootScope.ZoneSelection.zoneSelect == "true" && $rootScope.isWorkStation == false && $rootScope.isWorkStationEdit == true){
							 		$rootScope.ZoneSelection.zoneSelect = "false";
							 		$rootScope.isWorkStationEdit = false;
							 		$rootScope.isBackFromZone = false;
							 		$scope.editWS = {};
							 		
									 $scope.editWS.stationId = $rootScope.ZoneSelection.stationId;
									$scope.editWS.stationType = $rootScope.ZoneSelection.stationType;
									$scope.editWS.inbound = $rootScope.ZoneSelection.inbound;
									 $scope.editWS.workArea = $rootScope.ZoneSelection.workArea;
									 $scope.editWS.outbound = $rootScope.ZoneSelection.outbound;
									 $scope.editWS.storage = $rootScope.ZoneSelection.storage;
									 $scope.editWS.inLimit = $rootScope.ZoneSelection.inLimit;
									 $scope.editWS.outLimit = $rootScope.ZoneSelection.outLimit;
									 $scope.selectedOperatorEditWS = $rootScope.ZoneSelection.selectedOperator;
									 var inbound =  $rootScope.ZoneSelection.inbound;
								 	 var workArea =  $rootScope.ZoneSelection.workArea;
								 	 var outbound = $rootScope.ZoneSelection.outbound  ;
								 	 var storage = $rootScope.ZoneSelection.storage ;
								 	


									
									if($rootScope.selectedRowOnZoneDetail != undefined && $rootScope.selectedRowOnZoneDetail != null){
										$scope.selectedRow= $rootScope.selectedRowOnZoneDetail.selectedRow;
										$scope.currentPage = $rootScope.selectedRowOnZoneDetail.pageNum ;
										$scope.wrkStnData = $rootScope.selectedRowOnZoneDetail.wrkStnDataDetail ;
										
									}else{
										$scope.selectedRow= -1;
										$scope.currentPage = 1;
										$scope.wrkStnData = [];
									}
									
									$scope.wrkStnFilterList = $scope.wrkStnData;
									$scope.totalItems = $scope.wrkStnFilterList.length;
									$scope.currentPage = 1;
									$scope.getTotalPages();
									
									
									if ($scope.wrkStnData.length == 0
											|| $scope.wrkStnFilterList.length == 0) {
										$scope.wrkStnViewData = [];
										$scope.workStationNoData = true;

									} else {
										$scope.workStationNoData = false;

										if ($scope.wrkStnFilterList.length > $scope.numPerPage) {
											$scope.wrkStnViewData = JSON.parse(JSON
													.stringify($scope.wrkStnFilterList
															.slice(0, $scope.numPerPage)));
											$scope.PaginationTab = true;
										} else {
											$scope.wrkStnViewData = JSON.parse(JSON
													.stringify($scope.wrkStnFilterList));
											$scope.PaginationTab = false;
										}
									
									
								}
									
									$rootScope.selectedRowOnZoneDetail = null;
									
									if($rootScope.locationInfo == 'inBound'){
										$rootScope.locationInfo ="";
										$scope.editWS.inbound = $rootScope.ZoneSelection.selectedZone;
										$scope.editWS.workArea = $rootScope.ZoneSelection.workArea;
										$scope.editWS.outbound = $rootScope.ZoneSelection.outbound;
										$scope.editWS.storage = $rootScope.ZoneSelection.storage;
									}
									
									if ($rootScope.locationInfo == 'workArea'){
										$rootScope.locationInfo ="";
										$scope.editWS.inbound =  $rootScope.ZoneSelection.inbound;
										$scope.editWS.workArea = $rootScope.ZoneSelection.selectedZone;
										$scope.editWS.outbound = $rootScope.ZoneSelection.outbound;
										$scope.editWS.storage = $rootScope.ZoneSelection.storage;
									}
									
									 if ($rootScope.locationInfo == 'outBound'){
										$rootScope.locationInfo ="";
										$scope.editWS.inbound =  $rootScope.ZoneSelection.inbound;
										$scope.editWS.workArea =  $rootScope.ZoneSelection.workArea;
										$scope.editWS.outbound = $rootScope.ZoneSelection.selectedZone;
										$scope.editWS.storage = $rootScope.ZoneSelection.storage;
										}
									 if ($rootScope.locationInfo == 'shippingArea'){
											$rootScope.locationInfo ="";
											$scope.editWS.inbound =  $rootScope.ZoneSelection.inbound;
											$scope.editWS.workArea =  $rootScope.ZoneSelection.workArea;
											$scope.editWS.outbound = $rootScope.ZoneSelection.outbound;
											$scope.authen.storage = $rootScope.ZoneSelection.selectedZone;
											}
									
									$scope.zoneDetails = $rootScope.ZoneSelection.zoneDetails;
									$rootScope.ZoneSelection = {};
									$rootScope.setPageName("Work Station");
					 	}else
				 		{ 
					 		$scope.getZoneDetails();
				 		}
								$scope.editWorkStationValue();
							}
						}else{
							if ($scope.user.capabilityDetails.WWVA == 1) {
								$scope.displayWorkStationViewScreen();
							}else if ($scope.user.capabilityDetails.WWAD == 1) {
								$scope.displayWorkStationAddScreen();
							}else if ($scope.user.capabilityDetails.WWED == 1
									&& $scope.user.capabilityDetails.WWAD != 1) {
								$scope.displayWorkStationEditScreen();
							}
							
						}
						

					}

					// multisearch code starts
					$scope.multipleSearch = function() {
						$scope.showMultipleSearch = true;
						$scope.searchButton = false
						$scope.firstMultiSearchDropDown = "All";
						$scope.secondMultiSearchDropDown = "All";
						$scope.varANDORDropDown = "AND";
						$scope.firstSearchText = "";
						$scope.secondSearchText = "";
						$scope.showSearchBar = false;
					}
					$scope.closeMultiSearchView = function() {
						$scope.showMultipleSearch = false;
						$scope.showSearchBar = true;

						// for pagination

						$scope.wrkStnFilterList = $scope.wrkStnData;
						$scope.totalItems = $scope.wrkStnFilterList.length;
						$scope.currentPage = 1;
						$scope.getTotalPages();
						if ($scope.wrkStnData.length == 0
								|| $scope.wrkStnFilterList.length == 0) {
							$scope.workStationNoData = true;
							$scope.no_data_found = "No data found.";

						} else {
							$scope.workStationNoData = false;

							if ($scope.wrkStnFilterList.length > $scope.numPerPage) {
								$scope.wrkStnViewData = $scope.wrkStnFilterList
										.slice(0, $scope.numPerPage);
								$scope.PaginationTab = true;
							} else {
								$scope.wrkStnViewData = $scope.wrkStnFilterList;
								$scope.PaginationTab = false;
							}
						}

					}

					$scope.fnfirstSearchANDData = function() {

						$scope.firstSearchANDDataArray = [];

						for (var i = 0; i < $scope.wrkStnData.length; i++) {
							if ($scope.firstMultiSearchDropDown == "All") {
								if ($scope.firstSearchText == ""
										|| $scope.firstSearchText == undefined) {

									$scope.firstSearchANDDataArray
											.push($scope.wrkStnData[i]);
								} else {
									var obj = {};
									obj = $scope.wrkStnData[i];
									for (var x = 0; x < $scope.uniqueheaderList.length; x++) {
										var tableText = ($scope.wrkStnData[i][$scope.uniqueheaderList[x]])
												.toString();

										if ((tableText != undefined
												&& tableText != null ? tableText
												: "").toLowerCase().includes(
												$scope.firstSearchText
														.toLowerCase())) {
											$scope.firstSearchANDDataArray
													.push($scope.wrkStnData[i]);
											// break;
										}
									}

								}
							}
							// to check other than all
							else {
								var firstDropDownValue = $scope.firstMultiSearchDropDown;
								for (var x = 0; x < $scope.uniqueheaderList.length; x++) {
									if ($scope.uniqueheaderList[x] == firstDropDownValue)
										var actualHeaderfirstDropDownValue = $scope.uniqueheaderList[x];
								}
								if ($scope.firstSearchText == ""
										|| $scope.firstSearchText == undefined)
									$scope.firstSearchANDDataArray
											.push($scope.wrkStnData[i]);
								else {
									var obj = $scope.wrkStnData[i];

									// var tableText =
									// JSON.stringify($scope.wrkStnData[i][actualHeaderfirstDropDownValue]);

									var tableText = ($scope.wrkStnData[i][actualHeaderfirstDropDownValue])
											.toString();
									if ((tableText != undefined
											&& tableText != null ? tableText
											: "").toLowerCase().includes(
											$scope.firstSearchText
													.toLowerCase())) {
										$scope.firstSearchANDDataArray
												.push($scope.wrkStnData[i]);
										// break;
									}

								}

							}

						}

					}

					$scope.multiSearchChanged = function() {

						$scope.fnfirstSearchANDData();
						// console.log($scope.firstSearchANDDataArray);

						if ($scope.varANDORDropDown == "AND") {
							$scope.disableSecondFilterText = false;
							$scope.disableSecondFilterDropDown = false;
							$scope.wrkStnViewData = [];
							$scope.wrkStnFilterList = [];

							for (var i = 0; i < $scope.firstSearchANDDataArray.length; i++) {
								if ($scope.secondMultiSearchDropDown == "All") {

									if ($scope.secondSearchText == ""
											|| $scope.secondSearchText == undefined) {
										$scope.wrkStnFilterList
												.push($scope.firstSearchANDDataArray[i]);
									} else {

										var obj = $scope.wrkStnData[i];
										for (var x = 0; x < $scope.uniqueheaderList.length; x++) {
											var tableText = ($scope.firstSearchANDDataArray[i][$scope.uniqueheaderList[x]])
													.toString();

											if ((tableText != undefined
													&& tableText != null ? tableText
													: "")
													.toLowerCase()
													.includes(
															$scope.secondSearchText
																	.toLowerCase())) {
												$scope.wrkStnFilterList
														.push($scope.firstSearchANDDataArray[i]);
												// break;
											}
										}
									}
								} else {

									var secondDropDownValue = $scope.secondMultiSearchDropDown;
									for (var x = 0; x < $scope.uniqueheaderList.length; x++) {
										if ($scope.uniqueheaderList[x] == secondDropDownValue)
											var actualHeadersecondDropDownValue = $scope.uniqueheaderList[x];
									}
									if ($scope.secondSearchText == ""
											|| $scope.secondSearchText == undefined)

										$scope.wrkStnFilterList
												.push($scope.firstSearchANDDataArray[i]);
									else {
										var obj = $scope.wrkStnData[i];

										var tableText = ($scope.wrkStnData[i][actualHeadersecondDropDownValue])
												.toString();
										if ((tableText != undefined
												&& tableText != null ? tableText
												: "").toLowerCase().includes(
												$scope.secondSearchText
														.toLowerCase())) {
											$scope.wrkStnFilterList
													.push($scope.firstSearchANDDataArray[i]);

										}
									}

								}
							}

							$scope.totalItems = $scope.wrkStnFilterList.length;
							$scope.currentPage = 1;
							$scope.getTotalPages();

							if ($scope.wrkStnFilterList.length == 0) {
								$scope.workStationNoData = true;
								$scope.no_data_found = "No data found.";

							} else {
								$scope.workStationNoData = false;

								if ($scope.wrkStnFilterList.length > $scope.numPerPage) {
									$scope.wrkStnViewData = $scope.wrkStnFilterList
											.slice(0, $scope.numPerPage);
									$scope.PaginationTab = true;
								} else {
									$scope.wrkStnViewData = $scope.wrkStnFilterList;
									$scope.PaginationTab = false;
								}
							}

						} else if ($scope.varANDORDropDown == "OR") {

							if ($scope.firstMultiSearchDropDown == "All"
									&& ($scope.firstSearchText == "" || $scope.firstSearchText == undefined)) {
								$scope.disableSecondFilterText = true;
								$scope.disableSecondFilterDropDown = true;
								$scope.secondMultiSearchDropDown = "All"
								$scope.secondSearchText = ""

							} else {
								$scope.disableSecondFilterText = false;
								$scope.disableSecondFilterDropDown = false;
							}

							$scope.wrkStnViewData = []
							$scope.wrkStnFilterList = [];
							$scope.secondSearchORDataArray = [];
							for (var i = 0; i < $scope.wrkStnData.length; i++) {
								if ($scope.secondMultiSearchDropDown == "All") {
									if ($scope.secondSearchText == ""
											|| $scope.secondSearchText == undefined) {
										$scope.secondSearchORDataArray
												.push($scope.wrkStnData[i]);

									} else {
										var obj = $scope.wrkStnData[i];
										for (var x = 0; x < $scope.uniqueheaderList.length; x++) {
											var tableText = ($scope.wrkStnData[i][$scope.uniqueheaderList[x]])
													.toString();

											if ((tableText != undefined
													&& tableText != null ? tableText
													: "")
													.toLowerCase()
													.includes(
															$scope.secondSearchText
																	.toLowerCase())) {
												$scope.secondSearchORDataArray
														.push($scope.wrkStnData[i]);
												//break;									 
											}
										}
									}
								} else {

									// to check other than all in second search
									var secondDropDownValue = $scope.secondMultiSearchDropDown;
									for (var x = 0; x < $scope.uniqueheaderList.length; x++) {
										if ($scope.uniqueheaderList[x] == secondDropDownValue)
											var actualHeadersecondDropDownValue = $scope.uniqueheaderList[x];
									}
									if ($scope.secondSearchText == ""
											|| $scope.secondSearchText == undefined)

										$scope.secondSearchORDataArray
												.push($scope.wrkStnData[i]);
									else {
										var obj = $scope.wrkStnData[i];

										// var tableText =
										// JSON.stringify($scope.wrkStnData[i][actualHeadersecondDropDownValue]);
										var tableText = ($scope.wrkStnData[i][actualHeadersecondDropDownValue])
												.toString();
										if ((tableText != undefined
												&& tableText != null ? tableText
												: "").toLowerCase().includes(
												$scope.secondSearchText
														.toLowerCase())) {
											$scope.secondSearchORDataArray
													.push($scope.wrkStnData[i]);
											// break;
										}
									}

								}
							}
							// to concat first and second search arrays
							for (var i = 0; i < $scope.firstSearchANDDataArray.length; i++) {
								if (!$scope.secondSearchORDataArray
										.includes($scope.firstSearchANDDataArray[i]))
									$scope.secondSearchORDataArray
											.push($scope.firstSearchANDDataArray[i]);
							}
							$scope.wrkStnFilterList = $scope.secondSearchORDataArray;

							// for pagination

							$scope.totalItems = $scope.wrkStnFilterList.length;
							$scope.currentPage = 1;
							$scope.getTotalPages();

							if ($scope.wrkStnData.length == 0
									|| $scope.wrkStnFilterList.length == 0) {
								$scope.workStationNoData = true;
								$scope.no_data_found = "No data found.";

							} else {
								$scope.workStationNoData = false;

								if ($scope.wrkStnFilterList.length > $scope.numPerPage) {
									$scope.wrkStnViewData = $scope.wrkStnFilterList
											.slice(0, $scope.numPerPage);
									$scope.PaginationTab = true;
								} else {
									$scope.wrkStnViewData = $scope.wrkStnFilterList;
									$scope.PaginationTab = false;
								}
							}

						}

					}

				});