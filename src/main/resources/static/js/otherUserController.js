app
.controller(
	'otherUserController',
	function($scope, $http, $window, $rootScope, $location, $timeout) {
		$scope.user = JSON.parse($window.sessionStorage
				.getItem("user"));
		if ($scope.user == null) {
			$location.path('/login');
			return;
		}
		$scope.otherUserViewData  = new Array();
		$scope.otherUserFilterList = new Array();
		$scope.otherUserList = new Array();
		
		$scope.otherUserViewData.length =0;
		$scope.otherUserFilterList.length =0;
		$scope.otherUserList.length =0;
		$scope.searchText = "";
		 
		$scope.assignedTagMacId = '';
		$scope.trackMacId = '';
		$scope.showLocateBtn = false;		

		$scope.uniqueheaderList = ["visitorId", "firstName", "lastName", "contactNo", "type", "company", "contactPerson", "date", "fromTime", "toTime", "allocationEndTime"];		
		
		$scope.user = JSON.parse($window.sessionStorage
				.getItem("user"));
		
		if ($scope.user.capabilityDetails != undefined
				&& $scope.user.capabilityDetails != null) { 
			if ($scope.user.capabilityDetails.PLVW == 1) {
				$scope.showLocateBtn = true;
			}
			if ($scope.user.capabilityDetails.POTU == 1) {
				$scope.showViewAllBtn = true;
				$("#idOtherUserViewAll").addClass("active");
			} else {
				$scope.showViewAllBtn = false;
			}
		}
		 $scope.showSearchIcon = true;
		 $scope.showMultipleSearch = false;
		$scope.searchButton = true;
		$scope.otherUserViewScreen = true;
		$scope.getotherUserViewAllData = function() {
			
			if (!$rootScope.checkNetconnection()) { 				
			 	$rootScope.alertDialogChecknet();
				return; 
				}
			$rootScope.showloading('#loadingBar');
			$http({
				method : 'GET',
				url : './otherUsers',

				})
				.then(
				function success(response) {
					$rootScope.hideloading('#loadingBar');
				if (response != null
				&& response.data != null
				&& response.data != "BAD_REQUEST") {
					
				$scope.otherUserViewData.length =0;
				$scope.otherUserMainView = true;
				
				for (var i = 0; i < response.data.data.otherUsers.length; i++) {
					var fromTime ="",toTime ="",assignmentDate="" , returnedTime ="";
					if(response.data.data.otherUsers[i].fromTime != null && response.data.data.otherUsers[i].fromTime != "" && response.data.data.otherUsers[i].fromTime != undefined){
						fromTime = response.data.data.otherUsers[i].fromTime.substring(0, response.data.data.otherUsers[i].fromTime.length-3);
						}
					if(response.data.data.otherUsers[i].toTime != null && response.data.data.otherUsers[i].toTime != "" && response.data.data.otherUsers[i].toTime != undefined){
						toTime = response.data.data.otherUsers[i].toTime.substring(0, response.data.data.otherUsers[i].toTime.length-3);
					}
					if(response.data.data.otherUsers[i].date != null && response.data.data.otherUsers[i].date != "" && response.data.data.otherUsers[i].date != undefined){
						//response.data.data.otherUsers[i].date = response.data.data.otherUsers[i].date.substring(0, response.data.data.otherUsers[i].date.length-9);													
						assignmentDate = moment(response.data.data.otherUsers[i].date,"YYYY-MMM-DD HH:mm:ss").format("MMM DD, YYYY");
						}
					if(response.data.data.otherUsers[i].allocationEndTime != null && response.data.data.otherUsers[i].allocationEndTime != "" && response.data.data.otherUsers[i].allocationEndTime != undefined){
						//response.data.data.otherUsers[i].date = response.data.data.otherUsers[i].date.substring(0, response.data.data.otherUsers[i].date.length-9);													
						returnedTime = moment(response.data.data.otherUsers[i].allocationEndTime).format("MMM DD, YYYY HH:mm");
						}
				if(response.data.data.otherUsers[i] != "" && response.data.data.otherUsers[i] != undefined && response.data.data.otherUsers[i] != null){
				$scope.otherUserList.push({
						'visitorId': response.data.data.otherUsers[i].visitorId,
						'firstName': response.data.data.otherUsers[i].firstName,
						'lastName': response.data.data.otherUsers[i].lastName,
						'contactNo': response.data.data.otherUsers[i].contactNo,
						'type': response.data.data.otherUsers[i].type,
						'company': response.data.data.otherUsers[i].company,
						'contactPerson': response.data.data.otherUsers[i].contactPerson,
						'assignedTagMacId': response.data.data.otherUsers[i].assignedTagMacId,
						'assignedStatus': response.data.data.otherUsers[i].assignedStatus,
						'date':assignmentDate,
						'fromTime': fromTime,
						'toTime': toTime,
						'allocationEndTime':returnedTime
						
					});
				}
				}
				
				$scope.totalItems=$scope.otherUserList.length;
				$scope.getAssignedTagIds($scope.otherUserList);
				
				if($scope.otherUserViewData.length==0){
					$scope.otherUserNoData=true;					
					}else{
					$scope.otherUserNoData=false;
					
					}
				
			//	$scope.otherUserViewData.sort(GetSortOrder("visitorId"));
				$scope.getTotalPages();
				$scope.fnSearchFilterOption();
				
				} else {
				$scope.otherUserNoData = true;

				}
				},
				
				function error(response) {						
						$rootScope.hideloading('#loadingBar');						
						$rootScope.fnHttpError(response);
			    });
				}
		
	$scope.getotherUserViewAllData();
		// Search function
		$scope.fnOtherUserSearchOption = function() {
			$scope.fnSearchFilterOption();
		}
		
		$scope.trackTag = function() {
			if($scope.assignedTagMacId!=undefined && $scope.assignedTagMacId!=null 
					&& $scope.assignedTagMacId!='')
			{

				$rootScope.trackingMacId = $scope.assignedTagMacId;
				$rootScope.showFullView();
				var currentMenuName = $window.sessionStorage.getItem("menuName");
				if ($scope.user.role == "DemoUser")
				{
					$window.sessionStorage.setItem("menuName", "shopFloorAnalysis");
					$window.sessionStorage.setItem("demoMenuName", "peopleLiveView");
					$location.path('/shopFloorAnalysis');
				}else{
					$window.sessionStorage.setItem("menuName", "peopleLiveView");
					$location.path('/peopleLiveView/'+$scope.assignedTagMacId); 
				}
				$rootScope.$broadcast('sourcePreviousScreen',currentMenuName);
			}else{
				toastr.error('No assigned tags to track.', "EI4.0");  
			}
			 
		}
		
		$scope.trackSingleTag = function() {
			if ($scope.trackMacId != undefined
					&& $scope.trackMacId != null
					&& $scope.trackMacId != '') {
				$rootScope.trackingMacId = $scope.trackMacId;
				$rootScope.showFullView();
				var currentMenuName = $window.sessionStorage.getItem("menuName");
				if ($scope.user.role == "DemoUser")
				{
					$window.sessionStorage.setItem("menuName", "shopFloorAnalysis");
					$window.sessionStorage.setItem("demoMenuName", "peopleLiveView");
					$location.path('/shopFloorAnalysis');
				}else{
					$window.sessionStorage.setItem("menuName", "peopleLiveView");
					$location.path('/peopleLiveView');
				}
				$rootScope.$broadcast('sourcePreviousScreen',currentMenuName);
			} else {
				toastr.error('No assigned tags to track.', "EI4.0");
			}
		}
		
		//Search Filter
		$scope.fnSearchFilterOption = function() {
			$scope.otherUserViewData.length = 0;
			$scope.otherUserFilterList.length = 0;
			
			 
			for (var i = 0; i < $scope.otherUserList.length; i++) {
					if ($scope.searchText == '') {
						$scope.otherUserFilterList
								.push($scope.otherUserList[i]);
					} else {
						if ($scope.otherUserList[i].type.toLowerCase().includes($scope.searchText.toLowerCase())
								||(($scope.otherUserList[i].visitorId)
										.toString() != undefined
										&& ($scope.otherUserList[i].visitorId)
												.toString() != null ? ($scope.otherUserList[i].visitorId)
										.toString()
										: "").toLowerCase().includes(
										$scope.searchText.toLowerCase())
								||($scope.otherUserList[i].firstName != undefined
										&& $scope.otherUserList[i].firstName != null ? $scope.otherUserList[i].firstName: "").toLowerCase()
										.includes($scope.searchText.toLowerCase())
								|| ($scope.otherUserList[i].lastName != undefined
										&& $scope.otherUserList[i].lastName != null ? $scope.otherUserList[i].lastName
										: "")
										.toLowerCase()
										.includes(
												$scope.searchText
														.toLowerCase())
								|| ($scope.otherUserList[i].contactNo != undefined
										&& $scope.otherUserList[i].contactNo != null ? $scope.otherUserList[i].contactNo
										: "")
										.toLowerCase()
										.includes(
												$scope.searchText
														.toLowerCase())
								|| ($scope.otherUserList[i].company != undefined
										&& $scope.otherUserList[i].company != null ? $scope.otherUserList[i].company
										: "")
										.toLowerCase()
										.includes(
												$scope.searchText
														.toLowerCase())
								|| ($scope.otherUserList[i].contactPerson != undefined
										&& $scope.otherUserList[i].contactPerson != null ? $scope.otherUserList[i].contactPerson
										: "")
										.toLowerCase()
										.includes(
												$scope.searchText
														.toLowerCase())
								|| ($scope.otherUserList[i].date != undefined
										&& $scope.otherUserList[i].date != null ? $scope.otherUserList[i].date
										: "")
										.toLowerCase()
										.includes(
												$scope.searchText
														.toLowerCase())
								|| ($scope.otherUserList[i].fromTime != undefined
										&& $scope.otherUserList[i].fromTime != null ? $scope.otherUserList[i].fromTime
										: "")
										.toLowerCase()
										.includes(
												$scope.searchText
														.toLowerCase())
								|| ($scope.otherUserList[i].toTime != undefined
										&& $scope.otherUserList[i].toTime != null ? $scope.otherUserList[i].toTime
										: "")
								 		.toLowerCase()
										.includes(
												$scope.searchText
														.toLowerCase())) {
							$scope.otherUserFilterList
									.push($scope.otherUserList[i]);

						}
					}				
			}
			

			$scope.totalItems = $scope.otherUserFilterList.length;
			$scope.currentPage = 1;
			$scope.getTotalPages();
			$scope.getAssignedTagIds($scope.otherUserFilterList);
			 if($scope.otherUserList.length == 0 || $scope.otherUserFilterList.length == 0 ){
					$scope.otherUserNoData = true;

				}else{
					$scope.otherUserNoData = false;

			if ($scope.otherUserFilterList.length > $scope.numPerPage){
				$scope.otherUserViewData = $scope.otherUserFilterList
						.slice(0, $scope.numPerPage);
			$scope.PaginationTab=true;
			}
			else {
				$scope.otherUserViewData = $scope.otherUserFilterList;
				$scope.PaginationTab=false;
			}
		}		
			
		}
		
		$scope.cancelEdit = function() {
			$scope.selectedRow = -1;
			$scope.trackMacId = '';
		}
		
		// row highlighting		
		 $scope.rowHighilited = function(row) {
			 $scope.selectedRow = row;			 
			 $scope.authen = JSON.parse(JSON.stringify($scope.otherUserViewData[row])); 
			 
             $scope.assignedStatus = $scope.authen.assignedStatus;
             if($scope.assignedStatus=='Assigned'){
                 $scope.trackMacId = $scope.authen.assignedTagMacId;  
             }else{
                 $scope.trackMacId = ''; 
             }
			 
		 }
		 
		 $scope.getAssignedTagIds = function(assetList) { 
				
				$scope.assignedTagMacId = '';
				
				if(assetList!=undefined && assetList!=null){
					for(var i=0; i<assetList.length; i++){
						 if(assetList[i].assignedStatus=='Assigned'){
								if(assetList[i].assignedTagMacId !=undefined 
										&& assetList[i].assignedTagMacId !=''){
									if($scope.assignedTagMacId=='')
										$scope.assignedTagMacId = assetList[i].assignedTagMacId;
									else
										$scope.assignedTagMacId = $scope.assignedTagMacId+","+assetList[i].assignedTagMacId;
										
								}
							}
					 } 
				}
				 
			}
		 
//pagination button 
		
		$scope.currentPage = 1;
		$scope.numPerPage = 10;
		$scope.maxSize = 3;
		$scope.totalItems = 0;
		
		$scope
				.$watch(
						'currentPage + numPerPage',
						function() {
							var begin = (($scope.currentPage - 1) * $scope.numPerPage), end = begin
									+ $scope.numPerPage;

							if ($scope.searchText == '')
								$scope.otherUserViewData = $scope.otherUserList
										.slice(begin, end);
							else
								$scope.otherUserViewData = $scope.otherUserFilterList
										.slice(begin, end);

							
						});

		$scope.currentPage = 1;
		$scope.maxSize = 3;
		$scope.numPerPage = 10;
		$scope.totalItems = 0;
		$scope.totalPages = 0;

		$scope.hasNext = true;
		$scope.hasPrevious = true;
		$scope.isFirstPage = true;
		$scope.isLastPage = true;
		$scope.serial = 1;

		$scope
				.$watch(
						"currentPage",
						function(newVal, oldVal) {
							if (newVal !== oldVal) {

								if (newVal > $scope.totalPages) {
									$scope.currentPage = $scope.totalPages;
								} else {
									$scope.currentPage = newVal;
								}

								$scope.serial = (($scope.currentPage - 1) * $scope.numPerPage) + 1;

							}
						});

		$scope.enablePageButtons = function() {
			$scope.serial = (($scope.currentPage - 1) * $scope.numPerPage) + 1;
			$scope.selectedRow = -1;
			if ($scope.totalPages <= 1) {
				$scope.hasPrevious = true;
				$scope.isFirstPage = true;
				$scope.hasNext = true;
				$scope.isLastPage = true;

			} else if ($scope.totalPages > 1
					&& $scope.currentPage == $scope.totalPages) {

				$scope.hasPrevious = false;
				$scope.isFirstPage = false;
				$scope.hasNext = true;
				$scope.isLastPage = true;
			} else if ($scope.currentPage != 1
					&& $scope.currentPage < $scope.totalPages) {

				$scope.hasPrevious = false;
				$scope.isFirstPage = false;
				$scope.hasNext = false;
				$scope.isLastPage = false;

			} else {
				$scope.hasPrevious = true;
				$scope.isFirstPage = true;
				$scope.hasNext = false;
				$scope.isLastPage = false;
			}
			
 
		};

		$scope.getTotalPages = function() {

			if (($scope.totalItems % $scope.numPerPage) > 0)
				$scope.totalPages = parseInt($scope.totalItems
						/ $scope.numPerPage) + 1;
			else
				$scope.totalPages = parseInt($scope.totalItems
						/ $scope.numPerPage);

			$scope.enablePageButtons();

		};

		$scope.goFirstPage = function() {
			$scope.currentPage = 1;
			$scope.enablePageButtons();
		};

		$scope.goPreviousPage = function() {
			if ($scope.currentPage > 1)
				$scope.currentPage = $scope.currentPage - 1;
			$scope.enablePageButtons();
		};

		$scope.goNextPage = function() {
			if ($scope.currentPage <= $scope.totalPages)
				$scope.currentPage = $scope.currentPage + 1;
			$scope.enablePageButtons();
		};

		$scope.goLastPage = function() {
			$scope.currentPage = $scope.totalPages;
			$scope.enablePageButtons();
		};
		// multisearch code starts
		$scope.multipleSearch = function() {
			$scope.showMultipleSearch = true;
			$scope.searchButton = false
			$scope.firstMultiSearchDropDown = "All";
			$scope.secondMultiSearchDropDown = "All";
			$scope.varANDORDropDown = "AND";
			$scope.firstSearchText = "";
			$scope.secondSearchText = "";
			$scope.showSearchBar = false;
		}
		$scope.closeMultiSearchView = function() {
			$scope.showMultipleSearch = false;
			$scope.searchButton = true
			
			// for pagination
			
			$scope.otherUserFilterList = $scope.otherUserList;
			$scope.totalItems = $scope.otherUserFilterList.length;
			$scope.currentPage = 1;
			$scope.getTotalPages();			
			if ($scope.otherUserList.length == 0
					|| $scope.otherUserFilterList.length == 0) {
				$scope.otherUserNoData = true;
				$scope.no_data_found = "No data found.";

			} else {
				$scope.otherUserNoData = false;

				if ($scope.otherUserFilterList.length > $scope.numPerPage) {
					$scope.otherUserViewData =  $scope.otherUserFilterList.slice(0, $scope.numPerPage);
					$scope.PaginationTab = true;
				} else {
					$scope.otherUserViewData =  $scope.otherUserFilterList;
					$scope.PaginationTab = false;
				}
			}
			
			
		}
		
		$scope.fnfirstSearchANDData = function() {
			
			$scope.firstSearchANDDataArray = [];						
			
			for (var i = 0; i < $scope.otherUserList.length; i++) {
				if ($scope.firstMultiSearchDropDown == "All") {
					if ($scope.firstSearchText == ""
						|| $scope.firstSearchText == undefined) {
					
					$scope.firstSearchANDDataArray
							.push($scope.otherUserList[i]);
				} else {
					var obj = {};
					obj = $scope.otherUserList[i];
					 for(var x = 0; x < $scope.uniqueheaderList.length; x++){								
						 var tableText = ($scope.otherUserList[i][$scope.uniqueheaderList[x]]).toString();								
					
					if ((tableText != undefined && tableText != null ? tableText : "").toLowerCase()
									.includes($scope.firstSearchText.toLowerCase()))								
						{
							$scope.firstSearchANDDataArray.push($scope.otherUserList[i]);										
							//break;									 
						}	
				 }
					
			}
				}
				// to check other than all
				else {
					var firstDropDownValue = $scope.firstMultiSearchDropDown;
					 for(var x = 0; x < $scope.uniqueheaderList.length; x++){			
						 if( $scope.uniqueheaderList[x] == firstDropDownValue )
							var actualHeaderfirstDropDownValue = $scope.uniqueheaderList[x];
					 }
						if ($scope.firstSearchText == "" || $scope.firstSearchText == undefined) 
							$scope.firstSearchANDDataArray.push($scope.otherUserList[i]);
						else{
						var obj = $scope.otherUserList[i];
						
						// var tableText =
						// JSON.stringify($scope.otherUserList[i][actualHeaderfirstDropDownValue]);
						//console.log(actualHeaderfirstDropDownValue)
						//console.log($scope.otherUserList[i][actualHeaderfirstDropDownValue])
						
						var tableText = ($scope.otherUserList[i][actualHeaderfirstDropDownValue]).toString();	
						if((tableText != undefined && tableText != null ? tableText : "").toLowerCase()
										.includes($scope.firstSearchText.toLowerCase()))								
							{
								$scope.firstSearchANDDataArray.push($scope.otherUserList[i]);										
								//break;									 
							}		
					

					} 

				}
				
		} 
			
		}

		$scope.multiSearchChanged = function() {
			
			$scope.fnfirstSearchANDData();
		// console.log($scope.firstSearchANDDataArray);

			if ($scope.varANDORDropDown == "AND") {
				$scope.disableSecondFilterText = false;
				$scope.disableSecondFilterDropDown = false;
				$scope.otherUserViewData = [];
				$scope.otherUserFilterList = [];

				for (var i = 0; i < $scope.firstSearchANDDataArray.length; i++) {
					if ($scope.secondMultiSearchDropDown == "All") {

						if ($scope.secondSearchText == ""
								|| $scope.secondSearchText == undefined) {
							$scope.otherUserFilterList
									.push($scope.firstSearchANDDataArray[i]);
						} else {

							var obj = $scope.otherUserList[i];
							 for(var x = 0; x < $scope.uniqueheaderList.length; x++){								
								 var tableText = ($scope.firstSearchANDDataArray[i][$scope.uniqueheaderList[x]]).toString();								
							
							if ((tableText != undefined && tableText != null ? tableText : "").toLowerCase()
											.includes($scope.secondSearchText.toLowerCase()))								
								{													
									$scope.otherUserFilterList.push($scope.firstSearchANDDataArray[i]);
									//break;									 
								}	
						 }	
						}
					} else {

						var secondDropDownValue = $scope.secondMultiSearchDropDown;	
						for(var x = 0; x < $scope.uniqueheaderList.length; x++){			
							 if( $scope.uniqueheaderList[x] == secondDropDownValue )
								var actualHeadersecondDropDownValue = $scope.uniqueheaderList[x];
						 }
							if ($scope.secondSearchText == "" || $scope.secondSearchText == undefined) 
								
							$scope.otherUserFilterList.push($scope.firstSearchANDDataArray[i]);
							else{
							var obj = $scope.otherUserList[i];							
							
							var tableText = ($scope.otherUserList[i][actualHeadersecondDropDownValue]).toString();	
							if((tableText != undefined && tableText != null ? tableText : "").toLowerCase()
											.includes($scope.secondSearchText.toLowerCase()))								
								{
								$scope.otherUserFilterList.push($scope.firstSearchANDDataArray[i]);									
									
								}
						} 

					}
				}
				
				
				$scope.totalItems = $scope.otherUserFilterList.length;
				$scope.currentPage = 1;
				$scope.getTotalPages();
			 	

				if ($scope.otherUserFilterList.length == 0) {
					$scope.otherUserNoData = true;
					$scope.no_data_found = "No data found.";

				} else {
					$scope.otherUserNoData = false;

					
					if ($scope.otherUserFilterList.length > $scope.numPerPage) {
						$scope.otherUserViewData = $scope.otherUserFilterList
								.slice(0, $scope.numPerPage);
						$scope.PaginationTab = true;
					} else {
						$scope.otherUserViewData = $scope.otherUserFilterList;
						$scope.PaginationTab = false;
					}
				}
				
				
			} else if ($scope.varANDORDropDown == "OR") {

				if ($scope.firstMultiSearchDropDown == "All"
						&& ($scope.firstSearchText == "" || $scope.firstSearchText == undefined)) {
					$scope.disableSecondFilterText = true;
					$scope.disableSecondFilterDropDown = true;
					$scope.secondMultiSearchDropDown = "All"
					$scope.secondSearchText = ""

				} else {
					$scope.disableSecondFilterText = false;
					$scope.disableSecondFilterDropDown = false;
				}
				
				$scope.otherUserViewData = []
				$scope.otherUserFilterList = [];
				$scope.secondSearchORDataArray = [];
				for (var i = 0; i < $scope.otherUserList.length; i++) {
					if ($scope.secondMultiSearchDropDown == "All") {
						if ($scope.secondSearchText == ""
								|| $scope.secondSearchText == undefined) {
							$scope.secondSearchORDataArray
									.push($scope.otherUserList[i]);

						} else {
							var obj = $scope.otherUserList[i];
							 for(var x = 0; x < $scope.uniqueheaderList.length; x++){								
								 var tableText = ($scope.otherUserList[i][$scope.uniqueheaderList[x]]).toString();								
							
							if ((tableText != undefined && tableText != null ? tableText : "").toLowerCase()
											.includes($scope.secondSearchText.toLowerCase()))								
								{													
								$scope.secondSearchORDataArray.push($scope.otherUserList[i]);
									//break;									 
								}	
						 }
						}
					} else {

						// to check other than all in second search
						var secondDropDownValue = $scope.secondMultiSearchDropDown;	
						for(var x = 0; x < $scope.uniqueheaderList.length; x++){			
							 if( $scope.uniqueheaderList[x] == secondDropDownValue )
								var actualHeadersecondDropDownValue = $scope.uniqueheaderList[x];
						 }
							if ($scope.secondSearchText == "" || $scope.secondSearchText == undefined) 
								
							$scope.secondSearchORDataArray.push($scope.otherUserList[i]);
							else{
							var obj = $scope.otherUserList[i];
							
							// var tableText =
							// JSON.stringify($scope.otherUserList[i][actualHeadersecondDropDownValue]);
							var tableText = ($scope.otherUserList[i][actualHeadersecondDropDownValue]).toString();	
							if((tableText != undefined && tableText != null ? tableText : "").toLowerCase()
											.includes($scope.secondSearchText.toLowerCase()))								
								{
								$scope.secondSearchORDataArray.push($scope.otherUserList[i]);									
									// break;
								}
						}

					}
				}
				// to concat first and second search arrays
				for (var i = 0; i < $scope.firstSearchANDDataArray.length; i++) {
					if (!$scope.secondSearchORDataArray
							.includes($scope.firstSearchANDDataArray[i]))
						$scope.secondSearchORDataArray
								.push($scope.firstSearchANDDataArray[i]);
				}
				$scope.otherUserFilterList = $scope.secondSearchORDataArray;
				
				// for pagination
				
				$scope.totalItems = $scope.otherUserFilterList.length;
				$scope.currentPage = 1;
				$scope.getTotalPages();

				
				if ($scope.otherUserList.length == 0
						|| $scope.otherUserFilterList.length == 0) {
					$scope.otherUserNoData = true;
					$scope.no_data_found = "No data found.";

				} else {
					$scope.otherUserNoData = false;

					if ($scope.otherUserFilterList.length > $scope.numPerPage) {
						$scope.otherUserViewData =  $scope.otherUserFilterList.slice(0, $scope.numPerPage);
						$scope.PaginationTab = true;
					} else {
						$scope.otherUserViewData =  $scope.otherUserFilterList;
						$scope.PaginationTab = false;
					}
				}
				
			}
		

		}
	});