app
	.controller(
		'dashboardController',
		function($scope, $http, $window, $rootScope,$timeout, $location) {
			$scope.records ={
					"contractor": 0,
					"assetNonChrono": 0,
					"assetChrono": 0,
					"materials": 0,
					"vendor": 0,
					"vehicles": 0,
					"employee": 0,
					"visitor": 0,
					"sku": 0
					};
			
			$scope.user = JSON.parse($window.sessionStorage
					.getItem("user"));
			if ($scope.user == null) {
				$location.path('/login');
				return;
			}
			$scope.activeLocators = 0;
			 $scope.inActiveLocators = 0;
			 $scope.waitingLocators = 0;
			 $scope.discoveredLocators = 0; 
			 $scope.fwUpgradeLocators = 0; 
			 $scope.totalLocators = 0;
			$scope.$on('$destroy', function() { 
				  clearInterval($scope.locatorStatusUpdateInterval);
				  clearInterval($scope.memoryChartDataInterval);
				  clearInterval($scope.tagsStatusUpdateInterval);  
			});
				
			$http({
				method : 'GET',
				url : './dashboardInfo',
			})
					.then(
							function success(response) {
								if (response != null
										&& response.data != null) {
									$scope.records = response.data.data.assignedTags;
								}
							},
							function error(response) {
								$rootScope.fnHttpError(response);
							});	

			
			  var ticksStyle = {
					    fontColor: '#495057',
					    fontStyle: 'bold'
					  }

					  var mode = 'index'
					  var intersect = true
					  
					  
					  var $tagsChart = $('#idTagsChart')
					   var tagsChart = new Chart($tagsChart, {
					    type: 'bar',
					    data: {
					      labels: ['Tags'],
					      datasets: [
					    	  {
						          backgroundColor: '#008000',
						          borderColor: '#008000',
						          data: [0]
						      },
					         {
						          backgroundColor: '#ff0000',
						          borderColor: '#ff0000',
						          data: [0]
					        },
					        {
						          backgroundColor: '#ffff00',
						          borderColor: '#ffff00',
						          data: [0]
					        } 
					      ]
					    },
					   
					    options: {
					      maintainAspectRatio: false,
					      tooltips: {
					        mode: mode,
					        intersect: intersect,
					        enabled: false
					      },
					      animation: {
						        "duration": 1,
						        "onComplete": function() {
						          var chartInstance = this.chart,
						            ctx = chartInstance.ctx;
						         
						          Chart.defaults.global.defaultFontStyle='Bold';
						          ctx.font = Chart.helpers.fontString(Chart.defaults.global.defaultFontSize+4, Chart.defaults.global.defaultFontStyle, Chart.defaults.global.defaultFontFamily);
						          ctx.textAlign = 'center';
						          ctx.textBaseline = 'bottom';

						          this.data.datasets.forEach(function(dataset, i) {
						            var meta = chartInstance.controller.getDatasetMeta(i);
						            meta.data.forEach(function(bar, index) {
						              var data = dataset.data[index];
						              ctx.fillText(data, bar._model.x, bar._model.y -1);
						            });
						          });
						        }
						      },
					      hover: {
					        mode: mode,
					        intersect: intersect
					      },
					      legend: {
					        display: false
					      },
					      scales: {
					        yAxes: [{
					          // display: false,
					          gridLines: {
					            display: true,
					            lineWidth: '4px',
					            color: 'rgba(0, 0, 0, .2)',
					            zeroLineColor: 'transparent'
					          },
					          ticks: $.extend({
					            beginAtZero: true,
	                                steps: 5,
	                                stepValue: 5,
	                                max: 60,
	                           
					            // Include a dollar sign in the ticks
					            callback: function (value) {
					            	return value;
					              /*
									 * if (value >= 1000) { value /= 1000 value +=
									 * 'k' }
									 * 
									 * return '$' + value
									 */
					            }
					          }, ticksStyle)
					        }],
					        xAxes: [{
					          display: true,
					          gridLines: {
					            display: false
					          },
					          ticks: ticksStyle
					        }]
					      }
					    }
					  });

					  var $locatorChart = $('#idLocatorChart')
					  // eslint-disable-next-line no-unused-vars
					  var locatorChart = new Chart($locatorChart, {
					    type: 'bar',
					    data: {
					      labels: ['Locator'],
					      datasets: [
					    	  {
						          backgroundColor: '#008000',
						          borderColor: '#008000',
						          data: [0]
						      },
					         {
						          backgroundColor: '#ff0000',
						          borderColor: '#ff0000',
						          data: [0]
					        },
					        {
						          backgroundColor: '#ffff00',
						          borderColor: '#ffff00',
						          data: [0]
					        },
					        {
						          backgroundColor: '#0000ff',
						          borderColor: '#0000ff',
						          data: [0]
						    },
					        {
						          backgroundColor: '#800040',
						          borderColor: '#800040',
						          data: [0]
					        } 
					      ]
					    },
					   
					    options: {
					      maintainAspectRatio: false,
					      tooltips: {
					        mode: mode,
					        intersect: intersect,
					        enabled: false
					      },
					      animation: {
						        "duration": 1,
						        "onComplete": function() {
						          var chartInstance = this.chart,
						            ctx = chartInstance.ctx;
						         
						          Chart.defaults.global.defaultFontStyle='Bold';
						          ctx.font = Chart.helpers.fontString(Chart.defaults.global.defaultFontSize+4, Chart.defaults.global.defaultFontStyle, Chart.defaults.global.defaultFontFamily);
						          ctx.textAlign = 'center';
						          ctx.textBaseline = 'bottom';

						          this.data.datasets.forEach(function(dataset, i) {
						            var meta = chartInstance.controller.getDatasetMeta(i);
						            meta.data.forEach(function(bar, index) {
						              var data = dataset.data[index];
						              ctx.fillText(data, bar._model.x, bar._model.y -1);
						            });
						          });
						        }
						      },
					      hover: {
					        mode: mode,
					        intersect: intersect
					      },
					      legend: {
					        display: false
					      },
					      scales: {
					        yAxes: [{
					          // display: false,
					          gridLines: {
					            display: true,
					            lineWidth: '4px',
					            color: 'rgba(0, 0, 0, .2)',
					            zeroLineColor: 'transparent'
					          },
					          ticks: $.extend({
					            beginAtZero: true,
	                                steps: 5,
	                                stepValue: 5,
	                                max: 60,
	                           
					            // Include a dollar sign in the ticks
					            callback: function (value) {
					            	return value;
					              /*
									 * if (value >= 1000) { value /= 1000 value +=
									 * 'k' }
									 * 
									 * return '$' + value
									 */
					            }
					          }, ticksStyle)
					        }],
					        xAxes: [{
					          display: true,
					          gridLines: {
					            display: false
					          },
					          ticks: ticksStyle
					        }]
					      }
					    }
					  })	
			  
			  
			  $scope.updateMemoryChartData = function(usedMemoryVal, cpuUsageVal, qpeMemoryUsageVal) {
				  var usedMemory = $('#chart-usedMemory');
				  
				  
				  if(usedMemoryVal==undefined || usedMemoryVal==null || usedMemoryVal=='')
					  usedMemoryVal=0;
				  
				  if(cpuUsageVal==undefined || cpuUsageVal==null || cpuUsageVal=='')
					  cpuUsageVal=0;
				  
				  if(qpeMemoryUsageVal==undefined || qpeMemoryUsageVal==null || qpeMemoryUsageVal=='')
					  qpeMemoryUsageVal=0;
				  
				  
				  var usedMemoryColorValue = "#ff7143";
				  if(usedMemoryVal<=30)
					  usedMemoryColorValue = "#0fdc63";
				  else if(usedMemoryVal<=70)
					  usedMemoryColorValue = "#fd9704";
				  else 
					  usedMemoryColorValue = "#ff7143";
				  
				  var cpuUsageColorValue = "#ff7143";
				  if(cpuUsageVal<=30)
					  cpuUsageColorValue = "#0fdc63";
				  else if(cpuUsageVal<=70)
					  cpuUsageColorValue = "#fd9704";
				  else 
					  cpuUsageColorValue = "#ff7143";
				  
				  var qpeUsageColorValue = "#ff7143";
				  if(qpeMemoryUsageVal<=30)
					  qpeUsageColorValue = "#0fdc63";
				  else if(qpeMemoryUsageVal<=70)
					  qpeUsageColorValue = "#fd9704";
				  else 
					  qpeUsageColorValue = "#ff7143";
				  
				  new Chart(usedMemory, {
				  	type: "tsgauge",
				  	data: {
				  		datasets: [{
				  			backgroundColor: ["#0fdc63", "#fd9704", "#ff7143"],
				  			borderWidth: 0,
				  			gaugeData: {
				  				value: usedMemoryVal,
				  				valueColor: usedMemoryColorValue
				  			},
				  			gaugeLimits: [0, 30, 70, 100]
				  		}]
				  	},
				  	options: {
				              events: [],
				              showMarkers: true
				  	}
				  });
				  
				  var cpuUsage = $('#chart-cpuUsage');  
				  new Chart(cpuUsage, {
				  	type: "tsgauge",
				  	data: {
				  		datasets: [{
				  			backgroundColor: ["#0fdc63", "#fd9704", "#ff7143"],
				  			borderWidth: 0,
				  			gaugeData: {
				  				value: cpuUsageVal,
				  				valueColor: cpuUsageColorValue
				  			},
				  			gaugeLimits: [0, 30, 70, 100]
				  		}]
				  	},
				  	options: {
				              events: [],
				              showMarkers: true
				  	}
				  });
				  
				  
				  var qpeMemoryUsage = $('#chart-qpeMemory');// document.getElementById("chart-qpeMemory").getContext("2d");
				  new Chart(qpeMemoryUsage, {
				  	type: "tsgauge",
				  	data: {
				  		datasets: [{
				  			backgroundColor: ["#0fdc63", "#fd9704", "#ff7143"],
				  			borderWidth: 0,
				  			gaugeData: {
				  				value: qpeMemoryUsageVal,
				  				valueColor: qpeUsageColorValue
				  			},
				  			gaugeLimits: [0, 30, 70, 100]
				  		}]
				  	},
				  	options: {
				              events: [],
				              showMarkers: true
				  	}
				  });
				  
			  }
			  
			  $scope.updateMemoryChartData(0, 0, 0);
			  
			  $scope.getMemoryChartData =   function() {
				  
				  $http({
						method : 'GET',
						url : './deviceDetails',
					})
							.then(
									function success(response) {
										if (response != null
												&& response.data != null) {
											  
											 var deviceDetails = response.data.data;
											 
											 if(deviceDetails!=undefined && deviceDetails!=null){
												 
												 var cpuLoad = 0;
												 var RAM_USED_PERCENTAGE = 0;
												 var QPE_RAM_USED_PERCENTAGE = 0;
												 
												 $scope.systemMemory = deviceDetails['systemMemory']; 
												  
												 $scope.jvmMemory = deviceDetails['jvmMemory']; 
												  
												 if( $scope.systemMemory!=undefined &&  $scope.systemMemory!=null &&  $scope.systemMemory!='')
													 RAM_USED_PERCENTAGE = ($scope.systemMemory['used']/ $scope.systemMemory['total']) * 100;
												  
												 if( $scope.jvmMemory!=undefined &&  $scope.jvmMemory!=null &&  $scope.jvmMemory!='')
												 {
													 QPE_RAM_USED_PERCENTAGE = ($scope.jvmMemory['used']/ $scope.jvmMemory['total']) * 100;
													 cpuLoad = Math.round($scope.jvmMemory['cpuLoad'])
												 }

												 $scope.updateMemoryChartData(Math.round(RAM_USED_PERCENTAGE),  cpuLoad,  Math.round(QPE_RAM_USED_PERCENTAGE));
											 }   
											 
										}
									},
									function error(response) {
										$rootScope.fnHttpError(response);
									});	
			  }
			  
			  $scope.getMemoryChartData()
			  
			  
			  $scope.memoryChartDataInterval =  setInterval($scope.getMemoryChartData, 60*1000);
			  $scope.updateLocatorStatuData = function() {
				  
				 
				  
				  $http({
						method : 'GET',
						url : './locatorDetails/status',
					})
							.then(
									function success(response) {
										if (response != null
												&& response.data != null) { 
											 var locator = response.data.locator;
											 $scope.activeLocators = 0;
											 $scope.inActiveLocators = 0;
											 $scope.waitingLocators = 0;
											 $scope.discoveredLocators = 0; 
											 $scope.fwUpgradeLocators = 0; 
											 $scope.totalLocators = 0;
											 if(locator!=undefined && locator!=null && locator.length>0){
												 $scope.totalLocators = locator.length;
												 for (var i = 0; i < locator.length; i++) {
														if (locator[i].status.toLowerCase() == 'ok') {
															 $scope.activeLocators++;
														}else if (locator[i].status.toLowerCase() == 'failed'
															||locator[i].status.toLowerCase() == 'notconnectable') { 
															 $scope.inActiveLocators++;
														}else if (locator[i].status.toLowerCase() == 'waiting') {
															 $scope.waitingLocators++;
														}else if (locator[i].status.toLowerCase() == 'discovered') {
															 $scope.discoveredLocators++;
														}else if (locator[i].status.toLowerCase() == 'fwupgrade') {
															 $scope.fwUpgradeLocators++;
														}
													}
											 }  
											 
											 locatorChart.data.datasets[0].data[0] =  $scope.activeLocators;
											 locatorChart.data.datasets[1].data[0] =  $scope.inActiveLocators;
											 locatorChart.data.datasets[2].data[0] =  $scope.waitingLocators;
											 locatorChart.data.datasets[3].data[0] =  $scope.discoveredLocators;
											 locatorChart.data.datasets[4].data[0] =  $scope.fwUpgradeLocators;  
											 locatorChart.update();
											 
										}
									},
									function error(response) {

										$scope.totalLocators = 0; 
										$scope.activeLocators = 0;
										$scope.inActiveLocators = 0;
										$scope.waitingLocators = 0;
										$scope.discoveredLocators = 0; 
										$scope.fwUpgradeLocators = 0;  
										 locatorChart.data.datasets[0].data[0] =  $scope.activeLocators;
										 locatorChart.data.datasets[1].data[0] =  $scope.inActiveLocators;
										 locatorChart.data.datasets[2].data[0] =  $scope.waitingLocators;
										 locatorChart.data.datasets[3].data[0] =  $scope.discoveredLocators;
										 locatorChart.data.datasets[4].data[0] =  $scope.fwUpgradeLocators;  
										 locatorChart.update();
										 
										 $rootScope.fnHttpError(response);
									});	
				  
				  
			  }
			  
			  
			  
			  
			  $scope.updateTagsStatuData = function() { 
				  
				  $http({
						method : 'GET',
						url : './getTagsCount',
					})
							.then(
									function success(response) {
										if (response != null
												&& response.data != null) { 
											 var tags = response.data.tags;
											 $scope.totalTags = 0;
											 $scope.onLineTags = 0; 
											 $scope.missingTags = 0; 
											 $scope.batteryOffTags = 0; 
											 
											 if(tags!=undefined && tags!=null && tags!=''){ 
												 $scope.totalTags = tags['total']; 
												 $scope.onLineTags = tags['online']; 
												 $scope.missingTags = tags['missing']; 
												 $scope.batteryOffTags = tags['batteryOff'];  
											 }  
											 
											 tagsChart.data.datasets[0].data[0] =  $scope.onLineTags;
											 tagsChart.data.datasets[1].data[0] =  $scope.missingTags;
											 tagsChart.data.datasets[2].data[0] =  $scope.batteryOffTags; 
											 tagsChart.update();
											 
										}
									},
									function error(response) {

										 $scope.totalTags = 0;
										 $scope.onLineTags = 0; 
										 $scope.missingTags = 0; 
										 $scope.batteryOffTags = 0; 
										 tagsChart.data.datasets[0].data[0] =  $scope.onLineTags;
										 tagsChart.data.datasets[1].data[0] =  $scope.missingTags;
										 tagsChart.data.datasets[2].data[0] =  $scope.batteryOffTags; 
										 tagsChart.update();
										 
										 $rootScope.fnHttpError(response);
									});	
				  
				  
			  }
			  
			  $scope.viewTagDetails = function() {
				  // clearInterval($scope.locatorStatusUpdateInterval);
				  //toastr.info("Tags Details View", "EI4.0");
			  }
			  
			  $scope.viewLocatorsDetails = function() {
				  // clearInterval($scope.locatorStatusUpdateInterval);
				  //toastr.info("LocatorDetailsView", "EI4.0");
				  $rootScope.showFullView();
				  var currentMenuName = $window.sessionStorage.getItem("menuName");
  				  $window.sessionStorage.setItem("menuName", "zoneManagement");
				  $location.path('/locatorInfo'); 
				  $rootScope.$broadcast('sourcePreviousScreen',currentMenuName);
			  }
			  
			  
			  $scope.updateLocatorStatuData()
			  $scope.locatorStatusUpdateInterval =  setInterval($scope.updateLocatorStatuData, 60*1000);
			  
			  
			  $scope.updateTagsStatuData()
			  $scope.tagsStatusUpdateInterval =  setInterval($scope.updateTagsStatuData, 60*1000);
			  
			  var $cpuUsageChart = $('#idCPUUsagechart')
			  // eslint-disable-next-line no-unused-vars
			  var cpuUsageChart = new Chart($cpuUsageChart, {
				  type: 'doughnut',
				  plugins: [{
				    afterDraw: chart => {
				      var needleValue = chart.config.data.datasets[0].needleValue;
				      var dataTotal = chart.config.data.datasets[0].data.reduce((a, b) => a + b, 0);
				      var angle = Math.PI + (1 / dataTotal * needleValue * Math.PI);
				      var ctx = chart.ctx;
				      var cw = chart.canvas.offsetWidth;
				      var ch = chart.canvas.offsetHeight;
				      var cx = cw / 2;
				      var cy = ch - 6;

				      ctx.translate(cx, cy);
				      ctx.rotate(angle);
				      ctx.beginPath();
				      ctx.moveTo(0, -3);
				      ctx.lineTo(ch - 20, 0);
				      ctx.lineTo(0, 3);
				      ctx.fillStyle = 'rgb(0, 0, 0)';
				      ctx.fill();
				      ctx.rotate(-angle);
				      ctx.translate(-cx, -cy);
				      ctx.beginPath();
				      ctx.arc(cx, cy, 5, 0, Math.PI * 2);
				      ctx.fill();
				    }
				  }],
				  data: {
				    labels: [],
				    datasets: [{
				      data: [35, 40, 25],
				      needleValue: 33,
				      backgroundColor: [
				        'rgba(255, 99, 132, 0.2)',
				        'rgba(255, 206, 86, 0.2)',
				        'rgba(63, 191, 63, 0.2)'
				      ]
				    }]
				  },
				  options: {
				    responsive: false,
				    aspectRatio: 2,
				    layout: {
				      padding: {
				        bottom: 3
				      }
				    },
				    rotation: -90,
				    cutout: '50%',
				    circumference: 180,
				    legend: {
				      display: false
				    },
				    animation: {
				      animateRotate: false,
				      animateScale: true
				    }
				  }
				});
		});
