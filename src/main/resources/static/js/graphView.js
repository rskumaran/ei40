app.controller('graphViewController',
		function($scope, $http, $rootScope, $window, $timeout,$location) {
	$scope.user = JSON.parse($window.sessionStorage
			.getItem("user"));
	if ($scope.user == null) {
		$location.path('/login');
		return;
	}
	if (!$rootScope.checkNetconnection()) { 				
	 	$rootScope.alertDialogChecknet();
		return; 
		}

	$('#map').css('height',($(window).height()-135)); 
	
	// $scope.user = JSON.parse($window.sessionStorage.getItem("user"));
	$scope.pleaseSelect = true;
	$scope.totalInventryValue = ""
	$scope.no_data_text = "No data found";
	$scope.orgoperationalstarttime="12:45 AM"
	$scope.orgoperationalendtime="11:00 PM"
	
	/*
	 * if($window.sessionStorage.getItem("operationalendtime")!=null &&
	 * $window.sessionStorage.getItem("operationalendtime")!=undefined){
	 * $scope.orgoperationalendtime=$window.sessionStorage.getItem("operationalendtime").toUpperCase();
	 * $scope.orgoperationalstarttime=$window.sessionStorage.getItem("operationalstarttime").toUpperCase(); }
	 */	
	
	
	$scope.zoneList = [];
	$scope.zoneLists = [];
	$scope.staffDetails = new Array();
	$scope.exportArrayData = [];
	$scope.visitorList = [];
	$scope.vendorList = [];
	$scope.contractorList = [];
	$scope.staffArray =  [];
   	$scope.staffDwellArray = [];	
	$scope.visitorArray =  [];
   	$scope.visitorDwellArray = [];	
	// for map
	$scope.outlineDetailLayers = null;
	$scope.marker = null;
	$scope.marker1=null;	
	$scope.bSetZeroIndex=true;
	$scope.outlineList = [];	
	$scope.doorsList = [];
	$scope.wallList = [];
	$scope.partitionList = [];
	$scope.pathwayList = [];
	const OUTLINE_START = 100;
	const PATHWAY_START = 200;
	const PARTITION_START = 300;
	const WALL_START = 400;
	const DOOR_START = 500;
	$scope.editableLayers = null;
	var screenWidth = window.screen.width
			* window.devicePixelRatio;
	var screenHeight = window.screen.height
			* window.devicePixelRatio;
	$scope.zoneClassificationList = [];
	$scope.outlineList = [];	
	$scope.workOrderFlowViewData = [];
	$scope.workOrderFlowDataFilterList = [];
	$scope.workOrderFlowData = [];
	$scope.searchButton = false;
	 $scope.mapEmpIDVariable = 0
	/*
	 * $('#idPFAStartDatePicker').datetimepicker({ format : 'YYYY-MMM-DD',
	 * maxDate : new Date() });
	 */
	 $('#idPFAStartDatePicker').datetimepicker({
			format : 'LL',					
			maxDate : new Date()
		});
	
	/*
	 * $.getJSON('attendance.json', function (data) { "use strict";
	 * $scope.attendanceDataList = data; });
	 */	
	 
	if($rootScope.pageName == 'People Time Analytics'){
		$scope.please_select_text = "Please select date and user";		
		$scope.isPeopleTimeAnalyticsClicked = true;	
		$scope.isPlantArrivalRateClicked = false;	
		$scope.isPlantFloorAnalysisClicked = false;
		$scope.isAssetTimeAnalyticsClicked = false;
		$scope.isWorkOrderFlowClicked = false;
		$scope.isWorkStationFlowClicked = false;
		$scope.isConstraintClicked = false;
		$scope.isCurrentInventoryClicked = false;
		$scope.isAgingClicked = false;
		$scope.viewValue = "Column Chart";
		$scope.subtype = "Employee";
		
		// $('#chart-area').empty();
		$scope.dataList=[];			
			
		// $scope.viewValue=$scope.viewValueDwell;
		// $scope.emp=$scope.previousEmployee
				
		$scope.typeofday=true;		
		$scope.typeofdays=$scope.dateValueDwell;
	// $('#startdate').val($scope.DwellStartDate);
		// $('#enddate').val($scope.DwellEndDate);
		// $scope.selectdate=false;
			
		// $scope.viewOption();
	}
	else if($rootScope.pageName == 'Plant Traffic'){
		$scope.please_select_text = "Please select date";		
		$scope.isPeopleTimeAnalyticsClicked = false;	
		$scope.isPlantArrivalRateClicked = true;	
		$scope.isPlantFloorAnalysisClicked = false;
		$scope.isAssetTimeAnalyticsClicked = false;
		$scope.isWorkOrderFlowClicked = false;
		$scope.isWorkStationFlowClicked = false;
		$scope.isConstraintClicked = false;
		$scope.isCurrentInventoryClicked = false;
		$scope.isAgingClicked = false;
		$scope.viewValue = "Line Chart";
	}
	else if($rootScope.pageName == 'Plant Occupancy'){
		$scope.please_select_text = "Please select date";	
		$scope.filename = "Plant Occupancy"		
		$scope.sheetname = "Plant Occupancy"
		$scope.isPeopleTimeAnalyticsClicked = false;	
		$scope.isPlantArrivalRateClicked = false;	
		$scope.isPlantFloorAnalysisClicked = true;
		$scope.isAssetTimeAnalyticsClicked = false;
		$scope.isWorkOrderFlowClicked = false;
		$scope.isWorkStationFlowClicked = false;
		$scope.isConstraintClicked = false;
		$scope.isCurrentInventoryClicked = false;
		$scope.isAgingClicked = false;		
		$scope.viewValue = "Plant Occupancy";
		
		$scope.varPFAStartTime = moment(new Date()).format('MMM DD, YYYY');
		$("#idPFAEditStartDatePicker").val(moment(new Date()).format('YYYY-MMM-DD'));
	}
	else if($rootScope.pageName == 'Asset Time Analytics'){
		$scope.please_select_text = "Please select date and asset";			
		$scope.isPeopleTimeAnalyticsClicked = false;	
		$scope.isPlantArrivalRateClicked = false;	
		$scope.isPlantFloorAnalysisClicked = false ;
		$scope.isAssetTimeAnalyticsClicked = true;
		$scope.isWorkOrderFlowClicked = false;
		$scope.isWorkStationFlowClicked = false;
		$scope.isConstraintClicked = false;
		$scope.isCurrentInventoryClicked = false;
		$scope.isAgingClicked = false;
		$scope.viewValue = "Column Chart";
		$scope.subtype = "gages";
		$scope.assetProfileType = "Chrono";
		
	}
	else if($rootScope.pageName == 'Work Order Flow Analytics'){
		$scope.filename = "Work Order Flow Analytics"
		$scope.sheetname = "Work Order Flow Analytics"
		$scope.pleaseSelect = true;
		$scope.please_select_text = "Please select date";	
		$scope.isPeopleTimeAnalyticsClicked = false;	
		$scope.isPlantArrivalRateClicked = false;	
		$scope.isPlantFloorAnalysisClicked = false ;
		$scope.isAssetTimeAnalyticsClicked = false;
		$scope.isWorkOrderFlowClicked = true;
		$scope.isWorkStationFlowClicked = false;
		$scope.isConstraintClicked = false;
		$scope.isCurrentInventoryClicked = false;
		$scope.isAgingClicked = false;
		$scope.varSortType = "0";
		$scope.viewValue = "Line Chart";
		$scope.norecords = false;
	}
	else if($rootScope.pageName == 'Work Station Flow Analysis'){
		$('#idChartAgingCanvas').remove();
		$('#chartviewAgingchart').append('<canvas id="idChartAgingCanvas"  height="0" ></canvas>');
		$scope.please_select_text = "Please select date and work station";			
		$scope.isPeopleTimeAnalyticsClicked = false;	
		$scope.isPlantArrivalRateClicked = false;	
		$scope.isPlantFloorAnalysisClicked = false ;
		$scope.isAssetTimeAnalyticsClicked = false;
		$scope.isWorkOrderFlowClicked = false;
		$scope.isWorkStationFlowClicked = true;
		$scope.isConstraintClicked = false;
		$scope.isCurrentInventoryClicked = false;
		$scope.isAgingClicked = false;
		$scope.viewValue = "All";
		$scope.varSortType = "0";
	}
	else if($rootScope.pageName == 'Constraint Analytics'){
		$scope.please_select_text = "Please select date";	
		$scope.filename = "Constraint Analytics"
		$scope.sheetname = "Constraint Analytics"
		$scope.isPeopleTimeAnalyticsClicked = false;	
		$scope.isPlantArrivalRateClicked = false;	
		$scope.isPlantFloorAnalysisClicked = false ;
		$scope.isAssetTimeAnalyticsClicked = false;
		$scope.isWorkOrderFlowClicked = false;
		$scope.isWorkStationFlowClicked = false;
		$scope.isConstraintClicked = true;
		$scope.isCurrentInventoryClicked = false;
		$scope.isAgingClicked = false;
	}
	else if($rootScope.pageName == 'Current Inventory $'){
		$scope.please_select_text = "Please select date";	
		$scope.filename = "Current Inventory($)"
		$scope.sheetname = "Current Inventory($)"	
		$scope.isPeopleTimeAnalyticsClicked = false;	
		$scope.isPlantArrivalRateClicked = false;	
		$scope.isPlantFloorAnalysisClicked = false ;
		$scope.isAssetTimeAnalyticsClicked = false;
		$scope.isWorkOrderFlowClicked = false;
		$scope.isWorkStationFlowClicked = false;
		$scope.isConstraintClicked = false;
		$scope.isCurrentInventoryClicked = true;
		$scope.isAgingClicked = false;
		
		// to remove canvas when loaded initially to show please select text
		$('#idChartAgingCanvas').remove();
		$('#chartviewAgingchart').append('<canvas id="idChartAgingCanvas"  height="0" ></canvas>');
	// $scope.loadCurrentInventorychart();
	}
	else if($rootScope.pageName == 'Aging Chart'){
		$scope.please_select_text = "Please click generate report";	
		$scope.filename = "Aging Chart"
		$scope.sheetname = "Aging Chart"
		$scope.isPeopleTimeAnalyticsClicked = false;	
		$scope.isPlantArrivalRateClicked = false;	
		$scope.isPlantFloorAnalysisClicked = false ;
		$scope.isAssetTimeAnalyticsClicked = false;
		$scope.isWorkOrderFlowClicked = false;
		$scope.isWorkStationFlowClicked = false;
		$scope.isConstraintClicked = false;
		$scope.isCurrentInventoryClicked = false;
		$scope.isAgingClicked = true;
		// to remove canvas when loaded initially to show please select text
		$('#idChartAgingCanvas').remove();
		$('#chartviewAgingchart').append('<canvas id="idChartAgingCanvas"  height="0" ></canvas>');
		   
		$scope.viewValue = "All";
		$scope.varComboTypeofdaysAging = "1";
	}
	
	 $scope.startdate =  moment(new Date()).format('YYYY-MMM-DD')	
	 $scope.enddate = moment(new Date()).format('YYYY-MMM-DD')
	
	$('#selectdate').datetimepicker({
		defaultDate: new Date(),
		format : 'L',
		maxDate : new Date()
	}).on('dp.change', function(e) {		
		$scope.invalid = '';
	});
	
	
	 $('#idStartDatePicker').datetimepicker({
			format : 'MMM DD, YYYY',					
			maxDate : new Date()
		});
	 $('#idEndDatePicker').datetimepicker({
			format : 'MMM DD, YYYY',
			// defaultDate: new Date(),
			maxDate : new Date()
		});
	 if($scope.isConstraintClicked == true){
		 $scope.dateRange = moment().subtract(1, 'days').format("MMM DD, YYYY")+"-"+ moment(new Date()).format("MMM DD, YYYY");		
	 }
	 else{
	 $scope.dateRange =  moment(new Date()).format("MMM DD, YYYY")+"-"+ moment(new Date()).format("MMM DD, YYYY");
	 }
	  // Date range as a button
	 if($scope.isConstraintClicked == false){
		 $('#daterange-btn').daterangepicker(
			      {    	  
			    	
			        ranges   : {
			          'Today'       : [moment(), moment()],	          
			          'Yesterday'   : [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
			          'Last 7 Days' : [moment().subtract(6, 'days'), moment()],
			          'Last 14 Days': [moment().subtract(13, 'days'), moment()],
			        },
			     
			        startDate: moment().subtract(1, 'days'),
			        endDate  : moment()
			      }
			    )	 
	 }else if($scope.isConstraintClicked == true){
		 $('#daterange-btn').daterangepicker(			      {    	  
			    	
			        ranges   : {			                 
			          'Last 2 days'   : [moment().subtract(1, 'days'), moment()],
			          'Last 7 Days' : [moment().subtract(6, 'days'), moment()],
			          'Last 14 Days': [moment().subtract(13, 'days'), moment()],
			        },
			     
			        startDate: moment().subtract(1, 'days'),
			        endDate  : moment()
			      }
			    )	  
	 }
	    	 
	 $('#daterange-btn').on('cancel.daterangepicker', function(ev, picker) {
		 // do something, like clearing an input
		 $('#daterange-btn').val('');
		// $scope.dateRange = "";
		// $scope.$apply();
	 });
		 $('#daterange-btn').on('apply.daterangepicker', function(ev, picker) {
			$scope.startdate = "";
			$scope.enddate = "";
			 // do something, like clearing an input
			 $('#daterange-btn').val(picker.startDate.format('MMMM D, YYYY') + ' - ' + picker.endDate.format('MMMM D, YYYY'));
			// alert(picker.startDate.format('MMMM D, YYYY') + ' - ' +
			// picker.endDate.format('MMMM D, YYYY'));
			 $scope.dateRange = picker.startDate.format('MMM D, YYYY') + ' - ' + picker.endDate.format('MMM D, YYYY');
			 $scope.startdate = picker.startDate.format('YYYY-MMM-DD')
			 $scope.enddate = picker.endDate.format('YYYY-MMM-DD')
			 $scope.$apply();
		 });
	
	var screenHeight = $window.innerHeight;
   	var screenWidth = $window.innerWidth;

   	var maxZoomValue = 18;
	var minZoomValue = 0;   
	var defZoomValue = 8;  
	// Initialize map
	var map = L.map('map', {  
		editable : true,
		maxZoom : maxZoomValue,
		minZoom : minZoomValue,
		zoom: defZoomValue,
		crs : L.CRS.Simple,
		doubleClickZoom : false,
		dragging:true,
		zoomDelta: 0.25,
		zoomSnap : 0.01,
		scrollWheelZoom: false, // disable original zoom function
		  smoothWheelZoom: true,  // enable smooth zoom
		  smoothSensitivity: 1   // zoom speed. default is 1
	}).setView([ 0, 0 ], 14);

	// Zoom handler
	var ZoomViewer = L.Control.extend({
		options: {
			  position: 'topleft' 
			},
		onAdd: function(){

			var container= L.DomUtil.create('div');
			var gauge = L.DomUtil.create('div');
			container.style.width = '100px';
			container.style.background = 'rgba(255,255,255,0.5)';
			container.style.textAlign = 'center';
			map.on('zoomstart zoom zoomend', function(ev){
				gauge.innerHTML = 'Zoom level: ' + map.getZoom().toFixed(2);
			})
			container.appendChild(gauge);

			return container;
		}
	});

	(new ZoomViewer).addTo(map);
	map.zoomControl.setPosition('topleft');
	// Add Grid check box
	var command = L.control({position: 'topright'});

	command.onAdd = function (map) {
	    var div = L.DomUtil.create('div', 'command');

	    div.innerHTML = '<form>Grid (1X1M) <input id="command" type="checkbox" checked/></form>'; 
	    return div;
	};

	command.addTo(map);
	var layoutCmd = L.control({position: 'topright'});

	layoutCmd.onAdd = function (map) {
	    var div = L.DomUtil.create('div', 'layoutCmd');

	    div.innerHTML = '<form>Layout <input id="layoutCmd" type="checkbox" checked style="display: initial!important"/></form>';
	    return div;
	};
	layoutCmd.addTo(map);
	
	// add the event handler
	function handleCommand() {
		// alert("Clicked, checked = " + this.checked);
		$scope.isCheckedGrid = this.checked;
		if (this.checked == true) {
			$scope.grid.onAdd(map)
		} else {
			$scope.grid.onRemove(map);
		}
		$scope.editableLayers.bringToFront();
	}
	function displayLayout() {
		// alert("Clicked, checked = " + this.checked);
		
     if(this.checked==true) {
			 $scope.isCheckedLayout = true;
         // showZones();
			 map.addLayer($scope.outlineDetailLayers);
			 $scope.editableLayers.bringToFront();
    } else {
			$scope.isCheckedLayout = false;
			map.removeLayer($scope.outlineDetailLayers);
       
    }
	}
	$scope.isCheckedGrid = true; 
	$scope.isCheckedLayout = true; 

	document.getElementById("command").addEventListener(
			"click", handleCommand, false);
	document.getElementById("layoutCmd").addEventListener(
			"click", displayLayout, false);

	function pixelsToLatLng(x, y) {
		return map.unproject([ x, y ], defZoomValue);
	}

	$scope.campusGridArray = {};
	function loadMap() {
		// to display slider starts here
		$scope.orgoperationalstarttime="12:45 AM"
		$scope.orgoperationalendtime="11:00 PM"
		  
		var startpos=0;
		var endpos=0;
	 	var pos=0;
		var categories=[]; 
		for(var st=0;st<$scope.timearray.length;st++){
			if(startpos==0){
				if($scope.timearray[st]==$scope.orgoperationalstarttime){
					if(st!=0 && st>4)
						startpos=st-4;
					else
						startpos = 0;
				}
			}
			if(endpos==0){
				if($scope.timearray[st]==$scope.orgoperationalendtime){
					if(st!=96 && st<=92)
						endpos=st+4;
					else
						endpos = 94
				}
			}
		}
		for(var ct=startpos;ct<=endpos;ct++){
			categories.push($scope.timearray[ct]);
		} 
		
		$scope.timeval=categories[startpos]; 		
		$scope.tqidval=startpos;
		
		$( "#slider-range-min" ).slider({
			range: "min",
			min: 0,
			max: categories.length-1,
			value:0,
			slide: function( event, ui ) {				
	 			$scope.timeval=categories[ui.value];
				$scope.tqidval=ui.value;
				$scope.getZoneDetails()
				$scope.$apply();				
			} 
		});
		// to display slider ends here
		var height = $scope.mapHeight;
		var width = $scope.mapWidth;
		$scope.outlineDetailLayers = new L.FeatureGroup();
		map.addLayer($scope.outlineDetailLayers);

		$scope.campusGridArray = {};

		for (var i = 0; i < height; i++) {

			for (var j = 0; j < width; j++) {

				var campusArray = [];
				campusArray.push({
					"x" : i,
					"y" : j
				});
				campusArray.push({
					"x" : i,
					"y" : (j + 1)
				});
				campusArray.push({
					"x" : (i + 1),
					"y" : (j + 1)
				});
				campusArray.push({
					"x" : (i + 1),
					"y" : j
				});

				$scope.campusGridArray[i + '_' + j] = campusArray;
			}

		}

		

		map.setZoom(defZoomValue);
		var southWest = map.unproject([ 0, height ],
				defZoomValue);
		var northEast = map.unproject([ width, 0 ],
				defZoomValue);
		map.setMaxBounds(new L.LatLngBounds(southWest,
				northEast));
		
		map.fitBounds(new L.LatLngBounds(southWest, northEast));
		
		

		if ($scope.campusRect != undefined
				|| $scope.campusRect != null) {
			$scope.campusRect.onRemove(map);
		}

		var bounds1 = [
				[ pixelsToLatLng(parseFloat(0), parseFloat(0)) ],
				[ pixelsToLatLng(parseFloat(0),
						parseFloat(height)) ],
				[ pixelsToLatLng(parseFloat(width),
						parseFloat(height)) ],
				[ pixelsToLatLng(parseFloat(width),
						parseFloat(0)) ] ];

		$scope.campusRect = L.rectangle(bounds1, {
			color : $scope.mapColor,
			weight : 2,
			fillColor : $scope.mapColor,
			fillOpacity : 0.8
		});
		$scope.campusRect.addTo(map);
		$scope.campusRect.addTo($scope.outlineDetailLayers);		
		
		if ($scope.marker != undefined || $scope.marker != null) {
			$scope.marker.onRemove(map);
		}
		
		var originDotIcon = L.icon({
		    iconUrl: './assets/img/red_circle_tr.png',		    
		    iconSize:     [12, 12] // size of the icon
		});						
		
		$scope.marker = new L.Marker(
				pixelsToLatLng(0, 0),
				{
					icon : originDotIcon
				});					

		$scope.marker.addTo(map);
		map.on('move', function (e) {
			
		});
		// Dragend event of map for update marker position
		map.on('dragend', function(e) {
			var cnt = map.getCenter();
		        var position = $scope.marker.getLatLng();
			lat = Number(position['lat']).toFixed(5);
			lng = Number(position['lng']).toFixed(5);
			
		});
		$scope.mapDetails = {};
		$scope.mapDetails.width_m = width;
		$scope.mapDetails.height_m = height;
		$scope.mapDetails.height = height;
		$scope.mapDetails.width = width;
		$scope.mapDetails.mapType = 'custom';

		if ($scope.grid != undefined || $scope.grid != null) {
			$scope.grid.onRemove(map);
		}

		if ($scope.marker1 != undefined
				|| $scope.marker1 != null) {
			$scope.marker1.onRemove(map);
		}

		$scope.grid = L.grid(
				{
					options : {
						position : 'topright',
						bounds : new L.LatLngBounds(southWest,
								northEast),
						mapDetails : $scope.mapDetails,
						defZoomValue : defZoomValue,
						gridSize : 1
					}
				}).addTo(map);
/*
 * $scope.marker1 = new L.Marker( pixelsToLatLng(-35, -5), { icon : new
 * L.DivIcon( { className : 'my-div-icon', html : '<span
 * class="staringlabel">(0,0)</span>' }) });
 */
		var originValueIcon = L.icon({
		    iconUrl: './assets/img/origin_tr.png',
		    iconSize:     [29, 16] // size of the icon
		   
		});						
		$scope.marker1 = new L.Marker(
				pixelsToLatLng(-2, -2),
				{
					icon : originValueIcon
				});	
		$scope.marker1.addTo(map);
		
		$timeout(function() {
			map.setView([ $scope.campusRect.getCenter().lat,
					$scope.campusRect.getCenter().lng ], map
					.getZoom());

			getAllOutlinedetails();
			
		}, 1500);
		$timeout(function() {
			$scope.getZoneClassifications();
		}, 1500);
		$timeout(function() {
			$scope.getZoneDetails();

			}, 1500);
		
	}
	
		$scope.getCampusDetails = function() {
			if (!$rootScope.checkNetconnection()) { 				
			 	$rootScope.alertDialogChecknet();
				return; 
				}
			$rootScope.showloading('#loadingBar');
		$http({
			method : 'GET',
			url : './campusDetails'
		})
				.then(
						function success(response) {
							$rootScope
									.hideloading('#loadingBar');
							if (response != null
									&& response.data != null
									&& response.data != "BAD_REQUEST") {
								$scope.campusDetails = response.data.data;

								$scope.mapHeight = $scope.campusDetails.height;
								$scope.mapWidth = $scope.campusDetails.width;
								$scope.mapColor = $scope.campusDetails.colorCode;
								$scope.campusId = $scope.campusDetails.campusId;
								$scope.campusName = $scope.campusDetails.name;

								

								// $("#campusDetailsDiv").show();
								// $("#selectedId").hide();
								// $scope.loadTreeView();

								$timeout(function() {
									loadMap();
								}, 500);
							}
						},
						
						function error(response) {						
								$rootScope.hideloading('#loadingBar');						
								$rootScope.fnHttpError(response);
					    });
	}
			
	$scope.area = function(points) {
		var area = 0, i, j, point1, point2;

		var length = points.length;

		for (i = 0, j = length - 1; i < length; j = i, i += 1) {
			point1 = points[i];
			point2 = points[j];
			area += point1.x * point2.y;
			area -= point1.y * point2.x;
		}
		area /= 2;

		return Math.abs(area).toFixed(3);
	}
	
	function latLngToPixels(latlng) {
		var pixelArr = [];
		for (var i = 0; i < latlng[0].length; i++) {

			var latlngArr = latlng[0][i];

			var pt= map.project([ latlngArr.lat,
                latlngArr.lng ], defZoomValue); 
			
            pixelArr.push({
                "x" : pt.x.toFixed(),
                "y" : pt.y.toFixed(),
            });


		}
		return pixelArr;
	} 
	
	function roundVal(latlng) {
		var pixelArr = [];
		for (var i = 0; i < latlng.length; i++) {

			var pt = latlng[i];
				 
            
            pixelArr.push({
                "x" : (parseFloat(pt.x)).toFixed(),
                "y" : (parseFloat(pt.y)).toFixed(),
            });

		}
		return pixelArr;
	} 
	
	function editlatLngToPixels(latlng) {
		var pixelArr = [];
		for (var i = 0; i < latlng[0].length; i++) {

			var latlngArr = latlng[0][i];
			var pt = pt = map.project([ latlngArr.lat,
					latlngArr.lng ], defZoomValue);

			pixelArr.push({
				"x" : pt.x.toFixed(),
				"y" : pt.y.toFixed(),
			});

		}
		return pixelArr;
	}
	function getAllOutlinedetails() {
		if (!$rootScope.checkNetconnection()) { 				
		 	$rootScope.alertDialogChecknet();
			return; 
			}
		
		map.eachLayer(function(layer) {
			if (layer.id !== undefined) {
				layer.remove();
			}
		});
		$rootScope.showloading('#loadingBar');
		$http({
			method : 'GET',
			url : './outlineDetails/' + $scope.campusId
		})
				.then(
						function success(response) {
							$rootScope.hideloading('#loadingBar');
							if (response != null
									&& response.data != null
									&& response.data != "BAD_REQUEST") {

								$scope.outlineList.length = 0;
								if (response.data.status == 'success') {
									for (var i = 0; i < response.data.data.length; i++) {
										if (response.data.data[i].active)
											$scope.outlineList
													.push(response.data.data[i]);
									}
									/*
									 * var item = $scope.treeData[0];
									 * 
									 * var itemNode = [];
									 * 
									 * if ($scope.outlineList != null &&
									 * $scope.outlineList.length > 0) {
									 * 
									 * for (var i = 0; i <
									 * $scope.outlineList.length; i++) {
									 * 
									 * itemNode[i] = { text :
									 * $scope.outlineList[i].name }; } }
									 * 
									 * item.nodes = itemNode;
									 * 
									 * $scope.treeData[0] = item;
									 * 
									 * $scope.loadTreeView();
									 */
									
									
									if ($scope.outlineList.length != 0) {
										for (var i = 0; i < $scope.outlineList.length; i++) {
											if ($scope.outlineList[i].active) {
												$scope.outlineList[i].area = $scope
														.area($scope.outlineList[i].coordinateList);
												var bounds = [];

												$scope.colorCode = $scope.outlineList[i].bgColor;

												if ($scope.outlineList[i].geometryType == undefined
														|| $scope.outlineList[i].geometryType == null
														|| $scope.outlineList[i].geometryType == "polygon"
														|| $scope.outlineList[i].geometryType == "") {

													for (var j = 0; j < $scope.outlineList[i].coordinateList.length; j++) {
														bounds
																.push(pixelsToLatLng(
																		parseFloat($scope.outlineList[i].coordinateList[j].x),
																		parseFloat($scope.outlineList[i].coordinateList[j].y)));

													}

													$scope.outlineList[i].geometryType = "polygon";
													var pol = L
															.polygon(
																	bounds,
																	{
																		color : $scope.colorCode,
																		weight : 1,
																		fillColor : $scope.colorCode,
																		fillOpacity : 0.5
																	})
															.addTo(
																	map)
															.addTo(
																	$scope.outlineDetailLayers);

													$scope.outlinesArea = $scope
															.area(editlatLngToPixels(pol
																	.getLatLngs()));

													pol.id = i+100;
													pol.outlinesId = $scope.outlineList[i].outlinesId+100;
													pol.name = $scope.outlineList[i].name;
													pol.outlinesArea = $scope.outlinesArea;
													pol.colorCode = $scope.colorCode;																
													pol.wallType = $scope.outlineList[i].wallType;
													pol.palceType = $scope.outlineList[i].placeType;
													pol.geometryType = $scope.outlineList[i].geometryType;
													pol.coordinateList = bounds;

												} else {

													var bounds1 = [
															[ pixelsToLatLng(
																	parseFloat($scope.outlineList[i].coordinateList[0].x),
																	parseFloat($scope.outlineList[i].coordinateList[0].y)) ],
															[ pixelsToLatLng(
																	parseFloat($scope.outlineList[i].coordinateList[1].x),
																	parseFloat($scope.outlineList[i].coordinateList[1].y)) ],
															[ pixelsToLatLng(
																	parseFloat($scope.outlineList[i].coordinateList[2].x),
																	parseFloat($scope.outlineList[i].coordinateList[2].y)) ],
															[ pixelsToLatLng(
																	parseFloat($scope.outlineList[i].coordinateList[3].x),
																	parseFloat($scope.outlineList[i].coordinateList[3].y)) ] ];
													var rec = L
															.rectangle(
																	bounds1,
																	{
																		color : $scope.colorCode,
																		weight : 1,
																		fillColor : $scope.colorCode,
																		fillOpacity : 0.5
																	})
															.addTo(
																	map)
															.addTo(
																	$scope.outlineDetailLayers);
													$scope.outlinesArea = $scope
															.area(editlatLngToPixels(rec
																	.getLatLngs()));
													rec.id = i+100;
													rec.outlinesId = $scope.outlineList[i].outlinesId+100;
													rec.name = $scope.outlineList[i].name;
													rec.outlinesArea = $scope.outlinesArea;
													rec.colorCode = $scope.colorCode;																
													rec.wallType = $scope.outlineList[i].wallType;
													rec.placeType = $scope.outlineList[i].placeType;
													rec.geometryType = $scope.outlineList[i].geometryType;
													rec.coordinateList = bounds1;																
													
												}

											}
										}
									} else {
										$scope.noList = true;
										$scope.valueList = false;
										$scope.errorMsg = "No outlines details are available";
										$timeout(
												function() {
													$scope.error = false;
												}, 1500);
									}
									$scope.getAllPathways();
								} else {
									$scope.error = true;
									$scope.alert_error_msg = response.data.message;
									$timeout(function() {
										$scope.error = false;
									}, 1500);
								}

							}
						},
						function error(response) {
							$rootScope.hideloading('#loadingBar');
							
						});
	}
	$scope.popup = L.popup({
		maxWidth : 400
	});
	var mouseroverid = null;
	function mouseOverPopup(e) {
		
		if(!$scope.isEdit && !$scope.isDrawEvent){ 

			  if(mouseroverid!=e.sourceTarget.id){
				  var name = e.sourceTarget.name;
				  var restrictions = e.sourceTarget.capacity;
				  var classification = e.sourceTarget.classification;
				  var dWellTime = e.sourceTarget.dWellTime
				  var noOfOccupancy = e.sourceTarget.noOfOccupancy
				 
				  if(name==undefined || name==null)
					  name = '-';
				  if(restrictions==undefined || restrictions==null)
					  restrictions = '-';
				  if(classification==undefined || classification==null)
					  classification = '-';
				  if(dWellTime==undefined || dWellTime==null || dWellTime=="")
					  dWellTime = '0';
				  if( noOfOccupancy == undefined ||  noOfOccupancy == null ||  noOfOccupancy == "")
					  noOfOccupancy = '0';
				  if($scope.isPeopleTimeAnalyticsClicked==true){
				   var popupContent = "<strong>Zone name :" + name + "</strong><br /><strong>Classification:" +
				   classification + "</strong><br /> <strong>Max Occupancy:" + restrictions + "</strong><br /> <strong>Dwell Time :" + dWellTime + "</strong><br />";
				  }else if($scope.isAssetTimeAnalyticsClicked==true){
					   var popupContent = "<strong>Zone name :" + name + "</strong><br /><strong>Classification:" +
					   classification + "</strong><br /> <strong>Max Occupancy:" + restrictions + "</strong><br /> <strong>Dwell Time :" + dWellTime + "</strong><br />";
					  }
				  else if($scope.isPlantFloorAnalysisClicked==true){						
						var popupContent = "<strong>Zone name :" + name + "</strong><br /><strong>Classification:" +
						   classification + "</strong><br /> <strong>Max Occupancy:" + restrictions + "</strong><br /> <strong>No.of Occupancy :" + noOfOccupancy + "</strong><br />";
				  
				  }				   
				   $scope.popup.setLatLng(e.sourceTarget._bounds.getCenter());
				   $scope.popup.setContent(popupContent);
				   map.openPopup($scope.popup);
				   mouseroverid =e.sourceTarget.id;
				   }
		} 
	}

	function mouseOutClosePopup(e) {
		map.closePopup(); 
	}
	
	$scope.getZoneDetails = function(){
		if (!$rootScope.checkNetconnection()) { 				
		 	// $rootScope.alertDialogChecknet();
			return; 
			}
	    $scope.mapArrayData = [];
	    $scope.mapArrayDataPlantOccupancy = [];
	    var zoneDataclone={}; 
		var zoneData ={};
	    $scope.particularDropDownId = ""
	    	if($scope.isPeopleTimeAnalyticsClicked == true){
	    		$scope.mapEmpIDVariable = 1;
	    		$scope.mapHeaderName = "People Time Analytics";
	    		if($scope.subtype=='Employee')
	    	    	$scope.particularDropDownId = $scope.emp;
	    	    	else if($scope.subtype=='Visitor')
	    	    		$scope.particularDropDownId = $scope.visitor;
	    	    		else if($scope.subtype=='Vendor')
	    		    		$scope.particularDropDownId = $scope.vendor;
	    		    		else if($scope.subtype=='Contractor')
	    			    		$scope.particularDropDownId = $scope.contractor	;
	    	}  
	    	else if($scope.isAssetTimeAnalyticsClicked == true){
	    		$scope.mapHeaderName = "Asset Time Analytics";
	    		 $scope.particularDropDownId = $scope.assetIDForData
	    	}		
	    // code for people and asset time analytics
	   
	    if( $scope.dataList != undefined && $scope.dataList.dwellTime != undefined &&  $scope.dataList.dwellTime.length != 0 ){
	   		for (var i = 0; i < $scope.dataList.dwellTime.length; i++) {
	   			if($scope.dataList.dwellTime[i].id == $scope.particularDropDownId){
	   				var dateObj = $scope.dataList.dwellTime[i];				
					
					if (dateObj != undefined && dateObj != null) {
						var zonedataObj=dateObj.zones;							
							for (var j = 0; j < zonedataObj.length; j++) {									
							 var zoneName = zonedataObj[j].zoneName;
							 var zoneID = zonedataObj[j].zoneID;
							 var visiterDwellTime = zonedataObj[j].dwellTime;							
							 
							 $scope.mapArrayData.push({					
									'zoneName':zoneName,
									'zoneID':zoneID,										
									'mapDwellTime': visiterDwellTime
								});	
				}								
			}	   		
	   	}
	   }
	    }
if($scope.isPlantFloorAnalysisClicked==true){
			
			var startpos=0;
			var endpos=0; 
		 	var pos=0; 
		 	
			for(var st=0;st< $scope.timearray.length;st++){
				if(startpos==0){
					if( $scope.timearray[st]==$scope.orgoperationalstarttime){
						if(st!=0 && st>4)
							startpos=st-4;
						else
							startpos = 0;
					}
				}
				if(endpos==0){
					if( $scope.timearray[st]==$scope.orgoperationalendtime){
						if(st!=96 && st<=92)
							endpos=st+4;
						else
							endpos = 94
					}
				}
			}
		 	 
			if((startpos+$scope.tqidval)<9)// to display tq10 value
											// geetha/27-3-2021
				pos="TQ0"+(startpos+$scope.tqidval+1); 
			else
				pos="TQ"+(startpos+$scope.tqidval+1);

			var zoneDataclone={}; 
			var zoneData ={};
			$scope.zoneList.sort(GetSortOrder("name"));
			for (var n = 0; n < $scope.zoneList.length; n++) {
				zoneData[$scope.zoneList[n].id] =  {
						'zoneID': $scope.zoneList[n].id,
						'zoneName':$scope.zoneList[n].name, 
						'value':0 
				};
			}
			
			  if( $scope.dataList != undefined  &&  $scope.dataList.length != 0 ){
			for (var i = 0; i < $scope.dataList.length; i++) {
				var dateObj = $scope.dataList[i];
				zoneDataclone=zoneData;
				if (dateObj != undefined && dateObj != null) {
					var zoneObjList = dateObj.zone;
					var orgObjList = dateObj.orgAttendance;

 					if (zoneObjList != undefined && zoneObjList != null && zoneObjList.length>0&&orgObjList != undefined && orgObjList != null) {
						zoneObjList.sort(GetSortOrder("zoneName"));
						if(orgObjList[pos]!=undefined&&orgObjList[pos]!=null){

							if(orgObjList[pos].tot!=undefined&&orgObjList[pos].tot!=null)
								total=orgObjList[pos].tot;
							else
								total=0;

							if(orgObjList[pos].user!=undefined&&orgObjList[pos].user!=null){
								if(orgObjList[pos].user.emp!=undefined&&orgObjList[pos].user.emp!=null)
									employee=orgObjList[pos].user.emp;
								else
									employee=0;
								if(orgObjList[pos].user.vis!=undefined&&orgObjList[pos].user.vis!=null)
									visitor=orgObjList[pos].user.vis;
								else
									visitor=0; 
							}else
							{
								employee=0; 
								visitor=0;
							}
						}
						else{ 
							total=0;
							employee=0; 
							visitor=0;

						}
						for (var j = 0; j < zoneObjList.length; j++) {
							var zoneObj = zoneObjList[j];
							if (zoneObj.tQ[pos]!=undefined&&zoneObj.tQ[pos]!=null){
								if(zoneDataclone.hasOwnProperty(zoneObj.zoneID))
								{
									var listObj = zoneDataclone[zoneObj.zoneID];
									listObj.value=zoneObj.tQ[pos];  
									zoneDataclone[zoneObj.zoneID]=listObj;
									$scope.mapArrayDataPlantOccupancy.push(listObj);	 
								} 
							}
							
						}
					}
				}  
			} 
}
 		}

	   /*
		 * if($scope.startdate == $scope.enddate) $scope.chartHeaderTitle =
		 * $scope.mapHeaderName + " on " +moment($scope.startdate).format("MMM
		 * DD, YYYY"); else $scope.chartHeaderTitle= $scope.mapHeaderName + "
		 * from "+$scope.dateRange;
		 */
	    
		$scope.zoneDwelltime = $scope.mapArrayData;
				
    
    map.eachLayer(function(layer) {
		if (layer.type !== undefined
				&& layer.type == "circleWithText") {
			layer.remove();
		}
	}); 
   				
							if ($scope.zoneLists != undefined && $scope.zoneLists != null && $scope.zoneLists.length != 0) { 
							
								$scope.noList = false;
								$scope.valueList =true;
								// $scope.zoneLists= response.data.data;
								
								$scope.zoneList =[];
								$scope.editableLayers = new L.FeatureGroup();
								map.addLayer( $scope.editableLayers);
								
								
									for (var i = 0; i < $scope.zoneLists.length; i++) {
									if($scope.zoneLists[i].active) {
											$scope.zoneList.push($scope.zoneLists[i])
									}  
									}
									
								 
								if(!$scope.isAPView){ 
									if($scope.zoneList.length!=0){
										for (var i = 0; i < $scope.zoneList.length; i++) {														
									if($scope.zoneList[i].active) {
											$scope.zoneList[i].area = $scope.area($scope.zoneList[i].coordinateList);
										var bounds  = [];
										// var classification;
										if($scope.zoneList[i].zoneClassification != ""){												
											$scope.colorcode = $scope.zoneList[i].zoneClassification.color;												 
													 
											} else {
												$scope.colorcode = "#ff7800"
											}
										
										if($scope.zoneList[i].geometryType== undefined || $scope.zoneList[i].geometryType==null || $scope.zoneList[i].geometryType=="polygon" || $scope.zoneList[i].geometryType==""){ 
											for (var j = 0; j < $scope.zoneList[i].coordinateList.length; j++) {  
													bounds.push(pixelsToLatLng(parseFloat($scope.zoneList[i].coordinateList[j].x) 
															,parseFloat($scope.zoneList[i].coordinateList[j].y)));
											   
											}
											
											$scope.zoneList[i].geometryType="polygon";
														var pol = L
														.polygon(
																bounds,
																{
																	color : $scope.colorcode ,
																	weight : 1,
																	fillColor :$scope.colorcode , 
																	fillOpacity: 0.5
																	})
														.addTo(
																map)
														.addTo(
																$scope.editableLayers)
													
														.on('mouseover', mouseOverPopup); 

														pol.id = i;
														pol.primaryid = $scope.zoneList[i].id																	
														
														pol.name = $scope.zoneList[i].name;
														pol.classification = $scope.zoneList[i].zoneClassification.className;
														pol.capacity = $scope.zoneList[i].capacity;
														pol.ppm = $scope.ppm;
														pol.coordinateList = bounds;
														
														pol.colorcode = $scope.colorcode;
														pol.geometryType="polygon";
														pol.zoneType = $scope.zoneList[i].zoneType;
														pol.zonearea=$scope.zoneList[i].area;	
														
														// to display circle
														// with text
														
														if($scope.isPeopleTimeAnalyticsClicked==true || $scope.isAssetTimeAnalyticsClicked==true){
															pol.dWellTime = "";
															var radiusCircle = 0;
															for(var x=0;x<$scope.zoneDwelltime.length;x++){
																if($scope.zoneList[i].id == $scope.zoneDwelltime[x].zoneID)
																	pol.dWellTime = $scope.zoneDwelltime[i].mapDwellTime;	
																 radiusCircle = (pol.dWellTime/100)*10;
															}
															
															var pt =pol.getBounds().getCenter();														
															$scope.circleTextview =  $scope.circleWithText([pt.lat, pt.lng], pol.dWellTime, {radius:  radiusCircle});
															$scope.circleTextview.addTo(map);
														}
														
														if($scope.isPlantFloorAnalysisClicked==true){
															pol.noOfOccupancy = "";
															var radiusCircle = 0;
															for(var x=0;x<$scope.mapArrayDataPlantOccupancy.length;x++){
																if($scope.zoneList[i].id == $scope.mapArrayDataPlantOccupancy[x].zoneID)
																	pol.noOfOccupancy = $scope.mapArrayDataPlantOccupancy[x].value;	
																	radiusCircle = (pol.noOfOccupancy/100)*10;
															}
															var pt =pol.getBounds().getCenter();														
															$scope.circleTextview =  $scope.circleWithText([pt.lat, pt.lng], pol.noOfOccupancy, {radius:  radiusCircle});
															$scope.circleTextview.addTo(map);
														}
														
											} else {
												
										var bounds1 = [
												[ pixelsToLatLng(
														parseFloat($scope.zoneList[i].coordinateList[0].x) ,
														parseFloat($scope.zoneList[i].coordinateList[0].y)) ],
												[ pixelsToLatLng(
														parseFloat($scope.zoneList[i].coordinateList[1].x),
														parseFloat($scope.zoneList[i].coordinateList[1].y)) ],
												[ pixelsToLatLng(
														parseFloat($scope.zoneList[i].coordinateList[2].x),
														parseFloat($scope.zoneList[i].coordinateList[2].y)) ],
												[ pixelsToLatLng(
														parseFloat($scope.zoneList[i].coordinateList[3].x),
														parseFloat($scope.zoneList[i].coordinateList[3].y)) ] ];
										var rec = L
												.rectangle(
														bounds1,
														{
															color : $scope.colorcode ,
															weight : 1,
															fillColor :$scope.colorcode , 
															fillOpacity: 0.5
														})
												.addTo(
														map)
												.addTo(
														$scope.editableLayers)
												.on('mouseover', mouseOverPopup); 

									 
										rec.id = i;
										rec.primaryid = $scope.zoneList[i].id;
										rec.name = $scope.zoneList[i].name;
										rec.classification = $scope.zoneList[i].zoneClassification.className;
										rec.capacity = $scope.zoneList[i].capacity;
										rec.ppm = $scope.ppm;
										rec.coordinateList = bounds1;
										rec.geometryType="rectangle";
										rec.zoneType = $scope.zoneList[i].zoneType; 
										rec.colorcode = $scope.colorcode;
										rec.zonearea=$scope.zoneList[i].area;
										// to draw circle with text
										
										
										if($scope.isPeopleTimeAnalyticsClicked==true || $scope.isAssetTimeAnalyticsClicked==true){
											rec.dWellTime = "";
											var radiusCircle = 0;
											for(var x=0;x<$scope.zoneDwelltime.length;x++){
												if($scope.zoneList[i].id == $scope.zoneDwelltime[x].zoneID)
													rec.dWellTime = $scope.zoneDwelltime[x].mapDwellTime;	
												radiusCircle = (rec.dWellTime/100)*10;
											}
											
											var pt =rec.getBounds().getCenter();														
											$scope.circleTextview =  $scope.circleWithText([pt.lat, pt.lng], rec.dWellTime, {radius:  radiusCircle});
											$scope.circleTextview.addTo(map);
										}
										
										if($scope.isPlantFloorAnalysisClicked==true){
											rec.noOfOccupancy = "";
											var radiusCircle = 0;
											for(var x=0;x<$scope.mapArrayDataPlantOccupancy.length;x++){
												if($scope.zoneList[i].id == $scope.mapArrayDataPlantOccupancy[x].zoneID)
													rec.noOfOccupancy = $scope.mapArrayDataPlantOccupancy[x].value;	
												// var radiusCircle =
												// (rec.noOfOccupancy/100)*10;
												 radiusCircle = rec.noOfOccupancy;
											}
											var pt =rec.getBounds().getCenter();														
											$scope.circleTextview =  $scope.circleWithText([pt.lat, pt.lng], rec.noOfOccupancy, {radius:  radiusCircle});
											$scope.circleTextview.addTo(map);
										}
										
										
										  }
									}
									
								  }
										
									} else {
										$scope.noList = true;
										$scope.valueList =false;
										$scope.errorMsg="No zones details are available";
									}
								}

							} else {
								$scope.noList = true;
								$scope.valueList =false;
								$scope.errorMsg="No zones details are available";
							}
						
		
	}
	$scope.getZoneClassifications = function() {
		if (!$rootScope.checkNetconnection()) { 				
		 	// $rootScope.alertDialogChecknet();
			return; 
			}
		$rootScope.showloading('#loadingBar'); 					
			$http({
				method : 'GET',
				url : './zoneClassification',
			})          
					.then(
							function success(response) {
								$rootScope.hideloading('#loadingBar'); 	
								if (response != null
										&& response.data != null
										&& response.data != "BAD_REQUEST") {
									$scope.zoneClassificationList.length = 0;
									var classifications = response.data.data;

									for (var i = 0; i < classifications.length; i++) {
										if (classifications[i].active) {
											$scope.zoneClassificationList
													.push(classifications[i]);
										}
									}

									// $scope.zoneClassificationList.sort(GetSortOrder("classification"));
									
									// $scope.searchClassification();
								} else {
									$scope.noList = true;
									$scope.errorMsg="Classification details are not available";
								}
							},function error(response) {
								$rootScope.hideloading('#loadingBar');			
								
					    });
		}
	$scope.getAllPathways = function() {
		if (!$rootScope.checkNetconnection()) { 				
		 	// $rootScope.alertDialogChecknet();
			return; 
			}
		$rootScope.showloading('#loadingBar');
		$scope.isPathwayEdit = false;

		$http({
			method : 'GET',
			url : './pathWay'
		})
				.then(
						function success(response) {
							$rootScope
									.hideloading('#loadingBar');
							if (response != null
									&& response.data != null
									&& response.data != "BAD_REQUEST") {

								$scope.pathwayList.length = 0;
								if (response.data.status == 'success') {
									for (var i = 0; i < response.data.data.length; i++) {
										if (response.data.data[i].active)
											$scope.pathwayList
													.push(response.data.data[i]);
									}

									if ($scope.pathwayList.length != 0) {
										for (var i = 0; i < $scope.pathwayList.length; i++) {

											var bounds = [];

											for (var j = 0; j < $scope.pathwayList[i].coordinates.length; j++) {
												bounds
														.push(pixelsToLatLng(
																parseFloat($scope.pathwayList[i].coordinates[j].x),
																parseFloat($scope.pathwayList[i].coordinates[j].y)));

											}

											var layer = null;
											if ($scope.pathwayList[i].geometryType == "polygon") {
												layer = L
														.polygon(
																bounds,
																{
																	color : $scope.pathwayList[i].colorCode,
																	weight : 3,
																	fillColor : '#00FFFFFF',
																	fill : 'url(dist/img/path/image.gif)'
																	
																})
														.addTo(
																map)
														.addTo(
																$scope.outlineDetailLayers);

											} else {
												layer = L
														.rectangle(
																bounds,
																{
																	color : $scope.pathwayList[i].colorCode,
																	weight : 3,
																	fillColor : '#00FFFFFF',
																	fill : 'url(dist/img/path/image.gif)'
																	
																})
														.addTo(
																map)
														.addTo(
																$scope.outlineDetailLayers);
											}

											layer.layerType = "Pathway";
											layer.campusId = $scope.pathwayList[i].campusDetails.campusId;
											layer.pathwayId = $scope.pathwayList[i].id+PATHWAY_START;
											layer.pathwayName = $scope.pathwayList[i].name;
											layer.colorCode = $scope.pathwayList[i].colorCode;
											layer.geometryType = $scope.pathwayList[i].geometryType;
											layer.coordinateList = bounds;

										}
										// var jsonText =
										// JSON.stringify($scope.pathwayList);
										// saveText(jsonText, "Pathways.json");

									}
									
									$scope.getAllPartitions();
								} else {
									
									$scope.error = true;
									$scope.alert_error_msg = response.data.message;
									$timeout(function() {
										$scope.error = false;
									}, 500);
								}
							}
						},
						function error(response) {
							$rootScope
									.hideloading('#loadingBar');
							
						});

	}
	$scope.getAllPartitions = function() {
		if (!$rootScope.checkNetconnection()) { 				
		 	// $rootScope.alertDialogChecknet();
			return; 
			}
		$rootScope.showloading('#loadingBar');
		$scope.isPartitionEdit = false;

		$http({
			method : 'GET',
			url : './partitionsDetails'
		})
				.then(
						function success(response) {
							$rootScope
									.hideloading('#loadingBar');
							if (response != null
									&& response.data != null
									&& response.data != "BAD_REQUEST") {

								$scope.partitionList.length = 0;
								if (response.data.status == 'success') {
									for (var i = 0; i < response.data.data.length; i++) {
										if (response.data.data[i].active)
											$scope.partitionList
													.push(response.data.data[i]);
									}

									if ($scope.partitionList.length != 0) {
										for (var i = 0; i < $scope.partitionList.length; i++) {

											var bounds = [];

											for (var j = 0; j < $scope.partitionList[i].coordinates.length; j++) {
												bounds
														.push(pixelsToLatLng(
																parseFloat($scope.partitionList[i].coordinates[j].x),
																parseFloat($scope.partitionList[i].coordinates[j].y)));

											}

											var layer = null;
											if ($scope.partitionList[i].geometryType == "polyline") {

												layer = L
														.polyline(
																bounds,
																{
																	color : $scope.partitionList[i].colorCode,
																	weight : 3,
																	fillColor : $scope.partitionList[i].colorCode,
																	fillOpacity : 0
																})
														.addTo(
																map)
														.addTo(
																$scope.outlineDetailLayers);
											} else if ($scope.partitionList[i].geometryType == "polygon") {
												layer = L
														.polygon(
																bounds,
																{
																	color : $scope.partitionList[i].colorCode,
																	weight : 3,
																	fillColor : '#00FFFFFF',
																	fillOpacity : 0
																})
														.addTo(
																map)
														.addTo(
																$scope.outlineDetailLayers);

											} else {
												layer = L
														.rectangle(
																bounds,
																{
																	color : $scope.partitionList[i].colorCode,
																	weight : 3,
																	fillColor : '#00FFFFFF',
																	fillOpacity : 0
																})
														.addTo(
																map)
														.addTo(
																$scope.outlineDetailLayers);
											}

											layer.outlinesId = $scope.partitionList[i].outlinesDetails.outlinesId+OUTLINE_START;
											layer.layerType = "Partition";
											layer.partitionId = $scope.partitionList[i].id+PARTITION_START;
											layer.partitionName = $scope.partitionList[i].name;
											layer.colorCode = $scope.partitionList[i].colorCode;
											layer.geometryType = $scope.partitionList[i].geometryType;
											layer.coordinateList = bounds;

										}
									}
									// var jsonText =
									// JSON.stringify($scope.partitionList);
									// saveText(jsonText, "Partitions.json");

									$scope.getAllWalls();
								}else {
									
									$scope.error = true;
									$scope.alert_error_msg = response.data.message;
									$timeout(function() {
										$scope.error = false;
									}, 500);
								}
							}
						},
						function error(response) {
							$rootScope.hideloading('#loadingBar');
							
						});

	}
	$scope.getAllWalls = function() {
		if (!$rootScope.checkNetconnection()) { 				
		 	// $rootScope.alertDialogChecknet();
			return; 
			}
		$rootScope.showloading('#loadingBar');
		$scope.isWallEdit = false;

		$http({
			method : 'GET',
			url : './wallsDetails'
		})
				.then(
						function success(response) {
							$rootScope
									.hideloading('#loadingBar');
							if (response != null
									&& response.data != null
									&& response.data != "BAD_REQUEST") {

								$scope.wallList.length = 0;
								if (response.data.status == 'success') {
									for (var i = 0; i < response.data.data.length; i++) {
										if (response.data.data[i].active)
											$scope.wallList
													.push(response.data.data[i]);
									}

									if ($scope.wallList.length != 0) {
										for (var i = 0; i < $scope.wallList.length; i++) {

											var bounds = [];

											for (var j = 0; j < $scope.wallList[i].coordinates.length; j++) {
												bounds
														.push(pixelsToLatLng(
																parseFloat($scope.wallList[i].coordinates[j].x),
																parseFloat($scope.wallList[i].coordinates[j].y)));

											}

											var layer = null;
											if ($scope.wallList[i].geometryType == "polyline") {

												layer = L
														.polyline(
																bounds,
																{
																	color : $scope.wallList[i].colorCode,
																	weight : 3,
																	fillColor : $scope.wallList[i].colorCode,
																	fillOpacity : 0
																})
														.addTo(
																map)
														.addTo(
																$scope.outlineDetailLayers);
											} else if ($scope.wallList[i].geometryType == "polygon") {
												layer = L
														.polygon(
																bounds,
																{
																	color : $scope.wallList[i].colorCode,
																	weight : 3,
																	fillColor : '#00FFFFFF',
																	fillOpacity : 0
																})
														.addTo(
																map)
														.addTo(
																$scope.outlineDetailLayers);

											} else {
												layer = L
														.rectangle(
																bounds,
																{
																	color : $scope.wallList[i].colorCode,
																	weight : 3,
																	fillColor : '#00FFFFFF',
																	fillOpacity : 0
																})
														.addTo(
																map)
														.addTo(
																$scope.outlineDetailLayers);
											}

											layer.outlinesId = $scope.wallList[i].outlinesDetails.outlinesId+OUTLINE_START;
											layer.layerType = "Wall";
											layer.wallId = $scope.wallList[i].id+WALL_START;
											layer.name = $scope.wallList[i].name;
											layer.colorCode = $scope.wallList[i].colorCode;
											layer.geometryType = $scope.wallList[i].geometryType;
											layer.coordinateList = bounds;

										}
									}
									// var jsonText =
									// JSON.stringify($scope.wallList);
									// saveText(jsonText, "Walls.json");

									$scope.getAllDoors();

								}else {
									
									$scope.error = true;
									$scope.alert_error_msg = response.data.message;
									$timeout(function() {
										$scope.error = false;
									}, 500);
								}
							}
						},
						function error(response) {
							$rootScope
									.hideloading('#loadingBar');
							
						});

	}
	
	$scope.getAllDoors = function() {
		if (!$rootScope.checkNetconnection()) { 				
		 	// $rootScope.alertDialogChecknet();
			return; 
			}
		$rootScope.showloading('#loadingBar');
		$http({
			method : 'GET',
			url : './doorsDetails'
		})
				.then(
						function success(response) {
							$rootScope
									.hideloading('#loadingBar');
							if (response != null
									&& response.data != null
									&& response.data != "BAD_REQUEST") {

								$scope.doorsList.length = 0;
								if (response.data.status == 'success') {
									for (var i = 0; i < response.data.data.length; i++) {
										if (response.data.data[i].active)
											$scope.doorsList
													.push(response.data.data[i]);
									}

									if ($scope.doorsList.length != 0) {
										for (var i = 0; i < $scope.doorsList.length; i++) {
											var bounds = [];

											for (var j = 0; j < $scope.doorsList[i].coordinates.length; j++) {
												bounds
														.push(pixelsToLatLng(
																parseFloat($scope.doorsList[i].coordinates[j].x),
																parseFloat($scope.doorsList[i].coordinates[j].y)));

											}

											var layer = L
													.polyline(
															bounds,
															{
																color : $scope.doorsList[i].colorCode,
																weight : 1,
																fillColor : $scope.doorsList[i].colorCode,
																fillOpacity : 0.5
															})
													.addTo(map)
													.addTo(
															$scope.outlineDetailLayers);

											layer.outlinesId = $scope.doorsList[i].outlinesDetails.outlinesId+OUTLINE_START;
											layer.layerType = "Door";
											layer.doorId = $scope.doorsList[i].id+DOOR_START;
											layer.doorName = $scope.doorsList[i].name;
											layer.doorColorCode = $scope.doorsList[i].colorCode;
											layer.geometryType = "Line";
											layer.coordinateList = bounds;
											

										}
										// var jsonText =
										// JSON.stringify($scope.doorsList);
										// saveText(jsonText, "Doors.json");

									}

								}
							}
						},
						function error(response) {
							$rootScope.hideloading('#loadingBar');
							
						});
	}
	
	// function to check active employees for people time analytics
    $scope.checkActiveEmployee= function(empKeyValue) {  
    		
    	 	
  // active/inactive
    	 	var cDate;
    	 	var mDate;
    	 	var cStatus;
	for (var n = 0; n < $scope.staffDetails.length; n++) {
		if($scope.staffDetails[n].empId == empKeyValue){
			
		 cDate = $scope.staffDetails[n].createdDate.slice(0,11);
		 mDate = $scope.staffDetails[n].modifiedDate.slice(0,11);
		 cStatus = $scope.staffDetails[n].active;
		}		
		
		if (cStatus == true) {	
			
			if (cDate <= $scope.enddate) {
			return true;
				
			}
		}
		else if (cStatus == false) {		
				if (($scope.startdate <= cDate) && 
						($scope.enddate <= cDate)) {

				if (cDate == $scope.enddate) {					
					return true;				
				}
			} else {			
				if (mDate >= $scope.startdate) {						
					return true;					
				}

			}			
		}
	}
}
	
	$scope.loadPeopleTimeAnalyticsColumnchart=function(){

    	var ticksStyle = {
		   	    fontColor: '#495057',
		   	    fontStyle: 'bold'
		   	  }

		   	  var mode = 'index'
		   	  var intersect = true	  	  

		   	 
		   	 
		   	var rectangleSet = false;
		   	  // eslint-disable-next-line no-unused-vars
		   
		   	$scope.empArray =  [];
		   	$scope.primaryDwellArray = [];
		   	$scope.secondaryDwellArray = [];
		   	$scope.thirdDwellArray = [];
		   	$scope.otherDwellArray  = [];
			$scope.staffArray =  [];
		   	$scope.staffDwellArray = [];	
		   	$scope.exportArrayData = [];
		   	$scope.chartHeaderTitle = ""
		   	if($scope.emp == "All"){		   	
		   		for (var i = 0; i < $scope.dataList.dwellTime.length; i++) {			   		
					var dateObj = $scope.dataList.dwellTime[i];				
				
					if (dateObj != undefined && dateObj != null) {
						var checkActive = $scope.checkActiveEmployee(dateObj.id);
						if(checkActive == true){
							var employeeID=dateObj.id;
							var zonedataObj=dateObj.zones;
							$scope.getLocationDetails(employeeID,zonedataObj);

						}		
				}			
			}
		   	 if($scope.startdate == $scope.enddate) 
					$scope.chartHeaderTitle="People Time Analytics on " +moment($scope.startdate,"YYYY-MMM-DD").format("MMM DD, YYYY");
			   
					else
					 $scope.chartHeaderTitle="People Time Analytics from "+$scope.dateRange;
			  
		   	}else{
		   		for (var i = 0; i < $scope.dataList.dwellTime.length; i++) {
		   			if($scope.dataList.dwellTime[i].id == $scope.emp){
		   				var dateObj = $scope.dataList.dwellTime[i];				
						
						if (dateObj != undefined && dateObj != null) {
							var checkActive = $scope.checkActiveEmployee(dateObj.id);
							if(checkActive == true){
								var employeeID=dateObj.id;
								var zonedataObj=dateObj.zones;
								// $scope.getLocationDetails(employeeID,zonedataObj);
								for (var j = 0; j < zonedataObj.length; j++) {	
									 //var zoneID = zonedataObj[j].zoneID;
									 //var zoneName = zonedataObj[j].zoneName;
									 //var staffDwellTime = zonedataObj[j].dwellTime;	
									 $scope.staffArray.push(zonedataObj[j].zoneName);						
									 $scope.staffDwellArray.push(zonedataObj[j].dwellTime);										
										$scope.exportArrayData.push({					
											'Zone Name':zonedataObj[j].zoneName,											
											'Employee Dwell Time': zonedataObj[j].dwellTime											
										});										
								}
							}
							
						}
						
		   			}
		   		
		   		}
		   		
		   	 if($scope.startdate == $scope.enddate) 
					$scope.chartHeaderTitle="People Time Analytics for "+ $scope.emp +" on " +moment($scope.startdate,"YYYY-MMM-DD").format("MMM DD, YYYY");
			   
					else
					 $scope.chartHeaderTitle="People Time Analytics  for "+ $scope.emp +"  from "+$scope.dateRange;			  
					
		   	}
		   	$scope.filename = "People-Time-Analytics_" + $scope.emp
			$scope.sheetname = "People-Time-Analytics_"+ $scope.emp
		   if($scope.staffArray.length == 0 && $scope.emp != "All"){ 
					$scope.chartHeaderTitle = ""
					$scope.norecords = true;
					$scope.mapView = false;
		    		$scope.chartViewShow = false;
		    		$scope.exportXLS = false;
		    		$scope.exportPDF = false;	
		    		$scope.expandButton = false;
					return;
				}
		   
		   	
		   // $('#idChartDisplayWithWrapper').empty();
		    $('#idChartDisplayWithWrapper').remove();
		  $('#chartview').append('<canvas id="idChartDisplayWithWrapper"  height="550" width="1200"></canvas>');
		 
		 	if($scope.emp == "All"){
		 	   var $PeopleTimeAnalyticsColumnchart = $('#idChartDisplayWithWrapper')
		   	  var chartTest = new Chart($PeopleTimeAnalyticsColumnchart, {
		   	    type: 'bar',		   	 
		   	    data: {		   	     
		   	    	labels: $scope.empArray,
		   	    	datasets: [
		   	        {
		   	label: 'Primary Zone',
		   	          backgroundColor: '#2E86C1',
		   	          borderColor: '#007bff',
		   	          data: $scope.primaryDwellArray
		   	        },
		   	        {
		   	label: 'Secondary Zone',
		   	          backgroundColor: '#27AE60',
		   	          borderColor: '#ced4da',
		   	          data: $scope.secondaryDwellArray 
		   	        },
		   	        {
		   	label: 'Third Zone',
		   	          backgroundColor: '#D2691E',
		   	          borderColor: '#1c4166',
		   	          data: $scope.thirdDwellArray
		   	        },
		   	        {
		   	label: 'Other Zones',		   	
		   			  backgroundColor: '#ff0000',
		   			  borderColor: '#ced4da',		   			   	        
		   			  data: $scope.otherDwellArray	   	       
		   	     },
		   	      ]
		   	    },
		   	    
		   	    options: {
		   	      maintainAspectRatio: true,
		   	 responsive: true,
		   	      tooltips: {
		   	        mode: mode,
		   	        intersect: intersect
		   	      },
		   	      hover: {
		   	        mode: mode,
		   	        intersect: intersect
		   	      },
		   	      legend: {
		   	        display: true,
		   	position: 'right'
		   	      },
		   	 animation:{
		   	        onComplete : function(){
		   	            /* Your code here */
		   	if (!rectangleSet) {
		   	                        var scale = window.devicePixelRatio;                      

		   	                        var sourceCanvas = chartTest.canvas;
		   	                        var copyWidth = chartTest.scales['y-axis-0'].width - 10;
		   	                        var copyHeight = chartTest.scales['y-axis-0'].height + chartTest.scales['y-axis-0'].top + 10;

		   	                        var targetCtx = document.getElementById("axis-Test").getContext("2d");

		   	                        targetCtx.scale(scale, scale);
		   	                        targetCtx.canvas.width = copyWidth * scale;
		   	                        targetCtx.canvas.height = copyHeight * scale;

		   	                        targetCtx.canvas.style.width = `${copyWidth}px`;
		   	                        targetCtx.canvas.style.height = `${copyHeight}px`;
		   	                        targetCtx.drawImage(sourceCanvas, 0, 0, copyWidth * scale, copyHeight * scale, 0, 0, copyWidth * scale, copyHeight * scale);

		   	                        var sourceCtx = sourceCanvas.getContext('2d');

		   	                        // Normalize coordinate system to use css
									// pixels.

		   	                        sourceCtx.clearRect(0, 0, copyWidth * scale, copyHeight * scale);
		   	                        rectangleSet = true;
		   	                    }        
		   	},
		   	onProgress: function () {
		   	                    if (rectangleSet === true) {
		   	                        var copyWidth = chartTest.scales['y-axis-0'].width;
		   	                        var copyHeight = chartTest.scales['y-axis-0'].height + chartTest.scales['y-axis-0'].top + 10;

		   	                        var sourceCtx = chartTest.canvas.getContext('2d');
		   	                        sourceCtx.clearRect(0, 0, copyWidth, copyHeight);
		   	                    }
		   	                }
		   	    },
		   	      scales: {
		   	        yAxes: [{
		   	          // display: false,
		   	          gridLines: {
		   	            display: true,
		   	            lineWidth: '4px',
		   	            color: 'rgba(0, 0, 0, .2)',
		   	            zeroLineColor: 'transparent'
		   	          },
		   	  scaleLabel: {
		   	        display: true,
		   	        labelString: 'Time(mins)'
		   	      },
		   	          ticks: $.extend({
		   	            beginAtZero: true,

		   	            // Include a dollar sign in the ticks
		   	            callback: function (value) {
		   	              // if (value >= 1000) {
		   	              // value /= 1000
		   	              // value += 'k'
		   	              // }

		   	              // return '$' + value
		   	              return value
		   	            }
		   	          }, ticksStyle)
		   	        }],
		   	        xAxes: [{
		   	          display: true,
		   	 scaleLabel: {
		   	        display: true,
		   	        labelString: 'Employee'
		   	      },
		   	          gridLines: {
		   	            display: true
		   	          },
		   	          ticks: ticksStyle
		   	        }]
		   	      }
		   	    }
		   	  });
		 	}else{
		 	   var $IndividualPeopleTimeAnalyticsColumnchart = $('#idChartDisplayWithWrapper')
		 	 	 var chartTest = new Chart($IndividualPeopleTimeAnalyticsColumnchart, {
				   	    type: 'bar',		   	 
				   	    data: {		   	     
				   	    	labels: $scope.staffArray,
				   	    	datasets: [
				   	        {
				   	label: 'Employee Dwell Time',
				   	          backgroundColor: '#2E86C1',
				   	          borderColor: '#007bff',
				   	          data: $scope.staffDwellArray
				   	        }
				   	      ]
				   	    },
				   	    
				   	    options: {
				   	      maintainAspectRatio: true,
				   	 responsive: true,
				   	      tooltips: {
				   	        mode: mode,
				   	        intersect: intersect
				   	      },
				   	      hover: {
				   	        mode: mode,
				   	        intersect: intersect
				   	      },
				   	      legend: {
				   	        display: true,
				   	position: 'right'
				   	      },
				   	 animation:{
				   	        onComplete : function(){
				   	            /* Your code here */
				   	if (!rectangleSet) {
				   	                        var scale = window.devicePixelRatio;                      

				   	                        var sourceCanvas = chartTest.canvas;
				   	                        var copyWidth = chartTest.scales['y-axis-0'].width - 10;
				   	                        var copyHeight = chartTest.scales['y-axis-0'].height + chartTest.scales['y-axis-0'].top + 10;

				   	                        var targetCtx = document.getElementById("axis-Test").getContext("2d");

				   	                        targetCtx.scale(scale, scale);
				   	                        targetCtx.canvas.width = copyWidth * scale;
				   	                        targetCtx.canvas.height = copyHeight * scale;

				   	                        targetCtx.canvas.style.width = `${copyWidth}px`;
				   	                        targetCtx.canvas.style.height = `${copyHeight}px`;
				   	                        targetCtx.drawImage(sourceCanvas, 0, 0, copyWidth * scale, copyHeight * scale, 0, 0, copyWidth * scale, copyHeight * scale);

				   	                        var sourceCtx = sourceCanvas.getContext('2d');

				   	                        // Normalize coordinate system to
											// use css
											// pixels.

				   	                        sourceCtx.clearRect(0, 0, copyWidth * scale, copyHeight * scale);
				   	                        rectangleSet = true;
				   	                    }        
				   	},
				   	onProgress: function () {
				   	                    if (rectangleSet === true) {
				   	                        var copyWidth = chartTest.scales['y-axis-0'].width;
				   	                        var copyHeight = chartTest.scales['y-axis-0'].height + chartTest.scales['y-axis-0'].top + 10;

				   	                        var sourceCtx = chartTest.canvas.getContext('2d');
				   	                        sourceCtx.clearRect(0, 0, copyWidth, copyHeight);
				   	                    }
				   	                }
				   	    },
				   	      scales: {
				   	        yAxes: [{
				   	          // display: false,
				   	          gridLines: {
				   	            display: true,
				   	            lineWidth: '1px',
				   	            color: 'rgba(0, 0, 0, .2)',
				   	            zeroLineColor: 'transparent'
				   	          },
				   	  scaleLabel: {
				   	        display: true,
				   	        labelString: 'Time(mins)'
				   	      },
				   	          ticks: $.extend({
				   	            beginAtZero: true,

				   	            // Include a dollar sign in the ticks
				   	            callback: function (value) {
				   	              // if (value >= 1000) {
				   	              // value /= 1000
				   	              // value += 'k'
				   	              // }

				   	              // return '$' + value
				   	              return value
				   	            }
				   	          }, ticksStyle)
				   	        }],
				   	        xAxes: [{
				   	          display: true,
				   	 scaleLabel: {
				   	        display: true,
				   	        labelString: 'Zone'		   	        	
				   	      },
				   	          gridLines: {
				   	            display: true
				   	          },
				   	          ticks: ticksStyle
				   	        }]
				   	      }
				   	    }
				   	  });
		 	}
	}
	
	
	$scope.getLocationDetails = function(employeeID,zonedataObj) {	
    	var totalDwell = 0; 
    	var primaryDwellValue = secondaryDwellValue = thirdDwellValue = otherDwellValue = 0;	    	
    	
    	for (var i = 0; i < $scope.staffDetails.length; i++) {				
			
			if(employeeID == $scope.staffDetails[i].empId){					
				var primaryZoneName=$scope.staffDetails[i].primaryZone;
				var secondaryZoneName=$scope.staffDetails[i].secondaryZone;
				var thirdZoneName=$scope.staffDetails[i].thirdZone;
			}
		}
		
		
				for (var j = 0; j < zonedataObj.length; j++) {	
						totalDwell = totalDwell + zonedataObj[j].dwellTime							
				if (zonedataObj[j].zoneName == primaryZoneName)	{
					 primaryDwellValue = zonedataObj[j].dwellTime
										
				}else if (zonedataObj[j].zoneName == secondaryZoneName)	{						
					  secondaryDwellValue = zonedataObj[j].dwellTime						
										
				}else if (zonedataObj[j].zoneName == thirdZoneName)	{
					 thirdDwellValue = zonedataObj[j].dwellTime							
					 		
				}	
           }
				otherDwellValue = totalDwell-(primaryDwellValue+secondaryDwellValue+ thirdDwellValue)
					
					$scope.empArray.push(employeeID);						
					 $scope.primaryDwellArray.push(primaryDwellValue);
					 $scope.secondaryDwellArray.push(secondaryDwellValue);
					 $scope.thirdDwellArray.push(thirdDwellValue);
					 $scope.otherDwellArray.push(otherDwellValue);
					/*
					 * if(primaryDwellValue == "" )
					 * $scope.primaryDwellArray.push(0); else
					 * $scope.primaryDwellArray.push(primaryDwellValue);
					 * 
					 * 
					 * if(secondaryDwellValue == "" )
					 * $scope.secondaryDwellArray.push(0); else
					 * $scope.secondaryDwellArray.push(secondaryDwellValue);
					 * 
					 * 
					 * if(thirdDwellValue == "") $scope.thirdDwellArray.push(0);
					 * else $scope.thirdDwellArray.push(thirdDwellValue);
					 */
					
					
					$scope.exportArrayData.push({					
						'Employee':employeeID,
						'Primary Zone Dwell Time': primaryDwellValue,
						'Secondary Zone Dwell Time':secondaryDwellValue,	
						'Third Zone Dwell Time':thirdDwellValue,
						'Other Zone Dwell Time':otherDwellValue
						
					});
					
					
	}
	
	
	$scope.loadVisitorTimeAnalyticsColumnchart=function(){


    	var ticksStyle = {
		   	    fontColor: '#495057',
		   	    fontStyle: 'bold'
		   	  }

		   	  var mode = 'index'
		   	  var intersect = true
		   	  var rectangleSet = false;
		   	  // eslint-disable-next-line no-unused-vars
		   
		   	$scope.visitorArray =  [];
		   	$scope.visitorDwellArray = [];	   		   	
		    $scope.exportArrayData = [];
		    
		    if($scope.subtype == "Visitor"){				
				$scope.visitor = $scope.visitor;
				for (var i = 0; i < $scope.visitorList.length; i++) {
				if($scope.visitorList[i].visitorId == $scope.visitor){
					$scope.visitorNAme = $scope.visitorList[i].firstName+ " " + $scope.visitorList[i].lastName
					}			
				}				
			}else if($scope.subtype == "Vendor"){				
				$scope.visitor = $scope.vendor;
				for (var i = 0; i < $scope.vendorList.length; i++) {
					if($scope.vendorList[i].visitorId == $scope.visitor){
					$scope.visitorNAme = $scope.vendorList[i].firstName+ " " + $scope.vendorList[i].lastName
				}			
			}
				
			}else if($scope.subtype == "Contractor"){				
				$scope.visitor = $scope.contractor;
				for (var i = 0; i < $scope.contractorList.length; i++) {
					if($scope.contractorList[i].visitorId == $scope.visitor){
					$scope.visitorNAme = $scope.contractorList[i].firstName+ " " + $scope.contractorList[i].lastName
				}			
			}
				
			}
		   		for (var i = 0; i < $scope.dataList.dwellTime.length; i++) {
		   			if($scope.dataList.dwellTime[i].id == $scope.visitor){
		   				var dateObj = $scope.dataList.dwellTime[i];				
						
						if (dateObj != undefined && dateObj != null) {
							
								//var visitorID=dateObj.id;
								var zonedataObj=dateObj.zones;
								
								for (var j = 0; j < zonedataObj.length; j++) {									
								 //var zoneName = zonedataObj[j].zoneName;
								 //var zoneID = zonedataObj[j].zoneID;
								 //var visiterDwellTime = zonedataObj[j].dwellTime;	
								 $scope.visitorArray.push(zonedataObj[j].zoneName);						
								 $scope.visitorDwellArray.push(zonedataObj[j].dwellTime);
								 $scope.exportArrayData.push({					
										'Zone Name':zonedataObj[j].zoneName,										
										'Dwell Time': zonedataObj[j].dwellTime
										
									});
								 
					}		
								
				}
		   		
		   	}
		   		}
		   		if( $scope.visitorArray.length == 0){
					// to show no data found
					$scope.chartHeaderTitle = ""
					$scope.norecords = true;
					$scope.mapView = false;
		    		$scope.chartViewShow = false;
		    		$scope.exportXLS = false;
		    		$scope.exportPDF = false;	
		    		$scope.expandButton = false;
					return;
				}
		   		$scope.filename = "People-Time-Analytics_" + $scope.visitor
				$scope.sheetname = "People-Time-Analytics_"+ $scope.visitor
		    if($scope.startdate == $scope.enddate) 
				$scope.chartHeaderTitle="People Time Analytics for " + $scope.visitorNAme +  " on " +moment($scope.startdate,"YYYY-MMM-DD").format("MMM DD, YYYY");
				else
				 $scope.chartHeaderTitle="People Time Analytics for " + $scope.visitorNAme +  "  from "+$scope.dateRange;
			
		   	
		 // $('#idChartDisplayWithWrapper').empty();
		   	
		    $('#idChartDisplayWithWrapper').remove();
			   $('#chartview').append('<canvas id="idChartDisplayWithWrapper"  height="550" width="1200"></canvas>');
			   var $visitorTimeAnalyticsColumnchart = $('#idChartDisplayWithWrapper')
		   	 var chartTest = new Chart($visitorTimeAnalyticsColumnchart, {
		   	    type: 'bar',		   	 
		   	    data: {		   	     
		   	    	labels: $scope.visitorArray,
		   	    	datasets: [
		   	        {
		   	label: 'Dwell Time',
		   	          backgroundColor: '#2E86C1',
		   	          borderColor: '#007bff',
		   	          data: $scope.visitorDwellArray
		   	        }
		   	      ]
		   	    },
		   	    
		   	    options: {
		   	      maintainAspectRatio: true,
		   	 responsive: true,
		   	      tooltips: {
		   	        mode: mode,
		   	        intersect: intersect
		   	      },
		   	      hover: {
		   	        mode: mode,
		   	        intersect: intersect
		   	      },
		   	      legend: {
		   	        display: true,
		   	position: 'right'
		   	      },
		   	 animation:{
		   	        onComplete : function(){
		   	            /* Your code here */
		   	if (!rectangleSet) {
		   	                        var scale = window.devicePixelRatio;                      

		   	                        var sourceCanvas = chartTest.canvas;
		   	                        var copyWidth = chartTest.scales['y-axis-0'].width - 10;
		   	                        var copyHeight = chartTest.scales['y-axis-0'].height + chartTest.scales['y-axis-0'].top + 10;

		   	                        var targetCtx = document.getElementById("axis-Test").getContext("2d");

		   	                        targetCtx.scale(scale, scale);
		   	                        targetCtx.canvas.width = copyWidth * scale;
		   	                        targetCtx.canvas.height = copyHeight * scale;

		   	                        targetCtx.canvas.style.width = `${copyWidth}px`;
		   	                        targetCtx.canvas.style.height = `${copyHeight}px`;
		   	                        targetCtx.drawImage(sourceCanvas, 0, 0, copyWidth * scale, copyHeight * scale, 0, 0, copyWidth * scale, copyHeight * scale);

		   	                        var sourceCtx = sourceCanvas.getContext('2d');

		   	                        // Normalize coordinate system to use css
									// pixels.

		   	                        sourceCtx.clearRect(0, 0, copyWidth * scale, copyHeight * scale);
		   	                        rectangleSet = true;
		   	                    }        
		   	},
		   	onProgress: function () {
		   	                    if (rectangleSet === true) {
		   	                        var copyWidth = chartTest.scales['y-axis-0'].width;
		   	                        var copyHeight = chartTest.scales['y-axis-0'].height + chartTest.scales['y-axis-0'].top + 10;

		   	                        var sourceCtx = chartTest.canvas.getContext('2d');
		   	                        sourceCtx.clearRect(0, 0, copyWidth, copyHeight);
		   	                    }
		   	                }
		   	    },
		   	      scales: {
		   	        yAxes: [{
		   	          // display: false,
		   	          gridLines: {
		   	            display: true,
		   	            lineWidth: '1px',
		   	            color: 'rgba(0, 0, 0, .2)',
		   	            zeroLineColor: 'transparent'
		   	          },
		   	  scaleLabel: {
		   	        display: true,
		   	        labelString: 'Time(mins)'
		   	      },
		   	          ticks: $.extend({
		   	            beginAtZero: true,

		   	            // Include a dollar sign in the ticks
		   	            callback: function (value) {
		   	              // if (value >= 1000) {
		   	              // value /= 1000
		   	              // value += 'k'
		   	              // }

		   	              // return '$' + value
		   	              return value
		   	            }
		   	          }, ticksStyle)
		   	        }],
		   	        xAxes: [{
		   	          display: true,
		   	 scaleLabel: {
		   	        display: true,
		   	        labelString: 'Zone'		   	        	
		   	      },
		   	          gridLines: {
		   	            display: true
		   	          },
		   	          ticks: ticksStyle
		   	        }]
		   	      }
		   	    }
		   	  });
		
	}
	
	$scope.loadAssetTimeAnalyticsColumnchart1=function(){

    	var ticksStyle = {
		   	    fontColor: '#495057',
		   	    fontStyle: 'bold'
		   	  }

		   	  var mode = 'index'
		   	  var intersect = true	  	  

		   	 
		   	 
		   	var rectangleSet = false;
		   	  // eslint-disable-next-line no-unused-vars
		   
    	$scope.assetArray =  [];
	   	$scope.assetDwellArray = [];	   		   	
	    $scope.exportArrayData = [];
	  
	   	
	   		for (var i = 0; i < $scope.dataList.dwellTime.length; i++) {
	   			if($scope.dataList.dwellTime[i].id == $scope.assetIDForData){
	   				var dateObj = $scope.dataList.dwellTime[i];				
					
					if (dateObj != undefined && dateObj != null) {
						
							//var visitorID=dateObj.id;
							var zonedataObj=dateObj.zones;
							
							for (var j = 0; j < zonedataObj.length; j++) {									
							 //var zoneName = zonedataObj[j].zoneName;
							 //var zoneID = zonedataObj[j].zoneID;
							 //var assetDwellTime = zonedataObj[j].dwellTime;	
							 $scope.assetArray.push(zonedataObj[j].zoneName);						
							 $scope.assetDwellArray.push(zonedataObj[j].dwellTime);
							
							 $scope.exportArrayData.push({					
									'Zone Name':zonedataObj[j].zoneName,										
									'Asset Dwell Time': zonedataObj[j].dwellTime,
									
								});
				}		
							
			}
	   		
	   	}
	   		}
	   		
	   		
	   		$scope.filename = "Asset-Time-Analytics_" + $scope.assetIDForData
			$scope.sheetname = "Asset-Time-Analytics_"+ $scope.assetIDForData
	   		
	   		$scope.chartHeaderTitle = ""
	    if($scope.startdate == $scope.enddate) 
			$scope.chartHeaderTitle="Asset Time Analytics for "+ $scope.assetIDForData+ " on " +moment($scope.startdate,"YYYY-MMM-DD").format("MMM DD, YYYY");
			else
			 $scope.chartHeaderTitle="Asset Time Analytics  for "+ $scope.assetIDForData+ " from "+$scope.dateRange;
		  
		   	
		   // $('#idChartDisplayWithWrapper').empty();
		    $('#idChartDisplayWithWrapper').remove();
		  $('#chartview').append('<canvas id="idChartDisplayWithWrapper"  height="550" width="1200"></canvas>');
		 
		 	
		 	   var $PeopleTimeAnalyticsColumnchart1 = $('#idChartDisplayWithWrapper')
		   	var chartTest = new Chart($PeopleTimeAnalyticsColumnchart1, {
		    	 
		   	    type: 'bar',		   	 
		   	    data: {		   	     
		   	    	labels: $scope.assetArray,
		   	    	datasets: [
		   	        {
		   	label: 'Asset Dwell Time',
		   	          backgroundColor: '#ad5405',
		   	          borderColor: '#007bff',
		   	          data: $scope.assetDwellArray
		   	        }
		   	      ]
		   	    },
		   	    options: {
		   	      maintainAspectRatio: true,
		   	 responsive: true,
		   	      tooltips: {
		   	        mode: mode,
		   	        intersect: intersect
		   	      },
		   	      hover: {
		   	        mode: mode,
		   	        intersect: intersect
		   	      },
		   	      legend: {
		   	        display: true,
		   	position: 'right'
		   	      },
		   	 animation:{
		   	        onComplete : function(){
		   	            /* Your code here */
		   	if (!rectangleSet) {
		   	                        var scale = window.devicePixelRatio;                      

		   	                        var sourceCanvas = chartTest.canvas;
		   	                        var copyWidth = chartTest.scales['y-axis-0'].width - 10;
		   	                        var copyHeight = chartTest.scales['y-axis-0'].height + chartTest.scales['y-axis-0'].top + 10;

		   	                        var targetCtx = document.getElementById("axis-Test").getContext("2d");

		   	                        targetCtx.scale(scale, scale);
		   	                        targetCtx.canvas.width = copyWidth * scale;
		   	                        targetCtx.canvas.height = copyHeight * scale;

		   	                        targetCtx.canvas.style.width = `${copyWidth}px`;
		   	                        targetCtx.canvas.style.height = `${copyHeight}px`;
		   	                        targetCtx.drawImage(sourceCanvas, 0, 0, copyWidth * scale, copyHeight * scale, 0, 0, copyWidth * scale, copyHeight * scale);

		   	                        var sourceCtx = sourceCanvas.getContext('2d');

		   	                        // Normalize coordinate system to use css
									// pixels.

		   	                        sourceCtx.clearRect(0, 0, copyWidth * scale, copyHeight * scale);
		   	                        rectangleSet = true;
		   	                    }        
		   	},
		   	onProgress: function () {
		   	                    if (rectangleSet === true) {
		   	                        var copyWidth = chartTest.scales['y-axis-0'].width;
		   	                        var copyHeight = chartTest.scales['y-axis-0'].height + chartTest.scales['y-axis-0'].top + 10;

		   	                        var sourceCtx = chartTest.canvas.getContext('2d');
		   	                        sourceCtx.clearRect(0, 0, copyWidth, copyHeight);
		   	                    }
		   	                }
		   	    },
		   	      scales: {
		   	        yAxes: [{
		   	          // display: false,
		   	          gridLines: {
		   	            display: true,
		   	            lineWidth: '4px',
		   	            color: 'rgba(0, 0, 0, .2)',
		   	            zeroLineColor: 'transparent'
		   	          },
		   	  scaleLabel: {
		   	        display: true,
		   	        labelString: 'Time(mins)'
		   	      },
		   	          ticks: $.extend({
		   	            beginAtZero: true,

		   	            // Include a dollar sign in the ticks
		   	            callback: function (value) {
		   	              // if (value >= 1000) {
		   	              // value /= 1000
		   	              // value += 'k'
		   	              // }

		   	              // return '$' + value
		   	              return value
		   	            }
		   	          }, ticksStyle)
		   	        }],
		   	        xAxes: [{
		   	          display: true,
		   	 scaleLabel: {
		   	        display: true,
		   	        labelString: 'Asset'
		   	      },
		   	          gridLines: {
		   	            display: true
		   	          },
		   	          ticks: ticksStyle
		   	        }]
		   	      }
		   	    }
		   	  });
		 	
	}
	
	$scope.loadPlantTrafficChart = function(){

		$scope.exportArrayData=[];	
		
		var zoneName='';
		var zoneKey='';
		$('#chart-area').empty(); 
		$scope.exportXLS = true;
		$scope.exportPDF = true;
		var series = []; 
		var series1 = [];
		var categories=[];
		var maxTime = 0;
		var startpos=0;
		var endpos=0;
		var dataLabelAndValue = [];
		
		$scope.dataList.sort(GetSortOrder1("date"));	
		$scope.norecords=false;
		// $scope.zone= $scope.previousZoneArrival
		$scope.zoneKeyArrival=$scope.zone;
		// $scope.getZoneFinalList($scope.zoneKeyArrival, $scope.startdate,
		// $scope.enddate);
				
		
		for(var st=0;st<$scope.timearray.length;st++){
			if(startpos==0){
				if($scope.timearray[st]==$scope.orgoperationalstarttime){
					if(st!=0 && st>4)// to set x-axis 1 hour before and after
										// the ORg' time
						startpos=st-4;
					else
						startpos = 0;
				}
			}
			if(endpos==0){
				if($scope.timearray[st]==$scope.orgoperationalendtime){
					if(st!=96 && st<=92)
						endpos=st+4;
					else
						endpos = 94
				}
			}
		}
		
		for(var ct=startpos;ct<=endpos;ct++){
			if($scope.timearray[ct].includes("00")){
				categories.push($scope.timearray[ct])

			}
				else{
					categories.push(""); 

			}
		}
		
		$scope.finalArrivalList=[];
		for (var i = 0; i < $scope.dataList.length; i++) {
			// if ($scope.FINAL_DAY_ARRAY.includes($scope.dataList[i].date))
				// {
			var dateObj = $scope.dataList[i];
			$scope.finalArrivalList[i]=dateObj;
				// }
		}
		for (var i = 0; i < $scope.finalArrivalList.length; i++) {
			var dateObj = $scope.finalArrivalList[i];
			if (dateObj != undefined && dateObj != null) {
				var zonedataObj=dateObj.zone;
				var chartXData = [];
				var obj = {};
				if(zonedataObj.length>0 && zonedataObj[0].zoneID!= undefined && zonedataObj[0].zoneID != null && zonedataObj[0].zoneID==$scope.zone){
					zoneName=zonedataObj[0].zoneName; 

					if (zonedataObj[0].dwellTime.maximum > maxTime)
						maxTime = zonedataObj[0].dwellTime.maximum;

					for(var ct=startpos;ct<=endpos;ct++){

						var pos="TQ"+(ct+1);
						if(ct<9)// changed as 9 from 10 - to display TQ10
								// instead of TQ010
							pos="TQ0"+(ct+1);
						if(zonedataObj[0].tq!=undefined&&zonedataObj[0].tq!=null&&zonedataObj[0].tq[pos]!=undefined&&zonedataObj[0].tq[pos]!=null){
							chartXData.push(zonedataObj[0].tq[pos])							
						}
						else{
							
							chartXData.push("0")
						}
					}
				
					series.push({					
						'name': moment((dateObj.date),"YYYY-MMM-DD").format("MMM DD, YYYY"), 
						'data': chartXData
					});						
				}
			}
		}		
		
		var dataArray=[];
		for(i=0;i<series.length;i++){ 				
			dataArray.push(series[i].data);	
									
			}	
		// to display chart data in excel
		for(i=0;i<categories.length;i++){ 	
			 var obj={};
			 obj['Time']=categories[i];
		for(j=0;j<series.length;j++){				
					obj[series[j].name]=dataArray[j][i];				 
			
			}
		$scope.exportArrayData.push(obj);
		}
		
		$scope.filename = "Plant_Traffic_"+zoneName  
		$scope.sheetname = "Plant_Traffic_"+ zoneName
		$scope.chartHeaderTitle = ""
        if($scope.startdate == $scope.enddate) 
			$scope.chartHeaderTitle="Plant Traffic  @ "+ zoneName + " on " +moment($scope.startdate,"YYYY-MMM-DD").format("MMM DD, YYYY");
			else
			 $scope.chartHeaderTitle="Plant Traffic @  "+ zoneName + " on " + $scope.dateRange;
			
		
		 dataLabelAndValue = {
				categories: categories,
				series: series
		};
		 
		 // to set border color for lines in line chart
		 dynamicColorArray =[];
			for(var i = 0; i<dataArray.length; i++){
				 dynamicColorArray.push('#'+(0x1000000+(Math.random())*0xffffff).toString(16).substr(1,6))
			}
			
			// to set X-asis label for
			// var firstIndex =
			// $scope.timearray.indexOf($scope.orgoperationalstarttime);
			// var lastIndex =
			// $scope.timearray.indexOf($scope.orgoperationalendtime);
			const labelForPlantTrafficchart = $scope.timearray.slice(startpos, endpos);
			
		// to draw line chart
			 
	         $('#idChartDisplayWithWrapper').remove();
			  $('#chartview').append('<canvas id="idChartDisplayWithWrapper"  height="550" width="1200"></canvas>');
	   	var plantTrafficCanvas = document.getElementById("idChartDisplayWithWrapper");
		var plantTrafficChartTimeLabel = [];
		var dataForDisplay= [];
		for(var i = 0; i<dataLabelAndValue.series.length; i++){
			var	dataOptions = {
			   	    label: dataLabelAndValue.series[i].name,
			   	    data: dataLabelAndValue.series[i].data,
			   	    lineTension: 0,
			   	    fill: false,
			   	   borderColor: dynamicColorArray[i]
			   	  };
			dataForDisplay.push(dataOptions);
			   	
			   	plantTrafficChartTimeLabel = {
			   	  labels: labelForPlantTrafficchart,
			   	  datasets: dataForDisplay
			   	}
		}
	   	

	   	var chartOptions = {
	   	  legend: {
	   	    display: true,
	   	    position: 'top',
	   	    labels: {
	   	      boxWidth: 80,
	   	      fontColor: 'black'
	   	    }
	   	  }	
	   	};
	 
	   	var lineChart = new Chart(plantTrafficCanvas, {
	   	  type: 'line',
	   	  data: plantTrafficChartTimeLabel,
	   	  options: {
	        /*
			 * scales: { y: { suggestedMin: 0, suggestedMax: 100 } },
			 */
	        legend: {
		   	    display: true,
		   	    position: 'top',
		   	    labels: {
		   	      boxWidth: 80,
		   	      fontColor: 'black'
		   	    }
		   	  }	
	    }
	   	});
	   	
		}
	$scope.loadPlantOccupancyChart = function(){  
		$scope.exportArrayData=[];
		
		var series = [];
		var employeeSeries = [];
		var visitorSeries = [];
		var vendorSeries = [];
		var contractorSeries = [];
		var totalSeries = [];
		var categories=[];
		var maxTime = 0;
		var startpos=0;
		var endpos=0;
		for(var st=0;st<$scope.timearray.length;st++){
			if(startpos==0){
				if($scope.timearray[st]==$scope.orgoperationalstarttime){
					if(st!=0 && st>4)
						startpos=st-4;
					else
						startpos = 0;
				}
			}
			if(endpos==0){
				if($scope.timearray[st]==$scope.orgoperationalendtime){
					if(st!=96 && st<=92)
						endpos=st+4;
					else
						endpos = 94
				}
			}
		}
		for(var ct=startpos;ct<=endpos;ct++){
			if($scope.timearray[ct].includes("00"))
				categories.push($scope.timearray[ct])
				else
					categories.push(""); 
		}
		for (var i = 0; i < $scope.dataList.length; i++) {
			var dateObj = $scope.dataList[i].orgAttendance;
			if (dateObj != undefined && dateObj != null) {

				var total = [];
				var employee = [];				
				var visitor = [];
				var vendor = [];
				var contractor = [];

				//var t,e,vi="";

				for(var ct=startpos;ct<=endpos;ct++){

					var pos="TQ"+(ct+1);
					
					if(ct<9) // changed as 9 from 10 - to display TQ10
								// instead of TQ010
						pos="TQ0"+(ct+1);
					
					if(dateObj!=undefined&&dateObj!=null&&dateObj[pos]!=undefined&&dateObj[pos]!=null){

						if(dateObj[pos].tot!=undefined&&dateObj[pos].tot!=null){
							total.push(dateObj[pos].tot);
							
							if (dateObj[pos].tot > maxTime)
								maxTime = dateObj[pos].tot;
						}else
							total.push("0");
						
						if(dateObj[pos].user!=undefined&&dateObj[pos].user!=null){
							if(dateObj[pos].user.emp!=undefined&&dateObj[pos].user.emp!=null)
								employee.push(dateObj[pos].user.emp);							
							else
								employee.push("0");
							
							if(dateObj[pos].user.vis!=undefined&&dateObj[pos].user.vis!=null)
								visitor.push(dateObj[pos].user.vis);								
							else
								visitor.push("0");
							if(dateObj[pos].user.ven!=undefined&&dateObj[pos].user.ven!=null)
								vendor.push(dateObj[pos].user.ven);								
							else
								vendor.push("0");
							if(dateObj[pos].user.con!=undefined&&dateObj[pos].user.con!=null)
								contractor.push(dateObj[pos].user.con);								
							else
								contractor.push("0");
						}else
						{
							employee.push("0");			
							visitor.push("0");
							vendor.push("0");
							contractor.push("0");
							
						}
					}
					else{ 
						total.push("0");
						employee.push("0");						
						visitor.push("0");
						vendor.push("0");
						contractor.push("0");
					} 

				}
				employeeSeries.push(employee);
				visitorSeries.push(visitor);  
				vendorSeries.push(vendor);
				contractorSeries.push(contractor);
				totalSeries.push(total);	
			}
		}
		for(var i=0; i<categories.length; i++){
		    $scope.exportArrayData.push({
		    	"Time": categories[i],
		    	"Employee": employee[i],
		    	"Visitor": visitor[i],		    	
		    	"Vendor": vendor[i],		    	
		    	"Contractor": contractor[i],		    	
		    	"Total": total[i]
		    });		    
		}
		
		$scope.chartHeaderTitle = "";
		$scope.chartHeaderTitle = "Plant Occupancy  on " + moment($('#idPFAEditStartDatePicker').val()).format("MMM DD, YYYY");	
		$scope.dateRange =  moment($('#idPFAEditStartDatePicker').val()).format("MMM DD, YYYY");
		
		// to display plant analysis line chart
		 
        $('#idChartDisplayWithWrapper').remove();
		$('#chartview').append('<canvas id="idChartDisplayWithWrapper"  height="550" width="1200"></canvas>');
	  	var plantOccupancyCanvas = document.getElementById("idChartDisplayWithWrapper");

	   	var data1 = {
	   	    label: "Employee",
	   	    data: employeeSeries[0],
	   	    lineTension: 0,
	   	    fill: false,
	   	    borderColor: '#FF6347'
	   	  };

	   	var data2 = {
	   	    label: "Visitor",
	   	    data: visitorSeries[0],
	   	    lineTension: 0,
	   	    fill: false,
	   	    borderColor: '#006400'
	   	  };
	   	var data3 = {
		   	    label: "Vendor",
		   	    data: vendorSeries[0],
		   	    lineTension: 0,
		   	    fill: false,
		   	    borderColor: '#006464'
		   	  };
	   	var data4 = {
		   	    label: "Contractor",
		   	    data: contractorSeries[0],
		   	    lineTension: 0,
		   	    fill: false,
		   	    borderColor: '#646400'
		   	  };
		var data5 = {
		   	    label: "Total",
		   	    data: totalSeries[0],
		   	    lineTension: 0,
		   	    fill: false,
		   	    borderColor: '#00008b'
		   	  };		
		
		// to set label for line chart x-axis
			
		const labelForPlantOccupancychart = $scope.timearray.slice(startpos, endpos);
		
	   	var plantOccupancyData = {
	   	  labels: labelForPlantOccupancychart,
	   	  datasets: [data1, data2, data3, data4, data5]
	   	};

	   	var chartOptions = {
	   	  legend: {
	   	    display: true,
	   	    position: 'top',
	   	    labels: {
	   	      boxWidth: 80,
	   	      fontColor: 'black'
	   	    }
	   	  }
	   	};

	   	var lineChart = new Chart(plantOccupancyCanvas, {
	   	  type: 'line',
	   	  data: plantOccupancyData,
	   	  options: chartOptions
	   	});		
	}
	
	
	$scope.loadWorkOrderTable = function(){
		$scope.isDateSelected =true;
		$scope.chartViewShow = false
		$('#idChartAgingCanvas').remove();
		$('#chartviewAgingchart').append('<canvas id="idChartAgingCanvas"  height="0" ></canvas>');
		
			$scope.workOrderFlowData=[];
			$scope.exportArrayData = [];
			$scope.chartHeaderTitle = ""
				 if($scope.startdate == $scope.enddate) 
						$scope.chartHeaderTitle="Work Order Flow Analytics on " +moment($scope.startdate,"YYYY-MMM-DD").format("MMM DD, YYYY");
						else
						 $scope.chartHeaderTitle="Work Order Flow Analytics from "+$scope.dateRange;
			
	for(var i=0; i<$scope.dataList.length;i++){		
		var timein = timeout = totalTime = ""; 
		timein = ($scope.dataList[i].inTime).substring(12);
		timeout = ($scope.dataList[i].outTime).substring(12);
		totalTime = $scope.dataList[i].totalTime;
		$scope.workOrderFlowData.push({"workOrderNumber":$scope.dataList[i].workOrderNumber,"inTime":timein,"outTime":timeout,"totalTime":totalTime})
		$scope.exportArrayData.push({"WorkOrder Number":$scope.dataList[i].workOrderNumber,"In Time":timein,"Out Time":timeout,"Total Time":totalTime})
	    }
			
			if ($scope.workOrderFlowData.length == 0) {
				$scope.materialNoData = true;
				return;
			} else {
				$scope.materialNoData = false;
				$scope.searchButton = true;
			}

			if ($scope.workOrderFlowData.length > 10) {
				$scope.PaginationTab = true;
			} else {
				$scope.PaginationTab = false;
			}
			$scope.totalItems = $scope.workOrderFlowData.length;
				
			$scope.getTotalPages();

			$scope.currentPage = 1;
			if ($scope.workOrderFlowData.length > $scope.numPerPage) {
				$scope.workOrderFlowViewData = $scope.workOrderFlowData
						.slice(	0,$scope.numPerPage);
				$scope.hasPrevious = true;
				$scope.isFirstPage = true;
				$scope.hasNext = false;
				$scope.isLastPage = false;
			} else {
				$scope.workOrderFlowViewData = $scope.workOrderFlowData;
			}
			
			
	}
	
	$scope.loadWorkOrderChart = function(){
	
		var startpos=0;
		var endpos=0; 
	 	var pos=0; 
		var categories=[]; 
		var series = [];
		 $scope.exportArrayData=[];      
			
			for(var st=0;st<$scope.timearray.length;st++){
			if(startpos==0){
			if($scope.timearray[st]=="09:00 AM"){
				if(st!=0 && st>4)
					startpos=st-4;
				else
					startpos = 0;
				}
			}
			if(endpos==0){
			if($scope.timearray[st]=="06:00 PM"){
				if(st!=96 && st<=92)
					endpos=st+4;
				else
					endpos = 94
			}
			}
			}
		for(var ct=startpos;ct<=endpos;ct++){
		if($scope.timearray[ct].includes("00")){
			categories.push($scope.timearray[ct])

		}
			else{
				categories.push(""); 

		}
		}
		var firstIndex = startpos;// arr.indexOf($scope.orgoperationalstarttime);
									// 11Jan
		var lastIndex = endpos;// arr.indexOf($scope.orgoperationalendtime);
								// 11Jan
		const labelForWOFlowchart = $scope.timearray.slice(firstIndex, lastIndex);
		var dateSelection={};
		
		 $scope.chartHeaderTitle = ""
			 if($scope.startdate == $scope.enddate) 
					$scope.chartHeaderTitle = "Work Order Flow Analysis" + " on " +moment($scope.startdate,"YYYY-MMM-DD").format("MMM DD, YYYY");
					else
					 $scope.chartHeaderTitle= "Work Order Flow Analysis" + " from "+$scope.dateRange;
	 
						$scope.exportXLS = true;
						$scope.exportPDF = true;
	               
					var visible=true;				
					for(var j=0; j<$scope.dataList.length;j++){
						if(j>1)
						visible=false;
						var tQData =$scope.dataList[j];
						if (tQData != undefined && tQData != null) {
						var chartXData = [];
						for(var ct=startpos;ct<=endpos;ct++){
						var pos="TQ"+(ct+1);
						if(ct<9)// changed as 9 from 10 - to display TQ10
								// instead of TQ010
						pos="TQ0"+(ct+1);
						if(tQData.tQ!=undefined && tQData.tQ!=null && tQData.tQ[pos]!=undefined && tQData.tQ[pos]!=null){
						chartXData.push(tQData.tQ[pos].pr)
						
						}
						else{						
						chartXData.push("0")
						} 						

			   			}
					series.push( {
					// 'name':new Date(dateObj.date).toUTCString().slice(5,
					// 16),
					'name': tQData.workStationName,													
					'data': chartXData
					});					
 
						}
						
						}
				if(series.length>0){
							$scope.norecords = false;
				
						// to display chart data in excel
						for(i=0;i<categories.length;i++){ 	
							 var obj={};
							 obj['Time']=categories[i];
							for(j=0;j<series.length;j++){				
									obj[series[j].name]= series[j].data[i];					 
							
							}
						$scope.exportArrayData.push(obj);
						}
						dynamicColorArray =[];
						for(var i = 0; i<series.length; i++){
							 dynamicColorArray.push('#'+(0x1000000+(Math.random())*0xffffff).toString(16).substr(1,6))
						}
						
						// to set X-asis label for
						
						var firstIndex = startpos;// arr.indexOf($scope.orgoperationalstarttime);
													// 11Jan
						var lastIndex = endpos;// arr.indexOf($scope.orgoperationalendtime);
												// 11Jan
						const labels  = $scope.timearray.slice(firstIndex, lastIndex);
						
					// to draw line chart
						 $('#js-legend').empty();						
						 $('#myWOLineChart').remove();
						 $('#chartviewMaterialLineChart').append('<canvas id="myWOLineChart"  height="600" width="600"></canvas>');
					   	 // var woLineChartCanvas =
							// document.getElementById('myWOLineChart').getContext("2d");
					 	 var woLineChartCanvas = $('#myWOLineChart')
					   	var dataSets = []; 	
					for(var i = 0; i<series.length; i++){							
						var values = {
					   	    label: series[i].name,
					   	    backgroundColor: dynamicColorArray[i],
					   	    borderColor: dynamicColorArray[i],
					   	    data: series[i].data,
					   	  }
						dataSets.push(values);
					   	
					}  
						   	
						   	var data = {
						   		  labels: labels,
						   		  datasets: dataSets
						   		};
						
						var mode = 'index';
						var intersect = true;							
						
						// var ctx=
						// document.getElementById('myWOLineChart').getContext("2d");
						
						 var myWOLineChart = new Chart(woLineChartCanvas,{
						  type: 'line',
						  data,						  
						  options: { 
						       maintainAspectRatio: false,
							  responsive: true,
						      tooltips: {
						        mode: mode,
						        intersect: intersect
						      },
						      hover: {
						        mode: mode,
						        intersect: intersect
						      },
			                  elements: {
			                      line: {
			                              fill: false, 
			                              tension: 0
			                      }
			                  }
						    }
						  });

						
						// myWOLineChart.options.plugins.legend.position =
						// 'right';
						// myWOLineChart.options.plugins.legend.display= false;
						 myWOLineChart.legend.position = 'right';
						 myWOLineChart.legend.options.display= false;
						     
						// Add custom-legends as list and attach click event to
						// that
						var legendListId = document.getElementById("js-legend");
						customLegends(myWOLineChart,legendListId);
						

						function customLegends(chart, legendListId){
							for(var i=0; i < chart.data.datasets.length; i++){	
						      var li = document.createElement("li");
						      li.style.color =	chart.data.datasets[i].backgroundColor;
						      li.style.border = chart.data.datasets[i].borderColor;
						      li.setAttribute("id",i+2000);
						      li.appendChild(document.createTextNode(chart.data.datasets[i].label));
						      legendListId.appendChild(li); 
						     /*
								 * $('li').each(function (i) {
								 * $(this).attr('id', (i)); });
								 */
						    }
						    // Add click event to Custom-Legends being clicked
								$('li').click(function (event) {    		
						         var index = $(this).index();
						         var x = document.getElementById(index+2000); 
						         var hidden = false;  
						         if (typeof (chart.data.datasets[index].visible) === "undefined" || chart.data.datasets[index].visible) {
						           	chart.data.datasets[index].visible = false;
						         } else {
						           	chart.data.datasets[index].visible = true;
						         }

						        var meta = chart.getDatasetMeta(index);
						        meta.hidden = !chart.data.datasets[index].visible;
						        const legendLabelSpan = document.getElementById(index+2000);
						        legendLabelSpan.style.textDecoration = !chart.data.datasets[index].visible ? 'line-through' : '';
						        myWOLineChart.render();
						        myWOLineChart.update();
						  	});  
						}
					
						
						
						
											
						}
				
					
	}
	
	function GetSortOrder1(prop) {    
		return function(a, b) {    
			if(a[prop]!=null&&b[prop]!=null){
				if (a[prop].toLowerCase() > b[prop].toLowerCase()) {    
					return 1;    
				} else if (a[prop].toLowerCase() < b[prop].toLowerCase()) {    
					return -1;    
				}    
			}
			return 0;    
		}    
	}  
	$scope.loadWorkStationFlowAnalysis = function(){
		$scope.ZoneLabelList = [];
		$scope.WaitingData = [];
		$scope.OutgoingData = [];
		$scope.IncomingData = [];
		$scope.exportArrayData=[];
		var stationName = ""
		var	stationNameTitle = ""
		if($scope.workStation == "All"){
			stationNameTitle = "All"
			for(var i = 0; i < $scope.dataList.length; i++){
				var incomingTotal = outgoingTotal = waitingTotal = 0;
				var stationId = $scope.dataList[i].workStationid;
					stationName = $scope.dataList[i].workStationName;
				var stationOBJ = $scope.dataList[i].workStationAnalytics;
				
					for(var j = 0; j < stationOBJ.length; j++){
						incomingTotal = incomingTotal + stationOBJ[j].incoming;
						outgoingTotal = outgoingTotal + stationOBJ[j].outgoing;
						waitingTotal = waitingTotal + stationOBJ[j].waiting;
					}
					$scope.IncomingData.push(incomingTotal);
					$scope.OutgoingData.push(outgoingTotal);
					$scope.WaitingData.push(waitingTotal);				
					$scope.ZoneLabelList.push(stationName);
					$scope.exportArrayData.push({"WorkStation Name":stationName,"Incoming":incomingTotal,"Outgoing":outgoingTotal,"Waiting":waitingTotal})
			    
			}
		}else{
			for(var i = 0; i < $scope.dataList.length; i++){
	   			if($scope.dataList[i].workStationid == $scope.workStation){
	   				var stationOBJ = $scope.dataList[i].workStationAnalytics;				
					
					if (stationOBJ != undefined && stationOBJ != null) {
						var incomingTotal = outgoingTotal = waitingTotal = 0;
						 stationName = $scope.dataList[i].workStationName;
						 stationNameTitle = $scope.dataList[i].workStationName;
							for (var j = 0; j < stationOBJ.length; j++) {
							incomingTotal = incomingTotal + stationOBJ[j].incoming;
							outgoingTotal = outgoingTotal + stationOBJ[j].outgoing;
							waitingTotal = waitingTotal + stationOBJ[j].waiting;
							}	
			
					$scope.IncomingData.push(incomingTotal);
					$scope.OutgoingData.push(outgoingTotal);
					$scope.WaitingData.push(waitingTotal);				
					$scope.ZoneLabelList.push(stationName);
					$scope.exportArrayData.push({"WorkStation Name":stationName,"Incoming":incomingTotal,"Outgoing":outgoingTotal,"Waiting":waitingTotal})
				    
					}else{
						$scope.chartViewShow = false;
						$scope.exportXLS = false;
						$scope.exportPDF = false;
						$scope.expandButton = false;
						$scope.mapView = false;
						// to show no data found
						$scope.chartHeaderTitle = ""
						$scope.norecords = true;
						// to remove canvas when loaded initially to show please
						// select text
						$('#idChartAgingCanvas').remove();
						$('#chartviewAgingchart').append('<canvas id="idChartAgingCanvas"  height="0" ></canvas>');
					}
	   			}
			}
		}
		
		$scope.drawWorkOrderFlowChart($scope.IncomingData, $scope.OutgoingData, $scope.WaitingData, $scope.ZoneLabelList, stationNameTitle);
	}
	
	$scope.drawWorkOrderFlowChart = function(IncomingData, OutgoingData, WaitingData, ZoneLabelList, stationNameTitle){
		$scope.filename = "WorkStation_Flow_Analysis_" + stationNameTitle
		$scope.sheetname = "WorkStation_Flow_Analysis_" + stationNameTitle
		$scope.chartHeaderTitle = ""			 
			if($scope.startdate == $scope.enddate) 
					$scope.chartHeaderTitle="Work Station Flow Analytics for " + stationNameTitle + " on " +moment($scope.startdate,"YYYY-MMM-DD").format("MMM DD, YYYY");
					else
					 $scope.chartHeaderTitle="Work Station Flow Analytics for " + stationNameTitle + " from "+$scope.dateRange;
		var ticksStyle = {
				fontColor: '#495057',
				fontStyle: 'bold'
			}
			var mode = 'index'
			var intersect = true
			 $scope.sortingChat = function(dataSortIndex,otherIndex1,otherIndex2) {
		    	// Sorting data and label based on sorting type
		    	var dataset = $scope.ZoneFloorChart.data.datasets[dataSortIndex];
		    	var dataset1 = $scope.ZoneFloorChart.data.datasets[otherIndex1];
		    	var dataset2 = $scope.ZoneFloorChart.data.datasets[otherIndex2];
		    	for(var i = 0; i < dataset.data.length; i++){
		    	     
		    		   // Last i elements are already in place
		    		   for(var j = 0; j < ( dataset.data.length - i -1 ); j++){
		    		       
		    		     // Checking if the item at present iteration
		    		     // is greater than the next iteration
		    		     if(parseInt(dataset.data[j]) < parseInt(dataset.data[j+1])){
		    		         
		    		       // If the condition is true then swap them
		    		       var temp = dataset.data[j];
		    		       var tempOtherValue1 = dataset1.data[j];
		    		       var tempOtherValue2 = dataset2.data[j];
		    		       var tmpLabel = $scope.ZoneFloorChart.data.labels[j];
		    		       
		    		       dataset.data[j] = dataset.data[j + 1];
		    		       dataset1.data[j] = dataset1.data[j + 1];
		    		       dataset2.data[j] = dataset2.data[j + 1];
		    		       
		    		       $scope.ZoneFloorChart.data.labels[j]=$scope.ZoneFloorChart.data.labels[j+1]
		    		       
		    		       dataset.data[j+1] = temp;
		    		       dataset1.data[j+1] = tempOtherValue1;
		    		       dataset2.data[j+1] = tempOtherValue2;

		    		       $scope.ZoneFloorChart.data.labels[j+1] = tmpLabel;
		    		     }
		    		   }
		    		 }
		    	$scope.ZoneFloorChart.update(); // update the chart
		    }
		$('#idChartAgingCanvas').remove();
		$('#chartviewAgingchart').append('<canvas id="idChartAgingCanvas"  height="550" ></canvas>');
				  var $ZoneFloorChartInHtml = $('#idChartAgingCanvas')
				  // eslint-disable-next-line no-unused-vars
				  
				  // eslint-disable-next-line no-unused-vars
				  $scope.ZoneFloorChart = new Chart($ZoneFloorChartInHtml, {
				    type: 'bar',
				    data: {
				      labels: ZoneLabelList,
				      datasets: [
						        {
									label: 'Incoming', 
						          backgroundColor: '#6958db',
						          borderColor: '#6958db',
						          data: IncomingData
						        },
						        {
									label: 'Outgoing',
						          backgroundColor: 'green',
						          borderColor: 'green',
						          data: OutgoingData
						        },
						        {
									label: 'Waiting',
						          backgroundColor: '#007bff',
						          borderColor: '#007bff',
						          data: WaitingData
						        }
						      ]
				    },
				    options: {
				      maintainAspectRatio: false,
					  responsive: true,
				      tooltips: {
				        mode: mode,
				        intersect: intersect
				      },
				      hover: {
				        mode: mode,
				        intersect: intersect
				      },
				      legend: {
				        display: true,
						position: 'top'
				     },
				      scales: {
				        yAxes: [{
				          // display: false,
				          gridLines: {
				            display: true,
				            lineWidth: '4px',
				            color: 'rgba(0, 0, 0, .2)',
				            zeroLineColor: 'transparent'
				          },
						   scaleLabel: {
				        display: true,
				        labelString: '# Of Work Order'
				      },
				          ticks: $.extend({
				            beginAtZero: true,

				            // Include a dollar sign in the ticks
				            callback: function (value) {
				              // if (value >= 1000) {
				              // value /= 1000
				              // value += 'k'
				              // }

				              return value
				              // /return value
				            }
				          }, ticksStyle)
				        }],
				        xAxes: [{
				          display: true,
						  scaleLabel: {
				        display: true,
				        labelString: 'Work Station'
				      },
				          gridLines: {
				            display: true
				          },
				          ticks: ticksStyle
				        }]
				      }
				    }
				  })  
			$scope.sortingChat(0,1,2);
		 
		    	var dataSortIndex = 0;
		    	var otherIndex1,otherIndex2;
		    	if ($scope.varSortType == 0) {
		    		
		    		dataSortIndex = 0
		    		otherIndex1=1;
		    		otherIndex2=2;
		    		
		    		
		    	}else if ($scope.varSortType == 1){
		    		
		    		dataSortIndex = 1;
		    		otherIndex1=0;
		    		otherIndex2=2;
		    	}else {
		    		dataSortIndex = 2;
		    		otherIndex1=0;
		    		otherIndex2=1;
		    		
		    	}
		    	$scope.sortingChat(dataSortIndex,otherIndex1,otherIndex2);
		    	
	}
	$scope.loadConstraintAnalysis = function(){
		var zoneList = [];
		var zoneData = [];
		var zoneBarColor = [];
	
		$scope.dataListInDescOrder = [];
		$scope.exportArrayData = [];
		$scope.chartHeaderTitle = ""
		var ticksStyle = {
				fontColor: '#495057',
				fontStyle: 'bold'
			}
			var mode = 'index'
			var intersect = true
			$scope.chartHeaderTitle = "Constraint Analytics from "+$scope.dateRange;
			/*
			 * $scope.dataList = [ { "workStationid": "StationID1",
			 * "workStationName": "StationType1", "constraintValue": 1.0 }, {
			 * "workStationid": "StationID2", "workStationName": "StationType2",
			 * "constraintValue": 2.0 }, { "workStationid": "StationID3",
			 * "workStationName": "StationType3", "constraintValue": 4.0 } ]
			 */
		// to sort array in descending order by constraint Value
		$scope.dataList.sort((a, b) => b.constraintValue - a.constraintValue);

		$scope.dataList.forEach((e) => {		   
		    $scope.dataListInDescOrder.push({
		    	 "workStationid": e.workStationid,
		           "workStationName": e.workStationName,
		           "constraintValue": e.constraintValue
				});
		    $scope.exportArrayData.push({
				'Work Station': e.workStationName,
				'Constraint Value': e.constraintValue
				});
		});
		
		for(var i=0; i<$scope.dataListInDescOrder.length;i++){
			var workStationName = $scope.dataListInDescOrder[i].workStationName;
			var constraintValue = $scope.dataListInDescOrder[i].constraintValue;
			
			zoneList.push(workStationName);
			zoneData.push(constraintValue);
			zoneBarColor.push('#007bff');			
			} 		
		
			   function generateLabels() {
		        var chartLabels = [];
		        // for (x = 1; x < zoneList.length+1; x++) {
					chartLabels.push(zoneList[0]);
		        // }
		        return chartLabels;
		    }

		    function generateData() {
		        var chartData = [];
		       // for (x = 1; x < zoneList.length+1; x++) {
		            chartData.push(zoneData[0]);
		        // }
		        return chartData;
		    }

		    function addData(numData, chart) {
		        for (var i = 1; i < zoneList.length; i++) {
		            chart.data.datasets[0].data.push(zoneData[i]);
		            chart.data.labels.push(zoneList[i]);
		            var newwidth = $('.chartAreaWrapper2').width() + 40;
		            $('.chartAreaWrapper2').width(newwidth);
		        }
		    }

		    var chartData = {
		            labels: generateLabels(),
		            datasets: [{
		                label: "Days",
		                data: generateData(),
		    			backgroundColor: zoneBarColor
		              // borderColor: '#007bff'
		    			
		    			
		            }]
		        };				  
		         var rectangleSet = false;
		         
		         $('#idChartDisplayWithWrapper').remove();
				  $('#chartview').append('<canvas id="idChartDisplayWithWrapper"  height="550" width="1200"></canvas>');
				  
				  var $constraintChartInHtml = $('#idChartDisplayWithWrapper')
				  // eslint-disable-next-line no-unused-vars
				  var constraintChart = new Chart($constraintChartInHtml, {
			            type: 'bar',
			            data: chartData,
			            maintainAspectRatio: false,
			            responsive: true,
			            options: {
			                tooltips: {
			                    titleFontSize: 0,
			                    titleMarginBottom: 0,
			                    bodyFontSize: 12
			                },
			                legend: {
			                    display: true,
			                     position: 'right'
			                },
			                scales: {
			                    xAxes: [{
			          display: true,
					  scaleLabel: {
			        display: true,
			        labelString: 'Work Station'
			      },
			          gridLines: {
			            display: true
			          },
			          ticks: ticksStyle
			        }],
			                    yAxes: [{
			          // display: false,
			          gridLines: {
			            display: true,
			            lineWidth: '4px',
			            color: 'rgba(0, 0, 0, .2)',
			            zeroLineColor: 'transparent'
			          },
					   scaleLabel: {
			        display: true,
			        labelString: 'Days'
			      },
			          ticks: $.extend({
			            beginAtZero: true,

			            // Include a dollar sign in the ticks
			            callback: function (value) {
						value = value * 10
			              // if (value >= 1000) {
			              // value /= 1000
			              // value += 'k'
			              // }

			              // return '$' + value
			              return value
			            }
			          }, ticksStyle)
			        }]
			                },
			                animation: {
			                    onComplete: function () {
			                        if (!rectangleSet) {
			                            var scale = window.devicePixelRatio;                       

			                            var sourceCanvas = constraintChart.canvas;
			                            var copyWidth = constraintChart.scales['y-axis-0'].width - 10;
			                            var copyHeight = constraintChart.scales['y-axis-0'].height + constraintChart.scales['y-axis-0'].top + 10;

			                            var targetCtx = document.getElementById("axis-Test").getContext("2d");

			                            targetCtx.scale(scale, scale);
			                            targetCtx.canvas.width = copyWidth * scale;
			                            targetCtx.canvas.height = copyHeight * scale;

			                            targetCtx.canvas.style.width = `${copyWidth}px`;
			                            targetCtx.canvas.style.height = `${copyHeight}px`;
			                            targetCtx.drawImage(sourceCanvas, 0, 0, copyWidth * scale, copyHeight * scale, 0, 0, copyWidth * scale, copyHeight * scale);

			                            var sourceCtx = sourceCanvas.getContext('2d');

			                            // Normalize coordinate system to use
										// css pixels.

			                            sourceCtx.clearRect(0, 0, copyWidth * scale, copyHeight * scale);
			                            rectangleSet = true;
			                        }
			                    },
			                    onProgress: function () {
			                        if (rectangleSet === true) {
			                            var copyWidth = constraintChart.scales['y-axis-0'].width;
			                            var copyHeight = constraintChart.scales['y-axis-0'].height + constraintChart.scales['y-axis-0'].top + 10;

			                            var sourceCtx = constraintChart.canvas.getContext('2d');
			                            sourceCtx.clearRect(0, 0, copyWidth, copyHeight);
			                        }
			                    }
			                }
			            }
			        });
			        addData(zoneList.length-1, constraintChart);
				// change the color of the bar
				 var dataset = constraintChart.data.datasets[0];
				    for(var i=0;i<dataset.data.length;i++){
				       var color="green";
				       // You can check for bars[i].value and put your
						// conditions here
					   if(dataset.data[i] <=1){
				       	dataset.backgroundColor[i] = "green";
				       }
					   else if(dataset.data[i]<=2){
				       	dataset.backgroundColor[i] = "yellow"
				       }
					   else
						   dataset.backgroundColor[i] = "red"
				    }
				    constraintChart.update();
	}
	
	$scope.loadCurrentInventorychart = function(){
		 $scope.zoneList = [];
		 $scope.zoneDollarData=[];
		 $scope.exportArrayData = [];
		 $scope.chartHeaderTitle = ""
		 if($scope.startdate == $scope.enddate) 
				$scope.chartHeaderTitle = "Current Inventory($)" + " on " +moment($scope.startdate,"YYYY-MMM-DD").format("MMM DD, YYYY");
				else
				 $scope.chartHeaderTitle= "Current Inventory($)" + " from "+$scope.dateRange;
		
		var ticksStyle = {
				fontColor: '#495057',
				fontStyle: 'bold'
			}
			var mode = 'index'
			var intersect = true	
			
			for (var i = 0; i < $scope.dataList.length; i++) {
				
	   				var dateObj = $scope.dataList[i];	
					if (dateObj != undefined && dateObj != null) {						
						
							var workStationName=dateObj.workStationName;
							var totalInventoryAmount=dateObj.totalInventoryAmount;						
								
								 $scope.zoneList.push(workStationName);						
								 $scope.zoneDollarData.push(totalInventoryAmount);
								 
									$scope.exportArrayData.push({					
										'WorkStation Name':workStationName,											
										'Current Inventory $': totalInventoryAmount											
									});						
				}		
			
	   	}

		$scope.totalInventryValue = 0;
		for(var i = 0; i < $scope.zoneDollarData.length; i++)
		{
			$scope.totalInventryValue = $scope.totalInventryValue + parseInt($scope.zoneDollarData[i]);
		}	
		
		$scope.totalInventry = "Total Inventory : " + "$"+ $scope.totalInventryValue.toString().replace(/\B(?<!\.\d*)(?=(\d{3})+(?!\d))/g, ",");
			
		$('#idChartAgingCanvas').remove();
		   $('#chartviewAgingchart').append('<canvas id="idChartAgingCanvas"  height="550" ></canvas>');
			  var $inventoryChart = $('#idChartAgingCanvas')
			  // eslint-disable-next-line no-unused-vars
			  
			  // eslint-disable-next-line no-unused-vars
			  var dollarChart = new Chart($inventoryChart, {
			    type: 'bar',
			    data: {
			      labels: $scope.zoneList,
			      datasets: [
			        {
						
			          backgroundColor: '#007bff',
			          borderColor: '#007bff',
			          data: $scope.zoneDollarData
			        }
			      ]
			    },
			    options: {
			      maintainAspectRatio: false,
				  responsive: true,
			      tooltips: {
			        mode: mode,
			        intersect: intersect
			      },
			      hover: {
			        mode: mode,
			        intersect: intersect
			      },
				  "animation": {
			      "duration": 1
			    },
			      legend: {
			        display: false,
			     },
			      scales: {
			        yAxes: [{
			          // display: false,
			          gridLines: {
			            display: true,
			            lineWidth: '4px',
			            color: 'rgba(0, 0, 0, .2)',
			            zeroLineColor: 'transparent'
			          },
					   scaleLabel: {
			        display: false,
			        labelString: 'Days'
			      },
			          ticks: $.extend({
			            beginAtZero: true,

			            // Include a dollar sign in the ticks
			            callback: function (value) {
			              // if (value >= 1000) {
			              // value /= 1000
			              // value += 'k'
			              // }
			            if (parseInt(value) >= 1000) {
			                return '$' + value.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
			            } else {
			                return '$' + value;
			            }
			            // return '$' + value
			              // /return value
			            }
			          }, ticksStyle)
			        }],
			        xAxes: [{
			          display: true,
					  scaleLabel: {
			        display: true,
			        labelString: 'Work Station'
			      },
			          gridLines: {
			            display: true
			          },
			          ticks: ticksStyle
			        }]
			      }
			    }
			  })
		
	}
	$scope.loadAgingChart = function(){	
		 // to check
		$scope.dateRange =  moment(new Date()).format("MMM DD, YYYY");
		
			var ticksStyle = {
				fontColor: '#495057',
				fontStyle: 'bold'
			}
			var mode = 'index'
			var intersect = true;
			
			$scope.agingZoneList = [];
			$scope.Aging1DayData = [];
			$scope.Aging2DaysData = [];
			$scope.AgingMoreThan2DaysData = [];
			$scope.exportArrayData = [];
			if($scope.workStation == "All"){
				for(var i=0; i<$scope.dataList.length;i++){
					 var agingZoneListVariable = $scope.dataList[i].workStationName;
					 var Aging1DayDataVariable = $scope.dataList[i].today;
					 var Aging2DaysDataVariable = $scope.dataList[i].last2Days;	
					 var AgingMoreThan2DaysDataVariable = $scope.dataList[i].greaterThan2Days;	
					 
					 $scope.agingZoneList.push(agingZoneListVariable);						
					 $scope.Aging1DayData.push(Aging1DayDataVariable);
					 $scope.Aging2DaysData.push(Aging2DaysDataVariable);
					 $scope.AgingMoreThan2DaysData.push(AgingMoreThan2DaysDataVariable);

						$scope.exportArrayData.push({					
							'Work Station':agingZoneListVariable,											
							'Today':Aging1DayDataVariable,
							'Last Two Days':Aging2DaysDataVariable,
							'More than Two Days':AgingMoreThan2DaysDataVariable 
						});
		}			
			}else{
				for(var i=0; i<$scope.dataList.length;i++){
					if($scope.workStation == $scope.dataList[i].workStationid){
						var workStationOBJ = {};
						workStationOBJ = $scope.dataList[i];
						 var agingZoneListVariable = workStationOBJ.workStationName;
						 var Aging1DayDataVariable = workStationOBJ.today;
						 var Aging2DaysDataVariable = workStationOBJ.last2Days;	
						 var AgingMoreThan2DaysDataVariable = workStationOBJ.greaterThan2Days;	
						 
						 $scope.agingZoneList.push(agingZoneListVariable);						
						 $scope.Aging1DayData.push(Aging1DayDataVariable);
						 $scope.Aging2DaysData.push(Aging2DaysDataVariable);
						 $scope.AgingMoreThan2DaysData.push(AgingMoreThan2DaysDataVariable);

							$scope.exportArrayData.push({					
								'Work Station':agingZoneListVariable,											
								'Today':Aging1DayDataVariable,
								'Last Two Days':Aging2DaysDataVariable,
								'More than Two Days':AgingMoreThan2DaysDataVariable 
							});
						
					}
					
				}
			}
			
			$scope.chartHeaderTitle=""
				$scope.chartHeaderTitle = 'Aging Chart'
					$('#idChartAgingCanvas').remove();
				   $('#chartviewAgingchart').append('<canvas id="idChartAgingCanvas"  height="550" ></canvas>');
					
				  var $AgingChartInHtml = $('#idChartAgingCanvas')				 
				  var AgingChart = new Chart($AgingChartInHtml, {
				    type: 'bar',
				    data: {
				      labels: $scope.agingZoneList,
				      datasets: [
						        {
									label: '> 2 Days',
						          backgroundColor: '#6958db',
						          borderColor: '#6958db',
						          data: $scope.AgingMoreThan2DaysData
						        },
						        {
									label: '2 Days',
						          backgroundColor: 'green',
						          borderColor: 'green',
						          data: $scope.Aging2DaysData
						        },
						        {
									label: 'Today',
						          backgroundColor: '#007bff',
						          borderColor: '#007bff',
						          data: $scope.Aging1DayData
						        }
						      ]
				    },
				    options: {
				      maintainAspectRatio: false,
					  responsive: true,
				      tooltips: {
				        mode: mode,
				        intersect: intersect
				      },
				      hover: {
				        mode: mode,
				        intersect: intersect
				      },
				      legend: {
				        display: true,
						position: 'top'
				     },
				      scales: {
				        yAxes: [{
				          // display: false,
				          gridLines: {
				            display: true,
				            lineWidth: '4px',
				            color: 'rgba(0, 0, 0, .2)',
				            zeroLineColor: 'transparent'
				          },
						   scaleLabel: {
				        display: true,
				        labelString: '# Of Work Order'
				      },
				          ticks: $.extend({
				            beginAtZero: true,

				            // Include a dollar sign in the ticks
				            callback: function (value) {
				              // if (value >= 1000) {
				              // value /= 1000
				              // value += 'k'
				              // }

				              return value
				              // /return value
				            }
				          }, ticksStyle)
				        }],
				        xAxes: [{
				          display: true,
						  scaleLabel: {
				        display: true,
				        labelString: 'Work Station'
				      },
				          gridLines: {
				            display: true
				          },
				          ticks: ticksStyle
				        }]
				      }
				    }
				  })  
			
		  
				
		    	var dataSortIndex = 0;
		    	var otherIndex1,otherIndex2;
		    	if ($scope.varComboTypeofdaysAging == 1) {
		    		
		    		dataSortIndex = 2;
		    		otherIndex1=0;
		    		otherIndex2=1;
		    		
		    	}else if ($scope.varComboTypeofdaysAging == 2){
		    		
		    		dataSortIndex = 1;
		    		otherIndex1=0;
		    		otherIndex2=2;
		    	}else {
		    		dataSortIndex = 0
		    		otherIndex1=1;
		    		otherIndex2=2;
		    		
		    	}
		    	
		    	
		    	// Sorting data and label based on sorting type
		    	var dataset = AgingChart.data.datasets[dataSortIndex];
		    	var dataset1 = AgingChart.data.datasets[otherIndex1];
		    	var dataset2 = AgingChart.data.datasets[otherIndex2];
		    	for(var i = 0; i < dataset.data.length; i++){
		    	     
		    		   // Last i elements are already in place
		    		   for(var j = 0; j < ( dataset.data.length - i -1 ); j++){
		    		       
		    		     // Checking if the item at present iteration
		    		     // is greater than the next iteration
		    		     if(parseInt(dataset.data[j]) < parseInt(dataset.data[j+1])){
		    		         
		    		       // If the condition is true then swap them
		    		       var temp = dataset.data[j];
		    		       var tempOtherValue1 = dataset1.data[j];
		    		       var tempOtherValue2 = dataset2.data[j];
		    		       var tmpLabel = AgingChart.data.labels[j];
		    		       
		    		       dataset.data[j] = dataset.data[j + 1];
		    		       dataset1.data[j] = dataset1.data[j + 1];
		    		       dataset2.data[j] = dataset2.data[j + 1];
		    		       
		    		       AgingChart.data.labels[j]=AgingChart.data.labels[j+1]
		    		       
		    		       dataset.data[j+1] = temp;
		    		       dataset1.data[j+1] = tempOtherValue1;
		    		       dataset2.data[j+1] = tempOtherValue2;

		    		       AgingChart.data.labels[j+1] = tmpLabel;
		    		     }
		    		   }
		    		 }
		    	AgingChart.update(); // update the chart
		    
	
	}

$scope.viewOption = function(){
	$scope.expandButton = true;
	// map.closePopup();
	// $scope.iscolumnchartselected=false;
		
	 if($scope.viewValue=="Column Chart"){
			// $scope.iscolumnchartselected=true;
		 $scope.mapView = false;
			if($scope.dataList!=undefined && $scope.dataList.length!=0 ){
			if($scope.isPeopleTimeAnalyticsClicked){							
					$scope.norecords=false;						
					$scope.mapView = false;
		    		$scope.chartViewShow = true;	 
		    		$scope.peopleTypeList = false;
		    		$scope.exportXLS = true;
		    		$scope.exportPDF = true;					
		    		if($scope.subtype == "Employee"){
		    			$scope.loadPeopleTimeAnalyticsColumnchart();	
		    		}else if($scope.subtype == "Visitor" || 
		    				$scope.subtype == "Vendor" || //11Feb22
		    					$scope.subtype == "Contractor" ){
		    			$scope.loadVisitorTimeAnalyticsColumnchart();
		    		}
				}					
				else if($scope.isAssetTimeAnalyticsClicked){	
					$scope.norecords=false;	
					
					$scope.mapView = false;		    		
		    		$scope.chartViewShow = true;	
		    		$scope.peopleTypeList = false;
		    		$scope.exportXLS = true;
		    		$scope.exportPDF = true;
		    		$scope.loadAssetTimeAnalyticsColumnchart1();		    		
			} 
			}else{
				// to show no data found
				$scope.chartHeaderTitle = ""
				$scope.norecords = true;
				$scope.mapView = false;
	    		$scope.chartViewShow = false;	 
	    		$scope.peopleTypeList = false;
	    		$scope.exportXLS = false;
	    		$scope.exportPDF = false;	
	    		$scope.expandButton = false;	
	    		
			}
				
		}else if($scope.isWorkStationFlowClicked == true){
			if($scope.dataList!=undefined && $scope.dataList.length!=0 ){
				
				$scope.norecords=false;					
				$scope.mapView = false;
	    		$scope.chartViewShow = false;
	    		$scope.exportXLS = true;
	    		$scope.exportPDF = true;					
	    		
	    		$scope.loadWorkStationFlowAnalysis();
			}
		}else if($scope.isConstraintClicked == true){
			if($scope.dataList!=undefined && $scope.dataList.length!=0 ){
				
				$scope.norecords=false;					
				$scope.mapView = false;
	    		$scope.chartViewShow = true;
	    		$scope.exportXLS = true;
	    		$scope.exportPDF = true;					
	    		
	    		$scope.loadConstraintAnalysis();
			}
		}else if($scope.isAgingClicked == true){
			if($scope.dataList!=undefined && $scope.dataList.length!=0 ){
				
				$scope.norecords=false;					
				$scope.mapView = false;
	    		$scope.chartViewShow = false;
	    		$scope.exportXLS = true;
	    		$scope.exportPDF = true;					
	    		
	    			$scope.loadAgingChart();
			}
		}
		else if($scope.isCurrentInventoryClicked == true){

			if($scope.dataList!=undefined && $scope.dataList.length!=0 ){
				
				$scope.norecords=false;					
				$scope.mapView = false;
	    		$scope.chartViewShow = false;
	    		$scope.exportXLS = true;
	    		$scope.exportPDF = true;					
	    		$scope.idChartAgingCanvas  = true;	
	    		$scope.loadCurrentInventorychart();
			}
		
		}
		else if($scope.viewValue=="Line Chart"){
			if($scope.dataList!=undefined && $scope.dataList.length!=0 ){
				if($scope.isPlantArrivalRateClicked){
					$scope.norecords=false;					
					$scope.mapView = false;
		    		$scope.chartViewShow = true;
		    		$scope.exportXLS = true;
		    		$scope.exportPDF = true;					
		    		$scope.idChartAgingCanvas  = false;		    			
		    		$scope.loadPlantTrafficChart();	
				}else if($scope.isWorkOrderFlowClicked){
					$scope.isDateSelected =false;
					$scope.searchButton = false;
					$scope.norecords=false;					
					$scope.mapView = false;
		    		$scope.chartViewShow = false;
		    		$('#idChartAgingCanvas').remove();
					$('#chartviewAgingchart').append('<canvas id="idChartAgingCanvas"  height="0" ></canvas>');
		    		$scope.exportXLS = true;
		    		$scope.exportPDF = true;					
		    		$scope.idChartAgingCanvas  = false;		    			
		    		$scope.loadWorkOrderChart();	
				}
				
			}	
		}else if($scope.viewValue=="Plant Occupancy"){
			if($scope.dataList!=undefined && $scope.dataList.length!=0 ){
				
				$scope.norecords=false;					
				$scope.mapView = false;
	    		$scope.chartViewShow = true;
	    		$scope.exportXLS = true;
	    		$scope.exportPDF = true;					
	    		$scope.idChartAgingCanvas  = false;		    			
	    		$scope.loadPlantOccupancyChart();
			}	
		}else if($scope.viewValue == "Floor Occupancy"){
			$scope.please_select_text = "";		
		$scope.pleaseSelect = false;
		$scope.mapView = true; 
		$scope.chartViewShow = false;
		$scope.norecords = false;
		$scope.exportXLS = false;
		$scope.exportPDF = false;
		$scope.expandButton = false;	
		$scope.getCampusDetails();
		//loadMap();
	}else if($scope.viewValue=="Table"){
			$('#idChartAgingCanvas').remove();
			$('#chartviewAgingchart').append('<canvas id="idChartAgingCanvas"  height="0" ></canvas>');
			if($scope.dataList!=undefined && $scope.dataList.length!=0 ){				
				$scope.norecords=false;					
				$scope.mapView = false;
	    		$scope.chartViewShow = false; 	    		
	    		$scope.exportXLS = true;
	    		$scope.exportPDF = false;					
	    		$scope.idChartAgingCanvas  = false;		    			
	    		$scope.loadWorkOrderTable();
			}				
		}else if($scope.viewValue=="Map"){
			$scope.please_select_text = "";			
			$scope.pleaseSelect = false;
			$scope.mapView = true; 
			$scope.chartViewShow = false;
			$scope.norecords = false;
			$scope.exportXLS = false;
    		$scope.exportPDF = false;
    		$scope.expandButton = false;   
    		if($scope.isPeopleTimeAnalyticsClicked == true && $scope.mapEmpIDVariable == 0){
    			$scope.emp = $scope.staffDetails[0].empId; 
    		}    		
    		$scope.getCampusDetails();
    		//loadMap();
		}
		
	}

	// Employee drop down API call for combo box People ID in Assign screen for
	// Employee
	
	$scope.getEmpData = function() {				
		
		if (!$rootScope.checkNetconnection()) { 				
		 	$rootScope.alertDialogChecknet();
			return; 
			}
		$rootScope.showloading('#loadingBar');
		$http({
			method : 'GET',
			url : './staff',
			
		})
				.then(
						function success(response) {
							$rootScope.hideloading('#loadingBar');
							if (response != null
									&& response.data != null
									&& response.data.length != 0
									&& response.data != "BAD_REQUEST"
									&& response.data.data != undefined
									&& response.data.data.length != 0) {

								$scope.staffDetails = [];

								for (var i = 0; i < response.data.data.length; i++) {
									
									if (response.data.data[i].active == true) {
									
										$scope.staffDetails.push( {
											'empId': response.data.data[i].empId ,
											'primaryZone': response.data.data[i].location1,
											'secondaryZone': response.data.data[i].location2,
											'thirdZone': response.data.data[i].location3,
											'createdDate': response.data.data[i].createdDate,
											'modifiedDate': response.data.data[i].modifiedDate,
											'active': response.data.data[i].active,
									
										});
									}
								}
								
								$scope.staffDetails.sort(GetSortOrder("empId"));
								$scope.emp = "All";
							} else {
								$scope.peopleIdTable
								.push("No Employee Data");
							}
						},
						
						function error(response) {						
								$rootScope.hideloading('#loadingBar');						
								$rootScope.fnHttpError(response);
					    });
	}

	$scope.getOtherUserData = function() {
		if ($scope.checkNetconnection() == false) { 	         
	       // $rootScope.hideloading('#loadingBar')
	           alert("Please check the Internet Connection");
	            return;
	        } 
		$rootScope.showloading('#loadingBar')
		$http({
			method : 'GET',
			url : './allUsers',
			
		})
		.then(
				function success(response) {
					$rootScope.hideloading('#loadingBar')
					if (response != null
							&& response.data != null
							&& response.data != "BAD_REQUEST") {
						$scope.visitorList.length = 0;
						$scope.vendorList.length = 0;
						$scope.contractorList.length = 0;
						
						for (var i = 0; i < response.data.data.others.length; i++) {
							if(response.data.data.others[i].type == "Visitor"){
								$scope.visitorList.push(response.data.data.others[i]);							
							}else if(response.data.data.others[i].type == "Vendor"){
								$scope.vendorList.push(response.data.data.others[i]);							
							}else if(response.data.data.others[i].type == "Contractor"){
								$scope.contractorList.push(response.data.data.others[i]);							
							}
						}
						$scope.visitorList.sort(GetSortOrder("visitorId"));
						$scope.vendorList.sort(GetSortOrder("visitorId"));
						$scope.contractorList.sort(GetSortOrder("visitorId"));
						
						// $scope.visitor = $scope.visitorList[0].visitorId;
						// $scope.vendor = $scope.vendorList[0].visitorId;
						// $scope.contractor =
						// $scope.contractorList[0].visitorId;
						
					} else {
						$scope.noList = true;
						
					}
				},function error(response) {
					$rootScope.hideloading('#loadingBar')
					
		    });
	};
	
	$scope.userTypeChanged = function() {		
		if($scope.subtype == "Visitor"){
			if($scope.visitorList != undefined && $scope.visitorList.length != 0){ 
			$scope.visitor = $scope.visitorList[0].visitorId.toString();	
			$scope.visitorNAme = $scope.visitorList[0].firstName+ " " + $scope.visitorList[0].lastName}
		}else if($scope.subtype == "Vendor"){
			if($scope.vendorList != undefined && $scope.vendorList.length != 0) {
			$scope.vendor = $scope.vendorList[0].visitorId.toString();
			$scope.visitorNAme = $scope.vendorList[0].firstName+ " " + $scope.vendorList[0].lastName}
		}else if($scope.subtype == "Contractor"){
			if($scope.contractorList != undefined && $scope.contractorList.length != 0) {
			$scope.contractor = $scope.contractorList[0].visitorId.toString();	
			$scope.visitorNAme = $scope.contractorList[0].firstName+ " " + $scope.contractorList[0].lastName}
		}else if($scope.subtype == "Employee")
			$scope.emp = "All";
	}	

	
	// to call analytics api
	$scope.getAPIDetails = function(){	
		if (!$rootScope.checkNetconnection()) { 				
		 	$rootScope.alertDialogChecknet();
			return; 
			}
		// to show please select text
		$scope.chartHeaderTitle = ""
		$scope.pleaseSelect = false;	
		var url;
		var data={}
		var method = "";
		
		if($scope.isPeopleTimeAnalyticsClicked ==true){	
			method = 'POST';
			if($scope.subtype == "Employee")				
				url = './peopleDwellTime';
			else if($scope.subtype == "Visitor"){
				if ($scope.visitor ==undefined || $scope.visitor ==""){
					$.alert({
	                    title: $rootScope.MSGBOX_TITLE,
	                    content: "No Visitor is available to generate report",
	                    icon: 'fa fa-info-circle',
	                     });
					return;
					}
					
				
			
				url = './visitorDwellTime';
			}
			else if($scope.subtype == "Vendor") {
				if ($scope.vendor ==undefined ||  $scope.vendor ==""){
					$.alert({
	                    title: $rootScope.MSGBOX_TITLE,
	                    content: "No Vendor is available to generate report",
	                    icon: 'fa fa-info-circle',
	                     });
					return;
					}
				url = './visitorDwellTime';
			}
			else if($scope.subtype == "Contractor") {
				if ($scope.contractor == undefined || $scope.contractor ==""){
					$.alert({
	                    title: $rootScope.MSGBOX_TITLE,
	                    content: "No Contractor is available to generate report",
	                    icon: 'fa fa-info-circle',
	                     });
					return;
					}
				
				url = './visitorDwellTime';
			}
			data = {
			    "userId": "all",
			    "startDate": $scope.startdate,
			    "endDate": $scope.enddate
			}
		
		
		}if($scope.isPlantArrivalRateClicked ==true){			
			
			method = 'POST';
			url = './ZoneFootFall';
			data = {
			    "zoneId": $scope.zone,
			    "startDate": $scope.startdate,
			    "endDate": $scope.enddate
			}		
		
		}if($scope.isPlantFloorAnalysisClicked ==true){			
			
			method = 'POST';
			url = './attendance';
			data = {			   
			    "startDate":  moment($('#idPFAEditStartDatePicker').val()).format("YYYY-MMM-DD")
			}		
		
		}else if($scope.isAssetTimeAnalyticsClicked == true){
			method = 'POST';
			url = './assetDwellTime';
			
			$scope.assetIDForData = "";
			if($scope.assetProfileType == "Chrono")
				$scope.assetIDForData = $scope.chrono
			else if($scope.assetProfileType == "NonChrono")
				$scope.assetIDForData = $scope.nonChrono
				
			data = {
			    "assetId": $scope.assetIDForData,
			    "startDate": $scope.startdate,
			    "endDate": $scope.enddate,
			    "assetProfile": $scope.assetProfileType			  
			}
			
		}	else if($scope.isWorkOrderFlowClicked  == true){
			/*
			 * $scope.chartViewShow = false $('#idChartAgingCanvas').remove();
			 * $('#chartviewAgingchart').append('<canvas
			 * id="idChartAgingCanvas" height="0" ></canvas>');
			 */
			if($scope.viewValue=="Table"){				
			method = 'POST';
			url = './WorkOrderTimeAnalytics';	
			data = {
					 "startDate":  $scope.startdate,
					  "endDate": $scope.enddate
				}
			}else if($scope.viewValue=="Line Chart"){
				method = 'POST';
				url = './WorkStationChartAnalytics';	
				data = {
						"WorkStationId": "all",
						"startDate": $scope.startdate,
						"endDate": $scope.enddate
					}
			}
		}else if($scope.isWorkStationFlowClicked  == true){
			// to remove canvas when loaded initially to show please select text
			$('#idChartAgingCanvas').remove();
			$('#chartviewAgingchart').append('<canvas id="idChartAgingCanvas"  height="0" ></canvas>');
			method = 'POST';
			url = './WorkStationFlowAnalytics';	
			data = {
					   "WorkStationId": "all",
					   "startDate":  $scope.startdate,
					   "endDate": $scope.enddate		  
				}
		}	else if($scope.isConstraintClicked  == true){
			method = 'POST';
			url = './WorkStationConstraintAnalytics';	
			data = {
					   "WorkStationId": "all",
					   "startDate":  $scope.startdate,
					   "endDate": $scope.enddate		  
				}
		}	
		else if($scope.isCurrentInventoryClicked == true){
			method = 'POST';
			url = './WorkStationInventory';
			data = {
					   "WorkStationId": "all",
					   "startDate":  $scope.startdate,
					   "endDate": $scope.enddate		  
				}
		}
		else if($scope.isAgingClicked == true){
			method = 'POST';
			url = './WorkStationAgingAnalytics';
			
		}
		$rootScope.showloading('#loadingBar')
		$http({
			method : method,
			url : url,
			data:data
			
		}).then(
				
				function success(response) {	
					$rootScope.hideloading('#loadingBar');
					if (response.data.status == "error") {
						$scope.chartViewShow = false;
						$('#idChartAgingCanvas').remove();
						$('#chartviewAgingchart').append('<canvas id="idChartAgingCanvas"  height="0" ></canvas>');
						$scope.exportXLS = false;
						$scope.exportPDF = false;
						$scope.expandButton = false;
						$scope.mapView = false;
						// to show no data found
						$scope.chartHeaderTitle = ""
						$scope.norecords = true;	
						
						if (response.data.message == "Session timed-out or Invalid") {							
							$.alert({
			                    title: $rootScope.MSGBOX_TITLE,
			                    content: response.data.message + ". Please logout and login again",
			                    icon: 'fa fa-info-circle',
			                     });
							}
							else{
								$rootScope.hideloading('#loadingBar');
							 $.alert({
				                    title: $rootScope.MSGBOX_TITLE,
				                    content: response.data.message,
				                    icon: 'fa fa-info-circle',
				                     });
							}						
						return;
					} 
					if (response != null && response.data != null
					&& response.data != "BAD_REQUEST") {
						
						$rootScope.hideloading('#loadingBar');
						
						$scope.chartViewShow = true;
						$scope.exportXLS = true;
						$scope.exportPDF = true;
						$scope.expandButton = true;
						$scope.mapView = false; 
						
						if($scope.isPeopleTimeAnalyticsClicked == true){
							$scope.dataList=[];	
							$scope.dataList= response.data;							
						}else if($scope.isPlantArrivalRateClicked == true){
							$scope.dataList=[];	
							$scope.dataList= response.data.footFall;							
						}else if($scope.isPlantFloorAnalysisClicked == true){
							$scope.dataList=[];									
							$scope.dataList = response.data.attendance;							
						}else if($scope.isAssetTimeAnalyticsClicked == true){
							$scope.dataList=[];							
							$scope.dataList= response.data;							
						}else if($scope.isWorkOrderFlowClicked == true){
							$scope.dataList=[];	
							if($scope.viewValue == "Table")
							$scope.dataList= response.data.workOrderList;	
							else if($scope.viewValue == "Line Chart")
							$scope.dataList= response.data.workStations;
						}else if($scope.isWorkStationFlowClicked == true){
							$scope.dataList=[];
							$scope.dataList= response.data.workStations;							
						}else if($scope.isConstraintClicked == true){
							$scope.dataList=[];							
							$scope.dataList= response.data.workStations;							
						}else if($scope.isCurrentInventoryClicked == true){
							$scope.dataList=[];							
							$scope.dataList= response.data.workStations;							
						}else if($scope.isAgingClicked == true){
							$scope.dataList=[];							
							$scope.dataList= response.data.workStations;							
						}
					}
					
						$scope.viewOption();
					},
				
				function error(response) {
						$rootScope.hideloading('#loadingBar')
						$scope.chartViewShow = true;
						$scope.mapView = false;											
						$rootScope.fnHttpError(response);
			    });
	   	 
	}


	$scope.getAllZones=function(){
		if (!$rootScope.checkNetconnection()) { 				
		 	$rootScope.alertDialogChecknet();
			return; 
			}
		$rootScope.showloading('#loadingBar');
		$http({
			method : 'GET',
			url : './zone',
			
		})
		.then(
				function success(response) {
					
					$rootScope.hideloading('#loadingBar');
					if (response != null
							&& response.data != null
							&& response.data != "BAD_REQUEST") { 

						$scope.zoneList = response.data.data;	
						$scope.zoneLists = response.data.data;	
						$scope.zoneList.sort(GetSortOrder("name"));
						$scope.zone = $scope.zoneList[0].id.toString();
						// $scope.zone=$scope.zoneList[0].id;
						
					 }else {
						
						$scope.noList = true;
					}
				},
				
				function error(response) {						
						$rootScope.hideloading('#loadingBar');						
						$rootScope.fnHttpError(response);
			    });
	}

	
	$scope.getassetChronoData=function(){
		if (!$rootScope.checkNetconnection()) { 				
		 // $rootScope.alertDialogChecknet();
			return; 
			}
		$scope.chronoList = [];
		$rootScope.showloading('#loadingBar');
		$http({
			method : 'GET',
			url : './assetChrono',

		})
		.then(
				function success(response) {
					
					$rootScope.hideloading('#loadingBar');
					if (response != null
							&& response.data != null
							&& response.data != "BAD_REQUEST") { 

						$scope.chronoList = response.data.data;	
						$scope.chronoList.sort(GetSortOrder("assetId"));
						$scope.chrono=$scope.chronoList[0].assetId;
					 }else {
						
						$scope.noList = true;
					}
				}, function error(response) {
					  $rootScope.hideloading('#loadingBar');
					   }
					 );
	}
	
	$scope.getassetNonChronoData=function(){
		if (!$rootScope.checkNetconnection()) { 				
		 	$rootScope.alertDialogChecknet();
			return; 
			}
		$scope.nonChronoList = [];
		$rootScope.showloading('#loadingBar');
		$http({
			method : 'GET',
			url : './assetNonChrono',

		})
		.then(
				function success(response) {
					
					$rootScope.hideloading('#loadingBar');
					if (response != null
							&& response.data != null
							&& response.data != "BAD_REQUEST") { 

						$scope.nonChronoList = response.data.data;	
						$scope.nonChronoList.sort(GetSortOrder("assetId"));
						$scope.nonChrono=$scope.nonChronoList[0].assetId;
					 }else {
						
						$scope.noList = true;
					}
				},
				
				function error(response) {						
						$rootScope.hideloading('#loadingBar');						
						$rootScope.fnHttpError(response);
			    });
	}
	$scope.getWorkStationData=function(){
		if (!$rootScope.checkNetconnection()) { 				
		 	$rootScope.alertDialogChecknet();
			return; 
			}
		$scope.workStationList = [];
		$rootScope.showloading('#loadingBar');
		$http({
			method : 'GET',
			url : './workStation',

		})
		.then(
				function success(response) {
					
					$rootScope.hideloading('#loadingBar');
					if (response != null
							&& response.data != null
							&& response.data != "BAD_REQUEST") { 

						$scope.workStationList = response.data.data;	
						$scope.workStationList.sort(GetSortOrder("stationId"));
						$scope.workStation = "All";
					 }else {
						
						$scope.noList = true;
					}
				},
				
				function error(response) {						
						$rootScope.hideloading('#loadingBar');						
						$rootScope.fnHttpError(response);
			    });
	}
	
	$scope.getData = function() {
		
		if($scope.isPeopleTimeAnalyticsClicked == true){
	 			$scope.getEmpData();
	 			$scope.getOtherUserData();
		}
 		if($scope.isPeopleTimeAnalyticsClicked == true || $scope.isPlantArrivalRateClicked == true ||  $scope.isPlantFloorAnalysisClicked == true){
 				$scope.getAllZones();
 		}			
		if($scope.isAssetTimeAnalyticsClicked == true){
				$scope.getassetChronoData();
				$scope.getassetNonChronoData();	
		}
		if($scope.isAgingClicked == true || $scope.isWorkStationFlowClicked == true){
				$scope.getWorkStationData();
		}
		
	};

	$scope.getData();
	
	function GetSortOrder(prop) {    
		return function(a, b) {    
			if(a[prop]!=null&&b[prop]!=null){
				// if (a[prop].toLowerCase() > b[prop].toLowerCase()) {
				if (a[prop] > b[prop]) {   
					return 1;    
				// } else if (a[prop].toLowerCase() < b[prop].toLowerCase()) {
				} else if (a[prop] < b[prop]) {   
					return -1;    
				}    
			}
			return 0;    
		}    
	}  
	

	
	
	
$scope.getZoneFinalList = function(keyValue, startdate, enddate) {

		$scope.DAY_ARRAY = [];
		$scope.FINAL_DAY_ARRAY = [];
		
		$scope.zoneStatusFinalList = [];	
		
		function formatDate(d) {
			var month = '' + (d.getMonth() + 1), day = ''
					+ d.getDate(), year = d.getFullYear();

			if (month.length < 2)
				month = '0' + month;
			if (day.length < 2)
				day = '0' + day;

			return [ year, month, day ].join('-');
		}
		
		function convert(str) {
			var date = new Date(str)
            var now_utc = new Date(date.toUTCString().slice(0, -4));
            var mnth = ("0" + (now_utc.getMonth() + 1)).slice(-2);
            var day = ("0" + now_utc.getDate()).slice(-2);
            return [now_utc.getFullYear(), mnth, day].join("-");
		}		
		
		 function convertTimestamp(timestamp, convertint = 1) {
				var fromdate;
				if (convertint === 1)
					fromdate = parseInt(timestamp, 10);
				else fromdate = timestamp;

				var d = new Date(0);				
				d.setUTCSeconds(fromdate); // Convert the passed timestamp to
											// milliseconds
				var now_utc = new Date(d.toUTCString().slice(0, -4));
				var mnth = ("0" + (now_utc.getMonth() + 1)).slice(-2);
				var day = ("0" + now_utc.getDate()).slice(-2);
				return [now_utc.getFullYear(), mnth, day].join("-");
				
			}
		

        var getDatesBetweenDates = (startDate, endDate) => {
            let dates = []
                // to avoid modifying the original date
            const theDate = new Date(startDate);
            const theendDate = new Date(endDate); 

            while (theDate < theendDate) {
                dates = [...dates, new Date(theDate)]
                theDate.setDate(theDate.getDate() + 1)
            }
            dates = [...dates, endDate]
            return dates
        }
		
       
		for (var i = 0; i < $scope.zoneList.length; i++) {
			if($scope.zoneList[i].id == keyValue){
			
			var cDate = convertTimestamp($scope.zoneList[i].created_time);
			var mDate = convertTimestamp($scope.zoneList[i].modified_time);
			var cStatus = $scope.zoneList[i].zoneStatus;
			
									
			if (cStatus == "Active") {				
				if (new Date(cDate).toISOString().slice(0, 10) <= new Date(enddate).toISOString().slice(0, 10)) {
				
					$scope.DAY_ARRAY = getDatesBetweenDates(cDate,enddate);
					
				}

			}
			else if (cStatus == "Inactive") {
				if ((new Date(startdate).toISOString().slice(0, 10) <= new Date(cDate).toISOString().slice(0, 10)) && 
						(new Date(enddate).toISOString().slice(0, 10) <= new Date(cDate).toISOString().slice(0, 10))) {			
										

					if (new Date(cDate).toISOString().slice(0, 10) == new Date(enddate).toISOString().slice(0, 10)) {
						
						$scope.DAY_ARRAY = getDatesBetweenDates(cDate, mDate);
					
					}
				} else {
				
					if (new Date(mDate).toISOString().slice(0, 10) >= new Date(startdate).toISOString().slice(0, 10)) {						
						$scope.DAY_ARRAY = getDatesBetweenDates(cDate, mDate);
						
					}

				}

			}
			
			for (var da = 0; da < $scope.DAY_ARRAY.length; da++) {

				$scope.FINAL_DAY_ARRAY[da] = convert($scope.DAY_ARRAY[da].toString());
				// $scope.FINAL_DAY_ARRAY[da] =$scope.DAY_ARRAY[da].toString();
				
			}
			
		}
		}
		
			
	}



	var reN = /[^0-9]/g;

	function sortAlphaNum(a1, b1) {

		var aA = a1.dWellTime;
		var bA =b1.dWellTime;
		if (aA === bA) {
			return aA === bA ? 0 : aA < bA ? 1 : -1;
		} else {
			return aA < bA ? 1 : -1;
		}
	}
	function formatDate(d) {
		var month = '' + (d.getMonth() + 1), day = ''
			+ d.getDate(), year = d.getFullYear();

		if (month.length < 2)
			month = '0' + month;
		if (day.length < 2)
			day = '0' + day;

		return [ year, month, day ].join('-');
	}

	 $scope.timearray=["12:15 AM", "12:30 AM", "12:45 AM", "01:00 AM", "01:15 AM", "01:30 AM", "01:45 AM","02:00 AM", "02:15 AM", "02:30 AM",
		 			   "02:45 AM", "03:00 AM", "03:15 AM", "03:30 AM", "03:45 AM", "04:00 AM", "04:15 AM", "04:30 AM","04:45 AM", "05:00 AM", 
		 			   "05:15 AM", "05:30 AM", "05:45 AM", "06:00 AM", "06:15 AM", "06:30 AM ", "06:45 AM", "07:00 AM", "07:15 AM","07:30 AM",
		 			   "07:45 AM", "08:00 AM", "08:15 AM", "08:30 AM", "08:45 AM", "09:00 AM", "09:15 AM", "09:30 AM", "09:45 AM", "10:00 AM",
		 			   "10:15 AM", "10:30 AM","10:45 AM", "11:00 AM", "11:15 AM", "11:30 AM", "11:45 AM", "12:00 PM", "12:15 PM", "12:30 PM", 
		 			   "12:45 PM", "01:00 PM", "01:15 PM","01:30 PM", "01:45 PM", "02:00 PM", "02:15 PM", "02:30 PM", "02:45 PM", "03:00 PM",
		 			   "03:15 PM", "03:30 PM", "03:45 PM", "04:00 PM", "04:15 PM","04:30 PM", "04:45 PM", "05:00 PM", "05:15 PM", "05:30 PM", 
		 			   "05:45 PM", "06:00 PM", "06:15 PM", "06:30 PM", "06:45 PM", "07:00 PM","07:15 PM", "07:30 PM", "07:45 PM", "08:00 PM",
		 			   "08:15 PM", "08:30 PM", "08:45 PM", "09:00 PM", "09:15 PM", "09:30 PM", "09:45 PM", "10:00 PM","10:15 PM", "10:30 PM",
		 			   "10:45 PM", "11:00 PM", "11:15 PM", "11:30 PM", "11:45 PM","12:00 AM"];
	
	
	/**
	 * Description sort the string based on number attached to it.
	 * 
	 * @function sortAlphaNum1
	 * @access private
	 * @since 1.0.0
	 * @static
	 * @param a1 -
	 *            string 1
	 * @param b1 -
	 *            string 2
	 * @returns 1 or -1
	 */

	function sortAlphaNum1(a1, b1) {

		var reA = /[^a-zA-Z]/g;
		var reN = /[^0-9]/g;

		var a = a1.uId;
		var b = b1.uId;
		var aA = a.replace(reA, "");
		var bA = b.replace(reA, "");
		if (aA === bA) {
			var aN = parseInt(a.replace(reN, ""), 10);
			var bN = parseInt(b.replace(reN, ""), 10);
			return aN === bN ? 0 : aN > bN ? 1 : -1;
		} else {
			return aA > bA ? 1 : -1;
		}
	}
	
	
	$scope.exportExcelData=function(){
		if ($scope.checkNetconnection() == false) { 
	         // alert("Please check the Internet Connection");
			$rootScope.alertDialogChecknet();
	            return;
	        }   
	   	 else{
		var export_date = new Date();
		var exportDD = export_date
				.getDate();
		var exportMM = export_date
				.getMonth() + 1;
		var exportYYYY = export_date
				.getFullYear();

		if (exportDD.length < 2) {
			exportDD = "0" + exportDD;
		}
		if (exportMM.length < 2) {
			exportMM = "0" + exportMM;
		} 
		
		var exportDate = moment(export_date).format("MMM DD, YYYY")
		
		var exportHour = export_date.getHours();
		var exportMinutes = export_date.getMinutes();
		if (exportMinutes < 10) {
			exportMinutes = "0"
					+ exportMinutes;
		}
		var exportAmPm = "AM";
		if (exportHour > 12) { 
			exportHour -= 12;
			exportAmPm = "PM";
		} else if (exportHour === 12) {
			exportAmPm = "PM";
		} else if (exportHour === 0) {
			exportHour += 12;
		}

		var exportTime1 = exportHour + ":"
				+ exportMinutes + " "
				+ exportAmPm;
		var headerArray=[];
		var detailarray=[];
		var descarray=[];
		 detailarray=['Report Generated by','Report Generated Date & Time','Report'];
		descarray=[$scope.user.userName, exportDate+ ", "+exportTime1, $scope.chartHeaderTitle];
		
		
		for(var i=0; i<detailarray.length; i++){
			headerArray.push( {
				'Details': detailarray[i],				
				'Description':descarray[i] ,				
			});		    
		}
		
		
		var export_file_name = $scope.filename	+ "_"+ exportDate + "_"	+ exportTime1 + ".xlsx";
		var ws = XLSX.utils.json_to_sheet($scope.exportArrayData);	
		var wscols = [
		    {wch:20},
		    {wch:10}, 
		    {wch:10},
		    {wch:10},
		    {wch:10},
		    {wch:10},
		    {wch:10},
		];
		var ws1 = XLSX.utils.json_to_sheet(headerArray);
		var wscols1 = [
		    {wch:30},
		    {wch:35},   
		];
		ws['!cols'] = wscols;
		ws1['!cols'] = wscols1;
		var wb = XLSX.utils.book_new();
		XLSX.utils.book_append_sheet(wb, ws, $scope.sheetname);		
		XLSX.utils.book_append_sheet(wb, ws1, "Export Details");		
		XLSX.writeFile(wb,export_file_name );	
		}
	
	}
	$scope.exportPDFData=function(){
		if ($scope.checkNetconnection() == false) { 
	        // alert("Please check the Internet Connection");
			$rootScope.alertDialogChecknet();
	            return;
	        }   
	   	 else{
		function getTarget(e) {
			  return e.target || e.srcElement;			  
			}
		var export_date = new Date();
		var exportDD = export_date
				.getDate();
		var exportMM = export_date
				.getMonth() + 1;
		var exportYYYY = export_date
				.getFullYear();

		if (exportDD.length < 2) {
			exportDD = "0" + exportDD;
		}
		if (exportMM.length < 2) {
			exportMM = "0" + exportMM;
		} 
		
		var exportDate = moment(export_date).format("MMM DD, YYYY")		
		var exportHour = export_date.getHours();
		var exportMinutes = export_date.getMinutes();
		if (exportMinutes < 10) {
			exportMinutes = "0"
					+ exportMinutes;
		}
		var exportAmPm = "AM";
		if (exportHour > 12) { 
			exportHour -= 12;
			exportAmPm = "PM";
		} else if (exportHour === 12) {
			exportAmPm = "PM";
		} else if (exportHour === 0) {
			exportHour += 12;
		}

		var exportTime1 = exportHour + ":" + exportMinutes + " " + exportAmPm;
		
		// to export as PDF
		var data = ''
			// var canvasId = ''
			if($scope.isAgingClicked == true){
				data = document.getElementById('chartviewAgingchart');	
				// canvasId = 'idChartDisplayWithWrapper';
			}else{
				data = document.getElementById('chartview');	
				// canvasId = 'idChartAgingCanvas';
			}
		
		
		html2canvas(data).then(canvas => {
			var imgWidth = 208;
			var imgHeight = canvas.height * imgWidth / canvas.width;
			const contentDataURL = canvas.toDataURL('image/png')
			let pdf = new jspdf('l', 'mm', 'a4');
			var position = 0;

			
			if($scope.isAgingClicked || $scope.isCurrentInventoryClicked || $scope.isWorkStationFlowClicked){ 
				domtoimage.toPng(document.getElementById('idChartAgingCanvas')).then(function (chartImage) {
					var img = new Image();				
					img.src = chartImage;
					pdf.setFontSize(12);
					pdf.text(110, 7, $scope.chartHeaderTitle);
					pdf.setLineWidth(0.1);
					pdf.line(0, 10, 800, 10);
					pdf.setFontSize(11);
					pdf.text(20, 15, "Date : " + $scope.dateRange);
					pdf.setFontSize(11);
					pdf.text(20, 20, "Report Generated by : " + $scope.user.userName);					
					pdf.setFontSize(11);
					pdf.text(20, 25, "Report Generated on : " + exportDate + ", "	+ exportTime1);				
					pdf.setLineWidth(0.1);	       
					pdf.line(0, 30, 800, 33);
					pdf.addImage(chartImage, 'PNG', 5, 31, 250, 130)
					pdf.save($scope.filename	+ "_"+ exportDate + " "	+ exportTime1 + ".pdf");
				});		
			}else if($scope.isWorkOrderFlowClicked && $scope.viewValue=='Line Chart'){ 
				domtoimage.toPng(document.getElementById('idchartviewforPDf')).then(function (chartImage) {
					var img = new Image();				
					img.src = chartImage;
					pdf.setFontSize(12);
					pdf.text(110, 7, $scope.chartHeaderTitle);
					pdf.setLineWidth(0.1);
					pdf.line(0, 10, 800, 10);
					pdf.setFontSize(11);
					pdf.text(20, 15, "Date : " + $scope.dateRange);
					pdf.setFontSize(11);
					pdf.text(20, 20, "Report Generated by : " + $scope.user.userName);					
					pdf.setFontSize(11);
					pdf.text(20, 25, "Report Generated on : " + exportDate + ", "	+ exportTime1);				
					pdf.setLineWidth(0.1);	       
					pdf.line(0, 30, 800, 33);
					pdf.addImage(chartImage, 'PNG', 5, 31, 250, 130)
					pdf.save($scope.filename	+ "_"+ exportDate + " "	+ exportTime1 + ".pdf");
				});		
			}						
			else{
				domtoimage.toPng(document.getElementById('idChartDisplayWithWrapper'))
				.then(function (chartImage) {
					var img = new Image();				
					img.src = chartImage;
					pdf.setFontSize(12);
					pdf.text(110, 7, $scope.chartHeaderTitle);
					pdf.setLineWidth(0.1);
					pdf.line(0, 10, 800, 10);
					pdf.setFontSize(11);
					pdf.text(20, 15, "Date : " + $scope.dateRange);
					pdf.setFontSize(11);
					pdf.text(20, 20, "Report Generated by : " + $scope.user.userName);					
					pdf.setFontSize(11);
					pdf.text(20, 25, "Report Generated on : " + exportDate + ", "	+ exportTime1);				
					pdf.setLineWidth(0.1);	       
					pdf.line(0, 30, 800, 33);
					pdf.addImage(chartImage, 'PNG', 5, 31, 250, 130)
					pdf.save($scope.filename	+ "_"+ exportDate + " "	+ exportTime1 + ".pdf");
				});		
			}			
			
		});
/*
 * //to export as image domtoimage.toPng(document.getElementById('chart-area'))
 * .then(function (blob) { window.saveAs(blob, 'my-node3.png'); });
 */
		
	   	 }			  

}
	 $scope.circleWithText = function (latLng, txt, circleOptions) {
		 var group
		/*
		 * if (group != undefined || group != null) { group.onRemove(map); }
		 */
   	  var icon = L.divIcon({
   	    html: '<div class="txt">' + txt + '</div>',
   	    className: 'circle-with-txt',
   	    iconSize: [40, 40]
   	  });
   	  var circle = L.circleMarker(latLng, circleOptions);
   	  var marker = L.marker(latLng, {
   	    icon: icon
   	  });
   	   group = L.layerGroup([circle, marker]);
   	   group.type = "circleWithText";
   	  return(group);
   	}
	 
	 $scope.searchOption = function() {						
			$scope.filterOption();
		}

		$scope.filterOption = function() {
			var indexArray = [];
			$scope.workOrderFlowViewData = [];
			$scope.workOrderFlowDataFilterList = [];
			$scope.woFlowTableheaderList = ["workOrderNumber", "totalTime", "inTime", "outTime"];			 
		
		for (var i = 0; i < $scope.workOrderFlowData.length; i++) {
			if ($scope.searchText == '') {				
				$scope.workOrderFlowDataFilterList
						.push($scope.workOrderFlowData[i]);
			} else {
				var obj = {};
				obj = $scope.workOrderFlowData[i];
				 for(var x = 0; x < $scope.woFlowTableheaderList.length; x++){								
					 var tableText = JSON.stringify($scope.workOrderFlowData[i][$scope.woFlowTableheaderList[x]]);							
				
				if ((tableText != undefined && tableText != null ? tableText : "").toLowerCase()
								.includes($scope.searchText.toLowerCase()))								
					{
						$scope.workOrderFlowDataFilterList.push($scope.workOrderFlowData[i]);
						break;									 
					}	
			 }				
		}	
	} 
	
			$scope.totalItems = $scope.workOrderFlowDataFilterList.length;
			$scope.currentPage = 1;
			$scope.getTotalPages();
			
			if ($scope.workOrderFlowDataFilterList.length == 0) {
				$scope.materialNoData = true;
				$scope.no_data_found = "No data found.";

			} else {
				$scope.materialNoData = false;
				$scope.searchButton = true;
				if ($scope.workOrderFlowDataFilterList.length > $scope.numPerPage) {
					$scope.workOrderFlowViewData = $scope.workOrderFlowDataFilterList
							.slice(0, $scope.numPerPage);
					$scope.PaginationTab = true;
				} else {
					$scope.workOrderFlowViewData = $scope.workOrderFlowDataFilterList;
					$scope.PaginationTab = false;
				}
			}
			 	
			
		}
		// pagination button

		$scope.currentPage = 1;
		$scope.numPerPage = 10;
		$scope.maxSize = 3;
		$scope.totalItems = 0;
		$scope.workOrderFlowViewData = [];
		$scope.workOrderFlowDataFilterList = [];
		$scope
		.$watch(
		'currentPage + numPerPage',
		function() {
		var begin = (($scope.currentPage - 1) * $scope.numPerPage), end = begin	+ $scope.numPerPage;
		
			if($scope.searchText == undefined || $scope.searchText == "") 
				$scope.workOrderFlowViewData = $scope.workOrderFlowData
				.slice(begin, end);
			else
				$scope.workOrderFlowViewData = $scope.workOrderFlowDataFilterList
				.slice(begin, end);

			

		$scope.enablePageButtons();
		
		});

		$scope.currentPage = 1;
		$scope.maxSize = 3;
		$scope.numPerPage = 10;
		$scope.totalItems = 0;
		$scope.totalPages = 0;

		$scope.hasNext = true;
		$scope.hasPrevious = true;
		$scope.isFirstPage = true;
		$scope.isLastPage = true;
		$scope.serial = 1;

		$scope
				.$watch(
						"currentPage",
						function(newVal, oldVal) {
							if (newVal !== oldVal) {

								if (newVal > $scope.totalPages) {
									$scope.currentPage = $scope.totalPages;
								} else {
									$scope.currentPage = newVal;
								}

								$scope.serial = (($scope.currentPage - 1) * $scope.numPerPage) + 1;
								$scope.enablePageButtons();
							}
						});

		$scope.enablePageButtons = function() {
			$scope.serial = (($scope.currentPage - 1) * $scope.numPerPage) + 1;
			$scope.selectedRow = -1;
			if ($scope.totalPages <= 1) {
				$scope.hasPrevious = true;
				$scope.isFirstPage = true;
				$scope.hasNext = true;
				$scope.isLastPage = true;

			} else if ($scope.totalPages > 1
					&& $scope.currentPage == $scope.totalPages) {

				$scope.hasPrevious = false;
				$scope.isFirstPage = false;
				$scope.hasNext = true;
				$scope.isLastPage = true;
			} else if ($scope.currentPage != 1
					&& $scope.currentPage < $scope.totalPages) {

				$scope.hasPrevious = false;
				$scope.isFirstPage = false;
				$scope.hasNext = false;
				$scope.isLastPage = false;

			} else {
				$scope.hasPrevious = true;
				$scope.isFirstPage = true;
				$scope.hasNext = false;
				$scope.isLastPage = false;
			}
			 
		};

		$scope.getTotalPages = function() {
			if (($scope.totalItems % $scope.numPerPage) > 0)
				$scope.totalPages = parseInt($scope.totalItems
						/ $scope.numPerPage) + 1;
			else
				$scope.totalPages = parseInt($scope.totalItems
						/ $scope.numPerPage);

			$scope.enablePageButtons(); 
		};

		$scope.goFirstPage = function() {
			$scope.currentPage = 1;
			$scope.enablePageButtons();
		};

		$scope.goPreviousPage = function() {
			if ($scope.currentPage > 1)
				$scope.currentPage = $scope.currentPage - 1;
			$scope.enablePageButtons();
	};

		$scope.goNextPage = function() {
			if ($scope.currentPage <= $scope.totalPages)
				$scope.currentPage = $scope.currentPage + 1;
			$scope.enablePageButtons();
		};

		$scope.goLastPage = function() {
			$scope.currentPage = $scope.totalPages;
			$scope.enablePageButtons();
		};
		
		
});
