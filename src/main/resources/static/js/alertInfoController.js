app
		.controller(
				'alertInfoController',
				function($scope, $http, $rootScope, $window, $location,	$timeout) {
					$scope.user = JSON.parse($window.sessionStorage
							.getItem("user"));
					if ($scope.user == null) {
						$location.path('/login');
						return;
					}
					//$scope.selectedOrgUser = {};
					$scope.updateAlertData = {};
					$scope.selectedRow="";
					$scope.alertData = new Array();
					$scope.alertViewData = new Array();
					$scope.alertDataFilterList = new Array();
					$scope.searchText = "";
					
					$scope.selectedRow = 0;
					$scope.orgAlertDisplayNames = [ {
						'displayName' : 'Blocked Tag',
						'keyName' : 'blockedTag'
					}, {
						'displayName' : 'Crowded Zones',
						'keyName' : 'crowdedZones'
					}, {
						'displayName' : 'Late User',
						'keyName' : 'lateVisitor'
					}, {
						'displayName' : 'Lost Service Connection',
						'keyName' : 'lostConnection'
					}, {
						'displayName' : 'Lost Tag',
						'keyName' : 'lostTag'
					},  {
						'displayName' : 'Missing Tag',
						'keyName' : 'missingTag'
					}, {
						'displayName' : 'Late Asset',
						'keyName' : 'lateAsset'
					}, {
						'displayName' : 'Asset Expiry Alert',
						'keyName' : 'assetExpiryAlert'
					}, {
						'displayName' : 'LSLM Expiry Alert',
						'keyName' : 'lslmExpiryAlert'
					}, {
						'displayName' : 'Restricted Zone',
						'keyName' : 'restrictedZoneVisitor'
					}, {
						'displayName' : 'WO Shipping Zone',
						'keyName' : 'WOshippingZone'
					},{
						'displayName' : 'WO Limit',
						'keyName' : 'WOLimitInputZoneOutputZone'
					}];

					$scope.orgAlertDisplayNamesList = [];
					$scope.searchOption = function() {
						$scope.filterOption();
					}

					$scope.filterOption = function() {
						
						$scope.alertViewData = [];
						$scope.alertDataFilterList = [];

						for (var i = 0; i < $scope.alertData.length; i++) {
							

							if ($scope.searchText == '') {
								$scope.alertDataFilterList
										.push($scope.alertData[i]);
								$scope.rowHighilited(0);
							} else { 
								if (($scope.alertData[i].displayName != undefined && $scope.alertData[i].displayName != null ?
								    $scope.alertData[i].displayName : "").toLowerCase().includes($scope.searchText.toLowerCase())
								|| ($scope.alertData[i].emailID != undefined && $scope.alertData[i].emailID != null ?
								    $scope.alertData[i].emailID : "").toLowerCase().includes($scope.searchText.toLowerCase())
								|| ($scope.alertData[i].mobileNumber != undefined && $scope.alertData[i].mobileNumber != null ?
								    $scope.alertData[i].mobileNumber : "").toLowerCase().includes($scope.searchText.toLowerCase())
								|| ($scope.alertData[i].userType != undefined	&& $scope.alertData[i].userType != null ? 
									$scope.alertData[i].userType : "").toLowerCase().includes($scope.searchText.toLowerCase())) 
								{
									$scope.alertDataFilterList.push($scope.alertData[i]);

								}

							}
							
						}

						$scope.totalItems = $scope.alertDataFilterList.length;
						$scope.currentPage = 1;
						$scope.getTotalPages();

						if ($scope.alertData.length == 0
								|| $scope.alertDataFilterList.length == 0) {
							$scope.alertViewData.length = 0;
							$scope.alertNoData = true;

						} else {
							$scope.alertNoData = false;
							//$scope.rowHighilited(0);
							var begin = (($scope.currentPage - 1) * $scope.numPerPage), end = begin
									+ $scope.numPerPage;

							$scope.alertViewData = $scope.alertDataFilterList
									.slice(begin, end);
							
						}
						$scope.rowHighilited(0);
					}
					
					$scope.getRegisteredUsers = function() {
						if (!$rootScope.checkNetconnection()) { 				
						 	$rootScope.alertDialogChecknet();
							return; 
							}
						$rootScope.showloading('#loadingBar');
						$http({
							method : 'GET',
							url : './getRegisteredUsers',
							
						})
								.then(
										
										function(response) {
											$rootScope.hideloading('#loadingBar');		
											if (response != null
													&& response.data.data != null
													&& response.data.data.length != 0
													&& response.data.data != "BAD_REQUEST"
													&& response.data.data != undefined
													&& response.data.data.length != 0) {
												
												$scope.alertData.length = 0;												
												
												$scope.alertData=response.data.data;
												
												if ($scope.alertData.length == 0) {
													$scope.alertNoData = true;
													return;
												} else {
													$scope.alertNoData = false;
												}
												
												if ($scope.alertData.length > 10) {
													$scope.PaginationTab = true;
												} else {
													$scope.PaginationTab = false;
												}
												$scope.totalItems = $scope.alertData.length;
												
												$scope.getTotalPages();

												$scope.currentPage = 1;
												if ($scope.alertData.length > $scope.numPerPage) {
													$scope.alertViewData = $scope.alertData
															.slice(
																	0,
																	$scope.numPerPage);
													$scope.hasPrevious = true;
													$scope.isFirstPage = true;
													$scope.hasNext = false;
													$scope.isLastPage = false;
													$scope.rowHighilited(0);
												} else {
													$scope.alertViewData = $scope.alertData;
													$scope.rowHighilited(0);
												}
											}else{
												$scope.alertNoData = true;
											}
									},									
									function error(response) {								
										$rootScope.hideloading('#loadingBar');			
										$rootScope.fnHttpError(response);
								    });
									
					
					}
						
						$scope.getRegisteredUsers();
						$scope.initializeAlertVar = function() {
							$scope.originalAlertblockedtagsms = 0;
							$scope.originalAlertblockedtagmail = 0;
							$scope.originalAlertblockedtagnotification = 0;	
							
							$scope.originalAlertcrowdedZonessms = 0;
							$scope.originalAlertcrowdedZonesmail = 0;
							$scope.originalAlertcrowdedZonesnotification = 0;
							
							$scope.originalAlertlateVisitorsms = 0;
							$scope.originalAlertlateVisitormail = 0;
							$scope.originalAlertlateVisitornotification = 0;
							
							$scope.originalAlertlostConnectionsms = 0;
							$scope.originalAlertlostConnectionmail = 0;
							$scope.originalAlertlostConnectionnotification = 0;
							
							$scope.originalAlertlostTagsms = 0;
							$scope.originalAlertlostTagmail = 0;
							$scope.originalAlertlostTagnotification = 0;
							
							$scope.originalAlertmissingVistiorTagsms = 0;
							$scope.originalAlertmissingVistiorTagmail = 0;
							$scope.originalAlertmissingVistiorTagnotification = 0;
							
							$scope.originalAlertlateAssetsms = 0;
							$scope.originalAlertlateAssetmail = 0;
							$scope.originalAlertlateAssetnotification = 0;
							
							$scope.originalAlertassetExpiryAlertsms = 0;
							$scope.originalAlertassetExpiryAlertmail = 0;
							$scope.originalAlertassetExpiryAlertnotification = 0;
							
							$scope.originalAlertlslmExpiryAlertsms = 0;
							$scope.originalAlertlslmExpiryAlertmail = 0;
							$scope.originalAlertlslmExpiryAlertnotification = 0;	
							
							$scope.originalAlertrestrictedZonesms = 0;
							$scope.originalAlertrestrictedZonemail = 0;
							$scope.originalAlertrestrictedZonenotification = 0;									

							$scope.originalAlertWOshippingZonesms = 0;
							$scope.originalAlertWOshippingZonemail = 0;
							$scope.originalAlertWOshippingZonenotification = 0;									
						
						}
						$scope.rowHighilited = function(row) {							
							$scope.selectedRow = row;
							$scope.authen = $scope.alertViewData[row];
							
							//$scope.isEditbtn = true;
							//$scope.isEdit = true;
							//$scope.enableAlert = false;
							$scope.orgAlertDisplayNamesList.length = 0;
							if($scope.alertViewData != undefined
									&& $scope.alertViewData.length > 0
									&& $scope.alertViewData[row].orgAlert != undefined) {

								for (var i = 0; i < $scope.orgAlertDisplayNames.length; i++) {

									var alert = $scope.orgAlertDisplayNames[i];
									var alertTag = '';
									if ($scope.alertViewData[row].orgAlert
											.hasOwnProperty(alert.keyName)) {

										alertTag = {
											'displayName' : alert.displayName,
											'keyName' : alert.keyName,
											'sms' : $scope.alertViewData[row].orgAlert[alert.keyName].sms,
											'mail' : $scope.alertViewData[row].orgAlert[alert.keyName].mail,
											'notification' : $scope.alertViewData[row].orgAlert[alert.keyName].notification
										};
										} else {
										alertTag = {
											'displayName' : alert.displayName,
											'keyName' : alert.keyName,
											'sms' : 0,
											'mail' : 0,
											'notification' : 0
										};
									}

									$scope.orgAlertDisplayNamesList
											.push(alertTag);
								}
							}
									
									if($scope.alertViewData.length != 0 && $scope.alertViewData[row].orgAlert  != undefined && $scope.alertViewData[row].orgAlert  != null )
									{
										var orgAlertLength  = $rootScope.fnGetDictionaryLength($scope.alertViewData[row].orgAlert);
										if (orgAlertLength > 0){
											if ($scope.alertViewData[row].orgAlert.blockedTag != undefined) {
												$scope.originalAlertblockedtagsms = $scope.alertViewData[row].orgAlert.blockedTag.sms;
												$scope.originalAlertblockedtagmail = $scope.alertViewData[row].orgAlert.blockedTag.mail;
												$scope.originalAlertblockedtagnotification = $scope.alertViewData[row].orgAlert.blockedTag.notification;
											} else {
												$scope.originalAlertblockedtagsms = 0;
												$scope.originalAlertblockedtagmail = 0;
												$scope.originalAlertblockedtagnotification = 0;	
											}
										
											if ($scope.alertViewData[row].orgAlert.crowdedZones != undefined) {
												$scope.originalAlertcrowdedZonessms = $scope.alertViewData[row].orgAlert.crowdedZones.sms;
												$scope.originalAlertcrowdedZonesmail = $scope.alertViewData[row].orgAlert.crowdedZones.mail;
												$scope.originalAlertcrowdedZonesnotification = $scope.alertViewData[row].orgAlert.crowdedZones.notification;
											}
											else {
												$scope.originalAlertcrowdedZonessms = 0;
												$scope.originalAlertcrowdedZonesmail = 0;
												$scope.originalAlertcrowdedZonesnotification = 0;
											}
											if ($scope.alertViewData[row].orgAlert.lateVisitor != undefined) {
												$scope.originalAlertlateVisitorsms = $scope.alertViewData[row].orgAlert.lateVisitor.sms;
												$scope.originalAlertlateVisitormail = $scope.alertViewData[row].orgAlert.lateVisitor.mail;
												$scope.originalAlertlateVisitornotification = $scope.alertViewData[row].orgAlert.lateVisitor.notification;
											} else {
												$scope.originalAlertlateVisitorsms = 0;
												$scope.originalAlertlateVisitormail = 0;
												$scope.originalAlertlateVisitornotification = 0;
											}
											if ($scope.alertViewData[row].orgAlert.lostConnection != undefined) {
												$scope.originalAlertlostConnectionsms = $scope.alertViewData[row].orgAlert.lostConnection.sms;
												$scope.originalAlertlostConnectionmail = $scope.alertViewData[row].orgAlert.lostConnection.mail;
												$scope.originalAlertlostConnectionnotification = $scope.alertViewData[row].orgAlert.lostConnection.notification;
											} else {
												$scope.originalAlertlostConnectionsms = 0;
												$scope.originalAlertlostConnectionmail = 0;
												$scope.originalAlertlostConnectionnotification = 0;
											}
											if ($scope.alertViewData[row].orgAlert.lostTag != undefined) {
												$scope.originalAlertlostTagsms = $scope.alertViewData[row].orgAlert.lostTag.sms;
												$scope.originalAlertlostTagmail = $scope.alertViewData[row].orgAlert.lostTag.mail;
												$scope.originalAlertlostTagnotification = $scope.alertViewData[row].orgAlert.lostTag.notification;
											} else {
												$scope.originalAlertlostTagsms = 0;
												$scope.originalAlertlostTagmail = 0;
												$scope.originalAlertlostTagnotification = 0;
											}
											if ($scope.alertViewData[row].orgAlert.missingVistiorTag != undefined) {
												$scope.originalAlertmissingVistiorTagsms = $scope.alertViewData[row].orgAlert.missingVistiorTag.sms;
												$scope.originalAlertmissingVistiorTagmail = $scope.alertViewData[row].orgAlert.missingVistiorTag.mail;
												$scope.originalAlertmissingVistiorTagnotification = $scope.alertViewData[row].orgAlert.missingVistiorTag.notification;
											} else {
												$scope.originalAlertmissingVistiorTagsms = 0;
												$scope.originalAlertmissingVistiorTagmail = 0;
												$scope.originalAlertmissingVistiorTagnotification = 0;
											}
											if ($scope.alertViewData[row].orgAlert.lateAsset != undefined) {
												$scope.originalAlertlateAssetsms = $scope.alertViewData[row].orgAlert.lateAsset.sms;
												$scope.originalAlertlateAssetmail = $scope.alertViewData[row].orgAlert.lateAsset.mail;
												$scope.originalAlertlateAssetnotification = $scope.alertViewData[row].orgAlert.lateAsset.notification;
											} else {
												$scope.originalAlertlateAssetsms = 0;
												$scope.originalAlertlateAssetmail = 0;
												$scope.originalAlertlateAssetnotification = 0;
											}
											if ($scope.alertViewData[row].orgAlert.assetExpiryAlert != undefined) {
												$scope.originalAlertassetExpiryAlertsms = $scope.alertViewData[row].orgAlert.assetExpiryAlert.sms;
												$scope.originalAlertassetExpiryAlertmail = $scope.alertViewData[row].orgAlert.assetExpiryAlert.mail;
												$scope.originalAlertassetExpiryAlertnotification = $scope.alertViewData[row].orgAlert.assetExpiryAlert.notification;
											} else {
												$scope.originalAlertassetExpiryAlertsms = 0;
												$scope.originalAlertassetExpiryAlertmail = 0;
												$scope.originalAlertassetExpiryAlertnotification = 0;
											}
											if ($scope.alertViewData[row].orgAlert.lslmExpiryAlert != undefined) {
												$scope.originalAlertlslmExpiryAlertsms = $scope.alertViewData[row].orgAlert.lslmExpiryAlert.sms;
												$scope.originalAlertlslmExpiryAlertmail = $scope.alertViewData[row].orgAlert.lslmExpiryAlert.mail;
												$scope.originalAlertlslmExpiryAlertnotification = $scope.alertViewData[row].orgAlert.lslmExpiryAlert.notification;	
											} else {
												$scope.originalAlertlslmExpiryAlertsms = 0;
												$scope.originalAlertlslmExpiryAlertmail = 0;
												$scope.originalAlertlslmExpiryAlertnotification = 0;
											}
											if ($scope.alertViewData[row].orgAlert.restrictedZoneVisitor != undefined) {
												$scope.originalAlertrestrictedZonesms = $scope.alertViewData[row].orgAlert.restrictedZoneVisitor.sms;
												$scope.originalAlertrestrictedZonemail = $scope.alertViewData[row].orgAlert.restrictedZoneVisitor.mail;
												$scope.originalAlertrestrictedZonenotification = $scope.alertViewData[row].orgAlert.restrictedZoneVisitor.notification;									
											} else {
												$scope.originalAlertrestrictedZonesms = 0;
												$scope.originalAlertrestrictedZonemail = 0;
												$scope.originalAlertrestrictedZonenotification = 0;
											}
											if ($scope.alertViewData[row].orgAlert.WOshippingZone != undefined) {
												$scope.originalAlertWOshippingZonesms = $scope.alertViewData[row].orgAlert.WOshippingZone.sms;
												$scope.originalAlertWOshippingZonemail = $scope.alertViewData[row].orgAlert.WOshippingZone.mail;
												$scope.originalAlertWOshippingZonenotification = $scope.alertViewData[row].orgAlert.WOshippingZone.notification;	
											} else {
												$scope.originalAlertWOshippingZonesms = 0;
												$scope.originalAlertWOshippingZonemail = 0;
												$scope.originalAlertWOshippingZonenotification = 0; 
											}
											
											if ($scope.alertViewData[row].orgAlert.WOLimitInputZoneOutputZone != undefined) {
												$scope.originalAlertWOLimitInputZoneOutputZonesms = $scope.alertViewData[row].orgAlert.WOLimitInputZoneOutputZone.sms;
												$scope.originalAlertWOLimitInputZoneOutputZonemail = $scope.alertViewData[row].orgAlert.WOLimitInputZoneOutputZone.mail;
												$scope.originalAlertWOLimitInputZoneOutputZonenotification = $scope.alertViewData[row].orgAlert.WOLimitInputZoneOutputZone.notification;	
											} else {
												$scope.originalAlertWOLimitInputZoneOutputZonesms = 0;
												$scope.originalAlertWOLimitInputZoneOutputZonemail = 0;
												$scope.originalAlertWOLimitInputZoneOutputZonenotification = 0; 
											}
										}
										else{
											$scope.initializeAlertVar();
										}
							
									}else
									{
										$scope.initializeAlertVar();

									}
									$scope.editalertvalue();
						}
						
												
						$scope.changedPhone = function() {
							$scope.enableAlert = true;

						}
						$scope.changedMail = function() {
							$scope.enableAlert = true;

						}
						$scope.editalertvalue = function() {
							$scope.isEditbtn = false;
							$scope.isEdit = true;
							$scope.enableAlert = false;

						}
						$scope.cancelEdit = function() {
							$scope.isEdit = true;
							$scope.isEditbtn = false;
							$scope.enableAlert = false;
							$scope.alertViewData[$scope.selectedRow].orgAlert.blockedTag.sms = $scope.originalAlertblockedtagsms;
							$scope.alertViewData[$scope.selectedRow].orgAlert.blockedTag.mail = $scope.originalAlertblockedtagmail;
							$scope.alertViewData[$scope.selectedRow].orgAlert.blockedTag.notification = $scope.originalAlertblockedtagnotification;
							
							$scope.alertViewData[$scope.selectedRow].orgAlert.crowdedZones.sms = $scope.originalAlertcrowdedZonessms;
							$scope.alertViewData[$scope.selectedRow].orgAlert.crowdedZones.mail = $scope.originalAlertcrowdedZonesmail;
							$scope.alertViewData[$scope.selectedRow].orgAlert.crowdedZones.notification = $scope.originalAlertcrowdedZonesnotification;
							
							$scope.alertViewData[$scope.selectedRow].orgAlert.lateVisitor.sms = $scope.originalAlertlateVisitorsms;
							$scope.alertViewData[$scope.selectedRow].orgAlert.lateVisitor.mail = $scope.originalAlertlateVisitormail;
							$scope.alertViewData[$scope.selectedRow].orgAlert.lateVisitor.notification = $scope.originalAlertlateVisitornotification;
							
							$scope.alertViewData[$scope.selectedRow].orgAlert.lostConnection.sms = $scope.originalAlertlostConnectionsms;
							$scope.alertViewData[$scope.selectedRow].orgAlert.lostConnection.mail = $scope.originalAlertlostConnectionmail;
							$scope.alertViewData[$scope.selectedRow].orgAlert.lostConnection.notification = $scope.originalAlertlostConnectionnotification;
							
							$scope.alertViewData[$scope.selectedRow].orgAlert.lostTag.sms = $scope.originalAlertlostTagsms;
							$scope.alertViewData[$scope.selectedRow].orgAlert.lostTag.mail = $scope.originalAlertlostTagmail;
							$scope.alertViewData[$scope.selectedRow].orgAlert.lostTag.notification = $scope.originalAlertlostTagnotification;
							
							$scope.alertViewData[$scope.selectedRow].orgAlert.missingVistiorTag.sms = $scope.originalAlertmissingVistiorTagsms;
							$scope.alertViewData[$scope.selectedRow].orgAlert.missingVistiorTag.mail = $scope.originalAlertmissingVistiorTagmail;
							$scope.alertViewData[$scope.selectedRow].orgAlert.missingVistiorTag.notification = $scope.originalAlertmissingVistiorTagnotification;
							
							$scope.alertViewData[$scope.selectedRow].orgAlert.lateAsset.sms = $scope.originalAlertlateAssetsms;
							$scope.alertViewData[$scope.selectedRow].orgAlert.lateAsset.mail = $scope.originalAlertlateAssetmail;
							$scope.alertViewData[$scope.selectedRow].orgAlert.lateAsset.notification = $scope.originalAlertlateAssetnotification ;
							
							$scope.alertViewData[$scope.selectedRow].orgAlert.assetExpiryAlert.sms = $scope.originalAlertassetExpiryAlertsms;
							$scope.alertViewData[$scope.selectedRow].orgAlert.assetExpiryAlert.mail = 	$scope.originalAlertassetExpiryAlertmail ;
							$scope.alertViewData[$scope.selectedRow].orgAlert.assetExpiryAlert.notification = $scope.originalAlertassetExpiryAlertnotification ;
							
							$scope.alertViewData[$scope.selectedRow].orgAlert.lslmExpiryAlert.sms = $scope.originalAlertlslmExpiryAlertsms;
							$scope.alertViewData[$scope.selectedRow].orgAlert.lslmExpiryAlert.mail = $scope.originalAlertlslmExpiryAlertmail ;
							$scope.alertViewData[$scope.selectedRow].orgAlert.lslmExpiryAlert.notification = $scope.originalAlertlslmExpiryAlertnotification ;								

							$scope.alertViewData[$scope.selectedRow].orgAlert.restrictedZoneVisitor.sms = $scope.originalAlertrestrictedZonesms;
							$scope.alertViewData[$scope.selectedRow].orgAlert.restrictedZoneVisitor.mail = $scope.originalAlertrestrictedZonemail ;
							$scope.alertViewData[$scope.selectedRow].orgAlert.restrictedZoneVisitor.notification = $scope.originalAlertrestrictedZonenotification ;								
				
							$scope.alertViewData[$scope.selectedRow].orgAlert.WOshippingZone.sms = $scope.originalAlertWOshippingZonesms;
							$scope.alertViewData[$scope.selectedRow].orgAlert.WOshippingZone.mail = $scope.originalAlertWOshippingZonemail ;
							$scope.alertViewData[$scope.selectedRow].orgAlert.WOshippingZone.notification = $scope.originalAlertWOshippingZonenotification ;								

							$scope.alertViewData[$scope.selectedRow].orgAlert.WOLimitInputZoneOutputZone.sms = $scope.originalAlertWOLimitInputZoneOutputZonesms;
							$scope.alertViewData[$scope.selectedRow].orgAlert.WOLimitInputZoneOutputZone.mail = $scope.originalAlertWOLimitInputZoneOutputZonemail ;
							$scope.alertViewData[$scope.selectedRow].orgAlert.WOLimitInputZoneOutputZone.notification = $scope.originalAlertWOLimitInputZoneOutputZonenotification ;								
 
							$scope.rowHighilited($scope.selectedRow);

						}
						$scope.saveEditalert = function() {  
							//$scope.isEditbtn = true;
							$scope.isEdit = false;							
							$scope.enableAlert = false;
							$scope.onAlertDateilsUpdate();
						}
						$scope.onAlertDateilsUpdate = function() {

							var orgAlert = {};
							for (var i = 0; i < $scope.orgAlertDisplayNamesList.length; i++) {
								var alert = $scope.orgAlertDisplayNamesList[i];
								orgAlert[alert.keyName] = {
									"sms" : alert.sms,
									"mail" : alert.mail,
									"notification" : alert.notification
								};
							}

							//$scope.updateAlertData['emailID'] = $scope.selectedOrgUser.emailID,
							//$scope.updateAlertData['orgAlert'] = orgAlert;
							$scope.authen['orgAlert'] = orgAlert;

							
							if (!$rootScope.checkNetconnection()) { 				
							 	$rootScope.alertDialogChecknet();
								return; 
								}
							
							$rootScope.showloading('#loadingBar');
							$scope.promise = $http({
								method : 'PUT',
								url : './updateAlertDetails',
								data : $scope.authen,
								
							})
									.then(
											function success(result) {
												$rootScope.hideloading('#loadingBar');		
												$scope.orgAlertDisplayNamesList.length = 0;
												$scope.rowHighilited($scope.selectedRow);
												if (result.data.status == "error") {
													$scope.invalid = result.data.message;
												} else {
													$rootScope.alertDialog(result.data.message);
													}
											},
											function error(response) {
												$rootScope.hideloading('#loadingBar');		
												$rootScope.fnHttpError(response);									
											});
						}
						//pagination button 
						
							$scope.currentPage = 1;
							$scope.numPerPage = 10;							
							$scope.totalItems = 0;
							$scope.alertDataFilterList = [];
							$scope.alertViewData = [];
							$scope
							.$watch(
									'currentPage + numPerPage',
									function() {
										var begin = (($scope.currentPage - 1) * $scope.numPerPage), end = begin
												+ $scope.numPerPage;
										if ($scope.searchText == '') // kumar
											// 02/July
											// for
											// filter
											// text
											{
											$scope.alertViewData = $scope.alertData
											.slice(begin, end);
								$scope.rowHighilited(0);
											}
										else{

											$scope.alertViewData = $scope.alertDataFilterList
													.slice(begin, end);
										$scope.rowHighilited(0);
										}
									});

							$scope.currentPage = 1;

							

							$scope.numPerPage = 10;
							$scope.totalItems = 0;
							$scope.totalPages = 0;

							$scope.hasNext = true;
							$scope.hasPrevious = true;
							$scope.isFirstPage = true;
							$scope.isLastPage = true;
							$scope.serial = 1;

							$scope
							.$watch(
									"currentPage",
									function(newVal, oldVal) {
										if (newVal !== oldVal) {

											if (newVal > $scope.totalPages) {
												$scope.currentPage = $scope.totalPages;
											} else {
												$scope.currentPage = newVal;
											}

											$scope.serial = (($scope.currentPage - 1) * $scope.numPerPage) + 1;

										}
									});

							$scope.enablePageButtons = function() {
								$scope.serial = (($scope.currentPage - 1) * $scope.numPerPage) + 1;
								$scope.selectedRow = -1;
								if ($scope.totalPages <= 1) {
									$scope.hasPrevious = true;
									$scope.isFirstPage = true;
									$scope.hasNext = true;
									$scope.isLastPage = true;

								} else if ($scope.totalPages > 1
										&& $scope.currentPage == $scope.totalPages) {

									$scope.hasPrevious = false;
									$scope.isFirstPage = false;
									$scope.hasNext = true;
									$scope.isLastPage = true;
								} else if ($scope.currentPage != 1
										&& $scope.currentPage < $scope.totalPages) {

									$scope.hasPrevious = false;
									$scope.isFirstPage = false;
									$scope.hasNext = false;
									$scope.isLastPage = false;

								} else {
									$scope.hasPrevious = true;
									$scope.isFirstPage = true;
									$scope.hasNext = false;
									$scope.isLastPage = false;
								}
								$scope.rowHighilited(0);
							};

							$scope.getTotalPages = function() {

								if (($scope.totalItems % $scope.numPerPage) > 0)
									$scope.totalPages = parseInt($scope.totalItems
											/ $scope.numPerPage) + 1;
								else
									$scope.totalPages = parseInt($scope.totalItems
											/ $scope.numPerPage);

								$scope.enablePageButtons();

							};

							$scope.goFirstPage = function() {
								$scope.currentPage = 1;
								$scope.enablePageButtons();
							};

							$scope.goPreviousPage = function() {
								if ($scope.currentPage > 1)
									$scope.currentPage = $scope.currentPage - 1;
								$scope.enablePageButtons();
							};

							$scope.goNextPage = function() {
								if ($scope.currentPage <= $scope.totalPages)
									$scope.currentPage = $scope.currentPage + 1;
								$scope.enablePageButtons();
							};

							$scope.goLastPage = function() {
								$scope.currentPage = $scope.totalPages;
								$scope.enablePageButtons();
							};
						
				});
