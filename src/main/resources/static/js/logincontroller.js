app
		.controller(
				'logincontroller',
				function($scope, $http, $location, $window, $timeout, $modal,
						$rootScope, cssInjector, $timeout) {
					$rootScope.materialwarehouseApiFlag = 1;
					$rootScope.warehouseSaveDisplayFieldArray = [];
					$rootScope.dispatchwarehouseApiFlag = 1;
					$rootScope.dispatchSaveDisplayFieldArray = [];
					$rootScope.renamedFieldListStock  = [];	
					$rootScope.renamedFieldListDispatch = [];	
					if (window.location.protocol != 'https:') {
						// location.href = location.href.replace("error");
						// window.location.replace('http://www.entappiaplatform.com/#/info');
						// window.location.replace('http://www.entappiadata.com/#/info');
					}
					// Set the default value of inputType
					$scope.inputType = 'password';
					var capabiltydata;

					// Hide & show password function
					$scope.hideShowPassword = function() {
						if ($scope.passwordCheckbox)
							$scope.inputType = 'text';
						else
							$scope.inputType = 'password';
					};

					$(document).ready(function() {
						$('[data-toggle="tooltip"]').tooltip();
						// kumar 14-Jun-->

						// const passwordCtl =
						// document.querySelector('#id_password');

						// $("#togglePassword").on('click', function(event){

						/*
						 * if(passwordCtl.getAttribute('type') == "text"){
						 * passwordCtl.setAttribute('type', 'password');
						 * $('#togglePassword svg').removeClass( "fa-eye" );
						 * $('#togglePassword svg').addClass( "fa-eye-slash" );
						 * }else if(passwordCtl.getAttribute('type') ==
						 * "password"){ passwordCtl.setAttribute('type',
						 * 'text'); $('#togglePassword svg').removeClass(
						 * "fa-eye-slash" ); $('#togglePassword svg').addClass(
						 * "fa-eye" ); }
						 */
						// });
						// kumar 14-Jun <--
					});

					// kumar 14-Jun show password with eye icon
					$scope.eyeButtonClick = function() {
						const passwordCtl = document
								.querySelector('#id_password');

						if (passwordCtl.getAttribute('type') == "password") {
							passwordCtl.setAttribute('type', 'text');
							// $("#togglePassword
							// svg").removeClass("fa-eye-slash").addClass("fa-eye");
						} else if (passwordCtl.getAttribute('type') == "text") {
							passwordCtl.setAttribute('type', 'password');
							// $("#togglePassword
							// svg").removeClass("fa-eye").addClass("fa-eye-slash");
						}
						$("#togglePassword svg").toggleClass(
								"fa-eye fa-eye-slash");
					}
					// transfers sessionStorage from one tab to another
					var sessionStorage_transfer = function(event) {
						if (!event) {
							event = window.event;
						} // ie suq
						if (!event.newValue)
							return; // do nothing if no value to work with
						if (event.key == 'getSessionStorage') {
							// another tab asked for the sessionStorage -> send
							// it
							localStorage.setItem('sessionStorage', JSON
									.stringify(sessionStorage));
							// the other tab should now have it, so we're done
							// with it.
							localStorage.removeItem('sessionStorage'); // <-
							// could
							// do
							// short
							// timeout
							// as
							// well.
						} else if (event.key == 'sessionStorage'
								&& !sessionStorage.length) {
							// another tab sent data <- get it
							var data = JSON.parse(event.newValue);
							for ( var key in data) {
								sessionStorage.setItem(key, data[key]);
							}
						}
					};

					// listen for changes to localStorage
					if (window.addEventListener) {
						window.addEventListener("storage",
								sessionStorage_transfer, false);
					} else {
						window
								.attachEvent("onstorage",
										sessionStorage_transfer);
					};

					// Ask other tabs for session storage (this is ONLY to
					// trigger event)
					if (!sessionStorage.length) {
						localStorage.setItem('getSessionStorage', 'foobar');
						localStorage.removeItem('getSessionStorage', 'foobar');
					};
					
					// Hide & show password function
					$scope.hideShowPassword = function() {
						if ($scope.passwordCheckbox)
							$scope.inputType = 'text';
						else
							$scope.inputType = 'password';
					};
					/*$rootScope.checkNetconnection = function() {

						return $window.navigator.onLine;
					}*/
					$scope.forgotPassword = function() {
						$location.path("/forgotPassword");

					}
					$scope.register = function() {
						$location.path("/register");

					}
					$scope.changePwd = function() {
						$location.path("/changePassword");
					}
					$scope.fnGetDictionaryLength = function(dict) {
						var nCount = 0;
						for (i in dict)
							++nCount;
						return nCount;
					}

					$scope.authenticate = function() {
						
						// Multiple instance check
						if (!sessionStorage.length) {
							
							var emailLogin = document.getElementById("emailID");
							if (emailLogin.value.length == 0) {
								$scope.loginEmailError = true;
								$scope.loginEmail_error_msg = "Please enter Email Id";
								$timeout(function() {
									$scope.loginEmailError = false;
								}, 2000);
								return;
							}
							var mailformat = /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/ ;
							if (!mailformat.test(emailLogin.value)) {
								 $scope.loginEmailError = true;
								$scope.loginEmail_error_msg = "Please enter a valid Email Id";
								$timeout(function() {
									$scope.loginEmailError = false;
								}, 2000);
								return;
							}
							var loginPassword = document.getElementById("id_password");
							if (loginPassword.value.length == 0) {
								$scope.loginPasswordError = true;
								$scope.loginPassword_error_msg = "Please enter Password";
								$timeout(function() {
									$scope.loginPasswordError = false;
								}, 2000);
								return;
							}

								var loginParameter = {
									"emailID" : $scope.authen.emailID,
									"password" : $scope.authen.password

								};
								if (!$rootScope.checkNetconnection()) { 
									$rootScope.alertDialogChecknet();
									return;
											 } 
								$rootScope.showloading('#loadingBar');
								$scope.promise = $http.post('./login', loginParameter, {timeout: $rootScope.TIME_OUT_BEFORE_LOGIN})
										.then(
												function(result) {
													$rootScope.hideloading('#loadingBar');
													if (result.data.status == "error") {

														$rootScope.alertDialog(result.data.message);    

													} else {
														 

														// $rootScope.hideloading('#loadingBar');
														// $window.
														// alert("Welcome"+"
														// "+result.data.profile.userName);
														//console.log("success");
														var capabilityListLength = $scope
																.fnGetDictionaryLength(result.data.profile.capabilityList);
														if (capabilityListLength == 0) {

															$rootScope.alertDialog('Menu Access Denied. Please contact administrator');   
															 
															return;
														}
														var preferredMenuLength = $scope
																.fnGetDictionaryLength(result.data.profile.capabilityList.preferredMenu);
														if (preferredMenuLength > 0) {
															capabiltydata = result.data.profile.capabilityList.preferredMenu;
														} else {
															capabiltydata = result.data.profile.capabilityList.allowedMenu;
														}
														
														data = {
															role : result.data.profile.userType,
															emailID : result.data.profile.userEmail,
															capabilityDetails : capabiltydata,
															capabilityList : result.data.profile.capabilityList,
															userName : result.data.profile.userName,
															showNotification : result.data.profile.showNotification,
															profileColor : result.data.profile.profileColor,
															serviceProvider : "Quuppa"

														}
														$window.sessionStorage
																.setItem(
																		"lastSession",
																		"-");
														$window.sessionStorage
																.setItem(
																		"user",
																		JSON
																				.stringify(data));
														
														$window.sessionStorage
																.setItem(
																		"readNotificationCount",
																		result.data.profile.readNotificationCount);

														$window.sessionStorage
														.setItem(
																"profileImage",
																result.data.profile.profileImage);
														
														$rootScope
																.$broadcast('updateCapabilityMenu');

													}

												},
												function(error) {
													$rootScope.hideloading('#loadingBar');													
													  if (error.data == null && error.status == -1 ) {
														  $rootScope.outsideLoginTimeoutDialog();  
													  }else if (error.data.status == "error") {
														$rootScope.alertErrorDialog(error.data.message); 
													}

												});

							
						} else {
							
							$rootScope.alertErrorDialog("EI4.0 application is already running.");   
 
						}
					}

				});
