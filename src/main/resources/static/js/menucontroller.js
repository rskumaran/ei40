app
		.controller(
				"menuCtrl",
				function($scope, $rootScope, $location, $http, $window,
						cssInjector, Idle, Keepalive, $modal, $route, $timeout) {

					$('#modal').modal({
						backdrop : 'static',
						keyboard : false
					})

 					 
					$rootScope.countryCodes = ["+1","+91"];
					$scope.notificationCount = '';
					$scope.notificationEmergenyCount = '';
					$scope.notificationWarningCount = '';
					$scope.notificationInfoCount = '';
					$rootScope.isEmpManagement = false ;
					$rootScope.isEmpManagementEdit = false ;

					$rootScope.organizationMenu = false;
					$scope.mainMenu = true;
					$scope.varAccessMenuOption = 0;
					//let capabiltydata;
					$scope.varSysAdminColor = "#7691e3";
					$scope.varManagerColor = "#914f33";
					$scope.varSuperUserColor = "#03421b";
					$scope.varUserColor = "#322452";
					$scope.showNotification = false;
					$scope.menuLinkClick = false;
					$scope.orgList = [];
					
					$rootScope.selectedRowOnZoneDetail =  {};
					
					$rootScope.ERR_403_FOUND = 403;
					$rootScope.TIME_OUT_BEFORE_LOGIN = 10000;
					$rootScope.MSGBOX_TITLE = "EI4.O";
					$rootScope.ZoneSelection = {}; //zone select
					$scope.showNotificationBtn  = true;
					$scope.showPreviousScreenIcon = false;
					$scope.previousMenuFromLive = "";
					$scope.serverNotRespondingMessage = false;
					$rootScope.LocateItem = false; //Display sub menu of header option when return back to live view screen
					$scope.displayNone = {
						'display' : 'none'
					};
					$scope.displayBlock = {
						'display' : 'block'
					};

					toastr.options = {
						"closeButton" : true,
						"newestOnTop" : false,
						"progressBar" : true,
						"positionClass" : "toast-top-right",
						"preventDuplicates" : false,
						"onclick" : null,
						"showDuration" : "100",
						"hideDuration" : "1000",
						"timeOut" : "5000",
						"extendedTimeOut" : "1000",
						"showEasing" : "swing",
						"hideEasing" : "linear",
						"showMethod" : "fadeIn",
						"hideMethod" : "fadeOut"
					}

					 
					$rootScope.trackingMacId = '';
					$rootScope.trackingOutboundId = '';
					$rootScope.pageName = '';
					
					$scope.menuLink = function(page, pagename) {
						$scope.pagename = pagename;
						$rootScope.pageName = pagename;

						$scope.menuLinkClick = true;
						if (page != 'login')
							$window.sessionStorage.setItem("menuName", page);
						if (page == 'zoneManagement' || page == 'live'
								|| page == 'peopleLiveView'
								|| page == 'assetLiveView'
								|| page == 'workorderLiveView'
								|| page == 'skuLiveView'
								|| page == 'warehouseLiveView'
								|| page == 'shopFloorAnalysis') {
							$rootScope.showFullView();

						} else if (page == 'topologyEditor') {
							$("#loadingBar").removeClass("sidebar-mini");
							$("#loadingBar").addClass("sidebar-collapse");
							$scope.topoloy = true;
							$("#idAll").addClass("active");

						} else if (page == 'materialWarehouse' || page == 'employee' || page == 'workstation') { //zone select
							$rootScope.ZoneSelection = {}; //reset zone selection
						}else {
							// $("#loadingBar").removeClass("sidebar-collapse");
							$("#loadingBar").addClass("sidebar-mini");
							$scope.topoloy = false;
						}
						if ($scope.user.role == "DemoUser") {
							if (page == 'live'
								|| page == 'peopleLiveView'
								|| page == 'assetLiveView'
								|| page == 'workorderLiveView'
								|| page == 'skuLiveView'
								|| page == 'warehouseLiveView' ) {  
								page = page+"Demo";
							}
							
						}
						$scope.previousMenuFromLive = "";
						$scope.showPreviousScreenIcon = false; 
						$rootScope.isBackClicked = false;
						$rootScope.LocateItem = false;
						$scope.serverNotRespondingMessage = false;
						$location.path(page);

						$scope.setPageHeaderColor()

					}
					// Disable key press
					function disableKeyPressing(e) {

					    // keycodes table https://css-tricks.com/snippets/javascript/javascript-keycodes/
					    var conditions = [
					    	// Diable F12
					        (e.which || e.keyCode) == 123,
					        // Diable F5
					        (e.which || e.keyCode) == 116,
					        // Diable Ctrl+R
					        e.ctrlKey && (e.which === 82),
					        //Diable Ctrl+S
					        e.ctrlKey && (e.which === 83),
					        //Diable Ctrl+P
					        e.ctrlKey && (e.which === 80),
					        //Diable Ctrl+O
					        e.ctrlKey && (e.which === 79),
					        //Diable Shift+Ctrl+i
					        e.shiftKey && e.ctrlKey && (e.which === 73),
						    //Diable Shift+Ctrl+j
					        e.shiftKey && e.ctrlKey && (e.which === 74)
					    ]

					    if ( $.each(conditions, function(key, val) { val + ' || ' }) ) {
					        e.preventDefault();
					    }
					}
					
					
					$(document).on('keydown', function(e) {
						//console.log(e.which);
						// F12 is pressed
					    if((e.which || e.keyCode) == 123) {
					        disableKeyPressing(e);
					        //console.log('F12 is diabled now');
					    }
					    // F5 is pressed
					    if((e.which || e.keyCode) == 116) {
					        disableKeyPressing(e);
					        //console.log('F5 is diabled now');
					    }

					    // Ctrl+R
					    if (e.ctrlKey && (e.which === 82) ) {
					        disableKeyPressing(e);
					        //console.log('Ctrl+R is pressed and refresh is diabled now');
					    }
					    // Ctrl+S
					    if (e.ctrlKey && (e.which === 83) ) {
					        disableKeyPressing(e);
					        //console.log('Ctrl+S is pressed and refresh is diabled now');
					    }
					    // Ctrl+P
					    if (e.ctrlKey && (e.which === 80) ) {
					        disableKeyPressing(e);
					        //console.log('Ctrl+P is pressed and refresh is diabled now');
					    }
						 // shift+Ctrl+i
					    if (e.shiftKey && e.ctrlKey && (e.which === 73) ) {
					        disableKeyPressing(e);
					        //console.log('Ctrl+P is pressed and refresh is diabled now');
					    }
						 // shift+Ctrl+j
					    if (e.shiftKey && e.ctrlKey && (e.which === 74) ) {
					        disableKeyPressing(e);
					        //console.log('Ctrl+P is pressed and refresh is diabled now');
					    }
					    // Ctrl+O
					    if (e.ctrlKey && (e.which === 79) ) {
					        disableKeyPressing(e);
					        //console.log('Ctrl+O is pressed and refresh is diabled now');
					    }
					    });
					$rootScope.showFullView = function() {
						$("#loadingBar").removeClass("sidebar-mini");
						$("#loadingBar").addClass("sidebar-collapse");

						$scope.topoloy = false;
						$("#idAll").addClass("active");
					};

					$scope.setPageHeaderColor = function() {
						var pageColor = $scope.pagename
								.fontcolor($scope.varSysAdminColor);
						if ($scope.user.role == "SystemAdmin" || $scope.user.role == "DemoUser") {
							pageColor = $scope.pagename
									.fontcolor($scope.varSysAdminColor);
						} else if ($scope.user.role == "Manager") {
							pageColor = $scope.pagename
									.fontcolor($scope.varManagerColor);
						} else if ($scope.user.role == "SuperUser") {
							pageColor = $scope.pagename
									.fontcolor($scope.varSuperUserColor);
						} else if ($scope.user.role == "User") {
							pageColor = $scope.pagename
									.fontcolor($scope.varUserColor);
						}
						document.getElementById("pagenameColorChange").innerHTML = pageColor;
					}
					$scope.isActive = function(viewLocation) {
						var active = (viewLocation === $location.path());
						return active;
					};

					$scope.role = 'dashboard';
					cssInjector.add('css/admin.css');
					$rootScope.$on('updatemenu', function(event, data) {

						$scope.role = data;
						$scope.user = JSON.parse($window.sessionStorage
								.getItem("user"));

					});
					
					

					//$scope.cLIVE = 1;

					$rootScope
							.$on(
									'updateCapabilityMenu',
									function(event) {
										$scope.user = JSON
												.parse($window.sessionStorage
														.getItem("user"));
										$scope.showNotification = $scope.user.showNotification;
										$scope.role = $scope.user.role;
										$scope.showNotificationBtn   = true;
										if ($scope.user.role == "DemoUser") // don't display notication icon for DemoUser 
											$scope.showNotificationBtn   = false;
										var _path;
										var preferredMenuLength = $rootScope.fnGetDictionaryLength($scope.user.capabilityList.preferredMenu);
										if (preferredMenuLength > 0) {
											$scope.varAccessMenuOption = 1;
											
										} else {
											$scope.varAccessMenuOption = 0;
										
										}
										if ($scope.user.role == "SystemAdmin" || $scope.user.role == "DemoUser") {
											menuPageValidation();

											$scope.dshEnable = 1;
											_path = "/dashboard";
											$scope.pagename = "Dashboard";
										} else if ($scope.user.role == "Manager") {
											$scope.dshEnable = 0;
											// $scope.orgchange($scope.user.orgName);
											menuPageValidation();
										} else if ($scope.user.role == "SuperUser") {
											$scope.dshEnable = 0;
											// $scope.orgchange($scope.user.orgName);
											menuPageValidation();
										} else if ($scope.user.role == "User") {
											$scope.dshEnable = 0;
											menuPageValidation();
											// $scope.orgchange($scope.user.orgName);
										}
										//save the pagename here. it used to search and locate (locate and back to previous screen).
										$window.sessionStorage.setItem("menuName", _path.substring(1));
										$scope.connect();

										function menuPageValidation() {

											// People
											if (checkPeople() == true) {

												if (checkPeopleTag() == true) {
													_path = "/tagPeople";
													$scope.pagename = "People Tag Management";
												} else if (checkPeopleTag() == false
														&& checkPeopleEmp() == true) {
													_path = "/employee";
													$scope.pagename = "Employee";
												} else if (checkPeopleTag() == false
														&& checkPeopleEmp() == false
														&& $scope.user.capabilityDetails.POTU == 1) {
													_path = "/otherUser";
													$scope.pagename = "Other User Details";
												} else if (checkPeopleTag() == false
														&& checkPeopleEmp() == false
														&& ($scope.user.capabilityDetails.POTU == 0 || $scope.user.capabilityDetails.POTU == undefined)
														&& $scope.user.capabilityDetails.PTAL == 1) {
													_path = "/peopleType";
													$scope.pagename = "People Time Analytics";
												} else if (checkPeopleTag() == false
														&& checkPeopleEmp() == false
														&& ($scope.user.capabilityDetails.POTU == 0 || $scope.user.capabilityDetails.POTU == undefined)
														&& ($scope.user.capabilityDetails.PTAL == 0 || $scope.user.capabilityDetails.PTAL == undefined)
														&& $scope.user.capabilityDetails.PPAR == 1) {
													_path = "/plantTraffic";
													$scope.pagename = "Plant Traffic";
												} else if (checkPeopleTag() == false
														&& checkPeopleEmp() == false
														&& ($scope.user.capabilityDetails.POTU == 0 || $scope.user.capabilityDetails.POTU == undefined)
														&& ($scope.user.capabilityDetails.PTAL == 0 || $scope.user.capabilityDetails.PTAL == undefined)
														&& ($scope.user.capabilityDetails.PPAR == 0 || $scope.user.capabilityDetails.PPAR == undefined)
														&& $scope.user.capabilityDetails.PPFA == 1) {
													_path = "/plantOccupancy";
													$scope.pagename = "Plant Occupancy";
												} else if (checkPeopleTag() == false
														&& checkPeopleEmp() == false
														&& ($scope.user.capabilityDetails.POTU == 0 || $scope.user.capabilityDetails.POTU == undefined)
														&& ($scope.user.capabilityDetails.PPAR == 0 || $scope.user.capabilityDetails.PPAR == undefined)
														&& ($scope.user.capabilityDetails.PTAL == 0 || $scope.user.capabilityDetails.PTAL == undefined)
														&& ($scope.user.capabilityDetails.PPFA == 0 || $scope.user.capabilityDetails.PPFA == undefined)
														&& $scope.user.capabilityDetails.PLVW == 1) {
													_path = "/peopleLiveView";
													$scope.pagename = "People Live View";
												}

											}
											// Asset
											if (checkAsset() == true) {
												if (checkPeople() == false) {

													if (checkAssetTag() == true) {
														_path = "/tagAsset";
														$scope.pagename = "Asset Tag Management";
													} else if (checkAssetTag() == false
															&& checkAssetNonChrono() == true) {
														_path = "/assetNonChrono";
														$scope.pagename = "Asset- Non Chrono Management";
													} else if (checkAssetTag() == false
															&& checkAssetNonChrono() == false
															&& checkAssetChrono() == true) {
														_path = "/assetChrono";
														$scope.pagename = "Asset Chrono Management";
													} else if (checkAssetTag() == false
															&& checkAssetNonChrono() == false
															&& checkAssetChrono() == false
															&& $scope.user.capabilityDetails.AAVH == 1) {
														_path = "/assetVehicle";
														$scope.pagename = "Asset Vehicle";
													} else if (checkAssetTag() == false
															&& checkAssetNonChrono() == false
															&& checkAssetChrono() == false
															&& ($scope.user.capabilityDetails.AAVH == 0 || $scope.user.capabilityDetails.AAVH == undefined)
															&& $scope.user.capabilityDetails.ASTA == 1) {
														_path = "/assetTimeAnalytics";
														$scope.pagename = "Asset Time Analytics";
													} else if (checkAssetTag() == false
															&& checkAssetNonChrono() == false
															&& checkAssetChrono() == false
															&& ($scope.user.capabilityDetails.AAVH == 0 || $scope.user.capabilityDetails.AAVH == undefined)
															&& ($scope.user.capabilityDetails.ASTA == 0 || $scope.user.capabilityDetails.ASTA == undefined)
															&& $scope.user.capabilityDetails.ACAL == 1) {
														_path = "/calibration";
														$scope.pagename = "Calibration Record";
													} else if (checkAssetTag() == false
															&& checkAssetNonChrono() == false
															&& checkAssetChrono() == false
															&& ($scope.user.capabilityDetails.AAVH == 0 || $scope.user.capabilityDetails.AAVH == undefined)
															&& ($scope.user.capabilityDetails.ASTA == 0 || $scope.user.capabilityDetails.ASTA == undefined)
															&& ($scope.user.capabilityDetails.ACAL == 0 || $scope.user.capabilityDetails.ACAL == undefined)
															&& $scope.user.capabilityDetails.ASLV == 1) {
														_path = "/assetLiveView";
														$scope.pagename = "Asset Live View";
													}
												}

											}

											// WorkOrder
											if (checkWork() == true) {
												if (checkPeople() == false
														&& checkAsset() == false) {

													if (checkWorkTag() == true) {
														_path = "/tagWorkorder";
														$scope.pagename = "Work-Order Tag Management";
													} else if (checkWorkTag() == false
															&& checkWorkMaterial() == true) {
														_path = "/material";
														$scope.pagename = "Material Management";
													} else if (checkWorkTag() == false
															&& checkWorkMaterial() == false
															&& checkWorkStation() == true) {
														_path = "/workstation";
														$scope.pagename = "Work Station";
													} else if (checkWorkTag() == false
															&& checkWorkMaterial() == false
															&& checkWorkStation() == false
															&& $scope.user.capabilityDetails.WOFA == 1) {
														_path = "/workOrderFlowAnalytics";
														$scope.pagename = "Work Order Flow Analytics";
													} else if (checkWorkTag() == false
															&& checkWorkMaterial() == false
															&& checkWorkStation() == false
															&& ($scope.user.capabilityDetails.WOFA == undefined || $scope.user.capabilityDetails.WOFA == 0)
															&& $scope.user.capabilityDetails.WZFA == 1) {
														_path = "/zoneFlowAnalytics";
														$scope.pagename = "Work Station Flow Analysis";
													} else if (checkWorkTag() == false
															&& checkWorkMaterial() == false
															&& checkWorkStation() == false
															&& ($scope.user.capabilityDetails.WOFA == undefined || $scope.user.capabilityDetails.WOFA == 0)
															&& ($scope.user.capabilityDetails.WZFA == undefined || $scope.user.capabilityDetails.WZFA == 0)
															&& $scope.user.capabilityDetails.WCAL == 1) {
														_path = "/constraintAnalytics";
														$scope.pagename = "Constraint Analytics";
													} else if (checkWorkTag() == false
															&& checkWorkMaterial() == false
															&& checkWorkStation() == false
															&& ($scope.user.capabilityDetails.WOFA == undefined || $scope.user.capabilityDetails.WOFA == 0)
															&& ($scope.user.capabilityDetails.WZFA == undefined || $scope.user.capabilityDetails.WZFA == 0)
															&& ($scope.user.capabilityDetails.WCAL == undefined || $scope.user.capabilityDetails.WCAL == 0)
															&& $scope.user.capabilityDetails.WCND == 1) {
														_path = "/inventoryChart";
														$scope.pagename = "inventoryChart";
													} else if (checkWorkTag() == false
															&& checkWorkMaterial() == false
															&& checkWorkStation() == false
															&& ($scope.user.capabilityDetails.WOFA == undefined || $scope.user.capabilityDetails.WOFA == 0)
															&& ($scope.user.capabilityDetails.WZFA == undefined || $scope.user.capabilityDetails.WZFA == 0)
															&& ($scope.user.capabilityDetails.WCAL == undefined || $scope.user.capabilityDetails.WCAL == 0)
															&& ($scope.user.capabilityDetails.WCND == undefined || $scope.user.capabilityDetails.WCND == 0)
															&& $scope.user.capabilityDetails.WACT == 1) {
														_path = "/agingChart";
														$scope.pagename = "agingChart";
													} else if (checkWorkTag() == false
															&& checkWorkMaterial() == false
															&& checkWorkStation() == false
															&& ($scope.user.capabilityDetails.WOFA == undefined || $scope.user.capabilityDetails.WOFA == 0)
															&& ($scope.user.capabilityDetails.WOFA == undefined || $scope.user.capabilityDetails.WOFA == 0)
															&& ($scope.user.capabilityDetails.WCAL == undefined || $scope.user.capabilityDetails.WCAL == 0)
															&& ($scope.user.capabilityDetails.WCND == undefined || $scope.user.capabilityDetails.WCND == 0)
															&& ($scope.user.capabilityDetails.WACT == undefined || $scope.user.capabilityDetails.WACT == 0)
															&& $scope.user.capabilityDetails.WLVW == 1) {
														_path = "/workorderLiveView";
														$scope.pagename = "Work Order Live View";
													}
												}

											}

											//SKU 
											if (checkSKUMaterial() == true) {
												if (checkPeople() == false
														&& checkAsset() == false
														&& checkWork() == false) {
													if (checSKUTag() == true) {
														_path = "/tagSKU";
														$scope.pagename = "SKU Tag Management";
													} else if (checSKUTag() == false
															&& ($scope.user.capabilityDetails.SDET == 1 || $scope.user.capabilityDetails.SIMP == 1 || $scope.user.capabilityDetails.SDIS == 1)) {
														_path = "/materialSKU";
														$scope.pagename = "Stock Keeping Unit";
													} else if (checSKUTag() == false
															&& ($scope.user.capabilityDetails.SDET == undefined || $scope.user.capabilityDetails.SDET == 0)
															&& ($scope.user.capabilityDetails.SIMP == undefined || $scope.user.capabilityDetails.SIMP == 0)
															&& ($scope.user.capabilityDetails.SDIS == undefined || $scope.user.capabilityDetails.SDIS == 0)
															&& $scope.user.capabilityDetails.SKLV == 1) {
														_path = "/skuLiveView";
														$scope.pagename = "SKU Live View";
													}
												}
											}
											//Warehouse
											if (checkWarehouse() == true) {
												if (checkPeople() == false
														&& checkAsset() == false
														&& checkWork() == false
														&& checkSKUMaterial() == false) {
													if (checkWarehouseTag() == true) {
														_path = "/tagWarehouse";
														$scope.pagename = "Warehouse Tag Management";
													} else if (checkWarehouse() == false
															&& ($scope.user.capabilityDetails.HMVA == 1 || $scope.user.capabilityDetails.HMDP == 1)) {
														_path = "/materialWarehouse";
														$scope.pagename = "Material";
													} else if (checkWarehouse() == false
															&& ($scope.user.capabilityDetails.HMVA == undefined || $scope.user.capabilityDetails.HMVA == 0)
															&& ($scope.user.capabilityDetails.HMDP == undefined || $scope.user.capabilityDetails.HMDP == 0)
															&& $scope.user.capabilityDetails.HKLV == 1) {
														_path = "/warehouseLiveView";
														$scope.pagename = "Warehouse Live View";
													}
												}
											}
											
											// EI4.0Demo  
											if (checkEI40Demo() == true) {
												if (checkWarehouse() == false
														&& checkPeople() == false
														&& checkAsset() == false
														&& checkWork() == false
														&& checkSKUMaterial() == false) {
													if (checkEI40Analytics() == true) {
														_path = "/ei40Analytics";
														$scope.pagename = "EI4.0 Analytics";
													} else if (checkEI40Analytics() == false
															&& $scope.user.capabilityDetails.DSFA == 1 ) {
														_path = "/shopFloorAnalysis";
														$scope.pagename = "Shop Floor Analysis";
													} 
												}
											}
											// Configuration
											if (checkConfig() == true) {
												if (checkPeople() == false
														&& checkAsset() == false
														&& checkWork() == false
														&& checkSKUMaterial() == false
														&& checkWarehouse() == false
														&& checkEI40Demo() == false) {

													if ($scope.user.capabilityDetails.CORS == 1) {
														_path = "/configuration";
														$scope.pagename = "Configuration";
													} else if (($scope.user.capabilityDetails.CORS == undefined || $scope.user.capabilityDetails.CORS == 0)
															&& $scope.user.capabilityDetails.CTPE == 1) {
														_path = "/topologyEditor";
														$scope.pagename = "Topology Editor";
													} else if (($scope.user.capabilityDetails.CORS == undefined || $scope.user.capabilityDetails.CORS == 0)
															&& ($scope.user.capabilityDetails.CTPE == undefined || $scope.user.capabilityDetails.CTPE == 0)
															&& $scope.user.capabilityDetails.CZOM == 1) {
														_path = "/topologyEditor";
														$scope.pagename = "Zone Management";
													} else if (($scope.user.capabilityDetails.CORS == undefined || $scope.user.capabilityDetails.CORS == 0)
															&& ($scope.user.capabilityDetails.CTPE == undefined || $scope.user.capabilityDetails.CTPE == 0)
															&& ($scope.user.capabilityDetails.CZOM == undefined || $scope.user.capabilityDetails.CZOM == 0)
															&& $scope.user.capabilityDetails.CLVW == 1) {
														_path = "/live";
														$scope.pagename = "Live View";
													} else if (($scope.user.capabilityDetails.CORS == undefined || $scope.user.capabilityDetails.CORS == 0)
															&& ($scope.user.capabilityDetails.CTPE == undefined || $scope.user.capabilityDetails.CTPE == 0)
															&& ($scope.user.capabilityDetails.CZOM == undefined || $scope.user.capabilityDetails.CZOM == 0)
															&& ($scope.user.capabilityDetails.CLVW == undefined || $scope.user.capabilityDetails.CLVW == 0)
															&& $scope.user.capabilityDetails.CREG == 1) {
														_path = "/accountCreation";
														$scope.pagename = "Create Account";
													} else if (($scope.user.capabilityDetails.CORS == undefined || $scope.user.capabilityDetails.CORS == 0)
															&& ($scope.user.capabilityDetails.CTPE == undefined || $scope.user.capabilityDetails.CTPE == 0)
															&& ($scope.user.capabilityDetails.CZOM == undefined || $scope.user.capabilityDetails.CZOM == 0)
															&& ($scope.user.capabilityDetails.CLVW == undefined || $scope.user.capabilityDetails.CLVW == 0)
															&& ($scope.user.capabilityDetails.CREG == undefined || $scope.user.capabilityDetails.CREG == 0)
															&& $scope.user.capabilityDetails.CAMG == 1) {
														_path = "/accessmanagement";
														$scope.pagename = "Access Management";
													} else if (($scope.user.capabilityDetails.CORS == undefined || $scope.user.capabilityDetails.CORS == 0)
															&& ($scope.user.capabilityDetails.CTPE == undefined || $scope.user.capabilityDetails.CTPE == 0)
															&& ($scope.user.capabilityDetails.CZOM == undefined || $scope.user.capabilityDetails.CZOM == 0)
															&& ($scope.user.capabilityDetails.CLVW == undefined || $scope.user.capabilityDetails.CLVW == 0)
															&& ($scope.user.capabilityDetails.CREG == undefined || $scope.user.capabilityDetails.CREG == 0)
															&& ($scope.user.capabilityDetails.CAMG == undefined || $scope.user.capabilityDetails.CAMG == 0)
															&& $scope.user.capabilityDetails.CDPT == 1) {
														_path = "/department";
														$scope.pagename = "Department";
													} else if (($scope.user.capabilityDetails.CORS == undefined || $scope.user.capabilityDetails.CORS == 0)
															&& ($scope.user.capabilityDetails.CTPE == undefined || $scope.user.capabilityDetails.CTPE == 0)
															&& ($scope.user.capabilityDetails.CZOM == undefined || $scope.user.capabilityDetails.CZOM == 0)
															&& ($scope.user.capabilityDetails.CLVW == undefined || $scope.user.capabilityDetails.CLVW == 0)
															&& ($scope.user.capabilityDetails.CREG == undefined || $scope.user.capabilityDetails.CREG == 0)
															&& ($scope.user.capabilityDetails.CAMG == undefined || $scope.user.capabilityDetails.CAMG == 0)
															&& ($scope.user.capabilityDetails.CDPT == undefined || $scope.user.capabilityDetails.CDPT == 0)
															&& $scope.user.capabilityDetails.CCLA == 1) {
														_path = "/classification";
														$scope.pagename = "Zone Classification";
													} else if (($scope.user.capabilityDetails.CORS == undefined || $scope.user.capabilityDetails.CORS == 0)
															&& ($scope.user.capabilityDetails.CTPE == undefined || $scope.user.capabilityDetails.CTPE == 0)
															&& ($scope.user.capabilityDetails.CZOM == undefined || $scope.user.capabilityDetails.CZOM == 0)
															&& ($scope.user.capabilityDetails.CLVW == undefined || $scope.user.capabilityDetails.CLVW == 0)
															&& ($scope.user.capabilityDetails.CREG == undefined || $scope.user.capabilityDetails.CREG == 0)
															&& ($scope.user.capabilityDetails.CAMG == undefined || $scope.user.capabilityDetails.CAMG == 0)
															&& ($scope.user.capabilityDetails.CDPT == undefined || $scope.user.capabilityDetails.CDPT == 0)
															&& ($scope.user.capabilityDetails.CCLA == undefined || $scope.user.capabilityDetails.CCLA == 0)
															&& $scope.user.capabilityDetails.CART == 1) {
														_path = "/alertInfo";
														$scope.pagename = "Alert Information";
													} else if (($scope.user.capabilityDetails.CORS == undefined || $scope.user.capabilityDetails.CORS == 0)
															&& ($scope.user.capabilityDetails.CTPE == undefined || $scope.user.capabilityDetails.CTPE == 0)
															&& ($scope.user.capabilityDetails.CZOM == undefined || $scope.user.capabilityDetails.CZOM == 0)
															&& ($scope.user.capabilityDetails.CLVW == undefined || $scope.user.capabilityDetails.CLVW == 0)
															&& ($scope.user.capabilityDetails.CREG == undefined || $scope.user.capabilityDetails.CREG == 0)
															&& ($scope.user.capabilityDetails.CAMG == undefined || $scope.user.capabilityDetails.CAMG == 0)
															&& ($scope.user.capabilityDetails.CDPT == undefined || $scope.user.capabilityDetails.CDPT == 0)
															&& ($scope.user.capabilityDetails.CCLA == undefined || $scope.user.capabilityDetails.CCLA == 0)
															&& ($scope.user.capabilityDetails.CART == undefined || $scope.user.capabilityDetails.CART == 0)
															&& $scope.user.capabilityDetails.CTVR == 1) {
														_path = "/tag";
														$scope.pagename = "Tag Details";
													}

												}

											}

											// LOG
											if (checkLog() == true) {
												if (checkPeople() == false
														&& checkAsset() == false
														&& checkWork() == false
														&& checkSKUMaterial() == false
														&& checkWarehouse() == false
														&& checkEI40Demo() == false
														&& checkConfig() == false) {

													if ($scope.user.capabilityDetails.LSLG == 1) {
														_path = "/serverLog";
														$scope.pagename = "Server Log";
													}
													/*
													 * else if
													 * (($scope.user.capabilityDetails.LSLG ==
													 * undefined ||
													 * $scope.user.capabilityDetails.LSLG ==
													 * 0) &&
													 * $scope.user.capabilityDetails.LEMP ==
													 * 1) { _path =
													 * "/employeeProfile";
													 * $scope.pagename =
													 * "Employee Profile"; }
													 * else if
													 * (($scope.user.capabilityDetails.LSLG ==
													 * undefined ||
													 * $scope.user.capabilityDetails.LSLG ==
													 * 0) &&
													 * ($scope.user.capabilityDetails.LEMP ==
													 * undefined ||
													 * $scope.user.capabilityDetails.LEMP ==
													 * 0) &&
													 * $scope.user.capabilityDetails.LASP ==
													 * 1) { _path =
													 * "/assetProfile";
													 * $scope.pagename = "Asset
													 * Profile"; } else if
													 * (($scope.user.capabilityDetails.LSLG ==
													 * undefined ||
													 * $scope.user.capabilityDetails.LSLG ==
													 * 0) &&
													 * ($scope.user.capabilityDetails.LEMP ==
													 * undefined ||
													 * $scope.user.capabilityDetails.LEMP ==
													 * 0) &&
													 * ($scope.user.capabilityDetails.LASP ==
													 * undefined ||
													 * $scope.user.capabilityDetails.LASP ==
													 * 0) &&
													 * $scope.user.capabilityDetails.LVSP ==
													 * 1) { _path =
													 * "/visitorProfile";
													 * $scope.pagename =
													 * "Visitor Profile"; } else
													 * if
													 * (($scope.user.capabilityDetails.LSLG ==
													 * undefined ||
													 * $scope.user.capabilityDetails.LSLG ==
													 * 0) &&
													 * ($scope.user.capabilityDetails.LEMP ==
													 * undefined ||
													 * $scope.user.capabilityDetails.LEMP ==
													 * 0) &&
													 * ($scope.user.capabilityDetails.LASP ==
													 * undefined ||
													 * $scope.user.capabilityDetails.LASP ==
													 * 0) &&
													 * ($scope.user.capabilityDetails.LVSP ==
													 * undefined ||
													 * $scope.user.capabilityDetails.LVSP ==
													 * 0) &&
													 * $scope.user.capabilityDetails.LVHP ==
													 * 1) { _path =
													 * "/vehicleProfile";
													 * $scope.pagename =
													 * "Vehicle Profile"; } else
													 * if
													 * (($scope.user.capabilityDetails.LSLG ==
													 * undefined ||
													 * $scope.user.capabilityDetails.LSLG ==
													 * 0) &&
													 * ($scope.user.capabilityDetails.LEMP ==
													 * undefined ||
													 * $scope.user.capabilityDetails.LEMP ==
													 * 0) &&
													 * ($scope.user.capabilityDetails.LASP ==
													 * undefined ||
													 * $scope.user.capabilityDetails.LASP ==
													 * 0) &&
													 * ($scope.user.capabilityDetails.LVSP ==
													 * undefined ||
													 * $scope.user.capabilityDetails.LVSP ==
													 * 0) &&
													 * ($scope.user.capabilityDetails.LVHP ==
													 * undefined ||
													 * $scope.user.capabilityDetails.LVHP ==
													 * 0) &&
													 * $scope.user.capabilityDetails.LGNE ==
													 * 1) { _path = "/genie";
													 * $scope.pagename =
													 * "Genie"; }
													 */
												}

											}

										}

										// PeopleMenu
										if ($scope.user.capabilityDetails.PEVA == 1
												|| $scope.user.capabilityDetails.PEAD == 1
												|| $scope.user.capabilityDetails.PEED == 1) {
											$scope.pPEMP = 1;
										} else {
											$scope.pPEMP = 0;
										}
										if ($scope.user.capabilityDetails.PTAL == 1) {
											$scope.pPTAL = 1;
										} else {
											$scope.pPTAL = 0;
										}
										if ($scope.user.capabilityDetails.POTU == 1) {
											$scope.pPOTU = 1;
										} else {
											$scope.pPOTU = 0;
										}
										if ($scope.user.capabilityDetails.PPAR == 1) {
											$scope.pPPAR = 1;
										} else {
											$scope.pPPAR = 0;
										}
										if ($scope.user.capabilityDetails.PPFA == 1) {
											$scope.pPPFA = 1;
										} else {
											$scope.pPPFA = 0;
										}
										if ($scope.user.capabilityDetails.PLVW == 1) {
											$scope.pPLVW = 1;
										} else {
											$scope.pPLVW = 0;
										}

										// AssetMenu
										if ($scope.user.capabilityDetails.ATVA == 1
												|| $scope.user.capabilityDetails.ATAR == 1
												|| $scope.user.capabilityDetails.ATED == 1
												|| $scope.user.capabilityDetails.ATCL == 1
												|| $scope.user.capabilityDetails.ATRN == 1) {
											$scope.aTag = 1;
										} else {
											$scope.aTag = 0;
										}

										if ($scope.user.capabilityDetails.ANVA == 1
												|| $scope.user.capabilityDetails.ANAD == 1
												|| $scope.user.capabilityDetails.ANED == 1) {
											$scope.aNonC = 1;
										} else {
											$scope.aNonC = 0;
										}
										if ($scope.user.capabilityDetails.ACVA == 1
												|| $scope.user.capabilityDetails.ACAD == 1
												|| $scope.user.capabilityDetails.ACED == 1) {
											$scope.aChrono = 1;
										} else {
											$scope.aChrono = 0;
										}
										if ($scope.user.capabilityDetails.AAVH == 1) {
											$scope.aVehicle = 1;
										} else {
											$scope.aVehicle = 0;
										}
										if ($scope.user.capabilityDetails.ASTA == 1) {
											$scope.aTime = 1;
										} else {
											$scope.aTime = 0;
										}
										if ($scope.user.capabilityDetails.ACAL == 1) {
											$scope.aCali = 1;
										} else {
											$scope.aCali = 0;
										}
										if ($scope.user.capabilityDetails.ASLV == 1) {
											$scope.aLive = 1;
										} else {
											$scope.aLive = 0;
										}

										// WorkOrderMenu
										if ($scope.user.capabilityDetails.WTVA == 1
												|| $scope.user.capabilityDetails.WTAR == 1
												|| $scope.user.capabilityDetails.WTED == 1
												|| $scope.user.capabilityDetails.WTCL == 1
												|| $scope.user.capabilityDetails.WTRN == 1) {
											$scope.wTag = 1;
										} else {
											$scope.wTag = 0;
										}

										if ($scope.user.capabilityDetails.WMVA == 1
												|| $scope.user.capabilityDetails.WMAD == 1
												|| $scope.user.capabilityDetails.WMED == 1
												|| $scope.user.capabilityDetails.WMMA == 1) {
											$scope.wMAT = 1;
										} else {
											$scope.wMAT = 0;
										}

										if ($scope.user.capabilityDetails.WWVA == 1
												|| $scope.user.capabilityDetails.WWAD == 1
												|| $scope.user.capabilityDetails.WWED == 1) {
											$scope.wWWST = 1;
										} else {
											$scope.wWWST = 0;
										}

										if ($scope.user.capabilityDetails.WOFA == 1) {
											$scope.wWOFA = 1;
										} else {
											$scope.wWOFA = 0;
										}
										if ($scope.user.capabilityDetails.WZFA == 1) {
											$scope.wWZFA = 1;
										} else {
											$scope.wWZFA = 0;
										}
										if ($scope.user.capabilityDetails.WCAL == 1) {
											$scope.wWCAL = 1;
										} else {
											$scope.wWCAL = 0;
										}
										if ($scope.user.capabilityDetails.WCND == 1) {
											$scope.wWCND = 1;
										} else {
											$scope.wWCND = 0;
										}
										if ($scope.user.capabilityDetails.WACT == 1) {
											$scope.wWACT = 1;
										} else {
											$scope.wWACT = 0;
										}
										if ($scope.user.capabilityDetails.WLVW == 1) {
											$scope.wWLVW = 1;
										} else {
											$scope.wWLVW = 0;
										}
										// stock keeping unit
										if ($scope.user.capabilityDetails.STVA == 1
												|| $scope.user.capabilityDetails.STAR == 1
												|| $scope.user.capabilityDetails.STED == 1
												|| $scope.user.capabilityDetails.STRN == 1) {
											$scope.sSTAG = 1;
										} else {
											$scope.sSSTAG = 0;
										}

										if ($scope.user.capabilityDetails.SDET == 1 
												|| $scope.user.capabilityDetails.SIMP == 1 
												|| $scope.user.capabilityDetails.SDIS == 1) {
											$scope.sSKUM = 1;
										} else {
											$scope.sSKUM = 0;
										}

										if ($scope.user.capabilityDetails.SKLV == 1) {
											$scope.sSKULive = 1;
										} else {
											$scope.sSKULive = 0;
										}
										// warehouse  $scope.user.capabilityDetails.HTAR == 1 || 
										if ($scope.user.capabilityDetails.HTED == 1) {
											$scope.hTag = 1;
										} else {
											$scope.hTag = 0;
										}

										if ($scope.user.capabilityDetails.HMVA == 1
												|| $scope.user.capabilityDetails.HMDP == 1) {
											$scope.hWMAT = 1;
										} else {
											$scope.hWMAT = 0;
										}

										if ($scope.user.capabilityDetails.HKLV == 1) {
											$scope.hWLVW = 1;
										} else {
											$scope.hWLVW = 0;
										}
										if ($scope.user.capabilityDetails.DTAN == 1// Demo Time Analysis
												|| $scope.user.capabilityDetails.DPAR == 1 // Plant Arrival Rate
												|| $scope.user.capabilityDetails.DPFA == 1  // Plant Floor Analysis
												|| $scope.user.capabilityDetails.DWOA == 1  // Work Order Floor Analysis
												|| $scope.user.capabilityDetails.DZFA == 1 ) { // Work Station Flow Analysis
											$scope.hEI40Analytics = 1;
										} else {
											$scope.hEI40Analytics = 0;
										}
										if ($scope.user.capabilityDetails.DSFA == 1) {
											$scope.hDShopFloorAnalysis = 1;
										} else {
											$scope.hDShopFloorAnalysis = 0;
										}
										// ConfigurationMenu
										if ($scope.user.capabilityDetails.CORS == 1) {
											$scope.cCORS = 1;
										} else {
											$scope.cCORS = 0;
										}

										if ($scope.user.capabilityDetails.CTPE == 1) {
											$scope.cCTPE = 1;
										} else {
											$scope.cCTPE = 0;
										}
										if ($scope.user.capabilityDetails.CZOM == 1) {
											$scope.cCZOM = 1;
										} else {
											$scope.cCZOM = 0;
										}
										if ($scope.user.capabilityDetails.CLVW == 1) {
											$scope.cLIVE = 1;
										} else {
											$scope.cLIVE = 0;
										}
										if ($scope.user.capabilityDetails.CREG == 1) {
											$scope.cCREG = 1;
										} else {
											$scope.cCREG = 0;
										}
										if ($scope.user.capabilityDetails.CAMG == 1) {
											$scope.cCAMG = 1;
										} else {
											$scope.cCAMG = 0;
										}
										if ($scope.user.capabilityDetails.CSFT == 1) {
											$scope.cCSFT = 1;
										} else {
											$scope.cCSFT = 0;
										}
										if ($scope.user.capabilityDetails.CDPT == 1) {
											$scope.cCDPT = 1;
										} else {
											$scope.cCDPT = 0;
										}
										if ($scope.user.capabilityDetails.CCLA == 1) {
											$scope.cCCLA = 1;
										} else {
											$scope.cCCLA = 0;
										}
										if ($scope.user.capabilityDetails.CART == 1) {
											$scope.cCART = 1;
										} else {
											$scope.cCART = 0;
										}
										if ($scope.user.capabilityDetails.CTVA == 1
												|| $scope.user.capabilityDetails.CTRN == 1
												|| $scope.user.capabilityDetails.CTBI == 1) {
											$scope.cCTVR = 1;
										} else {
											$scope.cCTVR = 0;
										}

										// LOGMenu
										if ($scope.user.capabilityDetails.LSLG == 1) {
											$scope.lLSLG = 1;
										} else {
											$scope.lLSLG = 0;
										}
										/*
										 * if
										 * ($scope.user.capabilityDetails.LEMP ==
										 * 1) { $scope.lLEMP = 1; } else {
										 * $scope.lLEMP = 0; } if
										 * ($scope.user.capabilityDetails.LASP ==
										 * 1) { $scope.lLASP = 1; } else {
										 * $scope.lLASP = 0; } if
										 * ($scope.user.capabilityDetails.LVSP ==
										 * 1) { $scope.lLVSP = 1; } else {
										 * $scope.lLVSP = 0; } if
										 * ($scope.user.capabilityDetails.LVHP ==
										 * 1) { $scope.lLVHP = 1; } else {
										 * $scope.lLVHP = 0; } if
										 * ($scope.user.capabilityDetails.LGNE ==
										 * 1) { $scope.lLGNE = 1; } else {
										 * $scope.lLGNE = 0; }
										 */

										function checkPeople() {
											if ( /*
													 * ($scope.user.capabilityDetails.PTVA ==
													 * undefined ||
													 * $scope.user.capabilityDetails.PTVA ==
													 * 0)
													 */
											($scope.user.capabilityDetails.PTEA == undefined || $scope.user.capabilityDetails.PTEA == 0)
													&& ($scope.user.capabilityDetails.PTAR == undefined || $scope.user.capabilityDetails.PTAR == 0)
													&& ($scope.user.capabilityDetails.PTED == undefined || $scope.user.capabilityDetails.PTED == 0)
													/*
													 * &&
													 * ($scope.user.capabilityDetails.PTCL ==
													 * undefined ||
													 * $scope.user.capabilityDetails.PTCL ==
													 * 0) &&
													 * ($scope.user.capabilityDetails.PTRN ==
													 * undefined ||
													 * $scope.user.capabilityDetails.PTRN ==
													 * 0)
													 */
													&& ($scope.user.capabilityDetails.PEVA == undefined || $scope.user.capabilityDetails.PEVA == 0)
													&& ($scope.user.capabilityDetails.PEAD == undefined || $scope.user.capabilityDetails.PEAD == 0)
													&& ($scope.user.capabilityDetails.PEED == undefined || $scope.user.capabilityDetails.PEED == 0)
													&& ($scope.user.capabilityDetails.POTU == undefined || $scope.user.capabilityDetails.POTU == 0)
													&& ($scope.user.capabilityDetails.PTAL == undefined || $scope.user.capabilityDetails.PTAL == 0)
													&& ($scope.user.capabilityDetails.PPAR == undefined || $scope.user.capabilityDetails.PPAR == 0)
													&& ($scope.user.capabilityDetails.PPFA == undefined || $scope.user.capabilityDetails.PPFA == 0)
													&& ($scope.user.capabilityDetails.PLVW == undefined || $scope.user.capabilityDetails.PLVW == 0)) {
												$scope.pPEPL = 0;
												return false;
											} else {
												$scope.pPEPL = 1;
												return true;
											}
										}
										function checkPeopleTag() {
											if (/*
												 * $scope.user.capabilityDetails.PTVA ==
												 * 1
												 */
											$scope.user.capabilityDetails.PTEA == 1
													|| $scope.user.capabilityDetails.PTAR == 1
													|| $scope.user.capabilityDetails.PTED == 1
											/*
											 * ||
											 * $scope.user.capabilityDetails.PTCL ==
											 * 1 ||
											 * $scope.user.capabilityDetails.PTRN ==
											 * 1
											 */) {
												$scope.pTag = 1;
												return true;
											} else {
												$scope.pTag = 0;
												return false;
											}
										}
										function checkPeopleEmp() {
											if ($scope.user.capabilityDetails.PEVA == 1
													|| $scope.user.capabilityDetails.PEAD == 1
													|| $scope.user.capabilityDetails.PEED == 1) {
												$scope.pPEMP = 1;
												return true;
											} else {
												$scope.pPEMP = 0;
												return false;
											}
										}
										function checkAsset() {
											if (($scope.user.capabilityDetails.ATVA == undefined || $scope.user.capabilityDetails.ATVA == 0)
													&& ($scope.user.capabilityDetails.ATAR == undefined || $scope.user.capabilityDetails.ATAR == 0)
													&& ($scope.user.capabilityDetails.ATED == undefined || $scope.user.capabilityDetails.ATED == 0)
													&& ($scope.user.capabilityDetails.ATCL == undefined || $scope.user.capabilityDetails.ATCL == 0)
													&& ($scope.user.capabilityDetails.ATRN == undefined || $scope.user.capabilityDetails.ATRN == 0)
													&& ($scope.user.capabilityDetails.ANVA == undefined || $scope.user.capabilityDetails.ANVA == 0)
													&& ($scope.user.capabilityDetails.ANAD == undefined || $scope.user.capabilityDetails.ANAD == 0)
													&& ($scope.user.capabilityDetails.ANED == undefined || $scope.user.capabilityDetails.ANED == 0)
													&& ($scope.user.capabilityDetails.ANAR == undefined || $scope.user.capabilityDetails.ANAR == 0)
													&& ($scope.user.capabilityDetails.ACVA == undefined || $scope.user.capabilityDetails.ACVA == 0)
													&& ($scope.user.capabilityDetails.ACAD == undefined || $scope.user.capabilityDetails.ACAD == 0)
													&& ($scope.user.capabilityDetails.ACED == undefined || $scope.user.capabilityDetails.ACED == 0)
													&& ($scope.user.capabilityDetails.ACAR == undefined || $scope.user.capabilityDetails.ACAR == 0)
													&& ($scope.user.capabilityDetails.AAVH == undefined || $scope.user.capabilityDetails.AAVH == 0)
													&& ($scope.user.capabilityDetails.ASTA == undefined || $scope.user.capabilityDetails.ASTA == 0)
													&& ($scope.user.capabilityDetails.ACAL == undefined || $scope.user.capabilityDetails.ACAL == 0)
													&& ($scope.user.capabilityDetails.ASLV == undefined || $scope.user.capabilityDetails.ASLV == 0)) {
												$scope.aAsset = 0;
												return false;
											} else {
												$scope.aAsset = 1;
												return true;

											}
										}
										function checkAssetTag() {
											if ($scope.user.capabilityDetails.ATVA == 1
													|| $scope.user.capabilityDetails.ATAR == 1
													|| $scope.user.capabilityDetails.ATED == 1
													|| $scope.user.capabilityDetails.ATCL == 1
													|| $scope.user.capabilityDetails.ATRN == 1) {
												$scope.aTag = 1;
												return true;
											} else {
												$scope.aTag = 0;
												return false;
											}
										}
										function checkAssetNonChrono() {
											if ($scope.user.capabilityDetails.ANVA == 1
													|| $scope.user.capabilityDetails.ANAD == 1
													|| $scope.user.capabilityDetails.ANED == 1
													|| $scope.user.capabilityDetails.ANAR == 1) {
												return true;
											} else {
												return false;
											}
										}
										function checkAssetChrono() {
											if ($scope.user.capabilityDetails.ACVA == 1
													|| $scope.user.capabilityDetails.ACAD == 1
													|| $scope.user.capabilityDetails.ACED == 1
													|| $scope.user.capabilityDetails.ACAR == 1) {
												return true;
											} else {
												return false;
											}
										}

										// CheckSKUMaterial
										function checkSKUMaterial() {
											if (($scope.user.capabilityDetails.STVA == undefined || $scope.user.capabilityDetails.STVA == 0)
													&& ($scope.user.capabilityDetails.STAR == undefined || $scope.user.capabilityDetails.STAR == 0)
													&& ($scope.user.capabilityDetails.STED == undefined || $scope.user.capabilityDetails.STED == 0)
													&& ($scope.user.capabilityDetails.STRN == undefined || $scope.user.capabilityDetails.STRN == 0)
													&& ($scope.user.capabilityDetails.SDET == undefined || $scope.user.capabilityDetails.SDET == 0)
													&& ($scope.user.capabilityDetails.SIMP == undefined || $scope.user.capabilityDetails.SIMP == 0)
													&& ($scope.user.capabilityDetails.SDIS == undefined || $scope.user.capabilityDetails.SDIS == 0)
													&& ($scope.user.capabilityDetails.SKLV == undefined || $scope.user.capabilityDetails.SKLV == 0)) {
												$scope.sMSKU = 0;
												return false;
											} else {
												$scope.sMSKU = 1;
												return true;

											}
										}
										function checSKUTag() {
											if ($scope.user.capabilityDetails.STVA == 1
													|| $scope.user.capabilityDetails.STAR == 1
													|| $scope.user.capabilityDetails.STED == 1
													|| $scope.user.capabilityDetails.STRN == 1) {
												$scope.sSTAG = 1;
												return true;
											} else {
												$scope.sSTAG = 0;
												return false;
											}
										}
										// Check Warehouse
										function checkWarehouse() {
											if (($scope.user.capabilityDetails.HTAR == undefined || $scope.user.capabilityDetails.HTAR == 0)
													&& ($scope.user.capabilityDetails.HTED == undefined || $scope.user.capabilityDetails.HTED == 0)
													&& ($scope.user.capabilityDetails.HMVA == undefined || $scope.user.capabilityDetails.HMVA == 0)
													&& ($scope.user.capabilityDetails.HMDP == undefined || $scope.user.capabilityDetails.HMDP == 0)
													&& ($scope.user.capabilityDetails.HKLV == undefined || $scope.user.capabilityDetails.HKLV == 0)) {
												$scope.hWARE = 0;
												return false;
											} else {
												$scope.hWARE = 1;
												return true;

											}
										}
										// Check Warehouse
										function checkEI40Demo() {
											if (($scope.user.capabilityDetails.DANA == undefined || $scope.user.capabilityDetails.DANA == 0)
													&& ($scope.user.capabilityDetails.DTAN == undefined || $scope.user.capabilityDetails.DDWT == 0) // Demo Time Analysis
													&& ($scope.user.capabilityDetails.DPAR == undefined || $scope.user.capabilityDetails.DPAR == 0) // Plant Arrival Rate
													&& ($scope.user.capabilityDetails.DPFA == undefined || $scope.user.capabilityDetails.DPFA == 0) // Plant Floor Analysis
													&& ($scope.user.capabilityDetails.DWOA == undefined || $scope.user.capabilityDetails.DWOA == 0) // Work Order Floor Analysis
											&& ($scope.user.capabilityDetails.DZFA == undefined || $scope.user.capabilityDetails.DZFA == 0) // Work Station Flow Analysis
													&& ($scope.user.capabilityDetails.DSFA == undefined || $scope.user.capabilityDetails.DSFA == 0)) {
												$scope.hEI40Demo = 0;
												return false;
											} else {
												$scope.hEI40Demo = 1;
												return true;

											}
										}
										function checkEI40Analytics() {
											if (($scope.user.capabilityDetails.DTAN == undefined || $scope.user.capabilityDetails.DDWT == 0) // Demo Time Analysis
													&& ($scope.user.capabilityDetails.DPAR == undefined || $scope.user.capabilityDetails.DPAR == 0) // Plant Arrival Rate
													&& ($scope.user.capabilityDetails.DPFA == undefined || $scope.user.capabilityDetails.DPFA == 0) // Plant Floor Analysis
													&& ($scope.user.capabilityDetails.DWOA == undefined || $scope.user.capabilityDetails.DWOA == 0) // Work Order Floor Analysis
													&& ($scope.user.capabilityDetails.DZFA == undefined || $scope.user.capabilityDetails.DZFA == 0) ) { // Work Station Flow Analysis
												$scope.hEI40Analytics = 0;
												return false;
											} else {
												$scope.hEI40Analytics = 1;
												return true;
											}
										}
										
										function checkWarehouseTag() {
											
											//$scope.user.capabilityDetails.HTAR == 1 || 
											if ($scope.user.capabilityDetails.HTED == 1) {
												$scope.hTag = 1;
												return true;
											} else {
												$scope.hTag = 0;
												return false;
											}
										}

										// CheckWork
										function checkWork() {
											if (($scope.user.capabilityDetails.WTVA == undefined || $scope.user.capabilityDetails.WTVA == 0)
													&& ($scope.user.capabilityDetails.WTAR == undefined || $scope.user.capabilityDetails.WTAR == 0)
													&& ($scope.user.capabilityDetails.WTED == undefined || $scope.user.capabilityDetails.WTED == 0)
													&& ($scope.user.capabilityDetails.WTCL == undefined || $scope.user.capabilityDetails.WTCL == 0)
													&& ($scope.user.capabilityDetails.WTRN == undefined || $scope.user.capabilityDetails.WTRN == 0)
													&& ($scope.user.capabilityDetails.WMVA == undefined || $scope.user.capabilityDetails.WMVA == 0)
													&& ($scope.user.capabilityDetails.WMAD == undefined || $scope.user.capabilityDetails.WMAD == 0)
													&& ($scope.user.capabilityDetails.WMED == undefined || $scope.user.capabilityDetails.WMED == 0)
													&& ($scope.user.capabilityDetails.WWVA == undefined || $scope.user.capabilityDetails.WWVA == 0)
													&& ($scope.user.capabilityDetails.WWAD == undefined || $scope.user.capabilityDetails.WWAD == 0)
													&& ($scope.user.capabilityDetails.WWED == undefined || $scope.user.capabilityDetails.WWED == 0)
													&& ($scope.user.capabilityDetails.WOFA == undefined || $scope.user.capabilityDetails.WOFA == 0)
													&& ($scope.user.capabilityDetails.WZFA == undefined || $scope.user.capabilityDetails.WZFA == 0)
													&& ($scope.user.capabilityDetails.WCAL == undefined || $scope.user.capabilityDetails.WCAL == 0)
													&& ($scope.user.capabilityDetails.WCND == undefined || $scope.user.capabilityDetails.WCND == 0)
													&& ($scope.user.capabilityDetails.WACT == undefined || $scope.user.capabilityDetails.WACT == 0)
													&& ($scope.user.capabilityDetails.WLVW == undefined || $scope.user.capabilityDetails.WLVW == 0)) {
												$scope.wOrder = 0;
												return false;
											} else {
												$scope.wOrder = 1;
												return true;

											}
										}
										function checkWorkTag() {
											if ($scope.user.capabilityDetails.WTVA == 1
													|| $scope.user.capabilityDetails.WTAR == 1
													|| $scope.user.capabilityDetails.WTED == 1
													|| $scope.user.capabilityDetails.WTCL == 1
													|| $scope.user.capabilityDetails.WTRN == 1) {
												$scope.wTag = 1;
												return true;
											} else {
												$scope.wTag = 0;
												return false;
											}
										}
										function checkWorkMaterial() {
											if ($scope.user.capabilityDetails.WMVA == 1
													|| $scope.user.capabilityDetails.WMAD == 1
													|| $scope.user.capabilityDetails.WMED == 1) {
												return true;
											} else {
												return false;
											}
										}
										function checkWorkStation() {
											if ($scope.user.capabilityDetails.WWVA == 1
													|| $scope.user.capabilityDetails.WWAD == 1
													|| $scope.user.capabilityDetails.WWED == 1) {
												return true;
											} else {
												return false;
											}
										}

										function checkConfig() {
											if (($scope.user.capabilityDetails.CORS == undefined || $scope.user.capabilityDetails.CORS == 0)
													&& ($scope.user.capabilityDetails.CTPE == undefined || $scope.user.capabilityDetails.CTPE == 0)
													&& ($scope.user.capabilityDetails.CZOM == undefined || $scope.user.capabilityDetails.CZOM == 0)
													&& ($scope.user.capabilityDetails.CLVW == undefined || $scope.user.capabilityDetails.CLVW == 0)
													&& ($scope.user.capabilityDetails.CREG == undefined || $scope.user.capabilityDetails.CREG == 0)
													&& ($scope.user.capabilityDetails.CAMG == undefined || $scope.user.capabilityDetails.CAMG == 0)
													&& ($scope.user.capabilityDetails.CDPT == undefined || $scope.user.capabilityDetails.CDPT == 0)
													&& ($scope.user.capabilityDetails.CCLA == undefined || $scope.user.capabilityDetails.CCLA == 0)
													&& ($scope.user.capabilityDetails.CART == undefined || $scope.user.capabilityDetails.CART == 0)
													&& ($scope.user.capabilityDetails.CTVR == undefined || $scope.user.capabilityDetails.CTVR == 0)) {
												$scope.cConfig = 0;
												return false;
											} else {
												$scope.cConfig = 1;
												return true;

											}
										}
										function checkLog() {
											if (($scope.user.capabilityDetails.LSLG == undefined || $scope.user.capabilityDetails.LSLG == 0)
											/*
											 * &&
											 * ($scope.user.capabilityDetails.LEMP ==
											 * undefined ||
											 * $scope.user.capabilityDetails.LEMP ==
											 * 0) &&
											 * ($scope.user.capabilityDetails.LASP ==
											 * undefined ||
											 * $scope.user.capabilityDetails.LASP ==
											 * 0) &&
											 * ($scope.user.capabilityDetails.LVSP ==
											 * undefined ||
											 * $scope.user.capabilityDetails.LVSP ==
											 * 0) &&
											 * ($scope.user.capabilityDetails.LVHP ==
											 * undefined ||
											 * $scope.user.capabilityDetails.LVHP ==
											 * 0) &&
											 * ($scope.user.capabilityDetails.LGNE ==
											 * undefined ||
											 * $scope.user.capabilityDetails.LGNE ==
											 * 0)
											 */
											) {
												$scope.lLog = 0;
												return false;
											} else {
												$scope.lLog = 1;
												return true;

											}
										}
										$rootScope.pageName = $scope.pagename;
										$location.path(_path);
										Idle.watch();

										$timeout(function() {
											$rootScope.$broadcast(
													'updateProfileColor',
													$scope.user.profileColor,
													$scope.showNotification);

										}, 1000);
									});
					$rootScope.checkNetconnection = function() {
						return $window.navigator.onLine;
					}
					$rootScope.alertDialogChecknet = function() {
						$.alert({
							title : $rootScope.MSGBOX_TITLE,
							type : 'blue',
							content : 'Please check the Internet Connection',
							icon : 'fa fa-info-circle',
						});
					};
					
					$rootScope.exportXLSPDFFileName = function(fileName) {
						$rootScope.export_file_name ="";
						$rootScope.exportFileName = fileName;
						var export_date = new Date();
						var exportDD = export_date
								.getDate();
						var exportMM = export_date
								.getMonth() + 1;
						var exportYYYY = export_date
								.getFullYear();

						if (exportDD.length < 2) {
							exportDD = "0" + exportDD;
						}
						if (exportMM.length < 2) {
							exportMM = "0" + exportMM;
						} 
						
						$rootScope.exportDate = moment(export_date).format("MMM DD, YYYY")		
						var exportHour = export_date.getHours();
						var exportMinutes = export_date.getMinutes();
						if (exportMinutes < 10) {
							exportMinutes = "0"
									+ exportMinutes;
						}
						var exportAmPm = "AM";
						if (exportHour > 12) { 
							exportHour -= 12;
							exportAmPm = "PM";
						} else if (exportHour === 12) {
							exportAmPm = "PM";
						} else if (exportHour === 0) {
							exportHour += 12;
						}

						$rootScope.exportTime1 = exportHour + ":" + exportMinutes + " " + exportAmPm;
						if($rootScope.exportExcelDataFlag){
							$rootScope.exportExcelDataFlag = false;
							$rootScope.export_file_name = $rootScope.exportFileName+ "_"+ $rootScope.exportDate + "_"	+ $rootScope.exportTime1 + ".xlsx";
						}else{
							$rootScope.export_file_name = $rootScope.exportFileName+ "_"+ $rootScope.exportDate + "_"	+ $rootScope.exportTime1 + ".pdf";
						}
						
						
					};
					
					$rootScope.exportAsExcelFn= function(exportData,headerData){
						var ws = XLSX.utils.json_to_sheet(exportData);	
						var wscols = [
						    {wch:20},
						    {wch:10}, 
						    {wch:10},
						    {wch:10},
						    {wch:10},
						    {wch:10},
						    {wch:10},
						];
						var ws1 = XLSX.utils.json_to_sheet(headerData);
						var wscols1 = [
						    {wch:30},
						    {wch:35},   
						];
						ws['!cols'] = wscols;
						ws1['!cols'] = wscols1;
						var wb = XLSX.utils.book_new();
						var xlsxHeading = $rootScope.exportFileName.replace(/_/g, " ");
						XLSX.utils.book_append_sheet(wb, ws,xlsxHeading);		
						XLSX.utils.book_append_sheet(wb, ws1, "Export Details");		
						XLSX.writeFile(wb,$rootScope.export_file_name);	
					};
					
					$rootScope.GetSortOrderByDate = function(prop,ascending){
					    return function(a, b) {
					    	 
					    	
					        if (a[prop] === b[prop])
			    			{
				    			 return 0;
			    			}else if (a[prop]=='')
			    			{
				    			 return 1;
			    			}else if (b[prop]=='')
			    			{
				    			 return -1;
			    			}else
			    			{
			    				var a1 = moment(a[prop], 'MMM DD, YYYY');
			    				var b1 = moment(b[prop], 'MMM DD, YYYY');
			    				
			    				 if (ascending) {
							            return a1 < b1 ? -1 : 1;
							        }
							        // if descending, highest sorts first
							        else { 
							            return a1 < b1 ? 1 : -1;
							        }
			    				
			    			}	 
					        
					    }
					}
					$rootScope.exportPDFfn = function(col,rw, expiryDateIndex){
						var columns =[], rows =[];
						columns = col;
						rows = rw;
						var pdfHeading = $rootScope.exportFileName.replace(/_/g, " ");
						var doc = new jsPDF('landscape');
						doc.setFontSize(12);
						doc.textColor = [255,100,150];
						doc.text(115, 10, pdfHeading);
						doc.setLineWidth(0.1);
						doc.line(14, 13, 281, 13);
						
						doc.setFontSize(11);
						doc.text(20, 20, "Report Generated By : " + $scope.user.userName);
						doc.setFontSize(11);
						doc.text(20, 25,"Report Generated Date & Time : " + $rootScope.exportDate + ", "+ $rootScope.exportTime1);
						if($rootScope.SKUPDFDownloadFlag){
							$rootScope.SKUPDFDownloadFlag = false;
							if($rootScope.allDropDownSet){
								$rootScope.allDropDownSet = false;
								doc.setFontSize(11);
								doc.text(20, 30,"Reported Data : " + "All Data" );
							}
							else if($rootScope.LslmDropDownSet){
								$rootScope.LslmDropDownSet
								doc.setFontSize(11);
								doc.text(20, 30,"Reported Data : " + "LSLM Only" );
							}
							
						doc.setLineWidth(0.1);
						doc.line(14, 33, 281, 33);
						doc.autoTable({startY: 35});
						doc.autoTable(columns,rows,{
							columnStyles: {
								 0: {
									 cellWidth: 13
						         },
						         1: {
						        	 cellWidth: 'auto'
						         },
						         2: {
						        	 cellWidth: 'auto'
						         },
						         3: {
						        	 cellWidth: 'auto'
						         },
						         4: {
						        	 cellWidth: 80
						         },
						         5: {
						        	 cellWidth: 'auto'
						         },
						         6: {
						        	 cellWidth: 'auto'
						         },
						         7: {
						        	 cellWidth: 'auto'
						         },
						         8: {
						        	 cellWidth: 25
						         }
							},didParseCell(data) {
						    	if (data.row.raw[expiryDateIndex] !="" && data.row.raw[expiryDateIndex] !="Expiry Date"){
							    	var validityDate =  moment(data.row.raw[expiryDateIndex],"MMM DD, YYYY").format("YYYY-MMM-DD");
							    	if (moment(validityDate, 'YYYY-MMM-DD') < moment(moment(new Date(), 'YYYY-MMM-DD').format('YYYY-MMM-DD'), 'YYYY-MMM-DD')){
							    		data.cell.styles.textColor = [255, 0, 0]; //red color
							    	}else if (moment(validityDate, 'YYYY-MMM-DD') > moment(moment(new Date(), 'YYYY-MMM-DD').format('YYYY-MMM-DD'), 'YYYY-MMM-DD')){
							    		data.cell.styles.textColor = [0, 128, 0]; // green color
							    	}
							    	else{
							    		data.cell.styles.textColor = [0, 0, 255]; //blue color
							    	}
						    	}
							}
						
						});
						}else{
							doc.setLineWidth(0.1);
							doc.line(14, 33, 281, 33);
							doc.autoTable({startY: 35});
							doc.autoTable(columns,rows,{
								columnStyles: {
									 0: {
										 cellWidth: 12
							         },
							         1: {
							        	 cellWidth: 23
							         },
							         2: {
							        	 cellWidth: 22
							         },
							         3: {
							        	 cellWidth: 27
							         },
							         4: {
							        	 cellWidth: 20
							         },
							         5: {
							        	 cellWidth: 30
							         },
							         6: {
							        	 cellWidth: 45
							         },
							         7: {
							        	 cellWidth: 25
							         },
							         8: {
							        	 cellWidth: 21
							         },
							         9: {
							        	 cellWidth: 25
							         },
							         10: {
							        	 cellWidth: 25
							         }
								},didParseCell(data) {
							    	if (data.row.raw[expiryDateIndex] !="" && data.row.raw[expiryDateIndex] !="Expiry Date"){
								    	var validityDate =  moment(data.row.raw[expiryDateIndex],"MMM DD, YYYY").format("YYYY-MMM-DD");
								    	if (moment(validityDate, 'YYYY-MMM-DD') < moment(moment(new Date(), 'YYYY-MMM-DD').format('YYYY-MMM-DD'), 'YYYY-MMM-DD')){
								    		data.cell.styles.textColor = [255, 0, 0]; //red color
								    	}else if (moment(validityDate, 'YYYY-MMM-DD') > moment(moment(new Date(), 'YYYY-MMM-DD').format('YYYY-MMM-DD'), 'YYYY-MMM-DD')){
								    		data.cell.styles.textColor = [0, 128, 0]; // green color
								    	}
								    	else{
								    		data.cell.styles.textColor = [0, 0, 255]; //blue color
								    	}
							    	}
								}
							});
						}
						
						// PAGE NUMBERING
						// Add Page number at bottom-right
						// Get the number of pages
						var pageCount = doc.internal.getNumberOfPages(); //Total Page Number
						for(i = 0; i < pageCount; i++) { 
						  doc.setPage(i); 
						  let pageCurrent = doc.internal.getCurrentPageInfo().pageNumber; //Current Page
						  doc.setFontSize(11);
						  doc.text('Page: ' + pageCurrent + '/' + pageCount, 150-20,230-30,null,null,"right");
						}
						
						doc.save($rootScope.export_file_name);
						
					};
					
					
					
					
					$scope.about = function() {
						$('#about_modal').modal('show');
					}

					if ($location.$$path.split("/")[1] == 'liveviewmobile') {
						$scope.navBarHide = true;
					} else {
						$scope.navBarHide = false;
					}

					$rootScope.showloading = function(id, msg) {

						if (msg == undefined || msg == null || msg == '')
							msg = 'Loading, please wait...';

						$(id).waitMe({

							// none, rotateplane, stretch, orbit, roundBounce,
							// win8,
							// win8_linear, ios, facebook, rotation, timer,
							// pulse,
							// progressBar, bouncePulse or img
							effect : 'bounce',

							// place text under the effect (string).
							text : msg,

							// background for container (string).
							bg : 'rgba(255,255,255,0.5)',

							// color for background animation and text (string).
							color : 'rgba(0, 16, 64, 0.95)',

							// max size
							maxSize : '',

							// wait time im ms to close
							waitTime : -1,

							// url to image
							source : '',

							// or 'horizontal'
							textPos : 'vertical',

							// font size
							fontSize : '20px',

							// callback
							onClose : function() {
							}

						});
					}

					$rootScope.hideloading = function(id) {
						$(id).waitMe('hide');
					};

					$rootScope.sessionTimeoutDialog = function() {
						$scope.fnLogoutClick();
						$rootScope.showAlertDialog('Session timed-out or Invalid. Please login again');
						/*
						 * $.alert({ title : $rootScope.MSGBOX_TITLE, content :
						 * 'Session timed-out or Invalid. Please logout and
						 * login again', type : 'blue', columnClass : 'small',
						 * autoClose : false, icon : 'fa fa-info-circle', });
						 */
					};
					
					$rootScope.outsideLoginTimeoutDialog = function() {					
						$rootScope.showAlertDialog('Time out. Please Contact Your Administrator.');						
					};
					
					$rootScope.alertDialog = function(msg) {
						$.alert({
							title : $rootScope.MSGBOX_TITLE,
							content : msg,
							type : 'blue',
							columnClass : 'small',
							autoClose : false,
							icon : 'fa fa-info-circle',
						});
					};

					$rootScope.alertErrorDialog = function(msg) {
						$.alert({
							title : $rootScope.MSGBOX_TITLE,
							content : msg,
							type : 'red',
							columnClass : 'small',
							autoClose : false,
							icon : 'fa fa-exclamation-triangle',
						});
					};

					function closeModals() {
						if ($scope.warning) {
							$scope.warning.close();
							$scope.warning = null;
						}

						if ($scope.timedout) {
							$scope.timedout.close();
							$scope.timedout = null;
						}
					}

					$rootScope.showAlertDialog = function(message) {
						$.alert({
							title : $rootScope.MSGBOX_TITLE,
							content : message,
							type : 'blue',
							columnClass : 'small',
							autoClose : false,
							icon : 'fa fa-info-circle',
						});
					};

					$scope
							.$on(
									'IdleStart',
									function() {
										if (($location.$$path != "/changePassword")
												&& ($location.$$path != '/login')
												&& $location.$$path != '/forgotPassword') {
											// $scope.logout();
											$scope.fnLogoutClick();
											$rootScope
													.showAlertDialog('Your session timeout, Please login again');

											/*
											 * $timeout(function() {
											 * $window.sessionStorage.setItem("sessionDialog",
											 * true); }, 1000);
											 */
										}

									});

					$scope.$on('IdleEnd', function() {
					});

					$scope.$on('IdleTimeout', function() {
					});

					window.onbeforeunload = function() {
						// $scope.logout();
						$scope.fnLogoutClick();

					}
					$(document).ready(
							function() {
								window.history.pushState(null, "",
										window.location.href);
								window.onpopstate = function() {
									window.history.pushState(null, "",
											window.location.href);
								};

							});

					$scope.naviDashboardLink = function() {
						$("#highlightIdMenuDashboard").addClass("active");
						$("#highlightIdMenuPeople").removeClass("active");
						$("#highlightIdMenuAsset").removeClass("active");
						$("#highlightIdMenuWorkOrder").removeClass("active");
						$("#highlightIdMenuConfiguration")
								.removeClass("active");
						$("#highlightIdMenuLog").removeClass("active");
						$("#highlightIdMenuSKUMaterial").removeClass("active");
						$("#highlightIdMenuWarehouse").removeClass("active");
						$("#highlightIdMenuEI40Demo").removeClass("active");
						$scope.menuLink('dashboard', 'Dashboard');
					}
					$scope.naviPeopleLink = function() {
						$("#highlightIdMenuDashboard").removeClass("active");
						$("#highlightIdMenuPeople").addClass("active");
						$("#highlightIdMenuAsset").removeClass("active");
						$("#highlightIdMenuWorkOrder").removeClass("active");
						$("#highlightIdMenuConfiguration")
								.removeClass("active");
						$("#highlightIdMenuLog").removeClass("active");
						$("#highlightIdMenuSKUMaterial").removeClass("active");
						$("#highlightIdMenuWarehouse").removeClass("active");
						$("#highlightIdMenuEI40Demo").removeClass("active");
						var peopleFlag = 0;
						var classList = document
								.getElementById("toggleIdMenuPeople").classList;
						for (var i = 0; i < classList.length; i++) {
							if (classList[i] == "menu-is-opening") {
								peopleFlag = 1;
								break;
							} else
								peopleFlag = 0;
						}
						if (peopleFlag == 1 && $scope.menuLinkClick == false) {
							$("#idMenuPeople svg").removeClass("fa-angle-down").addClass("fa-angle-left");
 							$("#toggleIdMenuPeople").removeClass("menu-is-opening menu-open"); 
 							$("#idPeopleUL").attr("style", "display:none");
 							peopleFlag = 0; 

 
						} else {

							$scope.menuLinkClick = false;
							$("#idPeopleUL").css($scope.displayBlock);
							$("#toggleIdMenuPeople").addClass("menu-is-opening menu-open");
							$("#idMenuPeople svg").removeClass("fa-angle-left")
									.addClass("fa-angle-down");

							$("#idAssetUL").css($scope.displayNone); 
							$("#toggleIdMenuAsset").removeClass(
									"menu-is-opening menu-open");
							$("#idMenuAsset svg").removeClass("fa-angle-down")
									.addClass("fa-angle-left");

							$("#idWorkOrderUL").css($scope.displayNone);
							$("#toggleIdMenuWorkOrder").removeClass(
									"menu-is-opening menu-open");
							$("#idMenuWorkOrder svg").removeClass(
									"fa-angle-down").addClass("fa-angle-left");

							$("#idSKUUL").css($scope.displayNone);
							$("#toggleIdMenuSKUMaterial").removeClass(
									"menu-is-opening menu-open");
							$("#idMenuSKUMaterial svg").removeClass(
									"fa-angle-down").addClass("fa-angle-left");

							$("#idWarehouseUL").css($scope.displayNone);
							$("#toggleIdMenuWarehouse").removeClass(
									"menu-is-opening menu-open");
							$("#idMenuWarehouse svg").removeClass(
									"fa-angle-down").addClass("fa-angle-left");

 							$("#idEI40DemoUL").css($scope.displayNone);
							$("#toggleIdMenuEI40Demo").removeClass(
									"menu-is-opening menu-open");
							$("#idMenuEI40Demo svg").removeClass(
									"fa-angle-down").addClass("fa-angle-left"); 							


							$("#idConfigurationUL").css($scope.displayNone);
							$("#toggleIdMenuConfiguration").removeClass(
									"menu-is-opening menu-open");
							$("#idMenuConfiguration svg").removeClass(
									"fa-angle-down").addClass("fa-angle-left");

							$("#idLogUL").css($scope.displayNone);
							$("#toggleIdMenuLog").removeClass(
									"menu-is-opening menu-open");
							$("#idMenuLog svg").removeClass("fa-angle-down")
									.addClass("fa-angle-left");
						}
					}
					$scope.naviAssetLink = function() { 
						$("#highlightIdMenuDashboard").removeClass("active");
						$("#highlightIdMenuPeople").removeClass("active");
						$("#highlightIdMenuAsset").addClass("active");
						$("#highlightIdMenuWorkOrder").removeClass("active");
						$("#highlightIdMenuConfiguration")
								.removeClass("active");
						$("#highlightIdMenuLog").removeClass("active");
						$("#highlightIdMenuSKUMaterial").removeClass("active");
						$("#highlightIdMenuWarehouse").removeClass("active");
						$("#highlightIdMenuEI40Demo").removeClass("active");
						var assetFlag = 0;
						var classList = document
								.getElementById("toggleIdMenuAsset").classList;
						for (var i = 0; i < classList.length; i++) {
							if (classList[i] == "menu-is-opening") {
								assetFlag = 1;
								break;
							} else
								assetFlag = 0;
						}
						if (assetFlag == 1 && $scope.menuLinkClick == false) {
							$("#idMenuAsset svg").removeClass("fa-angle-down")
									.addClass("fa-angle-left");
 							$("#toggleIdMenuAsset").removeClass("menu-is-opening menu-open"); 
 							$("#idAssetUL").attr("style", "display:none");
 							assetFlag = 0; 

						} else {

							$scope.menuLinkClick = false;
							$("#idPeopleUL").css($scope.displayNone);
							$("#toggleIdMenuPeople").removeClass("menu-is-opening menu-open");
							$("#idMenuPeople svg").removeClass("fa-angle-down")
									.addClass("fa-angle-left");

							$("#idAssetUL").css($scope.displayBlock);
							$("#toggleIdMenuAsset").addClass("menu-is-opening menu-open");
							$("#idMenuAsset svg").removeClass("fa-angle-left")
									.addClass("fa-angle-down");

							$("#idWorkOrderUL").css($scope.displayNone);
							$("#toggleIdMenuWorkOrder").removeClass(
									"menu-is-opening menu-open");
							$("#idMenuWorkOrder svg").removeClass(
									"fa-angle-down").addClass("fa-angle-left");

							$("#idSKUUL").css($scope.displayNone);
							$("#toggleIdMenuSKUMaterial").removeClass(
									"menu-is-opening menu-open");
							$("#idMenuSKUMaterial svg").removeClass(
									"fa-angle-down").addClass("fa-angle-left");

							$("#idWarehouseUL").css($scope.displayNone);
							$("#toggleIdMenuWarehouse").removeClass(
									"menu-is-opening menu-open");
							$("#idMenuWarehouse svg").removeClass(
									"fa-angle-down").addClass("fa-angle-left");

 							$("#idEI40DemoUL").css($scope.displayNone);
							$("#toggleIdMenuEI40Demo").removeClass(
									"menu-is-opening menu-open");
							$("#idMenuEI40Demo svg").removeClass(
									"fa-angle-down").addClass("fa-angle-left"); 							


							$("#idConfigurationUL").css($scope.displayNone);
							$("#toggleIdMenuConfiguration").removeClass(
									"menu-is-opening menu-open");
							$("#idMenuConfiguration svg").removeClass(
									"fa-angle-down").addClass("fa-angle-left");

							$("#idLogUL").css($scope.displayNone);
							$("#toggleIdMenuLog").removeClass(
									"menu-is-opening menu-open");
							$("#idMenuLog svg").removeClass("fa-angle-down")
									.addClass("fa-angle-left");
						}

					}

					$rootScope.setPageName = function(pName) {
						$scope.pagename = pName;
						$scope.setPageHeaderColor();
					}

					$rootScope.getPageName = function() {
						return $scope.pagename;
					}

					$scope.naviWorkOrderLink = function() {
						$("#highlightIdMenuDashboard").removeClass("active");
						$("#highlightIdMenuPeople").removeClass("active");
						$("#highlightIdMenuAsset").removeClass("active");
						$("#highlightIdMenuWorkOrder").addClass("active");
						$("#highlightIdMenuSKUMaterial").removeClass("active");
						$("#highlightIdMenuWarehouse").removeClass("active");
						$("#highlightIdMenuEI40Demo").removeClass("active");
						$("#highlightIdMenuConfiguration")
								.removeClass("active");
						$("#highlightIdMenuLog").removeClass("active");
						var workorderFlag = 0;
						var classList = document
								.getElementById("toggleIdMenuWorkOrder").classList;
						for (var i = 0; i < classList.length; i++) {
							if (classList[i] == "menu-is-opening") {
								workorderFlag = 1;
								break;
							} else
								workorderFlag = 0;
						}
						if (workorderFlag == 1 && $scope.menuLinkClick == false) {
							$("#idMenuWorkOrder svg").removeClass(
									"fa-angle-down").addClass("fa-angle-left");
  							$("#toggleIdMenuWorkOrder").removeClass("menu-is-opening menu-open"); 
  							$("#idWorkOrderUL").attr("style", "display:none");
  							workorderFlag = 0; 
 
						} else {

							$scope.menuLinkClick = false;
							$("#idPeopleUL").css($scope.displayNone);
							$("#toggleIdMenuPeople").removeClass("menu-is-opening menu-open");
							$("#idMenuPeople svg").removeClass("fa-angle-down")
									.addClass("fa-angle-left");

							$("#idAssetUL").css($scope.displayNone);
							$("#toggleIdMenuAsset").removeClass(
									"menu-is-opening menu-open");
							$("#idMenuAsset svg").removeClass("fa-angle-down") 
									.addClass("fa-angle-left");

							$("#idWorkOrderUL").css($scope.displayBlock);
							$("#toggleIdMenuWorkOrder").addClass("menu-is-opening menu-open"); 
							$("#idMenuWorkOrder svg").removeClass(
									"fa-angle-left").addClass("fa-angle-down");

							$("#idSKUUL").css($scope.displayNone);
							$("#toggleIdMenuSKUMaterial").removeClass(
									"menu-is-opening menu-open");
							$("#idMenuSKUMaterial svg").removeClass(
									"fa-angle-down").addClass("fa-angle-left");

							$("#idWarehouseUL").css($scope.displayNone);
							$("#toggleIdMenuWarehouse").removeClass(
									"menu-is-opening menu-open");
							$("#idMenuWarehouse svg").removeClass(
									"fa-angle-down").addClass("fa-angle-left");

 							$("#idEI40DemoUL").css($scope.displayNone);
							$("#toggleIdMenuEI40Demo").removeClass(
									"menu-is-opening menu-open");
							$("#idMenuEI40Demo svg").removeClass(
									"fa-angle-down").addClass("fa-angle-left"); 							


							$("#idConfigurationUL").css($scope.displayNone);
							$("#toggleIdMenuConfiguration").removeClass(
									"menu-is-opening menu-open");
							$("#idMenuConfiguration svg").removeClass(
									"fa-angle-down").addClass("fa-angle-left");

							$("#idLogUL").css($scope.displayNone);
							$("#toggleIdMenuLog").removeClass(
									"menu-is-opening menu-open");
							$("#idMenuLog svg").removeClass("fa-angle-down")
									.addClass("fa-angle-left");
						}

					}
					$scope.naviSKUMaterialLink = function() {
						$("#highlightIdMenuDashboard").removeClass("active");
						$("#highlightIdMenuPeople").removeClass("active");
						$("#highlightIdMenuAsset").removeClass("active");
						$("#highlightIdMenuWorkOrder").removeClass("active");
						$("#highlightIdMenuSKUMaterial").addClass("active");
						$("#highlightIdMenuWarehouse").removeClass("active");
						$("#highlightIdMenuEI40Demo").removeClass("active");
						$("#highlightIdMenuConfiguration")
								.removeClass("active");
						$("#highlightIdMenuLog").removeClass("active");
						var skuMaterialFlag = 0;
						var classList = document
								.getElementById("toggleIdMenuSKUMaterial").classList;
						for (var i = 0; i < classList.length; i++) {
							if (classList[i] == "menu-is-opening") {
								skuMaterialFlag = 1;
								break;
							} else
								skuMaterialFlag = 0;
						}
						if (skuMaterialFlag == 1
								&& $scope.menuLinkClick == false) {
							$("#idMenuSKUMaterial svg").removeClass(
									"fa-angle-down").addClass("fa-angle-left");
							
							$("#toggleIdMenuSKUMaterial").removeClass("menu-is-opening menu-open"); 
  							$("#idSKUUL").attr("style", "display:none");
  							skuMaterialFlag = 0; 

						} else {

							$scope.menuLinkClick = false;

							$("#idPeopleUL").css($scope.displayNone);
							$("#toggleIdMenuPeople").removeClass(
									"menu-is-opening menu-open");
							$("#idMenuPeople svg").removeClass("fa-angle-down")
									.addClass("fa-angle-left");

							$("#idAssetUL").css($scope.displayNone);
							$("#toggleIdMenuAsset").removeClass(
									"menu-is-opening menu-open");
							$("#idMenuAsset svg").removeClass("fa-angle-down")
									.addClass("fa-angle-left");

							$("#idWorkOrderUL").css($scope.displayNone);
							$("#toggleIdMenuWorkOrder").removeClass(
									"menu-is-opening menu-open");
							$("#idMenuWorkOrder svg").removeClass(
									"fa-angle-down").addClass("fa-angle-left");

							$("#idSKUUL").css($scope.displayBlock);
							$("#toggleIdMenuSKUMaterial").addClass("menu-is-opening menu-open"); 
 							$("#idMenuSKUMaterial svg").removeClass(
									"fa-angle-left").addClass("fa-angle-down");

							$("#idWarehouseUL").css($scope.displayNone);
							$("#toggleIdMenuWarehouse").removeClass(
									"menu-is-opening menu-open");
							$("#idMenuWarehouse svg").removeClass(
									"fa-angle-down").addClass("fa-angle-left");

 							$("#idEI40DemoUL").css($scope.displayNone);
							$("#toggleIdMenuEI40Demo").removeClass(
									"menu-is-opening menu-open");
							$("#idMenuEI40Demo svg").removeClass(
									"fa-angle-down").addClass("fa-angle-left"); 							

							$("#idConfigurationUL").css($scope.displayNone);
							$("#toggleIdMenuConfiguration").removeClass(
									"menu-is-opening menu-open");
							$("#idMenuConfiguration svg").removeClass(
									"fa-angle-down").addClass("fa-angle-left");

							$("#idLogUL").css($scope.displayNone);
							$("#toggleIdMenuLog").removeClass(
									"menu-is-opening menu-open");
							$("#idMenuLog svg").removeClass("fa-angle-down")
									.addClass("fa-angle-left");

						}

					}
					$scope.naviWarehouseLink = function() {
						$("#highlightIdMenuDashboard").removeClass("active");
						$("#highlightIdMenuPeople").removeClass("active");
						$("#highlightIdMenuAsset").removeClass("active");
						$("#highlightIdMenuWorkOrder").removeClass("active");
						$("#highlightIdMenuSKUMaterial").removeClass("active");
						$("#highlightIdMenuWarehouse").addClass("active");
						$("#highlightIdMenuEI40Demo").removeClass("active");
						$("#highlightIdMenuConfiguration")
								.removeClass("active");
						$("#highlightIdMenuLog").removeClass("active");
						var WarehouseFlag = 0;
						var classList = document
								.getElementById("toggleIdMenuWarehouse").classList;
						for (var i = 0; i < classList.length; i++) {
							if (classList[i] == "menu-is-opening") {
								WarehouseFlag = 1;
								break;
							} else
								WarehouseFlag = 0;
						}
						if (WarehouseFlag == 1 && $scope.menuLinkClick == false) {
							$("#idMenuWarehouse svg").removeClass(
									"fa-angle-down").addClass("fa-angle-left");
							$("#toggleIdMenuWarehouse").removeClass("menu-is-opening menu-open"); 
  							$("#idWarehouseUL").attr("style", "display:none");
  							WarehouseFlag = 0; 
						} else {

							$scope.menuLinkClick = false;

							$("#idPeopleUL").css($scope.displayNone);
							$("#toggleIdMenuPeople").removeClass(
									"menu-is-opening menu-open");
							$("#idMenuPeople svg").removeClass("fa-angle-down")
									.addClass("fa-angle-left");

							$("#idAssetUL").css($scope.displayNone);
							$("#toggleIdMenuAsset").removeClass(
									"menu-is-opening menu-open");
							$("#idMenuAsset svg").removeClass("fa-angle-down")
									.addClass("fa-angle-left");

							$("#idWorkOrderUL").css($scope.displayNone);
							$("#toggleIdMenuWorkOrder").removeClass(
									"menu-is-opening menu-open");
							$("#idMenuWorkOrder svg").removeClass(
									"fa-angle-down").addClass("fa-angle-left");

							$("#idSKUUL").css($scope.displayNone);
							$("#toggleIdMenuSKUMaterial").removeClass(
									"menu-is-opening menu-open");
							$("#idMenuSKUMaterial svg").removeClass(
									"fa-angle-down").addClass("fa-angle-left");

							$("#idWarehouseUL").css($scope.displayBlock);
							$("#toggleIdMenuWarehouse").addClass("menu-is-opening menu-open"); 
 							$("#idMenuWarehouse svg").removeClass(
									"fa-angle-left").addClass("fa-angle-down");

 							$("#idEI40DemoUL").css($scope.displayNone);
							$("#toggleIdMenuEI40Demo").removeClass(
									"menu-is-opening menu-open");
							$("#idMenuEI40Demo svg").removeClass(
									"fa-angle-down").addClass("fa-angle-left"); 							
 							
							$("#idConfigurationUL").css($scope.displayNone);
							$("#toggleIdMenuConfiguration").removeClass(
									"menu-is-opening menu-open");
							$("#idMenuConfiguration svg").removeClass(
									"fa-angle-down").addClass("fa-angle-left");

							$("#idLogUL").css($scope.displayNone);
							$("#toggleIdMenuLog").removeClass(
									"menu-is-opening menu-open");
							$("#idMenuLog svg").removeClass("fa-angle-down")
									.addClass("fa-angle-left");

						}

					
					}
					$scope.naviEI40DemoLink = function() {
						$("#highlightIdMenuDashboard").removeClass("active");
						$("#highlightIdMenuPeople").removeClass("active");
						$("#highlightIdMenuAsset").removeClass("active");
						$("#highlightIdMenuWorkOrder").removeClass("active");
						$("#highlightIdMenuSKUMaterial").removeClass("active");
						$("#highlightIdMenuWarehouse").removeClass("active");
						$("#highlightIdMenuEI40Demo").addClass("active");
						$("#highlightIdMenuConfiguration")
								.removeClass("active");
						$("#highlightIdMenuLog").removeClass("active");
						var EI40DemoFlag = 0;
						var classList = document
								.getElementById("toggleIdMenuEI40Demo").classList;
						for (var i = 0; i < classList.length; i++) {
							if (classList[i] == "menu-is-opening") {
								EI40DemoFlag = 1;
								break;
							} else
								EI40DemoFlag = 0;
						}
						if (EI40DemoFlag == 1 && $scope.menuLinkClick == false) {
							$("#idMenuEI40Demo svg").removeClass(
									"fa-angle-down").addClass("fa-angle-left");
							$("#toggleIdMenuEI40Demo").removeClass("menu-is-opening menu-open"); 
  							$("#idEI40DemoUL").attr("style", "display:none");
  							EI40DemoFlag = 0; 
						} else {

							$scope.menuLinkClick = false;

							$("#idPeopleUL").css($scope.displayNone);
							$("#toggleIdMenuPeople").removeClass(
									"menu-is-opening menu-open");
							$("#idMenuPeople svg").removeClass("fa-angle-down")
									.addClass("fa-angle-left");

							$("#idAssetUL").css($scope.displayNone);
							$("#toggleIdMenuAsset").removeClass(
									"menu-is-opening menu-open");
							$("#idMenuAsset svg").removeClass("fa-angle-down")
									.addClass("fa-angle-left");

							$("#idWorkOrderUL").css($scope.displayNone);
							$("#toggleIdMenuWorkOrder").removeClass(
									"menu-is-opening menu-open");
							$("#idMenuWorkOrder svg").removeClass(
									"fa-angle-down").addClass("fa-angle-left");

							$("#idSKUUL").css($scope.displayNone);
							$("#toggleIdMenuSKUMaterial").removeClass(
									"menu-is-opening menu-open");
							$("#idMenuSKUMaterial svg").removeClass(
									"fa-angle-down").addClass("fa-angle-left");
							
							$("#idWarehouseUL").css($scope.displayNone);
							$("#toggleIdMenuWarehouse").removeClass(
									"menu-is-opening menu-open");
							$("#idMenuWarehouse svg").removeClass(
									"fa-angle-down").addClass("fa-angle-left");
							
							$("#idEI40DemoUL").css($scope.displayBlock);
							$("#toggleIdMenuEI40Demo").addClass("menu-is-opening menu-open"); 
 							$("#idMenuEI40Demo svg").removeClass(
									"fa-angle-left").addClass("fa-angle-down");

							$("#idConfigurationUL").css($scope.displayNone);
							$("#toggleIdMenuConfiguration").removeClass(
									"menu-is-opening menu-open");
							$("#idMenuConfiguration svg").removeClass(
									"fa-angle-down").addClass("fa-angle-left");

							$("#idLogUL").css($scope.displayNone);
							$("#toggleIdMenuLog").removeClass(
									"menu-is-opening menu-open");
							$("#idMenuLog svg").removeClass("fa-angle-down")
									.addClass("fa-angle-left");

						}

					}

					$scope.naviConfigurationLink = function() {
						$("#highlightIdMenuDashboard").removeClass("active");
						$("#highlightIdMenuPeople").removeClass("active");
						$("#highlightIdMenuAsset").removeClass("active");
						$("#highlightIdMenuWorkOrder").removeClass("active");
						$("#highlightIdMenuConfiguration").addClass("active");
						$("#highlightIdMenuLog").removeClass("active");
						$("#highlightIdMenuSKUMaterial").removeClass("active");
						$("#highlightIdMenuWarehouse").removeClass("active");
						$("#highlightIdMenuEI40Demo").removeClass("active");
						var configurationFlag = 0;
						var classList = document
								.getElementById("toggleIdMenuConfiguration").classList;
						for (var i = 0; i < classList.length; i++) {
							if (classList[i] == "menu-is-opening") {
								configurationFlag = 1;
								break;
							} else
								configurationFlag = 0;
						}
						if (configurationFlag == 1
								&& $scope.menuLinkClick == false) {
							$("#idMenuConfiguration svg").removeClass(
									"fa-angle-down").addClass("fa-angle-left");
							$("#toggleIdMenuConfiguration").removeClass("menu-is-opening menu-open"); 
  							$("#idConfigurationUL").attr("style", "display:none");
  							configurationFlag = 0; 
						} else {

							$scope.menuLinkClick = false;
							$("#idPeopleUL").css($scope.displayNone);
							$("#toggleIdMenuPeople").removeClass(
									"menu-is-opening menu-open");
							$("#idMenuPeople svg").removeClass("fa-angle-down")
									.addClass("fa-angle-left");

							$("#idAssetUL").css($scope.displayNone);
							$("#toggleIdMenuAsset").removeClass(
									"menu-is-opening menu-open");
							$("#idMenuAsset svg").removeClass("fa-angle-down")
									.addClass("fa-angle-left");

							$("#idWorkOrderUL").css($scope.displayNone);
							$("#toggleIdMenuWorkOrder").removeClass(
									"menu-is-opening menu-open");
							$("#idMenuWorkOrder svg").removeClass(
									"fa-angle-down").addClass("fa-angle-left");

							$("#idSKUUL").css($scope.displayNone);
							$("#toggleIdMenuSKUMaterial").removeClass(
									"menu-is-opening menu-open");
							$("#idMenuSKUMaterial svg").removeClass(
									"fa-angle-down").addClass("fa-angle-left");

							$("#idWarehouseUL").css($scope.displayNone);
							$("#toggleIdMenuWarehouse").removeClass(
									"menu-is-opening menu-open");
							$("#idMenuWarehouse svg").removeClass(
									"fa-angle-down").addClass("fa-angle-left");
							
 							$("#idEI40DemoUL").css($scope.displayNone);
							$("#toggleIdMenuEI40Demo").removeClass(
									"menu-is-opening menu-open");
							$("#idMenuEI40Demo svg").removeClass(
									"fa-angle-down").addClass("fa-angle-left"); 							

							$("#idConfigurationUL").css($scope.displayBlock);
							$("#toggleIdMenuConfiguration").addClass("menu-is-opening menu-open"); 
 							$("#idMenuConfiguration svg").removeClass(
									"fa-angle-left").addClass("fa-angle-down");

							$("#idLogUL").css($scope.displayNone);
							$("#toggleIdMenuLog").removeClass(
									"menu-is-opening menu-open");
							$("#idMenuLog svg").removeClass("fa-angle-down")
									.addClass("fa-angle-left");

						}

					}
					$scope.naviLogLink = function() {
						$("#highlightIdMenuDashboard").removeClass("active");
						$("#highlightIdMenuPeople").removeClass("active");
						$("#highlightIdMenuAsset").removeClass("active");
						$("#highlightIdMenuWorkOrder").removeClass("active");
						$("#highlightIdMenuConfiguration")
								.removeClass("active");
						$("#highlightIdMenuLog").addClass("active");
						$("#highlightIdMenuSKUMaterial").removeClass("active");
						$("#highlightIdMenuWarehouse").removeClass("active");
						$("#highlightIdMenuEI40Demo").removeClass("active");
						var logFlag = 0;
						var classList = document
								.getElementById("toggleIdMenuLog").classList;
						for (var i = 0; i < classList.length; i++) {
							if (classList[i] == "menu-is-opening") {
								logFlag = 1;
								break;
							} else
								logFlag = 0;
						}
						if (logFlag == 1 && $scope.menuLinkClick == false) {
							$("#idMenuLog svg").removeClass("fa-angle-down")
									.addClass("fa-angle-left");
							$("#toggleIdMenuLog").removeClass("menu-is-opening menu-open"); 
  							$("#idLogUL").attr("style", "display:none");
  							logFlag = 0; 
						} else {

							$scope.menuLinkClick = false;
							$("#idPeopleUL").css($scope.displayNone);
							$("#toggleIdMenuPeople").removeClass(
									"menu-is-opening menu-open");
							$("#idMenuPeople svg").removeClass("fa-angle-down")
									.addClass("fa-angle-left");

							$("#idAssetUL").css($scope.displayNone);
							$("#toggleIdMenuAsset").removeClass(
									"menu-is-opening menu-open");
							$("#idMenuAsset svg").removeClass("fa-angle-down")
									.addClass("fa-angle-left");

							$("#idWorkOrderUL").css($scope.displayNone);
							$("#toggleIdMenuWorkOrder").removeClass(
									"menu-is-opening menu-open");
							$("#idMenuWorkOrder svg").removeClass(
									"fa-angle-down").addClass("fa-angle-left");

							$("#idSKUUL").css($scope.displayNone);
							$("#toggleIdMenuSKUMaterial").removeClass(
									"menu-is-opening menu-open");
							$("#idMenuSKUMaterial svg").removeClass(
									"fa-angle-down").addClass("fa-angle-left");

							$("#idWarehouseUL").css($scope.displayNone);
							$("#toggleIdMenuWarehouse").removeClass(
									"menu-is-opening menu-open");
							$("#idMenuWarehouse svg").removeClass(
									"fa-angle-down").addClass("fa-angle-left");
							
 							$("#idEI40DemoUL").css($scope.displayNone);
							$("#toggleIdMenuEI40Demo").removeClass(
									"menu-is-opening menu-open");
							$("#idMenuEI40Demo svg").removeClass(
									"fa-angle-down").addClass("fa-angle-left"); 							

							$("#idConfigurationUL").css($scope.displayNone);
							$("#toggleIdMenuConfiguration").removeClass(
									"menu-is-opening menu-open");
							$("#idMenuConfiguration svg").removeClass(
									"fa-angle-down").addClass("fa-angle-left");

							$("#idLogUL").css($scope.displayBlock);
							$("#toggleIdMenuLog").addClass("menu-is-opening menu-open"); 
							$("#idMenuLog svg").removeClass("fa-angle-left")
									.addClass("fa-angle-down");

						}

					}
					/*
					 * $scope.naviAssetLink = function() { if($scope.assetFlag ==
					 * 0){ $("#idMenuAsset
					 * svg").removeClass("fa-angle-left").addClass("fa-angle-down");
					 * $scope.assetFlag=1; }else if($scope.assetFlag == 1){
					 * $("#idMenuAsset
					 * svg").removeClass("fa-angle-down").addClass("fa-angle-left");
					 * $scope.assetFlag=0; } } $scope.naviWorkOrderLink =
					 * function() { if($scope.workorderFlag == 0){
					 * $("#idMenuWorkOrder
					 * svg").removeClass("fa-angle-left").addClass("fa-angle-down");
					 * $scope.workorderFlag=1; }else if($scope.workorderFlag ==
					 * 1){ $("#idMenuWorkOrder
					 * svg").removeClass("fa-angle-down").addClass("fa-angle-left");
					 * $scope.workorderFlag=0; } } $scope.naviConfigurationLink =
					 * function() { if($scope.configurationFlag == 0){
					 * $("#idMenuConfiguration
					 * svg").removeClass("fa-angle-left").addClass("fa-angle-down");
					 * $scope.configurationFlag=1; }else
					 * if($scope.configurationFlag == 1){
					 * $("#idMenuConfiguration
					 * svg").removeClass("fa-angle-down").addClass("fa-angle-left");
					 * $scope.configurationFlag=0; } } $scope.naviLogLink =
					 * function() { if($scope.logFlag == 0){ $("#idMenuLog
					 * svg").removeClass("fa-angle-left").addClass("fa-angle-down");
					 * $scope.logFlag=1; }else if($scope.logFlag == 1){
					 * $("#idMenuLog
					 * svg").removeClass("fa-angle-down").addClass("fa-angle-left");
					 * $scope.logFlag=0; } }
					 */
					$rootScope.fnGetDictionaryLength = function(dict) {
						var nCount = 0;
						for (i in dict)
							++nCount;
						return nCount;
					}
					$scope.fnFrequentlyUsedMenuClick = function() {
						
						if ($scope.varAccessMenuOption == 0) {
							
						
							// read user data from storage
							$scope.user = JSON.parse($window.sessionStorage
									.getItem("user"));
							var preferredMenuLength = $rootScope.fnGetDictionaryLength($scope.user.capabilityList.preferredMenu);
							if (preferredMenuLength > 0) {
								$scope.varAccessMenuOption = 1;
								
								
								$scope.user.capabilityDetails = $scope.user.capabilityList.preferredMenu;
								// Write user data to storage
								$window.sessionStorage.setItem("user", JSON
										.stringify($scope.user));
								$rootScope.$broadcast('updateCapabilityMenu');
								// reset menu option after broadcast
								$scope.varAccessMenuOption = 1;
							} else {
								$scope.varAccessMenuOption = 0;
								document.getElementById("idCkAllowedMenu").checked = true;
								document.getElementById("idCkFreqUsedMenu").checked = false;
								
								// no preferred menu message
								$.alert({
									title : $rootScope.MSGBOX_TITLE,
									content : 'No My Menu Options',
									type : 'blue',
									columnClass : 'small',
									autoClose : false,
									icon : 'fa fa-info-circle',
								});
							}
						}
					}
					$scope.fnAllowedMenuClick = function() {
						//$scope.varAccessMenuOption = 0;
						if ($scope.varAccessMenuOption == 1) {
							$scope.varAccessMenuOption = 0;
						
							// read user data from storage
							$scope.user = JSON.parse($window.sessionStorage
									.getItem("user"));

							$scope.user.capabilityDetails = $scope.user.capabilityList.allowedMenu;
						
							// Write user data to storage
							$window.sessionStorage.setItem("user", JSON
									.stringify($scope.user));

							$rootScope.$broadcast('updateCapabilityMenu');
							$scope.varAccessMenuOption = 0;
						}
					}

					$scope.fnAccessMenuClick = function() {
						if ($scope.varAccessMenuOption == 0) {
							// $scope.user.capabilityDetails =
							// $scope.user.capabilityList.allowedMenu;
							document.getElementById("idCkAllowedMenu").checked = true;
							document.getElementById("idCkFreqUsedMenu").checked = false;

						} else { // if ($scope.varAccessMenuOption ==0)
							// $scope.user.capabilityDetails =
							// $scope.user.capabilityList.preferredMenu;
							document.getElementById("idCkFreqUsedMenu").checked = true;
							document.getElementById("idCkAllowedMenu").checked = false;
							// $rootScope.$broadcast('updateCapabilityMenu');
						}
					}

					$scope.fnProfileClick = function() {
						$scope.menuLink('userProfile', 'Profile Setting');
						$rootScope.$broadcast('updateNotification', 1);
					}
					
					$rootScope.fnHttpError = function(errorStr) {
						if (errorStr.status == $rootScope.ERR_403_FOUND) {
							if ($scope.serverNotRespondingMessage == false){
								$rootScope.sessionTimeoutDialog();
								$scope.serverNotRespondingMessage = true;
							}
						}else if (errorStr.status == -1) {
							if ($scope.serverNotRespondingMessage == false){
								$rootScope.alertDialog("Server is not responding. Please check the server");
								$scope.serverNotRespondingMessage = true;
							}
						}else if(errorStr.status == 500){
							return;
							
							
							
						}
						else {
							$rootScope.alertDialog(errorStr.data.message);
						}
					}
					
					$scope.fnLogoutClick = function() {
						
						$scope.role = 'dashboard';
						
						
						sessionStorage.clear();
						$scope.serverNotRespondingMessage = false;
						$window.sessionStorage.removeItem("user");
						$("#loadingBar").addClass("sidebar-mini");
						$("#loadingBar").removeClass("sidebar-collapse");
						$location.path('/login');
						$scope.disconnect();
					}
					$scope.totalNumberOfNotifications = 0;
					$rootScope
							.$on(
									'updateNotification',
									function(event, nNotification) {
										$scope.totalNumberOfNotifications = $scope.totalNumberOfNotifications
												+ nNotification;
									});

					$rootScope.$on('updateProfileColor', function(event,
							profileColor, showNotificationStatus) {
						$scope.showNotification = showNotificationStatus;
						if (profileColor != null && profileColor != "") {
							if ($scope.user.role == "SystemAdmin" || $scope.user.role == "DemoUser")
								$scope.varSysAdminColor = profileColor;
							else if ($scope.user.role == "Manager")
								$scope.varManagerColor = profileColor;
							else if ($scope.user.role == "SuperUser")
								$scope.varSuperUserColor = profileColor;
							else if ($scope.user.role == "User")
								$scope.varUserColor = profileColor;

						} else {
							var defaultProfileColor;
							defaultProfileColor = $rootScope.getProfileColor();
							$scope.user.profileColor = defaultProfileColor
							$window.sessionStorage.setItem("user", JSON
									.stringify($scope.user));
						}

						$scope.updateProfileImage();
						$scope.setPageHeaderColor();
					});

					$rootScope.$on('updateProfileHeaderImage', function(event) {
						$scope.updateProfileImage();
					});
					$rootScope.$on('enablePreviousScreenIcon', function(event,
							shouIcon) {
						if ($scope.previousMenuFromLive != "")
							$scope.showPreviousScreenIcon = shouIcon;
						else
							$scope.showPreviousScreenIcon = false;
					});

					$rootScope.$on('sourcePreviousScreen', function(event,
							sourceTrackMenu) {
						$scope.showPreviousScreenIcon = true;
						$rootScope.LocateItem = true;
						$scope.previousMenuFromLive = sourceTrackMenu;
						
					});
					
					$rootScope.isBackClicked = false;
					$scope.gotoSourcePage = function() {
						if ($scope.previousMenuFromLive != "")
							{
							    $rootScope.isBackClicked = true;
								$("#loadingBar").removeClass("sidebar-collapse");
								$location.path('/'+ $scope.previousMenuFromLive);
								$window.sessionStorage.setItem("menuName", $scope.previousMenuFromLive);
								$scope.previousMenuFromLive = "";
								$scope.showPreviousScreenIcon = false;
								$rootScope.setPageName($rootScope.pageName);
							}
					}
					$scope.updateProfileImage = function() {
						var profileImage = $window.sessionStorage
								.getItem("profileImage");

						if (profileImage == undefined || profileImage == '')
							$scope.imageData = './dist/img/userImage.jpg';
						else {
							$scope.imageData = 'data:image/png;base64, '
									+ profileImage;
						}

						document.getElementById("profileHeaderImg").src = $scope.imageData;
						document.getElementById("profileHeaderImg1").src = $scope.imageData;

					}

					$rootScope.getProfileColor = function() {
						var currentProfileColor;
						if ($scope.user.role == "SystemAdmin" || $scope.user.role == "DemoUser")
							currentProfileColor = $scope.varSysAdminColor;
						else if ($scope.user.role == "Manager")
							currentProfileColor = $scope.varManagerColor;
						else if ($scope.user.role == "SuperUser")
							currentProfileColor = $scope.varSuperUserColor;
						else if ($scope.user.role == "User")
							currentProfileColor = $scope.varUserColor;
						return currentProfileColor;
					}

					$scope.profileColorChange = function() {
						if ($scope.user.role == "SystemAdmin" || $scope.user.role == "DemoUser")
							document.getElementById("idProfileColor").style.backgroundColor = $scope.varSysAdminColor;
						else if ($scope.user.role == "Manager")
							document.getElementById("idProfileColor").style.backgroundColor = $scope.varManagerColor;
						else if ($scope.user.role == "SuperUser")
							document.getElementById("idProfileColor").style.backgroundColor = $scope.varSuperUserColor;
						else if ($scope.user.role == "User")
							document.getElementById("idProfileColor").style.backgroundColor = $scope.varUserColor;
					}

					var stompClient = null;
					$scope.connect = function() {
						var socket = new SockJS('/ei4o/gs-guide-websocket');
						stompClient = Stomp.over(socket);
						stompClient
								.connect(
										{},
										function(frame) {
											//console.log('Connected: ' + frame);
											subscribe();
											stompClient
													.subscribe(
															'/topic/notification',
															function(greeting) {
																var data = JSON
																		.parse(greeting.body);
																if (data != undefined
																		&& data != null) {
																	if ($scope.showNotification == true) {
																		if (data.status == 'success'
																				&& data.notificationType != undefined) {
																			if (data.notificationType == 0)
																				toastr
																						.info(
																								data.message,
																								"EI4.0");
																			else if (data.notificationType == 1)
																				toastr
																						.warning(
																								data.message,
																								"EI4.0");
																			else if (data.notificationType == 2)
																				toastr
																						.error(
																								data.message,
																								"EI4.0");
																		}
																	}

																	$scope
																			.updateNotificationCount();
																}

															});
										});
					}

					$scope.disconnect = function() {
						if (stompClient !== null) {
							unsubscribe();
							stompClient.disconnect();
						}
						//console.log("Disconnected");
					}

					function subscribe() {
						stompClient.send("/ei4o/subscribeNotification", {},
								JSON.stringify({
									'type' : 'subscribe' 
								}));
					}

					function unsubscribe() {
						stompClient.send("/ei4o/subscribeNotification", {},
								JSON.stringify({
									'type' : 'unsubscribe' 
								}));
					}

					$scope.updateNotificationCount = function() {
						$http(
								{
									method : 'GET',
									url : './notificationCount/'
											+ $window.sessionStorage
													.getItem("readNotificationCount")
								})
								.then(
										function success(response) {

											if (response != null
													&& response.data != null
													&& response.data != "BAD_REQUEST") {

												var responseData = response.data.data;

												$scope.notificationEmergenyCount = responseData.emergeny;
												$scope.notificationWarningCount = responseData.warning;
												$scope.notificationInfoCount = responseData.information;

												if (responseData.totalUnRead > 0)
													$scope.notificationCount = responseData.totalUnRead;
												else
													$scope.notificationCount = '';

											}
										}, function error(response) {
											$rootScope
											.hideloading('#loadingBar');
											$rootScope.fnHttpError(response);
											$scope.serverNotRespondingMessage = false;
									
								});
					}
					var script = document.createElement('script');
				    script.type = 'text/javascript';

				    script.src = "https://cdnjs.cloudflare.com/ajax/libs/jspdf-autotable/3.5.6/jspdf.plugin.autotable.min.js";
				    document.body.appendChild(script);
				});