app
	.controller(
		'forgotPasswordcontroller',
		function($scope, $http, $window, $rootScope, $location,$timeout) {
			//$rootScope.$broadcast('updatemenu',"admin");
			 // Anand Login page routing 25-5-21
			
			$scope.loginPage = function (){
				 
				 $location.path("/login");
			 }
			 /*$scope.registerPage = function (){
				 
				 $location.path("/register");
			 }*/
			 $scope.sendPassword = function (){
				 
				
				
				 $("#ForgotPwdForm").validate({

						// Rules for form validation
						rules : {
							ForgotPwduserID : {
								required : true
									
							}
							
						},
						// Messages for form validation
						messages : {
							ForgotPwduserID : {
								required : 'Please enter User ID',
								pattern:'Please enter valid Email format'
							}
						},

						// Do not change code below
						errorPlacement : function(error, element) {
							error.insertAfter(element.parent()); 
						}
					});
				 if($scope.ForgotPwdFormName.$valid){
				 
				 var forgotPwdParameter = {
							"userid" : $scope.ForgotPwduserID

						};
				 if (!$rootScope.checkNetconnection()) { 
						$rootScope.alertDialogChecknet();
						return;
								 } 
				  $rootScope.showloading('#loadingBar');
				 $scope.promise = $http
					.post('./forgotpassword',
							forgotPwdParameter, {timeout: $rootScope.TIME_OUT_BEFORE_LOGIN})
					.then(
							function(result) {
								 
								 if (result != null
											&& result.data != null
											&& result.data != "BAD_REQUEST") {
									 
									 $rootScope.hideloading('#loadingBar');
									 $rootScope.showAlertDialog(result.data.message);
					        		 $timeout(function() {
											
									        $location.path("/login"); 
					                     
											 }, 3000); 
								 } else {
									 $rootScope.hideloading('#loadingBar');
									 $rootScope.showAlertDialog(result.data.message);
					        		 
								 }

							}, function(error) {
								$rootScope.hideloading('#loadingBar');
								  if (error.data != null || error.status == -1 ) {
									  $rootScope.outsideLoginTimeoutDialog();  
								  }
								  else if (error.data.status == "error") {
									  $rootScope.showAlertDialog(error.data.message);
						        		 
										
									}
					       
							});

				 }
			 }
			 
		});

