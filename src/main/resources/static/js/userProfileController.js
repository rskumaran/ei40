app
		.controller(
				'userProfileController',
				function($scope, $http, $window, $rootScope, $location,
						$timeout) {

					$scope.user = JSON.parse($window.sessionStorage
							.getItem("user"));
					if ($scope.user == null) {
						$location.path('/login');
						return;
					}
					$scope.varProfileColor = $scope.user.profileColor;

					document.getElementById("idNotificationOnMenu").checked = $scope.user.showNotification;
					$("#idSettingsTab").show();
					// $("#idProfileColorTab").hide();
					$("#idChangePasswordTab").hide();
					$("#profileImgEdit").show();
					$("#showCropImageView").hide();

					$("#saveImageBtn").hide();
					$("#cancelImageBtn").hide();

					$scope.saveBtnText = 'Save';
					$scope.isImageEditMode = false;
					$scope.ticketSubject = "";
					$scope.ticketDescription = "";
					var profileImage = $window.sessionStorage.getItem("profileImage");

					if (profileImage == undefined || profileImage == '')
						$scope.imageData = './dist/img/userImage.jpg';
					else {
						$scope.imageData = 'data:image/png;base64, '
								+ profileImage;
					}

					document.getElementById("profileImg").src = $scope.imageData;
					document.getElementById("profileImgEdit").src = $scope.imageData;
					
					
					
				// Set the default value of inputType
				   $scope.inputType = 'password';
				// Hide & show password function
				   $scope.hideShowPassword = function(){
				    if ($scope.passwordCheckbox)
				      $scope.inputType = 'text';
				    else
				      $scope.inputType = 'password';
				   };
					
					$scope.fnSettingsClick = function() {
						$("#idSettingsTab").show();
						$("#idChangeProfileImageTab").hide();
						$("#idChangePasswordTab").hide();
						$("#idCreateTicketTab").hide();

					}
					/*
					 * $scope.fnProfileColorClick = function() {
					 * $("#idSettingsTab").hide();
					 * $("#idProfileColorTab").show();
					 * $("#idChangePasswordTab").hide(); }
					 */
					$scope.fnChangePasswordClick = function() {
						$("#idSettingsTab").hide();
						$("#idChangeProfileImageTab").hide();
						$("#idChangePasswordTab").show();
						$("#idCreateTicketTab").hide();
					}

					$scope.fnChangeProfileImage = function() {
						$scope.srcResized = null;
						$("#idSettingsTab").hide();
						$("#idChangeProfileImageTab").show();
						$("#idChangePasswordTab").hide();
						$("#idCreateTicketTab").hide();
						$("#profileImgEdit").show();
						$("#showCropImageView").hide();

					}
					$scope.fnCreateTicketTabClick = function() {
						$("#idSettingsTab").hide();
						$("#idChangeProfileImageTab").hide();
						$("#idChangePasswordTab").hide();
						$("#idCreateTicketTab").show();

					}
					$scope.fnCreateTicketBtn= function() {
						if ($scope.ticketSubject == "") {
							$scope.ticketSubjectError = true;
							$scope.ticketSubjectErrorMsg = "Please enter subject";
							$timeout(function() {
								$scope.ticketSubjectError = false;
								$scope.ticketSubjectErrorMsg = "";
							}, 2000);
							return;
						      
					        } 
						   else if($scope.ticketDescription == ""){
						      $scope.ticketDescError=true;
						      $scope.ticketDescErrorMsg="Please enter description";
             					  $timeout(function() {
								    $scope.ticketDescError= false;
				                     $scope.ticketDescErrorMsg="";
				                     }, 2000);
             					 return;
					        }
						
						var ticketData = {};
						ticketData.subject =  $scope.ticketSubject;
						ticketData.description= $scope.ticketDescription;
						$.confirm({
							title : $rootScope.MSGBOX_TITLE,
							content : 'Do you want to create the ticket?',
							type : 'blue',
							columnClass : 'medium',
							autoClose : false,
							icon : 'fa fa-info-circle',
							buttons : {
								yes : {
									text : 'Yes',
									btnClass : 'btn-blue',
									action : function() {
										$rootScope.showloading('#loadingBar');

						$http(
								{
									method : 'POST',
									url : './createTicket',
									data : ticketData,
									headers : {
										'Content-Type' : 'application/json'
									}
								}).then(
									function success(response) {
										$rootScope.hideloading('#loadingBar');
										
										if (response.data.status == "success") {
											$scope.ticketSubject="";
											$scope.ticketDescription="";
											$rootScope.showAlertDialog(response.data.message);
							
										}else if (response.data.status == "error") {
											$rootScope.showAlertDialog(response.data.message);
											
										} 
									},
									function error(response) {
										$rootScope.hideloading('#loadingBar');
										$rootScope.fnHttpError(response);
									});
									}
								},
								no : function() {
									$rootScope
									.hideloading('#loadingBar');
								}
							}
						});
						
						
						//toastr.success(
						//		"CreateTicket",
						//		"EI4.0");
						$scope.ticketSubject = $scope.ticketSubject; 
						$scope.ticketDescription = $scope.ticketDescription; 
							
					}
					
					$scope.fnClearTicketBtn= function() {
						$scope.ticketSubject = "";
						$scope.ticketDescription = "";
						document.getElementById("idTicketDescription").placeholder = "Enter Description";
						document.getElementById("idTicketSubject").placeholder = "Enter Subject";
					}
					
					$scope.srcResized = null;

					$scope.saveImageEdit = function() {

						if ($scope.isImageEditMode) {
							document.getElementById("profileImgEdit").src = $scope.srcResized;
							$scope.saveBtnText = 'Save';
							$("#saveImageBtn").html('Save');
							$("#profileImgEdit").show();
							$("#showCropImageView").hide();
							$scope.isImageEditMode = false;
						} else {
							$scope.updateProfileImage();
						}

					}

					$scope.cancelImageEdit = function() {

						document.getElementById("profileImgEdit").src = $scope.imageData;
						$("#profileImgEdit").show();
						$("#showCropImageView").hide();
						$scope.isImageEditMode = false;
						$scope.saveBtnText = 'Save';
						$("#saveImageBtn").html('Save');
						$("#saveImageBtn").hide();
						$("#cancelImageBtn").hide();
						$("#pickImageLabel").show();
						$(".rcrop-preview-wrapper").remove();
						$('#image-1').rcrop('destroy');
						$scope.srcResized = null;
					}

					 
					$("#pickImageLabel").click(function() {
						$("#pickImageBtn").val("");
					});
					
					$("#pickImageBtn").click(function() {
						$("#pickImageBtn").val("");
					});

					$("#pickImageBtn")
							.change(
									function() {

										if (this.files.length > 0) {

											var filename = this.files[0].name;

											var FR = new FileReader();

											FR
													.addEventListener(
															"load",
															function(e) {

																$scope.saveBtnText = 'Done';

																$(
																		"#saveImageBtn")
																		.html(
																				'Done');
																$(
																		"#profileImgEdit")
																		.hide();
																$(
																		"#showCropImageView")
																		.show();
																$(
																		"#pickImageLabel")
																		.hide();
																$(
																		"#saveImageBtn")
																		.show();
																$(
																		"#cancelImageBtn")
																		.show();

																$scope.isImageEditMode = true;
																document
																		.getElementById("image-1").src = e.target.result;

																$('#image-1')
																		.rcrop(
																				{
																					minSize : [
																							160,
																							160 ],
																					preserveAspectRatio : false,
																					preview : {
																						display : true,
																						size : [
																								100,
																								100 ],
																						wrapper : '#custom-preview-wrapper'
																					},
																					grid : true
																				});

																$('#image-1')
																		.on(
																				'rcrop-changed',
																				function() {
																					$scope.srcResized = $(
																							this)
																							.rcrop(
																									'getDataURL',
																									160,
																									160);
																					// console.log(srcResized);

																				});

																$('#image-1')
																		.on(
																				'rcrop-ready',
																				function() {
																					$scope.srcResized = $(
																							this)
																							.rcrop(
																									'getDataURL',
																									160,
																									160);
																				})

															});
											FR.readAsDataURL(this.files[0]);
										}
									});

					 

					$scope.updateProfileImage = function() {
						$rootScope.showloading('#loadingBar');
						$http({
							method : 'POST',
							url : './uploadProfileImage',
							data : {
								'emailId' : $scope.user.emailID,
								"profileImage" : $scope.srcResized
							},

						})
								.then(
										function success(result) {
											$rootScope
													.hideloading('#loadingBar');
											if (result.data.status == "success") {

												toastr.success(
														result.data.message,
														"EI4.0");
												$scope.imageData = $scope.srcResized;
												document
														.getElementById("profileImg").src = $scope.imageData;

												var profileImageArr = $scope.imageData
														.split(",");

												$window.sessionStorage.setItem(
														"profileImage",
														profileImageArr[1]);
												$scope.cancelImageEdit();
												$rootScope
														.$broadcast('updateProfileHeaderImage');

											}

										},
										function error(response) {
											$rootScope.hideloading('#loadingBar');
											$rootScope.fnHttpError(response);
										});
					}

					$scope.fnUpdateProfile = function() {

						var showNotificationChecked = document
								.getElementById("idNotificationOnMenu").checked;
						var updateProfileParameter = {
							"emailId" : $scope.user.emailID,
							"showNotification" : showNotificationChecked,
							"profileColor" : $scope.varProfileColor

						};
						if (!$rootScope.checkNetconnection()) { 
						 	$rootScope.alertDialogChecknet();
							return; 
						}
						// updateProfileParameter = updateProfileParameter;
						$rootScope.showloading('#loadingBar');
						$http({
							method : 'POST',
							url : './updateProfile',
							data : updateProfileParameter,

						})
								.then(
										function success(result) {
											$rootScope
													.hideloading('#loadingBar');
											if (result.data.status == "success") {

												$rootScope
														.$broadcast(
																'updateProfileColor',
																$scope.varProfileColor,
																showNotificationChecked);
												$scope.user.showNotification = showNotificationChecked;
												$scope.user.profileColor = $scope.varProfileColor;
												$window.sessionStorage
														.setItem(
																"user",
																JSON
																		.stringify($scope.user));

											}
											$
													.alert({
														title : $rootScope.MSGBOX_TITLE,
														content : result.data.message,
														type : 'blue',
														columnClass : 'small',
														autoClose : false,
														icon : 'fa fa-info-circle',
													});

										},
										function error(response) {
											$rootScope.hideloading('#loadingBar');
											$rootScope.fnHttpError(response);
										});

					}
					
					
					
					
					//kumar 14-Jun show password with eye icon
					$scope.oldPwdeyeButtonClick = function() {
						$scope.toggleEyeButton('#idChangePwdOldPwd',"#id_OldPwdToggle svg");
					}			 
					//kumar 14-Jun show password with eye icon
					$scope.newPwdeyeButtonClick = function() {
						$scope.toggleEyeButton('#idChangePwdNewPwd',"#id_NewPwdToggle svg");
					}			 
					//kumar 14-Jun show password with eye icon
					$scope.confirmPwdeyeButtonClick = function() {
						$scope.toggleEyeButton('#idChangePwdConfirmPwd',"#id_ConfirmPwdToggle svg");
					}			 
					//kumar 14-Jun show password with eye icon
				 $scope.toggleEyeButton = function (pwdEditCtrl, eyeButton){
						const passwordCtl = document.querySelector(pwdEditCtrl);
						
						if(passwordCtl.getAttribute('type') == "password"){
					       	passwordCtl.setAttribute('type', 'text');
					        //$("#togglePassword svg").removeClass("fa-eye-slash").addClass("fa-eye");
						}else if(passwordCtl.getAttribute('type') == "text"){
							passwordCtl.setAttribute('type', 'password');
					       	//$("#togglePassword svg").removeClass("fa-eye").addClass("fa-eye-slash");
						}
						$(eyeButton).toggleClass("fa-eye fa-eye-slash");
					 
				 }
				 $scope.changePassword = function (){
					 
					 var ChangeOldPwd = document.getElementById("idChangePwdOldPwd");
						if (ChangeOldPwd.value.length == 0) {
							$scope.changeOldPasswordError = true;
							$scope.changeOldPassword_error_msg = "Please enter old password";
							$timeout(function() {
								$scope.changeOldPasswordError = false;
							}, 2000);
							return;
						}
						
						var ChangeNewPwd = document.getElementById("idChangePwdNewPwd");
						if (ChangeNewPwd.value.length == 0) {
							$scope.changeNewPasswordError = true;
							$scope.changeNewPassword_error_msg = "Please enter new password";
							$timeout(function() {
								$scope.changeNewPasswordError = false;
							}, 2000);
							return;
						}
					 
						 var ChangeConfirmPwd = document.getElementById("idChangePwdConfirmPwd");
							if (ChangeConfirmPwd.value.length == 0) {
								$scope.changeConfirmPasswordError = true;
								$scope.changeConfirmPassword_error_msg = "Please enter confirm password";
								$timeout(function() {
									$scope.changeConfirmPasswordError = false;
								}, 2000);
								return;
							}
					 if($scope.changePwdForm.$valid){
						
						 if($scope.ChangePwdNewPwd != $scope.ChangePwdOldPwd && ($scope.ChangePwdNewPwd !="" || $scope.ChangePwdNewPwd != undefined) && 
								 ($scope.ChangePwdOldPwd  !="" || $scope.ChangePwdOldPwd  != undefined) ){
						 if($scope.ChangePwdNewPwd.length>=8 ){
							 
							 if($scope.ChangePwdNewPwd!=$scope.ChangePwdConfirmPwd && ($scope.ChangePwdNewPwd !="" 
								 || $scope.ChangePwdNewPwd != undefined) && ($scope.ChangePwdConfirmPwd !=undefined 
								|| scope.ChangePwdConfirmPwd !="")){
								 $rootScope.showAlertDialog("Password does not match");												 
								 
							 } else{ 
								 if(!/[ !@#$%^&*()_+\-=\[\]{};':"\\|,.<>\/?]/.test($scope.ChangePwdNewPwd)){
									 $rootScope.showAlertDialog("Password must contain special character");
									   }
									   else{
								 
							 
					 var changePwdParameter = {
							  "userid": $scope.user.emailID,
							  "password": $scope.ChangePwdNewPwd,
							  "old_password": $scope.ChangePwdOldPwd
							};
					 if (!$rootScope.checkNetconnection()) { 
						 	$rootScope.alertDialogChecknet();
							return; 
							}
					 $rootScope.showloading('#loadingBar');
					$scope.promise = $http
						.post('./changepassword',
								changePwdParameter)
						.then(
								function(result) {

									if (result != null
											&& result.data != null
											&& result.data != "BAD_REQUEST") {
										$rootScope.hideloading('#loadingBar');
										 $rootScope.showAlertDialog(result.data.message);
										var formID=document.getElementById("changePwdForm").reset();
										$("#changePwdForm").data('validator').resetForm();
									} else {
										$rootScope.hideloading('#loadingBar');
										 $rootScope.showAlertDialog(result.data.message);
										
									}

								}, function(error) {
									$rootScope.hideloading('#loadingBar');
									$rootScope.fnHttpError(error);
						       });
							 }
							 }
					 
				 }
						 else{
							 $rootScope.showAlertDialog("Please enter a password greater than or equal to 8");
							 }
						 }
						 else{
							 $rootScope.showAlertDialog("Old Password and New Password should be unique");
							 }
						 }
				 }
				
				});
