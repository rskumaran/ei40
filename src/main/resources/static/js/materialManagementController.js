app
		.controller(
				'materialManagementController',
				function($scope, $http, $window, $rootScope, $location,
						$timeout) {
					$scope.user = JSON.parse($window.sessionStorage
							.getItem("user"));
					if ($scope.user == null) {
						$location.path('/login');
						return;
					}
					$scope.materialViewScreen = true;
					$scope.searchButton = true;

					 
					$scope.assignedTagMacId = '';
					$scope.trackMacId = '';
					
					
					$scope.errorArray =new Array(); // error detail array for multiple material upload
					//Regex for multiple material validation
					
					var ProductIdRegex = /^[a-zA-Z0-9]*$/;
					var orderNoRegex = /^[0-9]*$/;
					var sourceDestPCRegex = /^[a-zA-Z0-9]*$/;
					var priceRegex = /^([0-9]{0,9}((.)[0-9]{0,2}))$/;
					var dateRegex = /^\d{4}\-(Jan|Feb|Mar|Apr|May|Jun|Jul|Aug|Sep|Oct|Nov|Dec)\-(([0-9])|([0-2][0-9])|([3][0-1]))$/;
					

					if ($scope.authen == undefined) {
						$scope.authen = {};
					}
					$scope.jsonContent = [];
					$scope.jsonMultipleMaterials = new Array();
					$scope.csvContent =[];
					$scope.ext ="";
					

					$scope.firstSearchANDDataArray = new Array();
					$scope.secondSearchORDataArray = new Array();
					$scope.firstSearchText = "";
					$scope.secondSearchText = "";

					$scope.materialList = new Array();
					$scope.materialViewData = new Array();
					$scope.materialFilterList = new Array();

					$scope.keyNames = new Array(); // for bulk material
					// dropdown value
					$scope.jsonDataArray = new Array(); // for bulk material
					// dropdown value
					var multipleMaterials = new Array();
					var material = {};

					$scope.materialList.length = 0;
					$scope.materialViewData.length = 0;
					$scope.materialFilterList.length = 0;
					$scope.searchText = "";
					$('#addMaterialDateTimePicker').datetimepicker({
						format : 'YYYY-MMM-DD',
						minDate : new Date()
					});
					$('#editIssueDatetimepicker').datetimepicker({
						format : 'YYYY-MMM-DD',
						maxDate : new Date()
					});

					$scope.showLocateBtn = false;
					
					$scope.user = JSON.parse($window.sessionStorage
							.getItem("user"));

					if ($scope.user.capabilityDetails != undefined
							&& $scope.user.capabilityDetails != null) {
						if ($scope.user.capabilityDetails.WMVA == 1) {
							$scope.showViewAllBtn = true;
						} else {
							$scope.showViewAllBtn = false;
						}
						if ($scope.user.capabilityDetails.WMAD == 1) {
							$scope.showAddBtn = true;
						} else {
							$scope.showAddBtn = false;
						}
						if ($scope.user.capabilityDetails.WMED == 1) {
							$scope.showEditBtn = true;
						} else {
							$scope.showEditBtn = false;
						}
						if ($scope.user.capabilityDetails.WMMA == 1) {
							$scope.showMultipleAddBtn = true;
						} else {
							$scope.showMultipleAddBtn = false;
						}
						
						if ($scope.user.capabilityDetails.WLVW == 1) {
							$scope.showLocateBtn = true;
						}

					}
					
					// kumar 05-Jul-21 ->
					$scope.fnMaterialSearchOption = function() {
						$scope.fnSearchFilterOption();
					}

					$scope.trackTag = function() {
						if($scope.assignedTagMacId!=undefined && $scope.assignedTagMacId!=null 
								&& $scope.assignedTagMacId!='')
						{

							$rootScope.trackingMacId = $scope.assignedTagMacId;
							$rootScope.showFullView();
							var currentMenuName = $window.sessionStorage.getItem("menuName");
							if ($scope.user.role == "DemoUser")
							{
								$window.sessionStorage.setItem("menuName", "shopFloorAnalysis");
								$window.sessionStorage.setItem("demoMenuName", "workorderLiveView");
								$location.path('/shopFloorAnalysis');
							}else{
								$window.sessionStorage.setItem("menuName",
										"workorderLiveView");
								$location.path('/workorderLiveView');
							}
							$rootScope.$broadcast('sourcePreviousScreen',currentMenuName);
						}else{
							toastr.error('No assigned tags to track.', "EI4.0");  
						}

					}
					
					$scope.trackSingleTag = function() {
						if ($scope.trackMacId != undefined
								&& $scope.trackMacId != null
								&& $scope.trackMacId != '') {
							$rootScope.showFullView();
							$rootScope.trackingMacId = $scope.trackMacId;
							var currentMenuName = $window.sessionStorage.getItem("menuName");
							if ($scope.user.role == "DemoUser")
							{
								$window.sessionStorage.setItem("menuName", "shopFloorAnalysis");
								$window.sessionStorage.setItem("demoMenuName", "workorderLiveView");
								$location.path('/shopFloorAnalysis');
							}else{
								$window.sessionStorage.setItem("menuName",
										"workorderLiveView");
								$location.path('/workorderLiveView');
							}
							$rootScope.$broadcast('sourcePreviousScreen',currentMenuName);
						} else {
							toastr.error('No assigned tags to track.', "EI4.0");
						}
					}
					
					

					$scope.fnSearchFilterOption = function() {
						$scope.materialViewData = [];
						$scope.materialFilterList = [];
 
						for (var i = 0; i < $scope.materialList.length; i++) {

							if ($scope.searchText == '') {
								$scope.materialFilterList
										.push($scope.materialList[i]);
							} else {
								if ($scope.materialList[i].prodId
										.toLowerCase()
										.includes(
												$scope.searchText.toLowerCase())
										|| ($scope.materialList[i].destination != undefined
												&& $scope.materialList[i].destination != null ? $scope.materialList[i].destination
												: "").toLowerCase()
												.includes(
														$scope.searchText
																.toLowerCase())
										|| ($scope.materialList[i].issueDate != undefined
												&& $scope.materialList[i].issueDate != null ? $scope.materialList[i].issueDate
												: "").toLowerCase()
												.includes(
														$scope.searchText
																.toLowerCase())
										|| ($scope.materialList[i].source != undefined
												&& $scope.materialList[i].source != null ? $scope.materialList[i].source
												: "").toLowerCase()
												.includes(
														$scope.searchText
																.toLowerCase())
										|| ($scope.materialList[i].processControl != undefined
												&& $scope.materialList[i].processControl != null ? $scope.materialList[i].processControl
												: "").toLowerCase()
												.includes(
														$scope.searchText
																.toLowerCase())

										|| ($scope.materialList[i].ordereNo != undefined
												&& $scope.materialList[i].ordereNo != null ? $scope.materialList[i].ordereNo
												: "").toString()
												.includes(
														$scope.searchText
																.toLowerCase())) {
									$scope.materialFilterList
											.push($scope.materialList[i]);

								}

							}

						}

						$scope.totalItems = $scope.materialFilterList.length;
						$scope.currentPage = 1;
						$scope.getTotalPages();
						$scope.getAssignedTagIds($scope.materialFilterList);

						if ($scope.materialList.length == 0
								|| $scope.materialFilterList.length == 0) {
							$scope.materialNoData = true;

						} else {
							$scope.materialNoData = false;

							if ($scope.materialFilterList.length > $scope.numPerPage) {
								$scope.materialViewData = $scope.materialFilterList
										.slice(0, $scope.numPerPage);
								$scope.PaginationTab = true;
							} else {
								$scope.materialViewData = $scope.materialFilterList;
								$scope.PaginationTab = false;
							}

						}

					}// kumar 05/Jul filter option <-

					// kumar 05/Jul sort based on EmpID option <-
					var reA = /[^a-zA-Z]/g;
					var reN = /[^0-9]/g;

					function sortAlphaNum(a1, b1) {
						var a = a1.prodId;
						var b = b1.prodId;
						var aA = a.replace(reA, "");
						var bA = b.replace(reA, "");
						if (aA === bA) {
							var aN = parseInt(a.replace(reN, ""), 10);
							var bN = parseInt(b.replace(reN, ""), 10);
							return aN === bN ? 0 : aN > bN ? 1 : -1;
						} else {
							return aA > bA ? 1 : -1;
						}
					}
					$scope.editMaterialRow = function() {
						$scope.materialEditScreen = false;
						$scope.materialEditUpdateScreen = true;
						$scope.searchButton = false;
					}
					$scope.editandsaveMaterial = function() {
						
						
						var prodId = document.getElementById("id_productIDEditMaterial");
						if (prodId.value.length == 0 ) {
							$scope.editProdIdError = true;
							$scope.editProdId_error_msg = "Please enter Product Id";
							$timeout(function() {
								$scope.editProdIdError= false;
							}, 2000);
							return;
						}
							var orderNo = document.getElementById("id_orderNumberEditMaterial");
							if (orderNo.value.length == 0 ) {
								$scope.editOrderNoError = true;
								$scope.editOrderNo_error_msg = "Please enter Order Number";
								$timeout(function() {
									$scope.editOrderNoError= false;
								}, 2000);
								return;
							}
							var issueDate = document.getElementById("id_issueDateEditMaterial");
							if (issueDate.value.length == 0 ) {
								$scope.editIssuedateError = true;
								$scope.editIssuedate_error_msg = "Please enter Issue Date";
								$timeout(function() {
									$scope.editIssuedateError= false;
								}, 2000);
								return;
							}
							var source = document.getElementById("id_sourceEditMaterial");
							if (source.value.length == 0 ) {
								$scope.editSourceError = true;
								$scope.editSource_error_msg = "Please enter Source";
								$timeout(function() {
									$scope.editSourceError= false;
								}, 2000);
								return;
							}
							var destination = document.getElementById("id_destinationEditMaterial");
							if (destination.value.length == 0 ) {
								$scope.editDestinationError = true;
								$scope.editDestination_error_msg = "Please enter Destination";
								$timeout(function() {
									$scope.editDestinationError= false;
								}, 2000);
								return;
							}
							var processControl = document.getElementById("id_processControlEditMaterial");
							if (processControl.value.length == 0 ) {
								 $scope.editProcessControlError = true;
								$scope.editProcessControl_error_msg = "Please enter Description";
								$timeout(function() {
									$scope.editProcessControlError= false;
								}, 2000);
								return;
							}
							var price = document.getElementById("id_priceEditdMaterial");
							if (price.value.length == 0 ) {
								$scope.editPriceError = true;
								$scope.editPrice_error_msg = "Please enter Price";
								$timeout(function() {
									$scope.editPriceError= false;
								}, 2000);
								return;
								
							}
							if (price.value > 100000000) {
								$scope.editPriceError = true;
								$scope.editPrice_error_msg = "Please enter value less than 100000000";
								$timeout(function() {
									$scope.editPriceError= false;
								}, 2000);
								return;
								
							}
							
							var priceRegex = /^\d+(?:\.\d{1,2})?$/;
							if (!priceRegex.test(price.value)) {
							
								$scope.editPriceError = true;
								$scope.editPrice_error_msg = "only two decimals allowed";
								$timeout(function() {
									$scope.editPriceError= false;
								}, 2000);
								return;
								
							}
							
							/*var issueDateMaterialedit =  moment(issueDate.value,"YYYY-MMM-DD").format("YYYY-MMM-DD");
							if ($scope.issueDateBeforeEdit  != issueDateMaterialedit ) {
								if (moment(issueDateMaterialedit, 'YYYY-MMM-DD')< moment(moment(new Date(), 'YYYY-MMM-DD').format('YYYY-MMM-DD'), 'YYYY-MMM-DD'))
									 {
									$scope.editIssuedateError = true;
									$scope.editIssuedate_error_msg = "Please enter Issue Date greater than or today's date";
									$timeout(function() {
										$scope.editIssuedateError= false;
									}, 2000);
									return;
									 
									 }
							}*/
							/*if($scope.editMaterial.issueDate != undefined && $scope.editMaterial.issueDate != null && 
									$scope.editMaterial.issueDate != "" 
									){
								issueDateMaterialedit = moment($scope.editMaterial.issueDate,"YYYY-MMM-DD").format("MMM DD, YYYY");
								
								}*/
						
							var editMaterialParameter = {

								"prodId" : $scope.editMaterial.prodId,
								"ordereNo" : $scope.editMaterial.ordereNo,
								"issueDate" : $scope.editMaterial.issueDate,
								"source" : $scope.editMaterial.source,
								"destination" : $scope.editMaterial.destination,
								"processControl" : $scope.editMaterial.processControl,
								"price" : $scope.editMaterial.price

							};
							if (!$rootScope.checkNetconnection()) { 
								$rootScope.alertDialogChecknet();
								return; }
							$rootScope.showloading('#loadingBar');
							$scope.promise = $http
									.put('./material', editMaterialParameter)
									.then(
											function(result) {
												$rootScope.hideloading('#loadingBar');
												
												
												if (result.data.status == "success") {
													$scope.editMaterial.issueDate = moment($scope.editMaterial.issueDate,"YYYY-MMM-DD").format("MMM DD, YYYY");
													
													$scope.materialViewData[$scope.selectedRow] = $scope.editMaterial;
													$rootScope.showAlertDialog(result.data.message);
													$scope.canceleditMaterial();
													$scope.materialEditScreen = true;
													$scope.materialEditUpdateScreen = false;
													$scope.searchButton = true;

												} else if (result.data.status == "error") {
													$rootScope.showAlertDialog(result.data.message);
													
												}

											},
											function(error) {
												$rootScope.hideloading('#loadingBar');
												$rootScope.fnHttpError(error);
											});

						

					}
					
					$scope.cancelMaterialSelection = function() { 
						$scope.selectedRow = -1;
						$scope.trackMacId = '';
					}
					
					$scope.canceleditMaterial = function() {
						$scope.cancelMaterialRow();
						$scope.materialEditScreen = true;
						$scope.materialEditUpdateScreen = false;
						$scope.searchButton = true;
						$scope.editMaterial = {};
						$scope.cancelMaterialBtn = false;
						$scope.editMaterialBtn = false;
						$scope.editMaterial = {};

						$('#id_productIDEditMaterial').val("");
						$('#id_orderNumberEditMaterial').val("");
						$('#id_issueDateEditMaterial').val("");
						$('#id_sourceEditMaterial').val("");
						$('#id_destinationEditMaterial').val("");
						$('#id_processControlEditMaterial').val("");
						$('#id_priceEditMaterial').val("");
						$timeout(
								function() {
									if ($("#idMaterialEditUpdateForm").data(
											'validator') != undefined)
										$("#idMaterialEditUpdateForm").data(
												'validator').resetForm();
								}, 100);

						$scope.selectedRow = -1;
						$scope.trackMacId = '';

					}

					// delete record

					$scope.deleteMaterial = function() {

						var name = document
								.getElementById("id_productIDEditMaterial");

						if (name.value.length == 0) {
							$scope.nameError = true;
							$scope.name_error_msg = "Please enter the product ID";
							$timeout(function() {
								$scope.nameError = false;

							}, 2000);

							return;
						}

						if ($scope.materialEditUpdateForm.$valid) {
							$
									.confirm({
										title : $rootScope.MSGBOX_TITLE,
										content : 'Are you sure to delete this record?',
										type : 'blue',
										columnClass : 'medium',
										autoClose : false,
										icon : 'fa fa-info-circle',
										buttons : {
											yes : {
												text : 'Yes',
												btnClass : 'btn-blue',
												action : function() {
													
													var productId = $scope.editMaterial.prodId
													data = {
														"prodId" : productId
													}
													if (!$rootScope.checkNetconnection()) { 
														$rootScope.alertDialogChecknet();
														return; }
													$rootScope.showloading('#loadingBar');
													$http(
															{
																method : 'DELETE',
																url : './material',
																headers : {
																	'Content-Type' : 'application/json'
																},
																data : data,

															})
															.then(
																	function(
																			response) {
																		if (response != null
																				&& response.data != null
																				&& response.data != "BAD_REQUEST") {
																			$rootScope.hideloading('#loadingBar');
																			$scope.deletedMaterial = response.data;
																			$rootScope.showAlertDialog(response.data.message);
																			$scope.getMaterialViewAllData();
																			$scope.canceleditMaterial();

																		} else {
																			$rootScope.hideloading('#loadingBar');
																			$rootScope.showAlertDialog(response.data.message);
																			$timeout(
																					function() {
																						$scope
																								.canceleditMaterial();
																					},
																					2000);
																		}
																	},
																	function(error) {
																		$rootScope.hideloading('#loadingBar');
																		$rootScope.fnHttpError(error);
																	});
												}
											},
											no : function() {

											}
										}
									});
						}

					}

					// row highlighting

					$scope.rowHighilited = function(row) {
						$scope.selectedRow = row;
						$scope.editMaterialBtn = true;
						$scope.cancelMaterialBtn = true;
						$scope.editMaterial = JSON.parse(JSON.stringify($scope.materialViewData[$scope.selectedRow]));
						$scope.editMaterial.issueDate = moment($scope.editMaterial.issueDate,"MMM DD, YYYY").format("YYYY-MMM-DD");
						//$scope.issueDateBeforeEdit = $scope.editMaterial.issueDate;
						
						if ($scope.editMaterial.assignedStatus == 'Assigned') {
							if ($scope.editMaterial.assignedTagMacId != undefined
									&& $scope.editMaterial.assignedTagMacId != '') {
								$scope.trackMacId = $scope.editMaterial.assignedTagMacId;
							} else
								$scope.trackMacId = '';
						} else
							$scope.trackMacId = '';
 
					}

					$scope.getAssignedTagIds = function(assetList) { 
						
						$scope.assignedTagMacId = '';
						
						if(assetList!=undefined && assetList!=null){
							for(var i=0; i<assetList.length; i++){
								 if(assetList[i].assignedStatus=='Assigned'){
										if(assetList[i].assignedTagMacId !=undefined 
												&& assetList[i].assignedTagMacId !=''){
											if($scope.assignedTagMacId=='')
												$scope.assignedTagMacId = assetList[i].assignedTagMacId;
											else
											{
												if (!$scope.assignedTagMacId
														.includes(assetList[i].assignedTagMacId)) {
													$scope.assignedTagMacId = $scope.assignedTagMacId
															+ ","
															+ assetList[i].assignedTagMacId;
												}
											}
												
										}
									}
							 } 
						}
						 
					}
					
					$scope.cancelMaterialRow = function() {
						$scope.selectedRow = -1;
						$scope.editMaterialBtn = false;
						$scope.cancelMaterialBtn = false;
					}

					// Anand View all Material table API call

					$scope.getMaterialViewAllData = function() {

						
						if (!$rootScope.checkNetconnection()) { 
							$rootScope.alertDialogChecknet();
							return; }
						$rootScope.showloading('#loadingBar');
						$http({
							method : 'GET',
							url : './material',

						})
								.then(
										function(response) {
											$rootScope.hideloading('#loadingBar');
											if (response != null
													&& response.data != null
													&& response.data.length != 0
													&& response.data != "BAD_REQUEST"
													&& response.data != undefined
													&& response.data.length != 0) {

												$scope.materialViewData.length = 0;
												$scope.materialList.length = 0;

												for (var i = 0; i < response.data.length; i++) {
													if (response.data[i].active == true) {
														if (response.data[i].issueDate != null
																&& response.data[i].issueDate != ""
																&& response.data[i].issueDate != undefined) {
															var issueDateMaterial = moment(
																	response.data[i].issueDate,"YYYY-MMM-DD")
																	.format(
																			"MMM DD, YYYY");
														}

														$scope.materialList
																.push({
																	'prodId' : response.data[i].prodId,
																	'ordereNo' : response.data[i].ordereNo,
																	'issueDate' : issueDateMaterial,
																	'source' : response.data[i].source,
																	'destination' : response.data[i].destination,
																	'processControl' : response.data[i].processControl,
																	'assignedTagMacId' : response.data[i].assignedTagMacId,
																	'assignedStatus' : response.data[i].assignedStatus,
																	'price' : response.data[i].price,
																});
													}
												}

												$scope.materialList
														.sort(sortAlphaNum);
												$scope.totalItems = $scope.materialList.length;
												if ($scope.materialList.length == 0) {
													$scope.materialNoData = true;
												} else {
													$scope.materialNoData = false;
												}
												$scope.getAssignedTagIds($scope.materialList);
												$scope.getTotalPages();
												$scope.fnSearchFilterOption();

											} else {
												$scope.materialNoData = true;

											}
										},
										function error(response) {
											$rootScope.hideloading('#loadingBar');
											$rootScope.fnHttpError(error);
										});
					}

					// $scope.getMaterialViewAllData();

					// View all employee table ends here

					// input field validation
					$(
							"#productID, #source, #id_destinationAdMaterial, #processControl")
							.keypress(
									function(e) {
										// if the letter is not alphabets then
										// display
										// error
										// and don't type anything
										$return = ((e.which > 64) && (e.which < 91))
												|| ((e.which > 96) && (e.which < 123))
												|| (e.which == 8)
												|| (e.which == 32)
												|| (e.which >= 48 && e.which <= 57)
										if (!$return)

										{
											// display error message

											return false;
										}
									});

					$("#orderNumber").keypress(
							function(e) {
								// if the letter is not digit then display error
								// and don't type anything

								if (e.which != 8 && e.which != 0
										&& (e.which < 48 || e.which > 57)) {

									return false;
								}
							});
					$("#id_priceAddMaterial").keypress(
							function(e) {
								// if the letter is not digit and allows .  then display error
								// and don't type anything

								if (e.which != 46 && e.which > 31
										&& (e.which < 48 || e.which > 57)) {

									return false;
								}
								
							});
					
					$scope.addMultipleMaterial = function() {
						
						var mulProdId = document.getElementById("id_multipleMaterialProductId");
						if (mulProdId.value == "Select an option" ) {
							$scope.addMulProdIdError = true;
							$scope.addMulProdId_error_msg = "Please select Product Id";
							$timeout(function() {
								$scope.addMulProdIdError= false;
							}, 2000);
							return;
						}
						var mulOrderNo = document.getElementById("id_multipleMaterialorderNumber");
						if (mulOrderNo.value == "Select an option" ) {
							$scope.addMulOrderNoError = true;
							$scope.addMulOrderNo_error_msg = "Please select Order Number";
							$timeout(function() {
								$scope.addMulOrderNoError= false;
							}, 2000);
							return;
						}
						var mulIssueDate = document.getElementById("id_multipleMaterialissueDate");
						if (mulIssueDate.value == "Select an option" ) {
							$scope.addMulIssueDateError = true;
							$scope.addMulIssueDate_error_msg = "Please select Issue Date";
							$timeout(function() {
								$scope.addMulIssueDateError= false;
							}, 2000);
							return;
						}
						var mulSource = document.getElementById("id_multipleMaterialSource");
						if (mulSource.value == "Select an option" ) {
							$scope.addMulSourceError = true;
							$scope.addMulSource_error_msg = "Please select Source";
							$timeout(function() {
								$scope.addMulSourceError= false;
							}, 2000);
							return;
						}
						var mulDestination = document.getElementById("id_multipleMaterialDestination");
						if (mulDestination.value == "Select an option" ) {
							$scope.addMulDestinationError = true;
							$scope.addMulDestination_error_msg = "Please select Destination";
							$timeout(function() {
								$scope.addMulDestinationError= false;
							}, 2000);
							return;
						}
						var mulDescription = document.getElementById("id_multipleMaterialProcessControl");
						if (mulDescription.value == "Select an option" ) {
							$scope.addMulDescriptionError = true;
							$scope.addMulDescription_error_msg = "Please select Description";
							$timeout(function() {
								$scope.addMulDescriptionError= false;
							}, 2000);
							return;
						}
						var mulPrice = document.getElementById("id_multipleMaterialPrice");
						if (mulPrice.value == "Select an option" ) {
							$scope.addMulPriceError = true;
							$scope.addMulPrice_error_msg = "Please select Price";
							$timeout(function() {
								$scope.addMulPriceError= false;
							}, 2000);
							return;
						}
						/*$("#addMultiMaterialForm").validate({

							// Rules for form validation
							rules : {
								productID : {
									required : true
								},
								orderNumber : {
									required : true
								},
								issueDate : {
									required : true
								},
								source : {
									required : true
								},
								destination : {
									required : true
								},
								processControl : {
									required : true
								}
							},
							// Messages for form validation
							messages : {
								productID : {
									required : 'Please select product ID',
								},
								orderNumber : {
									required : 'Please select order number'
								},
								issueDate : {
									required : 'Please select issue date'
								},
								source : {
									required : 'Please select source'
								},
								destination : {
									required : 'Please select destination'
								},
								processControl : {
									required : 'Please select description'
								},
								price : {
									required : 'Please select price'
								}
							
							},

							// Do not change code below
							errorPlacement : function(error, element) {
								error.insertAfter(element.parent());
							}
						});*/
						if ($scope.addMultiMaterialForm.$valid) {
							
							if($scope.multiMaterialAddScreen){
								if ($scope.multiMaterial.productID != undefined && $scope.multiMaterial.orderNumber != undefined &&
										$scope.multiMaterial.productID == $scope.multiMaterial.orderNumber) {
									 
									$rootScope.hideloading('#loadingBar');
									$rootScope.showAlertDialog("Select different field for Product id and Order Number");
							          return;
									
								}
								else if ($scope.multiMaterial.productID != undefined && $scope.multiMaterial.issueDate != undefined &&
										$scope.multiMaterial.productID == $scope.multiMaterial.issueDate) {
									 
									
									$rootScope.hideloading('#loadingBar');	
									$rootScope.showAlertDialog("Select different field for Product id and Issue Date");
							         return;
									
								}
								else if ($scope.multiMaterial.productID != undefined && $scope.multiMaterial.source != undefined &&
										$scope.multiMaterial.productID == $scope.multiMaterial.source) {
									 
									$rootScope.hideloading('#loadingBar');
									$rootScope.showAlertDialog("Select different field for Product id and Source");
							         return;
									
								}
								else if ($scope.multiMaterial.productID != undefined && $scope.multiMaterial.destination != undefined &&
										$scope.multiMaterial.productID == $scope.multiMaterial.destination) {
									 
									$rootScope.hideloading('#loadingBar');	
									$rootScope.showAlertDialog("Select different field for Product id and Destination");
							        return;
									
								}
								else if ($scope.multiMaterial.productID != undefined && $scope.multiMaterial.processControl != undefined &&
										$scope.multiMaterial.productID == $scope.multiMaterial.processControl) {
									 
									$rootScope.hideloading('#loadingBar');	
									$rootScope.showAlertDialog("Select different field for Product id and Description");
							         return;
											 
									
								}
								else if ($scope.multiMaterial.productID != undefined && $scope.multiMaterial.price != undefined &&
										$scope.multiMaterial.productID == $scope.multiMaterial.price) {
									 
									$rootScope.hideloading('#loadingBar');
									$rootScope.showAlertDialog("Select different field for Product id and Price");
							         return;
											 
									
								}
								else if ($scope.multiMaterial.orderNumber != undefined && $scope.multiMaterial.issueDate != undefined &&
										$scope.multiMaterial.orderNumber == $scope.multiMaterial.issueDate) {
									 
									$rootScope.hideloading('#loadingBar');
									$rootScope.showAlertDialog("Select different field for Order Number and Issue Date");
							         return;
									
								}
								else if ($scope.multiMaterial.orderNumber != undefined && $scope.multiMaterial.source != undefined &&
										$scope.multiMaterial.orderNumber == $scope.multiMaterial.source) {
									 
									$rootScope.hideloading('#loadingBar');	
									$rootScope.showAlertDialog("Select different field for Order Number and Source");
							         return;
											 
									
								}
								else if ($scope.multiMaterial.orderNumber != undefined && $scope.multiMaterial.destination != undefined &&
										$scope.multiMaterial.orderNumber == $scope.multiMaterial.destination) {
									 
									$rootScope.hideloading('#loadingBar');
									$rootScope.showAlertDialog("Select different field for Order Number and Destination");
							         return;
											 
									
								}
								else if ($scope.multiMaterial.orderNumber != undefined && $scope.multiMaterial.processControl != undefined &&
										$scope.multiMaterial.orderNumber == $scope.multiMaterial.processControl) {
									 
									$rootScope.hideloading('#loadingBar');	
									$rootScope.showAlertDialog("Select different field for Order Number and Description");
							         return;
											 
									
								}
								else if ($scope.multiMaterial.orderNumber != undefined && $scope.multiMaterial.price != undefined &&
										$scope.multiMaterial.orderNumber == $scope.multiMaterial.price) {
									 
									$rootScope.hideloading('#loadingBar');	
									$rootScope.showAlertDialog("Select different field for Order Number and Price");
							        return;
											 
									
								}
								else if ($scope.multiMaterial.issueDate != undefined && $scope.multiMaterial.source != undefined &&
										$scope.multiMaterial.issueDate == $scope.multiMaterial.source) {
									 
									$rootScope.hideloading('#loadingBar');
									$rootScope.showAlertDialog("Select different field for Issue Date and Source");
							         return;
											 
									
								}
								else if ($scope.multiMaterial.issueDate != undefined && $scope.multiMaterial.destination != undefined &&
										$scope.multiMaterial.issueDate == $scope.multiMaterial.destination) {
									 
									$rootScope.hideloading('#loadingBar');
									$rootScope.showAlertDialog("Select different field for Issue Date and Destination");
							         return;
											 
									
								}
								else if ($scope.multiMaterial.issueDate != undefined && $scope.multiMaterial.processControl != undefined &&
										$scope.multiMaterial.issueDate == $scope.multiMaterial.processControl) {
									 
									$rootScope.hideloading('#loadingBar');
									$rootScope.showAlertDialog("Select different field for Issue Date and Description");
							        return;
											 
									
								}
								else if ($scope.multiMaterial.issueDate != undefined && $scope.multiMaterial.price != undefined &&
										$scope.multiMaterial.issueDate == $scope.multiMaterial.price) {
									 
									$rootScope.hideloading('#loadingBar');
									$rootScope.showAlertDialog("Select different field for Issue Date and Price");
							         return;
											 
									
								}else if ($scope.multiMaterial.source != undefined && $scope.multiMaterial.destination != undefined &&
										$scope.multiMaterial.source == $scope.multiMaterial.destination) {
									 
									$rootScope.hideloading('#loadingBar');
									$rootScope.showAlertDialog("Select different field for Source Date and Destination");
							        return;
											 
									
								}else if ($scope.multiMaterial.source != undefined && $scope.multiMaterial.processControl != undefined &&
										$scope.multiMaterial.source == $scope.multiMaterial.processControl) {
									 
									$rootScope.hideloading('#loadingBar');	
									$rootScope.showAlertDialog("Select different field for Source Date and Description");
							         return;
											 
									
								}else if ($scope.multiMaterial.source != undefined && $scope.multiMaterial.price != undefined &&
										$scope.multiMaterial.source == $scope.multiMaterial.price) {
									 
									$rootScope.hideloading('#loadingBar');	
									$rootScope.showAlertDialog("Select different field for Source Date and Price");
							        return;
									
								}else if ($scope.multiMaterial.destination != undefined && $scope.multiMaterial.processControl != undefined &&
										$scope.multiMaterial.destination == $scope.multiMaterial.processControl) {
									 
									$rootScope.hideloading('#loadingBar');	
									$rootScope.showAlertDialog("Select different field for Destination Date and Description");
							        return;
								}else if ($scope.multiMaterial.destination != undefined && $scope.multiMaterial.price != undefined &&
										$scope.multiMaterial.source == $scope.multiMaterial.price) {
									 
									$rootScope.hideloading('#loadingBar');
									$rootScope.showAlertDialog("Select different field for Destination Date and Price");
							         return;
								}else if ($scope.multiMaterial.processControl != undefined && $scope.multiMaterial.price != undefined &&
										$scope.multiMaterial.processControl == $scope.multiMaterial.price) {
									 
									$rootScope.hideloading('#loadingBar');
									$rootScope.showAlertDialog("Select different field for Description and Price");
							         return;
								}else {
									$scope.Upload();
								}
								
								
								
							}
							
						}else{

							 
							$rootScope.hideloading('#loadingBar');
							$rootScope.showAlertDialog("Select Mandatory fields");
					         return;
							
						}
					}
					// Anand 2-6-21 add employee API call and validation
					$scope.addMaterial = function() {
								var prodId = document.getElementById("id_productIDAddMaterial");
								if (prodId.value.length == 0 ) {
									$scope.addProdIdError = true;
									$scope.addProdId_error_msg = "Please enter Product Id";
									$timeout(function() {
										$scope.addProdIdError= false;
									}, 2000);
									return;
								}
									var orderNo = document.getElementById("id_orderNumberAddMaterial");
									if (orderNo.value.length == 0 ) {
										$scope.addOrderNoError = true;
										$scope.addOrderNo_error_msg = "Please enter Order Number";
										$timeout(function() {
											$scope.addOrderNoError= false;
										}, 2000);
										return;
									}
									var issueDate = document.getElementById("id_issueDateAddmaterial");
									if (issueDate.value.length == 0 ) {
										$scope.addIssuedateError = true;
										$scope.addIssuedate_error_msg = "Please enter Issue Date";
										$timeout(function() {
											$scope.addIssuedateError= false;
										}, 2000);
										return;
									}
									var source = document.getElementById("id_sourceAddMaterial");
									if (source.value.length == 0 ) {
										 $scope.addSourceError = true;
										$scope.addSource_error_msg = "Please enter Source";
										$timeout(function() {
											$scope.addSourceError= false;
										}, 2000);
										return;
									}
									var destination = document.getElementById("id_destinationAddMaterial");
									if (destination.value.length == 0 ) {
										$scope.addDestinationError = true;
										$scope.addDestination_error_msg = "Please enter Destination";
										$timeout(function() {
											$scope.addDestinationError= false;
										}, 2000);
										return;
									}
									var processControl = document.getElementById("id_processControlAddMaterial");
									if (processControl.value.length == 0 ) {
										$scope.addProcessControlError = true;
										$scope.addProcessControl_error_msg = "Please enter Description";
										$timeout(function() {
											$scope.addProcessControlError= false;
										}, 2000);
										return;
									}
									var price = document.getElementById("id_priceAddMaterial");
									if (price.value.length == 0 ) {
										$scope.addPriceError = true;
										$scope.addPrice_error_msg = "Please enter Price";
										$timeout(function() {
											$scope.addPriceError= false;
										}, 2000);
										return;
										
									}
									if (price.value > 100000000) {
										$scope.addPriceError = true;
										$scope.addPrice_error_msg = "Please enter value less than 100000000";
										$timeout(function() {
											$scope.addPriceError= false;
										}, 2000);
										return;
										
									}
									
									var priceRegex = /^\d+(?:\.\d{1,2})?$/;
									if (!priceRegex.test(price.value)) {
									
										$scope.addPriceError = true;
										$scope.addPrice_error_msg = "only two decimals allowed";
										$timeout(function() {
											$scope.addPriceError= false;
										}, 2000);
										return;
										
									}
								var addMaterialParameter = {
									"prodId" : $scope.authen.productID,
									"ordereNo" : $scope.authen.orderNumber,
									"issueDate" : $scope.authen.issueDate,
									"source" : $scope.authen.source,
									"destination" : $scope.authen.destination,
									"processControl" : $scope.authen.processControl,
									"price" : $scope.authen.price
								};
								if (!$rootScope.checkNetconnection()) { 
									$rootScope.alertDialogChecknet();
									return; }
								$rootScope.showloading('#loadingBar');
								$scope.promise = $http
										.post('./material', addMaterialParameter)
										.then(
												function(result) {
													
													$rootScope.hideloading('#loadingBar');
													if (result.data.status == "success") {
														$rootScope.showAlertDialog(result.data.message);
														$scope.cancelMaterial();
														
													} else if (result.data.status == "error") {
														$rootScope.showAlertDialog(result.data.message);
														
													}

												
							 
											},
											function(error) {
												$rootScope.hideloading('#loadingBar');
												$rootScope.fnHttpError(error);
											});
						

					}
					$scope.cancelMaterial = function() {
						  
						$scope.authen.productID ="";
						$scope.authen.orderNumber ="";
						$scope.authen.source ="";
						$scope.authen.destination ="";
						$scope.authen.processControl ="";
						$scope.authen.price ="";
						
						$('#id_issueDateAddmaterial').val(
								moment(new Date()).format("YYYY-MMM-DD"));
						$scope.authen.issueDate = moment(new Date()).format(
								"YYYY-MMM-DD");
						
						
					}
					$scope.cancelMultipleMaterial = function() {
						
						$("#id_multipleMaterialProductId").val("Select an option"); 
						$("#id_multipleMaterialorderNumber").val("Select an option");
						$("#id_multipleMaterialissueDate").val("Select an option");
						$("#id_multipleMaterialSource").val("Select an option");
						$("#id_multipleMaterialDestination").val("Select an option");
						$("#id_multipleMaterialProcessControl").val("Select an option");
						$("#id_multipleMaterialPrice").val("Select an option");
						
						$scope.multiMaterialAddScreen = false;
						$scope.materialMultipleAddFileUploadScreen = true;
						
						
					}
					// pagination button

					$scope.currentPage = 1;
					$scope.numPerPage = 10;
					$scope.maxSize = 3;
					$scope.totalItems = 0;

					$scope
							.$watch(
									'currentPage + numPerPage',
									function() {
										var begin = (($scope.currentPage - 1) * $scope.numPerPage), end = begin
												+ $scope.numPerPage;

										if ($scope.searchText == '')
											$scope.materialViewData = $scope.materialList
													.slice(begin, end);
										else
											$scope.materialViewData = $scope.materialFilterList
													.slice(begin, end);

										$scope.enablePageButtons();
									});

					$scope.currentPage = 1;
					$scope.maxSize = 3;
					$scope.numPerPage = 10;
					$scope.totalItems = 0;
					$scope.totalPages = 0;

					$scope.hasNext = true;
					$scope.hasPrevious = true;
					$scope.isFirstPage = true;
					$scope.isLastPage = true;
					$scope.serial = 1;

					$scope
							.$watch(
									"currentPage",
									function(newVal, oldVal) {
										if (newVal !== oldVal) {

											if (newVal > $scope.totalPages) {
												$scope.currentPage = $scope.totalPages;
											} else {
												$scope.currentPage = newVal;
											}

											$scope.serial = (($scope.currentPage - 1) * $scope.numPerPage) + 1;
											$scope.enablePageButtons();
										}
									});

					$scope.enablePageButtons = function() {
						$scope.serial = (($scope.currentPage - 1) * $scope.numPerPage) + 1;
						$scope.selectedRow = -1;
						if ($scope.totalPages <= 1) {
							$scope.hasPrevious = true;
							$scope.isFirstPage = true;
							$scope.hasNext = true;
							$scope.isLastPage = true;

						} else if ($scope.totalPages > 1
								&& $scope.currentPage == $scope.totalPages) {

							$scope.hasPrevious = false;
							$scope.isFirstPage = false;
							$scope.hasNext = true;
							$scope.isLastPage = true;
						} else if ($scope.currentPage != 1
								&& $scope.currentPage < $scope.totalPages) {

							$scope.hasPrevious = false;
							$scope.isFirstPage = false;
							$scope.hasNext = false;
							$scope.isLastPage = false;

						} else {
							$scope.hasPrevious = true;
							$scope.isFirstPage = true;
							$scope.hasNext = false;
							$scope.isLastPage = false;
						}
 
					};

					$scope.getTotalPages = function() {

						if (($scope.totalItems % $scope.numPerPage) > 0)
							$scope.totalPages = parseInt($scope.totalItems
									/ $scope.numPerPage) + 1;
						else
							$scope.totalPages = parseInt($scope.totalItems
									/ $scope.numPerPage);

						$scope.enablePageButtons();

					};

					$scope.goFirstPage = function() {
						$scope.currentPage = 1;
						$scope.enablePageButtons();
					};

					$scope.goPreviousPage = function() {
						if ($scope.currentPage > 1)
							$scope.currentPage = $scope.currentPage - 1;
						$scope.enablePageButtons();
					};

					$scope.goNextPage = function() {
						if ($scope.currentPage <= $scope.totalPages)
							$scope.currentPage = $scope.currentPage + 1;
						$scope.enablePageButtons();
					};

					$scope.goLastPage = function() {
						$scope.currentPage = $scope.totalPages;
						$scope.enablePageButtons();
					};
					$scope.displayMaterialViewScreen = function() {
						$scope.selectedRow = -1;
						$scope.currentPage = 1;
						$scope.materialList = [];
						$scope.materialList.length = 0;
						$scope.materialViewData = [];
						$scope.materialViewData.length = 0;
						$scope.searchButton = true;
						$scope.showSearchIcon = true;
						$scope.showMultipleSearch = false;

						$scope.materialViewScreen = true;
						$scope.materialEditScreen = false;
						$scope.materialEditUpdateScreen = false;
						$scope.materialAddScreen = false;
						$scope.materialMultipleAddFileUploadScreen = false;
						$scope.multiMaterialAddScreen = false;
						$("#idMaterialView").addClass("active");
						$("#idMaterialAdd").removeClass("active");
						$("#idMaterialEdit").removeClass("active");
						$("#idBulkMaterialAdd").removeClass("active");

						$scope.getMaterialViewAllData();

					};
					$scope.displayMaterialAddScreen = function() {
						$scope.cancelMaterial();
						$scope.materialViewScreen = false;
						$scope.materialEditScreen = false;
						$scope.materialEditUpdateScreen = false;
						$scope.materialAddScreen = true;
						$scope.multiMaterialAddScreen = false;
						$scope.searchButton = false;
						$scope.materialMultipleAddFileUploadScreen = false;
						$scope.showSearchIcon = false;
						$scope.showMultipleSearch = false;
						$("#idMaterialView").removeClass("active");
						$("#idMaterialAdd").addClass("active");
						$("#idMaterialEdit").removeClass("active");
						$("#idBulkMaterialAdd").removeClass("active");
						/*
						 * $('#idPFAStartDatePicker').datetimepicker({ format :
						 * 'LL', minDate : new Date(), });
						 */
						$('#id_issueDateAddmaterial').val(
								moment(new Date()).format("YYYY-MMM-DD"));
						$scope.authen.issueDate = moment(new Date()).format(
								"YYYY-MMM-DD");
					};
					$scope.displayMultipleMaterialAddScreen = function() {
						$scope.materialViewScreen = false;
						$scope.materialEditScreen = false;
						$scope.materialEditUpdateScreen = false;
						$scope.materialAddScreen = false;
						$scope.searchButton = false;
						if(!$scope.multiMaterialAddScreen )
						$scope.materialMultipleAddFileUploadScreen = true;

						$scope.showSearchIcon = false;
						$scope.showMultipleSearch = false;

						$("#idMaterialView").removeClass("active");
						$("#idMaterialAdd").removeClass("active");
						$("#idMaterialEdit").removeClass("active");
						$("#idBulkMaterialAdd").addClass("active");

					}
					$scope.displayMaterialEditScreen = function() {
						$scope.selectedRow = -1;
						$scope.currentPage = 1;
						$scope.materialList = [];
						$scope.materialList.length = 0;
						$scope.materialViewData = [];
						$scope.materialViewData.length = 0;
						$scope.searchButton = true;
						$scope.showSearchIcon = true;
						$scope.showMultipleSearch = false;

						$scope.materialViewScreen = false;
						$scope.materialEditScreen = true;
						$scope.materialEditUpdateScreen = false;
						$scope.materialAddScreen = false;
						$scope.multiMaterialAddScreen = false;
						$scope.materialMultipleAddFileUploadScreen = false;
						$("#idMaterialView").removeClass("active");
						$("#idMaterialAdd").removeClass("active");
						$("#idMaterialEdit").addClass("active");
						$("#idBulkMaterialAdd").removeClass("active");
						$scope.getMaterialViewAllData();

					};

					// to display enabled sub menus
					if ($scope.user.capabilityDetails != undefined
							&& $scope.user.capabilityDetails != null) {
						if ($scope.user.capabilityDetails.WMVA != 0) {
							$scope.displayMaterialViewScreen();
						}
						if ($scope.user.capabilityDetails.WMVA != 1
								&& $scope.user.capabilityDetails.WMMA != 1) {
							$scope.displayMaterialAddScreen();
						}
						if ($scope.user.capabilityDetails.WMVA != 1
								&& $scope.user.capabilityDetails.WMAD != 1
								&& $scope.user.capabilityDetails.WMED != 1) {
							$scope.displayMultipleMaterialAddScreen();
						}
						if ($scope.user.capabilityDetails.WMVA != 1
								&& $scope.user.capabilityDetails.WMMA != 1
								&& $scope.user.capabilityDetails.WMAD != 1) {
							$scope.displayMaterialEditScreen();
						}

					}
					$scope.getJsondata = function() {
						$scope.jsonContent = [];
						$scope.csvContent =[];
						$scope.ext = "";
						var files = document.getElementById('upfile').files;
						var jsonfileName = document.getElementById('upfile').value;
						$scope.SelectedFile = jsonfileName;
						$scope.ext = jsonfileName.substring(jsonfileName.lastIndexOf('.') + 1); 
		                 if (files.length <= 0) {
							    return false;
							  }     						  
		                   if($scope.ext.toLowerCase() == 'json'){
		                	   		  $scope.keyNames =[];
		                	   		$scope.jsonDataArray.length = 0;
									  $scope.errorImport= false;	
									  var fr = new FileReader();										 					 
									  fr.onload = function(e) { 
										  try {
											  multipleMaterials = new Array(); 
											  
		  									 $scope.jsonContent = JSON.parse(e.target.result);
		  									//console.log($scope.jsonContent);
		  									 getAllKeys($scope.jsonDataArray,$scope.jsonContent, '');
		  									for (var i = 0; i < $scope.jsonDataArray.length; i++) {
												if (!$scope.keyNames.includes($scope.jsonDataArray[i]['key']))
													$scope.keyNames.push($scope.jsonDataArray[i]['key']);
												
		  									}
		  								//console.log($scope.keyNames);
		  									
											
		  									
										  } catch(e) {
											  $rootScope.alertErrorDialog(e); // error in the above string (in this case, yes)!
											  $('input[type=file]').val('');
										    }
										 // $scope.keyNames = headerCSV;
		  								}
									 //$scope.sendInsertData(multipleMaterials);
		  								fr.readAsText(files.item(0));	          							 
		  				  }else if($scope.ext.toLowerCase() == 'csv'){
		  					$scope.jsonDataArray = [];
		  					$scope.keyNames =[];
		  					$scope.csvContent =[];
		    					$scope.errorImport= false;	
		  					  var fr = new FileReader();										 					 
		  					  fr.onload = function(e) { 
		  						  try {
		  							  var csv = e.target.result;
		  							$scope.csvContent = csv;
		  							var headerCSV,dataCSV,headerCSVlength=[];
		  							$scope.parsedCSVfile = Papa.parse(csv, {
		  								header: true,
		  								complete: function(results) {
											 headerCSV = results.data[0];
											 dataCSV = results.data;
											headerCSVlength =headerCSV;
											},
											header: true,
											skipEmptyLines: true,
		  							
										});
		  							//$scope.jsonDataArray=dataCSV;
		  							 getAllKeys($scope.jsonDataArray,dataCSV, '');
		  							for (var i = 0; i < $scope.jsonDataArray.length; i++) {
										if (!$scope.keyNames.includes($scope.jsonDataArray[i]['key']))
											$scope.keyNames.push($scope.jsonDataArray[i]['key']);
										
  									}
		  							//console.log($scope.jsonDataArray);
		  							
		  					          
		  						  } catch(e) {
		  							  $rootScope.alertErrorDialog(e); // error in the above string (in this case, yes)!
		  							  $('input[type=file]').val('');
		  						    }
		  						 
		  						//$scope.keyNames = headerCSV;
		  						}
		  						fr.readAsText(files.item(0));
		  					
		  				  }
					}	
					$scope.importMaterialDetailFile = function(){
						 var fileVal = $('#upfile').val(); 
					        if(fileVal=='' || ($scope.ext.toLowerCase() != 'csv' && $scope.ext.toLowerCase() != 'json')) 
					        { 
					        	$rootScope.alertErrorDialog("Please select a valid CSV or JSON file");
								return;
					        }
					        else if($scope.ext.toLowerCase() == 'csv' || $scope.ext.toLowerCase() == 'json' ){
					        	$scope.materialMultipleAddFileUploadScreen = false;
					        	$scope.multiMaterialAddScreen = true;
							$timeout(function() {
								$("#id_multipleMaterialProductId").val("Select an option"); 
								$("#id_multipleMaterialorderNumber").val("Select an option");
								$("#id_multipleMaterialissueDate").val("Select an option");
								$("#id_multipleMaterialSource").val("Select an option");
								$("#id_multipleMaterialDestination").val("Select an option");
								$("#id_multipleMaterialProcessControl").val("Select an option");
								$("#id_multipleMaterialPrice").val("Select an option");
							}, 200);
				        }
						
					}
					function getAllKeys(keys, obj, path) {
						for (key in obj) {
							var currpath = path + '/' + key;

							if (typeof (obj[key]) == 'object'
									|| (obj[key] instanceof Array))
								getAllKeys(keys, obj[key], currpath);
							else
								keys.push({
									'key' : key,
									"path" : currpath,
									"value" : obj[key]
								});
						}
					}
					$scope.Upload = function(){
						//$scope.jsonContent = [];
						$scope.ext ="";
						var files = document.getElementById('upfile').files;
						var jsonfileName = document.getElementById('upfile').value;
						$scope.SelectedFile = jsonfileName;
						$scope.ext = jsonfileName.substring(jsonfileName.lastIndexOf('.') + 1); 
						 var fr = new FileReader();
		                 if (files.length <= 0) {
							    return false;
							  }     	
		                 if($scope.ext.toLowerCase() == 'csv'){
		                	 if($scope.addMultiMaterialForm.$valid){
		                		var headerCSV,dataCSV,headerCSVlength=[];
	  							$scope.parsedCSVfile = Papa.parse($scope.csvContent, {
									complete: function(results) {
										 headerCSV = results.data[0];
										 dataCSV = results.data;
										headerCSVlength =headerCSV;
										
									},
									quoteChar: '"',
									header: true,
									skipEmptyLines: true
									
									});
	  							//console.log($scope.csvContent);

			                	   multipleMaterials = new Array(); 
			                	   $scope.jsonMultipleMaterials =[];
			                	   
			                	   var multiMaterial = {};
									for (var i = 0; i < $scope.jsonDataArray.length; i++) {

										var json = $scope.jsonDataArray[i];

										if (json['key'] == $scope.multiMaterial.productID) {
											multiMaterial.prodId = json['value'];
										} else if (json['key'] == $scope.multiMaterial.orderNumber) {
											multiMaterial.ordereNo = json['value'];
										} else if (json['key'] == $scope.multiMaterial.issueDate) {
											multiMaterial.issueDate = json['value'];
										} else if (json['key'] == $scope.multiMaterial.source) {
											multiMaterial.source = json['value'];
										} else if (json['key'] == $scope.multiMaterial.destination) {
											multiMaterial.destination = json['value'];
										} else if (json['key'] == $scope.multiMaterial.processControl) {
											multiMaterial.processControl = json['value'];
										} else if (json['key'] == $scope.multiMaterial.price) {
											multiMaterial.price = json['value'];
										}
										if ($scope.multiMaterial.productID != undefined
												&& $scope.multiMaterial.orderNumber != undefined
												&& $scope.multiMaterial.issueDate != undefined
												&& $scope.multiMaterial.source != undefined
												&& $scope.multiMaterial.destination != undefined
												&& $scope.multiMaterial.processControl != undefined
												&& $scope.multiMaterial.price != undefined) {
											if (multiMaterial.hasOwnProperty('prodId')
													&& multiMaterial.hasOwnProperty('ordereNo')
													&& multiMaterial
															.hasOwnProperty('issueDate')
													&& multiMaterial.hasOwnProperty('source')
													&& multiMaterial
															.hasOwnProperty('destination')
													&& multiMaterial.hasOwnProperty('processControl')
													&& multiMaterial.hasOwnProperty('price')) {
												$scope.jsonMultipleMaterials.push(multiMaterial);
												multiMaterial = {};

											}
										}
									}
									$scope.multipleMaterialValidation();
			                	  // console.log($scope.jsonMultipleMaterials);
			                	   
				                   $scope.sendInsertData($scope.jsonMultipleMaterials);
										fr.readAsText(files.item(0));
			                  
		                 }
	  							
		                 }
		                 else if($scope.ext.toLowerCase() == 'json'){
		                	 if($scope.addMultiMaterialForm.$valid){
		                	   multipleMaterials = new Array(); 
		                	   $scope.jsonMultipleMaterials =[];
		                	   
		                	   var multiMaterial = {};
								for (var i = 0; i < $scope.jsonDataArray.length; i++) {

									var json = $scope.jsonDataArray[i];

									if (json['key'] == $scope.multiMaterial.productID) {
										multiMaterial.prodId = json['value'];
									} else if (json['key'] == $scope.multiMaterial.orderNumber) {
										multiMaterial.ordereNo = json['value'];
									} else if (json['key'] == $scope.multiMaterial.issueDate) {
										multiMaterial.issueDate = json['value'];
									} else if (json['key'] == $scope.multiMaterial.source) {
										multiMaterial.source = json['value'];
									} else if (json['key'] == $scope.multiMaterial.destination) {
										multiMaterial.destination = json['value'];
									} else if (json['key'] == $scope.multiMaterial.processControl) {
										multiMaterial.processControl = json['value'];
									} else if (json['key'] == $scope.multiMaterial.price) {
										multiMaterial.price = json['value'];
									}
									if ($scope.multiMaterial.productID != undefined
											&& $scope.multiMaterial.orderNumber != undefined
											&& $scope.multiMaterial.issueDate != undefined
											&& $scope.multiMaterial.source != undefined
											&& $scope.multiMaterial.destination != undefined
											&& $scope.multiMaterial.processControl != undefined
											&& $scope.multiMaterial.price != undefined) {
										if (multiMaterial.hasOwnProperty('prodId')
												&& multiMaterial.hasOwnProperty('ordereNo')
												&& multiMaterial
														.hasOwnProperty('issueDate')
												&& multiMaterial.hasOwnProperty('source')
												&& multiMaterial
														.hasOwnProperty('destination')
												&& multiMaterial.hasOwnProperty('processControl')
												&& multiMaterial.hasOwnProperty('price')) {
											$scope.jsonMultipleMaterials.push(multiMaterial);
											multiMaterial = {};

										}
									}
								}
								$scope.multipleMaterialValidation();
		                	  // console.log($scope.jsonMultipleMaterials);
		                	   
			                   $scope.sendInsertData($scope.jsonMultipleMaterials);
									fr.readAsText(files.item(0));
		                 } 
		                   }
		                   
					}
					$scope.bulkTagCloseBtnClik = function() {
						$('#bulkTagUpload_modal').modal('hide');
						$scope.errorArray = [];
					};
					$scope.sendInsertData = function(data) {
						if (!$rootScope.checkNetconnection()) { 
							$rootScope.alertDialogChecknet();
							return; } 
						if ($scope.errorArray.length > 0) {

							$('#bulkTagUpload_modal').modal('show');

						} else if (data.length > 0) {
							 
						$rootScope.showloading('#loadingBar');
								$http({
								method : 'POST',
								data : data,
								url : './multipleMaterial',
								
								})
								.then(
										function(result) {
											$rootScope.hideloading('#loadingBar');
											if (result.data.status == "success") {
												$rootScope.showAlertDialog(result.data.message);
												$scope.cancelMaterial();
												
											} else if (result.data.status == "error") {
												$rootScope.showAlertDialog(result.data.message);
												
											}				
											},
										function(error) {
														$rootScope.hideloading('#loadingBar');
														$rootScope.fnHttpError(error);
											});
								
						 }else {
								//alert('Invalid Data.');
							 $rootScope.showAlertDialog("Invalid Data");
								
								}
						
					};

					// multisearch code starts
					$scope.multipleSearch = function() {
						$scope.showMultipleSearch = true;
						$scope.searchButton = false
						$scope.firstMultiSearchDropDown = "All";
						$scope.secondMultiSearchDropDown = "All";
						$scope.varANDORDropDown = "AND";
						$scope.firstSearchText = "";
						$scope.secondSearchText = "";
					}
					$scope.closeMultiSearchView = function() {
						$scope.showMultipleSearch = false;
						$scope.searchButton = true

						$scope.materialFilterList = $scope.materialList;
						$scope.totalItems = $scope.materialFilterList.length;
						$scope.currentPage = 1;
						$scope.getTotalPages();
						$scope.getAssignedTagIds($scope.materialFilterList);

						if ($scope.materialList.length == 0
								|| $scope.materialFilterList.length == 0) {
							$scope.materialNoData = true;

						} else {
							$scope.materialNoData = false;

							if ($scope.materialFilterList.length > $scope.numPerPage) {
								$scope.materialViewData = JSON.parse(JSON
										.stringify($scope.materialFilterList
												.slice(0, $scope.numPerPage)));
								$scope.PaginationTab = true;
							} else {
								$scope.materialViewData = JSON.parse(JSON
										.stringify($scope.materialFilterList));
								$scope.PaginationTab = false;
							}
						}
					}

					$scope.fnfirstSearchANDData = function() {

						$scope.firstSearchANDDataArray = [];
						for (var i = 0; i < $scope.materialList.length; i++) {
							// to check all
							if ($scope.firstMultiSearchDropDown == "All") {
								if ($scope.firstSearchText == ""
										|| $scope.firstSearchText == undefined) {

									$scope.firstSearchANDDataArray
											.push($scope.materialList[i]);

								} else {
									if (($scope.materialList[i].prodId != undefined
											&& $scope.materialList[i].prodId != null ? $scope.materialList[i].prodId
											: "").toLowerCase().includes(
											$scope.firstSearchText
													.toLowerCase())
											|| ($scope.materialList[i].ordereNo != undefined
													&& $scope.materialList[i].ordereNo != null ? $scope.materialList[i].ordereNo
													: "").toString().includes(
													$scope.firstSearchText
															.toLowerCase())

											|| ($scope.materialList[i].issueDate != undefined
													&& $scope.materialList[i].issueDate != null ? $scope.materialList[i].issueDate
													: "")
													.toLowerCase()
													.includes(
															$scope.firstSearchText
																	.toLowerCase())
											|| ($scope.materialList[i].source != undefined
													&& $scope.materialList[i].source != null ? $scope.materialList[i].source
													: "")
													.toLowerCase()
													.includes(
															$scope.firstSearchText
																	.toLowerCase())
											|| ($scope.materialList[i].destination != undefined
													&& $scope.materialList[i].destination != null ? $scope.materialList[i].destination
													: "")
													.toLowerCase()
													.includes(
															$scope.firstSearchText
																	.toLowerCase())
											|| ($scope.materialList[i].processControl != undefined
													&& $scope.materialList[i].processControl != null ? $scope.materialList[i].processControl
													: "")
													.toLowerCase()
													.includes(
															$scope.firstSearchText
																	.toLowerCase())) {
										$scope.firstSearchANDDataArray
												.push($scope.materialList[i]);

									}

								}
							}
							// to check other than all
							else {
								if ($scope.firstMultiSearchDropDown == "prodId") {
									if ($scope.firstSearchText == ""
											|| $scope.firstSearchText == undefined) {
										$scope.firstSearchANDDataArray
												.push($scope.materialList[i]);
									} else if ((($scope.materialList[i].prodId)
											.toString() != undefined
											&& ($scope.materialList[i].prodId)
													.toString() != null ? ($scope.materialList[i].prodId)
											.toString()
											: "").toLowerCase().includes(
											$scope.firstSearchText
													.toLowerCase()))
										$scope.firstSearchANDDataArray
												.push($scope.materialList[i]);

								} else if ($scope.firstMultiSearchDropDown == "orderNo") {
									if ($scope.firstSearchText == ""
											|| $scope.firstSearchText == undefined) {
										$scope.firstSearchANDDataArray
												.push($scope.materialList[i]);
									} else if (($scope.materialList[i].ordereNo != undefined
											&& $scope.materialList[i].ordereNo != null ? $scope.materialList[i].ordereNo
											: "").toString().includes(
											$scope.firstSearchText
													.toLowerCase()))
										$scope.firstSearchANDDataArray
												.push($scope.materialList[i]);

								} else if ($scope.firstMultiSearchDropDown == "issueDate") {
									if ($scope.firstSearchText == ""
											|| $scope.firstSearchText == undefined) {
										$scope.firstSearchANDDataArray
												.push($scope.materialList[i]);
									} else if (($scope.materialList[i].issueDate != undefined
											&& $scope.materialList[i].issueDate != null ? $scope.materialList[i].issueDate
											: "").toLowerCase().includes(
											$scope.firstSearchText
													.toLowerCase()))
										$scope.firstSearchANDDataArray
												.push($scope.materialList[i]);

								} else if ($scope.firstMultiSearchDropDown == "source") {
									if ($scope.firstSearchText == ""
											|| $scope.firstSearchText == undefined) {
										$scope.firstSearchANDDataArray
												.push($scope.materialList[i]);
									} else if (($scope.materialList[i].source != undefined
											&& $scope.materialList[i].source != null ? $scope.materialList[i].source
											: "").toLowerCase().includes(
											$scope.firstSearchText
													.toLowerCase()))
										$scope.firstSearchANDDataArray
												.push($scope.materialList[i]);
								} else if ($scope.firstMultiSearchDropDown == "destination") {
									if ($scope.firstSearchText == ""
											|| $scope.firstSearchText == undefined) {
										$scope.firstSearchANDDataArray
												.push($scope.materialList[i]);
									} else if (($scope.materialList[i].destination != undefined
											&& $scope.materialList[i].destination != null ? $scope.materialList[i].destination
											: "").toLowerCase().includes(
											$scope.firstSearchText
													.toLowerCase()))
										$scope.firstSearchANDDataArray
												.push($scope.materialList[i]);

								} else if ($scope.firstMultiSearchDropDown == "processControl") {
									if ($scope.firstSearchText == ""
											|| $scope.firstSearchText == undefined) {
										$scope.firstSearchANDDataArray
												.push($scope.materialList[i]);
									} else if (($scope.materialList[i].processControl != undefined
											&& $scope.materialList[i].processControl != null ? $scope.materialList[i].processControl
											: "").toLowerCase().includes(
											$scope.firstSearchText
													.toLowerCase()))
										$scope.firstSearchANDDataArray
												.push($scope.materialList[i]);

								}

							}
						}
					}

					$scope.multiSearchChanged = function() {
						$scope.firstSearchANDDataArray = [];
						$scope.fnfirstSearchANDData();
						//console.log($scope.firstSearchANDDataArray);

						if ($scope.varANDORDropDown == "AND") {
							$scope.disableSecondFilterText = false;
							$scope.disableSecondFilterDropDown = false;
							$scope.materialViewData = [];
							$scope.materialFilterList = [];

							for (var i = 0; i < $scope.firstSearchANDDataArray.length; i++) {
								if ($scope.secondMultiSearchDropDown == "All") {

									if ($scope.secondSearchText == ""
											|| $scope.secondSearchText == undefined) {
										$scope.materialFilterList
												.push($scope.firstSearchANDDataArray[i]);
									} else {
										if ((($scope.firstSearchANDDataArray[i].prodId)
												.toString() != undefined
												&& ($scope.firstSearchANDDataArray[i].prodId)
														.toString() != null ? ($scope.firstSearchANDDataArray[i].prodId)
												.toString()
												: "").toLowerCase().includes(
												$scope.secondSearchText
														.toLowerCase())
												|| ($scope.firstSearchANDDataArray[i].ordereNo != undefined
														&& $scope.firstSearchANDDataArray[i].ordereNo != null ? $scope.firstSearchANDDataArray[i].ordereNo
														: "")
														.toString()
														.includes(
																$scope.secondSearchText
																		.toLowerCase())
												|| ($scope.firstSearchANDDataArray[i].issueDate != undefined
														&& $scope.firstSearchANDDataArray[i].issueDate != null ? $scope.firstSearchANDDataArray[i].issueDate
														: "")
														.toLowerCase()
														.includes(
																$scope.secondSearchText
																		.toLowerCase())
												|| ($scope.firstSearchANDDataArray[i].source != undefined
														&& $scope.firstSearchANDDataArray[i].source != null ? $scope.firstSearchANDDataArray[i].source
														: "")
														.toLowerCase()
														.includes(
																$scope.secondSearchText
																		.toLowerCase())
												|| ($scope.firstSearchANDDataArray[i].destination != undefined
														&& $scope.firstSearchANDDataArray[i].destination != null ? $scope.firstSearchANDDataArray[i].destination
														: "")
														.toLowerCase()
														.includes(
																$scope.secondSearchText
																		.toLowerCase())
												|| ($scope.firstSearchANDDataArray[i].processControl != undefined
														&& $scope.firstSearchANDDataArray[i].processControl != null ? $scope.firstSearchANDDataArray[i].processControl
														: "")
														.toLowerCase()
														.includes(
																$scope.secondSearchText
																		.toLowerCase())) {
											$scope.materialFilterList
													.push($scope.firstSearchANDDataArray[i]);

										}
									}
								} else {

									if ($scope.secondMultiSearchDropDown == "prodId") {
										if ($scope.secondSearchText == ""
												|| $scope.secondSearchText == undefined) {
											$scope.materialFilterList
													.push($scope.firstSearchANDDataArray[i]);
										} else if ((($scope.firstSearchANDDataArray[i].prodId)
												.toString() != undefined
												&& ($scope.firstSearchANDDataArray[i].prodId)
														.toString() != null ? ($scope.firstSearchANDDataArray[i].prodId)
												.toString()
												: "").toLowerCase().includes(
												$scope.secondSearchText
														.toLowerCase()))
											$scope.materialFilterList
													.push($scope.firstSearchANDDataArray[i]);

									} else if ($scope.secondMultiSearchDropDown == "orderNo") {
										if ($scope.secondSearchText == ""
												|| $scope.secondSearchText == undefined) {
											$scope.materialFilterList
													.push($scope.firstSearchANDDataArray[i]);
										} else if (($scope.firstSearchANDDataArray[i].ordereNo != undefined
												&& $scope.firstSearchANDDataArray[i].ordereNo != null ? $scope.firstSearchANDDataArray[i].ordereNo
												: "").toString().includes(
												$scope.secondSearchText
														.toLowerCase()))
											$scope.materialFilterList
													.push($scope.firstSearchANDDataArray[i]);

									} else if ($scope.secondMultiSearchDropDown == "issueDate") {
										if ($scope.secondSearchText == ""
												|| $scope.secondSearchText == undefined) {
											$scope.materialFilterList
													.push($scope.firstSearchANDDataArray[i]);
										} else if (($scope.firstSearchANDDataArray[i].issueDate != undefined
												&& $scope.firstSearchANDDataArray[i].issueDate != null ? $scope.firstSearchANDDataArray[i].issueDate
												: "").toLowerCase().includes(
												$scope.secondSearchText
														.toLowerCase()))
											$scope.materialFilterList
													.push($scope.firstSearchANDDataArray[i]);

									} else if ($scope.secondMultiSearchDropDown == "source") {
										if ($scope.secondSearchText == ""
												|| $scope.secondSearchText == undefined) {
											$scope.materialFilterList
													.push($scope.firstSearchANDDataArray[i]);
										} else if (($scope.firstSearchANDDataArray[i].source != undefined
												&& $scope.firstSearchANDDataArray[i].source != null ? $scope.firstSearchANDDataArray[i].source
												: "").toLowerCase().includes(
												$scope.secondSearchText
														.toLowerCase()))
											$scope.materialFilterList
													.push($scope.firstSearchANDDataArray[i]);
									} else if ($scope.secondMultiSearchDropDown == "destination") {
										if ($scope.secondSearchText == ""
												|| $scope.secondSearchText == undefined) {
											$scope.materialFilterList
													.push($scope.firstSearchANDDataArray[i]);
										} else if (($scope.firstSearchANDDataArray[i].destination != undefined
												&& $scope.firstSearchANDDataArray[i].destination != null ? $scope.firstSearchANDDataArray[i].destination
												: "").toLowerCase().includes(
												$scope.secondSearchText
														.toLowerCase()))
											$scope.materialFilterList
													.push($scope.firstSearchANDDataArray[i]);

									} else if ($scope.secondMultiSearchDropDown == "processControl") {
										if ($scope.secondSearchText == ""
												|| $scope.secondSearchText == undefined) {
											$scope.materialFilterList
													.push($scope.firstSearchANDDataArray[i]);
										} else if (($scope.firstSearchANDDataArray[i].processControl != undefined
												&& $scope.firstSearchANDDataArray[i].processControl != null ? $scope.firstSearchANDDataArray[i].processControl
												: "").toLowerCase().includes(
												$scope.secondSearchText
														.toLowerCase()))
											$scope.materialFilterList
													.push($scope.firstSearchANDDataArray[i]);

									}
								}
							}

							$scope.totalItems = $scope.materialFilterList.length;
							$scope.currentPage = 1;
							$scope.getTotalPages();
							$scope.getAssignedTagIds($scope.materialFilterList);

							if ($scope.materialList.length == 0
									|| $scope.materialFilterList.length == 0) {
								$scope.materialNoData = true;

							} else {
								$scope.materialNoData = false;

								if ($scope.materialFilterList.length > $scope.numPerPage) {
									$scope.materialViewData = $scope.materialFilterList
											.slice(0, $scope.numPerPage);
									$scope.PaginationTab = true;
								} else {
									$scope.materialViewData = $scope.materialFilterList;
									$scope.PaginationTab = false;
								}
							}

						} else if ($scope.varANDORDropDown == "OR") {

							if ($scope.firstMultiSearchDropDown == "All"
									&& ($scope.firstSearchText == "" || $scope.firstSearchText == undefined)) {
								$scope.disableSecondFilterText = true;
								$scope.disableSecondFilterDropDown = true;
								$scope.secondMultiSearchDropDown = "All"
								$scope.secondSearchText = ""

							} else {
								$scope.disableSecondFilterText = false;
								$scope.disableSecondFilterDropDown = false;
							}

							// $scope.materialViewData.length = 0;
							// $scope.materialFilterList.length = 0;
							$scope.materialViewData = []
							$scope.materialFilterList = [];
							$scope.secondSearchORDataArray = [];
							for (var i = 0; i < $scope.materialList.length; i++) {
								if ($scope.secondMultiSearchDropDown == "All") {
									if ($scope.secondSearchText == ""
											|| $scope.secondSearchText == undefined) {
										$scope.secondSearchORDataArray
												.push($scope.materialList[i]);

									} else {
										if ((($scope.materialList[i].prodId)
												.toString() != undefined
												&& ($scope.materialList[i].prodId)
														.toString() != null ? ($scope.materialList[i].prodId)
												.toString()
												: "").toLowerCase().includes(
												$scope.secondSearchText
														.toLowerCase())
												|| ($scope.materialList[i].ordereNo != undefined
														&& $scope.materialList[i].ordereNo != null ? $scope.materialList[i].ordereNo
														: "")
														.toString()
														.includes(
																$scope.secondSearchText
																		.toLowerCase())
												|| ($scope.materialList[i].issueDate != undefined
														&& $scope.materialList[i].issueDate != null ? $scope.materialList[i].issueDate
														: "")
														.toLowerCase()
														.includes(
																$scope.secondSearchText
																		.toLowerCase())
												|| ($scope.materialList[i].source != undefined
														&& $scope.materialList[i].source != null ? $scope.materialList[i].source
														: "")
														.toLowerCase()
														.includes(
																$scope.secondSearchText
																		.toLowerCase())
												|| ($scope.materialList[i].destination != undefined
														&& $scope.materialList[i].destination != null ? $scope.materialList[i].destination
														: "")
														.toLowerCase()
														.includes(
																$scope.secondSearchText
																		.toLowerCase())
												|| ($scope.materialList[i].processControl != undefined
														&& $scope.materialList[i].processControl != null ? $scope.materialList[i].processControl
														: "")
														.toLowerCase()
														.includes(
																$scope.secondSearchText
																		.toLowerCase())) {
											$scope.secondSearchORDataArray
													.push($scope.materialList[i]);

										}
									}
								} else {

									// to check other than all in second search
									if ($scope.secondMultiSearchDropDown == "prodId") {
										if ($scope.secondSearchText == ""
												|| $scope.secondSearchText == undefined) {
											$scope.secondSearchORDataArray
													.push($scope.materialList[i]);
										} else if ((($scope.materialList[i].prodId)
												.toString() != undefined
												&& ($scope.materialList[i].prodId)
														.toString() != null ? ($scope.materialList[i].prodId)
												.toString()
												: "").toLowerCase().includes(
												$scope.secondSearchText
														.toLowerCase()))
											$scope.secondSearchORDataArray
													.push($scope.materialList[i]);

									} else if ($scope.secondMultiSearchDropDown == "orderNo") {
										if ($scope.secondSearchText == ""
												|| $scope.secondSearchText == undefined) {
											$scope.secondSearchORDataArray
													.push($scope.materialList[i]);
										} else if (($scope.materialList[i].ordereNo != undefined
												&& $scope.materialList[i].ordereNo != null ? $scope.materialList[i].ordereNo
												: "").toString().includes(
												$scope.secondSearchText
														.toLowerCase()))
											$scope.secondSearchORDataArray
													.push($scope.materialList[i]);

									} else if ($scope.secondMultiSearchDropDown == "issueDate") {
										if ($scope.secondSearchText == ""
												|| $scope.secondSearchText == undefined) {
											$scope.secondSearchORDataArray
													.push($scope.materialList[i]);
										} else if (($scope.materialList[i].issueDate != undefined
												&& $scope.materialList[i].issueDate != null ? $scope.materialList[i].issueDate
												: "").toLowerCase().includes(
												$scope.secondSearchText
														.toLowerCase()))
											$scope.secondSearchORDataArray
													.push($scope.materialList[i]);

									} else if ($scope.secondMultiSearchDropDown == "source") {
										if ($scope.secondSearchText == ""
												|| $scope.secondSearchText == undefined) {
											$scope.secondSearchORDataArray
													.push($scope.materialList[i]);
										} else if (($scope.materialList[i].source != undefined
												&& $scope.materialList[i].source != null ? $scope.materialList[i].source
												: "").toLowerCase().includes(
												$scope.secondSearchText
														.toLowerCase()))
											$scope.secondSearchORDataArray
													.push($scope.materialList[i]);
									} else if ($scope.secondMultiSearchDropDown == "destination") {
										if ($scope.secondSearchText == ""
												|| $scope.secondSearchText == undefined) {
											$scope.secondSearchORDataArray
													.push($scope.materialList[i]);
										} else if (($scope.materialList[i].destination != undefined
												&& $scope.materialList[i].destination != null ? $scope.materialList[i].destination
												: "").toLowerCase().includes(
												$scope.secondSearchText
														.toLowerCase()))
											$scope.secondSearchORDataArray
													.push($scope.materialList[i]);

									} else if ($scope.secondMultiSearchDropDown == "processControl") {
										if ($scope.secondSearchText == ""
												|| $scope.secondSearchText == undefined) {
											$scope.secondSearchORDataArray
													.push($scope.materialList[i]);
										} else if (($scope.materialList[i].processControl != undefined
												&& $scope.materialList[i].processControl != null ? $scope.materialList[i].processControl
												: "").toLowerCase().includes(
												$scope.secondSearchText
														.toLowerCase()))
											$scope.secondSearchORDataArray
													.push($scope.materialList[i]);

									}
								}
							}
							// to concat first and second search arrays
							for (var i = 0; i < $scope.firstSearchANDDataArray.length; i++) {
								if (!$scope.secondSearchORDataArray
										.includes($scope.firstSearchANDDataArray[i]))
									$scope.secondSearchORDataArray
											.push($scope.firstSearchANDDataArray[i]);
							}
							$scope.materialFilterList = $scope.secondSearchORDataArray;
							// $scope.materialFilterList =
							// ($scope.firstSearchANDDataArray).concat($scope.secondSearchORDataArray);

							// for pagination
							$scope.totalItems = $scope.materialFilterList.length;
							$scope.currentPage = 1;
							$scope.getTotalPages();
							$scope.getAssignedTagIds($scope.materialFilterList);

							if ($scope.materialList.length == 0
									|| $scope.materialFilterList.length == 0) {
								$scope.materialNoData = true;

							} else {
								$scope.materialNoData = false;

								if ($scope.materialFilterList.length > $scope.numPerPage) {
									$scope.materialViewData = JSON
											.parse(JSON
													.stringify($scope.materialFilterList
															.slice(
																	0,
																	$scope.numPerPage)));
									$scope.PaginationTab = true;
								} else {
									$scope.materialViewData = JSON
											.parse(JSON
													.stringify($scope.materialFilterList));
									$scope.PaginationTab = false;
								}
							}
						}

					}
					// Multiple Material validation  in uploaded file 
					$scope.multipleMaterialValidation  = function() {
						$scope.errorArray = [];
						for (var i = 0; i < $scope.jsonMultipleMaterials.length; i++) {
							var recordCount = i + 1;
							if ($scope.jsonMultipleMaterials[i].prodId.length == 0) {
								$scope.errorArray
										.push({

											'recordcount' : recordCount,
											'errorField' : "Product ID : "
													+ " " + "***",
											'errorDescription' : "Product ID should not be empty. upto 12 characters allowed"
										});

							}
							if (!ProductIdRegex.test($scope.jsonMultipleMaterials[i].prodId)) {
								$scope.errorArray
										.push({

											'recordcount' : recordCount,
											'errorField' : "Product ID : "
													+ " "
													+ $scope.jsonMultipleMaterials[i].prodId,
											'errorDescription' : "Only Alphanumeric characters are allowed "
										});

							}
							if ($scope.jsonMultipleMaterials[i].prodId.length > 12) {
								$scope.errorArray
										.push({

											'recordcount' : recordCount,
											'errorField' : "Product ID : "
													+ " "
													+ $scope.jsonMultipleMaterials[i].prodId,
											'errorDescription' : "Exceeds limit. upto 12 characters allowed"
										});

							}
							
							if ($scope.jsonMultipleMaterials[i].ordereNo.length == 0) {
								$scope.errorArray
										.push({

											'recordcount' : recordCount,
											'errorField' : "Order No : "
													+ " " + "***",
											'errorDescription' : "Order Number should not be empty. upto 8 numeric characters allowed"
										});

							}
							if (!orderNoRegex.test($scope.jsonMultipleMaterials[i].ordereNo)) {
								$scope.errorArray
										.push({

											'recordcount' : recordCount,
											'errorField' : "Order No : "
													+ " "
													+ $scope.jsonMultipleMaterials[i].ordereNo,
											'errorDescription' : "Only Numeric characters are allowed "
										});

							}
							if ($scope.jsonMultipleMaterials[i].ordereNo.length > 8) {
								$scope.errorArray
										.push({

											'recordcount' : recordCount,
											'errorField' : "Order No : "
													+ " "
													+ $scope.jsonMultipleMaterials[i].ordereNo,
											'errorDescription' : "Exceeds limit. upto 8 Numeric characters allowed"
										});

							}
							
							if ($scope.jsonMultipleMaterials[i].source.length == 0) {
								$scope.errorArray
										.push({

											'recordcount' : recordCount,
											'errorField' : "Source : "
													+ " " + "***",
											'errorDescription' : "Source should not be empty. upto 30 characters allowed"
										});

							}
							if (!sourceDestPCRegex.test($scope.jsonMultipleMaterials[i].source)) {
								$scope.errorArray
										.push({

											'recordcount' : recordCount,
											'errorField' : "Source : "
													+ " "
													+ $scope.jsonMultipleMaterials[i].source,
											'errorDescription' : "Only Alphanumeric characters are allowed "
										});

							}
							if ($scope.jsonMultipleMaterials[i].source.length > 30) {
								$scope.errorArray
										.push({

											'recordcount' : recordCount,
											'errorField' : "Source : "
													+ " "
													+ $scope.jsonMultipleMaterials[i].source,
											'errorDescription' : "Exceeds limit. upto 30 characters allowed"
										});

							}
							
							if ($scope.jsonMultipleMaterials[i].destination.length == 0) {
								$scope.errorArray
										.push({

											'recordcount' : recordCount,
											'errorField' : "Destination : "
													+ " " + "***",
											'errorDescription' : "Destination should not be empty. upto 30 characters allowed"
										});

							}
							if (!sourceDestPCRegex.test($scope.jsonMultipleMaterials[i].destination)) {
								$scope.errorArray
										.push({

											'recordcount' : recordCount,
											'errorField' : "Destination : "
													+ " "
													+ $scope.jsonMultipleMaterials[i].destination,
											'errorDescription' : "Only Alphanumeric characters are allowed "
										});

							}
							if ($scope.jsonMultipleMaterials[i].destination.length > 30) {
								$scope.errorArray
										.push({

											'recordcount' : recordCount,
											'errorField' :"Destination : "
													+ " "
													+ $scope.jsonMultipleMaterials[i].destination,
											'errorDescription' : "Exceeds limit. upto 30 characters allowed"
										});

							}
							if ($scope.jsonMultipleMaterials[i].processControl.length == 0) {
								$scope.errorArray
										.push({

											'recordcount' : recordCount,
											'errorField' : "Description : "
													+ " " + "***",
											'errorDescription' : "Description should not be empty. upto 30 characters allowed"
										});

							}
							/*if (!sourceDestPCRegex.test($scope.jsonMultipleMaterials[i].processControl)) {
								$scope.errorArray
										.push({

											'recordcount' : recordCount,
											'errorField' : "Description : "
													+ " "
													+ $scope.jsonMultipleMaterials[i].processControl,
											'errorDescription' : "Only Alphanumeric characters are allowed "
										});

							}*/
							if ($scope.jsonMultipleMaterials[i].processControl.length > 30) {
								$scope.errorArray
										.push({

											'recordcount' : recordCount,
											'errorField' :"Description : "
													+ " "
													+ $scope.jsonMultipleMaterials[i].destination,
											'errorDescription' : "Exceeds limit. upto 30 characters allowed"
										});

							}
							if ($scope.jsonMultipleMaterials[i].price.length == 0) {
								$scope.errorArray
										.push({

											'recordcount' : recordCount,
											'errorField' : "Price : "
													+ " " + "***",
											'errorDescription' : "Price should not be empty. upto 9 digits allowed"
										});

							}
							if (!priceRegex.test($scope.jsonMultipleMaterials[i].price)) {
								$scope.errorArray
										.push({

											'recordcount' : recordCount,
											'errorField' : "Price : "
													+ " "
													+ $scope.jsonMultipleMaterials[i].price,
											'errorDescription' : "Only Numeric characters upto 9 digits after decimal two digits are allowed "
										});

							}
							  
							if ($scope.jsonMultipleMaterials[i].issueDate.length == 0) {
								$scope.errorArray
										.push({

											'recordcount' : recordCount,
											'errorField' : "Issue Date : "
													+ " " + "***",
											'errorDescription' : "Issue Date should not be empty.fill up date in YYYY-Mmm-dd format"
										});

							}
							if (!dateRegex.test($scope.jsonMultipleMaterials[i].issueDate)) {
								$scope.errorArray
										.push({

											'recordcount' : recordCount,
											'errorField' : "Issue Date : "
													+ " "
													+ $scope.jsonMultipleMaterials[i].issueDate,
											'errorDescription' : "Only allowed date format is YYYY-Mmm-dd i.e (2021-Jan-01) "
										});

							}
							
							

						}
						};

				});