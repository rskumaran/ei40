app
		.controller(
				'accessmanagementController',
				function($scope, $http, $rootScope, $window, $location,
						$timeout) {
					$scope.user = JSON.parse($window.sessionStorage
							.getItem("user"));
					if ($scope.user == null) {
						$location.path('/login');
						return;
					}
					
					let menudata;
					let frequentdata = 0;
					let tree1;
					$scope.capabilityArray = [];
					
					let prfselectObj = {};
					let allowselectObj = {};
					let menuIdArray = [];
					
					$scope.preferredArray = [];
					
					$scope.userData = new Array();
					$scope.userViewData = new Array();
					$scope.userFilterList = new Array();
					$scope.searchText = "";
					$scope.bShowUpdateBtn = false;
					$scope.userNoData = false;
					

					$scope.getPreferedData = function(data) {
						let disablePreferredMenu = [];
						if (frequentdata != null && frequentdata.length != 0
								&& frequentdata != undefined) {
							var frequentIdList = [];
							for (i = 0; i < frequentdata.length; i++) {
								frequentIdList.push(frequentdata[i].id);
							}

							for (i = 0; i < menuIdArray.length; i++) {
								if (!frequentIdList.includes(menuIdArray[i])) {
									disablePreferredMenu.push(menuIdArray[i]);
									//removed from preferred menu if unchecked from allowed menu
									if ($scope.preferredArray.includes(menuIdArray[i])) {
										$scope.preferredArray.splice($scope.preferredArray.indexOf(menuIdArray[i]), 1);
									
									}
								} 
							}
						} else if (frequentdata.length == 0) {
							disablePreferredMenu = menuIdArray;
						}
						tree1 = new Tree('.clPreferredMenu', {

							data : [ {
								id : '-1',
								text : 'All',
								children : data
							} ],
							closeDepth : 3,
							loaded : function() {
								this.disables = disablePreferredMenu;
								this.values = $scope.preferredArray;							

							},
							onChange : function() {
								
								prfselectObj = this.values;
								$scope.preferredArray = this.values;
							}

						})
					}

					
					$scope.getAllowedAndPreferredMenuAccess = function(data) {
								for ( var key in data.allowedMenu) {
									if (data.allowedMenu[key] == 1)
										$scope.capabilityArray.push(key);
								}
								for ( var prf in data.preferredMenu) {
									if (data.preferredMenu[prf] == 1)
										$scope.preferredArray.push(prf);
								}


							}
					$scope.setMenuAccess = function(){
						let tree = new Tree(
								'.clStandardMenu',
								{
									data : [ {
										id : '-1',
										text : 'All',
										children : menudata
									} ],
									closeDepth : 3,
									loaded : function() {

										this.values = $scope.capabilityArray;
									},
									onChange : function() {
										frequentdata = this.selectedNodes;
										bChangedTree = true;
										allowselectObj = this.values;
										$scope
												.getPreferedData(menudata);

									}
								})
						if (bChangedTree == false)
							$scope.getPreferedData(data);

					}
					$scope.getActualMenuDetails = function() {
						if (!$rootScope.checkNetconnection()) { 
							$rootScope.alertDialogChecknet();
							return;
									 }
					$http
							.get('menuaccess.json')
							.success(
									function(data) {
										menudata = data;
										var bChangedTree = false;
										for (var i = 0; i < menudata.length; i++) {
											//menuIdArray.push(menudata[i].id);
											let childMenu = menudata[i].children;
											if (childMenu != undefined
													&& childMenu != null
													&& childMenu.length > 0) {
												for (var j = 0; j < childMenu.length; j++) {
													menuIdArray
															.push(childMenu[j].id);
													let childMenuLevel2 = childMenu[j].children;

													if (childMenuLevel2 != undefined
															&& childMenuLevel2 != null
															&& childMenuLevel2.length > 0) {
														for (var k = 0; k < childMenuLevel2.length; k++) {
															menuIdArray
																	.push(childMenuLevel2[k].id);
														}
													}

												}
											}
										}

									})
							.error(
									function(response) {
										$rootScope.hideloading('#loadingBar');
										$rootScope.fnHttpError(response);
									});
					}
					$scope.editMenu = function() {
						$scope.bShowUpdateBtn = true;
					}
					$scope.editPreffered = function() {
						$scope.bShowUpdateBtn = true;
					}
					$scope.fnUpdateAccess = function() {
						var modifiedAccessMenuList = {};
						var updateMenuList = {};
						if (allowselectObj.length == 0){
							$rootScope.alertDialog("Please Select Menu");
							return;
						}
						var selectedAllowedmenu = {}

						for (i = 0; i <  menuIdArray.length; i++) {
							if (allowselectObj.includes(menuIdArray[i])) {
								selectedAllowedmenu[menuIdArray[i]] = 1;
								
							}
						}	
						modifiedAccessMenuList["allowedMenu"] = selectedAllowedmenu;
						var selectedPreferredmenu = {}

						for (i = 0; i <  menuIdArray.length; i++) {
							if (prfselectObj.includes(menuIdArray[i])) {
								selectedPreferredmenu[menuIdArray[i]] = 1;
								
							}
						}
						modifiedAccessMenuList["preferredMenu"] = selectedPreferredmenu;
						updateMenuList['emailID']=$scope.userViewData[$scope.selectedRow].emailID;
						updateMenuList['capabilityList']=modifiedAccessMenuList;
						$scope.userViewData[$scope.selectedRow].capabilityList = modifiedAccessMenuList
						for (var i = 0; i < $scope.userData.length; i++) {
							if ($scope.userData[i].emailID == $scope.userViewData[$scope.selectedRow].emailID){
								$scope.userData[i].capabilityList = modifiedAccessMenuList;
								break;
							}
						}
					
						if (!$rootScope.checkNetconnection()) { 
							$rootScope.alertDialogChecknet();
							return;
									 }
						$rootScope.showloading('#loadingBar');

						$http({
							method : 'PUT',
							url : './updateCapabilityDetails',
							data : updateMenuList,

						})
								.then(
										function success(response) {
											if (response != null
													&& response.data != null
													&& response.data != "BAD_REQUEST") {
												$rootScope.hideloading('#loadingBar');
												$rootScope.alertDialog(response.data.message + " Please logout and login again");
												$timeout(function() {
													$scope.cancelAccess();
												}, 2000);
											} else {
												$rootScope.hideloading('#loadingBar');
												$rootScope.alertDialog(response.data.message);
												$timeout(function() {
													$scope.cancelAccess();
												}, 2000);
											}
										},
										
										function error(response) {
											$rootScope.hideloading('#loadingBar');
											$rootScope.fnHttpError(response);
										});	
						
					}
					
					$scope.cancelAccess = function() {
						if ($scope.userNoData == false)
							$scope.rowHighilighted($scope.selectedRow);
					}
					
					
					$scope.fnUserSearchOption = function() {
						$scope.fnSearchFilterOption();
					}

					$scope.fnSearchFilterOption = function() {
						$scope.userViewData = [];
						$scope.userFilterList = [];
			
						for (var i = 0; i < $scope.userData.length; i++) {
			
								if ($scope.searchText == '') {
									$scope.userFilterList
											.push($scope.userData[i]);
								} else {
									if ($scope.userData[i].emailID.toLowerCase().includes($scope.searchText.toLowerCase())
											|| ($scope.userData[i].displayName != undefined
													&& $scope.userData[i].displayName != null ? $scope.userData[i].displayName: "").toLowerCase()
													.includes($scope.searchText.toLowerCase())
											|| ($scope.userData[i].userType != undefined
													&& $scope.userData[i].userType != null ? $scope.userData[i].userType
													: "")
													.toLowerCase()
													.includes(
															$scope.searchText
																	.toLowerCase())) {
										$scope.userFilterList
												.push($scope.userData[i]);
			
									}
			
								}
							//}
						}
			
						$scope.totalItems = $scope.userFilterList.length;
						$scope.currentPage = 1;
						$scope.getTotalPages();
						
						 if($scope.userData.length == 0 || $scope.userFilterList.length == 0 ){
								$scope.userNoData = true;
			
							}else{
								$scope.userNoData = false;
							
			
						if ($scope.userFilterList.length > $scope.numPerPage){
							$scope.userViewData = $scope.userFilterList
									.slice(0, $scope.numPerPage);
						$scope.PaginationTab=true;
						}
						else {
							$scope.userViewData = $scope.userFilterList;
							$scope.PaginationTab=true;//to be changed after pagination issue fixed
						}
						if ($scope.userNoData == false)
							$scope.rowHighilighted(0);
					}

			
			
		}//kumar 05/Jul filter option <-	
					
					$scope.getUserData = function() {
						if (!$rootScope.checkNetconnection()) { 
							$rootScope.alertDialogChecknet();
							return;
						}
						$rootScope.showloading('#loadingBar');
						$http({
							method : 'GET',
							url : './getRegisteredUsers',

						})
								.then(
										function(response) {
											$rootScope.hideloading('#loadingBar');
											if (response != null
													&& response.data != null
													&& response.data.length != 0
													&& response.data != "BAD_REQUEST"
													&& response.data.data != undefined
													&& response.data.data.length != 0) {

												$scope.userData.length = 0;
												

												for (var i = 0; i < response.data.data.length; i++) {
														$scope.userData
																.push(response.data.data[i]);

													
												}
												// kumar 02-Jul display no
												// record found
												if ($scope.userData.length == 0) {
													$scope.userNoData = true;
													return;
												} else {
													$scope.userNoData = false;
												}

												if ($scope.userData.length > 10) {
													$scope.userTab = true;
												} else {
													$scope.userTab = false;
												}
												$scope.totalItems = $scope.userData.length;
												

												$scope.getTotalPages();
												$scope.fnSearchFilterOption();
												/*$scope.currentPage = 1;
												if ($scope.userData.length > $scope.numPerPage) {
													$scope.userViewData = $scope.userData
															.slice(
																	0,
																	$scope.numPerPage);
													$scope.hasPrevious = true;
													$scope.isFirstPage = true;
													$scope.hasNext = false;
													$scope.isLastPage = false;
												} else {
													$scope.userViewData = $scope.userData;
												}*/
											}else{
												$scope.userNoData = true;
											}

										},function error(response) {
											$rootScope.hideloading('#loadingBar');
											$rootScope.fnHttpError(response);
										});
					}
					
					$scope.getActualMenuDetails();
					$scope.getUserData();
					
					
					$scope.rowHighilighted = function(row) {

						$scope.selectedRow = row;
						$scope.capabilityArray=[];
						$scope.preferredArray=[];
						$scope.capabilityArray.length = 0;
						$scope.preferredArray.length = 0;
						$scope.getAllowedAndPreferredMenuAccess($scope.userViewData[row].capabilityList);
						$scope.setMenuAccess();
						$scope.bShowUpdateBtn = true;
					}
					
					// pagination button
					$scope.currentPage = 1;
					$scope.numPerPage = 10;
					$scope.totalItems = 0;
					
					$scope
							.$watch(
									'currentPage + numPerPage',
									function() {
										var begin = (($scope.currentPage - 1) * $scope.numPerPage), end = begin
												+ $scope.numPerPage;
										if ($scope.searchText == '') // kumar
											// 02/July
											// for
											// filter
											// text
											$scope.userViewData = $scope.userData
													.slice(begin, end);
										else
											$scope.userViewData = $scope.userFilterList
													.slice(begin, end);
									});

					$scope.currentPage = 1;
					$scope.numPerPage = 10;
					$scope.totalItems = 0;
					$scope.totalPages = 0;

					$scope.hasNext = true;
					$scope.hasPrevious = true;
					$scope.isFirstPage = true;
					$scope.isLastPage = true;
					$scope.serial = 1;

					$scope
							.$watch(
									"currentPage",
									function(newVal, oldVal) {
										if (newVal !== oldVal) {

											if (newVal > $scope.totalPages) {
												$scope.currentPage = $scope.totalPages;
											} else {
												$scope.currentPage = newVal;
											}

											$scope.serial = (($scope.currentPage - 1) * $scope.numPerPage) + 1;

										}
									});

					$scope.enablePageButtons = function() {
						$scope.serial = (($scope.currentPage - 1) * $scope.numPerPage) + 1;
						$scope.selectedRow = -1;
						if ($scope.totalPages <= 1) {
							$scope.hasPrevious = true;
							$scope.isFirstPage = true;
							$scope.hasNext = true;
							$scope.isLastPage = true;

						} else if ($scope.totalPages > 1
								&& $scope.currentPage == $scope.totalPages) {

							$scope.hasPrevious = false;
							$scope.isFirstPage = false;
							$scope.hasNext = true;
							$scope.isLastPage = true;
						} else if ($scope.currentPage != 1
								&& $scope.currentPage < $scope.totalPages) {

							$scope.hasPrevious = false;
							$scope.isFirstPage = false;
							$scope.hasNext = false;
							$scope.isLastPage = false;

						} else {
							$scope.hasPrevious = true;
							$scope.isFirstPage = true;
							$scope.hasNext = false;
							$scope.isLastPage = false;
						}
					};

					$scope.getTotalPages = function() {

						if (($scope.totalItems % $scope.numPerPage) > 0)
							$scope.totalPages = parseInt($scope.totalItems
									/ $scope.numPerPage) + 1;
						else
							$scope.totalPages = parseInt($scope.totalItems
									/ $scope.numPerPage);

						$scope.enablePageButtons();

					};

					$scope.goFirstPage = function() {
						$scope.currentPage = 1;
						$scope.enablePageButtons();
					};

					$scope.goPreviousPage = function() {
						if ($scope.currentPage > 1)
							$scope.currentPage = $scope.currentPage - 1;
						$scope.enablePageButtons();
					};

					$scope.goNextPage = function() {
						if ($scope.currentPage <= $scope.totalPages)
							$scope.currentPage = $scope.currentPage + 1;
						$scope.enablePageButtons();
					};

					$scope.goLastPage = function() {
						$scope.currentPage = $scope.totalPages;
						$scope.enablePageButtons();
					};

				});
