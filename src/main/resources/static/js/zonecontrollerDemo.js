app
		.controller(
				'zoneControllerDemo',
				function($scope, $http, $rootScope, $window, $timeout,$location,$filter) { 
					$scope.user = JSON.parse($window.sessionStorage
							.getItem("user"));
					if ($scope.user == null) {
						$location.path('/login');
						return;
					}
					$('#map').css('height',($(window).height()-135));     

 					

					$scope.outlineDetailLayers = null;
					$scope.marker = null;
					$scope.marker1=null;
					$rootScope.hideloading('#loadingBar');
					$scope.zoneList = [];
					
					$scope.bSetZeroIndex=true;
					$scope.outlineList = [];	
					$scope.doorsList = [];
					$scope.wallList = [];
					$scope.partitionList = [];
					$scope.pathwayList = [];
					$scope.campusDetails ={};
					$scope.org = 'pump';
					const OUTLINE_START = 100;
					const PATHWAY_START = 200;
					const PARTITION_START = 300;
					const WALL_START = 400;
					const DOOR_START = 500;
					$scope.editableLayers = null;
					var screenWidth = window.screen.width
							* window.devicePixelRatio;
					var screenHeight = window.screen.height
							* window.devicePixelRatio;
					// $scope.mapDetails = '';

					$scope.zoneClassificationList = [];
					$scope.zoneTypeList=[];
					$scope.locatorList = [];
					$scope.zoneList =[];
					$scope.isAPView = false;
					$scope.isDrawEvent = false;
					
					$scope.zoneSearchList = new Array();
					$scope.searchText = "";
					$scope.nMaxZoneID = 0;
					$scope.success = false;
					$scope.isZoneDataView = true;
					$scope.isZoneView = true;
					$scope.zoneTable=true;
					$scope.isCheckedShowLocator = true; 
					$scope.isCheckedGrid = false; 
					$scope.isCheckedLayout = true; 
					$("#idZoneEditBtn").text('Edit'); 
					$("#idZoneDetailsCard").hide(); 
					var selectedRowCSS = {'background': '#4da3ff', 'color': '#ffffff', 'font-weight': 'bold'};
					var unSelectedRowCSS = {'background': '#ffffff', 'color': '#000000', 'font-weight': 'normal'};
					
					var maxZoomValue = 18;
					var minZoomValue = 8;   
					var defZoomValue = 8;  
					toastr.options = {
							  "closeButton": true,
							  "newestOnTop": false,
							  "progressBar": true,
							  "positionClass": "toast-top-right",
							  "preventDuplicates": false,
							  "onclick": null,
							  "showDuration": "100",
							  "hideDuration": "1000",
							  "timeOut": "5000",
							  "extendedTimeOut": "1000",
							  "showEasing": "swing",
							  "hideEasing": "linear",
							  "showMethod": "fadeIn",
							  "hideMethod": "fadeOut"
							}
					$("#zoneOverlapAlert").alert("close");
					$("#zoneSuccessAlert").alert("close");
					if ($rootScope.ZoneSelection.zoneSelect == "true"){ //zone select
						$scope.isEdit = false;
						$scope.isSelectZone = true;
						$rootScope.setPageName("Select Zone");
					} else {
						$scope.isSelectZone = false;
					}
					//Initialize map
					var map = L.map('map', {  
 						editable : true,
						maxZoom : maxZoomValue,
						minZoom : minZoomValue,
						zoom: defZoomValue,
						crs : L.CRS.Simple,
						doubleClickZoom : false,
						dragging:true,
						zoomDelta: 0.25,
						zoomSnap : 0.01,
						scrollWheelZoom: false, // disable original zoom function
						  smoothWheelZoom: true,  // enable smooth zoom 
						  smoothSensitivity: 1   // zoom speed. default is 1
					}).setView([ 0, 0 ], 14);
		
					//Zoom handler
					var ZoomViewer = L.Control.extend({
						 options: {
							  position: 'topleft' 
						 },
						onAdd: function(){

							var container= L.DomUtil.create('div');
							var gauge = L.DomUtil.create('div');
							container.style.width = '100px';
							container.style.background = 'rgba(255,255,255,0.5)';
							container.style.textAlign = 'center';
							map.on('zoomstart zoom zoomend', function(ev){
								gauge.innerHTML = 'Zoom: ' + ((map.getZoom()-minZoomValue)+1).toFixed(2);
							})
							container.appendChild(gauge);

							return container;
						}
					});

					(new ZoomViewer).addTo(map);
					map.zoomControl.setPosition('topleft');
					//Add Grid check box
					var command = L.control({position: 'topright'});

					command.onAdd = function (map) {
					    var div = L.DomUtil.create('div', 'command');

					    div.innerHTML = '<form>Grid (1X1M) <input id="command" type="checkbox" /></form>'; 
					    return div;
					};

					command.addTo(map);
					var layoutCmd = L.control({position: 'topright'});

					layoutCmd.onAdd = function (map) {
					    var div = L.DomUtil.create('div', 'layoutCmd');

					    div.innerHTML = '<form>Layout <input id="layoutCmd" type="checkbox" checked style="display: initial!important"/></form>';
					    return div;
					};
					layoutCmd.addTo(map);
					var locatorCmd = L.control({position: 'topright'});

					locatorCmd.onAdd = function (map) {
					    var div = L.DomUtil.create('div', 'locatorCmd');

					    div.innerHTML = '<form>Locator <input id="locatorCmd" type="checkbox" checked style="display: initial!important"/></form>';
					    return div;
					};
					locatorCmd.addTo(map);
					
					// add the event handler
					function handleCommand() {
						// alert("Clicked, checked = " + this.checked);
						$scope.isCheckedGrid = this.checked;
						if (this.checked == true) {
							$scope.grid.onAdd(map)
						} else {
							$scope.grid.onRemove(map);
						}
						$scope.editableLayers.bringToFront();
					}
					function displayLayout() {
						// alert("Clicked, checked = " + this.checked);
						
                        if(this.checked==true) {
							 $scope.isCheckedLayout = true;
                            //showZones();
							 map.addLayer($scope.outlineDetailLayers);
							 $scope.editableLayers.bringToFront();
                       } else {
							$scope.isCheckedLayout = false;
							map.removeLayer($scope.outlineDetailLayers);
                          
                       }
					}
					function displayLocators() {
                        map.eachLayer(function(layer) {
                            if(layer.markerType!=undefined && layer.markerType=='locator'){ 
                                layer.remove(); 
                            }

                        });
                        $scope.isCheckedShowLocator = this.checked;
                        if(this.checked==true) {
								for(var locatorIndex=0;locatorIndex<$scope.locatorList.length;locatorIndex++) {
	                                $scope.addLocatorsIntoMap(locatorIndex);
	                            } 
                        } 
					}
					$scope.isCheckedGrid = false; 
					$scope.isCheckedLayout = true; 
					$scope.isCheckedLocator = false; 

					document.getElementById("command").addEventListener(
							"click", handleCommand, false);
					document.getElementById("layoutCmd").addEventListener(
							"click", displayLayout, false);
					document.getElementById("locatorCmd").addEventListener(
							"click", displayLocators, false);
					if ($scope.reloadBtn != undefined || $scope.reloadBtn != null) {
						$scope.reloadBtn.onRemove(map);
					}
					
				  	$scope.reloadBtn = L.easyButton({
						  states:[
							    {
							      icon:'<i class="fas fa-sync-alt" style="font-size:20px; color:green"></i>',
							      title:     'Reload',  
							      onClick: fnReload
							    }
							  ]
							}); 
					
					
					
					$scope.reloadBtn.addTo(map);
					
					//$scope.addmapDrawOptions = function() {
						var options = {
							position : 'topright',
							draw : {
								polygon : true,
								polyline : false,
								circlemarker : false,
								circle : false,
								marker : false,
								rectangle : true
							},
							edit : false

						};
						if ($scope.drawControl != undefined || $scope.drawControl != null) {
							$scope.drawControl.onRemove(map);
						}
						if ($scope.drawControlEdit != undefined || $scope.drawControlEdit != null) {
							$scope.drawControlEdit.onRemove(map);
						}
						
						$scope.drawControl = new L.Control.Draw(options);
						$scope.drawControlEdit = new L.Control.Draw({
							draw : false
						});
						//map.addControl($scope.drawControl);
					//}

					function pixelsToLatLng(x, y) {
						return map.unproject([ x, y ], defZoomValue);
					}
					$scope.searchZone = function() {
						$scope.zoneSearchList.length = 0;
						for (var i = 0; i < $scope.zoneList.length; i++) {
						 
							
							if ($scope.searchText == "") {
								$scope.zoneSearchList
								.push($scope.zoneList[i]);
							}else
								{
								if(($scope.zoneList[i].name != undefined
													&& $scope.zoneList[i].name != null ? $scope.zoneList[i].name
													: "").toLowerCase().includes($scope.searchText.toLowerCase())
													||($scope.zoneList[i].zoneClassification.className != undefined
															&& $scope.zoneList[i].zoneClassification.className != null ? $scope.zoneList[i].zoneClassification.className
																	: "").toLowerCase().includes($scope.searchText.toLowerCase()))
									{

									$scope.zoneSearchList
									.push($scope.zoneList[i]);
									}
								
								}
						}
						if ($scope.bSetZeroIndex){
						$timeout(function() {
							$scope.selectZone(0);
	         			    }, 50);
							}
						else{
							$scope.bSetZeroIndex = true;
						}
						
					}

					$scope.campusGridArray = {};
					function loadMap() {

						var height = $scope.mapHeight;
						var width = $scope.mapWidth;
						
						
						$scope.outlineDetailLayers = new L.FeatureGroup();
						map.addLayer($scope.outlineDetailLayers);
						$scope.campusRect = null;
						$scope.campusGridArray = {};

						for (var i = 0; i < width ; i++) {

							for (var j = 0; j < height; j++) {

								var campusArray = [];
								campusArray.push({
									"x" : i,
									"y" : j
								});
								campusArray.push({
									"x" : i,
									"y" : (j + 1)
								});
								campusArray.push({
									"x" : (i + 1),
									"y" : (j + 1)
								});
								campusArray.push({
									"x" : (i + 1),
									"y" : j
								});

								$scope.campusGridArray[i + '_' + j] = campusArray;
							}

						}

						//console.log($scope.campusGridArray);

						map.setZoom(defZoomValue);
						var southWest = map.unproject([ 0, height ],
								defZoomValue);
						var northEast = map.unproject([ width, 0 ],
								defZoomValue);
						map.setMaxBounds(new L.LatLngBounds(southWest,
								northEast));
						
						map.fitBounds(new L.LatLngBounds(southWest, northEast));
						
						

						if ($scope.campusRect != undefined
								|| $scope.campusRect != null) {
							$scope.campusRect.onRemove(map);
						}

						var bounds1 = [
								[ pixelsToLatLng(parseFloat(0), parseFloat(0)) ],
								[ pixelsToLatLng(parseFloat(0),
										parseFloat(height)) ],
								[ pixelsToLatLng(parseFloat(width),
										parseFloat(height)) ],
								[ pixelsToLatLng(parseFloat(width),
										parseFloat(0)) ] ];

						$scope.campusRect = L.rectangle(bounds1, {
							color : $scope.mapColor,
							weight : 2,
							fillColor : $scope.mapColor,
							fillOpacity : 0.8
						});
						$scope.imageOverlay = L.imageOverlay(
								$scope.mapImage,
								new L.LatLngBounds(
										southWest,
										northEast))
								.addTo(map);

						if ($scope.marker != undefined || $scope.marker != null) {
							$scope.marker.onRemove(map);
						}
						var originDotIcon = L.icon({
						    iconUrl: './assets/img/red_circle_tr.png',
						    iconSize:     [12, 12] // size of the icon
						   
						});						
						
						$scope.marker = new L.Marker(
								pixelsToLatLng(0, 0),
								{
									icon : originDotIcon
								});					

						/*		
						$scope.marker = new L.Marker(
								pixelsToLatLng(0, 0),
								{
									icon : new L.DivIcon(
											{
												className : 'my-div-icon',
												html : '<div> <img class="staringImage" src="./assets/img/red_circle.png"/> '
											})
								},{draggable: true});
						
						*/
						$scope.marker.addTo(map);
						map.on('move', function (e) {
							
						});
						//Dragend event of map for update marker position
						map.on('dragend', function(e) {
							var cnt = map.getCenter();
						        var position = $scope.marker.getLatLng();
							lat = Number(position['lat']).toFixed(5);
							lng = Number(position['lng']).toFixed(5);
							//console.log(position);
							//setLeafLatLong(lat, lng);
							
							
						});
						$scope.mapDetails = {};
						$scope.mapDetails.width_m = width;
						$scope.mapDetails.height_m = height;
						$scope.mapDetails.height = height;
						$scope.mapDetails.width = width;
						$scope.mapDetails.mapType = 'custom';

						if ($scope.grid != undefined || $scope.grid != null) {
							$scope.grid.onRemove(map);
						}

						if ($scope.marker1 != undefined
								|| $scope.marker1 != null) {
							$scope.marker1.onRemove(map);
						}

						$scope.grid = L.grid(
								{
									options : {
										position : 'topright',
										bounds : new L.LatLngBounds(southWest,
												northEast),
										mapDetails : $scope.mapDetails,
										defZoomValue : defZoomValue,
										gridSize : 1
									}
								});
						
						/*
						$scope.marker1 = new L.Marker(
								pixelsToLatLng(-35, -5),
								{
									icon : new L.DivIcon(
											{
												className : 'my-div-icon',
												html : '<span class="staringlabel">(0,0)</span>'
											})
								});
						
						 */
						var originValueIcon = L.icon({
						    iconUrl: './assets/img/origin_tr.png',
						    iconSize:     [29, 16] // size of the icon
						   
						});						
						$scope.marker1 = new L.Marker(
								pixelsToLatLng(-2, -2),
								{
									icon : originValueIcon
								});					
						$scope.marker1.addTo(map);
						

						
						$timeout(function() {
							$scope.getZoneClassifications();
						}, 500);
						/*	
						$timeout(function() {
							$scope.getZoneDetails();

							}, 500);*/
							
					}
					function fnReload() {
						//toastr.success('Loading... Please wait.',"EI4.0")
						$scope.fnCancelEditZone();
						//getCampusDetails();
						map.eachLayer(function(layer) {
							if (layer.layerType=="Zone" ) {
								layer.remove();
							}
						});
						map.removeLayer($scope.editableLayers);
						$scope.getZoneDetails()
						//toastr.success('Loaded successfully.',"EI4.0")
					}

					function getCampusDetails() {

						$http({
							method : 'GET',
							url : './campusDetails'
						})
								.then(
										function success(response) {
											$rootScope
													.hideloading('#loadingBar');
											if (response != null
													&& response.data != null
													&& response.data != "BAD_REQUEST") {
												$scope.campusDetails = response.data.data;

												$scope.mapHeight = $scope.campusDetails.height;
												$scope.mapWidth = $scope.campusDetails.width;
												$scope.mapColor = $scope.campusDetails.colorCode;
												$scope.campusId = $scope.campusDetails.campusId;
												$scope.campusName = $scope.campusDetails.name;
												$scope.metersPerPixelX = $scope.campusDetails.metersPerPixelX;
												$scope.metersPerPixelY = $scope.campusDetails.metersPerPixelY;
												$scope.originX = $scope.campusDetails.originX;
												$scope.originY = $scope.campusDetails.originY;

												
												//var jsonText = JSON.stringify($scope.campusDetails);
												//saveText(jsonText, "Campus.json");

												//$("#campusDetailsDiv").show();
												//$("#idZoneDetailsCard").hide();
												//$scope.loadTreeView();

												$timeout(function() {
													loadMap();
												}, 500);
											}
										},
										function error(response) {
											$rootScope.hideloading('#loadingBar');
											$rootScope.fnHttpError(response);
										});
					}
					function getMapDetails() {
						$http({
							method : 'GET',
							url : './demo/mapdetails',
								headers : {
									'orgName' : $scope.org
								}
						})
						.then(
										function success(response) {
						
									
									//$scope.campusDetails = response.data.data;
									$scope.campusDetails.width = response.data.width_m;
									$scope.campusDetails.height = response.data.height_m;
									$scope.mapImage = response.data.url;
									$scope.mapHeight = response.data.height_m;
									$scope.mapWidth = response.data.width_m;
									$scope.mapColor = "#d4e0f2";
									
									$scope.campusId = 1;
									$scope.campusName = response.data.name;
									$scope.metersPerPixelX = response.data.ppmx;
									$scope.metersPerPixelY = response.data.ppmy;
									$scope.originX = 0;
									$scope.originY = response.data.height;

									
									
									$timeout(function() {
										loadMap();
									}, 500);
									//console.log(menuIdArray)
								},
										function error(response) {
									$rootScope.hideloading('#loadingBar');
									$rootScope.fnHttpError(response);
								});
						
					}
					getMapDetails();	
					
					$scope.area = function(points) {
						var area = 0, i, j, point1, point2;

						var length = points.length;

						for (i = 0, j = length - 1; i < length; j = i, i += 1) {
							point1 = points[i];
							point2 = points[j];
							area += point1.x * point2.y;
							area -= point1.y * point2.x;
						}
						area /= 2;

						return Math.abs(area).toFixed(3);
					}
					
					function latLngToPixels(latlng) {
						var pixelArr = [];
						for (var i = 0; i < latlng[0].length; i++) {

							var latlngArr = latlng[0][i];

							var pt= map.project([ latlngArr.lat,
                                latlngArr.lng ], defZoomValue); 
							
                            pixelArr.push({
                                "x" : pt.x.toFixed(),
                                "y" : pt.y.toFixed(),
                            });


						}
						return pixelArr;
					} 
					function latLngToPixelsFloat(latlng) {
						var pixelArr = [];
						for (var i = 0; i < latlng[0].length; i++) {

							var latlngArr = latlng[0][i];

							var pt = map.project(
									[ latlngArr.lat, latlngArr.lng ],
									defZoomValue);

							pixelArr.push({
								"x" : (parseFloat(pt.x)).toFixed(2),
								"y" : (parseFloat(pt.y)).toFixed(2),
							});

						}
						return pixelArr;
					}
					
					function roundVal(latlng) {
						var pixelArr = [];
						for (var i = 0; i < latlng.length; i++) {

							var pt = latlng[i];
 							 
                            
                            pixelArr.push({
                                "x" : (parseFloat(pt.x)).toFixed(),
                                "y" : (parseFloat(pt.y)).toFixed(),
                            });

						}
						return pixelArr;
					} 
					
					function editlatLngToPixels(latlng) {
						var pixelArr = [];
						for (var i = 0; i < latlng[0].length; i++) {

							var latlngArr = latlng[0][i];
							var pt = pt = map.project([ latlngArr.lat,
									latlngArr.lng ], defZoomValue);

							pixelArr.push({
								"x" : pt.x.toFixed(),
								"y" : pt.y.toFixed(),
							});

						}
						return pixelArr;
					}
					
					function saveText(text, filename){
						  var a = document.createElement('a');
						  a.setAttribute('href', 'data:text/plain;charset=utf-u,'+encodeURIComponent(text));
						  a.setAttribute('download', filename);
						  a.click()
						}
					
					function getAllOutlinedetails() {
						map.eachLayer(function(layer) {
							if (layer.id !== undefined) {
								layer.remove();
							}
						});

						$http({
							method : 'GET',
							url : './outlineDetails/' + $scope.campusId
						})
								.then(
										function success(response) {
											$rootScope
													.hideloading('#loadingBar');
											if (response != null
													&& response.data != null
													&& response.data != "BAD_REQUEST") {
												$scope.noList = false;
												$scope.outlineList.length = 0;
												if (response.data.status == 'success') {
													for (var i = 0; i < response.data.data.length; i++) {
														if (response.data.data[i].active)
															$scope.outlineList
																	.push(response.data.data[i]);
													}
													

													if ($scope.outlineList.length > 0) {
														$scope.addOutlineIntoMap();
													} else {
														$scope.noList = true;
														$scope.valueList = false;
														$scope.error = true;
														$scope.errorMsg = "No outlines details are available";
														$timeout(
																function() {
																	$scope.error = false;
																}, 500);
													}
													$scope.getAllPathways();
												} else {
																										
													$scope.error = true;
													$scope.alert_error_msg = response.data.message;
													$timeout(function() {
														$scope.error = false;
													}, 500);
												}

											}
										},
										function error(response) {
											$rootScope.hideloading('#loadingBar');
											$rootScope.fnHttpError(response);
										});
					}
					function getBoundsArray(points) {
						var bounds = [];
						for (var i = 0; i < points.length; i++) {

							bounds.push(pixelsToLatLng(parseFloat(points[i].x),
									parseFloat(points[i].y)));

						}
						return bounds;
					}

					$scope.addOutlineIntoMap = function() {
						if ($scope.outlineList.length > 0) {
							for (var i = 0; i < $scope.outlineList.length; i++) {
								if ($scope.outlineList[i].active) {
									$scope.outlineList[i].area = $scope
											.area($scope.outlineList[i].coordinateList);
									$scope.colorCode = $scope.outlineList[i].bgColor;
									var bounds = getBoundsArray($scope.outlineList[i].coordinateList);
									var layer = null;
									var theme = {
										color : $scope.colorCode,
										weight : 1,
										fillColor : $scope.colorCode,
										fillOpacity : 0.5
									};

									if ($scope.outlineList[i].geometryType == undefined
											|| $scope.outlineList[i].geometryType == null
											|| $scope.outlineList[i].geometryType == "polygon"
											|| $scope.outlineList[i].geometryType == "") {

										$scope.outlineList[i].geometryType = "polygon";
										layer = L.polygon(bounds, theme);

									} else {
										layer = L.rectangle(bounds, theme);
										}
									
									layer.addTo(map)
									.addTo($scope.outlineDetailLayers);
									$scope.outlinesArea = $scope.area(editlatLngToPixels(layer.getLatLngs()));
									

									layer.id = i+OUTLINE_START;
									layer.outlinesId = $scope.outlineList[i].outlinesId+OUTLINE_START;
									layer.name = $scope.outlineList[i].name;
									layer.outlinesArea = $scope.outlinesArea;
									layer.colorCode = $scope.colorCode;
									layer.menuType = "Building";
									layer.wallType = $scope.outlineList[i].wallType;
									layer.palceType = $scope.outlineList[i].placeType;
									layer.geometryType = $scope.outlineList[i].geometryType;
									layer.coordinateList = bounds;
									layer.layerType="Outline";

								}
							}
							
							//var jsonText = JSON.stringify($scope.outlineList);
							//saveText(jsonText, "outline.json");
							
						}
					}
					
					$scope.getZoneDetails_FromFile = function(){

								
								
			                    if ($rootScope.checkNetconnection() == false) { 
							               $rootScope.hideloading('#loadingBar');
							               $rootScope.alertErrorDialog('Please check the Internet Connection'); 
								           return;
								   } 						
			                    
			                    
			                    $http
								.get('pumpZones.json')
								.success(
													function (response) {
														$rootScope.hideloading('#loadingBar');
														if (response != null ) { 
														
															$scope.noList = false;
															$scope.valueList =true;
															$scope.zoneLists= response; 
															$scope.zoneList =[];
															$scope.zoneSearchList = [];
															$scope.zoneTypeList=[];
															$scope.editableLayers = new L.FeatureGroup();
															map.addLayer( $scope.editableLayers);
														
															$scope.zoneTypeList.push('Tracking zone');
															$scope.zoneTypeList.push('Dead Zone');
			 												for (var i = 0; i < $scope.zoneLists.length; i++) {
																if($scope.zoneLists[i].active) {
																	if ($scope.zoneLists[i].zoneType == "Trackingzone"){
																		$scope.zoneLists[i].zoneType = "Tracking zone";
																	}
																	else{
																		$scope.zoneLists[i].zoneType = "Dead Zone";
																	}
			 														$scope.zoneList.push($scope.zoneLists[i])
			 														
																}  
			 												}
			 												$("#idZoneDetailsCard").show(); 
															 
															if(!$scope.isAPView){ 
																if($scope.zoneList.length!=0){
			 													for (var i = 0; i < $scope.zoneList.length; i++) {
																if($scope.zoneList[i].active) {
				  													$scope.zoneList[i].area = $scope.area($scope.zoneList[i].coordinateList);
																	var bounds  = [];
																	//var classification;
																	if($scope.zoneList[i].zoneClassification != null && 
																		$scope.zoneList[i].zoneClassification != "" &&
																		$scope.zoneList[i].zoneClassification.active == true
																		){

																		$scope.colorCode = $scope.zoneList[i].zoneClassification.color;
																			 
																				 
																		} else {
																			$scope.colorCode = "#ff7800"
																		}
																	
																	if($scope.zoneList[i].geometryType== undefined || $scope.zoneList[i].geometryType==null || $scope.zoneList[i].geometryType=="polygon" || $scope.zoneList[i].geometryType==""){ 
																		for (var j = 0; j < $scope.zoneList[i].coordinateList.length; j++) {  
																				bounds.push(pixelsToLatLng(parseFloat($scope.zoneList[i].coordinateList[j].x) 
																						,parseFloat($scope.zoneList[i].coordinateList[j].y)));
																		   
																		}
																		
																		$scope.zoneList[i].geometryType="polygon";
																					var pol = L
																					.polygon(
																							bounds,
																							{
																								color : $scope.colorCode ,
																								weight : 1,
																								fillColor :$scope.colorCode , 
																								fillOpacity: 0.5
																								})
																					.addTo(
																							map)
																					.addTo(
																							$scope.editableLayers)
																					.on(
																							'click',
																							onZoneClick).on('mouseover', mouseOverPopup); 
				
																					pol.id = $scope.zoneList[i].id;
																					pol.primaryid = $scope.zoneList[i].id
																					pol.name = $scope.zoneList[i].name;
																					pol.classification = $scope.zoneList[i].zoneClassification.className;
																					pol.capacity = $scope.zoneList[i].capacity;
																					pol.ppm = $scope.ppm;
																					pol.coordinateList = bounds;
																					
																					pol.colorCode = $scope.colorCode;
																					pol.geometryType="polygon";
																					pol.zoneType = $scope.zoneList[i].zoneType;
																					pol.zonearea=$scope.zoneList[i].area;
																					pol.layerType="Zone";
																					
				 													} else {
				
																	var bounds1 = [
																			[ pixelsToLatLng(
																					parseFloat($scope.zoneList[i].coordinateList[0].x) ,
																					parseFloat($scope.zoneList[i].coordinateList[0].y)) ],
																			[ pixelsToLatLng(
																					parseFloat($scope.zoneList[i].coordinateList[1].x),
																					parseFloat($scope.zoneList[i].coordinateList[1].y)) ],
																			[ pixelsToLatLng(
																					parseFloat($scope.zoneList[i].coordinateList[2].x),
																					parseFloat($scope.zoneList[i].coordinateList[2].y)) ],
																			[ pixelsToLatLng(
																					parseFloat($scope.zoneList[i].coordinateList[3].x),
																					parseFloat($scope.zoneList[i].coordinateList[3].y)) ] ];
																	var rec = L
																			.rectangle(
																					bounds1,
																					{
																						color : $scope.colorCode ,
																						weight : 1,
																						fillColor :$scope.colorCode , 
																						fillOpacity: 0.5
																					})
																			.addTo(
																					map)
																			.addTo(
																					$scope.editableLayers)
																			.on(
																					'click',
																					onZoneClick).on('mouseover', mouseOverPopup); 
																 
																	rec.id = $scope.zoneList[i].id;
																	rec.primaryid = $scope.zoneList[i].id;
																	rec.name = $scope.zoneList[i].name;
																	rec.classification = $scope.zoneList[i].zoneClassification.className;
																	rec.capacity = $scope.zoneList[i].capacity;
																	rec.ppm = $scope.ppm;
																	rec.coordinateList = bounds1;
																	rec.geometryType="rectangle";
																	rec.zoneType = $scope.zoneList[i].zoneType; 
																	rec.colorCode = $scope.colorCode;
																	rec.zonearea=$scope.zoneList[i].area;
																	rec.layerType="Zone";
																	
				 												  }
																}
																
																
															  }
			 													
			 													$scope.searchZone();
			 													//$scope.selectZone(0);
			 													$scope.getLocatorDetails()
			 													$scope.editableLayers.bringToFront();
																} else {
																	$scope.noList = true;
																	$scope.valueList =false;
																	$scope.error = true;

																	$scope.errorMsg="No Zones are available";
																	$("#idZoneDetailsCard").hide(); 
																	$timeout(function() {
																		$scope.error = false;
																	}, 500);
																	//$scope.zoneSearchList.push('No Zones are available');
																}
															}
			   
														} else {
															$scope.noList = true;
															$scope.valueList =false;
															$scope.errorMsg="No zones are available";
															$("#idZoneDetailsCard").hide(); 
															$timeout(function() {
																$scope.error = false;
															}, 500);
														}
														
													})
													.error(function(response) {
														$rootScope.hideloading('#loadingBar');
														$rootScope.fnHttpError(response);
													});
									
							}
							
					$scope.getZoneDetails = function(){
						
						
                    if ($rootScope.checkNetconnection() == false) { 
				               $rootScope.hideloading('#loadingBar');
				               $rootScope.alertErrorDialog('Please check the Internet Connection'); 
					           return;
					   } 						
                    
                    
						$http({
							method : 'GET',
							url : './zone'
						})
								.then(
										function success(response) {
											$rootScope.hideloading('#loadingBar');
											if (response != null
													&& response.data != null
													&& response.data != "BAD_REQUEST") { 
											
												$scope.noList = false;
												$scope.valueList =true;
												$scope.zoneLists= response.data.data; 
												$scope.zoneList =[];
												$scope.zoneSearchList = [];
												$scope.zoneTypeList=[];
												$scope.editableLayers = new L.FeatureGroup();
												map.addLayer( $scope.editableLayers);
											
												$scope.zoneTypeList.push('Tracking zone');
												$scope.zoneTypeList.push('Dead Zone');
 												for (var i = 0; i < $scope.zoneLists.length; i++) {
													if($scope.zoneLists[i].active) {
														if ($scope.zoneLists[i].zoneType == "Trackingzone"){
															$scope.zoneLists[i].zoneType = "Tracking zone";
														}
														else{
															$scope.zoneLists[i].zoneType = "Dead Zone";
														}
 														$scope.zoneList.push($scope.zoneLists[i])
 														
													}  
 												}
 												$("#idZoneDetailsCard").show(); 
												 
												if(!$scope.isAPView){ 
													if($scope.zoneList.length!=0){
 													for (var i = 0; i < $scope.zoneList.length; i++) {
													if($scope.zoneList[i].active) {
	  													$scope.zoneList[i].area = $scope.area($scope.zoneList[i].coordinateList);
														var bounds  = [];
														//var classification;
														if($scope.zoneList[i].zoneClassification != null && 
															$scope.zoneList[i].zoneClassification != "" &&
															$scope.zoneList[i].zoneClassification.active == true
															){

															$scope.colorCode = $scope.zoneList[i].zoneClassification.color;
																 
																	 
															} else {
																$scope.colorCode = "#ff7800"
															}
														
														if($scope.zoneList[i].geometryType== undefined || $scope.zoneList[i].geometryType==null || $scope.zoneList[i].geometryType=="polygon" || $scope.zoneList[i].geometryType==""){ 
															for (var j = 0; j < $scope.zoneList[i].coordinateList.length; j++) {  
																	bounds.push(pixelsToLatLng(parseFloat($scope.zoneList[i].coordinateList[j].x) 
																			,parseFloat($scope.zoneList[i].coordinateList[j].y)));
															   
															}
															
															$scope.zoneList[i].geometryType="polygon";
																		var pol = L
																		.polygon(
																				bounds,
																				{
																					color : $scope.colorCode ,
																					weight : 1,
																					fillColor :$scope.colorCode , 
																					fillOpacity: 0.5
																					})
																		.addTo(
																				map)
																		.addTo(
																				$scope.editableLayers)
																		.on(
																				'click',
																				onZoneClick).on('mouseover', mouseOverPopup); 
	
																		pol.id = $scope.zoneList[i].id;
																		pol.primaryid = $scope.zoneList[i].id
																		pol.name = $scope.zoneList[i].name;
																		pol.classification = $scope.zoneList[i].zoneClassification.className;
																		pol.capacity = $scope.zoneList[i].capacity;
																		pol.ppm = $scope.ppm;
																		pol.coordinateList = bounds;
																		
																		pol.colorCode = $scope.colorCode;
																		pol.geometryType="polygon";
																		pol.zoneType = $scope.zoneList[i].zoneType;
																		pol.zonearea=$scope.zoneList[i].area;
																		pol.layerType="Zone";
																		
	 													} else {
	
														var bounds1 = [
																[ pixelsToLatLng(
																		parseFloat($scope.zoneList[i].coordinateList[0].x) ,
																		parseFloat($scope.zoneList[i].coordinateList[0].y)) ],
																[ pixelsToLatLng(
																		parseFloat($scope.zoneList[i].coordinateList[1].x),
																		parseFloat($scope.zoneList[i].coordinateList[1].y)) ],
																[ pixelsToLatLng(
																		parseFloat($scope.zoneList[i].coordinateList[2].x),
																		parseFloat($scope.zoneList[i].coordinateList[2].y)) ],
																[ pixelsToLatLng(
																		parseFloat($scope.zoneList[i].coordinateList[3].x),
																		parseFloat($scope.zoneList[i].coordinateList[3].y)) ] ];
														var rec = L
																.rectangle(
																		bounds1,
																		{
																			color : $scope.colorCode ,
																			weight : 1,
																			fillColor :$scope.colorCode , 
																			fillOpacity: 0.5
																		})
																.addTo(
																		map)
																.addTo(
																		$scope.editableLayers)
																.on(
																		'click',
																		onZoneClick).on('mouseover', mouseOverPopup); 
													 
														rec.id = $scope.zoneList[i].id;
														rec.primaryid = $scope.zoneList[i].id;
														rec.name = $scope.zoneList[i].name;
														rec.classification = $scope.zoneList[i].zoneClassification.className;
														rec.capacity = $scope.zoneList[i].capacity;
														rec.ppm = $scope.ppm;
														rec.coordinateList = bounds1;
														rec.geometryType="rectangle";
														rec.zoneType = $scope.zoneList[i].zoneType; 
														rec.colorCode = $scope.colorCode;
														rec.zonearea=$scope.zoneList[i].area;
														rec.layerType="Zone";
														
	 												  }
													}
													
													
												  }
 													
 													$scope.searchZone();
 													//$scope.selectZone(0);
 													$scope.getLocatorDetails()
 													$scope.editableLayers.bringToFront();
													} else {
														$scope.noList = true;
														$scope.valueList =false;
														$scope.error = true;

														$scope.errorMsg="No Zones are available";
														$("#idZoneDetailsCard").hide(); 
														$timeout(function() {
															$scope.error = false;
														}, 500);
														//$scope.zoneSearchList.push('No Zones are available');
													}
												}
   
											} else {
												$scope.noList = true;
												$scope.valueList =false;
												$scope.errorMsg="No zones are available";
												$("#idZoneDetailsCard").hide(); 
												$timeout(function() {
													$scope.error = false;
												}, 500);
											}
											
										},function error(response) {
											$rootScope.hideloading('#loadingBar');
											$rootScope.fnHttpError(response);
										});
						
					}
					$scope.getLocatorDetails = function(){
						
						
	                    if ($rootScope.checkNetconnection() == false) { 
					               $rootScope.hideloading('#loadingBar');
					               $rootScope.alertErrorDialog('Please check the Internet Connection'); 
						           return;
						   } 						
	                    
	                   
							$http({
								method : 'GET',
								url : './locatorDetails'
							})
									.then(
											function success(response) {
												$rootScope.hideloading('#loadingBar');
												if (response != null
														&& response.data != null
														&& response.data != "BAD_REQUEST") { 
												
													if (response.data.status == "success"){
													$scope.locatorList=[];
													if (response.data.locator.length > 0) {
														$scope.locatorList = response.data.locator;
														$scope.getQuuppaLocators();
													}
													else{
														$scope.noList = true;
														$scope.valueList =false;
														$scope.errorMsg="No Locators are available";
														$("#idZoneDetailsCard").hide(); 
														$timeout(function() {
															$scope.error = false;
														}, 500);
														
													}
													}
													
												} else {
													$scope.noList = true;
													$scope.valueList =false;
													$scope.errorMsg="No Locators are available";
													$("#idZoneDetailsCard").hide(); 
													$timeout(function() {
														$scope.error = false;
													}, 500);
												}
											},function error(response) {
												$rootScope.hideloading('#loadingBar');
												$rootScope.fnHttpError(response);
											});
							
						}
					//zone select
					$scope.fnSelectZone = function() { 
						if ($rootScope.ZoneSelection.zoneSelect == "true"){
							if ($scope.zoneType == "Tracking zone") {
								
								$rootScope.ZoneSelection.selectedZone = $scope.zoneName;
							
								$window.sessionStorage.setItem("menuName",
								"tagWarehouse");
								$location.path('/tagWarehouse');
								
								
							}
							else{
								toastr.info('You are selected Non-Tracking zone. Please Select Tracking Zone',"EI4.0")
							}
						}
					}
					$scope.fnZoneExport = function() {
						 var CurrentZonelist = [];
						 var CurrentZoneClassificationlist = [];
						 if($scope.zoneClassificationList.length>0){
								
								for (var i = 0; i < $scope.zoneClassificationList.length; i++) {
									
									var data = {};
									
										data.id = $scope.zoneClassificationList[i].id;
										data.className = $scope.zoneClassificationList[i].className;
										data.color = $scope.zoneClassificationList[i].color;
										CurrentZoneClassificationlist.push(data);	
									
									
								} 
							}
						 if($scope.zoneList.length>0){
								
								for (var i = 0; i < $scope.zoneList.length; i++) {
									
									var data = {};
			
										data.id = $scope.zoneList[i].id;
										data.campusId = $scope.campusId;
										data.name = $scope.zoneList[i].name;
										data.type = $scope.zoneList[i].type;
										data.accessControl = $scope.zoneList[i].accessControl;
										if ($scope.zoneList[i].type == undefined)
										{
										data.type = "person";
										}
										
										if ($scope.zoneList[i].zoneType == "Tracking zone"){
											data.zoneType = "Trackingzone";
										}
										else{
											data.zoneType = "DeadZone";
										}
										var coOrdinates = [];
										
										data.zoneClassification = $scope.zoneList[i].zoneClassification.id;
										data.area = $scope.zoneList[i].area;
										data.geometryType = $scope.zoneList[i].geometryType;
										data.capacity = $scope.zoneList[i].capacity;
										data.description = $scope.zoneList[i].description;
										for (var coordinateIndex = 0; coordinateIndex < $scope.zoneList[i].coordinateList.length; coordinateIndex++){
											var boundary={};
											boundary.x = $scope.zoneList[i].coordinateList[coordinateIndex].x;
											boundary.y = $scope.zoneList[i].coordinateList[coordinateIndex].y;
											coOrdinates.push(boundary);	
										}
										
										data.coordinateList = coOrdinates;
										if ($scope.zoneList[i].gridList != undefined)
										{
											data.gridList = $scope.zoneList[i].gridList;
										}
										else
											data.gridList = [];

										
										
										CurrentZonelist.push(data);	
									
									
								} 
							}
							var jsonDataforExport = {
									  "version": "version 1.0",
									  "type": "zone",
									  
									  "classification": CurrentZoneClassificationlist,									 
									  "zone": CurrentZonelist,
									  
									};
						    let dataStr = JSON.stringify(jsonDataforExport);
						    let dataUri = 'data:application/json;charset=utf-8,'+ encodeURIComponent(dataStr);
						    let exportFileDefaultName = 'ZoneDetails.json';
						    let linkElement = document.createElement('a');
						    linkElement.setAttribute('href', dataUri);
						    linkElement.setAttribute('download', exportFileDefaultName);
						    linkElement.click();
						    $rootScope.alertDialog('Zone Exported Successfully !'); 
						}
					
					$scope.fnCancelEditZone = function() {
						if ($rootScope.ZoneSelection.zoneSelect == "true"){
							$window.sessionStorage.setItem("menuName",
							"materialWarehouse");
							$location.path('/materialWarehouse');
							return;
						}
						

						//if ($scope.isEdit == false)
						//	return;
						$(".tbodyWidth").css({ 'height' : "300px" });


						$scope.drawControlEdit.remove();
						$scope.drawControl.addTo(map);
						
						$scope.isEdit = false;
						
						map.closePopup();
						map.eachLayer(function(layer) { 
							if(layer.layerType !== undefined && layer.layerType=="Zone") {
								if(layer.id !== undefined)
								{
									var zone; 
									
									for(var i= 0; i<$scope.zoneSearchList.length; i++)
										{
										if (layer.id == $scope.zoneSearchList[i].id){
											 zone = $scope.zoneSearchList[i];
											 break;
										}
										
										}
									
									if($scope.nMaxZoneID > 0)
									{
										if(layer.id==$scope.zoneSearchList[$scope.selectedRow].id){
											$scope.colorCode = layer.colorCode; 
											layer.disableEdit();
											setTimeout(function () { // give a little time before removing previous tile layer because new one will appear with some transition.
										        map.removeLayer(layer);
										      }, 500);
											$scope.editableLayers.removeLayer(layer);
											//layer.remove();
											$scope.zoneSearchList.splice($scope.zoneSearchList.length-1,
													1);
											$scope.zoneList.splice($scope.zoneList.length-1,
													1);
											$scope.nMaxZoneID = 0;
											$scope.selectedRow = $scope.selectedRow -1;
											if ($scope.zoneSearchList.length == 0){
												$scope.selectedRow = 0
												$("#idZoneDetailsCard").hide();
											}
										}
									}else{ 
										if(layer.id==$scope.zoneSearchList[$scope.selectedRow].id)
										{
											$scope.colorCode = layer.colorCode; 
											layer.disableEdit();
											setTimeout(function () { // give a little time before removing previous tile layer because new one will appear with some transition.
										        map.removeLayer(layer);
										      }, 500);
											$scope.editableLayers.removeLayer(layer);

 											layer.remove();
 											
											
										}else{
											if(layer.editor!== undefined) {
												 layer.editor.editLayer.clearLayers()
											 }
											 layer.setStyle({
													fillColor : layer.colorCode, 
													fillOpacity: 0.5,
													color: layer.colorCode,
													weight:1
												});
										} 
									}
								} 
						}
							   
 						});
						
						if($scope.selectedRow!=null){
							var zone = $scope.zoneSearchList[$scope.selectedRow]; 
							// zone.vertices = JSON.parse(
							// $("#zoneDimension").val());
							if(zone !=undefined){
								$scope.colorCode = $scope.zoneSearchList[$scope.selectedRow].zoneClassification.color;
							
							if(zone.geometryType =="polygon"){ 
								var bounds  = []; 
								for (var j = 0; j < zone.coordinateList.length; j++) {
											bounds.push(pixelsToLatLng(parseFloat(zone.coordinateList[j].x) 
												,parseFloat(zone.coordinateList[j].y)))
								}
								
											var pol = L
											.polygon(
													bounds,
													{
														color : $scope.colorCode ,
														weight :  1,
														fillColor : $scope.colorCode ,
														fillOpacity: 0.5 
														})
											.addTo(
													map)
											.addTo(
													$scope.editableLayers)
											.on(
													'click',
													onZoneClick).on('mouseover', mouseOverPopup); 
											

											
											pol.id = zone.id;
											pol.primaryid = zone.id;
											pol.name = zone.name;
											pol.classification = zone.zoneClassification.className;
											pol.capacity = zone.capacity;
											//pol.ppm = $scope.ppm;
											pol.coordinateList = bounds;
											pol.map_id = zone.map_id;
											pol.zoneType = zone.zoneType;  
											pol.colorCode = $scope.colorCode;
											pol.zonearea = zone.area;
											pol.geometryType="polygon";
											pol.layerType="Zone";
 											//pol.socialdistance=zone.socialdistance

							} else {

							var bounds1 = [
									[ pixelsToLatLng(
											parseFloat(zone.coordinateList[0].x),
											parseFloat(zone.coordinateList[0].y)) ],
									[ pixelsToLatLng(
											parseFloat(zone.coordinateList[1].x),
											parseFloat(zone.coordinateList[1].y)) ],
									[ pixelsToLatLng(
											parseFloat(zone.coordinateList[2].x),
											parseFloat(zone.coordinateList[2].y)) ],
									[ pixelsToLatLng(
											parseFloat(zone.coordinateList[3].x),
											parseFloat(zone.coordinateList[3].y)) ] ];
							var rec = L
									.rectangle(
											bounds1,
											{
												color : $scope.colorCode,
												weight : 1,
												fillColor : $scope.colorCode ,
												fillOpacity: 0.5 
											})
									.addTo(
											map)
									.addTo(
											$scope.editableLayers)
									.on(
											'click',
											onZoneClick).on('mouseover', mouseOverPopup); 
						

						 
							rec.id = zone.id;
							rec.primaryid = zone.id;
							rec.name = zone.name;
							rec.classification = zone.zoneClassification.className;;
							rec.capacity = zone.capacity;
							//rec.ppm = $scope.ppm;
							rec.coordinateList = bounds1;
							//rec.map_id = zone.map_id;
							rec.zoneType = zone.zoneType; 
							rec.colorCode = $scope.colorCode;
							rec.zonearea = zone.area;
							rec.geometryType="rectangle";
							rec.layerType="Zone";
							//rec.socialdistance=zone.socialdistance
							
						  } 
							} //if(zone !=undefined)
						}
						
						$scope.selectedRow = 0;
						selectId = null;
						$scope.isDrawEvent = false;
						map.removeLayer($scope.editableLayers);
						map.addLayer( $scope.editableLayers);
						$scope.editableLayers.bringToFront();
						if ($scope.zoneSearchList.length > 0){
							$timeout(function() {
								$scope.selectZone(0);
		         			    }, 50);
							fnMoveScrollBar();
						}
					}
					
					function fnMoveScrollBar() {
						var rows =$('#zoneTable tr');
						rows[$scope.selectedRow].scrollIntoView({
					    	behavior: 'smooth',
					      block: 'center'
					    });
						
					}
					  function onZoneClick(e) {
						  if($scope.isEdit) {
	   					       var r;
                          /*     $.confirm({
			                    title: $rootScope.MSGBOX_TITLE,
                                content: 'Are you sure you want to discard your changes?',
                                confirmButton: 'OK',
                                cancelButton: 'Cancel',
                                confirmButtonClass: 'btn-ok',
                                cancelButtonClass: 'btn-cancel',
                                autoClose:false,
                                icon: 'fa fa-question-circle',
								theme: 'hololight',
                                confirm: function(){
	                              r=true;
	                                },
                                cancel: function () {
	                             r=false;
                              },
                            });*/
								$.confirm({
									title : $rootScope.MSGBOX_TITLE,
									content : 'Are you sure you want to discard your changes?',
									type : 'blue',
									columnClass : 'medium',
									modal: true,
									autoClose : false,
									icon : 'fa fa-info-circle',
									buttons : {
										yes : {
											text : 'Yes',
											btnClass : 'btn-blue',
											action : function() {
												$scope.fnCancelEditZone();
											}
										},
										no : function() {
											$scope.fnHighlightClickedZone(e);

										}
									}
								});			  
	   						}
						  else{
							  $scope.fnHighlightClickedZone(e);
						  }
							  
						
					}
					  $scope.fnHighlightClickedZone = function(e)  {
							if ($scope.searchText != ""){
								$scope.searchText = "";
								$scope.bSetZeroIndex= false;
								$scope.searchZone();
							}
							 if($scope.isEdit)
								 return;
							$(".tbodyWidth").css({ 'height' : "200px" });
							$("#idZoneDetailsCard").show(); 
							$("#idZoneEditBtn").text('Edit');
							
							$("#idZoneDeletebtn").show();
							$("#zonename").val(e.sourceTarget.name);
							$("#classification").val(e.sourceTarget.classification);
							$("#restrictions").val(e.sourceTarget.capacity);
							$("#idZoneType").val(e.sourceTarget.zoneType);
							
							
							var arrpoint = latLngToPixelsFloat(e.sourceTarget._latlngs);
							$scope.zoneId = e.sourceTarget.primaryid
							$scope.zoneVertices =arrpoint;
							$scope.zonearea = e.sourceTarget.zonearea;
							$scope.zoneName = e.sourceTarget.name;
							//$scope.socialdistance = e.sourceTarget.socialdistance;
							$scope.zoneType = e.sourceTarget.zoneType;
							$scope.geometryType = e.sourceTarget.geometryType;
							$scope.$apply();
							// e.sourceTarget.scopeVal.zoneVertices =arrpoint;
							$("#zoneDimension").val(arrpoint);
							$("#idZoneType").val(e.sourceTarget.zoneType);
							$("#zoneId").val(e.sourceTarget.primaryid);
							$scope.orignalZoneVertices = [];
							for (var j = 0; j < $scope.zoneVertices.length; j++) {
								$scope.orignalZoneVertices.push({
									'x' : $scope.zoneVertices[j].x,
									'y' : $scope.zoneVertices[j].y
								});
							}
						

							var rowCount = ($('#zoneTable tr').length) - 1;
							for (var i = 0; i < rowCount; i++) {
								$("#zoneTable > tbody > tr:nth-child(" + (i + 1) + ")").css(unSelectedRowCSS);
							}

							for (var i = 0; i < rowCount; i++) {
								if ( $scope.zoneSearchList[i].id == e.sourceTarget.id) {
									$("#zoneTable > tbody > tr:nth-child(" + (i + 1) + ")").css(selectedRowCSS);
									$scope.selectedRow = i;
									break;
								}
							}
							//var table = document.querySelector('#zoneTable');
							//var rows = table.querySelectorAll('tr');
							fnMoveScrollBar();
							map.eachLayer(function(layer) {
								if (layer.layerType !== undefined && layer.layerType=="Zone") {
									if(layer.id !== undefined){ 
										if (e.sourceTarget.id == layer.id) {
		  									layer.setStyle({
		  										fillColor : layer.colorCode, 
												fillOpacity:  0.8,
												color: layer.colorCode,
												weight:2
											});
		  									
		 								} else {
											if(Object.keys(layer.options).length!=0){
			 									
												layer.setStyle({
													fillColor : layer.colorCode, 
													fillOpacity: 0.5,
													color: layer.colorCode,
													weight:1
												});
			 								}
			 							}
									}
								}
	 						});
							selectId = e.sourceTarget.id;
							$scope.editableLayers.bringToFront();
							

					  }
						$scope.popup = L.popup({
							maxWidth : 400
						});
						var mouseroverid = null;
						function mouseOverPopup(e) {
							
							if(!$scope.isEdit && !$scope.isDrawEvent){ 

								  if(mouseroverid!=e.sourceTarget.id){
									  var name = e.sourceTarget.name;
									  var restrictions = e.sourceTarget.capacity;
									  var classification = e.sourceTarget.classification;
									  
									  if(name==undefined || name==null)
										  name = '-';
									  if(restrictions==undefined || restrictions==null)
										  restrictions = '-';
									  if(classification==undefined || classification==null)
										  classification = '-';
									  
									   var popupContent = "<strong>Zone name :" + name + "</strong><br /><strong>Classification:" +
									   classification + "</strong><br /> <strong>Max Occupancy:" + restrictions + "</strong><br />";
									   $scope.popup.setLatLng(e.sourceTarget._bounds.getCenter());
									   $scope.popup.setContent(popupContent);
									   map.openPopup($scope.popup);
									   mouseroverid =e.sourceTarget.id;
	 							   }
							} 
						}
						$scope.getZoneClassifications = function() {
					        if ($rootScope.checkNetconnection() == false) { 
						               $rootScope.hideloading('#loadingBar');
							           return;
							   } 					
								$http({
									method : 'GET',
									url : './zoneClassification',
								})          
										.then(
												function success(response) {
													if (response != null
															&& response.data != null
															&& response.data != "BAD_REQUEST") {
														$scope.noList = false;
														$scope.zoneClassificationList.length = 0;
														var classifications = response.data.data;
														if (classifications.length > 0)	{

															for (var i = 0; i < classifications.length; i++) {
																if (classifications[i].active) {
																	$scope.zoneClassificationList
																			.push(classifications[i]);
																}
															}
														} 
														if ($scope.zoneClassificationList.length == 0){
															$scope.noList = true;
															
															$scope.error = true;
															toastr.error('No Classification are available.Please add classification',"EI4.0")	
															$scope.errorMsg="No Classification are available";
															$("#selectedId").hide(); 
															$scope.zoneSearchList.push('No Classification are available');
															$timeout(function() {
																$scope.error = false;
															}, 500);
														} else {
															map.addControl($scope.drawControl);
															$scope.getZoneDetails();
														}
														
														//$scope.zoneClassificationList.sort(GetSortOrder("classification"));
														
														//$scope.searchClassification(); 
													} else {
														$scope.noList = true;
														
														$scope.error = true;

														$scope.errorMsg="No Classification are available";
														$("#idZoneDetailsCard").hide(); 
														$scope.zoneSearchList.push('No Classification are available');
														$timeout(function() {
															$scope.error = false;
														}, 500);
														
													}
												},function error(response) {
													$rootScope.hideloading('#loadingBar');
													$rootScope.fnHttpError(response);
												});
							}
						$scope.getAllPathways = function() {

							$scope.isPathwayEdit = false;

							$http({
								method : 'GET',
								url : './pathWay'
							})
									.then(
											function success(response) {
												$rootScope
														.hideloading('#loadingBar');
												if (response != null
														&& response.data != null
														&& response.data != "BAD_REQUEST") {

													$scope.pathwayList.length = 0;
													if (response.data.status == 'success') {
														for (var i = 0; i < response.data.data.length; i++) {
															if (response.data.data[i].active)
																$scope.pathwayList
																		.push(response.data.data[i]);
														}

														$scope.addPathwayIntoMap();
														$scope.getAllPartitions();
													} else {
														
														$scope.error = true;
														$scope.alert_error_msg = response.data.message;
														$timeout(function() {
															$scope.error = false;
														}, 500);
													}
												}
											},
											function error(response) {
												$rootScope.hideloading('#loadingBar');
												$rootScope.fnHttpError(response);
											});

						}
						$scope.addPathwayIntoMap = function() {
							if ($scope.pathwayList.length > 0) {
								for (var i = 0; i < $scope.pathwayList.length; i++) {

									var bounds = getBoundsArray($scope.pathwayList[i].coordinates);
									var theme = {
											color : $scope.pathwayList[i].colorCode,
											weight : 3,
											fillColor : '#00FFFFFF',
											fill : 'url(dist/img/path/image.gif)',
											fillOpacity : 10
										};
									var layer = null;
									if ($scope.pathwayList[i].geometryType == "polygon") {
										layer = L.polygon(bounds, theme);
									} else {
										layer = L.rectangle(bounds, theme);
									}
									layer.addTo(map).addTo($scope.outlineDetailLayers);

									layer.layerType = "Pathway";
									layer.campusId = $scope.pathwayList[i].campusDetails.campusId;
									layer.pathwayId = $scope.pathwayList[i].id+PATHWAY_START;
									layer.pathwayName = $scope.pathwayList[i].name;
									layer.colorCode = $scope.pathwayList[i].colorCode;
									layer.geometryType = $scope.pathwayList[i].geometryType;
									layer.coordinateList = bounds;

								}
								//var jsonText = JSON.stringify($scope.pathwayList);
								//saveText(jsonText, "Pathways.json");

							}

						}
						$scope.getAllPartitions = function() {

							$scope.isPartitionEdit = false;

							$http({
								method : 'GET',
								url : './partitionsDetails'
							})
									.then(
											function success(response) {
												$rootScope
														.hideloading('#loadingBar');
												if (response != null
														&& response.data != null
														&& response.data != "BAD_REQUEST") {

													$scope.partitionList.length = 0;
													if (response.data.status == 'success') {
														for (var i = 0; i < response.data.data.length; i++) {
															if (response.data.data[i].active)
																$scope.partitionList
																		.push(response.data.data[i]);
														}
														
														$scope.addPartitionIntoMap();
														
														//var jsonText = JSON.stringify($scope.partitionList);
														//saveText(jsonText, "Partitions.json");

														$scope.getAllWalls();
													}else {
														
														$scope.error = true;
														$scope.alert_error_msg = response.data.message;
														$timeout(function() {
															$scope.error = false;
														}, 500);
													}
												}
											},
											function error(response) {
												$rootScope.hideloading('#loadingBar');
												$rootScope.fnHttpError(response);
											});

						}
						$scope.addPartitionIntoMap = function()
						{
							if ($scope.partitionList.length > 0) {
								for (var i = 0; i < $scope.partitionList.length; i++) {
									var bounds = getBoundsArray($scope.partitionList[i].coordinates);
									var theme = {
										color : $scope.partitionList[i].colorCode,
										weight : 3,
										fillColor : $scope.partitionList[i].colorCode,
										fillOpacity : 0
									};
									var layer = null;

									if ($scope.partitionList[i].geometryType == "polyline") {

										layer = L.polyline(bounds, theme);
									} else if ($scope.partitionList[i].geometryType == "polygon") {
										layer = L.polygon(bounds,theme);
									} else {
										layer = L.rectangle(bounds, theme);
									}

									layer.addTo(map)
											.addTo($scope.outlineDetailLayers);

									layer.outlinesId = $scope.partitionList[i].outlinesDetails.outlinesId+OUTLINE_START;
									layer.layerType = "Partition";
									layer.partitionId = $scope.partitionList[i].id+PARTITION_START;
									layer.partitionName = $scope.partitionList[i].name;
									layer.colorCode = $scope.partitionList[i].colorCode;
									layer.geometryType = $scope.partitionList[i].geometryType;
									layer.coordinateList = bounds;

								}
							}
						}
						$scope.getAllWalls = function() {
							$scope.isWallEdit = false;

							$http({
								method : 'GET',
								url : './wallsDetails'
							})
									.then(
											function success(response) {
												$rootScope
														.hideloading('#loadingBar');
												if (response != null
														&& response.data != null
														&& response.data != "BAD_REQUEST") {

													$scope.wallList.length = 0;
													if (response.data.status == 'success') {
														for (var i = 0; i < response.data.data.length; i++) {
															if (response.data.data[i].active)
																$scope.wallList
																		.push(response.data.data[i]);
														}

														$scope.addWallIntoMap();
														//var jsonText = JSON.stringify($scope.wallList);
														//saveText(jsonText, "Walls.json");

														$scope.getAllDoors();

													}else {
														
														$scope.error = true;
														$scope.alert_error_msg = response.data.message;
														$timeout(function() {
															$scope.error = false;
														}, 500);
													}
												}
											},
											function error(response) {
												$rootScope.hideloading('#loadingBar');
												$rootScope.fnHttpError(response);
											});

						}
						$scope.addWallIntoMap = function(){
							if ($scope.wallList.length > 0) {
								for (var i = 0; i < $scope.wallList.length; i++) {


									var bounds = getBoundsArray($scope.wallList[i].coordinates);
									var theme = {
										color : $scope.wallList[i].colorCode,
										weight : 3,
										fillColor : $scope.wallList[i].colorCode,
										fillOpacity : 0
									};
									var layer = null;

									if ($scope.wallList[i].geometryType == "polyline") {
										layer = L
												.polyline(
														bounds,
														theme);
									} else if ($scope.wallList[i].geometryType == "polygon") {
										layer = L
												.polygon(
														bounds,
														theme);
									} else {
										layer = L
												.rectangle(
														bounds,
														theme);
									}

									layer
											.addTo(map)
											.addTo(
													$scope.outlineDetailLayers);

									layer.outlinesId = $scope.wallList[i].outlinesDetails.outlinesId+OUTLINE_START;
									layer.layerType = "Wall";
									layer.wallId = $scope.wallList[i].id+WALL_START;
									layer.name = $scope.wallList[i].name;
									layer.colorCode = $scope.wallList[i].colorCode;
									layer.geometryType = $scope.wallList[i].geometryType;
									layer.coordinateList = bounds;

								}
							}
						}
						$scope.getAllDoors = function() {

							$http({
								method : 'GET',
								url : './doorsDetails'
							})
									.then(
											function success(response) {
												$rootScope
														.hideloading('#loadingBar');
												if (response != null
														&& response.data != null
														&& response.data != "BAD_REQUEST") {

													$scope.doorsList.length = 0;
													if (response.data.status == 'success') {
														for (var i = 0; i < response.data.data.length; i++) {
															if (response.data.data[i].active)
																$scope.doorsList
																		.push(response.data.data[i]);
														}

														$scope.addDoorIntoMap();

													}
												}
											},
											function error(response) {
												$rootScope.hideloading('#loadingBar');
												$rootScope.fnHttpError(response);
											});
						}
						
						
						$scope.addDoorIntoMap = function(){
							if ($scope.doorsList.length > 0) {
								for (var i = 0; i < $scope.doorsList.length; i++) {
									var bounds = getBoundsArray($scope.doorsList[i].coordinates); 

									var layer = L
											.polyline(
													bounds,
													{
														color : $scope.doorsList[i].colorCode,
														weight : 1,
														fillColor : $scope.doorsList[i].colorCode,
														fillOpacity : 0.5
													})
											.addTo(map)
											.addTo(
													$scope.outlineDetailLayers);

									layer.outlinesId = $scope.doorsList[i].outlinesDetails.outlinesId+OUTLINE_START;
									layer.layerType = "Door";
									layer.doorId = $scope.doorsList[i].id+DOOR_START;
									layer.doorName = $scope.doorsList[i].name;
									layer.doorColorCode = $scope.doorsList[i].colorCode;
									layer.geometryType = "Line";
									layer.coordinateList = bounds;
									

								}
								//var jsonText = JSON.stringify($scope.doorsList);
								//saveText(jsonText, "Doors.json");

							}
						}
						$scope.selectedRow = null;
						 
						$scope.selectZone = function(index) {
							
	 						
							if($scope.isEdit) {
		   					      
/*	                            $.confirm({
				                    title: $rootScope.MSGBOX_TITLE,
	                                content: 'Are you sure you want to discard your changes?',
	                                confirmButton: 'OK',
	                                cancelButton: 'Cancel',
	                                confirmButtonClass: 'btn-ok',
	                                cancelButtonClass: 'btn-cancel',
	                                autoClose:false,
	                                icon: 'fa fa-question-circle',
									theme: 'hololight',
	                                confirm: function(){
		                              r=true;
		                                },
	                                cancel: function () {
		                             r=false;
	                              },
	                            });*/
									$.confirm({
										title : $rootScope.MSGBOX_TITLE,
										content : 'Are you sure you want to discard your changes?',
										type : 'blue',
										columnClass : 'medium',
										autoClose : false,
										icon : 'fa fa-info-circle',
										buttons : {
											yes : {
												text : 'Yes',
												btnClass : 'btn-blue',
												action : function() {
													$scope.fnCancelEditZone();
												}
											},
											no : function() {
												$scope.fnHighlightSelectedZone(index);

											}
										}
									});			  
		   						}
							else{
								$scope.fnHighlightSelectedZone(index);
								}
						}
						$scope.fnHighlightSelectedZone = function(index) {
						
							if($scope.isEdit) 
								return;
							$(".tbodyWidth").css({ 'height' : "200px" });

							$("#idZoneDetailsCard").show();
							var rowCount = ($('#zoneTable tr').length) - 1;
							for (var i = 0; i < rowCount; i++) {
								$("#zoneTable > tbody > tr:nth-child(" + (i + 1) + ")").css(unSelectedRowCSS);
							}
							
							$("#zoneTable > tbody > tr:nth-child(" + (index + 1) + ")").css(selectedRowCSS); 

							 
							
							$scope.selectedRow = index;

							$scope.zoneId = $scope.zoneSearchList[$scope.selectedRow].id;
							$scope.zoneClassification = $scope.zoneSearchList[$scope.selectedRow].zoneClassification.className;;
							$scope.zoneName = $scope.zoneSearchList[$scope.selectedRow].name;
							$scope.zoneCapacity = $scope.zoneSearchList[$scope.selectedRow].capacity;
							$scope.zoneDescription = $scope.zoneSearchList[$scope.selectedRow].description;
							$scope.zoneVertices = $scope.zoneSearchList[$scope.selectedRow].coordinateList; 
							$scope.zoneType = $scope.zoneSearchList[$scope.selectedRow].zoneType;
							$scope.zonearea = $scope.zoneSearchList[$scope.selectedRow].area;
							$scope.geometryType =  $scope.zoneSearchList[$scope.selectedRow].geometryType;
							
							$("#zoneDimension").val($scope.zoneVertices);
							$("#classification").val($scope.zoneClassification);
							$("#idZoneType").val($scope.zoneType);
							$scope.orignalZoneVertices = [];
							for (var j = 0; j < $scope.zoneVertices.length; j++) {
								$scope.orignalZoneVertices.push({
									'x' : $scope.zoneVertices[j].x,
									'y' : $scope.zoneVertices[j].y
								});
							}

							if ($scope.zoneId == '') {  
								$("#idZoneEditBtn").text('Save');
								$scope.isEdit = true; 
								// $("#btnDelete").hide();
							} else { 
								$("#idZoneEditBtn").text('Edit');
								$scope.isEdit = false; 
								$("#idZoneDeletebtn").show();
							}
							map.eachLayer(function(layer) {
								if (layer.layerType !== undefined && layer.layerType=="Zone") {
									if(layer.id !== undefined){ 
										if ($scope.zoneId == layer.id) {
											selectId = layer.id;
		  									layer.setStyle({
		  										fillColor : layer.colorCode, 
												fillOpacity: 1.8,
												color: layer.colorCode,
												weight:2
											});
		 								} else {
											if(Object.keys(layer.options).length!=0){
												if(layer.setStyle!==undefined){
													layer.setStyle({
														fillColor : layer.colorCode, 
														fillOpacity: 0.5,
														color: layer.colorCode,
														weight:1
													});
													}
			 								}
			 							}
									}
								}
	 						});
							map.closePopup(); 
							
							if($scope.zoneVertices.length>0){
								map.panTo(pixelsToLatLng(parseFloat($scope.zoneVertices[0].x) 
		                                ,parseFloat($scope.zoneVertices[0].y)));
								
								/*
								 * map.flyTo(pixelsToLatLng(parseFloat($scope.zoneVertices[0].x)
								 * ,parseFloat($scope.zoneVertices[0].y)),
								 * map.getZoom());
								 */
							}
							

						}
						
						var selectId = null;
						$scope.fnEditZone = function() {
							 
							if (!$scope.isEdit) {
								$scope.drawControl.remove();
								$scope.drawControlEdit.addTo(map);
								
								$("#idZoneDeletebtn").hide(); 
								$("#idZoneEditBtn").text("Save");
								if (selectId != null) {
									map
											.eachLayer(function(layer) {
												if (layer.layerType !== undefined && layer.layerType=="Zone") {
													if (selectId == layer.id) {
														/*
														 * $scope.selectedRow =
														 * layer.id; $scope.selectedZone =
														 * zone;
														 */
														layer.enableEdit();
		
														// $scope.zoneId =
														// $("#zoneId").val();
														$scope.zoneClassification = $(
																"#classification")
																.val();
														$scope.zoneName = $("#zonename")
																.val();
														$scope.zoneCapacity = $(
														"#restrictions").val();
														$scope.zoneDescription = $(
														"#zoneDesc").val();
														// $scope.zoneVertices
														// =$("#zoneDimension").val() ;
														//$scope.zoneMapId = $scope.mapId;
														$scope.zoneType = $("#idZoneType").val();
														 
		 											} else {
		 												if(layer.editor!== undefined) {
		 													 layer.editor.editLayer.clearLayers()
		 												 }
		 											}
												}

											});
								} else {
									map.eachLayer(function(layer) {
										if (layer.layerType !== undefined && layer.layerType=="Zone") {
											if(layer.id !== undefined){ 
												if ($scope.zoneSearchList[$scope.selectedRow].id != null
														&& $scope.zoneSearchList[$scope.selectedRow].id == layer.id) {
											 
													layer.enableEdit();
													map.closePopup();
												} else {
														if(layer.editor!== undefined) {
															 layer.editor.editLayer.clearLayers()
														 }
													}
											}
										}

									});
								}
								$scope.isEdit = !$scope.isEdit;
							} else {
								if($scope.zoneName==undefined || $scope.zoneName==''){ 
									$scope.error = true;
									$scope.alert_error_msg = 'Enter zone name'; 
									$timeout(function() {
		 	         					$scope.error = false;
		 	         			    }, 500);
									 return;
							 	}else if($scope.zoneClassification==undefined || $scope.zoneClassification=='') 
						 		{
							 		$scope.error = true;
							 		$scope.alert_error_msg = 'Please select classification'; 
							 		 $timeout(function() {
			 	         					$scope.error = false;
			 	         			    }, 500);
							 		 return;
						 		}else if ($scope.zoneDescription == null || $scope.zoneDescription == ""){
						 			$scope.error = true;
							 		$scope.alert_error_msg = 'Please Enter Zone Description'; 
							 		 $timeout(function() {
			 	         					$scope.error = false;
			 	         			    }, 500);
									
									return;
								}
								if ($scope.zoneType == null || $scope.zoneType == ""){
						 			$scope.error = true;
							 		$scope.alert_error_msg = 'Please Enter Zone Type'; 
							 		 $timeout(function() {
			 	         					$scope.error = false;
			 	         			    }, 500);
									return;
								}else { 
						 			$scope.saveZones();
						 		}
							}
							
						}

						$scope.fnDeleteZone1 = function() {
							toastr.info("Delete Zone","EI4.0"); 
							let index = -1 
							for (var i = 0; i < $scope.zoneList.length; i++) {
								if ($scope.zoneId == $scope.zoneList[i].id){
									index = i;
									break;
								}
							}
							if (index > 0){
								$scope.zoneList.splice(index, 1);
								$scope.SaveZoneFile();
								toastr.error("Zone deleted successfully","EI4.0");
							} else {
								toastr.error("Zone not found","EI4.0");
							}
						}
						$scope.fnDeleteZone = function() {

		                    if ($rootScope.checkNetconnection() == false) { 
						               $rootScope.hideloading('#loadingBar');
							           return;
							   } 								
									
									var zoneDeleteParameter = {
											"id" : $scope.zoneId
										};
									$
									.confirm({
										title : $rootScope.MSGBOX_TITLE,
										content : 'Are you sure you want to delete this item?',
										type : 'blue',
										columnClass : 'medium',
										autoClose : false,
										icon : 'fa fa-info-circle',
										buttons : {
											yes : {
												text : 'Yes',
												btnClass : 'btn-blue',
												action : function() {
													$rootScope.showloading('#loadingBar');

									$http(
											{
												method : 'DELETE',
												url : './zone',
												headers : {
													'Content-Type' : 'application/json'
												},
												data : zoneDeleteParameter
											}).then(
												function success(response) {
													$rootScope.hideloading('#loadingBar');
													if (response.data.status == "success") {

														/*
														 * if ($scope.selectedRow !==
														 * -1) { $scope.zoneList.splice(
														 * $scope.selectedRow, 1); }
														 */
														$scope.selectedRow = null;
														$scope.success = true;
														$scope.alert_success_msg = 'Zone deleted successfully.';
														//$scope.showZoneSuccessMessage();
														toastr.success('Zone deleted successfully.',"EI4.0")
														map.removeLayer($scope.editableLayers);
														
														$timeout(function() {
															$scope.success = false;
															$scope.getZoneDetails();

															}, 500);
										
													}
												},
												function error(response) {
													$rootScope.hideloading('#loadingBar');
													$rootScope.fnHttpError(response);
												});
												}
											},
											no : function() {
												$rootScope
												.hideloading('#loadingBar');
											}
										}
									});

							}
						
						function checkGridOverlap(gridPoints, poly2) {
 
							
							var bounds1 = [];
							for (var j = 0; j < gridPoints.length; j++) {
								bounds1.push([ parseFloat(gridPoints[j].x),
									parseFloat(gridPoints[j].y) ]);
							}

							bounds1.push([ parseFloat(gridPoints[0].x),
								parseFloat(gridPoints[0].y) ]);

							var poly1 = turf.polygon([ bounds1 ]);
							
							if(turf.booleanOverlap(poly1, poly2) || turf.booleanContains(poly1, poly2) 
									|| turf.booleanContains(poly2, poly1)){
								return true;	
							} 
  
							return false;
						}

						$scope.getGridList = function() {
							var gridList = [];

							var vertices = $scope.zoneVertices;
							var bounds1 = [];
							for (var j = 0; j < vertices.length; j++) {
								bounds1.push([ parseFloat(vertices[j].x),
									parseFloat(vertices[j].y) ]);
							}

							bounds1.push([ parseFloat(vertices[0].x),
								parseFloat(vertices[0].y) ]);

							var poly2 = turf.polygon([ bounds1 ]);
							
							for ( var gridItems in $scope.campusGridArray) {
								if (checkGridOverlap($scope.campusGridArray[gridItems], poly2)) {
									gridList.push(gridItems);
								}
							}

							//console.log(gridList);
							return gridList;
						}
	
						$scope.saveZones = function() {
							   
							if($scope.geometryType==""){
								$scope.geometryType="polygon";
							}
							var gridDetails = $scope.getGridList();
							var nClassificationID;
							for (var j = 0; j < $scope.zoneClassificationList.length; j++) {
								if ($scope.zoneClassification == $scope.zoneClassificationList[j].className){
									nClassificationID = $scope.zoneClassificationList[j].id;
									break;
								}
							}
							
							
							var zoneData = {
								"id":$scope.zoneId,	
								"name" : $scope.zoneName,
								"campusId": $scope.campusId,
								"type" : "person",
								"accessControl": "accessControl1",
								"coordinateList" : $scope.zoneVertices,
								"zoneType": $scope.zoneType,
								"area": $scope.zonearea,
								"zoneClassification" : nClassificationID,
								"capacity" : $scope.zoneCapacity,
								"description" : $scope.zoneDescription,
								"geometryType": $scope.geometryType,
								"gridList":gridDetails
							};
	
							if ($scope.nMaxZoneID > 0) {
								$scope.insertZone1(zoneData);
								//$scope.nMaxZoneID = 0;
							} else {
								$scope.updateZone1(zoneData);
							} 
					}
						$scope.insertZone = function(zoneData) { 
							 $scope.zoneList.push(zoneData);
							 $scope.SaveZoneFile();
						}
						$scope.SaveZoneFile = function() { 
						
							 var CurrentZonelist = [];
							 if($scope.zoneList.length>0){
									
									for (var i = 0; i < $scope.zoneList.length; i++) {
										
										var data = {};
				
											data.id = $scope.zoneList[i].id;
											data.campusId = $scope.campusId;
											data.name = $scope.zoneList[i].name;
											data.type = $scope.zoneList[i].type;
											data.accessControl = $scope.zoneList[i].accessControl;
											if ($scope.zoneList[i].zoneType == "Tracking zone"){
												data.zoneType = "Trackingzone";
											}
											else{
												data.zoneType = "DeadZone";
											}
											var coOrdinates = [];
											var Classification = {};
											var j = 0;
											Classification.id = $scope.zoneList[i].zoneClassification.id;
											for ( j=0; j < $scope.zoneClassificationList.length; j++) {
												if ($scope.zoneList[i].zoneClassification.id == $scope.zoneClassificationList[j].id)
													break;
											}
											Classification.className = $scope.zoneClassificationList[j].className;
											Classification.color = $scope.zoneClassificationList[j].color;
											Classification.active = 1;
											data.active = "1";
											data.zoneClassification = Classification ;
											data.area = $scope.zoneList[i].area;
											data.geometryType = $scope.zoneList[i].geometryType;
											data.capacity = $scope.zoneList[i].capacity;
											data.description = $scope.zoneList[i].description;
											for (var coordinateIndex = 0; coordinateIndex < $scope.zoneList[i].coordinateList.length; coordinateIndex++){
												var boundary={};
												boundary.x = $scope.zoneList[i].coordinateList[coordinateIndex].x;
												boundary.y = $scope.zoneList[i].coordinateList[coordinateIndex].y;
												coOrdinates.push(boundary);	
											}
											
											data.coordinateList = coOrdinates;
											
											data.gridList = $scope.zoneList[i].gridList;
											
											CurrentZonelist.push(data);	
										
										
									} 
								}
								
							    let dataStr = JSON.stringify(CurrentZonelist);
							    let dataUri = 'data:application/json;charset=utf-8,'+ encodeURIComponent(dataStr);
							    let exportFileDefaultName = 'ZoneDetails.json';
							    let linkElement = document.createElement('a');
							    linkElement.setAttribute('href', dataUri);
							    linkElement.setAttribute('download', exportFileDefaultName);
							    linkElement.click();
							    $scope.fnCancelEditZone();
								$scope.isEdit = false;
								map.removeLayer($scope.editableLayers);
								$scope.nMaxZoneID = 0;
								
									$scope.success = false;
									$scope.getZoneDetails();
							
						}
						$scope.insertZone1 = function(zoneData) { 
							// $scope.loading =true;
	                    if ($rootScope.checkNetconnection() == false) { 
					               $rootScope.hideloading('#loadingBar');
						           return;
						   } 	
	                    //zoneData.id=$scope.nMaxZoneID;
						var pixelArr = [];
						for (var i = 0; i < zoneData.coordinateList.length; i++) {

							pixelArr.push({
								"x" : (zoneData.coordinateList[i].x),
								"y" : (zoneData.coordinateList[i].y) 
							});
						}
						zoneData.coordinateList = pixelArr;
							$rootScope.showloading('#loadingBar');
							$http({
								data : zoneData,
								method : 'POST',
								url : './zone'
								
							})
									.then(
											function success(response) {

												// if (response.data.status)
												// $scope.loading =false;
												$rootScope.hideloading('#loadingBar');
												if (response != null
														&& response.data != null
														&& response.data != "BAD_REQUEST") {
													
												if (response.data.status == 'success'){
													

														/*
																var zoneData = {
																"id":$scope.zoneId,	
																"name" : $scope.zoneName,
																"type" : "person",
																"accessControl": "accessControl1",
																"coordinateList" : $scope.zoneVertices,
																"zoneType": $scope.zoneType,
																"area": $scope.zonearea,
																"classification" : nClassificationID,
																"capacity" : $scope.zoneCapacity,
																
																"gridList":$scope.zoneVertices
																};
														 */
														$scope.success = true;
														$scope.alert_success_msg = 'Zone added successfully.';
														toastr.success('Zone added successfully.',"EI4.0")
														//$scope.showZoneSuccessMessage();
														$scope.fnCancelEditZone();
														$scope.isEdit = false;
														map.removeLayer($scope.editableLayers);
														$scope.nMaxZoneID = 0;
														$timeout(function() { 
															$scope.success = false;
															$scope.getZoneDetails();
														}, 500);  
													}else if (response.data.status !== 'undefined'
														|| response.data.status == 'error') {
													$scope.error = true;
													$scope.alert_error_msg = response.data.message;
													$scope.isEdit = true;
													$timeout(function() {
														$scope.error = false;
													}, 500);

												}  
											}
											},function error(response) {
												$rootScope.hideloading('#loadingBar');
												$rootScope.fnHttpError(response);
											});
								 
						}
						$scope.updateZone = function(zoneData) {
							for (var i = 0; i < $scope.zoneList.length; i++) {
								if (zoneData.id == $scope.zoneList[i].id){
									$scope.zoneList[i].id = zoneData.id;
									$scope.zoneList[i].name = zoneData.name;
									$scope.zoneList[i].campusId = zoneData.campusId;
									$scope.zoneList[i].type = zoneData.type;
									$scope.zoneList[i].accessControl = zoneData.accessControl;
									$scope.zoneList[i].coordinateList = zoneData.coordinateList;
									$scope.zoneList[i].zoneType = zoneData.zoneType;
									$scope.zoneList[i].area = zoneData.area;
									$scope.zoneList[i].zoneClassification = zoneData.zoneClassification;
									$scope.zoneList[i].capacity = zoneData.capacity;
									$scope.zoneList[i].description = zoneData.description;
									$scope.zoneList[i].geometryType = zoneData.geometryType;
									$scope.zoneList[i].gridList = zoneData.gridList;
									break;
								}
						}
						$scope.SaveZoneFile();
					}
					$scope.updateZone1 = function(zoneData) {
							// $scope.loading =true;
								
								var pixelArr = [];
								for (var i = 0; i < zoneData.coordinateList.length; i++) {

									pixelArr.push({
										"x" : (zoneData.coordinateList[i].x),
										"y" : (zoneData.coordinateList[i].y) 
									});
								}
								zoneData.coordinateList = pixelArr;
		                    if ($rootScope.checkNetconnection() == false) { 
						               $rootScope.hideloading('#loadingBar');
							           return;
							   } 							
							
							$rootScope.showloading('#loadingBar');
							$http({
									data : zoneData,
									method : 'PUT',
									url : './zone'

								})
										.then(
												function success(response) {
													$rootScope.hideloading('#loadingBar');
													if (response != null
															&& response.data != null
															&& response.data != "BAD_REQUEST") {
														
													if (response.data.status == 'success'){
														/*
														var zoneData = {
														"id":$scope.zoneId,	
														"name" : $scope.zoneName,
														"type" : "person",
														"accessControl": "accessControl1",
														"coordinateList" : $scope.zoneVertices,
														"zoneType": $scope.zoneType,
														"area": $scope.zonearea,
														"classification" : nClassificationID,
														"capacity" : $scope.zoneCapacity,
														
														"gridList":$scope.zoneVertices
														};
													 */
													$scope.success = true;
													$scope.alert_success_msg = 'Zone updated successfully.';
													toastr.success('Zone updated successfully.',"EI4.0")
													//$scope.showZoneSuccessMessage();
													$scope.fnCancelEditZone();
													$scope.isEdit = false;
													
													 
													map.removeLayer($scope.editableLayers);
													$timeout(function() {
														$scope.success = false; 
														$scope.getZoneDetails();
													}, 1000);  
													}else if (typeof response.data.status !== 'undefined'
															|| response.data.status == 'error') {
														toastr.error(response.data.message, "EI4.0")
														$scope.fnCancelEditZone();
														$scope.isEdit = false;

													} 
													}
												},function error(response) {
													$rootScope.hideloading('#loadingBar');
													$rootScope.fnHttpError(response);
												});
							}
					$scope.showZoneOverlapError = function()
					{
						// $scope.alert_error_msg = 'Zone should not overlap
						// with other zones.';
						 $("#zoneOverlapAlert").fadeIn();
						$timeout(
								function() {
									$("#zoneOverlapAlert").fadeOut(300);
								}, 1500);
					}
					$scope.showZoneSuccessMessage = function()
					{
						// $scope.alert_error_msg = 'Zone should not overlap
						// with other zones.';
						 $("#zoneSuccessAlert").fadeIn();
						$timeout(
								function() {
									$("#zoneSuccessAlert").fadeOut(300);
								}, 1500);
					}
						 function checkZoneOverlap(zoneId, points){
							 
							 var array = [];
							 
							 for (var k = 0; k < points.length; k++) {   
								    array.push([parseFloat(points[k].x) ,parseFloat(points[k].y)]);
								}
							 array.push([parseFloat(points[0].x) ,parseFloat(points[0].y)]);
							 
							 var poly1 = turf.polygon([array]);
							 
							 for (var i = 0; i < $scope.zoneList.length; i++) {
									var id  = $scope.zoneList[i].id;
									if(id!='' && id!=zoneId){
										var vertices = $scope.zoneList[i].coordinateList; 
										var bounds1  = []; 
										for (var j = 0; j < vertices.length; j++) {
											bounds1.push([parseFloat(vertices[j].x), parseFloat(vertices[j].y)]);  
										} 
										
										bounds1.push([parseFloat(vertices[0].x), parseFloat(vertices[0].y)]);  
										 
										var poly2 = turf.polygon([bounds1]); 
										if(turf.booleanOverlap(poly1, poly2) || turf.booleanContains(poly1, poly2) 
												|| turf.booleanContains(poly2, poly1)){
											return false;	
										} 
									}
									
								}
								return true;
						 }
						 function getMaxZoneId(){
							 var maxZoneId = 0;
							 if ($scope.zoneList.length > 0 )
								 {
									 maxZoneId = $scope.zoneList[0].id;
									 for (var i = 0; i < $scope.zoneList.length; i++) {
											if (maxZoneId  < $scope.zoneList[i].id)
												maxZoneId  = $scope.zoneList[i].id;
									 }
								 }
							 return maxZoneId;
						 }
						function isInsideRect(x1, y1, x2, y2, x, y) {
							if (x > x1 && x < x2 && y > y1 && y < y2)
							return true;

							return false;
						 }

						//Map created
						map
						.on(
								L.Draw.Event.CREATED,
								function(e) {
									
									
									var type = e.layerType, layer = e.layer;
									
									var points = latLngToPixelsFloat(layer
											.getLatLngs());

										var isInsideMap = true;
										var isZoneOverlap = true; 
																		 
										for (var i = 0; i < points.length; i++) {
											var point = points[i];
											if (!isInsideRect(0, 0,
												$scope.campusDetails.width,
												$scope.campusDetails.height,
												point.x, point.y)) {
												isInsideMap = false;
												break;
											}
										}
										
										
										if(isInsideMap){
											isZoneOverlap = checkZoneOverlap(e.layer.primaryid, points);
										}
										
								if (isInsideMap && isZoneOverlap) {

									if($scope.selectedRow!=null){
										$scope.fnCancelEditZone();
									}
									
									$scope.drawControl.remove();
									$scope.drawControlEdit.addTo(map);
									
									$scope.editableLayers.addLayer(layer);
									$(".tbodyWidth").css({ 'height' : "200px" });
									$scope.nMaxZoneID = getMaxZoneId() + 1;
										$timeout(
											function() {
												

												$scope.zoneList
														.push({
															"id" : $scope.nMaxZoneID,
															"name" : "zone" 
																	+ $scope.nMaxZoneID,
															"type" : "person",
															"accessControl": "accessControl1",
															"coordinateList" : latLngToPixelsFloat(layer
																	.getLatLngs()),
																	"zoneClassification":$scope.zoneClassificationList[0],
															"className" : $scope.zoneClassificationList[0].className,
															"capacity" : $scope.zoneCapacity,
															"description" : $scope.zoneDescription,
															"zoneType" : $scope.zoneType,
															"gemetryType" : type
															
														})
														$scope.zoneSearchList.push({
															"id" : $scope.nMaxZoneID,
															"name" : "zone" 
																	+ $scope.nMaxZoneID,
															"type" : "person",
															"accessControl": "accessControl1",
															"coordinateList" : latLngToPixelsFloat(layer
																	.getLatLngs()),
															
															"className" :$scope.zoneClassificationList[0].className,
															"zoneClassification":$scope.zoneClassificationList[0],
															"capacity" : $scope.zoneCapacity,
															"description" : $scope.zoneDescription,
															"zoneType" : $scope.zoneType,
															"gemetryType" : type,
															"newZone" : ""
															
														})

												$("#idZoneDetailsCard").show();

												$scope.selectedRow = $scope.zoneSearchList.length-1;
	
												$scope.zoneId = $scope.nMaxZoneID ;
												$scope.zoneClassification=$scope.zoneClassificationList[0].className;
												$scope.zoneName = "zone" + $scope.nMaxZoneID;
												$scope.zoneCapacity = 5;
												$scope.zoneDescription = "";

												$scope.zoneVertices = latLngToPixelsFloat(layer
														.getLatLngs());
												
												$scope.zoneType = "Tracking zone";
												$scope.geometryType = type;
												 
												$("#idZoneEditBtn").text('Save');
												$scope.isEdit = true;
												$("#idZoneDeletebtn").hide();
												$("#zoneDimension").val(JSON.stringify($scope.zoneVertices));
												
												$scope.zonearea =$scope.area(editlatLngToPixels(layer.getLatLngs()));
												$scope.$apply();
												var rowCount = ($('#zoneTable tr').length) - 1;
												for (var i = 0; i < rowCount; i++) {
													$("#zoneTable > tbody > tr:nth-child(" + (i + 1) + ")").css(unSelectedRowCSS);
												}
												$(
														"#zoneTable > tbody > tr:nth-child("
																+ rowCount
																+ ")").css(selectedRowCSS)
												$scope.$apply();
												layer.id=  $scope.nMaxZoneID;
												layer.geometryType =type;
												layer.name=$scope.zoneName;
												layer.classification=$scope.zoneClassification;
												layer.colorCode = $scope.zoneClassificationList[0].colorCode;
												layer.restrictions=$scope.zoneCapacity;
												layer.description=$scope.zoneDescription;
												layer.layerType="Zone";

												map.addLayer(layer);
												
												layer.enableEdit();
												fnMoveScrollBar();
											}, 500); 
										
										}else{
											$scope.showZoneOverlapError();
											layer.remove();
										}
								});
	
						map
						.on(
								'draw:drawstart',
								function(e) {
									map.closePopup(); 
									 if($scope.isEdit) {
				   					var r ;
						            $.confirm({
				                    title: $rootScope.MSGBOX_TITLE,
	                                content: 'Are you sure you want to discard your changes?',
	                                confirmButton: 'OK',
	                                cancelButton: 'Cancel',
	                                confirmButtonClass: 'btn-ok',
	                                cancelButtonClass: 'btn-cancel',
	                                autoClose:false,
	                                icon: 'fa fa-question-circle',
									theme: 'hololight',
	                                confirm: function(){
		                              r=true;
		                                },
	                                cancel: function () {
		                             r=false;
	                              },
	                            });
	                               if (r === false) {
					   				        return false;
					   				        } else {
					   				        	$scope.isDrawEvent = false;
					   				        	$scope.fnCancelEditZone();
					   				        }  
				   						}else
				   							{
				   								$scope.isDrawEvent = true;
				   							}
								});
						map
						.on(
								'draw:drawstop',
								function(e) {
									$scope.isDrawEvent = false;
								});
						
						map
						.on(
								'draw:toolbarclosed',
								function(e) {
									$scope.isDrawEvent = false;
								});
						
						// editable:editing
						map
								.on(
										'editable:vertex:dragend',
										function(e) {
										
											if($(".leaflet-vertex-icon").length!=0){
												for(var i=0;i<$(".leaflet-vertex-icon").length;i++){
													$(".leaflet-vertex-icon")[i].style.background="#fff"
												}
											}
											
											 
											
											if(e.layer!= undefined && e.layer.markerType!=undefined && e.layer.markerType=='locator'){
	 											
	 											
	 											var pt= map.project([ e.layer._latlng.lat,
	 				                                e.layer._latlng.lng ], defZoomValue); 
	 											
	  											$scope.apXPoint =pt.x;
	 											$scope.apYPoint =pt.y;
	 											$scope.$apply();
	 											
	 										}else{
											
											var zoneVertices = latLngToPixelsFloat(e.layer._latlngs);
											 
											 var isInsideMap = true; 
											 var isZoneOverlap = true; 
											 
												for (var i = 0; i < zoneVertices.length; i++) {
													var point = zoneVertices[i];
													if (!isInsideRect(0, 0,
														$scope.mapDetails.width,
														$scope.mapDetails.height,
														point.x, point.y)) {
														isInsideMap = false;
														break;
													}
												}
												
												if(isInsideMap){
													isZoneOverlap = checkZoneOverlap(e.layer.primaryid, zoneVertices);
												}
												
										if (isInsideMap && isZoneOverlap) {
											//console.log('Drag: Inside');
											map.closePopup(); 

											//$scope.selectedRow = e.layer.id;
											for (var i = 0; i < $scope.zoneSearchList.length; i++) {
												if ( $scope.zoneSearchList[i].id == e.layer.id) {
													
													$scope.selectedRow = i;
													break;
												}
											}

	 
											if(e.layer.primaryid!=undefined) {
												$scope.zoneId = e.layer.primaryid;
											} else {
												$scope.zoneId = '';
											}
											// $scope.zoneClassification =
											// e.layer.classification;
											$scope.zoneName = e.layer.name;
										// $scope.zoneRestrictions =
										// e.layer.restrictions;
											$scope.zoneSearchList[$scope.selectedRow].coordinateList = zoneVertices
											$scope.zoneVertices = zoneVertices;
											$scope.zoneMapId = $scope.mapId;
											$scope.zoneType=e.layer.zoneType;
											$scope.zonearea =$scope.area(editlatLngToPixels(e.layer._latlngs));

											$scope.$apply();
											// $("#zoneDimension").val(JSON.stringify($scope.zoneVertices));
											if(!$scope.isEdit){
												$("#idZoneDeletebtn").show();
											}
										}else{

											
											if(e.layer.colorCode!=undefined){
												$scope.colorCode = e.layer.colorCode;
												$scope.opacity = 0.8;
	 										} else {
												$scope.colorcode ='#3388ff';
												$scope.opacity = 0.5;
											}
											//e.layer.remove();
											setTimeout(function () { // give a little time before removing previous tile layer because new one will appear with some transition.
										        map.removeLayer(e.layer);
										      }, 500);
											$scope.editableLayers.removeLayer(e.layer);

											$scope.showZoneOverlapError();
											if($scope.geometryType =="polygon"){ 
												var bounds  = []; 
												for (var j = 0; j < $scope.zoneVertices.length; j++) {
												 
												
														bounds.push(pixelsToLatLng(parseFloat($scope.zoneVertices[j].x) 
																,parseFloat($scope.zoneVertices[j].y)));
												   
												}
												
															var pol = L
															.polygon(
																	bounds,
																	{
																		color : $scope.colorCode ,
	                                                                    weight :  2,
	                                                                    fillColor : $scope.colorCode ,
	                                                                    fillOpacity: $scope.opacity 
																		})
															.addTo(
																	map)
															.addTo(
																	$scope.editableLayers)
															.on(
																	'click',
																	onZoneClick).on('mouseover', mouseOverPopup); 


															pol.id = $scope.zoneId;
															pol.primaryid = $scope.zoneId,
															pol.name = $scope.zoneName;
															pol.classification = $scope.zoneClassification;
															pol.restrictions = $scope.zoneCapacity;
															pol.description = $scope.zoneDescription;
															//pol.ppm = $scope.ppm;
															pol.vertices = bounds;
															//pol.map_id = $scope.zoneMapId;
															pol.zoneType = $scope.zoneType; 
															pol.colorcode = $scope.colorCode;
															pol.geometryType="polygon";
															pol.layerType="Zone";
															//pol.socialdistance=$scope.socialdistance;
															pol.enableEdit();

											} else {

											var bounds1 = [
													[ pixelsToLatLng(
															parseFloat($scope.zoneVertices[0].x),
															parseFloat($scope.zoneVertices[0].y)) ],
													[ pixelsToLatLng(
															parseFloat($scope.zoneVertices[1].x),
															parseFloat($scope.zoneVertices[1].y)) ],
													[ pixelsToLatLng(
															parseFloat($scope.zoneVertices[2].x),
															parseFloat($scope.zoneVertices[2].y)) ],
													[ pixelsToLatLng(
															parseFloat($scope.zoneVertices[3].x),
															parseFloat($scope.zoneVertices[3].y)) ] ];
											var rec = L
													.rectangle(
															bounds1,
															{
																color : $scope.colorCode ,
	                                                            weight :  2,
	                                                            fillColor : $scope.colorCode ,
	                                                            fillOpacity: 0.8 
															})
													.addTo(
															map)
													.addTo(
															$scope.editableLayers)
													.on(
															'click',
															onZoneClick).on('mouseover', mouseOverPopup); 
										
							
										 
											rec.id = $scope.zoneId;
											rec.primaryid = $scope.zoneId,
											rec.name = $scope.zoneName;
											rec.classification = $scope.zoneClassification;
											rec.restrictions = $scope.zoneCapacity;
											rec.description = $scope.zoneDescription;
											
											//rec.ppm = $scope.ppm;
											rec.vertices = bounds1;
											//rec.map_id = $scope.zoneMapId;
											rec.colorCode = $scope.colorCode;
											rec.zoneType = $scope.zoneType;
											rec.geometryType="rectangle";
											rec.layerType="Zone";
											//rec.socialdistance=$scope.socialdistance;
											rec.enableEdit();
											
										  }  
										}
	 										}
										});

						map
								.on(
										'editable:dragend',
										function(e) {
											
	 										if(e.layer!= undefined && e.layer.markerType!=undefined && e.layer.markerType=='locator'){
	 											
	 											
	 											var pt= map.project([ e.layer._latlng.lat,
	 				                                e.layer._latlng.lng ], defZoomValue); 
	 											
	  											$scope.apXPoint =pt.x;
	 											$scope.apYPoint =pt.y;
	 											$scope.$apply();
	 										}else{
											
											 
											var points = latLngToPixelsFloat(e.layer._latlngs);

											var isInsideMap = true;
											 var isZoneOverlap = true; 
											for (var i = 0; i < points.length; i++) {
												var point = points[i];
												if (!isInsideRect(0, 0,
													$scope.mapDetails.width,
													$scope.mapDetails.height,
													point.x, point.y)) {
													isInsideMap = false;
													break;
												}
											}
											
											if(isInsideMap){
												isZoneOverlap = checkZoneOverlap(e.layer.primaryid, points);
											}
											
										if (isInsideMap && isZoneOverlap) {
											 
											for (var i = 0; i < $scope.zoneSearchList.length; i++) {
												if ( $scope.zoneSearchList[i].id == e.layer.id) {
													
													$scope.selectedRow = i;
													break;
												}
											}

											//$scope.selectedRow = e.layer.id;
											if(e.layer.primaryid!=undefined) {
												$scope.zoneId = e.layer.primaryid;
											} else {
												$scope.zoneId = '';
											}
											// $scope.zoneClassification =
											// e.layer.classification;
											$scope.zoneName = e.layer.name;
											// $scope.zoneRestrictions =
											// e.layer.restrictions;
											$scope.zoneVertices = points;
											$scope.zoneSearchList[$scope.selectedRow].coordinateList = points;

	 										//$scope.zoneMapId = $scope.mapId;
											$scope.zoneType=e.layer.zoneType;
											$scope.zonearea =$scope.area(editlatLngToPixels(e.layer._latlngs));

											// $("#zoneDimension").val(JSON.stringify($scope.zoneVertices));
											$scope.$apply();
											if(!$scope.isEdit){
												$("#idZoneDeletebtn").show();
											}
										}else{
											if(e.layer.colorCode!=undefined){
												$scope.colorCode = e.layer.colorCode;
												$scope.opacity = 0.8;
	 										} else {
												$scope.colorCode ='#3388ff';
												$scope.opacity = 0.5;
											}
											setTimeout(function () { // give a little time before removing previous tile layer because new one will appear with some transition.
										        map.removeLayer(e.layer);
										      }, 500);
											$scope.editableLayers.removeLayer(e.layer);
											//e.layer.remove(); 
											$scope.showZoneOverlapError();
											if($scope.geometryType =="polygon"){ 
												var bounds  = []; 
												for (var j = 0; j < $scope.zoneVertices.length; j++) {
												 
												
														bounds.push(pixelsToLatLng(parseFloat($scope.zoneVertices[j].x) 
																,parseFloat($scope.zoneVertices[j].y)));
												   
												}
												
															var pol = L
															.polygon(
																	bounds,
																	{
																		color : $scope.colorCode ,
	                                                                    weight :  2,
	                                                                    fillColor : $scope.colorCode ,
	                                                                    fillOpacity: $scope.opacity 
																		})
															.addTo(
																	map)
															.addTo(
																	$scope.editableLayers)
															.on(
																	'click',
																	onZoneClick).on('mouseover', mouseOverPopup); 
															pol.id = $scope.zoneId;
															pol.primaryid = $scope.zoneId,
															pol.name = $scope.zoneName;
															pol.classification = $scope.zoneClassification;
															pol.restrictions = $scope.zoneCapacity;
															pol.description = $scope.zoneDescription;
															//pol.ppm = $scope.ppm;
															pol.vertices = bounds;
															//pol.map_id = $scope.zoneMapId;
															pol.zoneType = $scope.zoneType; 
															pol.colorcode = $scope.colorCode;
															pol.geometryType="polygon";
															pol.layerType="Zone";
															//pol.socialdistance=$scope.socialdistance;
															pol.enableEdit();

	
											} else {

											var bounds1 = [
													[ pixelsToLatLng(
															parseFloat($scope.zoneVertices[0].x),
															parseFloat($scope.zoneVertices[0].y)) ],
													[ pixelsToLatLng(
															parseFloat($scope.zoneVertices[1].x),
															parseFloat($scope.zoneVertices[1].y)) ],
													[ pixelsToLatLng(
															parseFloat($scope.zoneVertices[2].x),
															parseFloat($scope.zoneVertices[2].y)) ],
													[ pixelsToLatLng(
															parseFloat($scope.zoneVertices[3].x),
															parseFloat($scope.zoneVertices[3].y)) ] ];
											var rec = L
													.rectangle(
															bounds1,
															{
																color : $scope.colorCode ,
	                                                            weight :  2,
	                                                            fillColor : $scope.colorCode ,
	                                                            fillOpacity: 0.8 
															})
													.addTo(
															map)
													.addTo(
															$scope.editableLayers)
													.on(
															'click',
															onZoneClick).on('mouseover', mouseOverPopup); 
										
											 
											rec.id = $scope.zoneId;
											rec.primaryid = $scope.zoneId,
											rec.name = $scope.zoneName;
											rec.classification = $scope.zoneClassification;
											rec.restrictions = $scope.zoneCapacity;
											rec.description = $scope.zoneDescription;
											//rec.ppm = $scope.ppm;
											rec.vertices = bounds1;
											//rec.map_id = $scope.zoneMapId;
											rec.colorCode = $scope.colorCode;
											rec.zoneType = $scope.zoneType;
											rec.geometryType="rectangle";
											rec.layerType="Zone";
											//rec.socialdistance=$scope.socialdistance;
											rec.enableEdit();

										 
											
										  }
	  
										}
											}

										});
						
						  $scope.verticesChange = function(singleVer,index,name,zoneVertices) {
	 						    
							    
	 						  if(name=="xVal"){
								  singleVer.x=$("#xVal"+index+"").val();
							  } else {
								  singleVer.y=$("#yVal"+index+"").val();
								  
	 						  }
							  
							  zoneVertices[index] = singleVer;
								var isInsideMap = true;
								var isOutlineOverlap = true;
								var isZoneOverlap = true;
								for (var i = 0; i < zoneVertices.length; i++) {
									var point = zoneVertices[i];
									if (!isInsideRect(0, 0, $scope.mapWidth,
											$scope.mapHeight, point.x, point.y)) {
										isInsideMap = false;
										break;
									}
								}

								//if (isInsideMap) {
								//	isZoneOverlap = checkZoneOverlap($scope.zoneId, zoneVertices);
									
								//}                    
	                        map.eachLayer(function(layer) { 
	                        	if (layer.layerType !== undefined && layer.layerType=="Zone"){
		                            if(layer.id!==undefined)
		                            {
										
											if ( $scope.zoneSearchList[$scope.selectedRow].id == layer.id) {
												$scope.colorCode = $scope.zoneSearchList[$scope.selectedRow].zoneClassification.color;
												setTimeout(function () { // give a little time before removing previous tile layer because new one will appear with some transition.
											        map.removeLayer(layer);
											      }, 500);
												$scope.editableLayers.removeLayer(layer);
	
												
											}
										
	
		                            }
	                        	}
	                        });
	                       
							if (isInsideMap && isZoneOverlap) {
								$scope.zoneVertices = zoneVertices;
							} else {
								$scope.showZoneOverlapError();
								$scope.zoneVertices = $scope.orignalZoneVertices;
								
							}
      	                        
	                       
	                          
	                        var  vertics = $scope.zoneVertices;
	                        
	                        if($scope.geometryType =="polygon"){ 
	                            var bounds  = []; 
	                            
	                            
	                            for (var j = 0; j < vertics.length; j++) {
	                             
	                            
	                                    bounds.push(pixelsToLatLng(parseFloat(vertics[j].x) 
	                                            ,parseFloat(vertics[j].y)));
	                               
	                            }
	                            
	                                        var pol = L
	                                        .polygon(
	                                                bounds,
	                                                {
	                                                    color : $scope.colorCode ,
	                                                    weight :  2,
	                                                    fillColor : $scope.colorCode ,
	                                                    fillOpacity: 0.8 
	                                                    })
	                                        .addTo(
	                                                map)
	                                        .addTo(
	                                        		$scope.editableLayers)
	                                        .on(
	                                                'click',
	                                                onZoneClick)
	                                                .on('mouseover', mouseOverPopup); 

											pol.id = $scope.zoneId;
											pol.primaryid = $scope.zoneId,
											pol.name = $scope.zoneName;
											pol.classification = $scope.zoneClassification;
											pol.restrictions = $scope.zoneCapacity;
											pol.description = $scope.zoneDescription;
											//pol.ppm = $scope.ppm;
											pol.vertices = bounds;
											//pol.map_id = $scope.zoneMapId;
											pol.zoneType = $scope.zoneType; 
											pol.colorcode = $scope.colorCode;
											pol.geometryType="polygon";
											pol.layerType="Zone";
											//pol.socialdistance=$scope.socialdistance;
											pol.enableEdit();
											

	                                        
	                                        
	                        } else {

	                        var bounds1 = [
	                                [ pixelsToLatLng(
	                                        parseFloat(vertics[0].x),
	                                        parseFloat(vertics[0].y)) ],
	                                [ pixelsToLatLng(
	                                        parseFloat(vertics[1].x),
	                                        parseFloat(vertics[1].y)) ],
	                                [ pixelsToLatLng(
	                                        parseFloat(vertics[2].x),
	                                        parseFloat(vertics[2].y)) ],
	                                [ pixelsToLatLng(
	                                        parseFloat(vertics[3].x),
	                                        parseFloat(vertics[3].y)) ] ];
	                        var rec = L
	                                .rectangle(
	                                        bounds1,
	                                        {
	                                            color : $scope.colorCode ,
	                                            weight :  2,
	                                            fillColor : $scope.colorCode ,
	                                            fillOpacity: 0.8 
	                                        })
	                                .addTo(
	                                        map)
	                                .addTo(
	                                		$scope.editableLayers)
	                                .on(
	                                        'click',
	                                        onZoneClick)
	                                        .on('mouseover', mouseOverPopup); 
	                    

	                     
							rec.id = $scope.zoneId;
							rec.primaryid = $scope.zoneId,
							rec.name = $scope.zoneName;
							rec.classification = $scope.zoneClassification;
							rec.restrictions = $scope.zoneCapacity;
							rec.description = $scope.zoneDescription;
							
							//rec.ppm = $scope.ppm;
							rec.vertices = bounds1;
							//rec.map_id = $scope.zoneMapId;
							rec.colorCode = $scope.colorCode;
							rec.zoneType = $scope.zoneType;
							rec.geometryType="rectangle";
							rec.layerType="Zone";
							//rec.socialdistance=$scope.socialdistance;
							rec.enableEdit();
	                        
	                      }
	                        $(".leaflet-vertex-icon")[index].style.background="red";
	                    }
						//Anand input field  validation 09-aug-2021
						  
							$("#zonename").keypress(
									function(e) {
										// if the letter is not alphabets then display
										// error
										// and don't type anything
										$return = ((e.which > 64) && (e.which < 91))
												|| ((e.which > 96) && (e.which < 123))
												|| (e.which == 8) || (e.which == 32)
												|| (e.which >= 48 && e.which <= 57)
										if (!$return)

										{
											// display error message
											
											return false;
										}
									});
							$("#zoneDesc").keypress(
									function(e) {
										// if the letter is not alphabets then display
										// error
										// and don't type anything
										$return = ((e.which > 64) && (e.which < 91))
												|| ((e.which > 96) && (e.which < 123))
												|| (e.which == 8) || (e.which == 32)
												|| (e.which >= 48 && e.which <= 57)
										if (!$return)

										{
											// display error message
											
											return false;
										}
									});
							
							$("#xVal0").keypress(
									function(e) {
										// if the letter is not digit then display error
										// and don't type anything
										
										if (e.which != 8 && e.which != 0
												&& (e.which < 48 || e.which > 57)) {
											
											return false;
										}
									});
							$("#yVal0").keypress(
									function(e) {
										// if the letter is not digit then display error
										// and don't type anything
										
										if (e.which != 8 && e.which != 0
												&& (e.which < 48 || e.which > 57)) {
											
											return false;
										}
									});
							$("#xVal1").keypress(
									function(e) {
										// if the letter is not digit then display error
										// and don't type anything
										
										if (e.which != 8 && e.which != 0
												&& (e.which < 48 || e.which > 57)) {
											
											return false;
										}
									});
							$("#yVal1").keypress(
									function(e) {
										// if the letter is not digit then display error
										// and don't type anything
										
										if (e.which != 8 && e.which != 0
												&& (e.which < 48 || e.which > 57)) {
											
											return false;
										}
									});
							$("#xVal2").keypress(
									function(e) {
										// if the letter is not digit then display error
										// and don't type anything
										
										if (e.which != 8 && e.which != 0
												&& (e.which < 48 || e.which > 57)) {
											
											return false;
										}
									});
							$("#yVal2").keypress(
									function(e) {
										// if the letter is not digit then display error
										// and don't type anything
										
										if (e.which != 8 && e.which != 0
												&& (e.which < 48 || e.which > 57)) {
											
											return false;
										}
									});
							$("#xVal3").keypress(
									function(e) {
										// if the letter is not digit then display error
										// and don't type anything
										
										if (e.which != 8 && e.which != 0
												&& (e.which < 48 || e.which > 57)) {
											
											return false;
										}
									});
							$("#id_tableValues").keypress(
									function(e) {
										// if the letter is not digit then display error
										// and don't type anything
										
										if (e.which != 8 && e.which != 0
												&& (e.which < 48 || e.which > 57)) {
											
											return false;
										}
									});
							$scope.getQuuppaLocators = function() {
								if($scope.isCheckedShowLocator){
									for(var i=0;i<$scope.locatorList.length;i++) {
									  $scope.addLocatorsIntoMap(i);
									}
								}
							} 

							 $scope.addLocatorsIntoMap = function(i) {
	 							 var color = '#10bc5e';
							 
								var html =""
								if($scope.locatorList[i].locatorType=="LD7L") {
									html ='<svg class="apOnline" style="width: 20px; height: 20px; color:  '+color+'" viewBox="0 0 48 48"> <g id="accessPoint" stroke="none" stroke-width="1" fill= '+color+'> <rect id="outline" stroke="currentcolor" stroke-width="2" fill="currentcolor" x="4" y="5" width="40" height="37.5" rx="6"> </rect> <ellipse id="led" fill="#ffffff" cx="23.41" cy="17.89" rx="4.13" ry="3.89"></ellipse></g> </svg>'
								} else if($scope.locatorList[i].locatorType=="Q17"){
									//html ='<svg style=" width: 20px;height: 20px; background: '+color+'; color: '+color+'; border-radius: 3px;" viewBox="0 0 48 48"><g id="accessPoint" stroke="none" stroke-width="1" fill="none"><rect id="outline" stroke="currentcolor" stroke-width="2" fill="#fff" x="4" y="5" width="40" height="37.5" rx="6"></rect></g></svg>';
									html ='<svg style=" width: 20px;height: 20px;" ><circle cx="10" cy="10" r="8" stroke='+color+' stroke-width="3" fill="white" /></svg>';
								} else {
									html ='<svg class="apOnline" style="width: 20px; height: 20px; color:  '+color+'" viewBox="0 0 48 48"> <g id="accessPoint" stroke="none" stroke-width="1" fill= '+color+'> <rect id="outline" stroke="currentcolor" stroke-width="2" fill="currentcolor" x="4" y="5" width="40" height="37.5" rx="6"> </rect> </g> </svg>'
								}
								
								locatorX = Math.abs(($scope.metersPerPixelX*$scope.originX)-$scope.locatorList[i].x); 
								locatorY = Math.abs(($scope.metersPerPixelY*$scope.originY)-$scope.locatorList[i].y);
							
								//$scope.accessmarker  = new L.Marker(pixelsToLatLng(($scope.locatorList[i].x)/($scope.ppmx), ($scope.locatorList[i].y)/($scope.ppmy)), {
								$scope.accessmarker  = new L.Marker(pixelsToLatLng(locatorX, locatorY), {
								    icon: new L.DivIcon({
								        className: 'my-div-icon',
								        html: html
								    })
								}); 
								$scope.accessmarker.addTo(map).on('mouseover', mouseoverLocatorPopup);
								// $scope.accessmarker.addTo(map).on('click',
								// accessPointClick);

	  							
								$scope.locatorList[i].model = $scope.locatorList[i].locatorType;
								$scope.locatorList[i].mac = $scope.locatorList[i].id;
								$scope.locatorList[i].name = $scope.locatorList[i].name;
	 							
								$scope.accessmarker.accessmarkerid = i;
								$scope.accessmarker.data = $scope.locatorList[i];
								$scope.accessmarker.markerType = 'locator';
					 }
								function mouseoverLocatorPopup(e) {
									
									if(!$scope.isEdit){ 
										
										//console.log(e.sourceTarget.data);
										var data = e.sourceTarget.data;
										  var name = data.name;
										  var mac = data.mac;
										  var model = data.model;
										  
										  if(name==undefined || name==null)
											  name = '-';
										  if(mac==undefined || mac==null)
											  mac = '-';
										  if(model==undefined || model==null)
											  model = '-';
										  
										   var popupContent = "<strong>Locator :" + name + "</strong><br /><strong>Model:" +
										   model + "</strong><br /> <strong>Mac:" + mac + "</strong><br />";
										   $scope.popup.setLatLng(e.sourceTarget._latlng);
										   $scope.popup.setContent(popupContent);
										   map.openPopup($scope.popup);
									} 
								}

							
				});
