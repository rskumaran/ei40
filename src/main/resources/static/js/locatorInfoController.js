app
		.controller(
				'locatorInfoController',
				function($scope, $http, $rootScope, $window, $timeout,$location,$filter) { 
					$scope.user = JSON.parse($window.sessionStorage
							.getItem("user"));
					if ($scope.user == null) {
						$location.path('/login');
						return;
					}
					$('#map').css('height',($(window).height()-135));     

 					$scope.user = JSON.parse($window.sessionStorage
							.getItem("user"));

					$scope.outlineDetailLayers = null;
					$scope.marker = null;
					$scope.marker1=null;
					$rootScope.hideloading('#loadingBar');
					$scope.zoneList = [];
					
					$scope.bSetZeroIndex=true;
					$scope.outlineList = [];	
					$scope.doorsList = [];
					$scope.wallList = [];
					$scope.partitionList = [];
					$scope.pathwayList = [];

					const OUTLINE_START = 100;
					const PATHWAY_START = 200;
					const PARTITION_START = 300;
					const WALL_START = 400;
					const DOOR_START = 500;
					$scope.zoneLayer = null;
					var screenWidth = window.screen.width
							* window.devicePixelRatio;
					var screenHeight = window.screen.height
							* window.devicePixelRatio;
					// $scope.mapDetails = '';

					$scope.zoneClassificationList = [];
					$scope.zoneTypeList=[];
					$scope.locatorList = [];
					$scope.zoneList =[];
					
					
					//$scope.selectedZoneIndex = 0;
					
					$scope.locatorSearchList = new Array();
					$scope.searchText = "";
					//$scope.nMaxZoneID = 0;
					//$scope.success = false;
					//$scope.isZoneDataView = true;
					//$scope.isZoneView = true;
					$rootScope.setPageName("Locator Details")
					$scope.isCheckedShowLocator = true; 
					$scope.isCheckedGrid = false; 
					$scope.isCheckedLayout = true; 
					var selectedRowCSS = {'background': '#4da3ff', 'color': '#ffffff', 'font-weight': 'bold'};
					var unSelectedRowCSS = {'background': '#ffffff', 'color': '#000000', 'font-weight': 'normal'};
					
					var maxZoomValue = 18;
					var minZoomValue = 8;   
					var defZoomValue = 8;  
					toastr.options = {
							  "closeButton": true,
							  "newestOnTop": false,
							  "progressBar": true,
							  "positionClass": "toast-top-right",
							  "preventDuplicates": false,
							  "onclick": null,
							  "showDuration": "100",
							  "hideDuration": "1000",
							  "timeOut": "5000",
							  "extendedTimeOut": "1000",
							  "showEasing": "swing",
							  "hideEasing": "linear",
							  "showMethod": "fadeIn",
							  "hideMethod": "fadeOut"
							}
					
					//Initialize map
					var map = L.map('map', {  
 						editable : true,
						maxZoom : maxZoomValue,
						minZoom : minZoomValue,
						zoom: defZoomValue,
						crs : L.CRS.Simple,
						doubleClickZoom : false,
						dragging:true,
						zoomDelta: 0.25,
						zoomSnap : 0.01,
						scrollWheelZoom: false, // disable original zoom function
						  smoothWheelZoom: true,  // enable smooth zoom 
						  smoothSensitivity: 1   // zoom speed. default is 1
					}).setView([ 0, 0 ], 14);
		
					//Zoom handler
					var ZoomViewer = L.Control.extend({
						 options: {
							  position: 'topleft' 
						 },
						onAdd: function(){

							var container= L.DomUtil.create('div');
							var gauge = L.DomUtil.create('div');
							container.style.width = '100px';
							container.style.background = 'rgba(255,255,255,0.5)';
							container.style.textAlign = 'center';
							map.on('zoomstart zoom zoomend', function(ev){
								gauge.innerHTML = 'Zoom: ' + ((map.getZoom()-minZoomValue)+1).toFixed(2);
							})
							container.appendChild(gauge);

							return container;
						}
					});

					(new ZoomViewer).addTo(map);
					map.zoomControl.setPosition('topleft');
					//Add Grid check box
					var command = L.control({position: 'topright'});

					command.onAdd = function (map) {
					    var div = L.DomUtil.create('div', 'command');

					    div.innerHTML = '<form>Grid (1X1M) <input id="command" type="checkbox" /></form>'; 
					    return div;
					};

					command.addTo(map);
					var layoutCmd = L.control({position: 'topright'});

					layoutCmd.onAdd = function (map) {
					    var div = L.DomUtil.create('div', 'layoutCmd');

					    div.innerHTML = '<form>Layout <input id="layoutCmd" type="checkbox" checked style="display: initial!important"/></form>';
					    return div;
					};
					layoutCmd.addTo(map);
					var locatorCmd = L.control({position: 'topright'});

					locatorCmd.onAdd = function (map) {
					    var div = L.DomUtil.create('div', 'locatorCmd');

					    div.innerHTML = '<form>Locator <input id="locatorCmd" type="checkbox" checked style="display: initial!important"/></form>';
					    return div;
					};
					locatorCmd.addTo(map);
					
					// add the event handler
					function handleCommand() {
						// alert("Clicked, checked = " + this.checked);
						$scope.isCheckedGrid = this.checked;
						if (this.checked == true) {
							$scope.grid.onAdd(map)
						} else {
							$scope.grid.onRemove(map);
						}
						$scope.zoneLayer.bringToFront();
					}
					function displayLayout() {
						// alert("Clicked, checked = " + this.checked);
						
                        if(this.checked==true) {
							 $scope.isCheckedLayout = true;
                            //showZones();
							 map.addLayer($scope.outlineDetailLayers);
							 $scope.zoneLayer.bringToFront();
                       } else {
							$scope.isCheckedLayout = false;
							map.removeLayer($scope.outlineDetailLayers);
                          
                       }
					}
					function displayLocators() {
                        map.eachLayer(function(layer) {
                            if(layer.markerType!=undefined && layer.markerType=='locator'){ 
                                layer.remove(); 
                            }

                        });
                        $scope.isCheckedShowLocator = this.checked;
                        if(this.checked==true) {
								for(var locatorIndex=0;locatorIndex<$scope.locatorList.length;locatorIndex++) {
	                                $scope.addLocatorsIntoMap(locatorIndex);
	                            } 
                        } 
					}
					$scope.isCheckedGrid = false; 
					$scope.isCheckedLayout = true; 
					$scope.isCheckedLocator = false; 

					document.getElementById("command").addEventListener(
							"click", handleCommand, false);
					document.getElementById("layoutCmd").addEventListener(
							"click", displayLayout, false);
					document.getElementById("locatorCmd").addEventListener(
							"click", displayLocators, false);
					if ($scope.reloadBtn != undefined || $scope.reloadBtn != null) {
						$scope.reloadBtn.onRemove(map);
					}
					
				  	$scope.reloadBtn = L.easyButton({
						  states:[
							    {
							      icon:'<i class="fas fa-sync-alt" style="font-size:20px; color:green"></i>',
							      title:     'Reload',  
							      onClick: fnReload
							    }
							  ]
							}); 
					
					
					
					$scope.reloadBtn.addTo(map);
					
					//$scope.addmapDrawOptions = function() {
						var options = {
							position : 'topright',
							draw : {
								polygon : true,
								polyline : false,
								circlemarker : false,
								circle : false,
								marker : false,
								rectangle : true
							},
							edit : false

						};
						if ($scope.drawControl != undefined || $scope.drawControl != null) {
							$scope.drawControl.onRemove(map);
						}
						if ($scope.drawControlEdit != undefined || $scope.drawControlEdit != null) {
							$scope.drawControlEdit.onRemove(map);
						}
						
						$scope.drawControl = new L.Control.Draw(options);
						$scope.drawControlEdit = new L.Control.Draw({
							draw : false
						});
						//map.addControl($scope.drawControl);
					//}

					function pixelsToLatLng(x, y) {
						return map.unproject([ x, y ], defZoomValue);
					}
					$scope.searchLocator = function() {
						$scope.locatorSearchList.length = 0;
						for (var i = 0; i < $scope.locatorList.length; i++) {
						 
							
							if ($scope.searchText == "") {
								$scope.locatorSearchList
								.push($scope.locatorList[i]);
							}else
								{
								if(($scope.locatorList[i].name != undefined
													&& $scope.locatorList[i].name != null ? $scope.locatorList[i].name
													: "").toLowerCase().includes($scope.searchText.toLowerCase())
													||($scope.locatorList[i].status != undefined
															&& $scope.locatorList[i].status != null ? $scope.locatorList[i].status
																	: "").toLowerCase().includes($scope.searchText.toLowerCase()))
									{

									$scope.locatorSearchList
									.push($scope.locatorList[i]);
									}
								
								}
						}
						if ($scope.bSetZeroIndex){
							if ($scope.locatorSearchList.length > 0) {
								$scope.selectLocator(0);
							}
						}else{
							$scope.bSetZeroIndex = true;
						}
						
					}

					$scope.campusGridArray = {};
					function loadMap() {

						var height = $scope.mapHeight;
						var width = $scope.mapWidth;
						
						
						$scope.outlineDetailLayers = new L.FeatureGroup();
						map.addLayer($scope.outlineDetailLayers);
						$scope.campusRect = null;
						$scope.campusGridArray = {};

						for (var i = 0; i < width ; i++) {

							for (var j = 0; j < height; j++) {

								var campusArray = [];
								campusArray.push({
									"x" : i,
									"y" : j
								});
								campusArray.push({
									"x" : i,
									"y" : (j + 1)
								});
								campusArray.push({
									"x" : (i + 1),
									"y" : (j + 1)
								});
								campusArray.push({
									"x" : (i + 1),
									"y" : j
								});

								$scope.campusGridArray[i + '_' + j] = campusArray;
							}

						}

						//console.log($scope.campusGridArray);

						map.setZoom(defZoomValue);
						var southWest = map.unproject([ 0, height ],
								defZoomValue);
						var northEast = map.unproject([ width, 0 ],
								defZoomValue);
						map.setMaxBounds(new L.LatLngBounds(southWest,
								northEast));
						
						map.fitBounds(new L.LatLngBounds(southWest, northEast));
						
						

						if ($scope.campusRect != undefined
								|| $scope.campusRect != null) {
							$scope.campusRect.onRemove(map);
						}

						var bounds1 = [
								[ pixelsToLatLng(parseFloat(0), parseFloat(0)) ],
								[ pixelsToLatLng(parseFloat(0),
										parseFloat(height)) ],
								[ pixelsToLatLng(parseFloat(width),
										parseFloat(height)) ],
								[ pixelsToLatLng(parseFloat(width),
										parseFloat(0)) ] ];

						$scope.campusRect = L.rectangle(bounds1, {
							color : $scope.mapColor,
							weight : 2,
							fillColor : $scope.mapColor,
							fillOpacity : 0.8
						});
						$scope.campusRect.addTo(map);
						$scope.campusRect.addTo($scope.outlineDetailLayers);

						if ($scope.marker != undefined || $scope.marker != null) {
							$scope.marker.onRemove(map);
						}
						var originDotIcon = L.icon({
						    iconUrl: './assets/img/red_circle_tr.png',
						    iconSize:     [12, 12] // size of the icon
						   
						});						
						
						$scope.marker = new L.Marker(
								pixelsToLatLng(0, 0),
								{
									icon : originDotIcon
								});					

						/*		
						$scope.marker = new L.Marker(
								pixelsToLatLng(0, 0),
								{
									icon : new L.DivIcon(
											{
												className : 'my-div-icon',
												html : '<div> <img class="staringImage" src="./assets/img/red_circle.png"/> '
											})
								},{draggable: true});
						
						*/
						$scope.marker.addTo(map);
						map.on('move', function (e) {
							
						});
						//Dragend event of map for update marker position
						map.on('dragend', function(e) {
							var cnt = map.getCenter();
						        var position = $scope.marker.getLatLng();
							lat = Number(position['lat']).toFixed(5);
							lng = Number(position['lng']).toFixed(5);
							//console.log(position);
							//setLeafLatLong(lat, lng);
							
							
						});
						$scope.mapDetails = {};
						$scope.mapDetails.width_m = width;
						$scope.mapDetails.height_m = height;
						$scope.mapDetails.height = height;
						$scope.mapDetails.width = width;
						$scope.mapDetails.mapType = 'custom';

						if ($scope.grid != undefined || $scope.grid != null) {
							$scope.grid.onRemove(map);
						}

						if ($scope.marker1 != undefined
								|| $scope.marker1 != null) {
							$scope.marker1.onRemove(map);
						}

						$scope.grid = L.grid(
								{
									options : {
										position : 'topright',
										bounds : new L.LatLngBounds(southWest,
												northEast),
										mapDetails : $scope.mapDetails,
										defZoomValue : defZoomValue,
										gridSize : 1
									}
								});
						
						/*
						$scope.marker1 = new L.Marker(
								pixelsToLatLng(-35, -5),
								{
									icon : new L.DivIcon(
											{
												className : 'my-div-icon',
												html : '<span class="staringlabel">(0,0)</span>'
											})
								});
						
						 */
						var originValueIcon = L.icon({
						    iconUrl: './assets/img/origin_tr.png',
						    iconSize:     [29, 16] // size of the icon
						   
						});						
						$scope.marker1 = new L.Marker(
								pixelsToLatLng(-2, -2),
								{
									icon : originValueIcon
								});					
						$scope.marker1.addTo(map);
						

						$timeout(function() {
							map.setView([ $scope.campusRect.getCenter().lat,
									$scope.campusRect.getCenter().lng ], map
									.getZoom());

							getAllOutlinedetails();
							
						}, 500);
						$timeout(function() {
							$scope.getZoneClassifications();
						}, 500);
						/*	
						$timeout(function() {
							$scope.getZoneDetails();

							}, 500);*/
							
					}
					function fnReload() {
						//toastr.success('Loading... Please wait.',"EI4.0")
						 //$scope.fnCancelEditZone();
						//getCampusDetails();
						map.eachLayer(function(layer) {
							if (layer.layerType=="Zone" ) {
								layer.remove();
							}
						});
						map.removeLayer($scope.zoneLayer);
						$scope.getZoneDetails()
						//toastr.success('Loaded successfully.',"EI4.0")
					}

					function getCampusDetails() {

						$http({
							method : 'GET',
							url : './campusDetails'
						})
								.then(
										function success(response) {
											$rootScope
													.hideloading('#loadingBar');
											if (response != null
													&& response.data != null
													&& response.data != "BAD_REQUEST") {
												$scope.campusDetails = response.data.data;

												$scope.mapHeight = $scope.campusDetails.height;
												$scope.mapWidth = $scope.campusDetails.width;
												$scope.mapColor = $scope.campusDetails.colorCode;
												$scope.campusId = $scope.campusDetails.campusId;
												$scope.campusName = $scope.campusDetails.name;
												$scope.metersPerPixelX = $scope.campusDetails.metersPerPixelX;
												$scope.metersPerPixelY = $scope.campusDetails.metersPerPixelY;
												$scope.originX = $scope.campusDetails.originX;
												$scope.originY = $scope.campusDetails.originY;

												
												

												$timeout(function() {
													loadMap();
												}, 500);
											}
										},
										function error(response) {
											$rootScope.hideloading('#loadingBar');
											$rootScope.fnHttpError(response);
										});
					}

					getCampusDetails();	
					
					$scope.area = function(points) {
						var area = 0, i, j, point1, point2;

						var length = points.length;

						for (i = 0, j = length - 1; i < length; j = i, i += 1) {
							point1 = points[i];
							point2 = points[j];
							area += point1.x * point2.y;
							area -= point1.y * point2.x;
						}
						area /= 2;

						return Math.abs(area).toFixed(3);
					}
					
					function latLngToPixels(latlng) {
						var pixelArr = [];
						for (var i = 0; i < latlng[0].length; i++) {

							var latlngArr = latlng[0][i];

							var pt= map.project([ latlngArr.lat,
                                latlngArr.lng ], defZoomValue); 
							
                            pixelArr.push({
                                "x" : pt.x.toFixed(),
                                "y" : pt.y.toFixed(),
                            });


						}
						return pixelArr;
					} 
					function latLngToPixelsFloat(latlng) {
						var pixelArr = [];
						for (var i = 0; i < latlng[0].length; i++) {

							var latlngArr = latlng[0][i];

							var pt = map.project(
									[ latlngArr.lat, latlngArr.lng ],
									defZoomValue);

							pixelArr.push({
								"x" : (parseFloat(pt.x)).toFixed(2),
								"y" : (parseFloat(pt.y)).toFixed(2),
							});

						}
						return pixelArr;
					}
					
					function roundVal(latlng) {
						var pixelArr = [];
						for (var i = 0; i < latlng.length; i++) {

							var pt = latlng[i];
 							 
                            
                            pixelArr.push({
                                "x" : (parseFloat(pt.x)).toFixed(),
                                "y" : (parseFloat(pt.y)).toFixed(),
                            });

						}
						return pixelArr;
					} 
					
					function editlatLngToPixels(latlng) {
						var pixelArr = [];
						for (var i = 0; i < latlng[0].length; i++) {

							var latlngArr = latlng[0][i];
							var pt = pt = map.project([ latlngArr.lat,
									latlngArr.lng ], defZoomValue);

							pixelArr.push({
								"x" : pt.x.toFixed(),
								"y" : pt.y.toFixed(),
							});

						}
						return pixelArr;
					}
					
					function saveText(text, filename){
						  var a = document.createElement('a');
						  a.setAttribute('href', 'data:text/plain;charset=utf-u,'+encodeURIComponent(text));
						  a.setAttribute('download', filename);
						  a.click()
						}
					
					function getAllOutlinedetails() {
						map.eachLayer(function(layer) {
							if (layer.id !== undefined) {
								layer.remove();
							}
						});

						$http({
							method : 'GET',
							url : './outlineDetails/' + $scope.campusId
						})
								.then(
										function success(response) {
											$rootScope
													.hideloading('#loadingBar');
											if (response != null
													&& response.data != null
													&& response.data != "BAD_REQUEST") {
												$scope.noList = false;
												$scope.outlineList.length = 0;
												if (response.data.status == 'success') {
													for (var i = 0; i < response.data.data.length; i++) {
														if (response.data.data[i].active)
															$scope.outlineList
																	.push(response.data.data[i]);
													}
													

													if ($scope.outlineList.length > 0) {
														$scope.addOutlineIntoMap();
													} else {
														$scope.noList = true;
														$scope.valueList = false;
														$scope.error = true;
														$scope.errorMsg = "No outlines details are available";
														$timeout(
																function() {
																	$scope.error = false;
																}, 500);
													}
													$scope.getAllPathways();
												} else {
																										
													$scope.error = true;
													$scope.alert_error_msg = response.data.message;
													$timeout(function() {
														$scope.error = false;
													}, 500);
												}

											}
										},
										function error(response) {
											$rootScope.hideloading('#loadingBar');
											$rootScope.fnHttpError(response);
										});
					}
					function getBoundsArray(points) {
						var bounds = [];
						for (var i = 0; i < points.length; i++) {

							bounds.push(pixelsToLatLng(parseFloat(points[i].x),
									parseFloat(points[i].y)));

						}
						return bounds;
					}

					$scope.addOutlineIntoMap = function() {
						if ($scope.outlineList.length > 0) {
							for (var i = 0; i < $scope.outlineList.length; i++) {
								if ($scope.outlineList[i].active) {
									$scope.outlineList[i].area = $scope
											.area($scope.outlineList[i].coordinateList);
									$scope.colorCode = $scope.outlineList[i].bgColor;
									var bounds = getBoundsArray($scope.outlineList[i].coordinateList);
									var layer = null;
									var theme = {
										color : $scope.colorCode,
										weight : 1,
										fillColor : $scope.colorCode,
										fillOpacity : 0.5
									};

									if ($scope.outlineList[i].geometryType == undefined
											|| $scope.outlineList[i].geometryType == null
											|| $scope.outlineList[i].geometryType == "polygon"
											|| $scope.outlineList[i].geometryType == "") {

										$scope.outlineList[i].geometryType = "polygon";
										layer = L.polygon(bounds, theme);

									} else {
										layer = L.rectangle(bounds, theme);
										}
									
									layer.addTo(map)
									.addTo($scope.outlineDetailLayers);
									$scope.outlinesArea = $scope.area(editlatLngToPixels(layer.getLatLngs()));
									

									layer.id = i+OUTLINE_START;
									layer.outlinesId = $scope.outlineList[i].outlinesId+OUTLINE_START;
									layer.name = $scope.outlineList[i].name;
									layer.outlinesArea = $scope.outlinesArea;
									layer.colorCode = $scope.colorCode;
									layer.menuType = "Building";
									layer.wallType = $scope.outlineList[i].wallType;
									layer.palceType = $scope.outlineList[i].placeType;
									layer.geometryType = $scope.outlineList[i].geometryType;
									layer.coordinateList = bounds;
									layer.layerType="Outline";

								}
							}
							
							//var jsonText = JSON.stringify($scope.outlineList);
							//saveText(jsonText, "outline.json");
							
						}
					}
					$scope.getZoneDetails = function(){
						
						
                    if ($rootScope.checkNetconnection() == false) { 
				               
				               $rootScope.alertErrorDialog('Please check the Internet Connection'); 
					           return;
					   } 						
                    $rootScope.showloading('#loadingBar');
                    
						$http({
							method : 'GET',
							url : './zone'
						})
								.then(
										function success(response) {
											$rootScope.hideloading('#loadingBar');
											if (response != null
													&& response.data != null
													&& response.data != "BAD_REQUEST") { 
											
												$scope.noList = false;
												$scope.valueList =false;
												$scope.zoneLists= response.data.data; 
												$scope.zoneList =[];
												$scope.locatorSearchList = [];
												$scope.zoneTypeList=[];
												$scope.zoneLayer = new L.FeatureGroup();
												map.addLayer( $scope.zoneLayer);
											
												$scope.zoneTypeList.push('Tracking zone');
												$scope.zoneTypeList.push('Dead Zone');
												
 												for (var i = 0; i < $scope.zoneLists.length; i++) {
													if($scope.zoneLists[i].active) {
														if ($scope.zoneLists[i].zoneType == "Trackingzone"){
															$scope.zoneLists[i].zoneType = "Tracking zone";
														}
														else{
															$scope.zoneLists[i].zoneType = "Dead Zone";
														}
 														$scope.zoneList.push($scope.zoneLists[i])
 														
													}  
 												}
 												
												 
												
													if($scope.zoneList.length!=0){
 													for (var i = 0; i < $scope.zoneList.length; i++) {
													if($scope.zoneList[i].active) {
														
	  													$scope.zoneList[i].area = $scope.area($scope.zoneList[i].coordinateList);
														var bounds  = [];
														//var classification;
														if($scope.zoneList[i].zoneClassification != null && 
															$scope.zoneList[i].zoneClassification != "" &&
															$scope.zoneList[i].zoneClassification.active == true
															){

															$scope.colorCode = $scope.zoneList[i].zoneClassification.color;
																 
																	 
															} else {
																$scope.colorCode = "#ff7800"
															}
														
														if($scope.zoneList[i].geometryType== undefined || $scope.zoneList[i].geometryType==null || $scope.zoneList[i].geometryType=="polygon" || $scope.zoneList[i].geometryType==""){ 
															for (var j = 0; j < $scope.zoneList[i].coordinateList.length; j++) {  
																	bounds.push(pixelsToLatLng(parseFloat($scope.zoneList[i].coordinateList[j].x) 
																			,parseFloat($scope.zoneList[i].coordinateList[j].y)));
															   
															}
															
															$scope.zoneList[i].geometryType="polygon";
																		var pol = L
																		.polygon(
																				bounds,
																				{
																					color : $scope.colorCode ,
																					weight : 1,
																					fillColor :$scope.colorCode , 
																					fillOpacity: 0.5
																					})
																		.addTo(
																				map)
																		.addTo(
																				$scope.zoneLayer);
																		/*.on( //kumar
																				'click',
																				onZoneClick).on('mouseover', mouseOverPopup);*/ 
	
																		pol.id = $scope.zoneList[i].id;
																		pol.primaryid = $scope.zoneList[i].id
																		pol.name = $scope.zoneList[i].name;
																		pol.classification = $scope.zoneList[i].zoneClassification.className;
																		pol.capacity = $scope.zoneList[i].capacity;
																		pol.ppm = $scope.ppm;
																		pol.coordinateList = bounds;
																		
																		pol.colorCode = $scope.colorCode;
																		pol.geometryType="polygon";
																		pol.zoneType = $scope.zoneList[i].zoneType;
																		pol.zonearea=$scope.zoneList[i].area;
																		pol.layerType="Zone";
																		
	 													} else {
	
														var bounds1 = [
																[ pixelsToLatLng(
																		parseFloat($scope.zoneList[i].coordinateList[0].x) ,
																		parseFloat($scope.zoneList[i].coordinateList[0].y)) ],
																[ pixelsToLatLng(
																		parseFloat($scope.zoneList[i].coordinateList[1].x),
																		parseFloat($scope.zoneList[i].coordinateList[1].y)) ],
																[ pixelsToLatLng(
																		parseFloat($scope.zoneList[i].coordinateList[2].x),
																		parseFloat($scope.zoneList[i].coordinateList[2].y)) ],
																[ pixelsToLatLng(
																		parseFloat($scope.zoneList[i].coordinateList[3].x),
																		parseFloat($scope.zoneList[i].coordinateList[3].y)) ] ];
														var rec = L
																.rectangle(
																		bounds1,
																		{
																			color : $scope.colorCode ,
																			weight : 1,
																			fillColor :$scope.colorCode , 
																			fillOpacity: 0.5
																		})
																.addTo(
																		map)
																.addTo(
																		$scope.zoneLayer)
																/*.on(
																		'click',
																		onZoneClick).on('mouseover', mouseOverPopup);*/ 
													 
														rec.id = $scope.zoneList[i].id;
														rec.primaryid = $scope.zoneList[i].id;
														rec.name = $scope.zoneList[i].name;
														rec.classification = $scope.zoneList[i].zoneClassification.className;
														rec.capacity = $scope.zoneList[i].capacity;
														rec.ppm = $scope.ppm;
														rec.coordinateList = bounds1;
														rec.geometryType="rectangle";
														rec.zoneType = $scope.zoneList[i].zoneType; 
														rec.colorCode = $scope.colorCode;
														rec.zonearea=$scope.zoneList[i].area;
														rec.layerType="Zone";
														
	 												  }
													}
													
													
												  }
 													
 													//$scope.searchZone();
 													//$scope.selectLocator(0);
 													$scope.getLocatorDetails()
 													$scope.zoneLayer.bringToFront();
													} else {
														$scope.noList = true;
														$scope.valueList =false;
														$scope.error = true;

														$scope.errorMsg="No Zones are available";
														
														$timeout(function() {
															$scope.error = false;
														}, 500);
														//$scope.locatorSearchList.push('No Zones are available');
													}
												
   
											} else {
												$scope.noList = true;
												$scope.valueList =false;
												$scope.errorMsg="No zones are available";
											
												$timeout(function() {
													$scope.error = false;
												}, 500);
											}
											
										},function error(response) {
											$rootScope.hideloading('#loadingBar');
											$rootScope.fnHttpError(response);
										});
						
					}
					function GetSortOrder(prop) {    
						return function(a, b) {    
							if(a[prop]!=null&&b[prop]!=null){
								if (a[prop].toLowerCase() > b[prop].toLowerCase()) {    
									return 1;    
								} else if (a[prop].toLowerCase() < b[prop].toLowerCase()) {    
									return -1;    
								}    
							}
							return 0;    
						}    
					} 
					$scope.getLocatorDetails = function(){
							$http({
								method : 'GET',
								url : './locatorDetails/status'
							})
									.then(
											function success(response) {
												$rootScope.hideloading('#loadingBar');
												if (response != null
														&& response.data != null
														&& response.data != "BAD_REQUEST") { 
												
													if (response.data.status == "success"){
														$scope.locatorList=[];
													
													/*if (response.data.locator.length > 0) {
													for (var i = 0; i < 13; i++) {
														var locatorDetail={}
														locatorDetail.index = i;
														locatorDetail.id =  "a4da22eff8a8";
															locatorDetail.locatorType= "Q17";
															locatorDetail.name= "L00" +(i+1).toString();
															locatorDetail.status= "Ok";
															locatorDetail.x =  "5.2";
															locatorDetail.y =  "17.22"
															$scope.locatorList.push(locatorDetail);
													}*/
															
														$scope.valueList =true;
														response.data.locator.sort(GetSortOrder("name"));
														if (response.data.locator.length > 0) {
															for (var i = 0; i < response.data.locator.length; i++) {
																	response.data.locator[i].index = i;
																	$scope.locatorList.push(response.data.locator[i]);
															}
															
															$scope.addQuuppaLocators();
															$scope.searchLocator();
														}
														else{
															$scope.noList = true;
															$scope.valueList =false;
															
															
															
														}
													}else if (response.data.status == "error"){
														toastr.error("Connection Timeout.Please check net connection","EI4.0")
													}
													
												} else {
													$scope.noList = true;
													$scope.valueList =false;
													toastr.error("No Locators are available","EI4.0")
													
												}
											},function error(response) {
												$rootScope.hideloading('#loadingBar');
												$rootScope.fnHttpError(response);
											});
							
						}
					
					
					function fnMoveScrollBar() {
						var rows =$('#idLocatorTable tr');
						rows[$scope.selectedRow].scrollIntoView({
					    	behavior: 'smooth',
					      block: 'center'
					    });
						
					}
					  function onLocatorClick(e) {
						  
							  $scope.fnHighlightClickedLocator(e);
						
					}
					  $scope.fnHighlightClickedLocator = function(e)  {
							if ($scope.searchText != ""){
								$scope.searchText = "";
								$scope.bSetZeroIndex= false;
								$scope.searchLocator();
								$scope.$apply();
							}
							
							$(".tbodyWidth").css({ 'height' : "200px" });

							var rowCount = ($('#idLocatorTable tr').length) - 1;
							for (var i = 0; i < rowCount; i++) {
								$("#idLocatorTable > tbody > tr:nth-child(" + (i + 1) + ")").css(unSelectedRowCSS);
							}

							for (var i = 0; i < rowCount; i++) {
								if ( $scope.locatorSearchList[i].index == e.sourceTarget.accessmarkerid) {
									$("#idLocatorTable > tbody > tr:nth-child(" + (i + 1) + ")").css(selectedRowCSS);
									$scope.selectedRow = i;
									break;
								}
							}
							//var table = document.querySelector('#idLocatorTable');
							//var rows = table.querySelectorAll('tr');
							
							map.eachLayer(function(layer) {
								 if(layer.markerType!=undefined && layer.markerType=='locator'){ 
									if (e.sourceTarget.accessmarkerid == layer.data.index) {
										layer.setOpacity(1);
									} else {
										layer.setOpacity(0.3);
									}
								}
	 						});
							
							fnMoveScrollBar();
							//$scope.zoneLayer.bringToFront();
							

					  }
						$scope.popup = L.popup({
							maxWidth : 400
						});
						var mouseroverid = null;
						
						$scope.getZoneClassifications = function() {
					        if ($rootScope.checkNetconnection() == false) { 
					        	$rootScope.alertDialogChecknet();
							           return;
							   } 					
								$http({
									method : 'GET',
									url : './zoneClassification',
								})          
										.then(
												function success(response) {
													if (response != null
															&& response.data != null
															&& response.data != "BAD_REQUEST") {
														$scope.noList = false;
														$scope.zoneClassificationList.length = 0;
														var classifications = response.data.data;
														if (classifications.length > 0)	{

															for (var i = 0; i < classifications.length; i++) {
																if (classifications[i].active) {
																	$scope.zoneClassificationList
																			.push(classifications[i]);
																}
															}
														} 
														if ($scope.zoneClassificationList.length == 0){
															$scope.noList = true;
															
															$scope.error = true;
															toastr.error('No Classification are available.Please add classification',"EI4.0")	
															$scope.errorMsg="No Classification are available";
															//$("#selectedId").hide(); 
															$scope.locatorSearchList.push('No Classification are available');
															$timeout(function() {
																$scope.error = false;
															}, 500);
														} else {
															map.addControl($scope.drawControl);
															$scope.getZoneDetails();
														}
														
														//$scope.zoneClassificationList.sort(GetSortOrder("classification"));
														
														//$scope.searchClassification(); 
													} else {
														$scope.noList = true;
														
														$scope.error = true;

														$scope.errorMsg="No Classification are available";
														
														$scope.locatorSearchList.push('No Classification are available');
														$timeout(function() {
															$scope.error = false;
														}, 500);
														
													}
												},function error(response) {
													$rootScope.hideloading('#loadingBar');
													$rootScope.fnHttpError(response);
												});
							}
						$scope.getAllPathways = function() {

							$scope.isPathwayEdit = false;

							$http({
								method : 'GET',
								url : './pathWay'
							})
									.then(
											function success(response) {
												$rootScope
														.hideloading('#loadingBar');
												if (response != null
														&& response.data != null
														&& response.data != "BAD_REQUEST") {

													$scope.pathwayList.length = 0;
													if (response.data.status == 'success') {
														for (var i = 0; i < response.data.data.length; i++) {
															if (response.data.data[i].active)
																$scope.pathwayList
																		.push(response.data.data[i]);
														}

														$scope.addPathwayIntoMap();
														$scope.getAllPartitions();
													} else {
														
														$scope.error = true;
														$scope.alert_error_msg = response.data.message;
														$timeout(function() {
															$scope.error = false;
														}, 500);
													}
												}
											},
											function error(response) {
												$rootScope.hideloading('#loadingBar');
												$rootScope.fnHttpError(response);
											});

						}
						$scope.addPathwayIntoMap = function() {
							if ($scope.pathwayList.length > 0) {
								for (var i = 0; i < $scope.pathwayList.length; i++) {

									var bounds = getBoundsArray($scope.pathwayList[i].coordinates);
									var theme = {
											color : $scope.pathwayList[i].colorCode,
											weight : 3,
											fillColor : '#00FFFFFF',
											fill : 'url(dist/img/path/image.gif)',
											fillOpacity : 10
										};
									var layer = null;
									if ($scope.pathwayList[i].geometryType == "polygon") {
										layer = L.polygon(bounds, theme);
									} else {
										layer = L.rectangle(bounds, theme);
									}
									layer.addTo(map).addTo($scope.outlineDetailLayers);

									layer.layerType = "Pathway";
									layer.campusId = $scope.pathwayList[i].campusDetails.campusId;
									layer.pathwayId = $scope.pathwayList[i].id+PATHWAY_START;
									layer.pathwayName = $scope.pathwayList[i].name;
									layer.colorCode = $scope.pathwayList[i].colorCode;
									layer.geometryType = $scope.pathwayList[i].geometryType;
									layer.coordinateList = bounds;

								}
								//var jsonText = JSON.stringify($scope.pathwayList);
								//saveText(jsonText, "Pathways.json");

							}

						}
						$scope.getAllPartitions = function() {

							$scope.isPartitionEdit = false;

							$http({
								method : 'GET',
								url : './partitionsDetails'
							})
									.then(
											function success(response) {
												$rootScope
														.hideloading('#loadingBar');
												if (response != null
														&& response.data != null
														&& response.data != "BAD_REQUEST") {

													$scope.partitionList.length = 0;
													if (response.data.status == 'success') {
														for (var i = 0; i < response.data.data.length; i++) {
															if (response.data.data[i].active)
																$scope.partitionList
																		.push(response.data.data[i]);
														}
														
														$scope.addPartitionIntoMap();
														
														//var jsonText = JSON.stringify($scope.partitionList);
														//saveText(jsonText, "Partitions.json");

														$scope.getAllWalls();
													}else {
														
														$scope.error = true;
														$scope.alert_error_msg = response.data.message;
														$timeout(function() {
															$scope.error = false;
														}, 500);
													}
												}
											},
											function error(response) {
												$rootScope.hideloading('#loadingBar');
												$rootScope.fnHttpError(response);
											});

						}
						$scope.addPartitionIntoMap = function()
						{
							if ($scope.partitionList.length > 0) {
								for (var i = 0; i < $scope.partitionList.length; i++) {
									var bounds = getBoundsArray($scope.partitionList[i].coordinates);
									var theme = {
										color : $scope.partitionList[i].colorCode,
										weight : 3,
										fillColor : $scope.partitionList[i].colorCode,
										fillOpacity : 0
									};
									var layer = null;

									if ($scope.partitionList[i].geometryType == "polyline") {

										layer = L.polyline(bounds, theme);
									} else if ($scope.partitionList[i].geometryType == "polygon") {
										layer = L.polygon(bounds,theme);
									} else {
										layer = L.rectangle(bounds, theme);
									}

									layer.addTo(map)
											.addTo($scope.outlineDetailLayers);

									layer.outlinesId = $scope.partitionList[i].outlinesDetails.outlinesId+OUTLINE_START;
									layer.layerType = "Partition";
									layer.partitionId = $scope.partitionList[i].id+PARTITION_START;
									layer.partitionName = $scope.partitionList[i].name;
									layer.colorCode = $scope.partitionList[i].colorCode;
									layer.geometryType = $scope.partitionList[i].geometryType;
									layer.coordinateList = bounds;

								}
							}
						}
						$scope.getAllWalls = function() {
							$scope.isWallEdit = false;

							$http({
								method : 'GET',
								url : './wallsDetails'
							})
									.then(
											function success(response) {
												$rootScope
														.hideloading('#loadingBar');
												if (response != null
														&& response.data != null
														&& response.data != "BAD_REQUEST") {

													$scope.wallList.length = 0;
													if (response.data.status == 'success') {
														for (var i = 0; i < response.data.data.length; i++) {
															if (response.data.data[i].active)
																$scope.wallList
																		.push(response.data.data[i]);
														}

														$scope.addWallIntoMap();
														//var jsonText = JSON.stringify($scope.wallList);
														//saveText(jsonText, "Walls.json");

														$scope.getAllDoors();

													}else {
														
														$scope.error = true;
														$scope.alert_error_msg = response.data.message;
														$timeout(function() {
															$scope.error = false;
														}, 500);
													}
												}
											},
											function error(response) {
												$rootScope.hideloading('#loadingBar');
												$rootScope.fnHttpError(response);
											});

						}
						$scope.addWallIntoMap = function(){
							if ($scope.wallList.length > 0) {
								for (var i = 0; i < $scope.wallList.length; i++) {


									var bounds = getBoundsArray($scope.wallList[i].coordinates);
									var theme = {
										color : $scope.wallList[i].colorCode,
										weight : 3,
										fillColor : $scope.wallList[i].colorCode,
										fillOpacity : 0
									};
									var layer = null;

									if ($scope.wallList[i].geometryType == "polyline") {
										layer = L
												.polyline(
														bounds,
														theme);
									} else if ($scope.wallList[i].geometryType == "polygon") {
										layer = L
												.polygon(
														bounds,
														theme);
									} else {
										layer = L
												.rectangle(
														bounds,
														theme);
									}

									layer
											.addTo(map)
											.addTo(
													$scope.outlineDetailLayers);

									layer.outlinesId = $scope.wallList[i].outlinesDetails.outlinesId+OUTLINE_START;
									layer.layerType = "Wall";
									layer.wallId = $scope.wallList[i].id+WALL_START;
									layer.name = $scope.wallList[i].name;
									layer.colorCode = $scope.wallList[i].colorCode;
									layer.geometryType = $scope.wallList[i].geometryType;
									layer.coordinateList = bounds;

								}
							}
						}
						$scope.getAllDoors = function() {

							$http({
								method : 'GET',
								url : './doorsDetails'
							})
									.then(
											function success(response) {
												$rootScope
														.hideloading('#loadingBar');
												if (response != null
														&& response.data != null
														&& response.data != "BAD_REQUEST") {

													$scope.doorsList.length = 0;
													if (response.data.status == 'success') {
														for (var i = 0; i < response.data.data.length; i++) {
															if (response.data.data[i].active)
																$scope.doorsList
																		.push(response.data.data[i]);
														}

														$scope.addDoorIntoMap();

													}
												}
											},
											function error(response) {
												$rootScope.hideloading('#loadingBar');
												$rootScope.fnHttpError(response);
											});
						}
						
						
						$scope.addDoorIntoMap = function(){
							if ($scope.doorsList.length > 0) {
								for (var i = 0; i < $scope.doorsList.length; i++) {
									var bounds = getBoundsArray($scope.doorsList[i].coordinates); 

									var layer = L
											.polyline(
													bounds,
													{
														color : $scope.doorsList[i].colorCode,
														weight : 1,
														fillColor : $scope.doorsList[i].colorCode,
														fillOpacity : 0.5
													})
											.addTo(map)
											.addTo(
													$scope.outlineDetailLayers);

									layer.outlinesId = $scope.doorsList[i].outlinesDetails.outlinesId+OUTLINE_START;
									layer.layerType = "Door";
									layer.doorId = $scope.doorsList[i].id+DOOR_START;
									layer.doorName = $scope.doorsList[i].name;
									layer.doorColorCode = $scope.doorsList[i].colorCode;
									layer.geometryType = "Line";
									layer.coordinateList = bounds;
									

								}
								//var jsonText = JSON.stringify($scope.doorsList);
								//saveText(jsonText, "Doors.json");

							}
						}
						$scope.selectedRow = null;
						 
						$scope.selectLocator = function(index) {
								$scope.fnHighlightSelectedZone(index);
						}
						$scope.fnHighlightSelectedZone = function(index) {
						
							
							$(".tbodyWidth").css({ 'height' : "200px" });

							
							var rowCount = ($('#idLocatorTable tr').length) - 1;
							for (var i = 0; i < rowCount; i++) {
								$("#idLocatorTable > tbody > tr:nth-child(" + (i + 1) + ")").css(unSelectedRowCSS);
							}
							
							$("#idLocatorTable > tbody > tr:nth-child(" + (index + 1) + ")").css(selectedRowCSS); 

							 
							
							$scope.selectedRow = index;
							$scope.accessmarker.markerType = 'locator';
							map.eachLayer(function(layer) {
								 if(layer.markerType!=undefined && layer.markerType=='locator'){ 
								if ($scope.locatorSearchList.length > 0){ 
									if ($scope.locatorSearchList[$scope.selectedRow].index == layer.data.index) {
										layer.setOpacity(1);
									} else {
										layer.setOpacity(0.3);
									}
								}
								 }
	 						});
							locatorX = Math.abs(($scope.metersPerPixelX*$scope.originX)-$scope.locatorSearchList[$scope.selectedRow].x); 
							locatorY = Math.abs(($scope.metersPerPixelY*$scope.originY)-$scope.locatorSearchList[$scope.selectedRow].y);
							map
							.panTo(pixelsToLatLng(
									parseFloat(locatorX),
									parseFloat(locatorY)));
							
							
						}
  					      $scope.addQuuppaLocators = function() {
								if($scope.isCheckedShowLocator){
									for(var i=0;i<$scope.locatorList.length;i++) {
									  $scope.addLocatorsIntoMap(i);
									}
								}
							} 

							 $scope.addLocatorsIntoMap = function(i) {
	 							 var color = '#ff0000'; //Failed and NotConnectable - red color by default
	 							 
	 							var okColor = '#008000'; //green
	 							var waitingColor = '#ffff00'; //yellow
	 							var discoveredColor = '#0000ff'; //blue
	 							var fwUpgradeColor = '#800040'; //pink
	 							
								var html =""
									
								if ($scope.locatorList[i].status != undefined){
									if  ($scope.locatorList[i].status == "Ok"){ 
										color = okColor;
                                    }else if ($scope.locatorList[i].status == "Waiting"){
                                    	color = waitingColor;
                                    }else if ($scope.locatorList[i].status == "Discovered"){
                                    	color = discoveredColor;
                                    }else if ($scope.locatorList[i].status == "FWUpgrade"){
                                    	color = fwUpgradeColor;
                                    }
							}
								
								if($scope.locatorList[i].locatorType=="LD7L") {
									html ='<svg class="apOnline" style="width: 20px; height: 20px; color:  '+color+'" viewBox="0 0 48 48"> <g id="accessPoint" stroke="none" stroke-width="1" fill= '+color+'> <rect id="outline" stroke="currentcolor" stroke-width="2" fill="currentcolor" x="4" y="5" width="40" height="37.5" rx="6"> </rect> <ellipse id="led" fill="#ffffff" cx="23.41" cy="17.89" rx="4.13" ry="3.89"></ellipse></g> </svg>'
								} else if($scope.locatorList[i].locatorType=="Q17"){
									//html ='<svg style=" width: 20px;height: 20px; background: '+color+'; color: '+color+'; border-radius: 3px;" viewBox="0 0 48 48"><g id="accessPoint" stroke="none" stroke-width="1" fill="none"><rect id="outline" stroke="currentcolor" stroke-width="2" fill="#fff" x="4" y="5" width="40" height="37.5" rx="6"></rect></g></svg>';
									html ='<svg style=" width: 20px;height: 20px;" ><circle cx="10" cy="10" r="8" stroke='+color+' stroke-width="3" fill="white" /></svg>';
								} else {
									html ='<svg class="apOnline" style="width: 20px; height: 20px; color:  '+color+'" viewBox="0 0 48 48"> <g id="accessPoint" stroke="none" stroke-width="1" fill= '+color+'> <rect id="outline" stroke="currentcolor" stroke-width="2" fill="currentcolor" x="4" y="5" width="40" height="37.5" rx="6"> </rect> </g> </svg>'
								}
								
								locatorX = Math.abs(($scope.metersPerPixelX*$scope.originX)-$scope.locatorList[i].x); 
								locatorY = Math.abs(($scope.metersPerPixelY*$scope.originY)-$scope.locatorList[i].y);
							
								//$scope.accessmarker  = new L.Marker(pixelsToLatLng(($scope.locatorList[i].x)/($scope.ppmx), ($scope.locatorList[i].y)/($scope.ppmy)), {
								$scope.accessmarker  = new L.Marker(pixelsToLatLng(locatorX, locatorY), {
								    icon: new L.DivIcon({
								        className: 'my-div-icon',
								        html: html
								    })
								}); 
								
								$scope.accessmarker.addTo(map).on('click',onLocatorClick).on('mouseover', mouseoverLocatorPopup);
								// $scope.accessmarker.addTo(map).on('click',
								// accessPointClick);

	  							
								$scope.locatorList[i].model = $scope.locatorList[i].locatorType;
								$scope.locatorList[i].mac = $scope.locatorList[i].id;
								$scope.locatorList[i].name = $scope.locatorList[i].name;
	 							
								$scope.accessmarker.accessmarkerid = i;
								$scope.accessmarker.data = $scope.locatorList[i];
								$scope.accessmarker.markerType = 'locator';
					 }
								function mouseoverLocatorPopup(e) {
										var data = e.sourceTarget.data;
										  var name = data.name;
										  var mac = data.mac;
										  var model = data.model;
										  if(name==undefined || name==null)
											  name = '-';
										  if(mac==undefined || mac==null)
											  mac = '-';
										  if(model==undefined || model==null)
											  model = '-';
										  
										   var popupContent = "<strong>Locator :" + name + "</strong><br /><strong>Model:" +
										   model + "</strong><br /> <strong>Mac:" + mac + "</strong><br />";
										   $scope.popup.setLatLng(e.sourceTarget._latlng);
										   $scope.popup.setContent(popupContent);
										   map.openPopup($scope.popup);
								}

							
				});
