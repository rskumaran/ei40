app
		.controller(
				'tagSKUManagementController',
				function($scope, $http, $window, $rootScope, $location,
						$timeout) {
					$scope.user = JSON.parse($window.sessionStorage
							.getItem("user"));
					if ($scope.user == null) {
						$location.path('/login');
						return;
					}

					if ($scope.authen == undefined) {
						$scope.authen = {};
					}
					if ($scope.fieldName == undefined) {
						$scope.fieldName = {};
					}
					$scope.currentMenuName = $window.sessionStorage
							.getItem("menuName");
 
					
					
					
					$scope.authen.countryCode = "+1";
					$scope.authen.contactPersonCountryCode = "+1";
					$scope.authen.tagType = "Employee";
					$scope.checkTagMacId = {};
					$scope.checkTagMacId = {};
					$scope.checkTagTagId = {};
					$scope.checkTagType = {};
					$scope.checkTagPeopleId = {};
					$scope.tagData = new Array();
					$scope.tagRenameData = [];
					$scope.peopleIdTable = [];
					$scope.checktagIdTable = [];
					$scope.tagDataFilterList = new Array();
					$scope.locationList = [];
					$scope.searchText = "";
					var menudata = [];
					$scope.accessLimit_returnScreen = [];
					$scope.accessLimitReturn = [];
					let selectedZones = 0;

					$scope.searchButton = true;
					$scope.finalallowedZones = [];
					$scope.selectedZoneIdList = [];

					$scope.show_peopleId = true;
					$scope.show_fname = false;
					$scope.show_lname = false;
					$scope.show_phoneNumber = false;
					$scope.show_companyName = false;
					$scope.show_accessLimit = false;
					$scope.show_date = false;
					$scope.show_stime = false;
					$scope.show_etime = false;
					$scope.show_contactPerson = false;
					$scope.varTagIdTextAssigned = '<div style="color:blue">';
					$scope.varTagIdTextFree = '<div style="color:green">';
					$scope.varTagIdTextNoTag = '<div style="color:red">';
					$scope.fetchTagIdTable = [];
					$('.select2').select2(); // searchable dropdown in fetch
												// screen intialization
					$('#checktagIdSelect').select2();

					// input field validation

					$("#id_skuNoAssignTag, #id_skuBatchNoAssignTag, #id_upcCodeAssignTag").keypress(
							function(e) {
								// if the letter is not alphabets then display
								// error
								// and don't type anything
								$return = ((e.which > 64) && (e.which < 91))
										|| ((e.which > 96) && (e.which < 123))
										|| (e.which == 8) || (e.which == 32)
										|| (e.which >= 48 && e.which <= 57)
								if (!$return)

								{

									return false;
								}
							});

					$("#id_skuTotalQtyAssignTag").keypress(
							function(e) {
								// if the letter is not digit then display error
								// and don't type anything

								if (e.which != 8 && e.which != 0
										&& (e.which < 48 || e.which > 57)) {

									return false;
								}
							});

					// to enable/disable sub menus
					$scope.user = JSON.parse($window.sessionStorage
							.getItem("user"));

					if ($scope.user.capabilityDetails != undefined
							&& $scope.user.capabilityDetails != null) {
						if ($scope.user.capabilityDetails.STVA == 1) {
							$scope.showViewAllBtn = true;
						} else {
							$scope.showViewAllBtn = false;
						}
						if ($scope.user.capabilityDetails.STAR == 1) {
							$scope.showAssignReturnBtn = true;
						} else {
							$scope.showAssignReturnBtn = false;
						}
						if ($scope.user.capabilityDetails.STED == 1) {
							$scope.showEditBtn = true;
						} else {
							$scope.showEditBtn = false;
						}
						

					}
					$rootScope.checkNetconnection = function() {

						return $window.navigator.onLine;
					}
					$scope.searchOption = function() {
						$scope.filterOption();
					}

					$scope.filterOption = function() {

						$scope.tagViewData = [];
						$scope.tagDataFilterList = [];
 
						for (var i = 0; i < $scope.tagData.length; i++) {

							if ($scope.searchText == '') {
								$scope.tagDataFilterList
										.push($scope.tagData[i]);
							} else {
								if (($scope.tagData[i].tagId != undefined
										&& $scope.tagData[i].tagId != null ? $scope.tagData[i].tagId
										: "").toLowerCase().includes(
										$scope.searchText.toLowerCase())
										/*|| ($scope.tagData[i].groupId != undefined
												&& $scope.tagData[i].groupId != null ? $scope.tagData[i].groupId
												: "").toLowerCase()
												.includes(
														$scope.searchText
																.toLowerCase())*/
										|| ($scope.tagData[i].macId != undefined
												&& $scope.tagData[i].macId != null ? $scope.tagData[i].macId
												: "").toLowerCase()
												.includes(
														$scope.searchText
																.toLowerCase())
										/*|| ($scope.tagData[i].manufacturer != undefined
												&& $scope.tagData[i].manufacturer != null ? $scope.tagData[i].manufacturer
												: "").toLowerCase()
												.includes(
														$scope.searchText
																.toLowerCase())*/
										|| ($scope.tagData[i].status != undefined
												&& $scope.tagData[i].status != null ? $scope.tagData[i].status
												: "").toLowerCase()

												.includes(
														$scope.searchText
																.toLowerCase())
										|| ($scope.tagData[i].assignmentId != undefined
												&& $scope.tagData[i].assignmentId != null ? $scope.tagData[i].assignmentId
												: "").toLowerCase()
												.includes(
														$scope.searchText
																.toLowerCase())
										|| ($scope.tagData[i].type != undefined
												&& $scope.tagData[i].type != null ? $scope.tagData[i].type
												: "").toLowerCase()
												.includes(
														$scope.searchText
																.toLowerCase())
										|| ($scope.tagData[i].assignmentDate != undefined
												&& $scope.tagData[i].assignmentDate != null ? $scope.tagData[i].assignmentDate
												: "").toLowerCase()
												.includes(
														$scope.searchText
																.toLowerCase())
										/*|| ($scope.tagData[i].createdDate != undefined
												&& $scope.tagData[i].createdDate != null ? $scope.tagData[i].createdDate
												: "").toLowerCase()
												.includes(
														$scope.searchText
																.toLowerCase())*/
										|| ($scope.tagData[i].modifiedDate != undefined
												&& $scope.tagData[i].modifiedDate != null ? $scope.tagData[i].modifiedDate
												: "").toLowerCase()
												.includes(
														$scope.searchText
																.toLowerCase())) {
									$scope.tagDataFilterList
											.push($scope.tagData[i]);

								}

							}

						}

						$scope.totalItems = $scope.tagDataFilterList.length;
						$scope.currentPage = 1;
						$scope.getTotalPages();
						//$scope.getAssignedTagIds($scope.tagDataFilterList);
						
						if ($scope.tagData.length == 0
								|| $scope.tagDataFilterList.length == 0) {
							$scope.tagNoData = true;

						} else {

							$scope.tagNoData = false;

							if ($scope.tagDataFilterList.length > $scope.numPerPage) {
								$scope.tagViewData = $scope.tagDataFilterList
										.slice(0, $scope.numPerPage);
								$scope.PaginationTab = true;
							} else {
								$scope.tagViewData = $scope.tagDataFilterList;
								$scope.PaginationTab = false;
							}

						}
					}// kumar 02/Jul filter option <-

					$scope.editSKUTag = function() {
						$scope.tagSKUEditScreen = false;
						$scope.tagSKUEditUpdateScreen = true;
						$scope.searchButton = false;
						if ($scope.authen.status == "Assigned"
								|| $scope.authen.status == "Blocked"
								|| $scope.authen.status == "Overdue")
							$scope.editStatusAssignmentType = true;
						else if ($scope.authen.status == "Free"
								|| $scope.authen.status == "NotWorking"
								|| $scope.authen.status == "Expired")
							$scope.editStatusAssignmentType = false;
					}

					$scope.skuLSLMChange = function() {
						if ($scope.skuLSLM == "Yes") {
							$scope.authen.tagType = "LSLM";
							$scope.showSKUExpiryDate = true;
						} else {
							$scope.authen.tagType = "Inventory";
							$scope.showSKUExpiryDate = false;
						}
					}
					$scope.assignTagType = function() {
						if ($scope.authen.tagType == "Employee") {
							$scope.show_peopleId = true;
							$scope.show_fname = false;
							$scope.show_lname = false;
							$scope.show_phoneNumber = false;
							$scope.show_companyName = false;
							$scope.show_accessLimit = false;
							$scope.show_date = false;
							$scope.show_stime = false;
							$scope.show_etime = false;
							$scope.show_contactPerson = false;
							$scope.authen.peopleId = $scope.peopleIdTable[0];

						} else if ($scope.authen.tagType == "visitor"
								|| $scope.authen.tagType == "Vendor"
								|| $scope.authen.tagType == "Contractor") {
							$scope.show_peopleId = false;
							$scope.show_fname = true;
							$scope.show_lname = true;
							$scope.show_phoneNumber = true;
							$scope.show_companyName = true;
							$scope.show_accessLimit = true;
							$scope.show_date = true;
							$scope.show_stime = true;
							$scope.show_etime = true;
							$scope.show_contactPerson = true;
							$scope.authen.countryCode = "+1";
							$scope.authen.contactPersonCountryCode = "+1";
							$scope.dateTimeReset();

							$scope.authen.firstName = "";
							$scope.authen.lastName = "";
							$scope.authen.companyName = "";
							$scope.authen.contactPerson = "";

							// $scope.treeViewZoneList =[];
							$scope.fnCreateTreeWithZoneNme();
							$scope.authen.countryCode = "+1";
							$scope.authen.phoneNumber = "";
							$scope.authen.contactPersonCountryCode = "+1";
							$scope.authen.contactPersonPhoneNumber = "";

						}
					}

					var reA = /[^a-zA-Z]/g;
					var reN = /[^0-9]/g;

					function sortAlphaNum(a1, b1) {
						var a = a1.tagId;
						var b = b1.tagId;
						var aA = a.replace(reA, "");
						var bA = b.replace(reA, "");
						if (aA === bA) {
							var aN = parseInt(a.replace(reN, ""), 10);
							var bN = parseInt(b.replace(reN, ""), 10);
							return aN === bN ? 0 : aN > bN ? 1 : -1;
						} else {
							return aA > bA ? 1 : -1;
						}
					}
					$scope.getTagData = function() {
						if (!$rootScope.checkNetconnection()) { 
							$rootScope.alertDialogChecknet();
							return; 
							}
						$rootScope.showloading('#loadingBar');
						$http({
							method : 'GET',
							url : './tag',

						})
								.then(
										function(response) {
											$rootScope
													.hideloading('#loadingBar');
											if (response != null
													&& response.data != null
													&& response.data.length != 0
													&& response.data != "BAD_REQUEST"
													&& response.data != undefined
													&& response.data.length != 0) {

												$scope.tagData.length = 0;
												response.data
														.sort(sortAlphaNum);
												for (var i = 0; i < response.data.length; i++) {
													var fromTime = "", toTime = "",createdDate="",modifiedDate="", assignmentDate = "";

													if (response.data[i].fromTime != null
															&& response.data[i].fromTime != ""
															&& response.data[i].fromTime != undefined) {
														fromTime = response.data[i].fromTime;
													}
													if (response.data[i].toTime != null
															&& response.data[i].toTime != ""
															&& response.data[i].toTime != undefined) {
														toTime = response.data[i].toTime;
													}
													if (response.data[i].createdDate != null
															&& response.data[i].createdDate != ""
															&& response.data[i].createdDate != undefined) {
														response.data[i].createdDate = response.data[i].createdDate
																.substring(
																		0,
																		response.data[i].createdDate.length - 9);
														 createdDate = moment(
																response.data[i].createdDate,"YYYY-MMM-DD")
																.format(
																		"MMM DD, YYYY");
													}
													if (response.data[i].modifiedDate != null
															&& response.data[i].modifiedDate != ""
															&& response.data[i].modifiedDate != undefined) {
														response.data[i].modifiedDate = response.data[i].modifiedDate
																.substring(
																		0,
																		response.data[i].modifiedDate.length - 9);
														 modifiedDate = moment(
																response.data[i].modifiedDate,"YYYY-MMM-DD")
																.format(
																		"MMM DD, YYYY");
													}
													if (response.data[i].assignmentDate != null
															&& response.data[i].assignmentDate != ""
															&& response.data[i].assignmentDate != undefined) {
														response.data[i].assignmentDate = response.data[i].assignmentDate
																.substring(
																		0,
																		response.data[i].assignmentDate.length - 9);
														assignmentDate = moment(
																response.data[i].assignmentDate,"YYYY-MMM-DD")
																.format(
																		"MMM DD, YYYY");
													}
													var tagType = response.data[i].type
													if (response.data[i].type == "Asset Chrono"
															|| response.data[i].type == "Asset NonChrono"
															|| response.data[i].type == "Materials")
														tagType = response.data[i].subType

													var skuTagType = response.data[i].type;
													var tagStatus = response.data[i].status;
													if (skuTagType == "LSLM"
															|| skuTagType == "Inventory"
															|| tagStatus == "Free"
															|| (tagStatus == "Lost" && (skuTagType == "LSLM" || skuTagType == "Inventory"))) {
														$scope.tagData
																.push({
																	'tagId' : response.data[i].tagId,
																	'groupId' : response.data[i].groupId,
																	'macId' : response.data[i].macId,
																	'manufacturer' : response.data[i].manufacturer,
																	'status' : response.data[i].status,
																	'assignmentId' : response.data[i].assignmentId,
																	'assignmentDate' : assignmentDate,
																	'fromTime' : fromTime,
																	'toTime' : toTime,
																	'createdDate' : createdDate,
																	'modifiedDate' : modifiedDate,
																	'major' : response.data[i].major,
																	'minor' : response.data[i].minor,
																	'type' : tagType,
																	'restart' : response.data[i].restart
																});
													}
												}
												/*
												 * $scope.tagRenameData= [];
												 * for(var j=0; j<
												 * response.data.length; j++){
												 * if(response.data[j].renamed ==
												 * 0){
												 * $scope.tagRenameData.push(response.data[j].macId); } }
												 * if
												 * ($scope.tagRenameData.length ==
												 * 0)
												 * $scope.tagRenameData.push("No
												 * Tags Found");
												 */

												// $scope.checktagIdTable.length
												// = 0;
												$scope.fetchTagIdTable = [];
												for (var k = 0; k < response.data.length; k++) {
													var varTagIdRec = {};
													if (response.data[k].tagId != undefined
															&& response.data[k].tagId != ''
															&& response.data[k].tagId != null) {
														if (response.data[k].type != undefined
																&& response.data[k].type != ''
																&& response.data[k].type != null) {
															if (response.data[k].type == "LSLM"
																	|| response.data[k].type == "Inventory") {
																varTagIdRec['id'] = response.data[k].tagId
																varTagIdRec['text'] = $scope.varTagIdTextAssigned
																		+ response.data[k].tagId
																		+ '<div>';
																$scope.fetchTagIdTable
																		.push(varTagIdRec);
															}
														}
														if (response.data[k].status != undefined
																&& response.data[k].status != ''
																&& response.data[k].status != null) {
															if (response.data[k].status == "Free") {

																// $scope.checktagIdTable.push(response.data[k].tagId);
																varTagIdRec['id'] = response.data[k].tagId
																varTagIdRec['text'] = $scope.varTagIdTextFree
																		+ response.data[k].tagId
																		+ '<div>';
																$scope.fetchTagIdTable
																		.push(varTagIdRec);
															}

														}
													}

												}
												/*
												 * if
												 * ($scope.checktagIdTable.length ==
												 * 0)
												 * $scope.checktagIdTable.push("No
												 * Tags Found ");
												 */
												if ($scope.fetchTagIdTable.length == 0) {
													varTagIdRec['id'] = "NoTag";
													varTagIdRec['text'] = $scope.varTagIdTextFree
															+ 'No Tags Found'
															+ '<div>';

												}
												$("#checktagIdSelect").empty();
												$('#checktagIdSelect').select2(
														'destroy');
												$('#checktagIdSelect')
														.select2(
																{
																	data : $scope.fetchTagIdTable,
																	templateResult : function(
																			d) {
																		// $scope.fieldName.checktagId=$(d.text);
																		return $(d.text);
																	},
																	templateSelection : function(
																			d) {
																		return $(d.text);
																	},
																});
												$('#checktagIdSelect').trigger(
														'change.select2');

												// $scope.fieldName.checktagId=$scope.checktagIdTable[0];
												// ----
												if ($scope.tagData.length == 0) {
													$scope.tagNoData = true;
													return;
												} else {
													$scope.tagNoData = false;
												}

												if ($scope.tagData.length > 10) {
													$scope.PaginationTab = true;
												} else {
													$scope.PaginationTab = false;
												}
												$scope.totalItems = $scope.tagData.length;

												$scope.getTotalPages();
												$scope.filterOption();
												//$scope.getAssignedTagIds($scope.tagData);

												$scope.currentPage = 1;
												if ($scope.tagData.length > $scope.numPerPage) {
													$scope.tagViewData = $scope.tagData
															.slice(
																	0,
																	$scope.numPerPage);
													$scope.hasPrevious = true;
													$scope.isFirstPage = true;
													$scope.hasNext = false;
													$scope.isLastPage = false;
												} else {
													$scope.tagViewData = $scope.tagData;
												}
											} else {
												$scope.tagNoData = true;
											}
										},
										function error(response) {
											$rootScope.hideloading('#loadingBar');
											$rootScope.fnHttpError(response);
										});

					}

					// $scope.getTagData();

					// assign Tag

					$scope.assignSKUTag = function() {

						var varupcCode = document
						.getElementById("id_upcCodeAssignTag");
						if (varupcCode.value.length == 0) {

							$scope.upcCodeError = true;
							$scope.upcCode_error_msg = "Please Enter UPC Code";
							$timeout(function() {
								$scope.upcCodeError = false;
							}, 2000);
							return;
						}
						var varSKUNo = document.getElementById("id_skuNoAssignTag");
						if (varSKUNo.value.length == 0) {
							$scope.skuNoError = true;
							$scope.skuNo_error_msg = "Please Enter SKU Number";
							$timeout(function() {
								$scope.skuNoError = false;
							}, 2000);
							return;
						}
						var varSKUBatchNo = document
								.getElementById("id_skuBatchNoAssignTag");
						/*if (varSKUBatchNo.value.length == 0) {

							$scope.showSKUBatchNoError = true;
							$scope.skuBatchNo_Error_error_msg = "Please Enter Batch No";
							$timeout(function() {
								$scope.showSKUBatchNoError = false;
							}, 2000);
							return;
						}*/
						var varSKUDesc = document
								.getElementById("id_skuDescriptionAssignTag");
						if (varSKUDesc.value.length == 0) {

							$scope.showSKUDescError = true;
							$scope.skuDesc_Error_error_msg = "Please Enter SKU Details";
							$timeout(function() {
								$scope.showSKUDescError = false;
							}, 2000);
							return;
						}

						var varSKUTotalQty = document
								.getElementById("id_skuTotalQtyAssignTag");
						if (varSKUTotalQty.value.length == 0) {

							$scope.showSKUTotalQtyError = true;
							$scope.skuTotalQty_Error_error_msg = "Please Enter Total Quantity";
							$timeout(function() {
								$scope.showSKUTotalQtyError = false;
							}, 2000);
							return;
						}
						var isSKULSLM = false;
						if ($scope.skuLSLM == "Yes")
							isSKULSLM = true;
						if (isSKULSLM) {
							var varSKUExpiryDate = document
									.getElementById("id_skuExpiryDateInput");
							if (varSKUExpiryDate.value.length == 0) {

								$scope.showSKUExpiryDateError = true;
								$scope.skuExpiryDate_Error_error_msg = "Please Enter Expiry Date";
								$timeout(function() {
									$scope.showSKUExpiryDateError = false;
								}, 2000);
								return;
							}
						}
						var isSKUTransaction = false;
						if ($scope.skuTransaction == "Yes")
							isSKUTransaction = true;
						
						if (isSKULSLM) {
							var assignTagParameter = {
								"tagId" : $scope.authen.tagId,
								"upcCode" : $scope.upcCode,
								"skuNo" : $scope.skuNo,
								"batchNo" : $scope.skuBatchNo,
								"description" : $scope.skuDescription,
								"transaction" : isSKUTransaction,
								"quantity" : $scope.skuTotalQuantity,
								"isLSLM" : isSKULSLM,
								"expiryDate" : $scope.skuExpiryDate
							};
						} else {
							var assignTagParameter = {
								"tagId" : $scope.authen.tagId,
								"upcCode" : $scope.upcCode,
								"skuNo" : $scope.skuNo,
								"batchNo" : $scope.skuBatchNo,
								"description" : $scope.skuDescription,
								"transaction" : isSKUTransaction,
								"quantity" : $scope.skuTotalQuantity,
								"isLSLM" : isSKULSLM
							}
						}
						if (!$rootScope.checkNetconnection()) { 
							$rootScope.alertDialogChecknet();
							return; 
							}
						$rootScope.showloading('#loadingBar');
						$scope.promise = $http
								.post('./skuAllocation', assignTagParameter)
								.then(
										function(result) {
											$rootScope
													.hideloading('#loadingBar');
											if (result.data.status == "success") {
												$rootScope.alertDialog(result.data.message);
												for (var i = 0; i < $scope.fetchTagIdTable.length; i++) {
													// look for the entry with a
													// matching `code` value
													if ($scope.fetchTagIdTable[i].id == $scope.authen.tagId) {
														$scope.fetchTagIdTable[i].text = $scope.varTagIdTextAssigned
																+ $scope.authen.tagId
																+ '<div>';
														break;
													}
												}
												$scope.cancelSKUTagAssign();

											} else if (result.data.status == "error") {
												$rootScope.alertDialog(result.data.message);
											}

										},
										function(error) {
											$rootScope.hideloading('#loadingBar');
											$rootScope.fnHttpError(error);
										});
					}

					
					
					
					
					

					$scope.rowHighilited = function(row) {
						$scope.selectedRow = row; 
						
						var a = new Array();
						var b = new Array();
						// $scope.authen = $scope.tagData[row];
						$scope.authen = JSON.parse(JSON
								.stringify($scope.tagViewData[row]));
						$scope.statusEdit = true; //kumar 21-Jan-21 Enable Edit button only when it is true
						if ($scope.authen.status == "Free") {

							$scope.names = [ "Free", "Lost", "Not Working",
									"Expired" ];
						} else if ($scope.authen.status == "Expired") {
							// $scope.showDetails = true;
							$scope.statusEdit = false;
							$scope.names = [ "Expired" ];
							// $scope.names = [ "Expired", "Lost", "Not
							// Working", "Blocked" ];
						} else if ($scope.authen.status == "Overdue") {

							$scope.showDetails = true;

							$scope.names = [ "Overdue", "Lost", "Not Working",
									"Blocked" ];
						} else if ($scope.authen.status == "Blocked") {

							$scope.showDetails = true;
							/*
							 * $scope.names=[]; $scope.names[0] =
							 * $scope.authen.status;
							 */
							$scope.names = [ "Blocked", "Lost", "Not Working",
									"Assigned" ];

						} else if ($scope.authen.status == "Lost"
								&& $scope.authen.restart == true) {
							// $scope.statusEdit = true;
							$scope.showDetails = true;
							$scope.names = [ "Lost", "Free" ];
						} else if ($scope.authen.status == "Lost") {
							$scope.statusEdit = false;
							$scope.names = [ "Lost" ];
						} else if ($scope.authen.status == "Not Working") {
							$scope.statusEdit = false;
							$scope.names = [ "Not Working" ];
						} else if ($scope.authen.status == "Assigned") {

							if ($scope.authen.type == 'Visitor'
									|| $scope.authen.type == 'Contractor'
									|| $scope.authen.type == 'Vendor') {
								if ($scope.authen.assignmentDate != undefined
										&& $scope.authen.assignmentDate != ''
										&& $scope.authen.assignmentDate != null) {
									var tagAssignmentDate = moment($scope.authen.assignmentDate);
									var currentDay = moment(new Date()).format(
											'MMM DD, YYYY');
									var diff = moment(currentDay)
											- tagAssignmentDate;
									if (diff == 0) { // today
										var today = new Date();
										var currentTime = ("0" + today
												.getHours()).slice(-2)
												+ ":"
												+ ("0" + today.getMinutes())
														.slice(-2);
										var toTime = $scope.authen.toTime
												.substring(
														0,
														$scope.authen.toTime.length - 3);
										var fromTime = $scope.authen.fromTime;
										a = toTime.split(':');
										b = currentTime.split(':');
										var toTimeMinutes = (+a[0]) * 60
												+ (+a[1]);
										var currentTimeMinutes = (+b[0]) * 60
												+ (+b[1]);

										if (currentTimeMinutes > toTimeMinutes)

											$scope.names = [ "Assigned",
													"Lost", "Not Working",
													"Blocked", "Expired",
													"Overdue" ];
										else
											$scope.names = [ "Assigned",
													"Lost", "Not Working",
													"Blocked", "Expired" ];
									} else if (diff > 0) { // assigned date is
															// previous date
										$scope.names = [ "Assigned", "Lost",
												"Not Working", "Blocked",
												"Expired", "Overdue" ];
									} else {
										$scope.names = [ "Assigned", "Lost",
												"Not Working", "Blocked",
												"Expired" ];
									}
								}
							} else {
								$scope.names = [ "Assigned", "Lost",
										"Not Working", "Expired" ];
							}


						}

					}

					$scope.updateTag = function() {
						
						if ($scope.names[0] == $scope.authen.status) {
							$rootScope.showAlertDialog("Please select other status");
							return;
						}
						
						var updateTagParameter = {
							"tagId" : $scope.authen.tagId,
							"status" : $scope.authen.status,
                            "type" : 'tagSKU'
						};
						if (!$rootScope.checkNetconnection()) { 
							$rootScope.alertDialogChecknet();
							return; 
							}
						$rootScope.showloading('#loadingBar');
						$scope.promise = $http
								.post('./updateTagStatus', updateTagParameter)
								.then(
										function(result) {
											$rootScope
													.hideloading('#loadingBar');
											if ($scope.authen.status == "Lost"){ //kumar 21-Jan-21
												$scope.authen.restart = false;
											}
											else if ($scope.authen.status == "Free"){ //kumar 21-Jan-21
												$scope.authen.restart = true;
												$scope.authen.type = "";
												$scope.authen.assignmentId ="";
												$scope.authen.assignmentDate="";
												$scope.authen.modifiedDate=moment(new Date()).format('MMM DD, YYYY');
											}
											$scope.tagViewData[$scope.selectedRow] = $scope.authen;
											if (result.data.status == "success") {
												$rootScope.showAlertDialog(result.data.message);    
												$scope.cancelTag();

											} else if (result.data.status == "error") {
												$rootScope.showAlertDialog(result.data.message);    
											}

										},
										function(error) {
											$rootScope.hideloading('#loadingBar');
											$rootScope.fnHttpError(error);
										});
					}
					$scope.cancelTag = function() {

						$scope.authen = {};
						$scope.selectedRow = -1;

						$scope.tagSKUEditScreen = true;
						$scope.tagSKUEditUpdateScreen = false;
						$scope.searchButton = true;
					}
					
					$scope.cancelEdit = function() {

						$scope.selectedRow = -1;
						
						$scope.searchButton = true; 
					}

					

					$scope.cancelReturnSKUTag = function() {
						// $('#checktagIdSelect').val("");
						$scope.tagSKUReturnScreen = false;
						$scope.fetchScreen = true;
						$("#checktagIdSelect").empty();
						$('#checktagIdSelect').select2('destroy');
						$('#checktagIdSelect').select2({
							data : $scope.fetchTagIdTable,
							templateResult : function(d) {
								// $scope.fieldName.checktagId=$(d.text);
								return $(d.text);
							},
							templateSelection : function(d) {
								return $(d.text);
							},
						});
						$('#checktagIdSelect').trigger('change.select2');
						// $scope.fieldName.checktagId=$scope.checktagIdTable[0];
					}
					$scope.returnSKUTag = function() {
						var returnTagParameter = {
							"tagId" : $scope.tagReturnTagID,
							"upcCode" : $scope.tagReturnUPCCode
						};
						if (!$rootScope.checkNetconnection()) { 
							$rootScope.alertDialogChecknet();
							return; 
							}
						$rootScope.showloading('#loadingBar');
						$scope.promise = $http
								.post('./returnSKUAsset', returnTagParameter)
								.then(
										function(result) {
											$rootScope
													.hideloading('#loadingBar');
											if (result.data.status == "success") {
												$rootScope.alertDialog(result.data.message);
												for (var i = 0; i < $scope.fetchTagIdTable.length; i++) {
													// look for the entry with a
													// matching `code` value
													if ($scope.fetchTagIdTable[i].id == $scope.tagReturnTagID) {
														$scope.fetchTagIdTable[i].text = $scope.varTagIdTextFree
																+ $scope.tagReturnTagID
																+ '<div>';
														break;
													}
												}
												$scope.cancelReturnSKUTag();
											} else if (result.data.status == "error") {
												$rootScope.alertDialog(result.data.message);
											}

										},
										function(error) {
											$rootScope.hideloading('#loadingBar');
											$rootScope.fnHttpError(error);
										});

					}

					$scope.fetchTag = function() {
						
						/*
						 * var fetchTag =
						 * document.getElementById("checktagIdSelect"); if
						 * (fetchTag.value.length == 0) {
						 * $rootScope.hideloading('#loadingBar');
						 * $scope.fetchTagError = true;
						 * $scope.fetchTag_error_msg = "Please enter Tag ID ";
						 * $timeout(function() { $scope.fetchTagError = false;
						 *  }, 2000);
						 * 
						 * return; }
						 */
						var varCheckTagID = $('#checktagIdSelect').val();
						if (varCheckTagID==undefined || varCheckTagID==null || varCheckTagID=='' ) {
							$rootScope.alertErrorDialog('Please Enter Tag ID ');
							return;
						}
						
						if (varCheckTagID == "NoTag") {
							$rootScope
									.alertErrorDialog('Tags are not available');
							return;
						}

						var checkTagParameter = {
							"tagId" : varCheckTagID
						};
						if (!$rootScope.checkNetconnection()) { 
							$rootScope.alertDialogChecknet();
							return; 
							}
						$rootScope.showloading('#loadingBar');
						$scope.promise = $http
								.post('./checkTag', checkTagParameter)
								.then(
										function(result) {

											$rootScope
													.hideloading('#loadingBar');
											if (result.data.status == "success") {
												if (result.data.tagStatus == "free") {
													$scope.checkTagMacId = result.data.tag.macId;
													var tagId = result.data.tag.tagId;
													$("#id_tagIdAssignTag")
															.val(tagId);
													$scope.assignScreen = true;
													$scope.fetchScreen = false;
													$scope.tagSKUReturnScreen = false;
													$scope.authen.tagId = tagId;
													$scope.authen.tagType = "Inventory";
													$scope.skuNo = "";
													$scope.batchNo = "";
													$scope.skuDescription = "";
													$scope.skuTransaction = "No";
													$scope.skuLSLM = "No";
													$scope.skuTotalQuantity = "";
													$('#id_skuExpiryDate')
															.datetimepicker(
																	{
																		format : 'YYYY-MMM-DD',
																		minDate : new Date(),
																	});
													var today = moment(new Date()).format('YYYY-MMM-DD')
													$scope.skuExpiryDate = today;
													$scope.showSKUExpiryDate = false;
													// $scope.assignTagType();
												} else if ((result.data.tagStatus == "Assigned")
														|| (result.data.tagStatus == "assigned")) {
													if ((result.data.tag.type == "Inventory")
															|| (result.data.tag.type == "LSLM")) {
														var varSKUIssueDate = "", varSKUExpiryDate = "";
														$scope.showReturnTagSKUExpiryDate = false;
														$scope.assignScreen = false;
														$scope.fetchScreen = false;
														$scope.tagReturnTagID = result.data.tag.tagId;
														$scope.tagSKUReturnScreen = true;
														if (result.data.skuAllocationDetails.issueDate != undefined
																&& result.data.skuAllocationDetails.issueDate != null)
															varSKUIssueDate = moment(
																	result.data.skuAllocationDetails.issueDate, "YYYY-MM-DD")
																	.format(
																			"MMM DD, YYYY");
														if (result.data.skuAllocationDetails.expiryDate != undefined
																&& result.data.skuAllocationDetails.expiryDate != null) {
															varSKUExpiryDate = moment(
																	result.data.skuAllocationDetails.expiryDate, "YYYY-MM-DD")
																	.format(
																			"MMM DD, YYYY");
															$scope.showReturnTagSKUExpiryDate = true;
															// $scope.showSKUExpiryDate
															// = true;
														}
														$scope.tagReturnUPCCode = result.data.skuAllocationDetails.upcCode;
														$scope.tagReturnSKUNo = result.data.skuAllocationDetails.skuNo;
														$scope.tagReturnBatchNo = result.data.skuAllocationDetails.batchNo;
														$scope.tagReturnSKUDescription = result.data.skuAllocationDetails.description;
														if (result.data.skuAllocationDetails.transaction)
															$scope.tagReturnSKUTransaction = "Yes";
														else
															$scope.tagReturnSKUTransaction = "No";
														$scope.tagReturnSKUIssueDate = varSKUIssueDate;
														$scope.tagReturnSKUTotalQty = result.data.skuAllocationDetails.quantity;
														$scope.tagReturnSKUAvailableQty = result.data.skuAllocationDetails.availableQuantity;
														if (result.data.skuAllocationDetails.lslm) {
															$scope.tagReturnSKULSLM = "Yes";
															$scope.showReturnTagSKUExpiryDate = true;
														}

														else {
															$scope.tagReturnSKULSLM = "No";
															$scope.showReturnTagSKUExpiryDate = false;
														}

														$scope.tagReturnSKUExpiryDate = varSKUExpiryDate;
													} else {
														$rootScope.alertDialog(result.data.message);
													}

												} else {
													$rootScope.alertDialog(result.data.message);
												}

											} else if (result.data.status == "error") {
												$rootScope.alertDialog(result.data.message);
											}

										},
										function(error) {
											$rootScope.hideloading('#loadingBar');
											$rootScope.fnHttpError(error);
										});

					}

					// pagination button
					$scope.currentPage = 1;

					$scope.currentPage = 1;
					$scope.numPerPage = 10;
					$scope.maxSize = 3;
					$scope.totalItems = 0;
					$scope.tagDataFilterList = [];
					$scope.tagViewData = [];
					$scope
							.$watch(
									'currentPage + numPerPage',
									function() {
										var begin = (($scope.currentPage - 1) * $scope.numPerPage), end = begin
												+ $scope.numPerPage;
										if ($scope.searchText == '') // kumar
											// 02/July
											// for
											// filter
											// text
											$scope.tagViewData = $scope.tagData
													.slice(begin, end);
										else
											$scope.tagViewData = $scope.tagDataFilterList
													.slice(begin, end);
										$scope.enablePageButtons();
									});

					$scope.currentPage = 1;

					$scope.maxSize = 3;

					$scope.numPerPage = 10;
					$scope.totalItems = 0;
					$scope.totalPages = 0;

					$scope.hasNext = true;
					$scope.hasPrevious = true;
					$scope.isFirstPage = true;
					$scope.isLastPage = true;
					$scope.serial = 1;

					$scope
							.$watch(
									"currentPage",
									function(newVal, oldVal) {
										if (newVal !== oldVal) {

											if (newVal > $scope.totalPages) {
												$scope.currentPage = $scope.totalPages;
											} else {
												$scope.currentPage = newVal;
											}

											$scope.serial = (($scope.currentPage - 1) * $scope.numPerPage) + 1;
											$scope.enablePageButtons();
										}
									});

					$scope.enablePageButtons = function() {
						$scope.serial = (($scope.currentPage - 1) * $scope.numPerPage) + 1;
						$scope.selectedRow = -1;
						if ($scope.totalPages <= 1) {
							$scope.hasPrevious = true;
							$scope.isFirstPage = true;
							$scope.hasNext = true;
							$scope.isLastPage = true;

						} else if ($scope.totalPages > 1
								&& $scope.currentPage == $scope.totalPages) {

							$scope.hasPrevious = false;
							$scope.isFirstPage = false;
							$scope.hasNext = true;
							$scope.isLastPage = true;
						} else if ($scope.currentPage != 1
								&& $scope.currentPage < $scope.totalPages) {

							$scope.hasPrevious = false;
							$scope.isFirstPage = false;
							$scope.hasNext = false;
							$scope.isLastPage = false;

						} else {
							$scope.hasPrevious = true;
							$scope.isFirstPage = true;
							$scope.hasNext = false;
							$scope.isLastPage = false;
						}
						 
					};

					$scope.getTotalPages = function() {

						if (($scope.totalItems % $scope.numPerPage) > 0)
							$scope.totalPages = parseInt($scope.totalItems
									/ $scope.numPerPage) + 1;
						else
							$scope.totalPages = parseInt($scope.totalItems
									/ $scope.numPerPage);

						$scope.enablePageButtons();

					};

					$scope.goFirstPage = function() {
						$scope.currentPage = 1;
						$scope.enablePageButtons();
					};

					$scope.goPreviousPage = function() {
						if ($scope.currentPage > 1)
							$scope.currentPage = $scope.currentPage - 1;
						$scope.enablePageButtons();
					};

					$scope.goNextPage = function() {
						if ($scope.currentPage <= $scope.totalPages)
							$scope.currentPage = $scope.currentPage + 1;
						$scope.enablePageButtons();
					};

					$scope.goLastPage = function() {
						$scope.currentPage = $scope.totalPages;
						$scope.enablePageButtons();
					};

					
					$scope.displayTagSKUAssignScreen = function() {
						// $("#checktagIdSelect").val("");
						// $scope.fieldName.checktagId=$scope.checktagIdTable[0];
						//$scope.tagPeopleViewScreen = false;
						$scope.tagSKUEditScreen = false;
						$scope.tagSKUEditUpdateScreen = false;
						//$scope.tagPeopleRenameScreen = false;
						$scope.fetchScreen = true;
						$scope.assignScreen = false;
						$scope.tagSKUReturnScreen = false;
						$("#idTagSKUAssign").addClass("active");
						$("#idTagSKUEdit").removeClass("active");
						$("#idTagPeopleReturn").removeClass("active");
						$scope.searchButton = false;
						$("#checktagIdSelect").empty();
						$('#checktagIdSelect').select2('destroy');
						$('#checktagIdSelect').select2({
							data : $scope.fetchTagIdTable,
							templateResult : function(d) {
								// $scope.fieldName.checktagId=$(d.text);
								return $(d.text);
							},
							templateSelection : function(d) {
								return $(d.text);
							},
						});
						$('#checktagIdSelect').trigger('change.select2');
						$scope.authen.peopleId = $scope.peopleIdTable[0];
						$scope.fieldName.checktagId = $scope.checktagIdTable[0];
						$scope.getTagData();

					};
					$scope.displayTagSKUEditScreen = function() {
						$scope.tagDataFilterList = [];
						$scope.tagViewData = [];
						$scope.searchText = "";

						$scope.selectedRow = -1;
						$scope.currentPage = 1;
						$scope.searchButton = true;
						//$scope.tagPeopleViewScreen = false;
						$scope.tagSKUEditScreen = true;
						$scope.tagSKUEditUpdateScreen = false;
						//$scope.tagPeopleRenameScreen = false;
						$scope.fetchScreen = false;
						$scope.assignScreen = false;
						$scope.tagSKUReturnScreen = false;
						$("#idTagSKUAssign").removeClass("active");
						$("#idTagSKUEdit").addClass("active");
						$("#idTagPeopleReturn").removeClass("active");
						$scope.getTagData();

					};
					

					$scope.tagAssignBackToView = function() {
						var formID = document.getElementById("assignTagForm")
								.reset();

						$scope.authen = {};
						$scope.selectedRow = -1;
						//$scope.tagPeopleViewScreen = true;
						$scope.searchButton = true;
						$scope.fetchScreen = false;
						$scope.assignScreen = false;
						$scope.tagSKUReturnScreen = false;

					}
					$scope.dateTimeReset = function() {
						//var today = new Date();
						$('#id_skuExpiryDate').datetimepicker({
							format : 'YYYY-MMM-DD',
							maxDate : new Date(),
						});

						var today = moment(new Date()).format('YYYY-MMM-DD')
						$scope.skuExpiryDate = today;
						$("#id_skuExpiryDate").val(today);

					}
					$scope.cancelSKUTagAssign = function() {

						// $('#checktagIdSelect').val("");
						$scope.fetchScreen = true;
						$scope.tagSKUReturnScreen = false;
						$scope.assignScreen = false;
						$scope.checkTagMacId = "";
						$scope.authen.tagType = "Inventory";
						$scope.skuNo = "";
						$scope.skuBatchNo = "";

						$scope.skuDescription = "";
						$scope.skuTransaction = "No";
						$scope.skuLSLM = "No";
						$scope.skuTotalQuantity = "";
						$scope.upcCode  = "";

						$scope.dateTimeReset();
						$("#checktagIdSelect").empty();
						$('#checktagIdSelect').select2('destroy');
						$('#checktagIdSelect').select2({
							data : $scope.fetchTagIdTable,
							templateResult : function(d) {
								// $scope.fieldName.checktagId=$(d.text);
								return $(d.text);
							},
							templateSelection : function(d) {
								return $(d.text);
							},
						});
						$('#checktagIdSelect').trigger('change.select2');

					}

					//to display enabled sub menus
					if ($scope.user.capabilityDetails != undefined
							&& $scope.user.capabilityDetails != null) {
						/*if ($scope.user.capabilityDetails.STVA != 0) {
							$scope.displayTagPeopleViewScreen();
						}*/
						if ($scope.user.capabilityDetails.STAR != 0) {
							$scope.displayTagSKUAssignScreen();
						}
						if ($scope.user.capabilityDetails.STAR != 1) {
							$scope.displayTagSKUEditScreen();
						}
						
					}

				});
