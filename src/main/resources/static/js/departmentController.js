app
	.controller(
		'departmentController',
		function($scope, $http, $rootScope, $window, $location,$timeout) {
			$scope.user = JSON.parse($window.sessionStorage
					.getItem("user"));
			if ($scope.user == null) {
				$location.path('/login');
				return;
			}
			 // Timepicker
  			$scope.bShowAddBtn=true;
			$scope.bShowUpdateBtn=false;
	     	$scope.bShowDeleteBtn=false;
           $scope.deptData= new Array();
            $scope.deptViewData= new Array();
            $scope.deptDataFilterList= new Array();
                      
            $scope.searchText = "";
            $scope.addDept={};
            const MAX_DEPT_RECORDS = 10;

			$scope.getDeptData = function() {
				if (!$rootScope.checkNetconnection()) { 
				 	$rootScope.alertDialogChecknet();
					return; 
					}
									
						 $rootScope.showloading('#loadingBar');
						$http({
							method : 'GET',
							url : './department',
							
						})
								.then(
										function(response) {
											 $rootScope.hideloading('#loadingBar');
											if (response != null
													&& response.data != null
													&& response.data.length != 0
													&& response.data != "BAD_REQUEST"
													&& response.data != undefined
													&& response.data.length != 0) {

											
												$scope.deptData.length = 0;

												for (var i = 0; i < response.data.length; i++) {
													if (response.data[i].active == true) {
														$scope.deptData
																.push(response.data[i]);
													}
												}
												if($scope.deptData.length > 10){
													$scope.dept_tabNavi=true;
												}else{
													$scope.dept_tabNavi=false;
												}
												$scope.totalItems=$scope.deptData.length;
												
												$scope.getTotalPages();
							
									$scope.currentPage=1;
									if ($scope.deptData.length > $scope.numPerPage){
										$scope.deptViewData =$scope.deptData
										.slice(0, $scope.numPerPage);
										$scope.hasPrevious = true;
										$scope.isFirstPage = true;
										$scope.hasNext = false;
										$scope.isLastPage = false;
									}
										else
									$scope.deptViewData = $scope.deptData;
												

											}else {
												$scope.deptNoData = true;
											} 
										},function(error) {
											 $rootScope.hideloading('#loadingBar');
											 $rootScope.fnHttpError(error);
										});
					}
                
				  $scope.getDeptData();
				  
				  $("#id_deptHead").keypress(
							function(e) {
								// if the letter is not alphabets then display error
								// and don't type anything
								$return = ((e.which > 64) && (e.which < 91)) || ((e.which> 96) && (e.which < 123)) ||(e.which == 8) || (e.which == 32)  || (e.which >= 48 && e.which <= 57)
								if (!$return)
													
								{
									
									return false;
								}
							});		
				  $("#id_deptName").keypress(
							function(e) {
								// if the letter is not alphabets then display error
								// and don't type anything
								$return = ((e.which > 64) && (e.which < 91)) || ((e.which> 96) && (e.which < 123)) ||(e.which == 8) || (e.which == 32)  || (e.which >= 35 && e.which <= 57)
								if (!$return)
													
								{
									
									return false;
								}
							});		
			      $scope.addDeptment = function() {
						if ($scope.deptData.length >= MAX_DEPT_RECORDS) {
							$rootScope.showAlertDialog("Maximum records reached");
							return;
						}			    	  
			    	  
						var name = document.getElementById("id_deptName");
						var deptno = document.getElementById("id_deptNo");
						var depthead = document.getElementById("id_deptHead");
						if (name.value.length == 0) {
							$scope.nameError = true;
							$scope.name_error_msg = "Please enter the name";
							$timeout(function() {
								$scope.nameError = false;
							}, 2000);
							return;
						}else if (deptno.value.length == 0) {
							$scope.noError = true;
							$scope.no_error_msg = "Please enter department no";
							$timeout(function() {
								$scope.noError = false;
							}, 2000);
							return;
						}else if (depthead.value.length == 0) {
							$scope.headError = true;
							$scope.head_error_msg = "Please enter the department head";
							$timeout(function() {
								$scope.headError = false;
							}, 2000);
							return;
						}

						if (!$rootScope.checkNetconnection()) { 
						 	$rootScope.alertDialogChecknet();
							return; 
							}
						if ($scope.addDeptForm.$valid) {
                                                     
							$rootScope.showloading('#loadingBar');
			
							$http({
								method : 'POST',
								url : './department',
								data : $scope.addDept,
								
							})
									.then(
											function(response) {
												if (response != null
														&& response.data != null
														&& response.data != "BAD_REQUEST") {
													$rootScope.hideloading('#loadingBar');
													$scope.dept = response.data;
													$rootScope.showAlertDialog(response.data.message);
													$timeout(
															function() {
																 $scope.getDeptData();
																 $scope.cancelDepartment();
															}, 2000);
												} else {
													$rootScope.hideloading('#loadingBar');
													$rootScope.showAlertDialog(response.data.message);
													$timeout(function() {
														$scope.cancelDepartment();
													}, 2000);
												}
											},function(error) {
												$rootScope.hideloading('#loadingBar');
												$rootScope.fnHttpError(error);
											});
						}

				
					}
					
					 
                 $scope.rowHighilited = function(row) {
	
	                    $scope.selectedRow = row;
	
		                $scope.addDept.deptName = $scope.deptData[row].deptName;
						$scope.addDept.deptNumber = $scope.deptData[row].deptNumber;
						$scope.addDept.deptHead = $scope.deptData[row].deptHead;
						
						$scope.deptNm=true;
						$scope.bShowUpdateBtn=true;
						$scope.bShowDeleteBtn=true;
						$scope.bShowAddBtn=false;
						
						}
				 $scope.updateDepartment = function() {		
					 var name = document.getElementById("id_deptName");
						var deptno = document.getElementById("id_deptNo");
						var depthead = document.getElementById("id_deptHead");
						if (name.value.length == 0) {
							$scope.nameError = true;
							$scope.name_error_msg = "Please enter the name";
							$timeout(function() {
								$scope.nameError = false;
							}, 2000);
							return;
						}else if (deptno.value.length == 0) {
							$scope.noError = true;
							$scope.no_error_msg = "Please enter department no";
							$timeout(function() {
								$scope.noError = false;
							}, 2000);
							return;
						}else if (depthead.value.length == 0) {
							$scope.headError = true;
							$scope.head_error_msg = "Please enter the department head";
							$timeout(function() {
								$scope.headError = false;
							}, 2000);
							return;
						}

						if (!$rootScope.checkNetconnection()) { 
						 	$rootScope.alertDialogChecknet();
							return; 
							}
						if ($scope.addDeptForm.$valid) {
							$rootScope.showloading('#loadingBar');
			
							$http({
								method : 'PUT',
								url : './department',
								data : $scope.addDept,
								
							})
									.then(
											function(response) {
												if (response != null
														&& response.data != null
														&& response.data != "BAD_REQUEST") {
													$rootScope.hideloading('#loadingBar');
													$scope.dept = response.data;
													$rootScope.showAlertDialog(response.data.message);
													$timeout(
															function() {
																 $scope.getDeptData();
																 $scope.cancelDepartment();

															}, 2000);
												} else {
													$rootScope.hideloading('#loadingBar');
													$rootScope.showAlertDialog(response.data.message);
													$timeout(function() {
														$scope.cancelDepartment();
													}, 2000);
												}
											},function(error) {
												$rootScope.hideloading('#loadingBar');
												$rootScope.fnHttpError(error);
											});
						}

				
					}
									
				 $scope.deleteDepartment = function() {		
					 var name = document.getElementById("id_deptName");
						
						
						if (name.value.length == 0) {
							$scope.nameError = true;
							$scope.name_error_msg = "Please enter the name";
							$timeout(function() {
								$scope.nameError = false;
							}, 2000);
							return;
						}


						if ($scope.addDeptForm.$valid) {
							$.confirm({
										title : $rootScope.MSGBOX_TITLE,
										content : 'Are you sure you want to delete this item?',
										type: 'blue',
										columnClass:'medium',
										autoClose : false,
                         				icon: 'fa fa-info-circle',
										buttons: {
										yes: {
										text: 'Yes', 
										btnClass: 'btn-blue',
										action : function() {
							
							var deptName = $scope.addDept.deptName
						     data = {
							 "deptName" : deptName 
						    }
							if (!$rootScope.checkNetconnection()) { 
								$rootScope.alertDialogChecknet();
								return; 
								}
							$rootScope.showloading('#loadingBar');
							$http({
								method : 'DELETE',
								url : './department',
								headers: {
								'Content-Type':'application/json'
								},
								data : data,
								
							})
									.then(
											function(response) {
												if (response != null
														&& response.data != null
														&& response.data != "BAD_REQUEST") {
													$rootScope.hideloading('#loadingBar');
													$scope.dept = response.data;
													$rootScope.showAlertDialog(response.data.message);
													$timeout(
															function() {
																 $scope.getDeptData();
														        $scope.cancelDepartment();
															}, 2000);
												} else {
													$rootScope.hideloading('#loadingBar');
													$rootScope.showAlertDialog(response.data.message);
													$timeout(function() {
														$scope.cancelDepartment();
													}, 2000);
												}
											},function(error) {
												$rootScope.hideloading('#loadingBar');
												$rootScope.fnHttpError(error);
											});
											}
											 },
									   no:function () {
								           
										   		}
											}
									});
						}

				
					}
				 $scope.cancelDepartment = function() {	
	                    $scope.addDept={};
                        $scope.deptNm=false;
						$scope.bShowUpdateBtn=false;
						$scope.bShowDeleteBtn=false;
						$scope.bShowAddBtn=true;
						$scope.selectedRow = -1;
					}
				 
				 $scope.searchOption = function() {
						$scope.filterOption();
					}
				 $scope.filterOption = function() {

						$scope.deptViewData = [];
						$scope.deptDataFilterList = [];

						for (var i = 0; i < $scope.deptData.length; i++) {
							// if ($scope.filterValue == "All"
							// || $scope.shiftData[i].status ==
							// $scope.filterValue) {

							if ($scope.searchText == '') {
								$scope.deptDataFilterList
										.push($scope.deptData[i]);
							} else {
								if ($scope.deptData[i].deptName
										.toLowerCase()
										.includes(
												$scope.searchText.toLowerCase())
										|| $scope.deptData[i].deptHead
												.toLowerCase().includes(
														$scope.searchText
																.toLowerCase())
										
										) {
									$scope.deptDataFilterList
											.push($scope.deptData[i]);

								}

							}
							// }
						}

						$scope.totalItems = $scope.deptDataFilterList.length;
						$scope.currentPage = 1;
						$scope.getTotalPages();

						if ($scope.deptData.length == 0
								|| $scope.deptDataFilterList.length == 0) {
							$scope.deptNoData = true;

						} else {
							$scope.deptNoData = false;

							var begin = (($scope.currentPage - 1) * $scope.numPerPage), end = begin
									+ $scope.numPerPage;

							$scope.deptViewData = $scope.deptDataFilterList
									.slice(begin, end);
							/*
							 * if ($scope.shiftDataFilterList.length >
							 * $scope.numPerPage) $scope.shiftViewData =
							 * $scope.shiftDataFilterList .slice(0,
							 * $scope.numPerPage); else $scope.shiftViewData =
							 * $scope.shiftDataFilterList; $scope.apply();
							 */
						}
					}// kumar 02/Jul filter option <-
// pagination button
		
		$scope.currentPage = 1;
		$scope.numPerPage = 10;
		$scope.maxSize = 3;
		$scope.totalItems = 0;
		 $scope.deptViewData=[];
		$scope
				.$watch(
						'currentPage + numPerPage',
						function() {
							var begin = (($scope.currentPage - 1) * $scope.numPerPage), end = begin
									+ $scope.numPerPage;

								$scope.deptViewData = $scope.deptData
										.slice(begin, end);
						

						});

		$scope.currentPage = 1;

		$scope.maxSize = 3;

		$scope.numPerPage = 10;
		$scope.totalItems = 0;
		$scope.totalPages = 0;

		$scope.hasNext = true;
		$scope.hasPrevious = true;
		$scope.isFirstPage = true;
		$scope.isLastPage = true;
		$scope.serial = 1;

		$scope
				.$watch(
						"currentPage",
						function(newVal, oldVal) {
							if (newVal !== oldVal) {

								if (newVal > $scope.totalPages) {
									$scope.currentPage = $scope.totalPages;
								} else {
									$scope.currentPage = newVal;
								}

								$scope.serial = (($scope.currentPage - 1) * $scope.numPerPage) + 1;

							}
						});

		$scope.enablePageButtons = function() {
			$scope.serial = (($scope.currentPage - 1) * $scope.numPerPage) + 1;
			$scope.selectedRow = -1;
			if ($scope.totalPages <= 1) {
				$scope.hasPrevious = true;
				$scope.isFirstPage = true;
				$scope.hasNext = true;
				$scope.isLastPage = true;

			} else if ($scope.totalPages > 1
					&& $scope.currentPage == $scope.totalPages) {

				$scope.hasPrevious = false;
				$scope.isFirstPage = false;
				$scope.hasNext = true;
				$scope.isLastPage = true;
			} else if ($scope.currentPage != 1
					&& $scope.currentPage < $scope.totalPages) {

				$scope.hasPrevious = false;
				$scope.isFirstPage = false;
				$scope.hasNext = false;
				$scope.isLastPage = false;

			} else {
				$scope.hasPrevious = true;
				$scope.isFirstPage = true;
				$scope.hasNext = false;
				$scope.isLastPage = false;
			}
		};

		$scope.getTotalPages = function() {

			if (($scope.totalItems % $scope.numPerPage) > 0)
				$scope.totalPages = parseInt($scope.totalItems
						/ $scope.numPerPage) + 1;
			else
				$scope.totalPages = parseInt($scope.totalItems
						/ $scope.numPerPage);

			$scope.enablePageButtons();

		};

		$scope.goFirstPage = function() {
			$scope.currentPage = 1;
			$scope.enablePageButtons();
		};

		$scope.goPreviousPage = function() {
			if ($scope.currentPage > 1)
				$scope.currentPage = $scope.currentPage - 1;
			$scope.enablePageButtons();
		};

		$scope.goNextPage = function() {
			if ($scope.currentPage <= $scope.totalPages)
				$scope.currentPage = $scope.currentPage + 1;
			$scope.enablePageButtons();
		};

		$scope.goLastPage = function() {
			$scope.currentPage = $scope.totalPages;
			$scope.enablePageButtons();
		};				
										
					
		});
