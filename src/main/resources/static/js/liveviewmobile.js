app
		.controller(
				'liveViewMobileController',
				function($scope, $http, $rootScope, $window, $timeout,$location,$interval, $routeParams ) {

					
					$('#map').css('height',($(window).height()-100));      
 
					
					/*$scope.user = JSON.parse($window.sessionStorage
							.getItem("user"));
					if ($scope.user != null) {
						$rootScope.$broadcast('updatemenu', $scope.user.role);
					if ($scope.user.capabilityDetails.ZOM.ZCL ==1){
					   $("#zomZC").show();
			           }
                    else{
					   $("#zomZC").hide();
					}
					if ($scope.user.capabilityDetails.ZOM.ZAP ==1){
					   $("#zomAP").show();
			           }
                    else{
					   $("#zomAP").hide();
					}
					if ($scope.user.capabilityDetails.ZOM.ZZN ==1){
					   $("#zomZON").show();
			           }
                    else{
					   $("#zomZON").hide();
					}
					} else {
 						 $location.path('/home');
 					}*/
					
 					$scope.org=$routeParams.org
 					/*if($routeParams.serviceprovider =="quuppa"){ 
						$scope.serviceProviderStatus = true; 
						$scope.getQuuppamap();
					} else {
						$scope.serviceProviderStatus = false; 
						$scope.mistLogin();
					}*/
 					
 					
					/* $scope.$on('updateorg', function (event, data) {
						 $scope.org = data; 
						 $rootScope.showloading('#liveview');
 						 $scope.getZoneClassifications(); 
 						 if($scope.user.role =="admin"){ 
							if($window.sessionStorage.getItem("serviceProvider") =="quuppa"){ 
								$scope.serviceProviderStatus = true; 
								$scope.getQuuppamap();
							} else {
								$scope.serviceProviderStatus = false; 
								$scope.mistLogin();
							}
						 } else {
						    if($scope.user.serviceProvider =="quuppa"){  
						    	$scope.serviceProviderStatus = true; 
								$scope.getQuuppamap();
							} else {
								$scope.serviceProviderStatus = false; 
								$scope.mistLogin();
							}
						 }
						 $scope.organizationMenu =true;
						 $("#selectedAssetView").hide();
					     $("#selectedAssetViewheader").hide(); 
					 }); */
					 
					 
					$scope.headingTitle = "Live View";
					$scope.siteId;
					$scope.mapId;
					$scope.orgDetails;
					$scope.region;
					$scope.imageOverlay;
					$scope.editableLayers = null;
					$scope.marker = null;
					$scope.marker1=null;
					$rootScope.hideloading('#liveview');
					$scope.zoneList = [];
					 var mytimeout = ""; 
 					
					var screenWidth = window.screen.width
							* window.devicePixelRatio;
					var screenHeight = window.screen.height
							* window.devicePixelRatio;
					// $scope.mapDetails = '';

					$scope.zoneClassificationList = []; 

					$scope.isDrawEvent = false;
					$scope.searchTag = "";
					$scope.success = false;
					$scope.isZoneDataView = true;
					$scope.isZoneView = true;
					$scope.zoneTable=true;
					$scope.isCheckedShowAp = true;
					$scope.selectedRow = null;
					$scope.type="All";
					$scope.isFilter=false;
					var maxZoomValue = 18;
					var minZoomValue = 0;   
					var defZoomValue = 8;   
					var map = L.map('map', {     
 						editable : true,
						maxZoom : maxZoomValue,
						minZoom : minZoomValue,
						zoom: defZoomValue, 
 					    zoomSnap: 1,
						crs : L.CRS.Simple
					}).setView([ 0, 0 ], 14);  
     
					
					var ZoomViewer = L.Control.extend({
						onAdd: function(){

							var container= L.DomUtil.create('div');
							var gauge = L.DomUtil.create('div');
							container.style.width = '100px';
							container.style.background = 'rgba(255,255,255,0.5)';
							container.style.textAlign = 'center';
							map.on('zoomstart zoom zoomend', function(ev){
								gauge.innerHTML = 'Zoom level: ' + map.getZoom();
							})
							container.appendChild(gauge);

							return container;
						}
					});

					(new ZoomViewer).addTo(map);
					
					// map.setZoom(2);
					window.addEventListener('resize', function(event){ 
					    // get the width of the screen after the resize event
					    var width = document.documentElement.clientWidth;
					    // tablets are between 768 and 922 pixels wide
					    // phones are less than 768 pixels wide
					    if (width < 768) {
					        // set the zoom level to 10
					        map.setZoom(10);
					    }  else {
					        // set the zoom level to 8
					        map.setZoom(8);
					    }
					});
					$scope.getTypes= function(){
								$rootScope.showloading('#loadingBar');
							$http({
								method : 'GET', 
								url : './getPersonColorType',
								headers : {
									'orgName' : $scope.org,
									'sessionid':$routeParams.sessionid
								}
							  })
									.then(
											function success(response) {
												if (response != null
													&& response.data != null
													&& response.data != "BAD_REQUEST") {
														$rootScope.hideloading('#loadingBar');
														$scope.colorList=response.data;	
														 $scope.colorArray=[];
													  $scope.colorArray.push({"type":"All"});

                                                     for (splitdata in $scope.colorList) {
	                                                      $scope.colorArray.push({"type":splitdata});
	                                                       }
 														  $scope.colorArray.push({"type":"Free"})
                                                          $scope.type=$scope.colorArray[0].type;
														}
												else {
												$rootScope.hideloading('#loadingBar');
												$scope.colorError = true;
												$scope.color_error_msg = response.data.message;
												$timeout(function() {
													$scope.colorError = false;
													$scope.color_error_msg="";
												}, 2000);
											}
												},
											function error(response) {
										$rootScope.hideloading('#loadingBar');							
											if (response.status = 403) {
									     $.alert({
			                             title: $rootScope.MSGBOX_TITLE,
                                         content: 'Session timed-out or Invalid. Please logout and login again',
                                         icon: 'fa fa-exclamation-triangle',
                                          });
												}
												
												
							     });
							}
					 
					$scope.getZoneClassifications = function() {
						$http({
							method : 'GET',
							url : './getZoneClassficationMobile',
							headers : {
								'orgName' : $scope.org,
								'mapId' : $scope.mapId,
								'siteId' : $scope.siteId,
								'region':$scope.region
							}
						})
								.then(
										function(response) {
											if (response != null
													&& response.data != null
													&& response.data != "BAD_REQUEST") {
												$scope.zoneClassificationList.length = 0;
												var classifications = response.data.classifications;

												for (var i = 0; i < classifications.length; i++) {
													if (classifications[i].status == "Active") {
														$scope.zoneClassificationList
																.push(classifications[i]);
													}
												}

												$scope.zoneClassificationList.sort(GetSortOrder("classification"));
											} else {
												$scope.noList = true;
											}
										},function(error) {
											$rootScope.hideloading('#loadingBar');
											if(error.status==401) {
												$("#error401").show();
												$("#loadingBar").hide();
 											} 
									}); 
					};
					
					$scope.getQuuppamap = function() {
						
						$scope.mapDetails={};
						// map.closePopup();
						map.eachLayer(function(layer) { 
							if(layer.id!==undefined) {
								layer.remove();  
							}
							if(layer.accessmarkerid!==undefined){
								layer.remove();  
							}
							if(layer.assetid!==undefined) {
								layer.remove();  
							}
 						});
						 if ($scope.imageOverlay!=null && map.hasLayer($scope.imageOverlay))
							 map.removeLayer( $scope.imageOverlay); 
						 

						 if ($scope.marker1!=null && map.hasLayer($scope.marker1))
							 map.removeLayer($scope.marker1); 
						  
						 if ($scope.marker!=null && map.hasLayer($scope.marker))
							 map.removeLayer($scope.marker); 
						 
			  
						  
						 if($scope.grid!=undefined)
							 $scope.grid.onRemove(map);
						 
						 $scope.imageOverlay = null;
						 $scope.marker1 = null;
						 $scope.marker = null;
						 $scope.assetList=[];  
 
						$http({
							method : 'GET',
							url : './getZoneDetails',
							headers : {
								'orgName' : $scope.org,
								'sessionid':$routeParams.sessionid
							}
 						})
								.then(
										function(response) {
											$rootScope.hideloading('#loadingBar');
											if (response != null
													&& response.data != null
													&& response.data != "BAD_REQUEST"
													&& response.data.url != null
													&& response.data.url != "") {
												$scope.tagData = true;
												$scope.tagNoData = false;
												// $scope.mapDetails =
												// response.data;
												$scope.mapImage = response.data.url;
												$scope.ppmx = response.data.ppmx;
												$scope.ppmy = response.data.ppmy;
												
												$scope.editableLayers = new L.FeatureGroup();
												map.addLayer( $scope.editableLayers);
												
												map.setZoom(defZoomValue);
												$scope.mapDetails.url = response.data.url;
												$scope.mapDetails.width = response.data.width/$scope.ppmx;
												$scope.mapDetails.height = response.data.height/$scope.ppmy;   
												 
												$scope.mapDetails.height_m= response.data.height;
												$scope.mapDetails.width_m= response.data.width;
												$scope.mapDetails.ppmx= response.data.ppmx; 
												$scope.mapDetails.ppmy= response.data.ppmy; 
												$scope.mapDetails.mapType= 'quuppa';  
												
												var southWest = map
														.unproject(
																[
																		0,
																		$scope.mapDetails.height ],
																		defZoomValue);
												var northEast = map.unproject(
														[ $scope.mapDetails.width,
																0 ], defZoomValue);
												map
														.setMaxBounds(new L.LatLngBounds(
																southWest,
																northEast));
												
												map.fitBounds(new L.LatLngBounds(southWest,northEast)); 

												$scope.imageOverlay = L.imageOverlay(
														$scope.mapImage,
														new L.LatLngBounds(
																southWest,
																northEast))
														.addTo(map);
												
												$scope.marker  = new L.Marker(pixelsToLatLng(0,0), {
												    icon: new L.DivIcon({
												        className: 'my-div-icon',
												        html: '<div> <img class="staringImage" src="./assets/img/red_circle.png"/> '
												    })
												});
												$scope.marker.addTo(map);
												
												$scope.grid = L.grid({options: {
											        position: 'topright',
											        bounds: new L.LatLngBounds(
															southWest,
															northEast),
													mapDetails:	$scope.mapDetails,
													defZoomValue:defZoomValue,
													gridSize: 1
											    }}).addTo(map);
												
												$scope.marker1 = new L.Marker(pixelsToLatLng(-35, -5), {
												    icon: new L.DivIcon({
												        className: 'my-div-icon',
												        html: '<span class="staringlabel">(0,0)</span>'
												    })
												});
												$scope.marker1.addTo(map);
												

												$scope.getZoneDetails();
												$scope.getQuuppaAssetDetail();
												$scope.getTypes();
											} else {
												$scope.tagData = false;
												$scope.tagNoData = true;
											}
										});
					};
					
					
					$scope.mistLogin = function() {
 						$scope.mapDetails={};
						$http({
							method : 'GET',
							url : './mistLogin',
							headers : {
								'orgName' : $scope.org,
								'sessionid':$routeParams.sessionid
 							}
						}).then(
								function(response) {
									if (response != null
											&& response.data != null
											&& response.data != "BAD_REQUEST" && response.data.status == "success") {
										 
										$scope.orgDetails = response.data.orgDetails ;
										$scope.siteId = $scope.orgDetails.siteId;
										$scope.mapId = $scope.orgDetails.mapId[0];
										$scope.orgId = $scope.orgDetails.orgId;
										$scope.region = $scope.orgDetails.region;
										
										// map.closePopup();
										map.eachLayer(function(layer) { 
											if(layer.id!==undefined) {
												layer.remove();  
											}
										 
				 						}); 
										 if ($scope.imageOverlay!=null && map.hasLayer($scope.imageOverlay))
											 map.removeLayer( $scope.imageOverlay); 
										 

										 if ($scope.marker1!=null && map.hasLayer($scope.marker1))
											 map.removeLayer($scope.marker1); 
										  
										 if ($scope.marker!=null && map.hasLayer($scope.marker))
											 map.removeLayer($scope.marker); 
										 
							  
										  
										 if($scope.grid!=undefined)
											 $scope.grid.onRemove(map);
										 
										 $scope.imageOverlay = null;
										 $scope.marker1 = null;
										 $scope.marker = null;
 								  
									     $scope.getmap(); 
									     $scope.getAssetDetail();
 									} else {
										$scope.noList = true;
										if(response.data.message!=undefined && response.data.message!=null)
											 $.alert({
			                             title: $rootScope.MSGBOX_TITLE,
                                         content: response.data.message,
                                         icon: 'fa fa-times',
                                          });
									}
								});
					};

					
					
					$scope.getAssetDetail=function(){ 
						
						map.eachLayer(function(layer) { 
							if(layer.assetid!==undefined) {
								layer.remove();  
							}
						 
 						});  
						
						$http({   
							method : 'GET',
							url : './getAssetStatus',
								headers : {
									'orgName' : $scope.org,
									'mapId' : $scope.mapId,
									'siteId' : $scope.siteId,
									'region':$scope.region,
									'sessionid':$routeParams.sessionid
 								}
						}) 
								.then(
										function(
												response) {
											$rootScope.hideloading('#loadingBar');
											if (response != null
													&& response.data != null
													&& response.data != "BAD_REQUEST") {  
												$scope.tagData = true;
												$scope.tagNoData = false;
												$scope.assetList = response.data;  
												for(var i=0;i<$scope.assetList.length;i++){
													$scope.assetLocation = new L.Marker(pixelsToLatLng($scope.assetList[i].x,$scope.assetList[i].y), {
													    icon: new L.DivIcon({
													        className: 'assetslabel',
													        html: '<span ><i class="fa fa-user"></i></span>'
													    })
													}); 
													$scope.assetLocation.assetid=i;
													$scope.assetLocation.name=$scope.assetList[i].name;
													$scope.assetLocation.addTo(map);  
													
 													$scope.assetLocation.on('mouseover', mouseoverpop);
 													$scope.assetLocation.on('mouseout', mouseout);
 													$scope.assetLocation.on('click', onClick);
												}
											} else { 
												$scope.tagData = false;
												$scope.tagNoData = true;
											}
										}); 
					}
					
					 $scope.defaultval==null
					 $scope.getConfigData=function(){ 
							
	 						$http({  
								method : 'GET',
								url : './getConfigData',
	 							headers : {
									'orgName' : $scope.org,
	 							}
							})  
							.then( 
									function(response) {
										if (response != null
												&& response.data != null && response.data.length!=0
												&& response.data != "BAD_REQUEST" && response.data.configData!=undefined && response.data.configData.length!=0) {
										 
	 										$scope.configData = response.data.configData;
	 										$scope.configData.forEach(value => {
	 										  if(value.name=="Live Tag Tracing"){
	 											$scope.defaultval = value.defaultval;
	 										  }
	 										});
	 										 
										} else {  
											$scope.configData= {}; 
	 									}
									});
						}
					 
					 $scope.getConfigData();
					 
					$scope.getQuuppaAssetDetail=function(){ 
						
						map.eachLayer(function(layer) { 
							if(layer.assetid!==undefined) {
								layer.remove();  
							}
						 
 						});  
						
						$http({   
							method : 'GET',
							url : './getLiveTagDetails',
								headers : {
									'orgName' : $scope.org,
									'sessionid':$routeParams.sessionid
 								}
						}) 
								.then(
										function(
												response) {
											$rootScope.hideloading('#loadingBar');
											if (response != null
													&& response.data != null
													&& response.data != "BAD_REQUEST" && response.data.length!=0) {  
												$scope.tagData = true;
												$scope.tagNoData = false;
												$scope.assetList=[];
												var colorVal='#089b3c';
												for(var i=0;i<response.data.length;i++){
													if($scope.type=="All"||$scope.type==response.data[i].minorType||($scope.type=="Free"&&response.data[i].majorType==""&&response.data[i].minorType=="")){
												$scope.assetList.push(response.data[i]);
													if(response.data[i].colorCode==undefined||response.data[i].colorCode=='')
														 colorVal = '#089b3c';
													else
													 colorVal = response.data[i].colorCode;
												
													$scope.assetLocation = new L.Marker(pixelsToLatLng((response.data[i].x)/($scope.ppmx),(response.data[i].y)/($scope.ppmy)), {
													    icon: new L.DivIcon({
													        className: 'assetslabel',
													        html: '<span style=color:'+colorVal+'><i class="fa fa-user"></i></span>'
													    })
													});  
													$scope.assetLocation.assetid=i;
													$scope.assetLocation.name=response.data[i].name;
													$scope.assetLocation.addTo(map);  
													
 													$scope.assetLocation.on('mouseover', mouseoverpop);
 													$scope.assetLocation.on('mouseout', mouseout);
 													$scope.assetLocation.on('click', onClick);
 													 }
												}
												if($scope.assetList.length==0&&$scope.type!="All") {
														$scope.noDataText="No tag found for "+$scope.type;
														$scope.tagData = false;
														$scope.tagNoData = true;
														$scope.isFilter=true;

														}else{
														$scope.tagData = true;
														$scope.tagNoData = false;
														$scope.isFilter=false;
														 
														}
											$timeout(function() {
													for(var i=0;i<$scope.assetList.length;i++){
													     $("#liveColorId"+i).val($scope.assetList[i].colorCode==undefined||$scope.assetList[i].colorCode==''?'#089b3c':$scope.assetList[i].colorCode); 
 													}
												}, 100);
												
												
												if($scope.selectedRow!=null) {
													$scope.last_seen= $scope.assetList[$scope.selectedRow].last_seen;
							 						$scope.xval = $scope.assetList[$scope.selectedRow].x;
													$scope.yval = $scope.assetList[$scope.selectedRow].y; 
												}
											} else { 
												$scope.tagData = false;
												$scope.tagNoData = true;
												$scope.noDataText="No data found.No updates for last 5 minutes";
											}
										}); 
					}
					$scope.filterTypeChange=function(){ 
					$scope.type= $("#colortypeSelect option:selected").text(); 
					$scope.getQuuppaAssetDetail();
						}

					var tooltipPopup;  
					function mouseout(e) { 
				        map.closePopup(tooltipPopup);
				    }
					
					function mouseoverpop(e) { 
					    tooltipPopup =L.popup({ offset: L.point(0, -5)});
					    		tooltipPopup.setContent(e.target.name);
					            tooltipPopup.setLatLng(e.target.getLatLng());
					            tooltipPopup.openOn(map);

					    }
  					
					
					
					  
					
					
					var command = L.control({position: 'topright'});

					command.onAdd = function (map) {
					    var div = L.DomUtil.create('div', 'command');

					    div.innerHTML = '<form>Grid (1X1M) <input id="command" type="checkbox" checked/></form>'; 
					    return div;
					};

					command.addTo(map);


					// add the event handler
					function handleCommand() {
 						if(this.checked==true) {
							$scope.grid.onAdd(map)
						} else {
							$scope.grid.onRemove(map);
						}
					}

					document.getElementById ("command").addEventListener ("click", handleCommand, false);
					
					 
				  var editMode=false;
					function reload() {  
						$scope.selectAll =false; 
                     	 if($routeParams.serviceprovider =="quuppa"){  
								 if($scope.isTrack==true) {
		 								$.confirm({
					                    title: $rootScope.MSGBOX_TITLE,
		                                content: 'Tracking will be stopped and its details will be reset. Do you want to continue?',
		                                confirmButton: 'OK',
		                                cancelButton: 'Cancel',
		                                confirmButtonClass: 'btn-ok',
		                                cancelButtonClass: 'btn-cancel',
		                                autoClose:false,
		                                icon: 'fa fa-question-circle',
										theme: 'hololight',
		                                confirm: function(){
				   				        	$scope.getQuuppaAssetDetail();
								            resetOK();  
		                                 	}, 
		                                cancel: function () {
			                              editMode=false;
		                              },
		                            });
								 
								}else{
									editMode=true;
								}
		   					    if (editMode === false) {
		   				           return;
		   				        } else {
		   				        	$rootScope.showloading('#loadingBar');
                                    editMode=false;
		   				        	$scope.getQuuppaAssetDetail();
		   				        }
							 
						} else {
							$rootScope.showloading('#loadingBar');
							 editMode=false;
							 $scope.getAssetDetail();
						      resetOK();  
						}
						
						 function resetOK(){
						 if($scope.selectedRow!=null) {
	                        if($scope.assetList[$scope.selectedRow].last_seen!=undefined) {
								$scope.last_seen= $scope.assetList[$scope.selectedRow].last_seen;
							} else{
								$scope.last_seen= "-";
							}
	                        
	                        $scope.xval = $scope.assetList[$scope.selectedRow].x;
							$scope.yval = $scope.assetList[$scope.selectedRow].y;
                         }
 						
						$interval.cancel(mytimeout) ;
						 assetVal.length=0;
 						// indexVal.length=0;
						//$scope.trackBtn = true;
						//$scope.stopBtn =false;
						//$scope.isTrack =false; 
						
						$scope.stopBtnIcon.remove();
						$scope.userLocationBtn.addTo(map);
						document.getElementById ("userLocationBtn").addEventListener ("click", onTimeout, false);
						$scope.isTrack =false;
						}
                        
                     }
 					
					  
					function latLngToPixels(latlng) {
						var pixelArr = [];
						for (var i = 0; i < latlng[0].length; i++) {

							var latlngArr = latlng[0][i];

							var pt= map.project([ latlngArr.lat,
                                latlngArr.lng ], defZoomValue); 
							
                            pixelArr.push({
                                "x" : pt.x.toFixed(),
                                "y" : pt.y.toFixed(),
                            });


						}
						return pixelArr;
					} 
					
					function roundVal(latlng) {
						var pixelArr = [];
						for (var i = 0; i < latlng.length; i++) {

							var pt = latlng[i];
 							 
                            
                            pixelArr.push({
                                "x" : (parseFloat(pt.x)).toFixed(),
                                "y" : (parseFloat(pt.y)).toFixed(),
                            });

						}
						return pixelArr;
					} 

					function editlatLngToPixels(latlng) {
						var pixelArr = [];
						for (var i = 0; i < latlng[0].length; i++) {

							var latlngArr = latlng[0][i];

							var pt= map.project([
                                (latlngArr.lat / $scope.ppm),
                                (latlngArr.lng / $scope.ppm) ], defZoomValue);
                                                         
                            
                            pixelArr.push({
                                "x" : pt.x.toFixed(),
                                "y" : pt.y.toFixed(),
                            });

						}
						return pixelArr;
					} 

					function pixelsToLatLng(x, y) {
						return map.unproject([ x, y ], defZoomValue);
					} 

				

					$scope.ppm = "";
 	                    
					$scope.getmap = function() {

						$http({
							method : 'GET',
							url : './mistmapDetails',
							headers : {
								'orgName' : $scope.org,
								'mapId' : $scope.mapId,
								'siteId' : $scope.siteId,
								'region':$scope.region,
								'sessionid':$routeParams.sessionid
							}
						})
								.then(
										function(response) {
 
											if (response != null
													&& response.data != null
													&& response.data != "BAD_REQUEST"
													&& response.data.url != null
													&& response.data.url != "") {

												$scope.mapDetails = response.data;
												$scope.mapImage = response.data.url;
												$scope.ppm = response.data.ppm;
												$scope.mapDetails.mapType= 'mist';
												$scope.editableLayers = new L.FeatureGroup();
												map.addLayer( $scope.editableLayers);
												
												map.setZoom(defZoomValue);
												var southWest = map
														.unproject(
																[
																		0,
																		response.data.height ],
																		defZoomValue);
												var northEast = map.unproject(
														[ response.data.width,
																0 ], defZoomValue); 
												
												map.setMaxBounds(new L.LatLngBounds(southWest,northEast));
												
												map.fitBounds(new L.LatLngBounds(southWest,northEast)); 
												
												$scope.imageOverlay = L.imageOverlay(
														$scope.mapImage,
														new L.LatLngBounds(
																southWest,
																northEast))
														.addTo(map);
												
												$scope.marker  = new L.Marker(pixelsToLatLng(0,0), {
												    icon: new L.DivIcon({
												        className: 'my-div-icon',
												        html: '<div> <img class="staringImage" src="./assets/img/red_circle.png"/> '
												    })
												});
												$scope.marker.addTo(map);
												
												$scope.grid = L.grid({options: {
											        position: 'topright',
											        bounds: new L.LatLngBounds(
															southWest,
															northEast),
													mapDetails:	$scope.mapDetails,
													defZoomValue:defZoomValue,
													gridSize: 1
											    }}).addTo(map);
												
												$scope.marker1 = new L.Marker(pixelsToLatLng(-35, -5), {
												    icon: new L.DivIcon({
												        className: 'my-div-icon',
												        html: '<span class="staringlabel">(0,0)</span>'
												    })
												});
												$scope.marker1.addTo(map);
												
												
												
 												$scope.getZoneDetails();
												
											} else {
												$scope.noList = true;
											}
										});
					};

				 
					$scope.getZoneDetails = function(){
						
 
						map.eachLayer(function(layer) {
							if(layer.id !== undefined){ 
								layer.remove();
							}
						});
						
						$http({
							method : 'GET',
							url : './mistZoneDetails',
							headers : {
								'orgName' : $scope.org,
								'mapId' : $scope.mapId,
								'siteId' : $scope.siteId,
								'region':$scope.region,
								'sessionid':$routeParams.sessionid
 							}
						})
								.then(
										function(
												response) {
											$rootScope.hideloading('#loadingBar');
											if (response != null
													&& response.data != null
													&& response.data != "BAD_REQUEST") { 

												$scope.zoneList = response.data; 
												$scope.zoneList.sort(GetSortOrder("name"));
												
												if(!$scope.isAPView){
												for (var i = 0; i < $scope.zoneList.length; i++) {
													if($scope.zoneList[i].zoneStatus=="Active") {
 													var bounds  = [];
														if($scope.zoneList[i].classification!=""){
															var classification = $scope.zoneClassificationList.filter(x => x.classification ===$scope.zoneList[i].classification)[0];
															if(classification!=null && classification!=undefined && classification!='')
																$scope.colorcode = classification.colorCode;
															 else  
																$scope.colorcode = "#ff7800"
																 
														} else {
															$scope.colorcode = "#ff7800"
														}
													
													if($scope.zoneList[i].zoneType== undefined || $scope.zoneList[i].zoneType==null || $scope.zoneList[i].zoneType=="polygon" || $scope.zoneList[i].zoneType==""){ 
														for (var j = 0; j < $scope.zoneList[i].vertices.length; j++) {  
																bounds.push(pixelsToLatLng(parseFloat($scope.zoneList[i].vertices[j].x) 
																		,parseFloat($scope.zoneList[i].vertices[j].y)));
														   
														}
														
														$scope.zoneList[i].zoneType="polygon";
																	var pol = L
																	.polygon(
																			bounds,
																			{
																				color : $scope.colorcode ,
																				weight : 1,
																				fillColor :$scope.colorcode , 
																				fillOpacity: 0.5
																				})
																	.addTo(
																			map)
																	.addTo(
																			$scope.editableLayers);
																	 

																	pol.id = i;
																	pol.primaryid = $scope.zoneList[i].id
																	pol.name = $scope.zoneList[i].name;
																	pol.classification = $scope.zoneList[i].classification;
																	pol.restrictions = $scope.zoneList[i].restrictions;
																	pol.ppm = $scope.ppm;
																	pol.vertices = bounds;
																	pol.map_id = $scope.zoneList[i].map_id;
																	pol.colorcode = $scope.colorcode;
																	pol.zoneType = $scope.zoneList[i].zoneType;
																	pol.scopeval =$scope;
													} else {

													var bounds1 = [
															[ pixelsToLatLng(
																	parseFloat($scope.zoneList[i].vertices[0].x) ,
																	parseFloat($scope.zoneList[i].vertices[0].y)) ],
															[ pixelsToLatLng(
																	parseFloat($scope.zoneList[i].vertices[1].x),
																	parseFloat($scope.zoneList[i].vertices[1].y)) ],
															[ pixelsToLatLng(
																	parseFloat($scope.zoneList[i].vertices[2].x),
																	parseFloat($scope.zoneList[i].vertices[2].y)) ],
															[ pixelsToLatLng(
																	parseFloat($scope.zoneList[i].vertices[3].x),
																	parseFloat($scope.zoneList[i].vertices[3].y)) ] ];
													var rec = L
															.rectangle(
																	bounds1,
																	{
																		color : $scope.colorcode ,
																		weight : 1,
																		fillColor :$scope.colorcode , 
																		fillOpacity: 0.5
																	})
															.addTo(
																	map)
															.addTo(
																	$scope.editableLayers);
										
													rec.id = i;
													rec.primaryid = $scope.zoneList[i].id
													rec.name = $scope.zoneList[i].name;
													rec.classification = $scope.zoneList[i].classification;
													rec.restrictions = $scope.zoneList[i].restrictions;
													rec.ppm = $scope.ppm;
													rec.vertices = bounds1;
													rec.map_id = $scope.zoneList[i].map_id;
													rec.zoneType = $scope.zoneList[i].zoneType; 
													rec.colorcode = $scope.colorcode;
													rec.scopeval =$scope;
												  }
 												 }
											   }
											 }

 											} else {
												$scope.noList = true;
											}
										});
						
					}
					
					
					function isInsidePolygon(point, vs) {
 
					    var x = point[0], y = point[1];

					    var inside = false;
					    for (var i = 0, j = vs.length - 1; i < vs.length; j = i++) {
					        var xi = vs[i][0], yi = vs[i][1];
					        var xj = vs[j][0], yj = vs[j][1];

					        var intersect = ((yi > y) != (yj > y))
					            && (x < (xj - xi) * (y - yi) / (yj - yi) + xi);
					        if (intersect) inside = !inside;
					    }

					    return inside;
					};
					
					 
					 function checkZoneOverlap(zoneId, points){
						 
						 var array = [];
						 
						 for (var k = 0; k < points.length; k++) {   
							    array.push([parseInt(points[k].x) ,parseInt(points[k].y)]);
							}
						 array.push([parseInt(points[0].x) ,parseInt(points[0].y)]);
						 
						 var poly1 = turf.polygon([array]);
						 
						 for (var i = 0; i < $scope.zoneList.length; i++) {
								var id  = $scope.zoneList[i].id;
								if(id!='' && id!=zoneId){
									var vertices = $scope.zoneList[i].vertices; 
									var bounds1  = []; 
									for (var j = 0; j < vertices.length; j++) {
										bounds1.push([parseInt(vertices[j].x), parseInt(vertices[j].y)]);  
									} 
									
									bounds1.push([parseInt(vertices[0].x), parseInt(vertices[0].y)]);  
									 
									var poly2 = turf.polygon([bounds1]); 
									if(turf.booleanOverlap(poly1, poly2) || turf.booleanContains(poly1, poly2) 
											|| turf.booleanContains(poly2, poly1)){
										return false;	
									} 
								}
								
							}
							return true;
					 }
					 				 				
					 
					function isInsideRect(x1, y1, x2, y2, x, y) {
						if (x > x1 && x < x2 && y > y1 && y < y2)
						return true;

						return false;
					 }
					
					
					
					$scope.checkAll = function() {
					    angular.forEach($scope.assetList, function(asset) {
					    	asset.selected = $scope.selectAll;
					    });
					  };  
					   
					    
					  function onStop(){
					    	$scope.isTrack =false;
					    	//$scope.trackBtn = true;
							// $scope.stopBtn =false;
							   assetVal.length=0; 
							   $scope.userLocationBtn.addTo(map);
								 document.getElementById ("userLocationBtn").addEventListener ("click", onTimeout, false);
								 $scope.stopBtnIcon.remove();
					    	$interval.cancel(mytimeout);
					    }
					    
					    $scope.$on('$destroy', function(){
					        $interval.cancel(mytimeout) 
					    });
					  
					    
					 var assetVal=[];
				 
					//$scope.trackBtn = true;
					// $scope.stopBtn =false;
					 
					 
					 
					  $scope.onTrack=function(){
						  $scope.stopBtnIcon.addTo(map);
						  document.getElementById ("stopBtnIcon").addEventListener ("click", onStop, false);
  						  $scope.userLocationBtn.remove();
						  var assetObj=[];
						  	 map.closePopup(); 
						   //  $scope.trackBtn = false;
							// $scope.stopBtn =true;
						   
						   	 
							  angular.forEach($scope.assetList, function(asset,index) {
 									  if (asset.selected){
										   var selectedColor = document.getElementById("liveColorId"+index).value;
 										   assetObj.push({"name":asset.id,"color":selectedColor,"index":index})
										  assetVal.push(asset.id);
										 
									  }
 							  });
							  
							  assetObj =  _.uniq(assetObj, JSON.stringify)

							  
						 if(assetVal.length!=0) { 
 							  
							assetVal=  _.uniq(assetVal)
					 
							  
								$http({   
									method : 'POST',
									url : './getLiveTagDetailsFindById',
									 data: {assetVal:assetVal},
										headers : {
											'orgName' : $scope.org,
											'sessionid':$routeParams.sessionid
		 								}
								}) 
										.then(
												function(
														response) {
													$rootScope.hideloading('#loadingBar');
													if (response != null
															&& response.data != null
															&& response.data != "BAD_REQUEST" && response.data.length!=0) {  
														
														map.eachLayer(function(layer) { 
															if(layer.assetid!==undefined) {
 																layer.remove();  
															}
							 	 						});  
														
														$scope.tagData = true;
														$scope.tagNoData = false;
														$scope.assetList1 = response.data;  
														
														
														for(var i=0;i<assetObj.length;i++){
													if($scope.type=="All"||$scope.type==response.data[i].minorType||($scope.type=="Free"&&response.data[i].majorType==""&&response.data[i].minorType=="")){

															var assetValues = $scope.assetList1.filter(x => x.name ===assetObj[i].name)[0];

															$scope.assetLocation = new L.Marker(pixelsToLatLng((assetValues.x)/($scope.ppmx),(assetValues.y)/($scope.ppmy)), {
															    icon: new L.DivIcon({
															        className: 'assetslabel',
															        html: '<span style=color:'+assetObj[i].color+'><i class="fa fa-user"></i></span>'
															    })
															});  
															
															$scope.assetList[assetObj[i].index].last_seen=assetValues.last_seen;
															
															$scope.assetLocation.assetid=assetObj[i].index;
															$scope.assetLocation.name=assetValues.name;
															$scope.assetLocation.addTo(map);  
															
		 													$scope.assetLocation.on('mouseover', mouseoverpop);
		 													$scope.assetLocation.on('mouseout', mouseout);
		 													$scope.assetLocation.on('click', onClick);
														}
														}
														
																if($scope.assetList.length==0&&$scope.type!="All") {
														$scope.noDataText="No tag found for "+$scope.type;
															$scope.tagData = false;
														$scope.tagNoData = true;
														}else{
																$scope.tagData = true;
														$scope.tagNoData = tfalserue;
														 
														}
														 
 														
														if($scope.selectedRow!=null) {
															if($scope.assetList[$scope.selectedRow].last_seen!=undefined) {
																$scope.last_seen= $scope.assetList[$scope.selectedRow].last_seen;
															} else{
																$scope.last_seen= "-";
															}
									 						$scope.xval = $scope.assetList[$scope.selectedRow].x;
															$scope.yval = $scope.assetList[$scope.selectedRow].y; 
														}
													} else { 
														map.eachLayer(function(layer) { 
															if(layer.assetid!==undefined) {
 																layer.remove();  
															}
							 	 						});  
														$("#selectedAssetView").hide();
													     $("#selectedAssetViewheader").hide(); 
														$scope.tagData = false;
														$scope.tagNoData = true; 
														$scope.noDataText="No data found.No updates for last 5 minutes";
													}
												}); 
						 }else {
							    $scope.isTrack =false;
								$scope.error = true;
 							 	//$scope.trackBtn = true;
							 	//$scope.stopBtn =false;
								 $scope.stopBtnIcon.remove();
		  						 $scope.userLocationBtn.addTo(map);
		  						 document.getElementById ("userLocationBtn").addEventListener ("click", onTimeout, false);
						    	 $interval.cancel(mytimeout);
 								$scope.alert_error_msg = "Please select the tag";
								$timeout(function() {
									$scope.error = false;
								}, 2000);
							 return ;
						 }
					  }  
					  $scope.isTrack =false;
					  function onTimeout (){
						  $scope.isTrack =true;
						   if($scope.defaultval==null){
							   $scope.defaultval = 10000;
						   }
					        mytimeout = $interval($scope.onTrack,10000);
					       $scope.onTrack();
					    }
 					  
						  $scope.checkIfAllSelected = function() {
						      $scope.selectAll = $scope.assetList.every(function(asset) {
						        return asset.selected == true
						      })
						    };
						
					 
					var options = {
						position : 'topright',
						draw : {
							polygon : false,
							polyline : false,
							circlemarker : false,
							circle : false,
							marker : false,
							rectangle : false
 						},
						edit : false

					};

					
					$scope.refreshBtn = L.control({position: 'topright'});
                    $scope.refreshBtn.onAdd = function (map) {

                        this._div = L.DomUtil.create('div', 'mainMenu'); 
                        this._div.innerHTML += ' <img class="reloadImage" id="refreshBtn" src="./assets/img/reload.jpg"/>';
                        return this._div;
                    };

                    $scope.refreshBtn.addTo(map);  

                    document.getElementById ("refreshBtn").addEventListener ("click", reload, false);

                    
                    $scope.userLocationBtn = L.control({position: 'topright'});
                    $scope.userLocationBtn.onAdd = function (map) {

                        this._div = L.DomUtil.create('div', 'mainMenu');
                        this._div.innerHTML += ' <img class="reloadImage" id="userLocationBtn" src="./assets/img/track.png"/>';
                        return this._div;
                    };

                    $scope.userLocationBtn.addTo(map);
                     document.getElementById ("userLocationBtn").addEventListener ("click", onTimeout, false);

                     $scope.stopBtnIcon = L.control({position: 'topright'});
                     $scope.stopBtnIcon.onAdd = function (map) {

                         this._div = L.DomUtil.create('div', 'mainMenu');
                         this._div.innerHTML += ' <img class="reloadImage" id="stopBtnIcon" src="./assets/img/stop.png"/>';
                         return this._div;
                     };
					
					$scope.drawControl = new L.Control.Draw(options);
					$scope.drawControlEdit = new L.Control.Draw({
						  draw: false
					});
					map.addControl($scope.drawControl);
				
					 
					 
					
					$scope.popup = L.popup({
						maxWidth : 400
					});
 
			 
					function mouseOutClosePopup(e) {
						map.closePopup(); 
					}

					
					
					 $scope.getData = function() {
							if ($scope.org != null && $scope.org != "") {
								$rootScope.showloading('#loadingBar');
								$scope.getZoneClassifications();
								if($routeParams.serviceprovider =="quuppa"){ 
									$scope.serviceProviderStatus = true; 
									$scope.getQuuppamap();
									$scope.userLocationBtn.addTo(map);
									document.getElementById ("userLocationBtn").addEventListener ("click", onTimeout, false);
								} else {
									$scope.serviceProviderStatus = false; 
									$scope.mistLogin();
									$scope.userLocationBtn.remove(map);
	 								$scope.stopBtnIcon.remove();
								}
								 
							} 
						};
						$scope.getData();
					
					var selectedRowCSS = {'background': '#cccccc', 'color': '#000000', 'font-weight': 'bold'};
					var unSelectedRowCSS = {'background': '#ffffff', 'color': '#000000', 'font-weight': 'normal'};
					
					$scope.selectZone = function(index) {

						$(".tbodyWidth").css({ 'height' : "200px" });

						$("#selectedAssetView").show();
						$("#selectedAssetViewheader").show();
						 
 						$scope.selectedRow = index;
  						var rowCount = ($('#zoneTable tr').length) - 1;
						for (var i = 0; i < rowCount; i++) {
							$("#zoneTable > tbody > tr:nth-child(" + (i + 1) + ")").css(unSelectedRowCSS);
						}
						
						$("#zoneTable > tbody > tr:nth-child(" + (index + 1) + ")").css(selectedRowCSS); 
						  
						map.eachLayer(function(layer) {
							if(layer.assetid !== undefined){ 
								if (index == layer.assetid) {
									   layer.setOpacity(1); 
									   tooltipPopup =L.popup({ offset: L.point(0, -5)});
							    		tooltipPopup.setContent(layer.name);
							            tooltipPopup.setLatLng(layer._latlng);
							            tooltipPopup.openOn(map); 
									} else {
									   layer.setOpacity(0.3); 
									} 
							}
 						});
						$scope.xval = $scope.assetList[index].x;
						$scope.yval = $scope.assetList[index].y;
						$scope.name = $scope.assetList[index].name;
						$scope.mac = $scope.assetList[index].mac;
 						$scope.last_seen= $scope.assetList[index].last_seen;
 						$scope.battery_voltage= $scope.assetList[index].battery_voltage;
 						$scope.ibeacon_minor= $scope.assetList[index].ibeacon_minor;
 						$scope.ibeacon_uuid= $scope.assetList[index].ibeacon_uuid;
 						$scope.ibeacon_major= $scope.assetList[index].ibeacon_major;
					    if($routeParams.serviceprovider =="quuppa"){  
 							map.panTo(pixelsToLatLng(parseFloat($scope.assetList[index].x/$scope.ppmx)
	                                ,parseFloat($scope.assetList[index].y/$scope.ppmy)));
 						}else
							map.panTo(pixelsToLatLng(parseFloat($scope.assetList[index].x)
	                                ,parseFloat($scope.assetList[index].y)));
				
					}
					$scope.closebtn=function(){
						$("#selectedAssetView").hide();
				        $("#selectedAssetViewheader").hide();
					}
					  function onClick(e) {  
						  
				        $(".tbodyWidth").css({ 'height' : "200px" }); 

						$("#selectedAssetView").show();
				        $("#selectedAssetViewheader").show();
 						$scope.selectedRow = e.sourceTarget.assetid;
 						
 						$scope.xval = $scope.assetList[e.sourceTarget.assetid].x;
						$scope.yval = $scope.assetList[e.sourceTarget.assetid].y;
						$scope.name = $scope.assetList[e.sourceTarget.assetid].name;
						$scope.mac = $scope.assetList[e.sourceTarget.assetid].mac;
 						$scope.last_seen= $scope.assetList[e.sourceTarget.assetid].last_seen;
 						$scope.battery_voltage= $scope.assetList[e.sourceTarget.assetid].battery_voltage;
 						$scope.ibeacon_minor= $scope.assetList[e.sourceTarget.assetid].ibeacon_minor;
 						$scope.ibeacon_uuid= $scope.assetList[e.sourceTarget.assetid].ibeacon_uuid;
 						$scope.ibeacon_major= $scope.assetList[e.sourceTarget.assetid].ibeacon_major;
 						
 						$scope.$apply();
 						var rowCount = ($('#zoneTable tr').length) - 1;
						for (var i = 0; i < rowCount; i++) {
							$("#zoneTable > tbody > tr:nth-child(" + (i + 1) + ")").css(unSelectedRowCSS);
						}
						
						$("#zoneTable > tbody > tr:nth-child(" + (e.sourceTarget.assetid + 1) + ")").css(selectedRowCSS); 

						  
					 
						map.eachLayer(function(layer) {
							if(layer.assetid !== undefined){ 
								if (e.sourceTarget.assetid == layer.assetid) {
									   layer.setOpacity(1); 
									} else {
									   layer.setOpacity(0.3); 
									} 
							} 
 						}); 
						
						map.panTo(pixelsToLatLng(parseFloat($scope.assetList[e.sourceTarget.assetid].x) 
                                ,parseFloat($scope.assetList[e.sourceTarget.assetid].y)));
  
					}

					 $scope.getDate=function(value){
						 return moment(value*1000).format("HH:mm")  
					 }
					  
					function GetSortOrder(prop) {    
					    return function(a, b) {    
					        if (a[prop].toLowerCase() > b[prop].toLowerCase()) {    
					            return 1;    
					        } else if (a[prop].toLowerCase() < b[prop].toLowerCase()) {    
					            return -1;    
					        }    
					        return 0;    
					    }    
					}  
	
					
				});
