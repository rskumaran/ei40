app
		.controller(
				'tagPeopleManagementController',
				function($scope, $http, $window, $rootScope, $location,
						$timeout) {
					$scope.user = JSON.parse($window.sessionStorage
							.getItem("user"));
					if ($scope.user == null) {
						$location.path('/login');
						return;
					}
					
					if ($scope.authen == undefined) {
						$scope.authen = {};
					}
					if ($scope.fieldName == undefined) {
						$scope.fieldName = {};
					}
				 
					$scope.visitorType =["Visitor", "Vendor", "Contractor"];
					$scope.authen.countryCode = $rootScope.countryCodes[0];
					$scope.authen.contactPersonCountryCode = $rootScope.countryCodes[0];
					// $scope.authen.tagType = "Employee";
					$scope.checkTagMacId = {};
					$scope.checkTagMacId = {};
					$scope.checkTagTagId = {};
					$scope.checkTagType = {};
					$scope.checkTagPeopleId = {};
					$scope.tagData = new Array();
					$scope.tagEmployeeAssignedDetails= [];
					$scope.peopleIdTable =[];
					// $scope.checktagIdTable=[];
					$scope.checktagIdEmpTable=new Array();
					$scope.tagDataFilterList = new Array();
					$scope.locationList = [];
					$scope.searchText = "";
					var menudata = [];
					$scope.accessLimit_returnScreen =[];
					//$scope.accessLimit_returnScreen1 =[]; removed Tree view - Zone are not legible when tree is in read-only mode. Added list box instead of that
					$scope.accessLimitReturn = [];
					let selectedZones = 0;
					
					$scope.searchButton = true;
					$scope.finalallowedZones = [];
					$scope.selectedZoneIdList = [];

					$scope.show_peopleId = true;
					$scope.show_fname = false;
					$scope.show_lname = false;
					$scope.show_phoneNumber = false;
					$scope.show_companyName = false;
					$scope.show_accessLimit = false;
					$scope.show_date = false;
					$scope.show_stime = false;
					$scope.show_etime = false;
					$scope.show_contactPerson = false;
					$scope.varTagIdTextAssigned = '<div style="color:blue">';
					$scope.varTagIdTextFree = '<div style="color:green">';
					$scope.varTagIdTextNoTag = '<div style="color:red">';

					
					$scope.fetchTagIdTable=[];
					$('.select2').select2(); // searchable dropdown in fetch
												// screen intialization
					/*
					 * $('#checktagIdSelect').select2({ data:
					 * $scope.fetchTagIdTable, templateResult: function (d) {
					 * $scope.fieldName.checktagId=$(d.text); return $(d.text); },
					 * templateSelection: function (d) { return $(d.text); },
					 * });
					 */
					$('#checktagIdSelectEmployee').select2('destroy');
					$('#checktagIdSelectEmployee').select2(); 
					$('#id_peopleIdAssignTag').select2('destroy');
					$('#id_peopleIdAssignTag').select2(); 
					
					$scope.authen.peopleId = $scope.peopleIdTable[0];
					$scope.fieldName.checktagIdEmp=$scope.checktagIdEmpTable[0];
					
					// input field validation
					
					$("#id_peopleIdAssignTag, #fname, #lname, #id_contactPerson, #id_newTagId, #checktagIdSelect").keypress(
							function(e) {
								// if the letter is not alphabets then display
								// error
								// and don't type anything
								$return = ((e.which > 64) && (e.which < 91)) || ((e.which> 96) && (e.which < 123)) ||(e.which == 8) || (e.which == 32)  || (e.which >= 48 && e.which <= 57)
								if (!$return)
													
								{
									
									return false;
								}
							});	
					$("#id_companyName").keypress(
							function(e) {
								// if the letter is not alphabets then display
								// error
								// and don't type anything
								$return = ((e.which > 64) && (e.which < 91)) 
								|| ((e.which > 43) && (e.which < 47))
								|| ((e.which> 96) && (e.which < 123)) 
								||(e.which == 8) || (e.which == 32)  
								|| (e.which == 95)|| (e.which == 38)
								|| (e.which >= 48 && e.which <= 57)
								if (!$return)
													
								{
									
									return false;
								}
							});	
					
					$("#id_contactPersonPhoneNumber, #phoneNumber").keypress(
							function(e) {
								// if the letter is not digit then display error
								// and don't type anything
								
								if (e.which != 8 && e.which != 0
										&& (e.which < 48 || e.which > 57)) {
									
									return false;
								}
							});
					
					// to enable/disable sub menus
					$scope.user = JSON.parse($window.sessionStorage
							.getItem("user"));
					
					if ($scope.user.capabilityDetails != undefined
							&& $scope.user.capabilityDetails != null) {
						/*
						 * if ($scope.user.capabilityDetails.PTVA == 1) {
						 * $scope.showViewAllBtn = true; } else {
						 * $scope.showViewAllBtn = false; }
						 */
						
						if ($scope.user.capabilityDetails.PTAR == 1) {
							$scope.showAssignReturnBtn = true;
						} else {
							$scope.showAssignReturnBtn = false;
						}
						if ($scope.user.capabilityDetails.PTED == 1) {
							$scope.showEditBtn = true;
						} else {
							$scope.showEditBtn = false;
						}
						/*
						 * if ($scope.user.capabilityDetails.PTRN == 1) {
						 * $scope.showRenameBtn = true; } else {
						 * $scope.showRenameBtn = false; }
						 */
						// this code is for Employee assign tab anand 24-09
						if ($scope.user.capabilityDetails.PTEA == 1) {
							$scope.showEmpAssignBtn = true;
						} else {
							$scope.showEmpAssignBtn = false;
						}

					}					
					
					$scope.searchOption = function() {
						$scope.filterOption();
					}

					$scope.filterOption = function() {

						$scope.tagViewData = [];
						$scope.tagDataFilterList = [];

						for (var i = 0; i < $scope.tagData.length; i++) {							

							if ($scope.searchText == '') {
								$scope.tagDataFilterList
										.push($scope.tagData[i]);
							} else {
								if (
										($scope.tagData[i].tagId != undefined
												&& $scope.tagData[i].tagId != null ? $scope.tagData[i].tagId
												: "").toLowerCase()
												.includes(
												$scope.searchText.toLowerCase())
										/*||($scope.tagData[i].groupId != undefined
												&& $scope.tagData[i].groupId != null ? $scope.tagData[i].groupId
														: "").toLowerCase()
														.includes(
												$scope.searchText.toLowerCase())*/
										|| ($scope.tagData[i].macId != undefined
												&& $scope.tagData[i].macId != null ? $scope.tagData[i].macId
														: "")
												.toLowerCase().includes(
														$scope.searchText
																.toLowerCase())
										/*||($scope.tagData[i].manufacturer != undefined
												&& $scope.tagData[i].manufacturer != null ? $scope.tagData[i].manufacturer
														: "").toLowerCase()
										.includes(
														$scope.searchText
																.toLowerCase())*/
										|| ($scope.tagData[i].status != undefined
												&& $scope.tagData[i].status != null ? $scope.tagData[i].status
														: "").toLowerCase()
										
														.includes(
														$scope.searchText
																.toLowerCase())
										|| ($scope.tagData[i].assignmentId != undefined
												&& $scope.tagData[i].assignmentId != null ? $scope.tagData[i].assignmentId
												: "").toLowerCase()
												.includes(
														$scope.searchText
																.toLowerCase())
										|| ($scope.tagData[i].fromTime != undefined
												&& $scope.tagData[i].fromTime != null ? $scope.tagData[i].fromTime
												: "").toLowerCase()
												.includes(
														$scope.searchText
																.toLowerCase())
										|| ($scope.tagData[i].toTime != undefined
												&& $scope.tagData[i].toTime != null ? $scope.tagData[i].toTime
												: "").toLowerCase()
												.includes(
														$scope.searchText
																.toLowerCase())														
										|| ($scope.tagData[i].assignmentDate != undefined
												&& $scope.tagData[i].assignmentDate != null ? $scope.tagData[i].assignmentDate
												: "").toLowerCase()
												.includes(
														$scope.searchText
																.toLowerCase())
										|| ($scope.tagData[i].type != undefined
												&& $scope.tagData[i].type != null ? $scope.tagData[i].type
														: "").toLowerCase()
														.includes(
												$scope.searchText.toLowerCase())
										|| ($scope.tagData[i].modifiedDate != undefined
												&& $scope.tagData[i].modifiedDate != null ? $scope.tagData[i].modifiedDate
												: "").toLowerCase()
												.includes(
														$scope.searchText
																.toLowerCase())) {
									$scope.tagDataFilterList
											.push($scope.tagData[i]);

								}

							}
							
						}

						$scope.totalItems = $scope.tagDataFilterList.length;
						$scope.currentPage = 1;
						$scope.getTotalPages();

						if ($scope.tagData.length == 0
								|| $scope.tagDataFilterList.length == 0) {
							$scope.tagNoData = true;

						} else {
							
							$scope.tagNoData = false;

					if ($scope.tagDataFilterList.length > $scope.numPerPage){
						$scope.tagViewData = $scope.tagDataFilterList
								.slice(0, $scope.numPerPage);
					$scope.PaginationTab=true;
					}
					else {
						$scope.tagViewData = $scope.tagDataFilterList;
						$scope.PaginationTab=false;
					}
				
						}
					}// kumar 02/Jul filter option <-
					
					$scope.editPeopleTag = function() {
						$scope.tagPeopleEditScreen = false;
						$scope.tagPeopleEditUpdateScreen = true;
						$scope.searchButton = false;
						if($scope.authen.status == "Assigned" ||$scope.authen.status == "Blocked"|| $scope.authen.status == "Overdue")
							$scope.editStatusAssignmentType = true;
						else if($scope.authen.status == "Free" ||$scope.authen.status == "NotWorking"|| $scope.authen.status == "Expired")
							$scope.editStatusAssignmentType = false;
						}

					$scope.assignTagType = function() {
						if ($scope.authen.tagType == "Employee") {
							$scope.show_peopleId = true;
							$scope.show_fname = false;
							$scope.show_lname = false;
							$scope.show_phoneNumber = false;
							$scope.show_companyName = false;
							$scope.show_accessLimit = false;
							$scope.show_date = false;
							$scope.show_stime = false;
							$scope.show_etime = false;
							$scope.show_contactPerson = false;
							$scope.authen.peopleId = $scope.peopleIdTable[0];
							
						} else if ($scope.authen.tagType == "Visitor"
								|| $scope.authen.tagType == "Vendor"
								|| $scope.authen.tagType == "Contractor") {
							$scope.show_peopleId = false;
							$scope.show_fname = true;
							$scope.show_lname = true;
							$scope.show_phoneNumber = true;
							$scope.show_companyName = true;
							$scope.show_accessLimit = true;
							$scope.show_date = true;
							$scope.show_stime = true;
							$scope.show_etime = true;
							$scope.show_contactPerson = true;
							$scope.authen.countryCode = $rootScope.countryCodes[0];
							$scope.authen.contactPersonCountryCode = $rootScope.countryCodes[0];
							$scope.dateTimeReset();
							
							
							 $scope.authen.firstName = "",
							 $scope.authen.lastName = "";
							 $scope.authen.companyName = "",
							 $scope.authen.contactPerson ="";					
							 
							// $scope.treeViewZoneList =[];
							 $scope.fnCreateTreeWithZoneNme();
							 $scope.authen.countryCode = $rootScope.countryCodes[0];
						     $scope.authen.phoneNumber ="";
						     $scope.authen.contactPersonCountryCode  = $rootScope.countryCodes[0];
						     $scope.authen.contactPersonPhoneNumber ="";

						}
					}
					
					// Employee drop down API call for combo box People ID in
					// Assign screen for Employee
					$scope.getEmpData = function() {				
						if (!$rootScope.checkNetconnection()) { 
							 	//$rootScope.alertDialogChecknet();
								return; 
								}
					 $rootScope.showloading('#loadingBar');
						$http({
							method : 'GET',
							url : './staff',
							
						})
								.then(
										function success(response) {
											$rootScope.hideloading('#loadingBar');
											if (response != null
													&& response.data != null
													&& response.data.length != 0
													&& response.data != "BAD_REQUEST"
													&& response.data.data != undefined
													&& response.data.data.length != 0) {

												$scope.peopleIdTable.length = 0;

												for (var i = 0; i < response.data.data.length; i++) {
													if (response.data.data[i].active == true) {
														$scope.peopleIdTable
																.push(response.data.data[i].empId);
													}
												}
												
												
												if ($scope.peopleIdTable.length == 0){
													$scope.peopleIdTable
													.push("No Employee Data");
												}
												$('#id_peopleIdAssignTag').select2('destroy');
												$('#id_peopleIdAssignTag').select2(); 
												
											} else {
												$scope.peopleIdTable
												.push("No Employee Data");
											}
										}, function error(response) {
											$rootScope.hideloading('#loadingBar');
											$rootScope.fnHttpError(response);
																	
			   							});
					}
					$scope.assignedEmployeeDetailsEmpCombo = function(){
						if($scope.tagEmployeeAssignedDetails.length > 0){
						for(var k = 0; k < $scope.tagEmployeeAssignedDetails.length; k++  ){
							
							for(var j = 0; j < $scope.peopleIdTable.length; j++  ){
								if($scope.peopleIdTable[j] == $scope.tagEmployeeAssignedDetails[k])
									$scope.peopleIdTable.splice(j,1);
							}
							
						}
							}
							$('#checktagIdSelectEmployee').select2('destroy');
							$('#checktagIdSelectEmployee').select2(); 
							$('#id_peopleIdAssignTag').select2('destroy');
							$('#id_peopleIdAssignTag').select2(); 
							
							$scope.authen.peopleId = $scope.peopleIdTable[0];
							$scope.fieldName.checktagIdEmp=$scope.checktagIdEmpTable[0];
						//console.log($scope.checktagIdEmpTable);
						$scope.fieldName.checktagIdEmp=$scope.checktagIdEmpTable[0];
						/*console.log($scope.checktagIdEmpTable);
						console.log($scope.fieldName.checktagIdEmp);
						console.log($scope.peopleIdTable);*/
					}
					// allowable zone list API
					$scope.getZoneDetails = function() {
						if (!$rootScope.checkNetconnection()) { 
						 	//$rootScope.alertDialogChecknet();
							return; 
							}
					$http({
						method : 'GET',
						url : './zone',

					})
							.then(
									function(response) {
										if (response != null
												&& response.data != null
												&& response.data.length != 0
												&& response.data != "BAD_REQUEST"
												&& response.data.data != undefined
												&& response.data.data.length != 0) {

											$scope.locationList = [];

											for (var i = 0; i < response.data.data.length; i++) {
												if (response.data.data[i].active == true) {
													if(response.data.data[i].zoneType == "Trackingzone"){
													$scope.locationList
															.push({
																"id" : (response.data.data[i].id),
																"text" : (response.data.data[i].name)
															});
													}

												}
											}											
											menudata = JSON
													.parse(JSON
															.stringify($scope.locationList));
											$scope.fnCreateTreeWithZoneNme();
										} else {

										}
									}, function error(response) {
										$rootScope.hideloading('#loadingBar');
										$rootScope.fnHttpError(response);
																
		   							});
					}
					$scope.fnCreateTreeWithZoneNme = function() {
						let frequentdata;
						let menuIdArray = [];


						let tree = new Tree('.frequent', {
							data : [ {
								id : -1,
								text : 'All',
								children : menudata
							} ],
							closeDepth : 2,
							loaded : function() {
								this.values = [];								
							},
							onChange : function() {
								// frequentdata = this.selectedNodes;
								selectedZones = this.selectedNodes;
								$scope.selectedZoneIdList = this.selectedNodes;

								for (var i = 0; i < selectedZones.length; i++) {
									if (selectedZones[i].id >= 0)
										menuIdArray.push(selectedZones[i].id);
								}
							}
						})
					}
					/*
					$scope.fnReturnTreeWithZoneNme = function() {
						
						for(var i=0; i<$scope.locationList.length; i++){							
								 $scope.accessLimitReturn.push($scope.locationList[i].id);
						}
						
						let frequentdata;
						let menuIdArray = [];					
						
						let tree = new Tree('.allowableZoneTree', {
							data : [ {
								id : -1,
								text : 'All',
								children : menudata
							} ],
							closeDepth : 2,
							loaded : function() {		
								this.disables =  $scope.accessLimitReturn;
								this.values = $scope.accessLimit_returnScreen1;
							},
							onChange : function() {
								// frequentdata = this.selectedNodes;
								selectedZones = this.selectedNodes;
								$scope.selectedZoneIdList = this.values;
								
								for (var i = 0; i < selectedZones.length; i++) {
									if (selectedZones[i].id >= 0)
										menuIdArray.push(selectedZones[i].id);
								}

							}
						})
						
					}*/
					var reA = /[^a-zA-Z]/g;
					var reN = /[^0-9]/g;

					function sortAlphaNum(a1, b1) {
						var a = a1.tagId;
						var b = b1.tagId;
						var aA = a.replace(reA, "");
						var bA = b.replace(reA, "");
						if (aA === bA) {
							var aN = parseInt(a.replace(reN, ""), 10);
							var bN = parseInt(b.replace(reN, ""), 10);
							return aN === bN ? 0 : aN > bN ? 1 : -1;
						} else {
							return aA > bA ? 1 : -1;
						}
					}
					$scope.getTagData = function() {
						if (!$rootScope.checkNetconnection()) { 
							 	$rootScope.alertDialogChecknet();
								return; 
								} 
						 $rootScope.showloading('#loadingBar');
						$http({
							method : 'GET',
							url : './tag',

						})
								.then(
										function(response) {
											$rootScope
											.hideloading('#loadingBar');
											if (response != null
													&& response.data != null
													&& response.data.length != 0
													&& response.data != "BAD_REQUEST"
													&& response.data != undefined
													&& response.data.length != 0) {

												$scope.tagData.length = 0;
												response.data
												.sort(sortAlphaNum);
												
												for(var i=0; i< response.data.length; i++){
													var fromTime ="",toTime ="",createdDate="",modifiedDate="",assignmentDate="" ;
													 
													if(response.data[i].fromTime != null && response.data[i].fromTime != "" && response.data[i].fromTime != undefined){
														fromTime = response.data[i].fromTime.substring(0, response.data[i].fromTime.length-3);
														}
													if(response.data[i].toTime != null && response.data[i].toTime != "" && response.data[i].toTime != undefined){
														toTime = response.data[i].toTime.substring(0, response.data[i].toTime.length-3);
													}
													if(response.data[i].createdDate != null && response.data[i].createdDate != "" && response.data[i].createdDate != undefined){
														response.data[i].createdDate = response.data[i].createdDate.substring(0, response.data[i].createdDate.length-9);
														createdDate = moment(response.data[i].createdDate,"YYYY-MMM-DD").format("MMM DD, YYYY");
													}
													if(response.data[i].modifiedDate != null && response.data[i].modifiedDate != "" && response.data[i].modifiedDate != undefined){
													response.data[i].modifiedDate = response.data[i].modifiedDate.substring(0, response.data[i].modifiedDate.length-9);													
													 modifiedDate = moment(response.data[i].modifiedDate,"YYYY-MMM-DD").format("MMM DD, YYYY");
													}
													if(response.data[i].assignmentDate != null && response.data[i].assignmentDate != "" && response.data[i].assignmentDate != undefined){
														response.data[i].assignmentDate = response.data[i].assignmentDate.substring(0, response.data[i].assignmentDate.length-9);													
														assignmentDate = moment(response.data[i].assignmentDate,"YYYY-MMM-DD").format("MMM DD, YYYY");
														}
													var tagType = response.data[i].type
													if (response.data[i].type == "Asset Chrono" || response.data[i].type == "Asset NonChrono"|| response.data[i].type == "Materials")
														tagType = response.data[i].subType
														
														var peopleTagType = response.data[i].type;
														var tagStatus =response.data[i].status;
														if (peopleTagType == "Employee" || peopleTagType == "Visitor"|| peopleTagType == "Vendor" || peopleTagType == "Contractor"|| tagStatus == "Free" ||  tagStatus == "Blocked"
															||(tagStatus == "Lost" && (peopleTagType == "Employee" || peopleTagType == "Visitor" || peopleTagType == "Vendor" || peopleTagType == "Contractor")))
														{	
													
													$scope.tagData.push( {
													'tagId':  response.data[i].tagId ,
													'groupId':  response.data[i].groupId,
													'macId':  response.data[i].macId,
													'manufacturer':  response.data[i].manufacturer,
													'status':  response.data[i].status,
													'assignmentId':  response.data[i].assignmentId,
													'assignmentDate':assignmentDate,
													'fromTime':  fromTime,
													'toTime': toTime,
													'createdDate':  createdDate,
													'modifiedDate': modifiedDate,
													'major': response.data[i].major,
													'minor': response.data[i].minor,
													'type': tagType,
													'restart':response.data[i].restart
													});
													
														}
														
														
													}
												$scope.tagEmployeeAssignedDetails= [];
												for(var j=0; j< response.data.length; j++){
													if(response.data[j].type != undefined && response.data[j].type != '' && response.data[j].type !=null){
													if(response.data[j].type == "Employee"){
														$scope.tagEmployeeAssignedDetails.push(response.data[j].assignmentId);
													}
													}
												 }
												//console.log($scope.tagEmployeeAssignedDetails);
												/*
												 * if
												 * ($scope.tagEmployeeAssignedDetails.length ==
												 * 0)
												 * $scope.tagEmployeeAssignedDetails.push("No
												 * Tags Found");
												 */
												
												// $scope.checktagIdTable.length
												// = 0;
												$scope.fetchTagIdTable = [];
												
												for(var k=0; k< response.data.length; k++){
													var varTagIdRec={};
													if(response.data[k].tagId != undefined && response.data[k].tagId != '' && response.data[k].tagId !=null){
														if(response.data[k].type != undefined && response.data[k].type != '' && response.data[k].type !=null){
															if((response.data[k].type == "Visitor") || (response.data[k].type == "Vendor") || (response.data[k].type == "Contractor")){
																// $scope.checktagIdTable.push(response.data[k].tagId);
																varTagIdRec['id'] = response.data[k].tagId
																varTagIdRec['text'] = $scope.varTagIdTextAssigned + response.data[k].tagId + '<div>';
																
																$scope.fetchTagIdTable.push(varTagIdRec);
															}
														}
														if(response.data[k].status != undefined && response.data[k].status != '' && response.data[k].status !=null){
															if(response.data[k].status == "Free") {
																// $scope.checktagIdTable.push(response.data[k].tagId);
																varTagIdRec['id'] = response.data[k].tagId
																varTagIdRec['text'] = $scope.varTagIdTextFree + response.data[k].tagId + '<div>';
																$scope.fetchTagIdTable.push(varTagIdRec);
															}
														}
													}
												 }
												// if
												// ($scope.checktagIdTable.length
												// == 0)
												// $scope.checktagIdTable.push("No
												// Tags Found");
												if ($scope.fetchTagIdTable.length == 0){
													varTagIdRec['id'] = "NoTag";
													varTagIdRec['text'] = $scope.varTagIdTextFree + 'No Tags Found' + '<div>';
													
												}
												$('#checktagIdSelect').select2('destroy');
												$('#checktagIdSelect').select2({
													data: $scope.fetchTagIdTable,
												      templateResult: function (d) {
												    	  // $scope.fieldName.checktagId=$(d.text);
												    	  return $(d.text); 
												    	  },
												      templateSelection: function (d) { 
												    	  return $(d.text); 
												    	  },
												});
												$('#checktagIdSelect').trigger('change.select2'); 
												
												// $scope.fieldName.checktagId=$scope.checktagIdTable[0];
												// ----
												$scope.checktagIdEmpTable.length	= 0;
												for(var k=0; k< response.data.length; k++){
													if(response.data[k].tagId != undefined && response.data[k].tagId != '' && response.data[k].tagId !=null){
														if(response.data[k].status != undefined && response.data[k].status != '' && response.data[k].status !=null){
															if(response.data[k].status == "Free")
																$scope.checktagIdEmpTable.push(response.data[k].tagId);
														}
														// if(response.data[k].type
														// == "Employee" &&
														// response.data[k].status
														// == "Assigned")
														// $scope.checktagIdEmpTable.push(response.data[k].tagId);
														
													}
												 }
												if ($scope.checktagIdEmpTable.length == 0)
													$scope.checktagIdEmpTable.push("No Tags Found");
												$('#checktagIdSelectEmployee').select2('destroy');
												$('#checktagIdSelectEmployee').select2();
												$scope.fieldName.checktagIdEmp=$scope.checktagIdEmpTable[0];
												
												if ($scope.tagData.length == 0) {
													$scope.tagNoData = true;
													return;
												} else {
													$scope.tagNoData = false;
												}

												if ($scope.tagData.length > 10) {
													$scope.PaginationTab = true;
												} else {
													$scope.PaginationTab = false;
												}
												$scope.totalItems = $scope.tagData.length;

												$scope.getTotalPages();
												$scope.filterOption();

												$scope.currentPage = 1;
												if ($scope.tagData.length > $scope.numPerPage) {
													$scope.tagViewData = $scope.tagData
															.slice(0,
																	$scope.numPerPage);
													$scope.hasPrevious = true;
													$scope.isFirstPage = true;
													$scope.hasNext = false;
													$scope.isLastPage = false;
												} else {
													$scope.tagViewData = $scope.tagData;
												}
											}else{
												$scope.tagNoData = true;
											}
										},
										function error(response) {
											$rootScope
													.hideloading('#loadingBar');
											$rootScope.fnHttpError(response);
										});

					}

					// $scope.getTagData();

					// assign Tag

					$scope.assignTag = function() {
						
						var zoneName = "";

						selectedZones = selectedZones;

						if ($scope.authen.tagType == "Employee"
								|| $scope.authen.tagType == "workmetrial"
								|| $scope.authen.tagType == "asset_nonchrono"
								|| $scope.authen.tagType == "asset_chrono") {

							var assignTagParameter = {

								"macId" : $scope.checkTagMacId,
								"type" : $scope.authen.tagType,
								"peopleId" : $scope.authen.peopleId

							};
							

						} else if ($scope.authen.tagType == "Visitor"
								|| $scope.authen.tagType == "Vendor"
								|| $scope.authen.tagType == "Contractor") {
							$scope.treeViewZoneList = [];
							$scope.phoneNo = $scope.authen.countryCode
									+ $scope.authen.phoneNumber;
							$scope.contactPersonPhoneNo = $scope.authen.contactPersonCountryCode + $scope.authen.contactPersonPhoneNumber;

							for (var i = 0; i < selectedZones.length; i++) {
								if (selectedZones[i].id >= 0)
									$scope.treeViewZoneList
											.push(''+selectedZones[i].text);
							}
							//console.log($scope.treeViewZoneList);
							// $scope.authen.accessLimit =
							// $scope.selectedZoneIdList;
							var assignTagParameter = {

								"macId" : $scope.checkTagMacId,
								"type" : $scope.authen.tagType,
								"firstName" : $scope.authen.firstName,
								"lastName" : $scope.authen.lastName,
								"company" : $scope.authen.companyName,
								"contactNo" : $scope.phoneNo,
								"date" : $scope.authen.date,
								"fromTime" : $scope.authen.stime,
								"toTime" : $scope.authen.etime,
								"accessLimit" : $scope.treeViewZoneList,
								"contactPerson" : $scope.authen.contactPerson,
								"contactPersonNumber": $scope.contactPersonPhoneNo

							};
							var fname = document.getElementById("fname");
							if (fname.value.length == 0) {
								$scope.fnameError = true;
								$scope.fname_error_msg = "Please enter the first name";
								$timeout(function() {
									$scope.fnameError = false;
								}, 2000);
								return;
							}
							var lname = document.getElementById("lname");
							if (lname.value.length == 0) {
								 $scope.lnameError = true;
								$scope.lname_error_msg = "Please enter the last name";
								$timeout(function() {
									$scope.lnameError = false;
								}, 2000);
								return;
							}
							var phnoErr = document.getElementById("phoneNumber");
							if (phnoErr.value.length == 0) {
								$scope.phNoError = true;
								$scope.phNo_error_msg = "Please enter phone number";
								$timeout(function() {
									$scope.phNoError = false;
								}, 2000);
								return;
							}
							if(phnoErr.value.length > 0 && phnoErr.value.length < 10 ){

								$scope.phNoError = true;
								$scope.phNo_error_msg = "Please enter valid 10 digit phone number";
								$timeout(function() {
									$scope.phNoError = false;
								}, 2000);
								return;
							
							}
							var company = document.getElementById("id_companyName");
							if (company.value.length == 0) {
								$scope.companyNameError = true;
								$scope.companyName_error_msg = "Please enter company name";
								$timeout(function() {
									$scope.companyNameError = false;
								}, 2000);
								return;
							}
							var contactPerson = document.getElementById("id_contactPerson");
							if (contactPerson.value.length == 0) {
								$scope.contactPersonError = true;
								$scope.contactPersonError_error_msg = "Please enter contact person name";
								$timeout(function() {
									$scope.contactPersonError = false;
								}, 2000);
								return;
							}
							var contactPersonPhno = document.getElementById("id_contactPersonPhoneNumber");
							if (contactPersonPhno.value.length > 0 && contactPersonPhno.value.length < 10 ) {
								$scope.contactPersonPhNoError = true;
								$scope.contactPersonPhNoError_error_msg = "Please enter contact person phone number";
								$timeout(function() {
									$scope.contactPersonPhNoError = false;
								}, 2000);
								return;
							}
							var dateErr = document.getElementById("id_date");
							if (dateErr.value.length == 0 ) {
								$scope.dateError = true;
								$scope.dateError_error_msg = "Please enter date";
								$timeout(function() {
									$scope.dateError= false;
								}, 2000);
								return;
							}
							var stimeErr = document.getElementById("stime");
							if (stimeErr.value.length == 0 ) {
								$scope.stimeError = true;
								$scope.stimeError_error_msg = "Please enter start time";
								$timeout(function() {
									$scope.stimeError= false;
								}, 2000);
								return;
							}
							var etimeErr = document.getElementById("etime");
							if (etimeErr.value.length == 0 ) {
								$scope.etimeError = true;
								$scope.etimeError_error_msg = "Please enter end time";
								$timeout(function() {
									$scope.etimeError= false;
								}, 2000);
								return;
							}
							if(stimeErr.value >= etimeErr.value){
								$rootScope.showAlertDialog("Please select end time greater than start time");
								return
							}
							if($scope.treeViewZoneList.length ==0){
								$rootScope.showAlertDialog("Please Select Zones");
								return
							}
							
							
						}
						 if (!$rootScope.checkNetconnection()) { 
							 	$rootScope.alertDialogChecknet();
								return; 
								}
						$rootScope.showloading('#loadingBar');
						$scope.promise = $http.post('./allocation',
								assignTagParameter).then(function(result) {
									 $rootScope.hideloading('#loadingBar');
							if (result.data.status == "success") {
								$rootScope.showAlertDialog(result.data.message);
								if ($scope.authen.tagType == "Employee"){
									
 									
									var index = $scope.checktagIdEmpTable.indexOf($scope.fieldName.checktagIdEmp);
									if (index !== -1) {
										$scope.checktagIdEmpTable.splice(index, 1);
										$scope.assignedEmployeeDetailsEmpCombo();
										//console.log($scope.peopleIdTable);
										// $scope.checktagIdEmpVal =
										// $scope.checktagIdEmpTable
									}
									var index = $scope.peopleIdTable.indexOf($scope.authen.peopleId);
									if (index !== -1) {
										$scope.peopleIdTable.splice(index, 1);
										
									}
									
									/*
									 * $scope.displayTagPeopleEmployeeAssignScreen();
									 * $timeout(function() {
									 * $('#checktagIdSelectEmployee').select2('destroy');
									 * $('#checktagIdSelectEmployee').select2(); },
									 * 300); $scope.$apply();
									 */
									/*
									 * $scope.getTagData();
									 */
									
								}
								else if ($scope.authen.tagType == "Visitor"
									|| $scope.authen.tagType == "Vendor"
									|| $scope.authen.tagType == "Contractor") {
									// iterate over each element in the array
								for (var i = 0; i < $scope.fetchTagIdTable.length; i++){
								  // look for the entry with a matching `code`
									// value
								  if ($scope.fetchTagIdTable[i].id == $scope.authen.tagId){
									  $scope.fetchTagIdTable[i].text =  $scope.varTagIdTextAssigned + $scope.authen.tagId + '<div>';
									  break;
								  }
								}
								}
								$scope.cancelTagAssign();
								$scope.treeViewZoneList.length=0;
							

							} else if (result.data.status == "error") {
								$rootScope.showAlertDialog(result.data.message);
							}

						}, function(error) {
							 $rootScope.hideloading('#loadingBar');
							 $rootScope.fnHttpError(error);
							
						});
					}

					$scope.rowHighilited = function(row) {
						$scope.selectedRow = row;
						var a = new Array();
						var b = new Array();
						// $scope.authen = $scope.tagData[row];
						 $scope.authen = JSON.parse(JSON.stringify($scope.tagViewData[row]));
						 $scope.statusEdit = true; //kumar 21-Jan-21 Enable Edit button only when it is true
						if ($scope.authen.status == "Free") {
							
							$scope.names = [ "Free", "Lost", "Not Working",
									"Expired" ];
						} else if ($scope.authen.status == "Expired") {
							// $scope.showDetails = true;
							$scope.statusEdit = false;
							$scope.names = [ "Expired" ];
							// $scope.names = [ "Expired", "Lost", "Not
							// Working", "Blocked" ];
						} else if ($scope.authen.status == "Overdue") {

							$scope.showDetails = true;

							$scope.names = [ "Overdue", "Lost", "Not Working",
									"Blocked" ];
						} else if ($scope.authen.status == "Blocked") {

							$scope.showDetails = true;
							/*
							 * $scope.names=[]; $scope.names[0] =
							 * $scope.authen.status;
							 */
							$scope.names = [ "Blocked", "Lost", "Not Working",
									"Assigned" ];

						} else if ($scope.authen.status == "Lost"
								&& $scope.authen.restart == true) {
							// $scope.statusEdit = true;
							$scope.showDetails = true;
							$scope.names = [ "Lost", "Free" ];
						} else if ($scope.authen.status == "Lost") {
							$scope.statusEdit = false;
							$scope.names = [ "Lost" ];
						} else if ($scope.authen.status == "Not Working") {
							$scope.statusEdit = false;
							$scope.names = [ "Not Working" ];
						} else if ($scope.authen.status == "Assigned") {
																					
							if ($scope.authen.type == 'Visitor' ||
									$scope.authen.type == 'Contractor' ||
									$scope.authen.type == 'Vendor'){
							if ($scope.authen.assignmentDate != undefined
								&& $scope.authen.assignmentDate != '' && $scope.authen.assignmentDate != null) {
								var tagAssignmentDate =  moment($scope.authen.assignmentDate);
								var currentDay = moment(new Date()).format('MMM DD, YYYY');
								var diff = moment(currentDay) - tagAssignmentDate;
								if (diff == 0){ // today
									 var today = new Date();
									 var currentTime = ("0" + today.getHours()).slice(-2)
										+ ":"
										+ ("0" + today.getMinutes()).slice(-2);
									 var toTime =$scope.authen.toTime.substring(0,$scope.authen.toTime.length-3);
									 var fromTime = $scope.authen.fromTime;
									  a = toTime.split(':');
									  b = currentTime.split(':');
									 var toTimeMinutes = (+a[0]) * 60 + (+a[1]);
									 var currentTimeMinutes = (+b[0]) * 60 + (+b[1]);
									 							 
									 
									 
									 if (currentTimeMinutes > toTimeMinutes)
																												
									 $scope.names = [ "Assigned", "Lost", "Not Working","Blocked", "Expired","Overdue" ];
									 else
									 $scope.names = [ "Assigned", "Lost", "Not Working","Blocked", "Expired" ];
								}else if (diff > 0){ // assigned date is
														// previous date
									$scope.names = [ "Assigned", "Lost", "Not Working",
										"Blocked", "Expired","Overdue"  ];
								}
								else{
									$scope.names = [ "Assigned", "Lost", "Not Working",
										"Blocked", "Expired"  ];
								}
							}
							}else {
								$scope.names = [ "Assigned", "Lost", "Not Working",	"Blocked", "Expired"  ];
							}
						}

					}

					$scope.updateTag = function() {
						 
						if ($scope.names[0] == $scope.authen.status) {
							$rootScope.showAlertDialog("Please select other status");
							return;
						}
							
						var updateTagParameter = {
							"tagId" : $scope.authen.tagId,
							"status" : $scope.authen.status,
							"type" : 'tagPeople'
						};
						 if (!$rootScope.checkNetconnection()) { 
							 	$rootScope.alertDialogChecknet();
								return; 
								}
						$rootScope.showloading('#loadingBar');
						$scope.promise = $http.post('./updateTagStatus', updateTagParameter)
								.then(function(result) {
									 $rootScope.hideloading('#loadingBar');
									 if ($scope.authen.status == "Lost"){ //kumar 21-Jan-21
											$scope.authen.restart = false;
										}
										else if ($scope.authen.status == "Free"){ //kumar 21-Jan-21
											$scope.authen.restart = true;
											$scope.authen.type = "";
											$scope.authen.assignmentId ="";
											$scope.authen.assignmentDate="";
											$scope.authen.modifiedDate=moment(new Date()).format('MMM DD, YYYY');
										}
										$scope.tagViewData[$scope.selectedRow]= $scope.authen;
									if (result.data.status == "success") {
										$rootScope.showAlertDialog(result.data.message); 
										
										$scope.cancelTag();

									} else if (result.data.status == "error") {
										$rootScope.showAlertDialog(result.data.message);  
									}

								}, function(error) {
									 $rootScope.hideloading('#loadingBar');
									 $rootScope.fnHttpError(error);
									
								});
					}
					$scope.cancelTag = function() {

						$scope.authen = {};
						$scope.selectedRow = -1;

						$scope.tagPeopleEditScreen = true;
						$scope.tagPeopleEditUpdateScreen = false;
						$scope.searchButton = true;
					}
					$scope.cancelEdit = function() {

						$scope.selectedRow = -1;
						$scope.searchButton = true;
					}

					$scope.cancelReturnTag = function() {
						// $('#checktagIdSelect').val("");
						$scope.tagPeopleReturnScreen = false;
					     if ($scope.authen.tagType == "Employee") {
								$scope.fetchScreenEmployee = true;
								$('#checktagIdSelectEmployee').select2('destroy');
								$('#checktagIdSelectEmployee').select2();
								$('#id_peopleIdAssignTag').select2('destroy');
								$('#id_peopleIdAssignTag').select2(); 
								$scope.fieldName.checktagIdEmp=$scope.checktagIdEmpTable[0];
						     }else{
								$scope.fetchScreen = true;
								$("#checktagIdSelect").empty();
								$('#checktagIdSelect').select2('destroy');
								
									$('#checktagIdSelect').select2({
										data: $scope.fetchTagIdTable,
									      templateResult: function (d) { return $(d.text); },
									      templateSelection: function (d) { return $(d.text); },
									});
									$('#checktagIdSelect').trigger('change.select2'); 
								
								
						     // $scope.$applyAsync();
						     }
						
					
						// $scope.fieldName.checktagId=$scope.checktagIdTable[0];
					}
					$scope.returnTag = function() {
						
						var returnTagParameter = {

							"macId" : $scope.checkTagMacId,
							"type" : $scope.tagReturnTagType,
							"peopleId" : $scope.tagReturnPeopleId
						};
						if (!$rootScope.checkNetconnection()) { 
						 	$rootScope.alertDialogChecknet();
							return; 
							}
						 $rootScope.showloading('#loadingBar');
						$scope.promise = $http.post('./returnTag',
								returnTagParameter).then(function(result) {
									 $rootScope.hideloading('#loadingBar');
							if (result.data.status == "success") {

								$rootScope.showAlertDialog(result.data.message);
								/*
								 * if ($scope.authen.tagType == "Employee"){
								 * $scope.getTagData(); }
								 */
								if ($scope.authen.tagType == "Visitor"
									|| $scope.authen.tagType == "Vendor"
									|| $scope.authen.tagType == "Contractor") {
								
								
								 
								// iterate over each element in the array
								for (var i = 0; i < $scope.fetchTagIdTable.length; i++){
								  // look for the entry with a matching `code`
									// value
								  if ($scope.fetchTagIdTable[i].id == $scope.tagReturnTagID){
									  $scope.fetchTagIdTable[i].text =  $scope.varTagIdTextFree + $scope.tagReturnTagID + '<div>';
									  break;
								  }
								}
								}
								$scope.cancelReturnTag();
							} else if (result.data.status == "error") {
								$rootScope.showAlertDialog(result.data.message);
							}

						}, function(error) {
							 $rootScope.hideloading('#loadingBar');
							 $rootScope.fnHttpError(error);
							 
						});

					}

					$scope.fetchTag = function() {
						 
						 /*
							 * var fetchTag =
							 * document.getElementById("checktagIdSelect"); if
							 * (fetchTag.value.length == 0) {
							 * $rootScope.hideloading('#loadingBar');
							 * $scope.fetchTagError = true;
							 * $scope.fetchTag_error_msg = "Please enter Tag ID ";
							 * $timeout(function() { $scope.fetchTagError =
							 * false;
							 *  }, 2000);
							 * 
							 * return; }
							 */
						 var varCheckTagID = '';
						 if($scope.fetchScreenEmployee)
							 varCheckTagID =  $('#checktagIdSelectEmployee').val();
						 else
							 varCheckTagID =  $('#checktagIdSelect').val();
						
						if (varCheckTagID==undefined || varCheckTagID==null || varCheckTagID=='' ) {
							$rootScope.alertErrorDialog('Please Enter Tag ID ');
							return;
						}
						
						if (varCheckTagID == "No Tags Found"){
							$rootScope.alertErrorDialog('Tags are not available');
							return;
						}
						if ($scope.authen.tagType == "Employee") {
							varCheckTagID =  $scope.fieldName.checktagIdEmp;
							$('#id_peopleIdAssignTag').select2('destroy');
							$('#id_peopleIdAssignTag').select2(); 
							$scope.authen.peopleId = $scope.peopleIdTable[0];
							$scope.assignedEmployeeDetailsEmpCombo ();
						}
						
						var checkTagParameter = {
							"tagId" : varCheckTagID
						};
						 if (!$rootScope.checkNetconnection()) { 
							 	$rootScope.alertDialogChecknet();
								return; }
						$rootScope.showloading('#loadingBar');
						$scope.promise = $http
								.post('./checkTag', checkTagParameter)
								.then(
										function(result) {

											 $rootScope.hideloading('#loadingBar');
											if (result.data.status == "success") {

												if (result.data.tagStatus == "free") {
													$scope.checkTagMacId = result.data.tag.macId;
													var tagId = result.data.tag.tagId;
													$("#id_tagIdAssignTag").val(tagId);
													$scope.assignScreen = true;
													$scope.fetchScreen = false;
													$scope.fetchScreenEmployee= false;
													$scope.tagPeopleReturnScreen = false;
													$scope.authen.tagId = result.data.tag.tagId;
													
													// $scope.authen.tagType =
													// "Employee";
													$('#datetimepicker4').datetimepicker({
														format : 'YYYY-MMM-DD',
														minDate : new Date(),
													});
													$scope.assignTagType();
												} else if ((result.data.tagStatus == "Assigned" )
														|| (result.data.tagStatus == "assigned"))
													{
													if(((result.data.tag.type == "Visitor")||(result.data.tag.type == "Vendor")
														||(result.data.tag.type == "Contractor")||(result.data.tag.type == "Employee"))){
														if((result.data.tag.type == "Visitor")||(result.data.tag.type == "Vendor")
																||(result.data.tag.type == "Contractor")	){
															$scope.show_accessLimit_ReturnScreen = true;
															$scope.show_fname_returnScreen = true;
															$scope.show_lname_returnScreen = true;
															$scope.show_date_returnScreen = true;
															$scope.show_stime_returnScreen = true;
															$scope.show_etime_returnScreen = true;
															var dateReturn = moment( result.data.tag.assignmentDate,"YYYY-MMM-DD HH:mm:ss").format("MMM DD, YYYY");
															
															$scope.firstNameReturn = result.data.firstName;
															$scope.lastNameReturn = result.data.lastName;
															$scope.date_returnScreen = dateReturn;
															$scope.stime_returnScreen = result.data.fromTime;
															$scope.etime_returnScreen = result.data.toTime;
															//var accessLimitRetScreen1 = new Array();
															var accessLimitRetScreen = new Array();
															/*for(var i=0; i< $scope.locationList.length;i++ )
																for(var j=0;j<result.data.accessLimit.length;j++)
																{
																if($scope.locationList[i].text== result.data.accessLimit[j])
																	accessLimitRetScreen1.push($scope.locationList[i].id);
																}*/
															for(var i=0; i< $scope.locationList.length;i++ ){
																for(var j=0;j<result.data.accessLimit.length;j++)
																{
																if($scope.locationList[i].text== result.data.accessLimit[j])
																	accessLimitRetScreen.push($scope.locationList[i].text);
																}
															}
															if (accessLimitRetScreen.length == 0){
																accessLimitRetScreen.push("Zone are not assigned");
															}
															//$scope.accessLimit_returnScreen1 = accessLimitRetScreen1;
															$scope.accessLimit_returnScreen = accessLimitRetScreen;
															//$scope.fnReturnTreeWithZoneNme();
															
														}else{
															// to show/hide tree
															// view in return
															// screen
															$scope.show_accessLimit_ReturnScreen = false;														
																
															$scope.show_fname_returnScreen = false;
															$scope.show_lname_returnScreen = false;
															$scope.show_date_returnScreen = false;
															$scope.show_stime_returnScreen = false;
															$scope.show_etime_returnScreen = false;
														}
													$scope.checkTagMacId = result.data.tag.macId;
													$scope.checkTagTagId = result.data.tag.tagId;
													$scope.checkTagType = result.data.tag.type;
													$scope.checkTagPeopleId = result.data.tag.assignmentId;

													$scope.tagPeopleReturnScreen = true;
													$scope.fetchScreen = false;
													$scope.fetchScreenEmployee = false;
													
													$scope.tagReturnTagID = $scope.checkTagTagId;
													$scope.tagReturnTagType = $scope.checkTagType;
													$scope.tagReturnPeopleId = $scope.checkTagPeopleId;
													
													$scope.peopleLabelName = result.data.tag.type + " ID";
													
													}else {
														$rootScope.showAlertDialog(result.data.message);
														
													}

												} else {
													$rootScope.showAlertDialog(result.data.message);
												}

											} else if (result.data.status == "error") {
												$rootScope.showAlertDialog(result.data.message);
											}
											
										},
										function(error) {
											 $rootScope.hideloading('#loadingBar');
											 $rootScope.fnHttpError(error);
											 
										});


					}

					// pagination button
					$scope.currentPage = 1;

					$scope.currentPage = 1;
					$scope.numPerPage = 10;
					$scope.maxSize = 3;
					$scope.totalItems = 0;
					$scope.tagDataFilterList = [];
					$scope.tagViewData = [];
					$scope
							.$watch(
									'currentPage + numPerPage',
									function() {
										var begin = (($scope.currentPage - 1) * $scope.numPerPage), end = begin
												+ $scope.numPerPage;
										if ($scope.searchText == '') // kumar
											// 02/July
											// for
											// filter
											// text
											$scope.tagViewData = $scope.tagData
													.slice(begin, end);
										else
											$scope.tagViewData = $scope.tagDataFilterList
													.slice(begin, end);
									});

					$scope.currentPage = 1;

					$scope.maxSize = 3;

					$scope.numPerPage = 10;
					$scope.totalItems = 0;
					$scope.totalPages = 0;

					$scope.hasNext = true;
					$scope.hasPrevious = true;
					$scope.isFirstPage = true;
					$scope.isLastPage = true;
					$scope.serial = 1;

					$scope
							.$watch(
									"currentPage",
									function(newVal, oldVal) {
										if (newVal !== oldVal) {

											if (newVal > $scope.totalPages) {
												$scope.currentPage = $scope.totalPages;
											} else {
												$scope.currentPage = newVal;
											}

											$scope.serial = (($scope.currentPage - 1) * $scope.numPerPage) + 1;

										}
									});

					$scope.enablePageButtons = function() {
						$scope.serial = (($scope.currentPage - 1) * $scope.numPerPage) + 1;
						$scope.selectedRow = -1;
						if ($scope.totalPages <= 1) {
							$scope.hasPrevious = true;
							$scope.isFirstPage = true;
							$scope.hasNext = true;
							$scope.isLastPage = true;

						} else if ($scope.totalPages > 1
								&& $scope.currentPage == $scope.totalPages) {

							$scope.hasPrevious = false;
							$scope.isFirstPage = false;
							$scope.hasNext = true;
							$scope.isLastPage = true;
						} else if ($scope.currentPage != 1
								&& $scope.currentPage < $scope.totalPages) {

							$scope.hasPrevious = false;
							$scope.isFirstPage = false;
							$scope.hasNext = false;
							$scope.isLastPage = false;

						} else {
							$scope.hasPrevious = true;
							$scope.isFirstPage = true;
							$scope.hasNext = false;
							$scope.isLastPage = false;
						}
					};

					$scope.getTotalPages = function() {

						if (($scope.totalItems % $scope.numPerPage) > 0)
							$scope.totalPages = parseInt($scope.totalItems
									/ $scope.numPerPage) + 1;
						else
							$scope.totalPages = parseInt($scope.totalItems
									/ $scope.numPerPage);

						$scope.enablePageButtons();

					};

					$scope.goFirstPage = function() {
						$scope.currentPage = 1;
						$scope.enablePageButtons();
					};

					$scope.goPreviousPage = function() {
						if ($scope.currentPage > 1)
							$scope.currentPage = $scope.currentPage - 1;
						$scope.enablePageButtons();
					};

					$scope.goNextPage = function() {
						if ($scope.currentPage <= $scope.totalPages)
							$scope.currentPage = $scope.currentPage + 1;
						$scope.enablePageButtons();
					};

					$scope.goLastPage = function() {
						$scope.currentPage = $scope.totalPages;
						$scope.enablePageButtons();
					};

		
					$scope.displayTagPeopleAssignScreen = function() {
						// $("#checktagIdSelect").val("");
						// $scope.fieldName.checktagId=$scope.checktagIdTable[0];
						//$scope.tagPeopleViewScreen = false;
						$scope.tagPeopleEditScreen = false;
						$scope.tagPeopleEditUpdateScreen = false;
						
						$scope.fetchScreen = true;
						$scope.fetchScreenEmployee= false;
						$scope.assignScreen = false;
						$scope.tagPeopleReturnScreen = false; 
						$("#idTagPeopleView").removeClass("active");
						$("#idTagPeopleAssign").addClass("active");
						$("#idTagPeopleEdit").removeClass("active");
						
						$("#idTagPeopleReturn").removeClass("active");
						$("#idTagPeopleEmployeeAssign").removeClass("active");
						// $scope.showEmpAssignBtn = false;
						$scope.showAssignReturnBtn = true;
						$scope.searchButton = false;
						$scope.authen.tagType = $scope.visitorType[0];
						$scope.tagTypeReadOnly =false;
						$scope.tagTypeEmployee = false;
						$scope.tagTypeVisitor =true;
						/*
						 * $('#checktagIdSelect').select2('destroy');
						 * $('#checktagIdSelect').select2({ data:
						 * $scope.fetchTagIdTable, templateResult: function (d) {
						 * return $(d.text); }, templateSelection: function (d) {
						 * return $(d.text); }, });
						 */
						// $scope.authen.peopleId = $scope.peopleIdTable[0];
						// $scope.fieldName.checktagId=$scope.checktagIdTable[0];
						$scope.getTagData();
							
					};
					$scope.displayTagPeopleEmployeeAssignScreen = function() {
						//$scope.tagPeopleViewScreen = false;
						$scope.tagPeopleEditScreen = false;
						$scope.tagPeopleEditUpdateScreen = false;
						
						$scope.fetchScreen = false;
						$scope.assignScreen = false;
						$scope.tagPeopleReturnScreen = false;
						$scope.fetchScreenEmployee = true; 
						$scope.searchButton = false;
						// $scope.showEmpAssignBtn = true;
						$scope.tagTypeReadOnly =true;
						$scope.tagTypeEmployee = true;
						$scope.tagTypeVisitor =false;
						$("#idTagPeopleView").removeClass("active");
						$("#idTagPeopleAssign").removeClass("active");
						$("#idTagPeopleEdit").removeClass("active");
						
						$("#idTagPeopleReturn").removeClass("active");
						$("#idTagPeopleEmployeeAssign").addClass("active");
						
						$scope.authen.tagType = "Employee";
						$('#checktagIdSelectEmployee').select2('destroy');
						$('#checktagIdSelectEmployee').select2(); 
						$('#id_peopleIdAssignTag').select2('destroy');
						$('#id_peopleIdAssignTag').select2(); 
						
						$scope.authen.peopleId = $scope.peopleIdTable[0];
						$scope.fieldName.checktagIdEmp=$scope.checktagIdEmpTable[0];

						$scope.getEmpData();
						$scope.getTagData();
						$scope.assignedEmployeeDetailsEmpCombo();
						
					};
					$scope.displayTagPeopleEditScreen = function() {
						$scope.tagDataFilterList = [];
						$scope.tagViewData = [];
						$scope.searchText = "";

						$scope.selectedRow = -1;
						$scope.currentPage = 1;
						$scope.searchButton = true;
						//$scope.tagPeopleViewScreen = false;
						$scope.tagPeopleEditScreen = true;
						$scope.tagPeopleEditUpdateScreen = false;
						
						$scope.fetchScreen = false;
						$scope.assignScreen = false;
						$scope.tagPeopleReturnScreen = false;
						$scope.fetchScreenEmployee = false;
						// $scope.showEmpAssignBtn = false;
						$("#idTagPeopleView").removeClass("active");
						$("#idTagPeopleAssign").removeClass("active");
						$("#idTagPeopleEdit").addClass("active");
						
						$("#idTagPeopleReturn").removeClass("active");
						$("#idTagPeopleEmployeeAssign").removeClass("active");
						$scope.getTagData();
						
					};
					

					$scope.tagAssignBackToView = function() {
						var formID = document.getElementById("assignTagForm")
								.reset();

						$scope.authen = {};
						$scope.selectedRow = -1;
						$scope.searchButton = true;
						$scope.fetchScreen = false;
						$scope.assignScreen = false;
						$scope.tagPeopleReturnScreen = false;

					}
					$scope.dateTimeReset = function() {
						var today = new Date();
						$('#datetimepicker4').datetimepicker({
							format : 'YYYY-MMM-DD',
							minDate : new Date(),
						});

						$('#stimepicker').datetimepicker({
							format : 'HH:mm',
							minDate : new Date(),
							useCurrent : true
						});
						$('#etimepicker').datetimepicker({

							format : 'HH:mm',
							minDate : new Date(),

						});

						var startTime = ("0" + today.getHours()).slice(-2)
								+ ":"
								+ ("0" + today.getMinutes()).slice(-2);
						var endTime = ("0" + (today.getHours() + 2))
								.slice(-2)
								+ ":"
								+ ("0" + today.getMinutes()).slice(-2);
						$("#stime").val(startTime);
						$("#etime").val(endTime);
						$scope.authen.date = moment(new Date()).format('YYYY-MMM-DD');
						$("#datetimepicker4").val(moment(new Date()).format('YYYY-MMM-DD'));
						$scope.authen.stime = startTime;
						$scope.authen.etime = endTime;
						$("#etimepicker").val(endTime);

					}
					$scope.cancelTagAssign = function() {
						
						// $('#checktagIdSelect').val("");
						// $scope.checktagIdEmpTable=[];
						$scope.tagPeopleReturnScreen = false; 
						$scope.assignScreen = false;
						$scope.checkTagMacId ="";
						// $scope.authen.tagType = "",
						 $scope.authen.firstName = "",
						 $scope.authen.lastName = "";
						 $scope.authen.companyName = "",
						 $scope.authen.contactPerson ="";					
						 
						 $scope.treeViewZoneList =[],
						 $scope.authen.countryCode = $rootScope.countryCodes[0];
					     $scope.authen.phoneNumber ="";
					     $scope.authen.contactPersonCountryCode  = $rootScope.countryCodes[0];
					     $scope.authen.contactPersonPhoneNumber ="";
					     $scope.dateTimeReset();
					     if ($scope.authen.tagType == "Employee") {
							$scope.fetchScreenEmployee = true;
							$('#checktagIdSelectEmployee').select2('destroy');
							$('#checktagIdSelectEmployee').select2(); 
							$('#id_peopleIdAssignTag').select2('destroy');
							$('#id_peopleIdAssignTag').select2(); 
							
							$scope.authen.peopleId = $scope.peopleIdTable[0];
							$scope.fieldName.checktagIdEmp=$scope.checktagIdEmpTable[0];
					     }else{
							$scope.fetchScreen = true;
							$("#checktagIdSelect").empty();
							$('#checktagIdSelect').select2('destroy');
							$('#checktagIdSelect').select2({
								data: $scope.fetchTagIdTable,
							      templateResult: function (d) { return $(d.text); },
							      templateSelection: function (d) { return $(d.text); },
							});
							$('#checktagIdSelect').trigger('change.select2'); 
					     }
					     $scope.$applyAsync(); 
						// $scope.fieldName.checktagId=$scope.checktagIdTable[0];
						// $('#checktagIdSelect').val($scope.fieldName.checktagId);
						// $("#checktagIdSelect").val($scope.fieldName.checktagId);
						// $("#checktagIdSelect").attr("title",$scope.fieldName.checktagId);
						// $('#checktagIdSelect').select2().trigger('change');
						

					}
					
					// to display enabled sub menus
					if ($scope.user.capabilityDetails != undefined
							&& $scope.user.capabilityDetails != null) {
						/*
						 * if ($scope.user.capabilityDetails.PTVA != 0) {
						 * $scope.displayTagPeopleViewScreen(); }
						 */
						if ($scope.user.capabilityDetails.PTAR != 0) {
							$scope.displayTagPeopleAssignScreen();
						}
						
						if ($scope.user.capabilityDetails.PTAR != 1 &&
							$scope.user.capabilityDetails.PTEA != 1) {
							$scope.displayTagPeopleEditScreen();
						}
						if ($scope.user.capabilityDetails.PTAR != 1 /*
																	 * &&
																	 * $scope.user.capabilityDetails.PTED !=
																	 * 1
																	 */) {
								$scope.displayTagPeopleEmployeeAssignScreen();
							}
						if ( /*
								 * $scope.user.capabilityDetails.PTED == 1 &&
								 */$scope.user.capabilityDetails.PTEA == 1
						&&  $scope.user.capabilityDetails.PTAR != 1) {
					return;
				} else {
					$scope.getZoneDetails();							
				}
						
					}
					
					
				});
