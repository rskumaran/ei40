app
		.controller(
				'configurationTagController',
				function($scope, $http, $window, $rootScope, $location,
						$timeout) {
					$scope.user = JSON.parse($window.sessionStorage
							.getItem("user"));
					if ($scope.user == null) {
						$location.path('/login');
						return;
					}

					$scope.checkTagMacId = {};
					$scope.checkTagTagId = {};
					$scope.tagRenameData = [];
					$scope.assetChronoIdTable = [];
					$scope.assetNonChronoIdTable = [];
					$scope.assetIdVehicleTable = [];
					$scope.checktagIdTable = [];
					$scope.checkTagType = {};
					$scope.checkTagPeopleId = {};
					$scope.tagData = new Array();
					$scope.tagViewData = new Array();
					$scope.tagDataFilterList = new Array();
					$scope.searchText = "";
					$scope.showTagViewAllScreen = true;
					$scope.searchButton = true;
					// $scope.showBatteryInfo = true;

					$scope.assetIdText = "Asset ID";
					$('.select2').select2(); // searchable dropdown in fetch
					// screen intialization

					if ($scope.authen == undefined) {
						$scope.authen = {};
					}
					if ($scope.fieldName == undefined) {
						$scope.fieldName = {};
					}

					// to enable/disable sub menus
					$scope.user = JSON.parse($window.sessionStorage
							.getItem("user"));
					// if ($scope.user != null) {
					// $rootScope.$broadcast('updatemenu', $scope.user.role);
					if ($scope.user.capabilityDetails != undefined
							&& $scope.user.capabilityDetails != null) {
						if ($scope.user.capabilityDetails.CTVA == 1) {
							$scope.showViewAllBtn = true;
						} else {
							$scope.showViewAllBtn = false;
						}

						if ($scope.user.capabilityDetails.CTRN == 1) {
							$scope.showRenameBtn = true;
						} else {
							$scope.showRenameBtn = false;
						}
						if ($scope.user.capabilityDetails.CTBI == 1) {
							$scope.showBatteryInfo = true;
						} else {
							$scope.showBatteryInfo = false;
						}

					}

					// input field validation
					$("#id_currentTag, #id_newTagId").keypress(
							function(e) {
								// if the letter is not alphabets then display
								// error
								// and don't type anything
								$return = ((e.which > 64) && (e.which < 91))
										|| ((e.which > 96) && (e.which < 123))
										|| (e.which == 8) || (e.which == 32)
										|| (e.which >= 48 && e.which <= 57)
								if (!$return) {

									return false;
								}
							});
					
					$scope.trackSingleTag = function() {
						if ($scope.trackMacId != undefined
								&& $scope.trackMacId != null
								&& $scope.trackMacId != '') {
							
							$rootScope.trackingMacId = $scope.trackMacId;
							$rootScope.showFullView();
							var currentMenuName = $window.sessionStorage.getItem("menuName");
							if ($scope.user.role == "DemoUser")
							{
								$window.sessionStorage.setItem("menuName", "shopFloorAnalysis");
								$window.sessionStorage.setItem("demoMenuName", "assetLiveView");
								$location.path('/shopFloorAnalysis');
							}else{
								$window.sessionStorage.setItem("menuName", "live");
								$location.path('/live');
							}
							$rootScope.$broadcast('sourcePreviousScreen', currentMenuName);
						} else {
							toastr.error('No tags to track.', "EI4.0");
						}
					}

					$scope.rowHighilited = function(row) {
						$scope.selectedRow = row;
						$scope.showLocateBtn = true;
						$scope.trackMacId = $scope.tagBatteryInfoViewData[row].macId;
					}
					$scope.cancelLocate = function() {
						$scope.selectedRow = -1;
						$scope.trackMacId = '';
					}
					$scope.searchOption = function() {
						$scope.filterOption();
					}

					$scope.filterOption = function() {

						if (!$scope.showBatteryInfoScreen) {

							$scope.tagViewData = [];
							$scope.tagDataFilterList = [];

							for (var i = 0; i < $scope.tagData.length; i++) {

								if ($scope.searchText == '') {
									$scope.tagDataFilterList
											.push($scope.tagData[i]);
								} else {

									var searchKey = $scope.searchText
											.toLowerCase();
									var tagInfo = $scope.tagData[i];
									if (getStringValue(tagInfo.tagId).includes(
											searchKey)
											|| getStringValue(tagInfo.groupId)
													.includes(searchKey)
											|| getStringValue(tagInfo.macId)
													.includes(searchKey)
											|| getStringValue(
													tagInfo.manufacturer)
													.includes(searchKey)
											|| getStringValue(tagInfo.status)
													.includes(searchKey)
											|| getStringValue(tagInfo.type)
													.includes(searchKey)
											|| getStringValue(
													tagInfo.assignmentId)
													.includes(searchKey)
											|| getStringValue(
													tagInfo.assignmentTime)
													.includes(searchKey)
											|| getStringValue(
													tagInfo.createdDate)
													.includes(searchKey)
											|| getStringValue(
													tagInfo.modifiedDate)
													.includes(searchKey)) {
										$scope.tagDataFilterList
												.push($scope.tagData[i]);

									}

								}

							}

							$scope.totalItems = $scope.tagDataFilterList.length;
							$scope.currentPage = 1;
							$scope.getTotalPages();

							if ($scope.tagData.length == 0
									|| $scope.tagDataFilterList.length == 0) {
								$scope.tagNoData = true;

							} else {

								$scope.tagNoData = false;

								if ($scope.tagDataFilterList.length > $scope.numPerPage) {
									$scope.tagViewData = $scope.tagDataFilterList
											.slice(0, $scope.numPerPage);
									$scope.PaginationTab = true;
								} else {
									$scope.tagViewData = $scope.tagDataFilterList;
									$scope.PaginationTab = false;
								}

							}

						} else {
							$scope.tagBatteryInfoViewData = [];
							$scope.tagBatteryInfoFilterList = [];

							for (var i = 0; i < $scope.tagBatteryInfoData.length; i++) {

								if ($scope.searchText == '') {
									$scope.tagBatteryInfoFilterList
											.push($scope.tagBatteryInfoData[i]);
								} else {

									var searchKey = $scope.searchText
											.toLowerCase();
									var tagBatteryInfo = $scope.tagBatteryInfoData[i];

									if (getStringValue(tagBatteryInfo.tagId)
											.includes(searchKey)
											|| getStringValue(
													tagBatteryInfo.macId)
													.includes(searchKey)
											|| getStringValue(
													tagBatteryInfo.type)
													.includes(searchKey)
											|| getStringValue(
													tagBatteryInfo.status)
													.includes(searchKey)
											|| getStringValue(
													tagBatteryInfo.assignmentId)
													.includes(searchKey)
											|| getStringValue(''+ tagBatteryInfo.batteryVoltage)
													.includes(searchKey)
											|| getStringValue(
													tagBatteryInfo.batteryAlarm)
													.includes(searchKey)) {
										$scope.tagBatteryInfoFilterList
												.push(tagBatteryInfo);
									}

								}

							}

							$scope.totalItems = $scope.tagBatteryInfoFilterList.length;
							$scope.currentPage = 1;
							$scope.getTotalPages();

							if ($scope.tagBatteryInfoData.length == 0
									|| $scope.tagBatteryInfoFilterList.length == 0) {
								$scope.tagNoData = true;

							} else {

								$scope.tagNoData = false;

								if ($scope.tagBatteryInfoFilterList.length > $scope.numPerPage) {
									$scope.tagBatteryInfoViewData = $scope.tagBatteryInfoFilterList
											.slice(0, $scope.numPerPage);
									$scope.PaginationTab = true;
								} else {
									$scope.tagBatteryInfoViewData = $scope.tagBatteryInfoFilterList;
									$scope.PaginationTab = false;
								}

							}
						}
					}

					function getStringValue(val) {

						return (val != undefined && val != null ? val : "")
								.toLowerCase();
					}

					// kumar 02/Jul filter option <-

					var reA = /[^a-zA-Z]/g;
					var reN = /[^0-9]/g;

					function sortAlphaNum(a1, b1) {
						var a = a1.tagId;
						var b = b1.tagId;
						var aA = a.replace(reA, "");
						var bA = b.replace(reA, "");
						if (aA === bA) {
							var aN = parseInt(a.replace(reN, ""), 10);
							var bN = parseInt(b.replace(reN, ""), 10);
							return aN === bN ? 0 : aN > bN ? 1 : -1;
						} else {
							return aA > bA ? 1 : -1;
						}
					}

					$scope.getBatteryInfoDetails = function() {

						if (!$rootScope.checkNetconnection()) {
							$rootScope.alertDialogChecknet();
							return;
						}
						$rootScope.showloading('#loadingBar');
						$http({
							method : 'GET',
							url : './getTagBatteryDetails',

						})
								.then(
										function(response) {
											$rootScope
													.hideloading('#loadingBar');
											if (response != null
													&& response.data != null
													&& response.data != "BAD_REQUEST") {

												$scope.tagBatteryInfoData = response.data.batteryInfo;
												$scope.tagBatteryInfoViewData = [];

												if ($scope.tagBatteryInfoData.length == 0) {
													$scope.tagNoData = true;
													return;
												} else {
													$scope.tagBatteryInfoData
															.sort(GetSortOrder("batteryVoltage"));
													$scope.tagNoData = false;
												}

												if ($scope.tagBatteryInfoData.length > 10) {
													$scope.PaginationTab = true;
												} else {
													$scope.PaginationTab = false;
												}

												$scope.totalItems = $scope.tagBatteryInfoData.length;

												$scope.getTotalPages();
												$scope.filterOption();

												$scope.currentPage = 1;
												if ($scope.tagBatteryInfoData.length > $scope.numPerPage) {
													$scope.tagBatteryInfoViewData = $scope.tagBatteryInfoData
															.slice(
																	0,
																	$scope.numPerPage);
													$scope.hasPrevious = true;
													$scope.isFirstPage = true;
													$scope.hasNext = false;
													$scope.isLastPage = false;
												} else {
													$scope.tagBatteryInfoViewData = $scope.tagBatteryInfoData;
												}
											} else {
												$scope.tagNoData = true;
											}

										},
										function error(response) {
											$rootScope
													.hideloading('#loadingBar');
											$rootScope.fnHttpError(response);
										});
					}

					function GetSortOrder(prop) {
						return function(a, b) {
							if (a[prop] > b[prop]) {
								return 1;
							} else if (a[prop] < b[prop]) {
								return -1;
							}
							return 0;
						}
					}

					$scope.getTagData = function() {

						if (!$rootScope.checkNetconnection()) {
							$rootScope.alertDialogChecknet();
							return;
						}
						$rootScope.showloading('#loadingBar');
						$http({
							method : 'GET',
							url : './tag',

						})
								.then(
										function(response) {
											$rootScope
													.hideloading('#loadingBar');
											if (response != null
													&& response.data != null
													&& response.data.length != 0
													&& response.data != "BAD_REQUEST"
													&& response.data != undefined
													&& response.data.length != 0) {

												$scope.tagData.length = 0;
												// $scope.tagData =
												// response.data;
												response.data.sort(sortAlphaNum);
												for (var i = 0; i < response.data.length; i++) {
													var fromTime = "", toTime = "", assignmentDate = "";
													var modifiedDate = "", createdDate = "", tagLastSeen = "", tagLastSeenMS = "";
													
													if (response.data[i].fromTime != undefined
															&& response.data[i].fromTime != null
															&& response.data[i].fromTime != "") {
														fromTime = response.data[i].fromTime;
													}
													if (response.data[i].toTime != undefined
															&& response.data[i].toTime != null
															&& response.data[i].toTime != "") {
														toTime = response.data[i].toTime;
													}
													if (response.data[i].createdDate != undefined
															&& response.data[i].createdDate != null
															&& response.data[i].createdDate != "") {
														response.data[i].createdDate = response.data[i].createdDate
																.substring(
																		0,
																		response.data[i].createdDate.length - 9);
														createdDate = moment(
																response.data[i].createdDate)
																.format(
																		"MMM DD, YYYY");
													}
													if (response.data[i].modifiedDate != undefined
															&& response.data[i].modifiedDate != null
															&& response.data[i].modifiedDate != "") {
														response.data[i].modifiedDate = response.data[i].modifiedDate
																.substring(
																		0,
																		response.data[i].modifiedDate.length - 9);
														modifiedDate = moment(
																response.data[i].modifiedDate,
																"YYYY-MMM-DD")
																.format(
																		"MMM DD, YYYY");
													}
													if (response.data[i].assignmentDate != undefined
															&& response.data[i].assignmentDate != null
															&& response.data[i].assignmentDate != "") {
														response.data[i].assignmentDate = response.data[i].assignmentDate
																.substring(
																		0,
																		response.data[i].assignmentDate.length - 9);
														assignmentDate = moment(response.data[i].assignmentDate,"YYYY-MMM-DD")
																.format("MMM DD, YYYY");
													}
													if (response.data[i].last_seen != undefined
															&& response.data[i].last_seen != null
															&& response.data[i].last_seen != "") {

														
														tagLastSeen = moment(new Date(Number(response.data[i].last_seen))).format("MMM DD, YY HH:mm");
														tagLastSeenMS = Number(response.data[i].last_seen);
														//lastSeenMSLength  = tagLastSeenMS.toString().length;
														
														
													} else {
														tagLastSeen = moment(new Date()).format("MMM DD, YY HH:mm");
														
														var myDate = new Date();
														tagLastSeenMS = myDate.getTime();
														
													}
													var tagType = response.data[i].type
													if (response.data[i].type == "Asset Chrono"
															|| response.data[i].type == "Asset NonChrono")
														tagType = response.data[i].subType

													$scope.tagData
															.push({
																'tagId' : response.data[i].tagId,
																'groupId' : response.data[i].groupId,
																'macId' : response.data[i].macId,
																'manufacturer' : response.data[i].manufacturer,
																'status' : response.data[i].status,
																'assignmentId' : response.data[i].assignmentId,
																'assignmentDate' : assignmentDate,
																'fromTime' : fromTime,
																'toTime' : toTime,
																'createdDate' : createdDate,
																'lastSeen' : tagLastSeen,
																'lastSeenMS' : tagLastSeenMS,
																'modifiedDate' : modifiedDate,
																'major' : response.data[i].major,
																'minor' : response.data[i].minor,
																'type' : tagType,
																'restart' : response.data[i].restart
															});
													
												}

												$scope.tagRenameData.length = 0;
												for (var j = 0; j < response.data.length; j++) {
													if (response.data[j].renamed == 0) {
														$scope.tagRenameData
																.push(response.data[j].macId);
													}

												}
												if ($scope.tagRenameData.length == 0)
													$scope.tagRenameData
															.push("No Tags Found");

												$scope.checktagIdTable.length = 0;
												for (var k = 0; k < response.data.length; k++) {
													if (response.data[k].tagId != undefined
															&& response.data[k].tagId != ''
															&& response.data[k].tagId != null) {
														$scope.checktagIdTable
																.push(response.data[k].tagId);
													}

												}
												if ($scope.checktagIdTable.length == 0)
													$scope.checktagIdTable
															.push("No Tags Found");
												$('#checktagIdSelect').select2(
														'destroy');
												$('#checktagIdSelect')
														.select2();

												// ----
												if ($scope.tagData.length == 0) {
													$scope.tagNoData = true;
													return;
												} else {
													$scope.tagNoData = false;
												}

												if ($scope.tagData.length > 10) {
													$scope.PaginationTab = true;
												} else {
													$scope.PaginationTab = false;
												}
												$scope.totalItems = $scope.tagData.length;

												$scope.getTotalPages();
												$scope.filterOption();

												$scope.currentPage = 1;
												if ($scope.tagData.length > $scope.numPerPage) {
													$scope.tagViewData = $scope.tagData
															.slice(0,
																	$scope.numPerPage);
													$scope.hasPrevious = true;
													$scope.isFirstPage = true;
													$scope.hasNext = false;
													$scope.isLastPage = false;
												} else {
													$scope.tagViewData = $scope.tagData;
												}
											} else {
												$scope.tagNoData = true;
											}

										},
										function error(response) {
											$rootScope
													.hideloading('#loadingBar');
											$rootScope.fnHttpError(response);
										});
						//console.log($scope.tagData);
					}

					// $scope.getTagData();

					$scope.cancelTag = function() {

						$scope.authen = {};
						$scope.selectedRow = -1;

						$scope.viewscreen = true;
						$scope.addscreen = false;
						$scope.searchButton = true;
					}

					$scope.renameTagBtn = function() {

						var renameTagParameter = {
							"macId" : $scope.authen.currentTagId,
							"tagId" : $scope.authen.newTagId

						};

						if ($scope.authen.currentTagId == "No Tags Found") {
							$rootScope.showAlertDialog("All Tags are renamed");
							return;
						}

						var newTagId = document.getElementById("id_newTagId");
						if (newTagId.value.length == 0) {
							$scope.newTagIdError = true;
							$scope.newTagId_error_msg = "Please enter new tag Id ";
							$timeout(function() {
								$scope.newTagIdError = false;

							}, 2000);

							return;
						}
						if (!$rootScope.checkNetconnection()) {
							$rootScope.alertDialogChecknet();
							return;
						}
						$rootScope.showloading('#loadingBar');
						$scope.promise = $http
								.post('./renameTag', renameTagParameter)
								.then(
										function(result) {
											$rootScope
													.hideloading('#loadingBar');
											if (result.data.status == "success") {
												$rootScope
														.showAlertDialog(result.data.message);
												$scope.authen.newTagId = "";
												if ($scope.tagRenameData != undefined
														&& $scope.tagRenameData != null) {

													var index = $scope.tagRenameData
															.indexOf($scope.authen.currentTagId);
													if (index !== -1) {
														$scope.tagRenameData
																.splice(index,
																		1);
													}
													if ($scope.tagRenameData.length == 0)
														$scope.tagRenameData
																.push("No Tags Found");
												}
												$timeout(
														function() {
															$(
																	'#id_currentTagId')
																	.select2(
																			'destroy');
															$(
																	'#id_currentTagId')
																	.select2();

														}, 500);
												$scope.authen.currentTagId = $scope.tagRenameData[0];

											} else if (result.data.status == "error") {
												$rootScope
														.showAlertDialog(result.data.message);
											}

										},
										function(error) {
											$rootScope
													.hideloading('#loadingBar');
											$rootScope.fnHttpError(error);
										});

					}

					// pagination button

					$scope.currentPage = 1;
					$scope.numPerPage = 10;
					//$scope.maxSize = 3;
					$scope.totalItems = 0;
					$scope.tagDataFilterList = [];
					$scope.tagViewData = [];
					$scope
							.$watch(
									'currentPage + numPerPage',
									function() {
										var begin = (($scope.currentPage - 1) * $scope.numPerPage), end = begin
												+ $scope.numPerPage;

										if (!$scope.showBatteryInfoScreen) {
											if ($scope.searchButton && $scope.searchText == '')
												$scope.tagViewData = $scope.tagData
														.slice(begin, end);
											else
												$scope.tagViewData = $scope.tagDataFilterList
														.slice(begin, end);
										} else {
											if ($scope.searchButton && $scope.searchText == '') {
												//takes time to get data from server
												//avoid exception in the below scenario Locate and back to battery info screen
												if ($scope.tagBatteryInfoData != undefined){
													$scope.tagBatteryInfoViewData = $scope.tagBatteryInfoData
															.slice(begin, end);
												}
											}
											else
												$scope.tagBatteryInfoViewData = $scope.tagBatteryInfoFilterList
														.slice(begin, end);

										}
									});

					$scope.currentPage = 1;

					//$scope.maxSize = 3;

					$scope.numPerPage = 10;
					$scope.totalItems = 0;
					$scope.totalPages = 0;
					$scope.hasNext = true;
					$scope.hasPrevious = true;
					$scope.isFirstPage = true;
					$scope.isLastPage = true;
					$scope.serial = 1;

					$scope
							.$watch(
									"currentPage",
									function(newVal, oldVal) {
										if (newVal !== oldVal) {

											if (newVal > $scope.totalPages) {
												$scope.currentPage = $scope.totalPages;
											} else {
												$scope.currentPage = newVal;
											}

											$scope.serial = (($scope.currentPage - 1) * $scope.numPerPage) + 1;

										}
									});

					$scope.enablePageButtons = function() {
						$scope.serial = (($scope.currentPage - 1) * $scope.numPerPage) + 1;
						$scope.selectedRow = -1;
						if ($scope.totalPages <= 1) {
							$scope.hasPrevious = true;
							$scope.isFirstPage = true;
							$scope.hasNext = true;
							$scope.isLastPage = true;

						} else if ($scope.totalPages > 1
								&& $scope.currentPage == $scope.totalPages) {

							$scope.hasPrevious = false;
							$scope.isFirstPage = false;
							$scope.hasNext = true;
							$scope.isLastPage = true;
						} else if ($scope.currentPage != 1
								&& $scope.currentPage < $scope.totalPages) {

							$scope.hasPrevious = false;
							$scope.isFirstPage = false;
							$scope.hasNext = false;
							$scope.isLastPage = false;

						} else {
							$scope.hasPrevious = true;
							$scope.isFirstPage = true;
							$scope.hasNext = false;
							$scope.isLastPage = false;
						}
					};

					$scope.getTotalPages = function() {

						if (($scope.totalItems % $scope.numPerPage) > 0)
							$scope.totalPages = parseInt($scope.totalItems
									/ $scope.numPerPage) + 1;
						else
							$scope.totalPages = parseInt($scope.totalItems
									/ $scope.numPerPage);

						$scope.enablePageButtons();

					};

					$scope.goFirstPage = function() {
						$scope.currentPage = 1;
						$scope.enablePageButtons();
					};

					$scope.goPreviousPage = function() {
						if ($scope.currentPage > 1)
							$scope.currentPage = $scope.currentPage - 1;
						$scope.enablePageButtons();
					};

					$scope.goNextPage = function() {
						if ($scope.currentPage <= $scope.totalPages)
							$scope.currentPage = $scope.currentPage + 1;
						$scope.enablePageButtons();
					};

					$scope.goLastPage = function() {
						$scope.currentPage = $scope.totalPages;
						$scope.enablePageButtons();
					};

					$scope.displayTagViewAllScreen = function() {

						$scope.tagDataFilterList = [];
						$scope.tagViewData = [];
						$scope.searchText = "";

						$scope.selectedRow = -1;
						$scope.currentPage = 1;
						$scope.searchButton = true;
						$scope.showTagViewAllScreen = true;
						$scope.tagRenameScreen = false;
						$scope.showBatteryInfoScreen = false;
						$scope.showBatteryInfoSearchCard = false;
						$scope.showTagDetailMultipleSearchCard = false;
						$scope.showSearchIcon = true;
						$("#idConfigurationTagViewAll").addClass("active");
						$("#idConfigurationTagRename").removeClass("active");
						$("#idBatteryInfo").removeClass("active");

						$scope.getTagData();
					};

					$scope.displayBatteryInfo = function() {

						$scope.showTagViewAllScreen = false;
						$scope.tagRenameScreen = false;
						$scope.showBatteryInfoScreen = true;
						$scope.showSearchIcon = false;
						$scope.searchText = "";
						$scope.showBatteryInfoSearchCard = false;
						$scope.showTagDetailMultipleSearchCard = false;

						$scope.selectedRow = -1;
						$scope.currentPage = 1;
						$scope.searchButton = true;
						$("#idConfigurationTagViewAll").removeClass("active");
						$("#idConfigurationTagRename").removeClass("active");
						$("#idBatteryInfo").addClass("active");

						$scope.getBatteryInfoDetails();
					}

					$scope.displayTagRenameScreen = function() {
						if ($("#id_renameTagIDform").data('validator') != undefined)
							$("#id_renameTagIDform").data('validator')
									.resetForm();
						$scope.searchButton = false;
						$scope.showTagViewAllScreen = false;
						$scope.tagRenameScreen = true;
						$scope.showBatteryInfoScreen = false;
						$scope.showSearchIcon = false;
						$scope.showBatteryInfoSearchCard = false;
						$scope.showTagDetailMultipleSearchCard = false;

						$("#idConfigurationTagViewAll").removeClass("active");
						$("#idConfigurationTagRename").addClass("active");
						$("#idBatteryInfo").removeClass("active");

						$scope.getTagData();
						if ($scope.tagRenameData != undefined
								&& $scope.tagRenameData != null)
							$scope.authen.currentTagId = $scope.tagRenameData[0];
					};

					/*$scope.tagAssignBackToView = function() {
						var formID = document.getElementById("assignTagForm")
								.reset();

						$scope.authen = {};
						$scope.selectedRow = -1;
						$scope.showTagViewAllScreen = true;
						$scope.searchButton = true;
						$scope.fetchScreen = false;
						$scope.assignScreen = false;
						$scope.tagPeopleReturnScreen = false;

					}*/
					
					$scope.fnMultipleSearch = function() {
						if($scope.showBatteryInfoScreen){
							$scope.showBatteryInfoSearchCard = true;
							$scope.searchButton = false;
							$scope.varCompareVoltagedropdown = "equalTo";
							$scope.firstSearchTextBattery = "";
						}
						else if($scope.showTagViewAllScreen){
							$scope.showTagDetailMultipleSearchCard = true;
							$scope.searchButton = false;
							$scope.tagTypeMultiSearchDropDown = "All";
							$scope.tagTypeDayRange = "today";
							$scope.AssetTagTypeTimeRange = "1Hour";
							$scope.multiSearchChanged();
								
						}
						
						
					}
					$scope.batteryVoltageSearchFn = function() {
						
						if($scope.showBatteryInfoScreen && $scope.showBatteryInfoSearchCard){
							
						//$scope.batteryVoltageSearchArray = [];
						$scope.tagBatteryInfoViewData = [];
						$scope.tagBatteryInfoFilterList  = [];
						for (var i = 0; i < $scope.tagBatteryInfoData.length; i++) {
							
							
								if ($scope.firstSearchTextBattery == "" || $scope.firstSearchTextBattery == undefined) {
									
									$scope.tagBatteryInfoFilterList.push($scope.tagBatteryInfoData[i]);
									
								} else if ($scope.varCompareVoltagedropdown == "equalTo"){

									if ((($scope.tagBatteryInfoData[i].batteryVoltage).toString() != undefined
											&& ($scope.tagBatteryInfoData[i].batteryVoltage).toString() != null))
											{
										
										if($scope.tagBatteryInfoData[i].batteryVoltage == parseFloat($scope.firstSearchTextBattery)){
											$scope.tagBatteryInfoFilterList.push($scope.tagBatteryInfoData[i]);
										}
										

									}

									
								}else if ($scope.varCompareVoltagedropdown == "lessThan"){

									if ((($scope.tagBatteryInfoData[i].batteryVoltage).toString() != undefined
											&& ($scope.tagBatteryInfoData[i].batteryVoltage).toString() != null))
											{
										
										if($scope.tagBatteryInfoData[i].batteryVoltage < parseFloat($scope.firstSearchTextBattery)){
											$scope.tagBatteryInfoFilterList.push($scope.tagBatteryInfoData[i]);
										}
										

									}

									
								}else if ($scope.varCompareVoltagedropdown == "greaterThan"){

									if ((($scope.tagBatteryInfoData[i].batteryVoltage).toString() != undefined
											&& ($scope.tagBatteryInfoData[i].batteryVoltage).toString() != null))
											{
										
										if($scope.tagBatteryInfoData[i].batteryVoltage > parseFloat($scope.firstSearchTextBattery)){
											$scope.tagBatteryInfoFilterList.push($scope.tagBatteryInfoData[i]);
										}
										

									}

									
								}
								
							
						}
						
						$scope.totalItems = $scope.tagBatteryInfoFilterList.length;
						$scope.currentPage = 1;
						$scope.getTotalPages();

						if ($scope.tagBatteryInfoData.length == 0
								|| $scope.tagBatteryInfoFilterList.length == 0) {
							$scope.tagNoData = true;

						} else {

							$scope.tagNoData = false;

							if ($scope.tagBatteryInfoFilterList.length > $scope.numPerPage) {
								$scope.tagBatteryInfoViewData = $scope.tagBatteryInfoFilterList
								.slice(0, $scope.numPerPage);
								$scope.PaginationTab = true;
							} else {
								$scope.tagBatteryInfoViewData = $scope.tagBatteryInfoFilterList;
								$scope.PaginationTab = false;
							}
						
					}
					}
				}
					$scope.fnCloseTagDetailMultiSearchView = function() {
						$scope.showTagDetailMultipleSearchCard = false;
						$scope.searchButton = true;
						$scope.fnDisplayAllrecordsViewAllTag();
						
					}
					$scope.fnCloseBatteryInfoSearch = function() {
						
						$scope.showBatteryInfoSearchCard = false;
						$scope.searchButton = true;
						$scope.fnDisplayAllrecords();
						
					}
					
					/*$scope.noOfDaysFn  = function(dayCount,lastSeen) {
						$scope.dayCountValue = dayCount;
						$scope.noOfDays = new Date();
						$scope.noOfDays.setDate($scope.noOfDays.getDate() - $scope.dayCountValue);
						$scope.noOfDaysEpoch = $scope.noOfDays.getTime()/1000.0;
						$scope.noOfDaysEpoch = parseInt($scope.noOfDaysEpoch);
						
						$scope.lastSeenMSEpoch = lastSeen;
						$scope.lastSeenLength  = $scope.lastSeenMSEpoch.toString().length;
						if( $scope.lastSeenLength > 10 && $scope.lastSeenLength == 13 )
							$scope.lastSeenMSEpoch = $scope.lastSeenMSEpoch / 1000;
						
						
					}*/
					
					$scope.noOfHoursFn  = function(hourCount, lastSeen) {
						$scope.hourCountValue = hourCount;
						$scope.noOfHours = new Date();
						
						$scope.noOfHoursEpoch = $scope.noOfHours.getTime()/1000.0;
						$scope.noOfHoursEpoch = parseInt($scope.noOfHoursEpoch);
						$scope.noOfHoursEpoch = $scope.noOfHoursEpoch - $scope.hourCountValue;
						
						$scope.lastSeenEpoch = lastSeen;
						$scope.lastSeenMSLength  = $scope.lastSeenEpoch.toString().length;
						if( $scope.lastSeenMSLength > 10 && $scope.lastSeenMSLength == 13 )
							$scope.lastSeenEpoch = $scope.lastSeenEpoch / 1000;
						
					}
					
					$scope.multiSearchChanged = function() {
						//$scope.batteryVoltageSearchArray  = [];
						
						if($scope.showBatteryInfoScreen && $scope.showBatteryInfoSearchCard){
						$scope.batteryVoltageSearchFn();
						
						
						}
						else if($scope.showTagViewAllScreen && $scope.showTagDetailMultipleSearchCard){
							
							
							if($scope.tagTypeMultiSearchDropDown != "employee" && $scope.tagTypeDayRange == "today"){
								$scope.assetTimeRangeDropDown = true;
						}else{
							$scope.assetTimeRangeDropDown = false;
						}
							$scope.tagViewData = [];
							$scope.tagDataFilterList = [];
							
							
							var todayFormatedDate = moment(new Date()).format("MMM DD, YYYY");
							$scope.noOfDays = new Date(); 
							if($scope.tagTypeDayRange != "today"){
								
								if($scope.tagTypeDayRange == "2Days"){
									$scope.noOfDays.setDate($scope.noOfDays.getDate() - 1);
								}else if($scope.tagTypeDayRange == "3Days"){ 
									$scope.noOfDays.setDate($scope.noOfDays.getDate() - 2); 
								}else if($scope.tagTypeDayRange == "7Days"){
									$scope.noOfDays.setDate($scope.noOfDays.getDate() - 6); 
								}else if($scope.tagTypeDayRange == "14Days"){ 
									$scope.noOfDays.setDate($scope.noOfDays.getDate() - 13);
								}
								
								$scope.noOfDays.setHours(0, 0, 0, 0);
							}else
							{  
								if($scope.AssetTagTypeTimeRange == "1Hour"){ 
									$scope.noOfDays.setHours($scope.noOfDays.getHours() - 1);
								}else if($scope.AssetTagTypeTimeRange == "2Hours"){ 
									$scope.noOfDays.setHours($scope.noOfDays.getHours() - 2);
								}else if($scope.AssetTagTypeTimeRange == "3Hours"){ 
									$scope.noOfDays.setHours($scope.noOfDays.getHours() - 3);
								}else if($scope.AssetTagTypeTimeRange == "4Hours"){  
									$scope.noOfDays.setHours($scope.noOfDays.getHours() - 4);
								}
							}
							
							var noOfDaysEpoch = $scope.noOfDays.getTime();
							
							for (var i = 0; i < $scope.tagData.length; i++) {
								 
								if($scope.tagTypeMultiSearchDropDown == "All" ||  
									( $scope.tagData[i].type != undefined && $scope.tagData[i].type != "" && 
									(( $scope.tagTypeMultiSearchDropDown == "employee" && $scope.tagData[i].type == "Employee")
									||( $scope.tagTypeMultiSearchDropDown == "gages" && ($scope.tagData[i].type == "gages" || $scope.tagData[i].type == "Gages"))
									||( $scope.tagTypeMultiSearchDropDown == "tools" && ($scope.tagData[i].type == "tools" || $scope.tagData[i].type == "Tools"))
									||( $scope.tagTypeMultiSearchDropDown == "fixtures" && ($scope.tagData[i].type == "fixtures" || $scope.tagData[i].type == "Fixtures"))
									||( $scope.tagTypeMultiSearchDropDown == "Supplies" && ($scope.tagData[i].type == "supplies" || $scope.tagData[i].type == "Supplies"))
									||( $scope.tagTypeMultiSearchDropDown == "Vehicles" && ($scope.tagData[i].type == "vehicles" || $scope.tagData[i].type == "Vehicles"))
									||( $scope.tagTypeMultiSearchDropDown == "parts" && ($scope.tagData[i].type == "parts" || $scope.tagData[i].type == "Parts"))
									||( $scope.tagTypeMultiSearchDropDown == "material" && ($scope.tagData[i].type == "material" || $scope.tagData[i].type == "Material"
										|| $scope.tagData[i].type == "Materials"|| $scope.tagData[i].type == "materials"))
									||( $scope.tagTypeMultiSearchDropDown == "RMA" && ($scope.tagData[i].type == "RMA"))
									||( $scope.tagTypeMultiSearchDropDown == "LSLM" && ($scope.tagData[i].type == "LSLM"))
									||( $scope.tagTypeMultiSearchDropDown == "inventory" && ($scope.tagData[i].type == "inventory" || $scope.tagData[i].type == "Inventory"))
									||( $scope.tagTypeMultiSearchDropDown == "warehouseMaterials" && ($scope.tagData[i].type == "warehouse materials" || $scope.tagData[i].type == "Warehouse Materials"
										|| $scope.tagData[i].type == "WarehouseMaterials"|| $scope.tagData[i].type == "warehouseMaterials"
											|| $scope.tagData[i].type == "WarehouseMaterials"|| $scope.tagData[i].type == "Warehouse"
											|| $scope.tagData[i].type == "WareHouse")) 
								))){
									
									var myDateEpoch = $scope.tagData[i].lastSeenMS; 
									if($scope.tagTypeDayRange == "today"){
										var lastSeenFormatedDate = moment(myDateEpoch).format("MMM DD, YYYY"); 
										if($scope.tagTypeMultiSearchDropDown == "employee"){
											
											if(lastSeenFormatedDate ==  todayFormatedDate)
											{
												$scope.tagDataFilterList.push($scope.tagData[i]);
											}
										}else
										{
											if(lastSeenFormatedDate ==  todayFormatedDate && noOfDaysEpoch >  myDateEpoch)
											{
												$scope.tagDataFilterList.push($scope.tagData[i]);
											}
										} 
									
									}else {
										
										if(noOfDaysEpoch >  myDateEpoch)
										{
										   $scope.tagDataFilterList.push($scope.tagData[i]);
										}
									}
									
								}   
							}
							
							$scope.tagViewData = $scope.tagDataFilterList;
							$scope.totalItems = $scope.tagDataFilterList.length;
							$scope.currentPage = 1;
							$scope.getTotalPages();

							if ($scope.tagData.length == 0
									|| $scope.tagDataFilterList.length == 0) {
								$scope.tagNoData = true;

							} else {

								$scope.tagNoData = false;

								if ($scope.tagDataFilterList.length > $scope.numPerPage) {
									$scope.tagViewData = JSON.parse(JSON.stringify($scope.tagDataFilterList.slice(0, $scope.numPerPage)));
									
									$scope.PaginationTab = true;
								} else {
									$scope.tagViewData = $scope.tagDataFilterList;
									$scope.PaginationTab = false;
								}
							
						}
							
						}
						
					}
					
					$scope.fnDisplayAllrecordsViewAllTag = function() {
						//$scope.tagDataFilterList =[];
						$scope.tagDataFilterList = $scope.tagData;
						$scope.totalItems = $scope.tagDataFilterList.length;
						$scope.currentPage = 1;
						$scope.getTotalPages();

						//$scope.getAssignedTagIds($scope.tagBatteryInfoFilterList);

						if ($scope.tagData.length == 0
								|| $scope.tagDataFilterList.length == 0) {
							$scope.tagNoData = true;

						} else {
							$scope.tagNoData = false;

							if ($scope.tagDataFilterList.length > $scope.numPerPage) {
								$scope.tagViewData = JSON.parse(JSON
										.stringify($scope.tagDataFilterList
												.slice(0, $scope.numPerPage)));
								$scope.PaginationTab = true;
							} else {
								$scope.tagViewData = JSON.parse(JSON
										.stringify($scope.tagDataFilterList));
								$scope.PaginationTab = false;
							}
						}
					}
					
					$scope.fnDisplayAllrecords = function() {
						$scope.tagBatteryInfoFilterList = $scope.tagBatteryInfoData;
						$scope.totalItems = $scope.tagBatteryInfoFilterList.length;
						$scope.currentPage = 1;
						$scope.getTotalPages();

						//$scope.getAssignedTagIds($scope.tagBatteryInfoFilterList);

						if ($scope.tagBatteryInfoData.length == 0
								|| $scope.tagBatteryInfoFilterList.length == 0) {
							$scope.tagNoData = true;

						} else {
							$scope.tagNoData = false;

							if ($scope.tagBatteryInfoFilterList.length > $scope.numPerPage) {
								$scope.tagBatteryInfoViewData = JSON.parse(JSON
										.stringify($scope.tagBatteryInfoFilterList
												.slice(0, $scope.numPerPage)));
								$scope.PaginationTab = true;
							} else {
								$scope.tagBatteryInfoViewData = JSON.parse(JSON
										.stringify($scope.tagBatteryInfoFilterList));
								$scope.PaginationTab = false;
							}
						}
					}
					
					// to display enabled sub menus
					if ($scope.user.capabilityDetails != undefined
							&& $scope.user.capabilityDetails != null) {
						
						if(!$rootScope.isBackClicked){
							if ($scope.user.capabilityDetails.CTVA != 0) {
								$scope.displayTagViewAllScreen();
							}
							if ($scope.user.capabilityDetails.CTVA != 1) {
								$scope.displayTagRenameScreen();
							}
						}else{
							$rootScope.isBackClicked = false;
							$scope.displayBatteryInfo();
						}
						
						
						

					}

				});
