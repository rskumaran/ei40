app.controller(
		'configurationController',
		function($scope, $http, $rootScope, $window, $location,$timeout) {
			$scope.user = JSON.parse($window.sessionStorage
					.getItem("user"));
			if ($scope.user == null) {
				$location.path('/login');
				return;
			}
            $scope.configData=[];
            $scope.configViewData=[]; 
            $scope.config={};
            $scope.httpMethod='POST';
            var orgName="";
            $scope.configurationShiftType =["Day", "Night", "First", "Second", "Third"];
            //from shift screen
            $scope.bShowAddBtn = true;
			$scope.bShowUpdateBtn = false;
			$scope.bShowDeleteBtn = false;					
			$scope.shiftData = new Array();
			$scope.shiftViewData = new Array();
			$scope.shiftDataFilterList = new Array();
			$scope.searchText = "";					
			$scope.addshift = {};
			$scope.addshift.shiftType = $scope.configurationShiftType[0];
			
            $scope.config.transactionDataAge = 30;

            $scope.config.staffTagColor = "#f250b6";
			$scope.config.visitorTagColor = "#cb1e0b";
			$scope.config.vendorTagColor = "#547360";
			$scope.config.contractorTagColor = "#b77b62";
			
			$scope.config.vehicleTagColor = "#b07fbd";
			$scope.config.gageTagColor = "#ba10d1";
			$scope.config.toolTagColor = "#0eec15";
			$scope.config.fixtureTagColor = "#3b4ee3";

			$scope.config.partTagColor = "#dbf708";
			$scope.config.materialTagColor = "#128dd9";
			$scope.config.rmaTagColor = "#db6f0a";

			$scope.config.inventoryTagColor = "#19B6A7";
			$scope.config.lslmTagColor = "#2CAEEB";
			$scope.config.wareHouseTagColor = "#1BA0AD";
			$scope.config.suppliesTagColor  = "#5CBB0CD";
			$('#idStaffTagColor').spectrum({
				  type: "color",
				  showInitial: true,
				  hideAfterPaletteSelect: true
				});
			$("#idStaffTagColor").spectrum("set", $scope.config.staffTagColor);
			
			$('#idVisitorTagColor').spectrum({
				  type: "color",
				  showInitial: true,
				  hideAfterPaletteSelect: true
				});
			$("#idVisitorTagColor").spectrum("set", $scope.config.visitorTagColor);
			$('#idVendorTagColor').spectrum({
				  type: "color",
				  showInitial: true,
				  hideAfterPaletteSelect: true
				});
			$("#idVendorTagColor").spectrum("set", $scope.config.vendorTagColor);
			$('#idContractorTagColor').spectrum({
				  type: "color",
				  showInitial: true,
				  hideAfterPaletteSelect: true
				});
			$("#idContractorTagColor").spectrum("set", $scope.config.contractorTagColor);

			$('#idVehicleTagColor').spectrum({
				  type: "color",
				  showInitial: true,
				  hideAfterPaletteSelect: true
				});
			$("#idVehicleTagColor").spectrum("set", $scope.config.vehicleTagColor);
			
			$('#idGageTagColor').spectrum({
				  type: "color",
				  showInitial: true,
				  hideAfterPaletteSelect: true
				});
			$("#idGageTagColor").spectrum("set", $scope.config.gageTagColor);
			$('#idToolTagColor').spectrum({
				  type: "color",
				  showInitial: true,
				  hideAfterPaletteSelect: true
				});
			$("#idToolTagColor").spectrum("set", $scope.config.toolTagColor);
			$('#idFixtureTagColor').spectrum({
				  type: "color",
				  showInitial: true,
				  hideAfterPaletteSelect: true
				});
			$("#idFixtureTagColor").spectrum("set", $scope.config.fixtureTagColor);
			$('#idPartTagColor').spectrum({
				  type: "color",
				  showInitial: true,
				  hideAfterPaletteSelect: true
				});
			$("#idPartTagColor").spectrum("set", $scope.config.partTagColor);
			
			$('#idMaterialTagColor').spectrum({
				  type: "color",
				  showInitial: true,
				  hideAfterPaletteSelect: true
				});
			$("#idMaterialTagColor").spectrum("set", $scope.config.materialTagColor);
			$('#idRmaTagColor').spectrum({
				  type: "color",
				  showInitial: true,
				  hideAfterPaletteSelect: true
				});
			$("#idRmaTagColor").spectrum("set", $scope.config.rmaTagColor);
			$('#idInventoryTagColor').spectrum({
				  type: "color",
				  showInitial: true,
				  hideAfterPaletteSelect: true
				});
			$("#idInventoryTagColor").spectrum("set", $scope.config.inventoryTagColor);			
			$('#idLSLMTagColor').spectrum({
				  type: "color",
				  showInitial: true,
				  hideAfterPaletteSelect: true
				});
			$("#idLSLMTagColor").spectrum("set", $scope.config.lslmTagColor);		
			$('#idWareHouseTagColor').spectrum({
				  type: "color",
				  showInitial: true,
				  hideAfterPaletteSelect: true
				});
			$("#idWareHouseTagColor").spectrum("set", $scope.config.wareHouseTagColor);		

			$('#idSuppliesTagColor').spectrum({
				  type: "color",
				  showInitial: true,
				  hideAfterPaletteSelect: true
				});
			$("#idSuppliesTagColor").spectrum("set", $scope.config.suppliesTagColor);		

			const MAX_SHIFT_RECORDS = 3;
			$('#shiftStarttimepicker').datetimepicker({
				format : 'HH:mm'
			})
			$('#shiftEndtimepicker').datetimepicker({
				format : 'HH:mm'
			})
            //from shift screen

       		$('#stimepicker').datetimepicker({
			      format: 'HH:mm'
		    })
        	 $('#etimepicker').datetimepicker({
			      format: 'HH:mm'
		    })


			$scope.getConfigData = function() {
				if (!$rootScope.checkNetconnection()) { 
					$rootScope.alertDialogChecknet();
					return;
					}	
						$rootScope.showloading('#loadingBar');
						$http({
							method : 'GET',
							url : './configuration/'+orgName,
							
						})
								.then(
										function(response) {
											if (response != null
													&& response.data != null
													&& response.data.length != 0
													&& response.data != "BAD_REQUEST"
													&& response.data.data != undefined
													&& response.data.data.length != 0) {
														$scope.httpMethod='PUT';
														$rootScope.hideloading('#loadingBar');

											for (var i = 0; i < response.data.data.length; i++) {
														$scope.config.orgName = response.data.data[i].orgName;
							                            $scope.config.logAge = response.data.data[i].logAge;
                                                    /*
													 * $scope.config.timeZone =
													 * response.data.data[i].timeZone;
													 * $scope.config.licenseKey =
													 * response.data.data[i].licenseKey;
													 */
	                                                    $scope.config.transactionDataAge = response.data.data[i].transactionDataAge;
														$scope.config.staffTagColor = response.data.data[i].staffTagColor;
														$scope.config.visitorTagColor = response.data.data[i].visitorTagColor;
														$scope.config.gageTagColor = response.data.data[i].gageTagColor;
														$scope.config.toolTagColor = response.data.data[i].toolTagColor;
														$scope.config.fixtureTagColor = response.data.data[i].fixtureTagColor;
														$scope.config.partTagColor = response.data.data[i].partTagColor;
														$scope.config.materialTagColor = response.data.data[i].materialTagColor;
														$scope.config.rmaTagColor = response.data.data[i].rmaTagColor;

														$scope.config.vendorTagColor = response.data.data[i].vendorTagColor;
														$scope.config.contractorTagColor = response.data.data[i].contractorTagColor;
														$scope.config.vehicleTagColor = response.data.data[i].vehicleTagColor;
														$scope.config.inventoryTagColor = response.data.data[i].inventoryTagColor;
														$scope.config.lslmTagColor = response.data.data[i].lslmtagColor;
														$scope.config.wareHouseTagColor = response.data.data[i].wareHouseTagColor;
														$scope.config.suppliesTagColor = response.data.data[i].suppliesTagColor;
														
														$scope.config.operationStartTime = response.data.data[i].operationStartTime;
														$scope.config.operationEndTime = response.data.data[i].operationEndTime;
														
												}
											  $("#idStaffTagColor").spectrum("set", $scope.config.staffTagColor);
												$("#idVisitorTagColor").spectrum("set", $scope.config.visitorTagColor);
												$("#idVendorTagColor").spectrum("set", $scope.config.vendorTagColor);
												$("#idContractorTagColor").spectrum("set", $scope.config.contractorTagColor);
												$("#idVehicleTagColor").spectrum("set", $scope.config.vehicleTagColor);
												$("#idGageTagColor").spectrum("set", $scope.config.gageTagColor);
												$("#idToolTagColor").spectrum("set", $scope.config.toolTagColor);
												$("#idFixtureTagColor").spectrum("set", $scope.config.fixtureTagColor);
												$("#idPartTagColor").spectrum("set", $scope.config.partTagColor);
												$("#idMaterialTagColor").spectrum("set", $scope.config.materialTagColor);
												$("#idInventoryTagColor").spectrum("set", $scope.config.inventoryTagColor);			
												$("#idLSLMTagColor").spectrum("set", $scope.config.lslmTagColor);			
												$("#idWareHouseTagColor").spectrum("set", $scope.config.wareHouseTagColor);			
												$("#idSuppliesTagColor").spectrum("set", $scope.config.suppliesTagColor);			
												$("#idRmaTagColor").spectrum("set", $scope.config.rmaTagColor);
					   }else if(response.data.status == "error"){
												$rootScope.hideloading('#loadingBar');
												$scope.httpMethod='POST';
					                                   
														$scope.config.orgName = orgName ;
					
					   }
		            else{
						$rootScope.hideloading('#loadingBar');

		                }
		
					  },function(error) {
							$rootScope.hideloading('#loadingBar');
							$rootScope.fnHttpError(error);
						});
					}
                
				  $scope.getConfigData();

				  $("#id_orgName").keypress(
							function(e) {
								// if the letter is not alphabets then display error
								// and don't type anything
								$return = ((e.which > 64) && (e.which < 91)) || ((e.which> 96) && (e.which < 123)) ||(e.which == 8) || (e.which == 32)  || (e.which >= 48 && e.which <= 57)
								if (!$return)
													
								{
									
									return false;
								}
							});	
				  /*$("#id_logAge").keypress(
							function(e) {
								// if the letter is not digit then display error
								// and don't type anything
								
								if (e.which != 8 && e.which != 0
										&& (e.which < 48 || e.which > 57)) {
									
									return false;
								}
							});
				  $("#id_transactionDataAge").keypress(
							function(e) {
								// if the letter is not digit then display error
								// and don't type anything
								
								if (e.which != 8 && e.which != 0
										&& (e.which < 48 || e.which > 57)) {
									
									return false;
								}
							});*/
				  
				 $scope.updateConfig = function() {
					 		
						
						var orgName = document.getElementById("id_orgName");
						var operationStartTime = document.getElementById("operationStartTime");
						var operationEndTime = document.getElementById("operationEndTime");
						var logAge = 30 ;
						var transactionDataAge =30;
						/*var logAge = document.getElementById("id_logAge");
						var transactionDataAge = document.getElementById("id_transactionDataAge" +"" +"" +"");*/

						if (orgName.value.length == 0) {
							$scope.orgNameError = true;
							$scope.orgName_error_msg = "Please enter the Org name";
							$timeout(function() {
								$scope.orgNameError = false;
							}, 2000);
							return;
						}else if (operationStartTime.value.length == 0) {
							$scope.operationStartTimeError = true;
							$scope.operationStartTime_error_msg = "Please enter the StartTime";
							$timeout(function() {
								$scope.operationStartTimeError = false;
							}, 2000);
							return;
						}else if (operationEndTime.value.length == 0) {
							$scope.operationEndTimeError = true;
							$scope.operationEndTime_error_msg = "Please enter the EndTime";
							$timeout(function() {
								$scope.operationEndTimeError = false;
							}, 2000);
							return;
						}
						/*else if (logAge.value.length == 0) {
							$scope.logAgeError = true;
							$scope.logAge_error_msg = "Please enter the Log Age";
							$timeout(function() {
								$scope.logAgeError = false;
							}, 2000);
							return;
						}else if (transactionDataAge.value.length == 0) {
							$scope.dataAgeError = true;
							$scope.dataAge_error_msg = "Please enter the Transaction Data Age";
							$timeout(function() {
								$scope.dataAgeError = false;
							}, 2000);
							return;
						} */ 
						
						$scope.config.staffTagColor = $("#idStaffTagColor").val();
						$scope.config.visitorTagColor = $("#idVisitorTagColor").val(); 
						$scope.config.vendorTagColor = $("#idVendorTagColor").val();
						$scope.config.contractorTagColor = $("#idContractorTagColor").val(); 
						$scope.config.vehicleTagColor = $("#idVehicleTagColor").val();
						$scope.config.gageTagColor = $("#idGageTagColor").val(); 
						$scope.config.toolTagColor = $("#idToolTagColor").val();
						$scope.config.fixtureTagColor = $("#idFixtureTagColor").val(); 
						$scope.config.partTagColor = $("#idPartTagColor").val();
						$scope.config.materialTagColor = $("#idMaterialTagColor").val(); 
						$scope.config.inventoryTagColor = $("#idInventoryTagColor").val(); 			
						$scope.config.lslmTagColor = $("#idLSLMTagColor").val(); 			
						$scope.config.wareHouseTagColor = $("#idWareHouseTagColor").val(); 			
						$scope.config.suppliesTagColor = $("#idSuppliesTagColor").val(); 			
						$scope.config.rmaTagColor = $("#idRmaTagColor").val();
						
						data = {
								"orgName":$scope.config.orgName,
								"transactionDataAge":transactionDataAge,
								"logAge":logAge,
								"operationStartTime":$scope.config.operationStartTime,
								"operationEndTime":$scope.config.operationEndTime,
							    "staffTagColor":$scope.config.staffTagColor,
							 	"visitorTagColor":$scope.config.visitorTagColor,
							 	"gageTagColor":$scope.config.gageTagColor,
							 	"toolTagColor":$scope.config.toolTagColor,
							 	"fixtureTagColor":$scope.config.fixtureTagColor,
							 	"partTagColor":$scope.config.partTagColor,
							 	"materialTagColor":$scope.config.materialTagColor,
							 	"rmaTagColor":$scope.config.rmaTagColor,
								"vendorTagColor": $scope.config.vendorTagColor,
								"contractorTagColor": $scope.config.contractorTagColor,
								"vehicleTagColor": $scope.config.vehicleTagColor,
								"inventoryTagColor": $scope.config.inventoryTagColor,
								"lslmtagColor": $scope.config.lslmTagColor,
								"wareHouseTagColor": $scope.config.wareHouseTagColor,
								"suppliesTagColor": $scope.config.suppliesTagColor
								
							    }
						

						if ($scope.configForm.$valid) {
							$rootScope.showloading('#loadingBar');
							$http({
								method : $scope.httpMethod,
								url : './configuration',
								data : data
								
							})
									.then(
											function(response) {
												if (response != null
														&& response.data != null
														&& response.data != "BAD_REQUEST") {
													$rootScope.hideloading('#loadingBar');
													$scope.config = response.data;
													 $rootScope.alertDialog(response.data.message); 
													/*$.alert({
							                            title: $rootScope.MSGBOX_TITLE,
							                            content: response.data.message,
							                            type: 'blue',
														columnClass:'medium',
														autoClose : false,
									                    icon: 'fa fa-info-circle',
							                             });*/
													$timeout(
															function() {
																 $scope.getConfigData();
															}, 2000);
												} else {
													$rootScope
															.hideloading('#loadingBar');
													$scope.noList = true;
													$scope.alert_error_msg = "Configuration "
															+ $scope.message
															+ " process failed";
													$timeout(function() {
														$scope.error = false;
													}, 2000);
												}
											},function(error) {
												$rootScope.hideloading('#loadingBar');
												$rootScope.fnHttpError(error);
											});
						}

				
					}
				/*					
				 $scope.deleteConfig = function() {		
					   var orgName = document.getElementById("orgName");

						if (orgName.value.length == 0) {
							$scope.orgNameError = true;
							$scope.orgName_error_msg = "Please enter the Org name";
							$timeout(function() {
								$scope.orgNameError = false;
							}, 2000);
							return;
						}

						if ($scope.configForm.$valid) {
							$rootScope.showloading('#loadingBar');
							var orgName = $scope.config.orgName
						     data = {
							 "orgName" : orgName 
						    }
							$http({
								method : 'DELETE',
								url : './configuration/Entappia',
								headers: {
								'Content-Type':'application/json'
								},
								data : data,
								
							})
									.then(
											function(response) {
												if (response != null
														&& response.data != null
														&& response.data != "BAD_REQUEST") {
													$rootScope.hideloading('#loadingBar');
													$scope.config = response.data;
													 $rootScope.alertDialog(response.data.message); 
													//$.alert({
							                         //   title: $rootScope.MSGBOX_TITLE,
							                           // content: response.data.message,
							                           // type: 'blue',
													//	columnClass:'medium',
													//	autoClose : false,
									                 //   icon: 'fa fa-info-circle',
							                         //    });
													$timeout(
															function() {
																 $scope.getConfigData();
															}, 2000);
												} else {
													$rootScope
															.hideloading('#loadingBar');
													$scope.noList = true;
													$scope.alert_error_msg = "Configuration "
															+ $scope.message
															+ " process failed";
													$timeout(function() {
														$scope.error = false;
													}, 2000);
												}
											});
						}

				
					}
					*/
				// from shift screen
					
					$scope.searchOption = function() {
						$scope.filterOption();
					}

					$scope.filterOption = function() {

						$scope.shiftViewData = [];
						$scope.shiftDataFilterList = [];

						for (var i = 0; i < $scope.shiftData.length; i++) {
							// if ($scope.filterValue == "All"
							// || $scope.shiftData[i].status ==
							// $scope.filterValue) {

							if ($scope.searchText == '') {
								$scope.shiftDataFilterList
										.push($scope.shiftData[i]);
							} else {
								if ($scope.shiftData[i].shiftName
										.toLowerCase()
										.includes(
												$scope.searchText.toLowerCase())
										|| $scope.shiftData[i].shiftType
												.toLowerCase().includes(
														$scope.searchText
																.toLowerCase())
										|| ($scope.shiftData[i].shiftStartTime != undefined
												&& $scope.shiftData[i].shiftStartTime != null ? $scope.shiftData[i].shiftStartTime
												: "").toLowerCase()
												.includes(
														$scope.searchText
																.toLowerCase())
										|| ($scope.shiftData[i].shiftEndTime != undefined
												&& $scope.shiftData[i].shiftEndTime != null ? $scope.shiftData[i].shiftEndTime
												: "").toLowerCase()
												.includes(
														$scope.searchText
																.toLowerCase())) {
									$scope.shiftDataFilterList
											.push($scope.shiftData[i]);

								}

							}
							// }
						}

						$scope.totalItems = $scope.shiftDataFilterList.length;
						$scope.currentPage = 1;
						$scope.getTotalPages();

						if ($scope.shiftData.length == 0
								|| $scope.shiftDataFilterList.length == 0) {
							$scope.shiftNoData = true;

						} else {
							$scope.shiftNoData = false;

							var begin = (($scope.currentPage - 1) * $scope.numPerPage), end = begin
									+ $scope.numPerPage;

							$scope.shiftViewData = $scope.shiftDataFilterList
									.slice(begin, end);
							/*
							 * if ($scope.shiftDataFilterList.length >
							 * $scope.numPerPage) $scope.shiftViewData =
							 * $scope.shiftDataFilterList .slice(0,
							 * $scope.numPerPage); else $scope.shiftViewData =
							 * $scope.shiftDataFilterList; $scope.apply();
							 */
						}
					}// kumar 02/Jul filter option <-

					$scope.getShiftData = function() {
						if (!$rootScope.checkNetconnection()) { 
							//$rootScope.alertDialogChecknet();
							return;
							}

						// $rootScope.showloading('#loadingBar');
						$http({
							method : 'GET',
							url : './shift',

						})
								.then(
										function(response) {
											if (response != null
													&& response.data != null
													&& response.data.length != 0
													&& response.data != "BAD_REQUEST"
													&& response.data.data != undefined
													&& response.data.data.length != 0) {

												$scope.shiftData.length = 0;
												$scope.addshift.shiftType = $scope.configurationShiftType[0];

												for (var i = 0; i < response.data.data.length; i++) {
													if (response.data.data[i].active == true) {
														$scope.shiftData
																.push(response.data.data[i]);

													}
												}
												// kumar 02-Jul display no
												// record found
												if ($scope.shiftData.length == 0) {
													$scope.shiftNoData = true;
													return;
												} else {
													$scope.shiftNoData = false;
												}

												if ($scope.shiftData.length > 10) {
													$scope.shiftTab = true;
												} else {
													$scope.shiftTab = false;
												}
												$scope.totalItems = $scope.shiftData.length;												

												$scope.getTotalPages();

												$scope.currentPage = 1;
												if ($scope.shiftData.length > $scope.numPerPage) {
													$scope.shiftViewData = $scope.shiftData
															.slice(
																	0,
																	$scope.numPerPage);
													$scope.hasPrevious = true;
													$scope.isFirstPage = true;
													$scope.hasNext = false;
													$scope.isLastPage = false;
												} else {
													$scope.shiftViewData = $scope.shiftData;
												}
											}

										},function(error) {
											$rootScope.hideloading('#loadingBar');
											$rootScope.fnHttpError(error);
										});
					}

					$scope.getShiftData();

					 $("#id_shiftName").keypress(
								function(e) {
									// if the letter is not alphabets then display error
									// and don't type anything
									$return = ((e.which > 64) && (e.which < 91)) || ((e.which> 96) && (e.which < 123)) ||(e.which == 8) || (e.which == 32)  || (e.which >= 48 && e.which <= 57)
									if (!$return)
														
									{
										
										return false;
									}
								});		
					
					$scope.fnAddShift = function() {
						if ($scope.shiftData.length >= MAX_SHIFT_RECORDS) {
							$rootScope.alertDialog("Maximum records reached");
							return;
						}

						var name = document.getElementById("id_shiftName");
						var stime = document.getElementById("stime");
						var etime = document.getElementById("etime");
						if (name.value.length == 0) {
							$scope.nameError = true;
							$scope.name_error_msg = "Please enter the name";
							$timeout(function() {
								$scope.nameError = false;

							}, 2000);

							return;
						} else if (stime.value.length == 0) {
							$scope.stimeError = true;
							$scope.stime_error_msg = "Please enter the start time";
							$timeout(function() {
								$scope.stimeError = false;
							}, 2000);

							return;
						} else if (etime.value.length == 0) {
							$scope.etimeError = true;
							$scope.etime_error_msg = "Please enter the end time";
							$timeout(function() {
								$scope.etimeError = false;
							}, 2000);

							return;
						}
						if (!$rootScope.checkNetconnection()) { 
							$rootScope.alertDialogChecknet();
							return;
							}
						if ($scope.addshiftForm.$valid) {

							$rootScope.showloading('#loadingBar');

							$http({
								method : 'POST',
								url : './shift',
								data : $scope.addshift,

							})
									.then(
											function(response) {
												if (response != null
														&& response.data != null
														&& response.data != "BAD_REQUEST") {
													$rootScope
															.hideloading('#loadingBar');
													$scope.shift = response.data;
													$rootScope.alertDialog(response.data.message);
													$timeout(function() {
														$scope.getShiftData();
														$scope.cancelShift();
													}, 2000);
												} else {
													$rootScope.hideloading('#loadingBar');
													$rootScope.alertDialog(response.data.message);
													$timeout(function() {
														$scope.cancelShift();
													}, 2000);
												}
											},function(error) {
												$rootScope.hideloading('#loadingBar');
												$rootScope.fnHttpError(error);
											});
						}

					}

					$scope.rowHighilited = function(row) {

						$scope.selectedRow = row;

						$scope.addshift.shiftName = $scope.shiftViewData[row].shiftName;
						$scope.addshift.shiftType = $scope.shiftViewData[row].shiftType;
						$scope.addshift.shiftStartTime = $scope.shiftViewData[row].shiftStartTime;
						$scope.addshift.shiftEndTime = $scope.shiftViewData[row].shiftEndTime;

						$scope.shiftName = true;
						$scope.bShowUpdateBtn = true;
						$scope.bShowDeleteBtn = true;
						$scope.bShowAddBtn = false;

					}
					$scope.fnUpdateShift = function() {
						
						var name = document.getElementById("id_shiftName");
						var stime = document.getElementById("stime");
						var etime = document.getElementById("etime");
						if (name.value.length == 0) {
							$scope.nameError = true;
							$scope.name_error_msg = "Please enter the name";
							$timeout(function() {
								$scope.nameError = false;

							}, 2000);

							return;
						} else if (stime.value.length == 0) {
							$scope.stimeError = true;
							$scope.stime_error_msg = "Please enter the start time";
							$timeout(function() {
								$scope.stimeError = false;
							}, 2000);

							return;
						} else if (etime.value.length == 0) {
							$scope.etimeError = true;
							$scope.etime_error_msg = "Please enter the end time";
							$timeout(function() {
								$scope.etimeError = false;
							}, 2000);

							return;
						}
						if (!$rootScope.checkNetconnection()) { 
							$rootScope.alertDialogChecknet();
							return;
							}
						if ($scope.addshiftForm.$valid) {
							$rootScope.showloading('#loadingBar');

							$http({
								method : 'PUT',
								url : './shift',
								data : $scope.addshift,

							})
									.then(
											function(response) {
												if (response != null
														&& response.data != null
														&& response.data != "BAD_REQUEST") {
													$rootScope
															.hideloading('#loadingBar');
													$scope.shift = response.data;
													$rootScope.alertDialog(response.data.message);
													$timeout(function() {
														$scope.getShiftData();
														$scope.cancelShift();
													}, 2000);
												} else {
													$rootScope.hideloading('#loadingBar');
													$rootScope.alertDialog(response.data.message);
													$timeout(function() {
														$scope.cancelShift();
													}, 2000);
												}
											},function(error) {
												$rootScope.hideloading('#loadingBar');
												$rootScope.fnHttpError(error);
											});
						}

					}

					$scope.fnDeleteShift = function() {
						
						var name = document.getElementById("id_shiftName");
						
						if (name.value.length == 0) {
							$scope.nameError = true;
							$scope.name_error_msg = "Please enter the name";
							$timeout(function() {
								$scope.nameError = false;

							}, 2000);

							return;
						} 
						
						if (!$rootScope.checkNetconnection()) { 
							$rootScope.alertDialogChecknet();
							return;
							}
						if ($scope.addshiftForm.$valid) {
							$
									.confirm({
										title : $rootScope.MSGBOX_TITLE,
										content : 'Are you sure you want to delete this item?',
										type : 'blue',
										columnClass : 'medium',
										autoClose : false,
										icon : 'fa fa-info-circle',
										buttons : {
											yes : {
												text : 'Yes',
												btnClass : 'btn-blue',
												action : function() {
													$rootScope
															.showloading('#loadingBar');
													var shiftName = $scope.addshift.shiftName
													data = {
														"shiftName" : shiftName
													}
													$http(
															{
																method : 'DELETE',
																url : './shift',
																headers : {
																	'Content-Type' : 'application/json'
																},
																data : data,

															})
															.then(
																	function(response) {
																		if (response != null
																				&& response.data != null
																				&& response.data != "BAD_REQUEST") {
																			$rootScope
																					.hideloading('#loadingBar');
																			$scope.shift = response.data;
																			$rootScope.alertDialog(response.data.message);
																			$scope.getShiftData();
																			$scope.cancelShift();

																		} else {
																			$rootScope.hideloading('#loadingBar');
																			$rootScope.alertDialog(response.data.message);
																			$timeout(
																					function() {
																						$scope
																								.cancelShift();
																					},
																					2000);
																		}
																	},function(error) {
																		$rootScope.hideloading('#loadingBar');
																		$rootScope.fnHttpError(error);
																	});
												}
											},
											no : function() {

											}
										}
									});
						}

					}
					$scope.cancelShift = function() {

						$scope.addshift = {};
						$scope.shiftName = false;
						$scope.addshift.shiftType = $scope.configurationShiftType[0];
						$scope.bShowUpdateBtn = false;
						$scope.bShowDeleteBtn = false;
						$scope.bShowAddBtn = true;
						$scope.selectedRow = -1;
					}

					// pagination button

					$scope.currentPage = 1;
					$scope.numPerPage = 10;
					$scope.maxSize = 3;
					$scope.totalItems = 0;
					$scope.shiftFilterList = [];
					$scope.shiftViewData = [];
					$scope
							.$watch(
									'currentPage + numPerPage',
									function() {
										var begin = (($scope.currentPage - 1) * $scope.numPerPage), end = begin
												+ $scope.numPerPage;
										if ($scope.searchText == '') // kumar
											// 02/July
											// for
											// filter
											// text
											$scope.shiftViewData = $scope.shiftData
													.slice(begin, end);
										else
											$scope.shiftViewData = $scope.shiftDataFilterList
													.slice(begin, end);
									});

					$scope.currentPage = 1;

					$scope.maxSize = 3;

					$scope.numPerPage = 10;
					$scope.totalItems = 0;
					$scope.totalPages = 0;

					$scope.hasNext = true;
					$scope.hasPrevious = true;
					$scope.isFirstPage = true;
					$scope.isLastPage = true;
					$scope.serial = 1;

					$scope
							.$watch(
									"currentPage",
									function(newVal, oldVal) {
										if (newVal !== oldVal) {

											if (newVal > $scope.totalPages) {
												$scope.currentPage = $scope.totalPages;
											} else {
												$scope.currentPage = newVal;
											}

											$scope.serial = (($scope.currentPage - 1) * $scope.numPerPage) + 1;

										}
									});

					$scope.enablePageButtons = function() {
						$scope.serial = (($scope.currentPage - 1) * $scope.numPerPage) + 1;
						$scope.selectedRow = -1;
						if ($scope.totalPages <= 1) {
							$scope.hasPrevious = true;
							$scope.isFirstPage = true;
							$scope.hasNext = true;
							$scope.isLastPage = true;

						} else if ($scope.totalPages > 1
								&& $scope.currentPage == $scope.totalPages) {

							$scope.hasPrevious = false;
							$scope.isFirstPage = false;
							$scope.hasNext = true;
							$scope.isLastPage = true;
						} else if ($scope.currentPage != 1
								&& $scope.currentPage < $scope.totalPages) {

							$scope.hasPrevious = false;
							$scope.isFirstPage = false;
							$scope.hasNext = false;
							$scope.isLastPage = false;

						} else {
							$scope.hasPrevious = true;
							$scope.isFirstPage = true;
							$scope.hasNext = false;
							$scope.isLastPage = false;
						}
					};

					$scope.getTotalPages = function() {

						if (($scope.totalItems % $scope.numPerPage) > 0)
							$scope.totalPages = parseInt($scope.totalItems
									/ $scope.numPerPage) + 1;
						else
							$scope.totalPages = parseInt($scope.totalItems
									/ $scope.numPerPage);

						$scope.enablePageButtons();

					};

					$scope.goFirstPage = function() {
						$scope.currentPage = 1;
						$scope.enablePageButtons();
					};

					$scope.goPreviousPage = function() {
						if ($scope.currentPage > 1)
							$scope.currentPage = $scope.currentPage - 1;
						$scope.enablePageButtons();
					};

					$scope.goNextPage = function() {
						if ($scope.currentPage <= $scope.totalPages)
							$scope.currentPage = $scope.currentPage + 1;
						$scope.enablePageButtons();
					};

					$scope.goLastPage = function() {
						$scope.currentPage = $scope.totalPages;
						$scope.enablePageButtons();
					};

				 //from shift screen
			
					
		});
