app
	.controller(
		'assetVehicleController',
		function($scope, $http, $rootScope, $window, $location,$timeout) {
			$scope.user = JSON.parse($window.sessionStorage
					.getItem("user"));
			if ($scope.user == null) {
				$location.path('/login');
				return;
			}
			 // Timepicker
  			$scope.add=true;
  			
			$scope.update=false;
	     	$scope.delete=false;
            
            $scope.addassetVehicle={};
            $scope.assetVehicleData = new Array();
			$scope.assetVehicleViewData = new Array();
			$scope.assetVehicleDataFilterList = new Array();
			$scope.searchText = "";
			 
			$scope.assignedTagMacId = '';
			$scope.trackMacId = '';
			$scope.showLocateBtn = false;
			
			//$scope.user = JSON.parse($window.sessionStorage
			//		.getItem("user"));
			
			if ($scope.user.capabilityDetails != undefined
					&& $scope.user.capabilityDetails != null) {
				if ($scope.user.capabilityDetails.ASLV == 1) {
					$scope.showLocateBtn = true;
				}  
			}
			
			$scope.searchOption = function() {
				$scope.filterOption();
			}

			$scope.filterOption = function() {

				$scope.assetVehicleViewData = [];
				$scope.assetVehicleDataFilterList = [];
 
				
				for (var i = 0; i < $scope.assetVehicleData.length; i++) {
					

					if ($scope.searchText == '') {
						$scope.assetVehicleDataFilterList
								.push($scope.assetVehicleData[i]);
					} else { 
						if ($scope.assetVehicleData[i].assetType.toLowerCase().includes($scope.searchText.toLowerCase())
						|| $scope.assetVehicleData[i].subType.toLowerCase().includes($scope.searchText.toLowerCase())
						|| $scope.assetVehicleData[i].assetId.toLowerCase().includes($scope.searchText.toLowerCase()) ) 
						{
							$scope.assetVehicleDataFilterList.push($scope.assetVehicleData[i]);

						} 
					} 
				}

				$scope.totalItems = $scope.assetVehicleDataFilterList.length;
				$scope.currentPage = 1;
				$scope.getTotalPages();

				 $scope.getAssignedTagIds($scope.assetVehicleDataFilterList);
				 
				if ($scope.assetVehicleData.length == 0
						|| $scope.assetVehicleDataFilterList.length == 0) {
					$scope.assetNoData = true; 
				} else {
					
					$scope.assetNoData = false; 
					if ($scope.assetVehicleDataFilterList.length > $scope.numPerPage){
						$scope.assetVehicleViewData = $scope.assetVehicleDataFilterList
								.slice(0, $scope.numPerPage);
						$scope.assetTab	=true; // pagination tab
					}
					else {
						$scope.assetVehicleViewData = $scope.assetVehicleDataFilterList;
						$scope.assetTab=false; // pagination tab
					} 
				
				}
			}
			
			// kumar 02/Jul filter option <-
			// input field validation
			
			$("#astId, #astType, #sbType").keypress(
					function(e) {
						// if the letter is not alphabets then display error
						// and don't type anything
						$return = ((e.which > 64) && (e.which < 91)) || ((e.which> 96) && (e.which < 123)) ||(e.which == 8) || (e.which == 32)  || (e.which >= 48 && e.which <= 57)
						if (!$return)
											
						{
							
							return false;
						}
					});	
			
			$scope.getAssetVehicleData = function() {
				
				if (!$rootScope.checkNetconnection()) { 
					$rootScope.alertDialogChecknet();
					return;
					}
						$rootScope.showloading('#loadingBar');
						$http({
							method : 'GET',
							url : './assetVehicle',
							
						})
								.then(
										function(response) {
											$rootScope.hideloading('#loadingBar');
											if (response != null
													&& response.data != null
													&& response.data.length != 0
													&& response.data != "BAD_REQUEST"
													&& response.data.data != undefined
													&& response.data.data.length != 0) {
												
												$scope.assetVehicleData.length = 0;

												for (var i = 0; i < response.data.data.length; i++) {
													if (response.data.data[i].active == true) {
														$scope.assetVehicleData
																.push(response.data.data[i]);
													}
												}
												if ($scope.assetVehicleData.length == 0) {
													$scope.assetNoData = true;
													return;
												} else {
													$scope.assetNoData = false;
												}
												
												
												if($scope.assetVehicleData.length > 10){
													$scope.assetTab=true;
												}else{
													$scope.assetTab=false;
												}
												$scope.totalItems=$scope.assetVehicleData.length;
												$scope.getTotalPages();
							
												$scope.getAssignedTagIds($scope.assetVehicleData);
												
									$scope.currentPage=1;
									if ($scope.assetVehicleData.length > $scope.numPerPage){
										$scope.assetVehicleViewData =$scope.assetVehicleData
										.slice(0, $scope.numPerPage);
										            $scope.hasPrevious = true;
				                                    $scope.isFirstPage = true;
				                                    $scope.hasNext = false;
				                                    $scope.isLastPage = false;
										}
										else{
									$scope.assetVehicleViewData = $scope.assetVehicleData;
									}
									}else{
										$scope.assetNoData = true;
									}
									
					},
					function(error) {
						$rootScope.hideloading('#loadingBar');
						$rootScope.fnHttpError(error);
					});
					}
                
				  $scope.getAssetVehicleData();
		
			      $scope.addAssetVehicle = function() {

						var astId = document.getElementById("astId");
						var astType = document.getElementById("astType");
						var sbType = document.getElementById("sbType");

						if (astId.value.length == 0) {
							$scope.astIdError = true;
							$scope.astId_error_msg = "Please enter the ID";
							$timeout(function() {
								$scope.astIdError = false;
							}, 2000);
							return;
						}else if (astType.value.length == 0) {
							$scope.astTypeError = true;
							$scope.astType_error_msg = "Please select the type";
							$timeout(function() {
								$scope.astTypeError = false;
							}, 2000);
							return;
						}else if (sbType.value.length == 0) {
							$scope.sbTypeError = true;
							$scope.sbType_error_msg = "Please enter the sub type";
							$timeout(function() {
								$scope.sbTypeError = false;
							}, 2000);
							return;
						}
						if (!$rootScope.checkNetconnection()) { 
							$rootScope.alertDialogChecknet();
							return;
							}
						if ($scope.addassetForm.$valid) {
                          
							$rootScope.showloading('#loadingBar');
			
							$http({
								method : 'POST',
								url : './assetVehicle',
								data : $scope.addassetVehicle,
								
							})
									.then(
											function(response) {
												if (response != null
														&& response.data != null
														&& response.data != "BAD_REQUEST") {
													$rootScope.hideloading('#loadingBar');
													$scope.assetVehicle = response.data;
													$rootScope.showAlertDialog( response.data.message) ;
													$timeout(
															function() {
																 $scope.getAssetVehicleData();
																$scope.cancelAssetVehicle();

															}, 2000);
												} else {
													$rootScope
															.hideloading('#loadingBar');
													$rootScope.showAlertDialog(response.data.message);
													$timeout(function() {
                                                       $scope.cancelAssetVehicle();													
                                                           }, 2000);
												}
											},function(error) {
												$rootScope.hideloading('#loadingBar');
												$rootScope.fnHttpError(error);
											});
						}

				
					}
			      
			      $scope.trackTag = function() {
			    	  if($scope.assignedTagMacId!=undefined && $scope.assignedTagMacId!=null 
								&& $scope.assignedTagMacId!='')
						{
			    		    $rootScope.trackingMacId = $scope.assignedTagMacId;
							$rootScope.showFullView();
							var currentMenuName = $window.sessionStorage.getItem("menuName");
							if ($scope.user.role == "DemoUser")
							{
								$window.sessionStorage.setItem("menuName", "shopFloorAnalysis");
								$window.sessionStorage.setItem("demoMenuName", "assetLiveView");
								$location.path('/shopFloorAnalysis');
							}else{
								$window.sessionStorage.setItem("menuName", "assetLiveView");
								$location.path('/assetLiveView'); 
							}
							$rootScope.$broadcast('sourcePreviousScreen',currentMenuName);
						}else{
							toastr.error('No assigned tags to track.', "EI4.0");  
						}
						 
					}
			      
			      $scope.trackSingleTag = function() {
						if ($scope.trackMacId != undefined
								&& $scope.trackMacId != null
								&& $scope.trackMacId != '') {
							 $rootScope.trackingMacId = $scope.trackMacId;
							$rootScope.showFullView();
							var currentMenuName = $window.sessionStorage.getItem("menuName");
							if ($scope.user.role == "DemoUser")
							{
								$window.sessionStorage.setItem("menuName", "shopFloorAnalysis");
								$window.sessionStorage.setItem("demoMenuName", "assetLiveView");
								$location.path('/shopFloorAnalysis');
							}else{
								$window.sessionStorage.setItem("menuName",
										"assetLiveView");
								$location.path('/assetLiveView');
							}
							$rootScope.$broadcast('sourcePreviousScreen',currentMenuName);
						} else {
							toastr.error('No assigned tags to track.', "EI4.0");
						}
					}
				
                 $scope.rowHighilited = function(row) {
	
	                    $scope.selectedRow = row;
	
		                $scope.addassetVehicle.assetId = $scope.assetVehicleViewData[row].assetId;
						$scope.addassetVehicle.assetType = $scope.assetVehicleViewData[row].assetType;
						$scope.addassetVehicle.subType = $scope.assetVehicleViewData[row].subType;
						
						if($scope.assetVehicleViewData[row].assignedTagMacId !=undefined 
								&& $scope.assetVehicleViewData[row].assignedTagMacId !=''){
							$scope.trackMacId = $scope.assetVehicleViewData[row].assignedTagMacId;  
						}else
						{
							$scope.trackMacId = '';
						}
						 
						$scope.assetID=true;
						$scope.update=true;
						$scope.delete=true;
						$scope.add=false;
						
						}
                 
                 $scope.getAssignedTagIds = function(assetList) { 
     				
						$scope.assignedTagMacId = '';
						
						if(assetList!=undefined && assetList!=null){
							for(var i=0; i<assetList.length; i++){ 
								if(assetList[i].assignedTagMacId !=undefined 
										&& assetList[i].assignedTagMacId !=''){
									if($scope.assignedTagMacId=='')
										$scope.assignedTagMacId = assetList[i].assignedTagMacId;
									else
										$scope.assignedTagMacId = $scope.assignedTagMacId+","+assetList[i].assignedTagMacId;
										
								} 
							 } 
						}
						 
					}
                 
				 $scope.updateAssetVehicle = function() {		
						
						var astId = document.getElementById("astId");
						var astType = document.getElementById("astType");
						var sbType = document.getElementById("sbType");

						if (astId.value.length == 0) {
							$scope.astIdError = true;
							$scope.astId_error_msg = "Please enter the ID";
							$timeout(function() {
								$scope.astIdError = false;
							}, 2000);
							return;
						}else if (astType.value.length == 0) {
							$scope.astTypeError = true;
							$scope.astType_error_msg = "Please select the type";
							$timeout(function() {
								$scope.astTypeError = false;
							}, 2000);
							return;
						}else if (sbType.value.length == 0) {
							$scope.sbTypeError = true;
							$scope.sbType_error_msg = "Please enter the sub type";
							$timeout(function() {
								$scope.sbTypeError = false;
							}, 2000);
							return;
						}
						if (!$rootScope.checkNetconnection()) { 
							$rootScope.alertDialogChecknet();
							return;
							}
						if ($scope.addassetForm.$valid) {
							$rootScope.showloading('#loadingBar');
			
							$http({
								method : 'PUT',
								url : './assetVehicle',
								data : $scope.addassetVehicle,
								
							})
									.then(
											function(response) {
												if (response != null
														&& response.data != null
														&& response.data != "BAD_REQUEST") {
													$rootScope.hideloading('#loadingBar');
													$scope.assetVehicle = response.data;
													$rootScope.showAlertDialog(response.data.message);
													$timeout(
															function() {
																 $scope.getAssetVehicleData();
																 $scope.cancelAssetVehicle();
															}, 2000);
												} else {
													$rootScope.hideloading('#loadingBar');
													$rootScope.showAlertDialog(response.data.message);
													$timeout(function() {
																$scope.cancelAssetVehicle();
													}, 2000);
												}
											},function(error) {
												$rootScope.hideloading('#loadingBar');
												$rootScope.fnHttpError(error);
											});
						}

				
					}
									
				 $scope.deleteAssetVehicle = function() {		

						var astId = document.getElementById("astId");

						if (astId.value.length == 0) {
							$scope.astIdError = true;
							$scope.astId_error_msg = "Please enter the ID";
							$timeout(function() {
								$scope.astIdError = false;
							}, 2000);
							return;
						}
						if (!$rootScope.checkNetconnection()) { 
							$rootScope.alertDialogChecknet();
							return;
							}
						if ($scope.addassetForm.$valid) {
							$.confirm({
										title : $rootScope.MSGBOX_TITLE,
										content : 'Are you sure you want to delete this item?',
										type: 'blue',
										columnClass:'medium',
										autoClose : false,
                         				icon: 'fa fa-info-circle',
                                        buttons: {
										yes: {
										text: 'Yes', 
										btnClass: 'btn-blue',
										action : function() {							
							$rootScope.showloading('#loadingBar');
							var assetId = $scope.addassetVehicle.assetId
						     data = {
							 "assetId" : assetId 
						    }
							$http({
								method : 'DELETE',
								url : './assetVehicle',
								headers: {
								'Content-Type':'application/json'
								},
								data : data,
								
							})
									.then(
											function(response) {
												if (response != null
														&& response.data != null
														&& response.data != "BAD_REQUEST") {
													$rootScope.hideloading('#loadingBar');
													$scope.assetVehicle = response.data;
													$rootScope.showAlertDialog(response.data.message);
													$timeout(
															function() {
																 $scope.getAssetVehicleData();
																$scope.cancelAssetVehicle();
															}, 2000);
												} else {
													$rootScope.hideloading('#loadingBar');
													$rootScope.showAlertDialog(response.data.message);
													$timeout(function() {
														$scope.cancelAssetVehicle();
													}, 2000);
												}
											},function(error) {
												$rootScope.hideloading('#loadingBar');
												$rootScope.fnHttpError(error);
											});
											}
											  },
									   no:function () {
								           
										   		}
											}
									});
						}

				
					}
				 $scope.cancelAssetVehicle = function() {	
						$scope.addassetVehicle={};
						$scope.assetID=false;
						$scope.update=false;
						$scope.delete=false;
						$scope.add=true;
						$scope.selectedRow = -1;
						$scope.trackMacId  = '';
						 
						
					}
					
		// pagination button
		
		$scope.currentPage = 1;
		$scope.numPerPage = 10;
		$scope.maxSize = 3;
		$scope.totalItems = 0;
		 $scope.assetVehicleDataFilterList=[];
		 $scope.assetVehicleViewData=[];
		$scope
				.$watch(
						'currentPage + numPerPage',
						function() {
							var begin = (($scope.currentPage - 1) * $scope.numPerPage), end = begin
									+ $scope.numPerPage;
							if ($scope.searchText == '')
								$scope.assetVehicleViewData = $scope.assetVehicleData
										.slice(begin, end);
							else
								$scope.assetVehicleViewData = $scope.assetVehicleDataFilterList
										.slice(begin, end);
							$scope.enablePageButtons();
						});

		$scope.currentPage = 1;

		$scope.maxSize = 3;

		$scope.numPerPage = 10;
		$scope.totalItems = 0;
		$scope.totalPages = 0;

		$scope.hasNext = true;
		$scope.hasPrevious = true;
		$scope.isFirstPage = true;
		$scope.isLastPage = true;
		$scope.serial = 1;

		$scope
				.$watch(
						"currentPage",
						function(newVal, oldVal) {
							if (newVal !== oldVal) {

								if (newVal > $scope.totalPages) {
									$scope.currentPage = $scope.totalPages;
								} else {
									$scope.currentPage = newVal;
								}

								$scope.serial = (($scope.currentPage - 1) * $scope.numPerPage) + 1;
								$scope.enablePageButtons();
							}
						});

		$scope.enablePageButtons = function() {
			$scope.serial = (($scope.currentPage - 1) * $scope.numPerPage) + 1;
			$scope.selectedRow = -1;
			if ($scope.totalPages <= 1) {
				$scope.hasPrevious = true;
				$scope.isFirstPage = true;
				$scope.hasNext = true;
				$scope.isLastPage = true;

			} else if ($scope.totalPages > 1
					&& $scope.currentPage == $scope.totalPages) {

				$scope.hasPrevious = false;
				$scope.isFirstPage = false;
				$scope.hasNext = true;
				$scope.isLastPage = true;
			} else if ($scope.currentPage != 1
					&& $scope.currentPage < $scope.totalPages) {

				$scope.hasPrevious = false;
				$scope.isFirstPage = false;
				$scope.hasNext = false;
				$scope.isLastPage = false;

			} else {
				$scope.hasPrevious = true;
				$scope.isFirstPage = true;
				$scope.hasNext = false;
				$scope.isLastPage = false;
			}
			 
		};

		$scope.getTotalPages = function() {

			if (($scope.totalItems % $scope.numPerPage) > 0)
				$scope.totalPages = parseInt($scope.totalItems
						/ $scope.numPerPage) + 1;
			else
				$scope.totalPages = parseInt($scope.totalItems
						/ $scope.numPerPage);

			$scope.enablePageButtons();

		};

		$scope.goFirstPage = function() {
			$scope.currentPage = 1;
			$scope.enablePageButtons();
		};

		$scope.goPreviousPage = function() {
			if ($scope.currentPage > 1)
				$scope.currentPage = $scope.currentPage - 1;
			$scope.enablePageButtons();
		};

		$scope.goNextPage = function() {
			if ($scope.currentPage <= $scope.totalPages)
				$scope.currentPage = $scope.currentPage + 1;
			$scope.enablePageButtons();
		};

		$scope.goLastPage = function() {
			$scope.currentPage = $scope.totalPages;
			$scope.enablePageButtons();
		};				
					
		});
