app
		.controller(
				'assetNonChronoController',
				function($scope, $http, $window, $rootScope, $location,
						$timeout) {
					//$rootScope.$broadcast('updatemenu', "admin");

					// date picker and date time picker format
					/*
					 * $('#datetimepicker4').datetimepicker({ format :
					 * 'YYYY-MMM-DD', // defaultDate: new Date(), minDate : new
					 * Date() });
					 */
					$scope.user = JSON.parse($window.sessionStorage
							.getItem("user"));
					if ($scope.user == null) {
						$location.path('/login');
						return;
					}
					 $scope.nonChronoAssetType =["Tools", "Gages", "Fixtures", "Supplies"];
					
					$('#id_nonChronoAssignedDate').datetimepicker({
						format : 'YYYY-MMM-DD',
						// defaultDate: new Date(),
						minDate : new Date()
					});

					$scope.assetNonChronoData = new Array();
					$scope.assetNonChronoViewData = new Array();
					$scope.assetNonChronoDataFilterList = new Array();
					$scope.fetchAssetIdAPIData = new Array();

					$scope.firstSearchANDDataArray = new Array();
					$scope.secondSearchORDataArray = new Array();
					$scope.firstSearchText = "";
					$scope.secondSearchText = "";

					$scope.assignedTagMacId = '';
					$scope.trackMacId = '';

					$scope.searchText = "";
					$scope.NonchronoEditSelectScreen = false;
					$scope.NonchronoEditUpdateScreen = false;
					$scope.NonchronoViewScreen = true;
					$scope.assetNonChronoAssignReturnFetchScreen = false;
					$scope.assetNonChronoAssignScreen = false;
					$scope.assetNonChronoReturnScreen = false;
					$scope.searchButton = true;
					// $scope.calibrationData =[];
					$scope.fetchAssetIdAPIData = new Array();
					$scope.assetIdTable = [];

					$('.select2').select2(); // searchable dropdown in fetch
												// screen intialization
					var today = new Date();
					if ($scope.authen == undefined) {
						$scope.authen = {};
					}
					$scope.authen.type = $scope.nonChronoAssetType[0];
					$scope.authen.calibrationId = "None";
					// $rootScope.showloading('#loadingBar');

					// input field validation
					$(
							"#id_assetIdAddAsset, #id_assetIdEditAsset, #id_assetTypeAddAsset, #id_assetTypeEditAsset, #id_subTypeAddAsset, #id_subTypeEditAsset, #id_rangeAddAsset, #id_rangeEditAsset,  #id_calibrationIdAddAsset, #id_calibrationIdEditAsset, #id_fetchAssetId")
							.keypress(
									function(e) {
										// if the letter is not alphabets then
										// display error
										// and don't type anything
										$return = ((e.which > 64) && (e.which < 91))
												|| ((e.which > 96) && (e.which < 123))
												|| (e.which == 8)
												|| (e.which == 32)
												|| (e.which >= 48 && e.which <= 57)
										if (!$return)

										{

											return false;
										}
									});

					$("#id_manufacturerAddAsset, #id_manufacturerEditAsset").keypress(
							function(e) {
								// if the letter is not alphabets then display
								// error
								// and don't type anything
								$return = ((e.which > 64) && (e.which < 91)) 
								|| ((e.which > 43) && (e.which < 47))
								|| ((e.which> 96) && (e.which < 123)) 
								||(e.which == 8) || (e.which == 32)  
								|| (e.which == 95)|| (e.which == 38)
								|| (e.which >= 48 && e.which <= 57)
								if (!$return)
													
								{
									
									return false;
								}
							});	
					$scope.showLocateBtn = false;
					
					// to enable/disable sub menus
					$scope.user = JSON.parse($window.sessionStorage
							.getItem("user"));

					if ($scope.user.capabilityDetails != undefined
							&& $scope.user.capabilityDetails != null) {
						if ($scope.user.capabilityDetails.ANVA == 1) {
							$scope.showViewAllBtn = true;
						} else {
							$scope.showViewAllBtn = false;
						}
						if ($scope.user.capabilityDetails.ANAD == 1) {
							$scope.showAddBtn = true;
						} else {
							$scope.showAddBtn = false;
						}
						if ($scope.user.capabilityDetails.ANED == 1) {
							$scope.showEditBtn = true;
						} else {
							$scope.showEditBtn = false;
						}
						if ($scope.user.capabilityDetails.ANAR == 1) {
							$scope.showAssignReturnBtn = true;
						} else {
							$scope.showAssignReturnBtn = false;
						}
						
						if ($scope.user.capabilityDetails.ASLV == 1) {
							$scope.showLocateBtn = true;
						}

					}
					
					$scope.trackTag = function() {
						if ($scope.assignedTagMacId != undefined
								&& $scope.assignedTagMacId != null
								&& $scope.assignedTagMacId != '') {
							$rootScope.trackingMacId = $scope.assignedTagMacId;
							$rootScope.showFullView();
							var currentMenuName = $window.sessionStorage.getItem("menuName");
							if ($scope.user.role == "DemoUser")
							{
								$window.sessionStorage.setItem("menuName", "shopFloorAnalysis");
								$window.sessionStorage.setItem("demoMenuName", "assetLiveView");
								$location.path('/shopFloorAnalysis');
							}else{
								$window.sessionStorage.setItem("menuName",
										"assetLiveView");
								$location.path('/assetLiveView');
							}
							$rootScope.$broadcast('sourcePreviousScreen',currentMenuName);
						} else {
							toastr.error('No assigned tags to track.', "EI4.0");
						}

					}

					$scope.trackSingleTag = function() {
						if ($scope.trackMacId != undefined
								&& $scope.trackMacId != null
								&& $scope.trackMacId != '') {
							$rootScope.showFullView();
							$rootScope.trackingMacId = $scope.trackMacId;
							var currentMenuName = $window.sessionStorage.getItem("menuName");
							if ($scope.user.role == "DemoUser")
							{
								$window.sessionStorage.setItem("menuName", "shopFloorAnalysis");
								$window.sessionStorage.setItem("demoMenuName", "assetLiveView");
								$location.path('/shopFloorAnalysis');
							}else{
								$window.sessionStorage.setItem("menuName",
										"assetLiveView");
								$location.path('/assetLiveView');
							}
							$rootScope.$broadcast('sourcePreviousScreen',currentMenuName);
						} else {
							toastr.error('No assigned tags to track.', "EI4.0");
						}
					}

					$scope.searchOption = function() {
						$scope.filterOption();
					}

					$scope.filterOption = function() {
						$scope.assetNonChronoViewData = [];
						$scope.assetNonChronoDataFilterList = [];

						for (var i = 0; i < $scope.assetNonChronoData.length; i++) {

							if ($scope.searchText == '') {
								$scope.assetNonChronoDataFilterList
										.push($scope.assetNonChronoData[i]);
							} else {
								if (($scope.assetNonChronoData[i].assetId != undefined
										&& $scope.assetNonChronoData[i].assetId != null ? $scope.assetNonChronoData[i].assetId
										: "").toLowerCase().includes(
										$scope.searchText.toLowerCase())
										|| ($scope.assetNonChronoData[i].assetType != undefined
												&& $scope.assetNonChronoData[i].assetType != null ? $scope.assetNonChronoData[i].assetType
												: "").toLowerCase()
												.includes(
														$scope.searchText
																.toLowerCase())
										|| ($scope.assetNonChronoData[i].subType != undefined
												&& $scope.assetNonChronoData[i].subType != null ? $scope.assetNonChronoData[i].subType
												: "").toLowerCase()
												.includes(
														$scope.searchText
																.toLowerCase())
										|| ($scope.assetNonChronoData[i].type != undefined
												&& $scope.assetNonChronoData[i].type != null ? $scope.assetNonChronoData[i].type
												: "").toLowerCase()
												.includes(
														$scope.searchText
																.toLowerCase())
										|| ($scope.assetNonChronoData[i].rangeValue != undefined
												&& $scope.assetNonChronoData[i].rangeValue != null ? $scope.assetNonChronoData[i].rangeValue
												: "").toLowerCase()
												.includes(
														$scope.searchText
																.toLowerCase())
										|| ($scope.assetNonChronoData[i].manufacturer != undefined
												&& $scope.assetNonChronoData[i].manufacturer != null ? $scope.assetNonChronoData[i].manufacturer
												: "").toLowerCase()
												.includes(
														$scope.searchText
																.toLowerCase())
										|| ($scope.assetNonChronoData[i].calibrationRecord != undefined
												&& $scope.assetNonChronoData[i].calibrationRecord != null ? $scope.assetNonChronoData[i].calibrationRecord
												: "").toLowerCase()
												.includes(
														$scope.searchText
																.toLowerCase())
										|| ($scope.assetNonChronoData[i].calibrationRecord != undefined
												&& $scope.assetNonChronoData[i].calibrationRecord != null ? $scope.assetNonChronoData[i].calibrationRecord
												: "").toLowerCase()
												.includes(
														$scope.searchText
																.toLowerCase())
										|| ($scope.assetNonChronoData[i].assignedPerson != undefined
												&& $scope.assetNonChronoData[i].assignedPerson != null ? $scope.assetNonChronoData[i].assignedPerson
												: "").toLowerCase()
												.includes(
														$scope.searchText
																.toLowerCase())
										|| ($scope.assetNonChronoData[i].department != undefined
												&& $scope.assetNonChronoData[i].department != null ? $scope.assetNonChronoData[i].department
												: "").toLowerCase()
												.includes(
														$scope.searchText
																.toLowerCase())
										|| ($scope.assetNonChronoData[i].fromTime != undefined
												&& $scope.assetNonChronoData[i].fromTime != null ? $scope.assetNonChronoData[i].fromTime
												: "").toLowerCase()
												.includes(
														$scope.searchText
																.toLowerCase())
										|| ($scope.assetNonChronoData[i].toTime != undefined
												&& $scope.assetNonChronoData[i].toTime != null ? $scope.assetNonChronoData[i].toTime
												: "").toLowerCase()
												.includes(
														$scope.searchText
																.toLowerCase())) {
									$scope.assetNonChronoDataFilterList
											.push($scope.assetNonChronoData[i]);

								}

							}

						}

						$scope.totalItems = $scope.assetNonChronoDataFilterList.length;
						$scope.currentPage = 1;
						$scope.getTotalPages();
						$scope
								.getAssignedTagIds($scope.assetNonChronoDataFilterList);

						if ($scope.assetNonChronoData.length == 0
								|| $scope.assetNonChronoDataFilterList.length == 0) {
							$scope.assetNonChronoNoData = true;

						} else {

							$scope.assetNonChronoNoData = false;

							if ($scope.assetNonChronoDataFilterList.length > $scope.numPerPage) {
								$scope.assetNonChronoViewData = $scope.assetNonChronoDataFilterList
										.slice(0, $scope.numPerPage);
								$scope.PaginationTab = true;
							} else {
								$scope.assetNonChronoViewData = $scope.assetNonChronoDataFilterList;
								$scope.PaginationTab = false;
							}

						}
					}// kumar 02/Jul filter option <-
					// Assigned person dropdown (dept API call)
					$scope.getStaffDetails = function() {
						if (!$rootScope.checkNetconnection()) { 
							//$rootScope.alertDialogChecknet();
							return;
							}
						$http({
							method : 'GET',
							url : './staff',

						})
								.then(
										function(response) {
											if (response != null
													&& response.data != null
													&& response.data.length != 0
													&& response.data != "BAD_REQUEST"
													&& response.data.data != undefined
													&& response.data.data.length != 0) {

												$scope.empViewData = [];

												for (var i = 0; i < response.data.data.length; i++) {
													if (response.data.data[i].active == true) {
														$scope.empViewData
																.push(response.data.data[i].empId);
													}
												}

											} else {
												$scope.noList = true;
											}
										},function(error) {
											$rootScope.fnHttpError(error);
										});

					}

					// data for calibration id drop down (calibration ID API
					// call )

					/*
					 * $scope.getCalibrationData = function() {
					 *  // $rootScope.showloading('#loadingBar'); $http({ method :
					 * 'GET', url : './calibrationRecord',
					 *  }) .then( function(response) { if (response != null &&
					 * response.data.data != null && response.data.datalength !=
					 * 0 && response.data.data != "BAD_REQUEST" &&
					 * response.data.data != undefined &&
					 * response.data.data.length != 0) {
					 * 
					 * 
					 * $scope.calibrationData.length = 0;
					 * $scope.calibrationData.push("None"); for (var i = 0; i <
					 * response.data.data.length; i++) { if
					 * (response.data.data[i].active == true) {
					 * $scope.calibrationData.push(response.data.data[i].calibrationId); } }
					 *  } }); }
					 */

					// data for department dropdown (dept API call)
					$scope.getDepartmentDetails = function() {
						if (!$rootScope.checkNetconnection()) { 
							//$rootScope.alertDialogChecknet();
							return;
							}
						$http({
							method : 'GET',
							url : './department',

						})
								.then(
										function(response) {
											if (response != null
													&& response.data != null
													&& response.data.length != 0
													&& response.data != "BAD_REQUEST"
													&& response.data != undefined
													&& response.data.length != 0) {

												$scope.departmentList = [];

												for (var i = 0; i < response.data.length; i++) {
													if (response.data[i].active == true) {
														$scope.departmentList
																.push(response.data[i].deptName);
													}
												}

											} else {
												$scope.noList = true;
											}
										},function(error) {
											$rootScope.fnHttpError(error);
										});
					}

					$scope.fetchAsset = function() {
						
						if (!$rootScope.checkNetconnection()) { 
							$rootScope.alertDialogChecknet();
							return;
							}
						var fetchAsset = document
								.getElementById("id_fetchAssetId");
						if (fetchAsset.value.length == 0) {
							$scope.fetchAssetError = true;
							$scope.fetchAsset_error_msg = "Please enter Asset ID ";
							$timeout(function() {
								$scope.fetchAssetError = false;

							}, 2000);

							return;
						}
						$rootScope.showloading('#loadingBar');
						$scope.promise = $http
								.get('./assetNonChrono/' + $scope.fetchAssetId)
								.then(
										function(result) {
											$rootScope.hideloading('#loadingBar');
											$scope.fetchAssetIdAPIData = result;
											if (result.data.status == "success") {

												if (result.data.data[0].assignmentStatus == "NOT_ASSIGNED") {

													$scope.assetNonChronoAssignReturnFetchScreen = false;
													$scope.assetNonChronoAssignScreen = true;
													$scope.assetNonChronoReturnScreen = false;

													$scope.authen.assetId = result.data.data[0].assetId;
													$scope.authen.assetType = result.data.data[0].assetType;
													$scope.authen.subType = result.data.data[0].subType;
													$scope.authen.rangeValue = result.data.data[0].rangeValue;
													$scope.authen.manufacturer = result.data.data[0].manufacturer;
													if (result.data.data[0].calibrationRecord != null)
														$scope.authen.calibrationId = result.data.data[0].calibrationRecord.calibrationId;
													else
														$scope.authen.calibrationId = "None";
													$scope.authen.type = result.data.data[0].type;

													$scope.authen.assignmentStatus = "PERSON";

													$scope.authen.assignedPerson = $scope.empViewData[0];
													$scope.showAssignedPerson = true;
													$scope.showAssignmentDepartment = false;

													$scope.assignedPersonReadonly = false;
													$scope.assignmentDepartmentReadonly = true;

													/*
													 * var startTime = ("0" +
													 * today.getHours()).slice(-2) +
													 * ":" + ("0" +
													 * today.getMinutes()).slice(-2);
													 * var endTime = ("0" +
													 * (today.getHours() + 2))
													 * .slice(-2) + ":" + ("0" +
													 * today.getMinutes()).slice(-2);
													 */
													var startTime = moment(
															new Date())
															.format(
																	'YYYY-MMM-DD HH:mm');
													var endTime = moment(
															new Date())
															.format(
																	'YYYY-MMM-DD HH:mm');
													endTime = moment(new Date())
															.format(
																	'YYYY-MMM-DD ')
															+ ("0" + (today
																	.getHours() + 2))
																	.slice(-2)
															+ ":"
															+ ("0" + today
																	.getMinutes())
																	.slice(-2);
													$("#stime").val(startTime);
													$("#etime").val(endTime);
													$scope.authen.stime = startTime;
													$scope.authen.etime = endTime;
													$(
															"#id_nonChronoAssignedDate")
															.val(
																	moment(
																			new Date())
																			.format(
																					'YYYY-MMM-DD'));
													$scope.authen.date = moment(
															new Date()).format(
															"YYYY-MMM-DD");
												} else if (result.data.data[0].assignmentStatus == "ASSIGNED") {
													var assetAssignmentDetails = result.data.data[0].assetAssignmentDetails;
													var currentAssetAssignment;
													for ( var key in assetAssignmentDetails) {
														currentAssetAssignment = assetAssignmentDetails[key]
														break;
													}

													if (currentAssetAssignment.assignmentType == "PERSON"
															|| currentAssetAssignment.assignmentType == "DEPT") {

														$scope.assetNonChronoAssignReturnFetchScreen = false;
														$scope.assetNonChronoAssignScreen = false;
														$scope.assetNonChronoReturnScreen = true;

														$scope.authen.assetId = result.data.data[0].assetId;
														$scope.authen.assignmentStatus = result.data.data[0].assignmentStatus;
														$(
																"#id_assetIdReturnAsset")
																.val(
																		$scope.authen.assetId);
														$(
																"#id_assignmentStatusReturnAsset")
																.val(
																		$scope.authen.assignmentStatus);

														if (currentAssetAssignment.assignmentType == "PERSON") {
															$scope.authen.assignedTo = currentAssetAssignment.empId;
														} else if (currentAssetAssignment.assignmentType == "DEPT") {
															$scope.authen.assignedTo = currentAssetAssignment.deptName;
														}
														$(
																"#id_assignedToReturnAsset")
																.val(
																		$scope.authen.assignedTo);
													}

												}

											} else if (result.data.status == "error") {
												$rootScope.showAlertDialog(result.data.message);
											}

										},
										function(error) {
											$rootScope.hideloading('#loadingBar');
											$rootScope.fnHttpError(error);
										});

						// }

					}

					$scope.assignmentStatusValue = function() {

						if ($scope.authen.assignmentStatus == "PERSON") {
							$scope.showAssignedPerson = true;
							$scope.showAssignmentDepartment = false;

							$scope.assignedPersonReadonly = false;
							$scope.assignmentDepartmentReadonly = true;
							$scope.authen.assignedPerson = $scope.empViewData[0];
							$scope.authen.assignmentDepartment = "";
						}

						else if ($scope.authen.assignmentStatus == "DEPT") {
							$scope.showAssignedPerson = false;
							$scope.showAssignmentDepartment = true;

							$scope.assignedPersonReadonly = true;
							$scope.assignmentDepartmentReadonly = false;
							$scope.authen.assignmentDepartment = $scope.departmentList[0];
							$scope.authen.assignedPerson = "";

						}

					}

					$scope.assignAssetNonChrono = function() {

						$("#idAssignAssetNonChronoForm")
								.validate(
										{

											// Rules for form validation
											rules : {
												assignmentStatus : {
													required : true
												},
												date : {
													required : true
												},
												stime : {
													required : true
												},
												etime : {
													required : true
												}
											},
											// Messages for form validation
											messages : {
												assignmentStatus : {
													required : 'Please select assignment status',
												},
												date : {
													required : 'Please enter date',
												},
												stime : {
													required : 'Please enter start time '

												},
												etime : {
													required : 'Please enter end time'
												}
											},

											// Do not change code below
											errorPlacement : function(error,
													element) {
												error.insertAfter(element
														.parent());
											}
										});
						if ($scope.assignAssetNonChronoForm.$valid) {
							
							if ($scope.authen.assignmentStatus == "PERSON") {
								var assignAssetNonChronoParameter = {
									"assetId" : $scope.authen.assetId,
									"date" : $scope.authen.date,
									"fromTime" : $scope.authen.stime,
									"toTime" : $scope.authen.etime,
									"assignmentType" : $scope.authen.assignmentStatus,
									"empId" : $scope.authen.assignedPerson

								};
							} else if ($scope.authen.assignmentStatus == "DEPT") {

								var assignAssetNonChronoParameter = {
									"assetId" : $scope.authen.assetId,
									"date" : $scope.authen.date,
									"fromTime" : $scope.authen.stime,
									"toTime" : $scope.authen.etime,
									"assignmentType" : $scope.authen.assignmentStatus,
									"deptName" : $scope.authen.assignmentDepartment

								};
							}
							
							if ($scope.authen.stime > $scope.authen.etime) {
								$rootScope.showAlertDialog("Warning : End time should be later than Start time");
								return;
							}
							if (!$rootScope.checkNetconnection()) { 
								
								$rootScope.alertDialogChecknet();
								return;
								}
							$rootScope.showloading('#loadingBar');
							$scope.promise = $http
									.put('./assignAssetNonChrono',
											assignAssetNonChronoParameter)
									.then(
											function(result) {
												$rootScope.hideloading('#loadingBar');
												if (result.data.status == "success") {

													$scope.assetNonChronoAssignScreen = false;
													$scope.assetNonChronoAssignReturnFetchScreen = true;
													$scope.assetNonChronoReturnScreen = false;
													$rootScope.showAlertDialog(result.data.message);
													
													$scope.cancelAssignAssetNonChrono();

												} else if (result.data.status == "error") {
													$rootScope.showAlertDialog(result.data.message);
												}

											},
											function(error) {
												$rootScope.hideloading('#loadingBar');
												$rootScope.fnHttpError(error);
											});
						}

					}

					$scope.cancelAssignAssetNonChrono = function() {
						// $('#id_fetchAssetId').val("");111111111111
						// $scope.fetchAssetId = $scope.assetIdTable[0];

						$scope.authen.assetId = "";
						$scope.authen.assetType = "";
						$scope.authen.subType = "";
						$scope.authen.rangeValue = "";
						$scope.authen.manufacturer = "";
						$scope.authen.calibrationId = "None";
						$scope.authen.type = $scope.nonChronoAssetType[0];
						$scope.authen.assignedPerson = "";
						$scope.authen.date = "";
						$scope.authen.stime = "";
						$scope.authen.etime = "";

						$scope.assetNonChronoAssignReturnFetchScreen = true;
						$scope.assetNonChronoAssignScreen = false;
						$scope.assetNonChronoReturnScreen = false;
					}
					$scope.cancelreturnAssetNonChrono = function() {
						// $('#id_fetchAssetId').val("");
						// $scope.fetchAssetId = $scope.assetIdTable[0];
						$scope.assetNonChronoAssignReturnFetchScreen = true;
						$scope.assetNonChronoAssignScreen = false;
						$scope.assetNonChronoReturnScreen = false;
					}

					$scope.returnAssetNonChrono = function() {
						

						
						var assetAssignmentDetails = $scope.fetchAssetIdAPIData.data.data[0].assetAssignmentDetails;
						var currentAssetAssignment;
						for ( var key in assetAssignmentDetails) {
							currentAssetAssignment = assetAssignmentDetails[key]
							break;
						}

						if (currentAssetAssignment.assignmentType == "DEPT") {
							var returnAssetNonChronoParameter = {
								"assetId" : $scope.authen.assetId,
								"assignmentType" : currentAssetAssignment.assignmentType,
								"deptName" : $scope.authen.assignedTo
							};
						} else if (currentAssetAssignment.assignmentType == "PERSON") {
							var returnAssetNonChronoParameter = {
								"assetId" : $scope.authen.assetId,
								"assignmentType" : currentAssetAssignment.assignmentType,
								"empId" : $scope.authen.assignedTo
							};
						}
						if (!$rootScope.checkNetconnection()) { 
							$rootScope.alertDialogChecknet();
							return;
							}
						$rootScope.showloading('#loadingBar');
						$scope.promise = $http.post('./returnAssetNonChrono',
								returnAssetNonChronoParameter).then(
								function(result) {
									$rootScope.hideloading('#loadingBar');
									if (result.data.status == "success") {
										$rootScope.showAlertDialog(result.data.message);
										$scope.cancelreturnAssetNonChrono();
									} else if (result.data.status == "error") {
										$rootScope.showAlertDialog(result.data.message);
									
									}

								}, function(error) {
									$rootScope.hideloading('#loadingBar');
									$rootScope.fnHttpError(error);
								});
					}

					$scope.editAssetValue = function() {
						$scope.NonchronoEditSelectScreen = false;
						$scope.NonchronoEditUpdateScreen = true;
						$scope.searchButton = false;
					}
					$scope.updateAssetNonChrono = function() {
						
							
							/* if( $scope.authen.calibrationId =="None") */// if expiry date comes it comes under asset chrono commented after discussion with Dr Anand sir 28-09-21
							$scope.authen.calibrationId = 0;
							
							var assetId = document.getElementById("id_assetIdEditAsset");
							if (assetId.value.length == 0) {
								 $scope.editAssetIdError = true;
								$scope.editAssetId_error_msg = "Please enter the Asset Id";
								$timeout(function() {
									$scope.editAssetIdError = false;
								}, 2000);
								return;
							}
								var assetType = document.getElementById("id_assetTypeEditAsset");
								if (assetType.value.length == 0) {
									 $scope.editTypeError = true;
									$scope.editType_error_msg = "Please enter Type";
									$timeout(function() {
										$scope.editTypeError = false;
									}, 2000);
									return;
								}
								var subType = document.getElementById("id_subTypeEditAsset");
								if (subType.value.length == 0) {
									 $scope.editSubTypeError = true;
									$scope.editSubType_error_msg = "Please enter Subtype";
									$timeout(function() {
										$scope.editSubTypeError = false;
									}, 2000);
									return;
								}
								var rangeValue = document.getElementById("id_rangeEditAsset");
								if (rangeValue.value.length == 0) {
									 $scope.editRangeError = true;
									$scope.editRange_error_msg = "Please enter Range Value";
									$timeout(function() {
										$scope.editRangeError = false;
									}, 2000);
									return;
								}
								var manufacturer = document.getElementById("id_manufacturerEditAsset");
								if (manufacturer.value.length == 0) {
									 $scope.editManufacturerError = true;
									$scope.editManufacturer_error_msg = "Please enter Manufacturer";
									$timeout(function() {
										$scope.editManufacturerError = false;
									}, 2000);
									return;
								}
							
							
							
							var editAssetNonChronoParameter = {
								"assetId" : $scope.authen.assetId,
								"assetType" : $scope.authen.assetType,
								"subType" : $scope.authen.subType,
								"type" : $scope.authen.type,
								"rangeValue" : $scope.authen.rangeValue,
								"manufacturer" : $scope.authen.manufacturer,
								"calibrationId" : $scope.authen.calibrationId

							};
							if (!$rootScope.checkNetconnection()) { 
								$rootScope.alertDialogChecknet();
								return;
								}
							$rootScope.showloading('#loadingBar');
							$scope.promise = $http
									.put('./assetNonChrono',
											editAssetNonChronoParameter)
									.then(
											function(result) {
												// $scope.selectedRow = -1;
												$rootScope.hideloading('#loadingBar');
												$scope.assetNonChronoViewData[$scope.selectedRow] = JSON
														.parse(JSON
																.stringify($scope.authen));

												if (result.data.status == "success") {
													// $scope.selectedRow = -1;
													$rootScope.showAlertDialog(result.data.message);
													$scope.cancelEditAssetNonChrono();

													/*
													 * $timeout(function() {
													 * 
													 * //$location.path("/login");
													 * $scope.cancelEmployee(); },
													 * 1500);
													 */
												} else if (result.data.status == "error") {
													// $scope.selectedRow = -1;
													$rootScope.showAlertDialog(result.data.message);
													
												}

											},
											function(error) {
												$rootScope.hideloading('#loadingBar');
												$rootScope.fnHttpError(error);
												$scope.cancelEditAssetNonChrono();
												
												
											});
						

					}

					$scope.cancelEditAssetNonChrono = function() {
						$scope.authen = {};
						$scope.selectedRow = -1;
						$scope.NonchronoEditSelectScreen = true;
						$scope.NonchronoEditUpdateScreen = false;
						$scope.searchButton = true;
					}

					$scope.rowHighilited = function(row) {
						$scope.selectedRow = row;
						// $scope.authen = $scope.assetNonChronoData[row];
						$scope.authen = JSON.parse(JSON
								.stringify($scope.assetNonChronoViewData[row]));

						if ($scope.authen.assignedStatus == 'Assigned') {
							if ($scope.authen.assignedTagMacId != undefined
									&& $scope.authen.assignedTagMacId != '') {
								$scope.trackMacId = $scope.authen.assignedTagMacId;
							} else
								$scope.trackMacId = '';
						} else
							$scope.trackMacId = '';

					}

					$scope.cancelEdit = function() {
						$scope.selectedRow = -1;
						$scope.trackMacId = '';
					}

					$scope.deleteAssetValue = function() {

						var name = document
								.getElementById("id_assetIdEditAsset");

						/*
						 * if (name.value.length == 0) { $scope.nameError =
						 * true; $scope.name_error_msg = "Please enter the Asset
						 * ID"; $timeout(function() { $scope.nameError = false;
						 *  }, 2000); return; }
						 */
						/* if ($scope.editAssetNonChronoForm.$valid) { */
						$
								.confirm({
									title : $rootScope.MSGBOX_TITLE,
									content : 'Are you sure to delete this record?',
									type : 'blue',
									columnClass : 'medium',
									autoClose : false,
									icon : 'fa fa-info-circle',
									buttons : {
										yes : {
											text : 'Yes',
											btnClass : 'btn-blue',
											action : function() {
												
												var assetId = $scope.authen.assetId
												//console.log($scope.authen.assetId)
												data = {
													"assetId" : assetId
												}
												if (!$rootScope.checkNetconnection()) { 
													$rootScope.alertDialogChecknet();
													return;
													}
												$rootScope.showloading('#loadingBar');
												$http(
														{
															method : 'DELETE',
															url : './assetNonChrono',
															headers : {
																'Content-Type' : 'application/json'
															},
															data : data,

														})
														.then(
																function(
																		response) {
																	if (response != null
																			&& response.data != null
																			&& response.data != "BAD_REQUEST") {
																		$rootScope.hideloading('#loadingBar');
																		$scope.deletedAsset = response.data;
																		$rootScope.showAlertDialog(response.data.message);
																		$scope.getAssetNonChronoData();
																		$scope.cancelEditAssetNonChrono();

																	} else {
																		$rootScope.hideloading('#loadingBar');
																		$rootScope.showAlertDialog(response.data.message);
																		$timeout(
																				function() {
																					$scope.cancelEditAssetNonChrono();
																				},
																				2000);
																	}
																},function(error) {
																	$rootScope.hideloading('#loadingBar');
																	$rootScope.fnHttpError(error);
																});
											}
										},
										no : function() {

										}
									}
								});

					}
					// ----
					var reA = /[^a-zA-Z]/g;
					var reN = /[^0-9]/g;

					function sortAlphaNum(a1, b1) {
						var a = a1.assetId;
						var b = b1.assetId;
						var aA = a.replace(reA, "");
						var bA = b.replace(reA, "");
						if (aA === bA) {
							var aN = parseInt(a.replace(reN, ""), 10);
							var bN = parseInt(b.replace(reN, ""), 10);
							return aN === bN ? 0 : aN > bN ? 1 : -1;
						} else {
							return aA > bA ? 1 : -1;
						}
					}
					$scope.getAssetNonChronoData = function() {

						
						if (!$rootScope.checkNetconnection()) { 
							$rootScope.alertDialogChecknet();
							return;
							}
						$rootScope.showloading('#loadingBar');
						$http({
							method : 'GET',
							url : './assetNonChrono',

						})
								.then(
										function success(response) {
											$rootScope.hideloading('#loadingBar');
											if (response != null
													&& response.data != null
													&& response.data.length != 0
													&& response.data != "BAD_REQUEST"
													&& response.data.data != undefined
													&& response.data.data.length != 0) {

												$scope.assetNonChronoData.length = 0;
												response.data.data
														.sort(sortAlphaNum);
												/* $scope.assetNonChronoData=response.data.data; */
												for (var i = 0; i < response.data.data.length; i++) {
													var toTime = "", calibrationExpiryDate = "", assignedPerson = "", deptName = "", fromTime = "", assignedDate = "";
													if (response.data.data[i].active == true) {
														// $scope.assetNonChronoData.push(response.data.data[i]);

														if (response.data.data[i].assignmentStatus == "ASSIGNED") {
															// .
															var assetAssignmentDetails = response.data.data[i].assetAssignmentDetails;
															var currentAssetAssignment;
															for ( var key in assetAssignmentDetails) {
																currentAssetAssignment = assetAssignmentDetails[key]
																break;
															}
															if (currentAssetAssignment.fromTime != undefined
																	&& currentAssetAssignment.fromTime != null
																	&& currentAssetAssignment.fromTime != ""
																	) {
																// response.data.data[i].fromTime
																// =
																// response.data[i].createdDate.substring(0,
																// response.data[i].createdDate.length-9);
																// fromTime =
																// currentAssetAssignment.fromTime;
																fromTime = moment(
																		currentAssetAssignment.fromTime,"YYYY-MMM-DD HH:mm")
																		.format(
																				"MMM DD, YYYY HH:mm");
															}
															if (currentAssetAssignment.date != undefined
																	&& currentAssetAssignment.date != null
																	&& currentAssetAssignment.date != ""
																	) {
																// response.data.data[i].fromTime
																// =
																// response.data[i].createdDate.substring(0,
																// response.data[i].createdDate.length-9);
																assignedDate = moment(
																		currentAssetAssignment.date,"YYYY-MMM-DD")
																		.format(
																				"MMM DD, YYYY");
															}
															if (currentAssetAssignment.toTime != undefined
																	&& currentAssetAssignment.toTime != null
																	&& currentAssetAssignment.toTime != ""
																	) {

																// toTime =
																// currentAssetAssignment.toTime;
																toTime = moment(
																		currentAssetAssignment.toTime,"YYYY-MMM-DD HH:mm")
																		.format(
																				"MMM DD, YYYY HH:mm");

															}
															if (currentAssetAssignment.assignmentType == "PERSON") {
																assignedPerson = currentAssetAssignment.empId
															}
															if (currentAssetAssignment.assignmentType == "DEPT") {
																deptName = currentAssetAssignment.deptName
															}
														}

														if (response.data.data[i].calibrationRecord != null) {
															if (response.data.data[i].calibrationRecord.calibrationExpiryDate != undefined
																	&& response.data.data[i].calibrationRecord.calibrationExpiryDate != null
																	&& response.data.data[i].calibrationRecord.calibrationExpiryDate != ""
																	) {
																// response.data.data[i].calibrationRecord.calibrationExpiryDate
																// =
																// response.data.data[i].calibrationRecord.calibrationExpiryDate.substring(0,(response.data.data[i].calibrationRecord.calibrationExpiryDate.length-9));
																calibrationExpiryDate = moment(
																		response.data.data[i].calibrationRecord.calibrationExpiryDate)
																		.format(
																				"MMM DD, YYYY");
															}
														}

														if (response.data.data[i].calibrationRecord != null) {

															$scope.assetNonChronoData
																	.push({
																		'assetId' : response.data.data[i].assetId,
																		'type' : response.data.data[i].type,
																		'assetType' : response.data.data[i].assetType,
																		'subType' : response.data.data[i].subType,
																		'manufacturer' : response.data.data[i].manufacturer,
																		'rangeValue' : response.data.data[i].rangeValue,
																		'calibrationId' : response.data.data[i].calibrationRecord.calibrationId,
																		'assignedTagMacId' : response.data.data[i].assignedTagMacId,
																		'assignedStatus' : response.data.data[i].assignedStatus,
																		'calibrationExpiryDate' : calibrationExpiryDate,
																		'assignedPerson' : assignedPerson,
																		'deptName' : deptName,
																		'fromTime' : fromTime,
																		'toTime' : toTime,
																		'date' : assignedDate

																	});
														} else {
															$scope.assetNonChronoData
																	.push({
																		'assetId' : response.data.data[i].assetId,
																		'type' : response.data.data[i].type,
																		'assetType' : response.data.data[i].assetType,
																		'subType' : response.data.data[i].subType,
																		'manufacturer' : response.data.data[i].manufacturer,
																		'rangeValue' : response.data.data[i].rangeValue,
																		'assignedTagMacId' : response.data.data[i].assignedTagMacId,
																		'assignedStatus' : response.data.data[i].assignedStatus,
																		'calibrationId' : "None",
																		'calibrationExpiryDate' : calibrationExpiryDate,
																		'assignedPerson' : assignedPerson,
																		'deptName' : deptName,
																		'fromTime' : fromTime,
																		'toTime' : toTime,
																		'date' : assignedDate

																	});

														}

													}
												}
												$scope.assetIdTable.length = 0;
												for (var k = 0; k < $scope.assetNonChronoData.length; k++) {
													if ($scope.assetNonChronoData[k].assetId != undefined
															&& $scope.assetNonChronoData[k].assetId != ''
															&& $scope.assetNonChronoData[k].assetId != null) {
														$scope.assetIdTable
																.push($scope.assetNonChronoData[k].assetId);
													}

												}
												if ($scope.assetIdTable.length == 0)
													$scope.assetIdTable
															.push("No asset found ");

												if ($scope.assetNonChronoData.length == 0) {
													$scope.assetNonChronoNoData = true;
													return;
												} else {
													$scope.assetNonChronoNoData = false;
												}

												if ($scope.assetNonChronoData.length > 10) {
													$scope.PaginationTab = true;
												} else {
													$scope.PaginationTab = false;
												}
												$scope.totalItems = $scope.assetNonChronoData.length;
												$scope
														.getAssignedTagIds($scope.assetNonChronoData);
												$scope.getTotalPages();
												$scope.filterOption();

												$scope.currentPage = 1;
												if ($scope.assetNonChronoData.length > $scope.numPerPage) {
													$scope.assetNonChronoViewData = $scope.assetNonChronoData
															.slice(
																	0,
																	$scope.numPerPage);
													$scope.hasPrevious = true;
													$scope.isFirstPage = true;
													$scope.hasNext = false;
													$scope.isLastPage = false;
												} else {
													$scope.assetNonChronoViewData = $scope.assetNonChronoData;
												}
											} else {
												$scope.assetNonChronoNoData = true;
											}
										},
										function error(response) {
											$rootScope.hideloading('#loadingBar');
											$rootScope.fnHttpError(response);
										});
					}

					// $scope.getAssetNonChronoData();

					// pagination button

					$scope.currentPage = 1;
					$scope.numPerPage = 10;
					$scope.maxSize = 3;
					$scope.totalItems = 0;
					$scope.assetNonChronoViewData = [];
					$scope.assetNonChronoDataFilterList = [];
					$scope
							.$watch(
									'currentPage + numPerPage',
									function() {
										var begin = (($scope.currentPage - 1) * $scope.numPerPage), end = begin
												+ $scope.numPerPage;
										if ($scope.searchText == '') // kumar
											// 02/July
											// for
											// filter
											// text
											$scope.assetNonChronoViewData = $scope.assetNonChronoData
													.slice(begin, end);
										else
											$scope.assetNonChronoViewData = $scope.assetNonChronoDataFilterList
													.slice(begin, end);

										$scope.enablePageButtons();
									});

					$scope.currentPage = 1;
					$scope.maxSize = 3;
					$scope.numPerPage = 10;
					$scope.totalItems = 0;
					$scope.totalPages = 0;

					$scope.hasNext = true;
					$scope.hasPrevious = true;
					$scope.isFirstPage = true;
					$scope.isLastPage = true;
					$scope.serial = 1;

					$scope
							.$watch(
									"currentPage",
									function(newVal, oldVal) {
										if (newVal !== oldVal) {

											if (newVal > $scope.totalPages) {
												$scope.currentPage = $scope.totalPages;
											} else {
												$scope.currentPage = newVal;
											}

											$scope.serial = (($scope.currentPage - 1) * $scope.numPerPage) + 1;
											$scope.enablePageButtons();
										}
									});

					$scope.enablePageButtons = function() {
						$scope.serial = (($scope.currentPage - 1) * $scope.numPerPage) + 1;
						$scope.selectedRow = -1;
						if ($scope.totalPages <= 1) {
							$scope.hasPrevious = true;
							$scope.isFirstPage = true;
							$scope.hasNext = true;
							$scope.isLastPage = true;

						} else if ($scope.totalPages > 1
								&& $scope.currentPage == $scope.totalPages) {

							$scope.hasPrevious = false;
							$scope.isFirstPage = false;
							$scope.hasNext = true;
							$scope.isLastPage = true;
						} else if ($scope.currentPage != 1
								&& $scope.currentPage < $scope.totalPages) {

							$scope.hasPrevious = false;
							$scope.isFirstPage = false;
							$scope.hasNext = false;
							$scope.isLastPage = false;

						} else {
							$scope.hasPrevious = true;
							$scope.isFirstPage = true;
							$scope.hasNext = false;
							$scope.isLastPage = false;
						}

					};

					$scope.getTotalPages = function() {

						if (($scope.totalItems % $scope.numPerPage) > 0)
							$scope.totalPages = parseInt($scope.totalItems
									/ $scope.numPerPage) + 1;
						else
							$scope.totalPages = parseInt($scope.totalItems
									/ $scope.numPerPage);

						$scope.enablePageButtons();

					};

					$scope.getAssignedTagIds = function(assetList) {

						$scope.assignedTagMacId = '';

						if (assetList != undefined && assetList != null) {
							for (var i = 0; i < assetList.length; i++) {
								if (assetList[i].assignedStatus == 'Assigned') {
									if (assetList[i].assignedTagMacId != undefined
											&& assetList[i].assignedTagMacId != '') {
										if ($scope.assignedTagMacId == '')
											$scope.assignedTagMacId = assetList[i].assignedTagMacId;
										else
											$scope.assignedTagMacId = $scope.assignedTagMacId
													+ ","
													+ assetList[i].assignedTagMacId;

									}
								}
							}
						}

					}

					$scope.goFirstPage = function() {
						$scope.currentPage = 1;
						$scope.enablePageButtons();
					};

					$scope.goPreviousPage = function() {
						if ($scope.currentPage > 1)
							$scope.currentPage = $scope.currentPage - 1;
						$scope.enablePageButtons();
					};

					$scope.goNextPage = function() {
						if ($scope.currentPage <= $scope.totalPages)
							$scope.currentPage = $scope.currentPage + 1;
						$scope.enablePageButtons();
					};

					$scope.goLastPage = function() {
						$scope.currentPage = $scope.totalPages;
						$scope.enablePageButtons();
					};

					// add asset non chrono

					$scope.addAssetNonChrono = function() {
						
							/* if ($scope.authen.calibrationId == "None") */// if expiry date comes it comes under asset chrono commented after  discussion  with Dr Anand sir 28-09-21
							$scope.authen.calibrationId = 0;

							var assetId = document.getElementById("id_assetIdAddAsset");
							if (assetId.value.length == 0) {
								 $scope.addAssetIdError = true;
								$scope.addAssetId_error_msg = "Please enter the Asset Id";
								$timeout(function() {
									$scope.addAssetIdError = false;
								}, 2000);
								return;
							}
								var assetType = document.getElementById("id_assetTypeAddAsset");
								if (assetType.value.length == 0) {
									 $scope.addTypeError = true;
									$scope.addType_error_msg = "Please enter Type";
									$timeout(function() {
										$scope.addTypeError = false;
									}, 2000);
									return;
								}
								var subType = document.getElementById("id_subTypeAddAsset");
								if (subType.value.length == 0) {
									$scope.addSubTypeError = true;
									$scope.addSubType_error_msg = "Please enter Subtype";
									$timeout(function() {
										$scope.addSubTypeError = false;
									}, 2000);
									return;
								}
								var rangeValue = document.getElementById("id_rangeAddAsset");
								if (rangeValue.value.length == 0) {
									$scope.addRangeError = true;
									$scope.addRange_error_msg = "Please enter Range Value";
									$timeout(function() {
										$scope.addRangeError = false;
									}, 2000);
									return;
								}
								var manufacturer = document.getElementById("id_manufacturerAddAsset");
								if (manufacturer.value.length == 0) {
									 $scope.addManufacturerError = true;
									$scope.addManufacturer_error_msg = "Please enter Manufacturer";
									$timeout(function() {
										$scope.addManufacturerError = false;
									}, 2000);
									return;
								}
							var addAssetNonChronoParameter = {
								"assetId" : $scope.authen.assetId,
								"assetType" : $scope.authen.assetType,
								"subType" : $scope.authen.subType,
								"type" : $scope.authen.type,
								"rangeValue" : $scope.authen.range,
								"manufacturer" : $scope.authen.manufacturer,
								"calibrationId" : $scope.authen.calibrationId
							};
							if (!$rootScope.checkNetconnection()) { 
								$rootScope.alertDialogChecknet();
								return;
								}
							$rootScope.showloading('#loadingBar');
							$scope.promise = $http
									.post('./assetNonChrono',
											addAssetNonChronoParameter)
									.then(
											function(result) {
												$rootScope.hideloading('#loadingBar');
												if (result.data.status == "success") {
													$rootScope.showAlertDialog(result.data.message);
													$scope.clearAddAssetNonChrono();
													/*
													 * $timeout(function() {
													 * 
													 * //$location.path("/login");
													 * $scope.cancelEmployee(); },
													 * 1500);
													 */
												} else if (result.data.status == "error") {
													$rootScope.showAlertDialog(result.data.message);
													
												}

											},
											function(error) {
												$rootScope.hideloading('#loadingBar');
												$rootScope.fnHttpError(error);
											});
						
					}
					$scope.clearAddAssetNonChrono = function() {
						var formID = document.getElementById(
								"idAddassetNonChronoForm").reset();
					}

					$scope.cancelAssetChrono = function() {
						var formID = document.getElementById(
								"assetNonChronoForm").reset();

					}

					$scope.displayNonChronoViewScreen = function() {
						$scope.showMultipleSearch = false;
						$scope.showSearchIcon = true;
						$scope.assetNonChronoViewData = [];
						$scope.assetNonChronoDataFilterList = [];
						$scope.searchText = "";
						$scope.selectedRow = -1;
						$scope.currentPage = 1;
						$scope.searchButton = true;
						$scope.authen.type = $scope.nonChronoAssetType[0];
						$scope.authen.calibrationId = "None";

						$scope.NonchronoViewScreen = true;
						$scope.NonchronoEditSelectScreen = false;
						$scope.NonchronoEditUpdateScreen = false;
						$scope.NonchronoAddScreen = false;
						$scope.assetNonChronoAssignReturnFetchScreen = false;
						$scope.assetNonChronoAssignScreen = false;
						$scope.assetNonChronoReturnScreen = false;
						$("#idNonChronoView").addClass("active");
						$("#idNonChronoAdd").removeClass("active");
						$("#idNonChronoEdit").removeClass("active");
						$("#idNonChronoAssign").removeClass("active");
						$scope.getAssetNonChronoData();

					};
					$scope.displayNonChronoAddScreen = function() {
						$scope.showMultipleSearch = false;
						$scope.showSearchIcon = false;
						$scope.searchButton = false;
						$scope.clearAddAssetNonChrono();
						$scope.NonchronoViewScreen = false;
						$scope.NonchronoEditSelectScreen = false;
						$scope.NonchronoEditUpdateScreen = false;
						$scope.NonchronoAddScreen = true;
						$scope.assetNonChronoAssignReturnFetchScreen = false;
						$scope.assetNonChronoAssignScreen = false;
						$scope.assetNonChronoReturnScreen = false;
						$("#idNonChronoView").removeClass("active");
						$("#idNonChronoAdd").addClass("active");
						$("#idNonChronoEdit").removeClass("active");
						$("#idNonChronoAssign").removeClass("active");
						// $scope.menuLink('addEmployee','addEmployee')
						// $scope.getCalibrationData();
					};
					$scope.displayNonChronoEditScreen = function() {
						$scope.showMultipleSearch = false;
						$scope.showSearchIcon = true;
						$scope.assetNonChronoViewData = [];
						$scope.assetNonChronoDataFilterList = [];
						$scope.searchText = "";
						$scope.selectedRow = -1;
						$scope.currentPage = 1;
						$scope.searchButton = true;

						$scope.NonchronoViewScreen = false;
						$scope.NonchronoEditSelectScreen = true;
						$scope.NonchronoEditUpdateScreen = false;
						$scope.NonchronoAddScreen = false;
						$scope.assetNonChronoAssignReturnFetchScreen = false;
						$scope.assetNonChronoAssignScreen = false;
						$scope.assetNonChronoReturnScreen = false;
						$("#idNonChronoView").removeClass("active");
						$("#idNonChronoAdd").removeClass("active");
						$("#idNonChronoEdit").addClass("active");
						$("#idNonChronoAssign").removeClass("active");
						$scope.getAssetNonChronoData();
						// $scope.getCalibrationData();
						// $scope.menuLink('editEmployee','editEmployee')
					};
					$scope.displayNonChronoAssignScreen = function() {
						$scope.showMultipleSearch = false;
						$scope.showSearchIcon = false;
						// $('#id_fetchAssetId').val("");
						// $scope.fetchAssetId = $scope.assetIdTable[0];
						$scope.searchButton = false;
						$scope.NonchronoViewScreen = false;
						$scope.NonchronoEditSelectScreen = false;
						$scope.NonchronoEditUpdateScreen = false;
						$scope.NonchronoAddScreen = false;
						$scope.assetNonChronoAssignReturnFetchScreen = true;
						$scope.assetNonChronoAssignScreen = false;
						$scope.assetNonChronoReturnScreen = false;
						$("#idNonChronoView").removeClass("active");
						$("#idNonChronoAdd").removeClass("active");
						$("#idNonChronoEdit").removeClass("active");
						$("#idNonChronoAssign").addClass("active");
						$scope.fetchAssetId = $scope.assetIdTable[0];
						$scope.getAssetNonChronoData();

						// $scope.menuLink('editEmployee','editEmployee')
						$('#id_nonChronoAssignedDate').datetimepicker({
							format : 'YYYY-MMM-DD',
							maxDate : new Date(),
						});

						$('#stimepicker').datetimepicker({
							format : 'YYYY-MMM-DD HH:mm',
							minDate : new Date(),
							maxDate : new Date()
						});
						$('#etimepicker').datetimepicker({
							format : 'YYYY-MMM-DD HH:mm',
							minDate : new Date(),
							icons : {
								time : 'fas fa-clock'
							}
						});

						/*
						 * var startTime = ("0" + today.getHours()).slice(-2) +
						 * ":" + ("0" + today.getMinutes()).slice(-2); var
						 * endTime = ("0" + (today.getHours() + 2)) .slice(-2) +
						 * ":" + ("0" + today.getMinutes()).slice(-2);
						 */
						var startTime = moment(new Date()).format(
								'YYYY-MMM-DD HH:mm');
						var endTime = moment(new Date()).format(
								'YYYY-MMM-DD HH:mm');
						endTime = moment(new Date()).format('YYYY-MMM-DD ')
								+ ("0" + (today.getHours() + 2)).slice(-2)
								+ ":" + ("0" + today.getMinutes()).slice(-2);
						$("#stime").val(startTime);
						$("#etime").val(endTime);
						$scope.authen.date = moment(new Date()).format(
								'YYYY-MMM-DD');
						$("#id_nonChronoAssignedDate").val(
								moment(new Date()).format('YYYY-MMM-DD'));
						$scope.authen.stime = startTime;
						$scope.authen.etime = endTime;
						$("#etimepicker").val(endTime);
						if ($scope.authen.assignmentStatus == "DEPT") {
							if ($scope.departmentList != undefined
									&& $scope.departmentList != null
									&& $scope.departmentList != "") {
								$scope.authen.assignmentDepartment = $scope.departmentList[0];
								$scope.authen.assignedPerson = "";
							}
						} else if ($scope.authen.assignmentStatus == "PERSON") {

							if ($scope.empViewData != undefined
									&& $scope.empViewData != null
									&& $scope.empViewData != "") {
								$scope.authen.assignedPerson = $scope.empViewData[0];
								$scope.authen.assignmentDepartment = "";
							}

						}

					};

					// to display enabled sub menus
					if ($scope.user.capabilityDetails != undefined
							&& $scope.user.capabilityDetails != null) {
						if ($scope.user.capabilityDetails.ANVA != 0) {
							$scope.displayNonChronoViewScreen();
						}
						if ($scope.user.capabilityDetails.ANVA != 1) {
							$scope.displayNonChronoAddScreen();
						}
						if ($scope.user.capabilityDetails.ACVA != 1
								&& $scope.user.capabilityDetails.ANAD != 1) {
							$scope.displayNonChronoEditScreen();
						}
						if ($scope.user.capabilityDetails.ANVA != 1
								&& $scope.user.capabilityDetails.ANAD != 1
								&& $scope.user.capabilityDetails.ANED != 1) {
							$scope.displayNonChronoAssignScreen();
						}
						if ($scope.user.capabilityDetails.ANVA == 1
								&& $scope.user.capabilityDetails.ANAD == 1
								&& $scope.user.capabilityDetails.ANED == 1
								&& $scope.user.capabilityDetails.ANAR != 1) {
							return;
						} else {
							$scope.getDepartmentDetails();
							$scope.getStaffDetails();
						}
					}

					// multisearch code starts
					$scope.multipleSearch = function() {
						$scope.showMultipleSearch = true;
						$scope.searchButton = false
						$scope.firstMultiSearchDropDown = "All";
						$scope.secondMultiSearchDropDown = "All";
						$scope.varANDORDropDown = "AND";
						$scope.firstSearchText = "";
						$scope.secondSearchText = "";
					}

					$scope.closeMultiSearchView = function() {
						$scope.showMultipleSearch = false;
						$scope.searchButton = true

						$scope.assetNonChronoDataFilterList = $scope.assetNonChronoData;
						$scope.totalItems = $scope.assetNonChronoDataFilterList.length;
						$scope.currentPage = 1;
						$scope.getTotalPages();

						$scope
								.getAssignedTagIds($scope.assetNonChronoDataFilterList);

						if ($scope.assetNonChronoData.length == 0
								|| $scope.assetNonChronoDataFilterList.length == 0) {
							$scope.assetNonChronoNoData = true;

						} else {
							$scope.assetNonChronoNoData = false;

							if ($scope.assetNonChronoDataFilterList.length > $scope.numPerPage) {
								$scope.assetNonChronoViewData = JSON
										.parse(JSON
												.stringify($scope.assetNonChronoDataFilterList
														.slice(
																0,
																$scope.numPerPage)));
								$scope.PaginationTab = true;
							} else {
								$scope.assetNonChronoViewData = JSON
										.parse(JSON
												.stringify($scope.assetNonChronoDataFilterList));
								$scope.PaginationTab = false;
							}
						}
					}

					$scope.fnfirstSearchANDData = function() {

						$scope.firstSearchANDDataArray = [];
						for (var i = 0; i < $scope.assetNonChronoData.length; i++) {
							// to check all
							if ($scope.firstMultiSearchDropDown == "All") {
								if ($scope.firstSearchText == ""
										|| $scope.firstSearchText == undefined) {

									$scope.firstSearchANDDataArray
											.push($scope.assetNonChronoData[i]);

								} else {
									if (($scope.assetNonChronoData[i].assetId != undefined
											&& $scope.assetNonChronoData[i].assetId != null ? $scope.assetNonChronoData[i].assetId
											: "").toLowerCase().includes(
											$scope.firstSearchText
													.toLowerCase())
											|| ($scope.assetNonChronoData[i].assetType != undefined
													&& $scope.assetNonChronoData[i].assetType != null ? $scope.assetNonChronoData[i].assetType
													: "")
													.toLowerCase()
													.includes(
															$scope.firstSearchText
																	.toLowerCase())
											|| ($scope.assetNonChronoData[i].type != undefined
													&& $scope.assetNonChronoData[i].type != null ? $scope.assetNonChronoData[i].type
													: "")
													.toLowerCase()
													.includes(
															$scope.firstSearchText
																	.toLowerCase())
											|| ($scope.assetNonChronoData[i].subType != undefined
													&& $scope.assetNonChronoData[i].subType != null ? $scope.assetNonChronoData[i].subType
													: "")
													.toLowerCase()
													.includes(
															$scope.firstSearchText
																	.toLowerCase())
											|| ($scope.assetNonChronoData[i].rangeValue != undefined
													&& $scope.assetNonChronoData[i].rangeValue != null ? $scope.assetNonChronoData[i].rangeValue
													: "")
													.toLowerCase()
													.includes(
															$scope.firstSearchText
																	.toLowerCase())
											|| ($scope.assetNonChronoData[i].manufacturer != undefined
													&& $scope.assetNonChronoData[i].manufacturer != null ? $scope.assetNonChronoData[i].manufacturer
													: "")
													.toLowerCase()
													.includes(
															$scope.firstSearchText
																	.toLowerCase())
											|| ($scope.assetNonChronoData[i].assignedPerson != undefined
													&& $scope.assetNonChronoData[i].assignedPerson != null ? $scope.assetNonChronoData[i].assignedPerson
													: "")
													.toLowerCase()
													.includes(
															$scope.firstSearchText
																	.toLowerCase())
											|| ($scope.assetNonChronoData[i].deptName != undefined
													&& $scope.assetNonChronoData[i].deptName != null ? $scope.assetNonChronoData[i].deptName
													: "")
													.toLowerCase()
													.includes(
															$scope.firstSearchText
																	.toLowerCase())
											|| ($scope.assetNonChronoData[i].fromTime != undefined
													&& $scope.assetNonChronoData[i].fromTime != null ? $scope.assetNonChronoData[i].fromTime
													: "")
													.toLowerCase()
													.includes(
															$scope.firstSearchText
																	.toLowerCase())
											|| ($scope.assetNonChronoData[i].toTime != undefined
													&& $scope.assetNonChronoData[i].toTime != null ? $scope.assetNonChronoData[i].toTime
													: "")
													.toLowerCase()
													.includes(
															$scope.firstSearchText
																	.toLowerCase())) {
										$scope.firstSearchANDDataArray
												.push($scope.assetNonChronoData[i]);

									}

								}
							}
							// to check other than all
							else {
								if ($scope.firstMultiSearchDropDown == "assetId") {
									if ($scope.firstSearchText == ""
											|| $scope.firstSearchText == undefined) {
										$scope.firstSearchANDDataArray
												.push($scope.assetNonChronoData[i]);
									} else if ((($scope.assetNonChronoData[i].assetId)
											.toString() != undefined
											&& ($scope.assetNonChronoData[i].assetId)
													.toString() != null ? ($scope.assetNonChronoData[i].assetId)
											.toString()
											: "").toLowerCase().includes(
											$scope.firstSearchText
													.toLowerCase()))
										$scope.firstSearchANDDataArray
												.push($scope.assetNonChronoData[i]);

								} else if ($scope.firstMultiSearchDropDown == "assetType") {
									if ($scope.firstSearchText == ""
											|| $scope.firstSearchText == undefined) {
										$scope.firstSearchANDDataArray
												.push($scope.assetNonChronoData[i]);
									} else if (($scope.assetNonChronoData[i].type != undefined
											&& $scope.assetNonChronoData[i].type != null ? $scope.assetNonChronoData[i].type
											: "").toLowerCase().includes(
											$scope.firstSearchText
													.toLowerCase()))
										$scope.firstSearchANDDataArray
												.push($scope.assetNonChronoData[i]);

								} else if ($scope.firstMultiSearchDropDown == "subType") {
									if ($scope.firstSearchText == ""
											|| $scope.firstSearchText == undefined) {
										$scope.firstSearchANDDataArray
												.push($scope.assetNonChronoData[i]);
									} else if (($scope.assetNonChronoData[i].subType != undefined
											&& $scope.assetNonChronoData[i].subType != null ? $scope.assetNonChronoData[i].subType
											: "").toLowerCase().includes(
											$scope.firstSearchText
													.toLowerCase()))
										$scope.firstSearchANDDataArray
												.push($scope.assetNonChronoData[i]);

								} else if ($scope.firstMultiSearchDropDown == "range") {
									if ($scope.firstSearchText == ""
											|| $scope.firstSearchText == undefined) {
										$scope.firstSearchANDDataArray
												.push($scope.assetNonChronoData[i]);
									} else if (($scope.assetNonChronoData[i].rangeValue != undefined
											&& $scope.assetNonChronoData[i].rangeValue != null ? $scope.assetNonChronoData[i].rangeValue
											: "").toLowerCase().includes(
											$scope.firstSearchText
													.toLowerCase()))
										$scope.firstSearchANDDataArray
												.push($scope.assetNonChronoData[i]);
								} else if ($scope.firstMultiSearchDropDown == "type") {
									if ($scope.firstSearchText == ""
											|| $scope.firstSearchText == undefined) {
										$scope.firstSearchANDDataArray
												.push($scope.assetNonChronoData[i]);
									} else if (($scope.assetNonChronoData[i].assetType != undefined
											&& $scope.assetNonChronoData[i].assetType != null ? $scope.assetNonChronoData[i].assetType
											: "").toLowerCase().includes(
											$scope.firstSearchText
													.toLowerCase()))
										$scope.firstSearchANDDataArray
												.push($scope.assetNonChronoData[i]);

								} else if ($scope.firstMultiSearchDropDown == "manufacturer") {
									if ($scope.firstSearchText == ""
											|| $scope.firstSearchText == undefined) {
										$scope.firstSearchANDDataArray
												.push($scope.assetNonChronoData[i]);
									} else if (($scope.assetNonChronoData[i].manufacturer != undefined
											&& $scope.assetNonChronoData[i].manufacturer != null ? $scope.assetNonChronoData[i].manufacturer
											: "").toLowerCase().includes(
											$scope.firstSearchText
													.toLowerCase()))
										$scope.firstSearchANDDataArray
												.push($scope.assetNonChronoData[i]);

								} else if ($scope.firstMultiSearchDropDown == "assignedTo") {
									if ($scope.firstSearchText == ""
											|| $scope.firstSearchText == undefined) {
										$scope.firstSearchANDDataArray
												.push($scope.assetNonChronoData[i]);
									} else if (($scope.assetNonChronoData[i].assignedPerson != undefined
											&& $scope.assetNonChronoData[i].assignedPerson != null ? $scope.assetNonChronoData[i].assignedPerson
											: "").toLowerCase().includes(
											$scope.firstSearchText
													.toLowerCase())
											|| ($scope.assetNonChronoData[i].deptName != undefined
													&& $scope.assetNonChronoData[i].deptName != null ? $scope.assetNonChronoData[i].deptName
													: "")
													.toLowerCase()
													.includes(
															$scope.firstSearchText
																	.toLowerCase()))
										$scope.firstSearchANDDataArray
												.push($scope.assetNonChronoData[i]);

								} else if ($scope.firstMultiSearchDropDown == "fromTime") {
									if ($scope.firstSearchText == ""
											|| $scope.firstSearchText == undefined) {
										$scope.firstSearchANDDataArray
												.push($scope.assetNonChronoData[i]);
									} else if (($scope.assetNonChronoData[i].shiftDetails.fromTime != undefined
											&& $scope.assetNonChronoData[i].fromTime != null ? $scope.assetNonChronoData[i].fromTime
											: "").toLowerCase().includes(
											$scope.firstSearchText
													.toLowerCase()))
										$scope.firstSearchANDDataArray
												.push($scope.assetNonChronoData[i]);

								} else if ($scope.firstMultiSearchDropDown == "toTime") {
									if ($scope.firstSearchText == ""
											|| $scope.firstSearchText == undefined) {
										$scope.firstSearchANDDataArray
												.push($scope.assetNonChronoData[i]);
									} else if (($scope.assetNonChronoData[i].toTime != undefined
											&& $scope.assetNonChronoData[i].toTime != null ? $scope.assetNonChronoData[i].toTime
											: "").toLowerCase().includes(
											$scope.firstSearchText
													.toLowerCase()))
										$scope.firstSearchANDDataArray
												.push($scope.assetNonChronoData[i]);

								}

							}
						}
					}

					$scope.multiSearchChanged = function() {
						$scope.firstSearchANDDataArray = [];
						$scope.fnfirstSearchANDData();
						//console.log($scope.firstSearchANDDataArray);

						if ($scope.varANDORDropDown == "AND") {
							$scope.disableSecondFilterText = false;
							$scope.disableSecondFilterDropDown = false;
							$scope.assetNonChronoViewData = [];
							$scope.assetNonChronoDataFilterList = [];

							for (var i = 0; i < $scope.firstSearchANDDataArray.length; i++) {
								if ($scope.secondMultiSearchDropDown == "All") {

									if ($scope.secondSearchText == ""
											|| $scope.secondSearchText == undefined) {
										$scope.assetNonChronoDataFilterList
												.push($scope.firstSearchANDDataArray[i]);
									} else {
										if ((($scope.firstSearchANDDataArray[i].assetId)
												.toString() != undefined
												&& ($scope.firstSearchANDDataArray[i].assetId)
														.toString() != null ? ($scope.firstSearchANDDataArray[i].assetId)
												.toString()
												: "").toLowerCase().includes(
												$scope.secondSearchText
														.toLowerCase())
												|| ($scope.firstSearchANDDataArray[i].assetType != undefined
														&& $scope.firstSearchANDDataArray[i].assetType != null ? $scope.firstSearchANDDataArray[i].assetType
														: "")
														.toLowerCase()
														.includes(
																$scope.secondSearchText
																		.toLowerCase())
												|| ($scope.firstSearchANDDataArray[i].subType != undefined
														&& $scope.firstSearchANDDataArray[i].subType != null ? $scope.firstSearchANDDataArray[i].subType
														: "")
														.toLowerCase()
														.includes(
																$scope.secondSearchText
																		.toLowerCase())
												|| ($scope.firstSearchANDDataArray[i].rangeValue != undefined
														&& $scope.firstSearchANDDataArray[i].rangeValue != null ? $scope.firstSearchANDDataArray[i].rangeValue
														: "")
														.toLowerCase()
														.includes(
																$scope.secondSearchText
																		.toLowerCase())
												|| ($scope.firstSearchANDDataArray[i].type != undefined
														&& $scope.firstSearchANDDataArray[i].type != null ? $scope.firstSearchANDDataArray[i].type
														: "")
														.toLowerCase()
														.includes(
																$scope.secondSearchText
																		.toLowerCase())
												|| ($scope.firstSearchANDDataArray[i].manufacturer != undefined
														&& $scope.firstSearchANDDataArray[i].manufacturer != null ? $scope.firstSearchANDDataArray[i].manufacturer
														: "")
														.toLowerCase()
														.includes(
																$scope.secondSearchText
																		.toLowerCase())
												|| ($scope.firstSearchANDDataArray[i].assignedPerson != undefined
														&& $scope.firstSearchANDDataArray[i].assignedPerson != null ? $scope.firstSearchANDDataArray[i].assignedPerson
														: "")
														.toLowerCase()
														.includes(
																$scope.secondSearchText
																		.toLowerCase())
												|| ($scope.firstSearchANDDataArray[i].deptName != undefined
														&& $scope.firstSearchANDDataArray[i].deptName != null ? $scope.firstSearchANDDataArray[i].deptName
														: "")
														.toLowerCase()
														.includes(
																$scope.secondSearchText
																		.toLowerCase())
												|| ($scope.firstSearchANDDataArray[i].fromTime != undefined
														&& $scope.firstSearchANDDataArray[i].fromTime != null ? $scope.firstSearchANDDataArray[i].fromTime
														: "")
														.toLowerCase()
														.includes(
																$scope.secondSearchText
																		.toLowerCase())
												|| ($scope.firstSearchANDDataArray[i].toTime != undefined
														&& $scope.firstSearchANDDataArray[i].toTime != null ? $scope.firstSearchANDDataArray[i].toTime
														: "")
														.toLowerCase()
														.includes(
																$scope.secondSearchText
																		.toLowerCase())) {
											$scope.assetNonChronoDataFilterList
													.push($scope.firstSearchANDDataArray[i]);

										}
									}
								} else {

									if ($scope.secondMultiSearchDropDown == "assetId") {
										if ($scope.secondSearchText == ""
												|| $scope.secondSearchText == undefined) {
											$scope.assetNonChronoDataFilterList
													.push($scope.firstSearchANDDataArray[i]);
										} else if ((($scope.firstSearchANDDataArray[i].assetId)
												.toString() != undefined
												&& ($scope.firstSearchANDDataArray[i].assetId)
														.toString() != null ? ($scope.firstSearchANDDataArray[i].assetId)
												.toString()
												: "").toLowerCase().includes(
												$scope.secondSearchText
														.toLowerCase()))
											$scope.assetNonChronoDataFilterList
													.push($scope.firstSearchANDDataArray[i]);

									} else if ($scope.secondMultiSearchDropDown == "assetType") {
										if ($scope.secondSearchText == ""
												|| $scope.secondSearchText == undefined) {
											$scope.assetNonChronoDataFilterList
													.push($scope.firstSearchANDDataArray[i]);
										} else if (($scope.firstSearchANDDataArray[i].type != undefined
												&& $scope.firstSearchANDDataArray[i].type != null ? $scope.firstSearchANDDataArray[i].type
												: "").toLowerCase().includes(
												$scope.secondSearchText
														.toLowerCase()))
											$scope.assetNonChronoDataFilterList
													.push($scope.firstSearchANDDataArray[i]);

									} else if ($scope.secondMultiSearchDropDown == "subType") {
										if ($scope.secondSearchText == ""
												|| $scope.secondSearchText == undefined) {
											$scope.assetNonChronoDataFilterList
													.push($scope.firstSearchANDDataArray[i]);
										} else if (($scope.firstSearchANDDataArray[i].subType != undefined
												&& $scope.firstSearchANDDataArray[i].subType != null ? $scope.firstSearchANDDataArray[i].subType
												: "").toLowerCase().includes(
												$scope.secondSearchText
														.toLowerCase()))
											$scope.assetNonChronoDataFilterList
													.push($scope.firstSearchANDDataArray[i]);

									} else if ($scope.secondMultiSearchDropDown == "range") {
										if ($scope.secondSearchText == ""
												|| $scope.secondSearchText == undefined) {
											$scope.assetNonChronoDataFilterList
													.push($scope.firstSearchANDDataArray[i]);
										} else if (($scope.firstSearchANDDataArray[i].rangeValue != undefined
												&& $scope.firstSearchANDDataArray[i].rangeValue != null ? $scope.firstSearchANDDataArray[i].rangeValue
												: "").toLowerCase().includes(
												$scope.secondSearchText
														.toLowerCase()))
											$scope.assetNonChronoDataFilterList
													.push($scope.firstSearchANDDataArray[i]);
									} else if ($scope.secondMultiSearchDropDown == "type") {
										if ($scope.secondSearchText == ""
												|| $scope.secondSearchText == undefined) {
											$scope.assetNonChronoDataFilterList
													.push($scope.firstSearchANDDataArray[i]);
										} else if (($scope.firstSearchANDDataArray[i].assetType != undefined
												&& $scope.firstSearchANDDataArray[i].assetType != null ? $scope.firstSearchANDDataArray[i].assetType
												: "").toLowerCase().includes(
												$scope.secondSearchText
														.toLowerCase()))
											$scope.assetNonChronoDataFilterList
													.push($scope.firstSearchANDDataArray[i]);

									} else if ($scope.secondMultiSearchDropDown == "manufacturer") {
										if ($scope.secondSearchText == ""
												|| $scope.secondSearchText == undefined) {
											$scope.assetNonChronoDataFilterList
													.push($scope.firstSearchANDDataArray[i]);
										} else if (($scope.firstSearchANDDataArray[i].manufacturer != undefined
												&& $scope.firstSearchANDDataArray[i].manufacturer != null ? $scope.firstSearchANDDataArray[i].manufacturer
												: "").toLowerCase().includes(
												$scope.secondSearchText
														.toLowerCase()))
											$scope.assetNonChronoDataFilterList
													.push($scope.firstSearchANDDataArray[i]);

									} else if ($scope.secondMultiSearchDropDown == "assignedTo") {
										if ($scope.secondSearchText == ""
												|| $scope.secondSearchText == undefined) {
											$scope.assetNonChronoDataFilterList
													.push($scope.firstSearchANDDataArray[i]);
										} else if (($scope.firstSearchANDDataArray[i].assignedPerson != undefined
												&& $scope.firstSearchANDDataArray[i].assignedPerson != null ? $scope.firstSearchANDDataArray[i].assignedPerson
												: "").toLowerCase().includes(
												$scope.secondSearchText
														.toLowerCase())
												|| ($scope.firstSearchANDDataArray[i].deptName != undefined
														&& $scope.firstSearchANDDataArray[i].deptName != null ? $scope.firstSearchANDDataArray[i].deptName
														: "")
														.toLowerCase()
														.includes(
																$scope.secondSearchText
																		.toLowerCase()))
											$scope.assetNonChronoDataFilterList
													.push($scope.firstSearchANDDataArray[i]);

									} else if ($scope.secondMultiSearchDropDown == "fromTime") {
										if ($scope.secondSearchText == ""
												|| $scope.secondSearchText == undefined) {
											$scope.assetNonChronoDataFilterList
													.push($scope.firstSearchANDDataArray[i]);
										} else if (($scope.firstSearchANDDataArray[i].fromTime != undefined
												&& $scope.firstSearchANDDataArray[i].fromTime != null ? $scope.firstSearchANDDataArray[i].fromTime
												: "").toLowerCase().includes(
												$scope.secondSearchText
														.toLowerCase()))
											$scope.assetNonChronoDataFilterList
													.push($scope.firstSearchANDDataArray[i]);

									} else if ($scope.secondMultiSearchDropDown == "toTime") {
										if ($scope.secondSearchText == ""
												|| $scope.secondSearchText == undefined) {
											$scope.assetNonChronoDataFilterList
													.push($scope.firstSearchANDDataArray[i]);
										} else if (($scope.firstSearchANDDataArray[i].toTime != undefined
												&& $scope.firstSearchANDDataArray[i].toTime != null ? $scope.firstSearchANDDataArray[i].toTime
												: "").toLowerCase().includes(
												$scope.secondSearchText
														.toLowerCase()))
											$scope.assetNonChronoDataFilterList
													.push($scope.firstSearchANDDataArray[i]);

									}

								}
							}

							$scope.totalItems = $scope.assetNonChronoDataFilterList.length;
							$scope.currentPage = 1;
							$scope.getTotalPages();
							$scope
									.getAssignedTagIds($scope.assetNonChronoDataFilterList);

							if ($scope.assetNonChronoData.length == 0
									|| $scope.assetNonChronoDataFilterList.length == 0) {
								$scope.assetNonChronoNoData = true;

							} else {
								$scope.assetNonChronoNoData = false;

								if ($scope.assetNonChronoDataFilterList.length > $scope.numPerPage) {
									$scope.assetNonChronoViewData = $scope.assetNonChronoDataFilterList
											.slice(0, $scope.numPerPage);
									$scope.PaginationTab = true;
								} else {
									$scope.assetNonChronoViewData = $scope.assetNonChronoDataFilterList;
									$scope.PaginationTab = false;
								}
							}

						} else if ($scope.varANDORDropDown == "OR") {

							if ($scope.firstMultiSearchDropDown == "All"
									&& ($scope.firstSearchText == "" || $scope.firstSearchText == undefined)) {
								$scope.disableSecondFilterText = true;
								$scope.disableSecondFilterDropDown = true;
								$scope.secondMultiSearchDropDown = "All"
								$scope.secondSearchText = ""

							} else {
								$scope.disableSecondFilterText = false;
								$scope.disableSecondFilterDropDown = false;
							}

							// $scope.assetNonChronoViewData.length = 0;
							// $scope.assetNonChronoDataFilterList.length = 0;
							$scope.assetNonChronoViewData = []
							$scope.assetNonChronoDataFilterList = [];
							$scope.secondSearchORDataArray = [];
							for (var i = 0; i < $scope.assetNonChronoData.length; i++) {
								if ($scope.secondMultiSearchDropDown == "All") {
									if ($scope.secondSearchText == ""
											|| $scope.secondSearchText == undefined) {
										$scope.secondSearchORDataArray
												.push($scope.assetNonChronoData[i]);

									} else {
										if ((($scope.assetNonChronoData[i].assetId)
												.toString() != undefined
												&& ($scope.assetNonChronoData[i].assetId)
														.toString() != null ? ($scope.assetNonChronoData[i].assetId)
												.toString()
												: "").toLowerCase().includes(
												$scope.secondSearchText
														.toLowerCase())
												|| ($scope.assetNonChronoData[i].assetType != undefined
														&& $scope.assetNonChronoData[i].assetType != null ? $scope.assetNonChronoData[i].assetType
														: "")
														.toLowerCase()
														.includes(
																$scope.secondSearchText
																		.toLowerCase())
												|| ($scope.assetNonChronoData[i].subType != undefined
														&& $scope.assetNonChronoData[i].subType != null ? $scope.assetNonChronoData[i].subType
														: "")
														.toLowerCase()
														.includes(
																$scope.secondSearchText
																		.toLowerCase())
												|| ($scope.assetNonChronoData[i].rangeValue != undefined
														&& $scope.assetNonChronoData[i].rangeValue != null ? $scope.assetNonChronoData[i].rangeValue
														: "")
														.toLowerCase()
														.includes(
																$scope.secondSearchText
																		.toLowerCase())
												|| ($scope.assetNonChronoData[i].type != undefined
														&& $scope.assetNonChronoData[i].type != null ? $scope.assetNonChronoData[i].type
														: "")
														.toLowerCase()
														.includes(
																$scope.secondSearchText
																		.toLowerCase())
												|| ($scope.assetNonChronoData[i].manufacturer != undefined
														&& $scope.assetNonChronoData[i].manufacturer != null ? $scope.assetNonChronoData[i].manufacturer
														: "")
														.toLowerCase()
														.includes(
																$scope.secondSearchText
																		.toLowerCase())
												|| ($scope.assetNonChronoData[i].assignedPerson != undefined
														&& $scope.assetNonChronoData[i].assignedPerson != null ? $scope.assetNonChronoData[i].assignedPerson
														: "")
														.toLowerCase()
														.includes(
																$scope.secondSearchText
																		.toLowerCase())
												|| ($scope.assetNonChronoData[i].deptName != undefined
														&& $scope.assetNonChronoData[i].deptName != null ? $scope.assetNonChronoData[i].deptName
														: "")
														.toLowerCase()
														.includes(
																$scope.secondSearchText
																		.toLowerCase())
												|| ($scope.assetNonChronoData[i].fromTime != undefined
														&& $scope.assetNonChronoData[i].fromTime != null ? $scope.assetNonChronoData[i].fromTime
														: "")
														.toLowerCase()
														.includes(
																$scope.secondSearchText
																		.toLowerCase())
												|| ($scope.assetNonChronoData[i].toTime != undefined
														&& $scope.assetNonChronoData[i].toTime != null ? $scope.assetNonChronoData[i].toTime
														: "")
														.toLowerCase()
														.includes(
																$scope.secondSearchText
																		.toLowerCase())) {
											$scope.secondSearchORDataArray
													.push($scope.assetNonChronoData[i]);

										}
									}
								} else {

									// to check other than all in second search
									if ($scope.secondMultiSearchDropDown == "assetId") {
										if ($scope.secondSearchText == ""
												|| $scope.secondSearchText == undefined) {
											$scope.secondSearchORDataArray
													.push($scope.assetNonChronoData[i]);
										} else if ((($scope.assetNonChronoData[i].assetId)
												.toString() != undefined
												&& ($scope.assetNonChronoData[i].assetId)
														.toString() != null ? ($scope.assetNonChronoData[i].assetId)
												.toString()
												: "").toLowerCase().includes(
												$scope.secondSearchText
														.toLowerCase()))
											$scope.secondSearchORDataArray
													.push($scope.assetNonChronoData[i]);

									} else if ($scope.secondMultiSearchDropDown == "assetType") {
										if ($scope.secondSearchText == ""
												|| $scope.secondSearchText == undefined) {
											$scope.secondSearchORDataArray
													.push($scope.assetNonChronoData[i]);
										} else if (($scope.assetNonChronoData[i].type != undefined
												&& $scope.assetNonChronoData[i].type != null ? $scope.assetNonChronoData[i].type
												: "").toLowerCase().includes(
												$scope.secondSearchText
														.toLowerCase()))
											$scope.secondSearchORDataArray
													.push($scope.assetNonChronoData[i]);

									} else if ($scope.secondMultiSearchDropDown == "subType") {
										if ($scope.secondSearchText == ""
												|| $scope.secondSearchText == undefined) {
											$scope.secondSearchORDataArray
													.push($scope.assetNonChronoData[i]);
										} else if (($scope.assetNonChronoData[i].subType != undefined
												&& $scope.assetNonChronoData[i].subType != null ? $scope.assetNonChronoData[i].subType
												: "").toLowerCase().includes(
												$scope.secondSearchText
														.toLowerCase()))
											$scope.secondSearchORDataArray
													.push($scope.assetNonChronoData[i]);

									} else if ($scope.secondMultiSearchDropDown == "range") {
										if ($scope.secondSearchText == ""
												|| $scope.secondSearchText == undefined) {
											$scope.secondSearchORDataArray
													.push($scope.assetNonChronoData[i]);
										} else if (($scope.assetNonChronoData[i].rangeValue != undefined
												&& $scope.assetNonChronoData[i].rangeValue != null ? $scope.assetNonChronoData[i].rangeValue
												: "").toLowerCase().includes(
												$scope.secondSearchText
														.toLowerCase()))
											$scope.secondSearchORDataArray
													.push($scope.assetNonChronoData[i]);
									} else if ($scope.secondMultiSearchDropDown == "type") {
										if ($scope.secondSearchText == ""
												|| $scope.secondSearchText == undefined) {
											$scope.secondSearchORDataArray
													.push($scope.assetNonChronoData[i]);
										} else if (($scope.assetNonChronoData[i].assetType != undefined
												&& $scope.assetNonChronoData[i].assetType != null ? $scope.assetNonChronoData[i].assetType
												: "").toLowerCase().includes(
												$scope.secondSearchText
														.toLowerCase()))
											$scope.secondSearchORDataArray
													.push($scope.assetNonChronoData[i]);

									} else if ($scope.secondMultiSearchDropDown == "manufacturer") {
										if ($scope.secondSearchText == ""
												|| $scope.secondSearchText == undefined) {
											$scope.secondSearchORDataArray
													.push($scope.assetNonChronoData[i]);
										} else if (($scope.assetNonChronoData[i].manufacturer != undefined
												&& $scope.assetNonChronoData[i].manufacturer != null ? $scope.assetNonChronoData[i].manufacturer
												: "").toLowerCase().includes(
												$scope.secondSearchText
														.toLowerCase()))
											$scope.secondSearchORDataArray
													.push($scope.assetNonChronoData[i]);

									} else if ($scope.secondMultiSearchDropDown == "assignedTo") {
										if ($scope.secondSearchText == ""
												|| $scope.secondSearchText == undefined) {
											$scope.secondSearchORDataArray
													.push($scope.assetNonChronoData[i]);
										} else if (($scope.assetNonChronoData[i].assignedPerson != undefined
												&& $scope.assetNonChronoData[i].assignedPerson != null ? $scope.assetNonChronoData[i].assignedPerson
												: "").toLowerCase().includes(
												$scope.secondSearchText
														.toLowerCase())
												|| ($scope.assetNonChronoData[i].deptName != undefined
														&& $scope.assetNonChronoData[i].deptName != null ? $scope.assetNonChronoData[i].deptName
														: "")
														.toLowerCase()
														.includes(
																$scope.secondSearchText
																		.toLowerCase()))
											$scope.secondSearchORDataArray
													.push($scope.assetNonChronoData[i]);

									} else if ($scope.secondMultiSearchDropDown == "fromTime") {
										if ($scope.secondSearchText == ""
												|| $scope.secondSearchText == undefined) {
											$scope.secondSearchORDataArray
													.push($scope.assetNonChronoData[i]);
										} else if (($scope.assetNonChronoData[i].fromTime != undefined
												&& $scope.assetNonChronoData[i].fromTime != null ? $scope.assetNonChronoData[i].fromTime
												: "").toLowerCase().includes(
												$scope.secondSearchText
														.toLowerCase()))
											$scope.secondSearchORDataArray
													.push($scope.assetNonChronoData[i]);

									} else if ($scope.secondMultiSearchDropDown == "toTime") {
										if ($scope.secondSearchText == ""
												|| $scope.secondSearchText == undefined) {
											$scope.secondSearchORDataArray
													.push($scope.assetNonChronoData[i]);
										} else if (($scope.assetNonChronoData[i].toTime != undefined
												&& $scope.assetNonChronoData[i].toTime != null ? $scope.assetNonChronoData[i].toTime
												: "").toLowerCase().includes(
												$scope.secondSearchText
														.toLowerCase()))
											$scope.secondSearchORDataArray
													.push($scope.assetNonChronoData[i]);

									}

								}
							}
							// to concat first and second search arrays
							for (var i = 0; i < $scope.firstSearchANDDataArray.length; i++) {
								if (!$scope.secondSearchORDataArray
										.includes($scope.firstSearchANDDataArray[i]))
									$scope.secondSearchORDataArray
											.push($scope.firstSearchANDDataArray[i]);
							}
							$scope.assetNonChronoDataFilterList = $scope.secondSearchORDataArray;
							//$scope.assetNonChronoDataFilterList = ($scope.firstSearchANDDataArray).concat($scope.secondSearchORDataArray);

							//for pagination
							$scope.totalItems = $scope.assetNonChronoDataFilterList.length;
							$scope.currentPage = 1;
							$scope.getTotalPages();
							$scope
									.getAssignedTagIds($scope.assetNonChronoDataFilterList);

							if ($scope.assetNonChronoData.length == 0
									|| $scope.assetNonChronoDataFilterList.length == 0) {
								$scope.assetNonChronoNoData = true;

							} else {
								$scope.assetNonChronoNoData = false;

								if ($scope.assetNonChronoDataFilterList.length > $scope.numPerPage) {
									$scope.assetNonChronoViewData = JSON
											.parse(JSON
													.stringify($scope.assetNonChronoDataFilterList
															.slice(
																	0,
																	$scope.numPerPage)));
									$scope.PaginationTab = true;
								} else {
									$scope.assetNonChronoViewData = JSON
											.parse(JSON
													.stringify($scope.assetNonChronoDataFilterList));
									$scope.PaginationTab = false;
								}
							}
						}

					}

				});
