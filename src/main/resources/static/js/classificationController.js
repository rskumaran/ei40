app
	.controller(
		'classificationController',
		function($scope, $http, $rootScope, $window, $location,$timeout) {
			$scope.user = JSON.parse($window.sessionStorage
					.getItem("user"));
			if ($scope.user == null) {
				$location.path('/login');
				return;
			}
			 // Timepicker
  			$scope.add=true;
			$scope.update=false;
	     	$scope.delete=false;
            $scope.classificData= new Array();
            $scope.classificViewData= new Array();
            $scope.classificDataFilterList = new Array();
			$scope.searchText = "";
            $scope.addclassific={};

		   $('#stimepicker').datetimepicker({
			      format: 'HH:mm'
		    })
         $('#etimepicker').datetimepicker({
			      format: 'HH:mm'
		    })

		    
		    $scope.searchOption = function() {
						$scope.filterOption();
					}

		   $scope.filterOption = function() {

				$scope.classificViewData = [];
				$scope.classificDataFilterList = [];

				for (var i = 0; i < $scope.classificData.length; i++) {
					// if ($scope.filterValue == "All"
					// || $scope.shiftData[i].status ==
					// $scope.filterValue) {

					if ($scope.searchText == '') {
						$scope.classificDataFilterList
								.push($scope.classificData[i]);
					} else {
						if ($scope.classificData[i].className
								.toLowerCase()
								.includes(
										$scope.searchText.toLowerCase())) {
							$scope.classificDataFilterList
									.push($scope.classificData[i]);

						}

					}
					// }
				}

				$scope.totalItems = $scope.classificDataFilterList.length;
				$scope.currentPage = 1;
				$scope.getTotalPages();

				if ($scope.classificData.length == 0
						|| $scope.classificDataFilterList.length == 0) {
					$scope.classificNoData = true;

				} else {
					$scope.classificNoData = false;

					var begin = (($scope.currentPage - 1) * $scope.numPerPage), end = begin
							+ $scope.numPerPage;

					$scope.classificViewData = $scope.classificDataFilterList
							.slice(begin, end);
					/*
					 * if ($scope.shiftDataFilterList.length >
					 * $scope.numPerPage) $scope.shiftViewData =
					 * $scope.shiftDataFilterList .slice(0,
					 * $scope.numPerPage); else $scope.shiftViewData =
					 * $scope.shiftDataFilterList; $scope.apply();
					 */
				}
			}

			$scope.getClassificData = function() {
				if (!$rootScope.checkNetconnection()) { 
					$rootScope.alertDialogChecknet();
					return; 
					}			
						$rootScope.showloading('#loadingBar');
						$http({
							method : 'GET',
							url : './zoneClassification',
							
						})
								.then(
										function success(response) {
											$rootScope.hideloading('#loadingBar');
											if (response != null
													&& response.data != null
													&& response.data.length != 0
													&& response.data != "BAD_REQUEST"
													&& response.data.data != undefined
													&& response.data.data.length != 0) {
												
											  
												$scope.classificData.length = 0;

												for (var i = 0; i < response.data.data.length; i++) {
													if (response.data.data[i].active == true) {
														$scope.classificData
																.push(response.data.data[i]);
													}
												}
												if ($scope.classificData.length == 0) {
													$scope.classificNoData = true;
													return;
												} else {
													$scope.classificNoData = false;
												}
												
												if($scope.classificData.length > 10){
													$scope.clsTab=true;
												}else{
													$scope.clsTab=false;
												}
												$scope.totalItems=$scope.classificData.length;
											
												$scope.getTotalPages();
							
									$scope.currentPage=1;
									if ($scope.classificData.length > $scope.numPerPage){
										$scope.classificViewData =$scope.classificData
										.slice(0, $scope.numPerPage);
											        $scope.hasPrevious = true;
				                                    $scope.isFirstPage = true;
				                                    $scope.hasNext = false;
				                                    $scope.isLastPage = false;
										}
										else{
									$scope.classificViewData = $scope.classificData;
									}
									}else {
										$scope.classificNoData = true;
									}
									
					},function error(response) {
						$rootScope.hideloading('#loadingBar');
						$rootScope.fnHttpError(response);
					});
					}
                
				  $scope.getClassificData();
		
				  $("#id_classificationName").keypress(
							function(e) {
								// if the letter is not alphabets then display error
								// and don't type anything
								$return = ((e.which > 64) && (e.which < 91)) || ((e.which> 96) && (e.which < 123)) ||(e.which == 8) || (e.which == 32)  || (e.which >= 48 && e.which <= 57)
								if (!$return)
													
								{
									
									return false;
								}
							});		
			      $scope.addClassific = function() {
			    	    var clsname = document.getElementById("id_classificationName");
						var clscolor = document.getElementById("clscolor");
					     data = {
						 "className" : $scope.addclassific.className ,
					     "color" : $scope.addclassific.clscolor 
					    }
						
						if (clsname.value.length == 0) {
							$scope.nameError = true;
							$scope.name_error_msg = "Please enter the name";
							$timeout(function() {
								$scope.nameError = false;
							}, 2000);
							return;
						}
					     if(data.color==undefined){
					    	 data = {
									 "className" : $scope.addclassific.className ,
								     "color" : "#000000" 
								    }
					     }

						if ($scope.addclassificForm.$valid) {
							if (!$rootScope.checkNetconnection()) { 
								$rootScope.alertDialogChecknet();
								return; 
								}
							$rootScope.showloading('#loadingBar');
			
							$http({
								method : 'POST',
								url : './zoneClassification',
								data : data
								
							})
									.then(
											function(response) {
												if (response != null
														&& response.data != null
														&& response.data != "BAD_REQUEST") {
													$rootScope.hideloading('#loadingBar');
													$scope.shift = response.data;
													$rootScope.alertDialog(response.data.message);
													$timeout(
															function() {
																 $scope.getClassificData();
																$scope.cancelClassific();

															}, 2000);
												} else {
													$rootScope.hideloading('#loadingBar');
													$rootScope.alertDialog(response.data.message);
													$timeout(function() {
                                                       $scope.cancelClassific();													
                                                           }, 2000);
												}
											},function(error) {
												$rootScope.hideloading('#loadingBar');
												$rootScope.fnHttpError(error);
											});
						}

				
					}
					
					 
                 $scope.rowHighilited = function(row) {
	
	                    $scope.selectedRow = row;
	
		                $scope.addclassific.className = $scope.classificViewData[row].className;
						$scope.addclassific.clscolor = $scope.classificViewData[row].color;
						$scope.addclassific.id = $scope.classificViewData[row].id;
						$scope.update=true;
						$scope.delete=true;
						$scope.add=false;
						
						}
				 $scope.updateClassific = function() {		
	                   var name = document.getElementById("id_classificationName");
						
						if (name.value.length == 0) {
							$scope.nameError = true;
							$scope.name_error_msg = "Please enter the name";
							$timeout(function() {
								$scope.nameError = false;
							}, 2000);
							return;
						}
						if (!$rootScope.checkNetconnection()) { 
							$rootScope.alertDialogChecknet();
							return; 
							}
						if ($scope.addclassificForm.$valid) {
							$rootScope.showloading('#loadingBar');
				         var classificName = $scope.addclassific.className;
				         var clscolor = $scope.addclassific.clscolor;
				         var id = $scope.addclassific.id

						     data = {
							 "className" : classificName ,
						     "color" : clscolor ,
	                         "id" : id 
						    }
							$http({
								method : 'PUT',
								url : './zoneClassification',
								data : data,
								
							})
									.then(
											function(response) {
												if (response != null
														&& response.data != null
														&& response.data != "BAD_REQUEST") {
													$rootScope.hideloading('#loadingBar');
													$scope.classific = response.data;
													$rootScope.alertDialog(response.data.message);
												
													$timeout(
															function() {
																 $scope.getClassificData();
																 $scope.cancelClassific();
															}, 2000);
												} else {
													$rootScope.hideloading('#loadingBar');
													$rootScope.alertDialog(response.data.message);
													$timeout(function() {
																$scope.cancelClassific();
													}, 2000);
												}
											},function(error) {
												$rootScope.hideloading('#loadingBar');
												$rootScope.fnHttpError(error);
											});
						}

				
					}
									
				 $scope.deleteClassific = function() {
					 
	                      var name = document.getElementById("id_classificationName");
						
						if (name.value.length == 0) {
							$scope.nameError = true;
							$scope.name_error_msg = "Please enter the name";
							$timeout(function() {
								$scope.nameError = false;
							}, 2000);
							return;
						}

						if ($scope.addclassificForm.$valid) {
							$.confirm({
										title : $rootScope.MSGBOX_TITLE,
										content : 'Are you sure you want to delete this item?',
										type: 'blue',
										columnClass:'medium',
										autoClose : false,
                         				icon: 'fa fa-info-circle',
										buttons: {
										yes: {
										text: 'Yes', 
										btnClass: 'btn-blue',
										action : function() {
											if (!$rootScope.checkNetconnection()) { 
												$rootScope.alertDialogChecknet();
												return; 
												}				
							$rootScope.showloading('#loadingBar');
							var id = $scope.addclassific.id
						     data = {
							 "id" : id 
						    }
							$http({
								method : 'DELETE',
								url : './zoneClassification',
								headers: {
								'Content-Type':'application/json'
								},
								data : data,
								
							})
									.then(
											function(response) {
												if (response != null
														&& response.data != null
														&& response.data != "BAD_REQUEST") {
													$rootScope.hideloading('#loadingBar');
													$scope.classific = response.data;
													$rootScope.alertDialog(response.data.message);
													$timeout(
															function() {
																 $scope.getClassificData();
																$scope.cancelClassific();
															}, 2000);
												} else {
													$rootScope.hideloading('#loadingBar');
													$rootScope.alertDialog(response.data.message);
													$timeout(function() {
														$scope.cancelClassific();
													}, 2000);
												}
											},function(error) {
												$rootScope.hideloading('#loadingBar');
												$rootScope.fnHttpError(error);
											});
											  }
									},
									   no:function () {
								           
										   		}
											}
									});
						}

				
					}
				 $scope.cancelClassific = function() {	
				        $scope.addclassific={};
						$scope.update=false;
						$scope.delete=false;
						$scope.add=true;
						$scope.selectedRow = -1;
					}
					
		// pagination button
		
		$scope.currentPage = 1;
		$scope.numPerPage = 10;
		$scope.maxSize = 3;
		$scope.totalItems = 0;
		 $scope.classificDataFilterList=[];
		 $scope.classificViewData=[];
		$scope
				.$watch(
						'currentPage + numPerPage',
						function() {
							var begin = (($scope.currentPage - 1) * $scope.numPerPage), end = begin
									+ $scope.numPerPage;

								$scope.classificViewData = $scope.classificData
										.slice(begin, end);
						

						});

		$scope.currentPage = 1;

		$scope.maxSize = 3;

		$scope.numPerPage = 10;
		$scope.totalItems = 0;
		$scope.totalPages = 0;

		$scope.hasNext = true;
		$scope.hasPrevious = true;
		$scope.isFirstPage = true;
		$scope.isLastPage = true;
		$scope.serial = 1;

		$scope
				.$watch(
						"currentPage",
						function(newVal, oldVal) {
							if (newVal !== oldVal) {

								if (newVal > $scope.totalPages) {
									$scope.currentPage = $scope.totalPages;
								} else {
									$scope.currentPage = newVal;
								}

								$scope.serial = (($scope.currentPage - 1) * $scope.numPerPage) + 1;

							}
						});

		$scope.enablePageButtons = function() {
			$scope.serial = (($scope.currentPage - 1) * $scope.numPerPage) + 1;
			$scope.selectedRow = -1;
			if ($scope.totalPages <= 1) {
				$scope.hasPrevious = true;
				$scope.isFirstPage = true;
				$scope.hasNext = true;
				$scope.isLastPage = true;

			} else if ($scope.totalPages > 1
					&& $scope.currentPage == $scope.totalPages) {

				$scope.hasPrevious = false;
				$scope.isFirstPage = false;
				$scope.hasNext = true;
				$scope.isLastPage = true;
			} else if ($scope.currentPage != 1
					&& $scope.currentPage < $scope.totalPages) {

				$scope.hasPrevious = false;
				$scope.isFirstPage = false;
				$scope.hasNext = false;
				$scope.isLastPage = false;

			} else {
				$scope.hasPrevious = true;
				$scope.isFirstPage = true;
				$scope.hasNext = false;
				$scope.isLastPage = false;
			}
		};

		$scope.getTotalPages = function() {

			if (($scope.totalItems % $scope.numPerPage) > 0)
				$scope.totalPages = parseInt($scope.totalItems
						/ $scope.numPerPage) + 1;
			else
				$scope.totalPages = parseInt($scope.totalItems
						/ $scope.numPerPage);

			$scope.enablePageButtons();

		};

		$scope.goFirstPage = function() {
			$scope.currentPage = 1;
			$scope.enablePageButtons();
		};

		$scope.goPreviousPage = function() {
			if ($scope.currentPage > 1)
				$scope.currentPage = $scope.currentPage - 1;
			$scope.enablePageButtons();
		};

		$scope.goNextPage = function() {
			if ($scope.currentPage <= $scope.totalPages)
				$scope.currentPage = $scope.currentPage + 1;
			$scope.enablePageButtons();
		};

		$scope.goLastPage = function() {
			$scope.currentPage = $scope.totalPages;
			$scope.enablePageButtons();
		};				
					
		});
