/*
 * L.Grid displays a grid of lat/lng lines on the map.
 */

L.Grid = L.LayerGroup
		.extend({
			options : {/*
			 * xticks : 10, yticks : 10,
			 */
				// 'decimal' or one of the templates below
				coordStyle : 'MinDec',
				coordTemplates : {
					'MinDec' : '{degAbs}&deg;&nbsp;{minDec}\'{dir}',
					'DMS' : '{degAbs}{dir}{min}\'{sec}"'
				},

				// Path style for the grid lines
				lineStyle : {
					stroke : true,
					color : '#000',
					opacity : 0.8,
					weight : 0.5
				},

				// Redraw on move or moveend
				redraw : 'move'
			},

			initialize : function(options) {
				L.LayerGroup.prototype.initialize.call(this);
				L.Util.setOptions(this, options);

			},

			onAdd : function(map) {
				this._map = map;

				var grid = this.redraw();
				this._map.on('viewreset ' + this.options.redraw, function() {
					grid.redraw();
				});

				this.eachLayer(map.addLayer, map);
			},

			onRemove : function(map) {
				// remove layer listeners and elements
				map.off('viewreset ' + this.options.redraw, this.map);
				this.eachLayer(this.removeLayer, this);
			},

			redraw : function() {
				this.eachLayer(this.removeLayer, this);
				// pad the bounds to make sure we draw the lines a little longer
				this._bounds = this.options.options.bounds; // this._map.getBounds().pad(0.5);
				if (this._bounds == undefined)
					this._map.getBounds().pad(0.5);

				var grid = [];
				var i;

				var latLines = this._latLines();
				for (i in latLines) {
					if (Math.abs(latLines[i]) > 90) {
						continue;
					}
					grid.push(this._drawLine(latLines[i]));
					// grid.push(this._label('lat', latLines[i]));
				}

				var lngLines = this._lngLines();
				for (i in lngLines) {
					grid.push(this._drawLine(lngLines[i]));
					// grid.push(this._label('lng', lngLines[i]));
				}

				this.eachLayer(this.removeLayer, this);

				for (i in grid) {
					this.addLayer(grid[i]);
				}
				return this;
			},

			_latLines : function() {
				var yticks = this.options.options.gridSize;
				return this._lines(yticks, 'lat');
			},
			_lngLines : function() {

				var xticks = this.options.options.gridSize;
				return this._lines(xticks, 'lng');
			},

			_lines : function(gridSize, type) {
				var ticks = 0, low;
				if (type == 'lng') {
					ticks = Math.abs(this.options.options.mapDetails.width_m
							/ gridSize);
				} else {
					ticks = Math.abs(this.options.options.mapDetails.height_m
							/ gridSize);
				}

				if (this.options.options.mapDetails.mapType == 'mist')
					gridSize = gridSize * this.options.options.mapDetails.ppm;
				else if (this.options.options.mapDetails.mapType == 'quuppa'){
					if (type == 'lng') {
						gridSize = gridSize
								* (1 / this.options.options.mapDetails.ppmx);

					} else {
						gridSize = gridSize
								* (1 / this.options.options.mapDetails.ppmy);

					}
				}
				var lines = [];
				for (var i = 1; i <= ticks; i++) {
					if (type == 'lng') {
						var st_pt = {
							'x' : i * gridSize,
							'y' : 0
						};
						var et_pt = {
							'x' : i * gridSize,
							'y' : this.options.options.mapDetails.height
						};

						var pt = [];
						pt.push(st_pt);
						pt.push(et_pt);
						lines.push(pt);
					} else {
						var st_pt = {
							'x' : 0,
							'y' : i * gridSize
						};
						var et_pt = {
							'x' : this.options.options.mapDetails.width,
							'y' : i * gridSize
						};

						var pt = [];
						pt.push(st_pt);
						pt.push(et_pt);
						lines.push(pt);
					}
				}
				return lines;
			},

			_drawLine : function(latlng) {
				return new L.Polyline([
						this.pixelsToLatLng(latlng[0].x, latlng[0].y),
						this.pixelsToLatLng(latlng[1].x, latlng[1].y) ],
						this.options.lineStyle);
			},

			pixelsToLatLng : function(x, y) {
				return this._map.unproject([ x, y ],
						this.options.options.defZoomValue);
			},

			_round : function(num, delta) {
				var ret;

				delta = Math.abs(delta);
				if (delta >= 1) {
					if (Math.abs(num) > 1) {
						ret = Math.round(num);
					} else {
						ret = (num < 0) ? Math.floor(num) : Math.ceil(num);
					}
				} else {
					var dms = this._dec2dms(delta);
					if (dms.min >= 1) {
						ret = Math.ceil(dms.min) * 60;
					} else {
						ret = Math.ceil(dms.minDec * 60);
					}
				}

				return ret;
			},

			_dec2dms : function(num) {
				var deg = Math.floor(num);
				var min = ((num - deg) * 60);
				var sec = Math.floor((min - Math.floor(min)) * 60);
				return {
					deg : deg,
					degAbs : Math.abs(deg),
					min : Math.floor(min),
					minDec : min,
					sec : sec
				};
			},

			formatCoord : function(num, axis, style) {
				if (!style) {
					style = this.options.coordStyle;
				}
				if (style == 'decimal') {
					var digits;
					if (num >= 10) {
						digits = 2;
					} else if (num >= 1) {
						digits = 3;
					} else {
						digits = 4;
					}
					return num.toFixed(digits);
				} else {
					// Calculate some values to allow flexible templating
					var dms = this._dec2dms(num);

					var dir;
					if (dms.deg === 0) {
						dir = '&nbsp;';
					} else {
						if (axis == 'lat') {
							dir = (dms.deg > 0 ? 'N' : 'S');
						} else {
							dir = (dms.deg > 0 ? 'E' : 'W');
						}
					}

					return L.Util.template(this.options.coordTemplates[style],
							L.Util.extend(dms, {
								dir : dir,
								minDec : Math.round(dms.minDec, 2)
							}));
				}
			}

		});

L.grid = function(options) {
	return new L.Grid(options);
};
