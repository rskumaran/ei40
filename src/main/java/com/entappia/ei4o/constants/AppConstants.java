package com.entappia.ei4o.constants;

public class AppConstants {

	public static boolean print_log = false;

	public static enum AssignmentStatus {
		NOT_ASSIGNED, ASSIGNED
	}

	public static enum AssignmentType {
		 PERSON, DEPT, TAG
	}

	public static enum ZoneType {
		Trackingzone, DeadZone
	}

	public static enum LogEventType {
		EVENT, ERROR
	}

	public static enum NotificationType {
		INFORMATION, WARNING, EMERGENCY
	}

	public static final String timeZone = "Asia/Kolkata";
	public static final String MISSING_TAG_ALERT = "missingTagAlert";
	public static final String TRANSACTION_TABLE_AGE = "transactionTableAge";
	
	
	

	public static final String[] alertKeys = { "lostTag", "blockedTag", "lateVisitor", "crowdedZones",
			"missingTag", "lostConnection", "lateAsset", "assetExpiryAlert", "lslmExpiryAlert", "restrictedZoneVisitor", "WOshippingZone","WOLimitInputZoneOutputZone" };

	public static final String RequestDateFormat = "yyyy-MMM-dd";
	public static final String LogRequestDateFormat = "yyyy-MMM-dd HH:mm:ss.SSS";
	
	
	public static final String[] capabilityList = { "ANAR", "ASTA", "DWOA", "PTEA", "ANVA", "PTED",  "CZOM", "HTED", "PTAL",
			"ATED", "ANAD", "PTAR", "ANED", "WACT", "PPAR", "WTED", "PLVW", "HTAR", "DPAR", "DTAN", "PPFA", "ASLV", "HMRP",
			"WWVA", "WMVA", "DSFA", "WCND", "WWAD", "CORS", "CTPE", "ATAR", "CDPT", "WOFA", "CART", "WMMA", "CTVA", "SKLV",
			"PEVA", "CCLA", "AAVH", "ACAR", "CREG", "HMVA", "WMAD", "ACAL", "ACVA", "WWED", "ACAD", "WMED", "ACED", "SIMP",
			"WCAL", "LSLG", "POTU", "SDET", "STED", "WTAR", "CAMG", "CTBI", "DPFA", "HKLV", "HMDP", "DZFA", "PEED", "CTRN",
			"STAR", "CLVW", "HMDS", "WZFA", "PEAD", "WLVW"};
	
	 
	/*
	 * public static final Pattern DATE_PATTERN_WITH_T= Pattern.compile(
	 * "^\\d{4}/\\d{2}/\\d{2}\\s+\\d{2}:\\d{2}:\\d{2}$");
	 */
	public static final String seckretKey = "Ei4.0123!!!";

	public static final String SMSOTPAPIKEY = "161195AOLU0fn2594b827e";
	public static final String SMSAPIKEY = "352172AUBKftjitxg86006943cP1";

	public static final String session_time = "session_time";
	// Log types
	public static final String event = "Event";
	public static final String error = "Error";
	public static final String heartBeat = "HeartBeat";

	// log result
	public static final String request = "Request";
	public static final String success = "Success";
	public static final String errormsg = "Error in process";
	public static final String errorsession = "SessionError";
	public static final String SystemLog = "SystemLog";
	public static final String errorDatemsg = "Invalid date format, valid date format is yyyy-MMM-dd";
	public static final String errorDateTimemsg = "Invalid date format, valid date format is yyyy-MMM-dd HH:mm";
	public static final String errorDatemismatch = "Start date is greater than end date";
	public static final String errorEndDateOverflow = "End date is greater than today";
	public static final String errorNoRecordsFound = "No Records Found";
	
	
	// main api type
	public static final String zoneAnalytics = "ZOA";
	public static final String CT = "CTR";
	public static final String zoneClassification = "CLA";
	public static final String tag = "TAG";
	public static final String GAU = "GEt All USER";
	public static final String Dept = "DEPARTMENT";
	public static final String ASSTVCE = "ASSETVECICLE";
	public static final String ASSTCHO = "ASSTCHO";
	public static final String ASSTNCHO = "ASSTNCHO";
	public static final String ALLOCATION = "ALLOCATION";
	public static final String SKUALLOCATION = "SKUALLOCATION";
	public static final String WORK_STATION = "WSTA";
	public static final String PATHWAY = "PATHWAY";
	public static final String OUTLINE = "OUTLINE";
	public static final String PATHSEGMENTS = "PATHSEGMENTS";
	public static final String CONTROLS = "CONTROL";
	public static final String WEBSOCKET = "WEBSOCKET";
	public static final String NOTIFICATION = "NOTIFICATION";
	public static final String PROFILE = "PROFILE";
	public static final String ANALYTICS = "ANALYTICS";
	public static final String SKUDETAILS = "SKUDETAILS";
	public static final String WAREHOUSE = "WAREHOUSE";
	public static final String SOCKET = "SOCKET";
	public static final String DEVICEDETAILS = "DEVICEDETAILS";
	public static final String ZOHO_DESK = "ZOHO_DESK";
	
	
	public static final String LOCATOR = "LOCATOR";
	public static final String MTL = "MATERIAL";
	public static final String MAP = "ZONE";
	public static final String apiemp = "EMP";
	public static final String apiauthen = "CRE";
	public static final String apiorgdetails = "ORG";
	public static final String apimist = "MST";
	public static final String apiQuuppa = "QPA";
	public static final String apiOnDemandmist = "ODM";
	public static final String configuration = "CFG";
	public static final String demo = "DEM";
	public static final String credential = "CRE";
	public static final String shift = "SHI";
	public static final String staff = "STA";
	public static final String calibrationRecord = "CAR";
	public static final String campusdetails = "CDE";

	public static final String WALLS = "WALLs";
	public static final String PARTITIONS = "PARTITIONS";
	public static final String DOORS = "DOORS";

	// capability tag for apis
	public static final String dashboardApi = "DSB";
	public static final String logApi = "LOG";
	public static final String BACKUPRESTORE = "BKPRST";
	public static final String BG_TASK = "BG_TASK";

	public static final String firmware = "FIRMWARE";
	public static final String MISSING_TAG_MAC_IDS = "missingTagMacIds";

	// error msg
	public static final String sessiontimeout = "Session timed-out or Invalid";
	public static final String fieldsmissing = "Mandatory fields are missing";
	public static final String UUIDerror = "Invalid UUID";

	// Quuppa Apis
	public static String GET_QPE_INFO = "/qpe/getPEInfo?version=2";
	public static String GET_PROJECT_INFO = "/qpe/getProjectInfo?version=2";
	public static String GET_TAG_URL = "/qpe/getTagData?version=2&maxAge=60000&radius=10";
	public static String GET_LOCATOR_INFO = "/qpe/getLocatorInfo?version=2";
	public static String GET_BATTERY_INFO = "qpe/getTagData?format=defaultInfo&version=2";
	public static String SEND_BUZZER_ALERT = "/qpe/sendQuuppaRequest?tag=%s&requestData=0xff1d00b30a03320a03320a053241&humanReadable=true";
	 
}
