package com.entappia.ei4o.repository;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.entappia.ei4o.dbmodels.OutlinesDetails;

@Repository
public interface OutLineRepository extends CrudRepository<OutlinesDetails, Integer> {

	@Query(value = "select * from outlines_details where campus_id=:campusId", nativeQuery = true)
	List<OutlinesDetails> findByCampusId(@Param("campusId") long campusId);

	OutlinesDetails findByOutlinesId(long outlinesId);
	
	List<OutlinesDetails> findAll();
}
