package com.entappia.ei4o.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.entappia.ei4o.dbmodels.Settings;

@Repository
public interface SettingsRepository extends CrudRepository<Settings, Integer> {

	Settings findByEmailId(String emailId);
	
	
}
