package com.entappia.ei4o.repository;

import java.util.List;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
 
import com.entappia.ei4o.dbmodels.Material;

@Repository
public interface MaterialRepository  extends CrudRepository<Material, Integer> {
	List<Material> findAll();
	 
	Material findByProdId(String prodId);
	 
	List<Material> findByOrdereNo(int ordereNo);
	
	
	
}
