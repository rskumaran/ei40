package com.entappia.ei4o.repository;

import java.util.List;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.entappia.ei4o.dbmodels.WorkStation;
import com.fasterxml.jackson.annotation.JsonIgnore;

@Repository
public interface WorkStationRepository extends CrudRepository<WorkStation, Integer> {
	
	@JsonIgnore 
	WorkStation findByStationId(String stationId);
	
	@JsonIgnore 
	List<WorkStation> findAll();
	
}
