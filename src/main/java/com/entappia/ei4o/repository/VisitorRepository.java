package com.entappia.ei4o.repository;

import java.util.List;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.entappia.ei4o.dbmodels.Staff;
import com.entappia.ei4o.dbmodels.Visitor;
 

@Repository
public interface VisitorRepository  extends CrudRepository<Visitor, Integer> {

	Visitor findByVisitorId(long visitorId);
	
	Visitor findByContactNo(String contactNo);
	
	List<Visitor> findAll();
 
}
