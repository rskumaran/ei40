package com.entappia.ei4o.repository;

import java.util.List;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.entappia.ei4o.dbmodels.CalibrationRecord;

@Repository
public interface CalibrationRecordRepository extends CrudRepository<CalibrationRecord, Integer> {

	CalibrationRecord findByCalibrationId(String calibrationId);
	
	List<CalibrationRecord> findAll();
}
