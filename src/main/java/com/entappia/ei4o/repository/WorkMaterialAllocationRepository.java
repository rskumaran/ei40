package com.entappia.ei4o.repository;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.entappia.ei4o.dbmodels.WorkMaterialAllocation;

@Repository
public interface WorkMaterialAllocationRepository extends CrudRepository<WorkMaterialAllocation, Integer> {
	
	WorkMaterialAllocation findByOrderNoAndMacId(int orderNo, String macId);
	
	@Query(value = "SELECT * FROM work_material_allocation where order_no=:orderNo", nativeQuery = true)
	WorkMaterialAllocation findAllocation(@Param("orderNo") int orderNo);
	
}
