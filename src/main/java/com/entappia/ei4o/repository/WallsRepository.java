package com.entappia.ei4o.repository;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.entappia.ei4o.dbmodels.Walls; 

@Repository
public interface WallsRepository  extends CrudRepository<Walls, Integer> {

	@Query(value = "select * from walls where outlines_id=:outlinesId", nativeQuery = true)
	List<Walls> findByOutlinesId(@Param("outlinesId") long outlinesId);
	
	
	@Query(value = "select * from walls where active=true", nativeQuery = true)
	List<Walls> findAllActiveWalls();
	
	Walls findAllById(long id);
	
	@Modifying
	@Transactional
	@Query(value = "truncate walls", nativeQuery = true)
	void truncateTable();
}
