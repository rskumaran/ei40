package com.entappia.ei4o.repository;

import java.util.List;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.entappia.ei4o.dbmodels.Controls;

@Repository
public interface ControlsRepository  extends CrudRepository<Controls, Integer> {
	
	Controls findByControlId(String controlId);
	
	List<Controls> findAll();
}
