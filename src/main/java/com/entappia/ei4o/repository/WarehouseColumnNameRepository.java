package com.entappia.ei4o.repository;

import java.util.List;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.entappia.ei4o.dbmodels.WarehouseColumnName;
 

@Repository
public interface WarehouseColumnNameRepository extends CrudRepository<WarehouseColumnName, Integer> {

	List<WarehouseColumnName> findAll();
}
