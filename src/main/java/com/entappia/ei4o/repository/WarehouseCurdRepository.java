package com.entappia.ei4o.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.entappia.ei4o.dbmodels.Warehouse;

@Repository
public interface WarehouseCurdRepository extends CrudRepository<Warehouse, Integer>{

}
