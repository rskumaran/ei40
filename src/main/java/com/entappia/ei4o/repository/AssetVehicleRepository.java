package com.entappia.ei4o.repository;

import java.util.List;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.entappia.ei4o.dbmodels.AssetVehicle;
import com.entappia.ei4o.dbmodels.Department;
import com.entappia.ei4o.dbmodels.Material;

@Repository
public interface AssetVehicleRepository extends CrudRepository<AssetVehicle, Integer> {
	List<AssetVehicle> findAll();
	 
	AssetVehicle findByAssetId(String assetId);
}
