package com.entappia.ei4o.repository;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.entappia.ei4o.constants.AppConstants.AssignmentType;
import com.entappia.ei4o.dbmodels.AssetNonChronoAllocation;

@Repository
public interface AssetNonChronoAllocationRepository extends CrudRepository<AssetNonChronoAllocation, Integer> {

	AssetNonChronoAllocation findByAssetIdAndMacId(String assetId, String macId);
	
	@Query(value = "SELECT * FROM asset_non_chrono_allocation where mac_id=:macId  and status='Valid';", nativeQuery = true)
	AssetNonChronoAllocation findByMacId(@Param("macId") String macId);

	AssetNonChronoAllocation findByAssetIdAndAssignmentTypeAndMacId(String assetId, AssignmentType tag, String macId);
	
	AssetNonChronoAllocation findByAssetIdAndAssignmentTypeAndStatus(String assetId, AssignmentType tag, String status);

	@Query(value = "SELECT * FROM asset_non_chrono_allocation where asset_id=:assetId and type=:type and status='Valid';", nativeQuery = true)
	AssetNonChronoAllocation findAllocation(@Param("assetId") String assetId, @Param("type") String type);

}
