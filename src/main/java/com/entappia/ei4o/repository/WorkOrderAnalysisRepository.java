package com.entappia.ei4o.repository;

import java.util.Date;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.entappia.ei4o.dbmodels.WorkOrderAnalysis;


@Repository
public interface WorkOrderAnalysisRepository extends CrudRepository<WorkOrderAnalysis, Integer> {
	
	WorkOrderAnalysis findByOrderNoAndAnalysisDateAndStationId(int orderNo, Date analysisDate, String stationId);
	//WorkOrderAnalysis findByOrderNoAndAnalysisDateAndStationIdAndShiftName(int orderNo, Date analysisDate, String stationId, String shiftName);
	
	/*String value = "INSERT INTO WorkOrderFlow values (orderNo, analysisDate, stationId, shiftName, inboundZoneId, outboundZoneId, workAreaZoneId, storageZoneId,"
			+ "inboundZoneTimeDuration, outboundZoneTimeDuration, workAreaZoneTimeDuration, storageZoneTimeDuration,"
			+ " inboundWorkOrderCount, outboundWorkOrderCount, workAreaWorkOrderCount, storageWorkOrderCount)";	*/
	
	@Query(value = "select * from WorkOrderFlow where active = 1 ", nativeQuery = true)
	void insertOrUpdate();

}
