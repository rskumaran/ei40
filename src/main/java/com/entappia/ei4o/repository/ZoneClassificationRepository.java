package com.entappia.ei4o.repository;

import java.util.List;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.entappia.ei4o.dbmodels.ZoneClassification;

@Repository
public interface ZoneClassificationRepository extends CrudRepository<ZoneClassification, Integer> {
	List<ZoneClassification> findAll();

	ZoneClassification findById(int id);

	ZoneClassification findByClassName(String className);
}
