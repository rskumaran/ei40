package com.entappia.ei4o.repository;

import java.util.Date;
import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.entappia.ei4o.dbmodels.AssetChrono;

@Repository
public interface AssetChronoRepository extends CrudRepository<AssetChrono, Integer> {
	
	AssetChrono findByAssetId(String assetId);
	
	List<AssetChrono> findAll();
	
	@Query(value = "SELECT * FROM asset_chrono where validity_date <= :toDate and validity_date>=:fromDate order by validity_date asc", nativeQuery = true)
	List<AssetChrono> getAssetChronos(@Param("fromDate") Date fromDate, @Param ("toDate") Date toDate );
	
	@Query(value = "SELECT * FROM asset_chrono where validity_date>=:fromDate order by validity_date asc", nativeQuery = true)
	List<AssetChrono> getAssetChronos(@Param("fromDate") Date fromDate );
	
}
