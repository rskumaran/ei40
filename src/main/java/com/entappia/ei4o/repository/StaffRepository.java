package com.entappia.ei4o.repository;

import java.util.List;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.entappia.ei4o.dbmodels.Staff;
 

@Repository
public interface StaffRepository  extends CrudRepository<Staff, Integer> {

	Staff findByEmpId(String empId);
	
	List<Staff> findAll();
}
