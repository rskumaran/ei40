package com.entappia.ei4o.repository;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.entappia.ei4o.dbmodels.Doors;

@Repository
public interface DoorsRepository extends CrudRepository<Doors, Integer> {
	
	@Query(value = "select * from doors where outlines_id=:outlinesId", nativeQuery = true)
	List<Doors> findByOutlinesId(@Param("outlinesId") long outlinesId);
	
	@Query(value = "select * from doors where active=true", nativeQuery = true)
	List<Doors> findAllActiveDoors();

	Doors findAllById(long id);
	
	@Modifying
	@Transactional
	@Query(value = "truncate doors", nativeQuery = true)
	void truncateTable();
}
