package com.entappia.ei4o.repository;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.entappia.ei4o.dbmodels.ProfileConfiguration;

@Repository
public interface ProfileConfigurationRepository extends CrudRepository<ProfileConfiguration, String> {

	ProfileConfiguration findByOrganization(String organization);

	List<ProfileConfiguration> findAll();

	@Modifying
	@Transactional
	@Query(value = "truncate table Profile_Configuration", nativeQuery = true)
	void truncateMyTable();
}
