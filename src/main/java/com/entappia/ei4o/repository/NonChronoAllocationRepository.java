package com.entappia.ei4o.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.entappia.ei4o.dbmodels.AssetNonChronoAllocation; 

@Repository
public interface NonChronoAllocationRepository extends CrudRepository<AssetNonChronoAllocation, Integer> {

}
