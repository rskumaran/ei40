package com.entappia.ei4o.repository;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.entappia.ei4o.constants.AppConstants.AssignmentType;
import com.entappia.ei4o.dbmodels.AssetChronoAllocation;
import com.entappia.ei4o.dbmodels.AssetNonChronoAllocation;

@Repository
public interface AssetChronoAllocationRepository extends CrudRepository<AssetChronoAllocation, Integer> {
	
	AssetChronoAllocation findByAssetId(String assetId);
	 
	@Query(value = "SELECT * FROM asset_chrono_allocation where mac_id=:macId  and status='Valid';", nativeQuery = true)
	AssetChronoAllocation findByMacId(@Param("macId") String macId); 
	  
	AssetChronoAllocation findByAssetIdAndAssignmentTypeAndMacId(String assetId, AssignmentType tag, String macId);
	
	AssetChronoAllocation findByAssetIdAndAssignmentTypeAndStatus(String assetId, AssignmentType tag, String status);
	
	List<AssetChronoAllocation> findByAssignmentTypeAndStatus(AssignmentType tag, String status);


	
 	AssetChronoAllocation findByAssetIdAndMacId(String assetId, String macId);

	@Query(value = "SELECT * FROM asset_chrono_allocation where asset_id=:assetId and status='Valid';", nativeQuery = true)
	AssetChronoAllocation findAllocation(@Param("assetId") String assetId);
	
}
