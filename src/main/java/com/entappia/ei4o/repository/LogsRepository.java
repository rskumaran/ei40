package com.entappia.ei4o.repository;

import java.util.Date;

import javax.transaction.Transactional;

import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.entappia.ei4o.dbmodels.Logs;

@Repository
public interface LogsRepository extends CrudRepository<Logs, Integer> {
	
	@Modifying(clearAutomatically = true)
	@Transactional
	@Query(value = "DELETE FROM logs WHERE date < :date", nativeQuery = true)
	int deleteByDate(@Param("date") Date date);
	
	/*@Query(value = "Select max(log_id) from logs", nativeQuery = true)
	int GetMaxLogId();
	
	//@Query(value = "SELECT * FROM logs WHERE log_id BETWEEN 60 and 75")
	//SELECT * FROM db_ei40.logs where log_id <= 301 order by log_id desc limit 10;
	
	//@Query(value = "SELECT * FROM logs ORDER BY log_id DESC LIMIT :pageNumber,:pageSize", nativeQuery = true)
	@Query(value = "SELECT * FROM logs where log_id < :logId order by log_id desc limit :noOfRecords", nativeQuery = true)
	Iterable<Logs> selectLogs(@Param("logId") int logId,@Param ("noOfRecords") int noOfRecords  );*/
}
