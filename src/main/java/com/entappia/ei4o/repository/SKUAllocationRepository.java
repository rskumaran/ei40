package com.entappia.ei4o.repository;

import java.util.Date;
import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.entappia.ei4o.dbmodels.SKUAllocation;

@Repository
public interface SKUAllocationRepository extends CrudRepository<SKUAllocation, Integer> {

	SKUAllocation findByTagIdAndUpcCode(String tagId, String upcCode);

	@Query(value = "SELECT * FROM sku_allocation where upc_code = :upcCode and status='Valid'", nativeQuery = true)
	SKUAllocation findByUPCCode(@Param("upcCode") String upcCode);

	@Query(value = "SELECT * FROM sku_allocation where tag_id = :tagId and status='Valid'", nativeQuery = true)
	SKUAllocation findByValidTagId(@Param("tagId") String tagId);

	@Query(value = "SELECT * FROM sku_allocation where expiry_date <= :toDate and expiry_date>=:fromDate and status='Valid' and islslm= true order by expiry_date desc", nativeQuery = true)
	List<SKUAllocation> findBySKUAllocations(@Param("fromDate") Date fromDate, @Param("toDate") Date toDate);

	@Query(value = "SELECT * FROM sku_allocation where status='Valid'", nativeQuery = true)
	List<SKUAllocation> findValidAllocations();

	@Query(value = "SELECT * FROM sku_allocation", nativeQuery = true) 
	List<SKUAllocation> findAllAllocations();

}
