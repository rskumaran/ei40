package com.entappia.ei4o.repository;

import java.util.Date;

import javax.transaction.Transactional;

import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.entappia.ei4o.dbmodels.LocationHistory;
 

@Repository
public interface LocationHistoryDataRepository extends CrudRepository<LocationHistory, Integer> {

	LocationHistory findByDate(Date date);
	
	@Modifying
	@Transactional
	@Query(value = "DELETE FROM location_history WHERE date <:date", nativeQuery = true)
	int deleteByDateLessThan(@Param("date") Date date);
}
