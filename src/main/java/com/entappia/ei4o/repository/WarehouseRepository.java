package com.entappia.ei4o.repository;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.persistence.EntityManager;
import javax.transaction.Transactional;

import org.hibernate.query.Query;
import org.hibernate.transform.AliasToEntityMapResultTransformer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.entappia.ei4o.constants.AppConstants;
import com.entappia.ei4o.dbmodels.WarehouseColumnName;
import com.entappia.ei4o.utils.Utils;

@Repository
public class WarehouseRepository {
	 

	@Autowired
	private EntityManager entityManager;

	@Autowired
	private WarehouseColumnNameRepository warehouseColumnNameRepository;
	
	 
	@Transactional
	public void addNewColumn(String columnName) {

		String query = "alter table db_ei40.warehouse add column " + columnName + " varchar(255) default null";
		if (AppConstants.print_log)
			System.out.println(query);
		entityManager.createNativeQuery(query).executeUpdate();
	}

	@SuppressWarnings("unchecked")
	public List<HashMap<String, Object>> readWarehouseData() {

		String queryString = "Select * from db_ei40.warehouse";
		Query<?> query = (Query<?>) entityManager.createNativeQuery(queryString);
		query.setResultTransformer(AliasToEntityMapResultTransformer.INSTANCE);
		List<HashMap<String, Object>> list = (List<HashMap<String, Object>>) query.getResultList();

		return list;
	}
	
	@SuppressWarnings("unchecked")
	public HashMap<String, Object> getWarehouseBySKUNo(String skuNo) {

		String queryString = "Select * from db_ei40.warehouse where sku_no='"+skuNo+"'";
		Query<?> query = (Query<?>) entityManager.createNativeQuery(queryString);
		query.setResultTransformer(AliasToEntityMapResultTransformer.INSTANCE);
		List<HashMap<String, Object>> list = (List<HashMap<String, Object>>) query.getResultList();
		if(list!=null && list.size()>0)
			return list.get(0);
		else
			return null;
	}
	
	@SuppressWarnings("unchecked")
	public List<HashMap<String, Object>> readWarehouseData(String queryString) {
		if (AppConstants.print_log)
			System.out.println(queryString);
		Query<?> query = (Query<?>) entityManager.createNativeQuery(queryString);
		query.setResultTransformer(AliasToEntityMapResultTransformer.INSTANCE);
		List<HashMap<String, Object>> list = (List<HashMap<String, Object>>) query.getResultList();

		return list;
	}
	
	@SuppressWarnings("unchecked")
	public String getWarehouseQuery() {
		 
		List<WarehouseColumnName> listColumnNames = warehouseColumnNameRepository.findAll();

		HashMap<String, String> hashMap = new HashMap<>();
		listColumnNames.forEach(warehouseColumnName -> {
			hashMap.put(warehouseColumnName.getFieldName(), warehouseColumnName.getColumnName());
		});

		String columnString = "";
		List<String> columnNameList = readWarehouseColumnNames();

		for (String columnName : columnNameList) {
			if (Utils.isEmptyString(columnString)) {
				if (hashMap.containsKey(columnName))
					columnString = "wh." + columnName + " as '" + hashMap.get(columnName) + "'";
				else
					columnString = "wh." + columnName + " as '" + columnName + "'";

			} else {
				if (hashMap.containsKey(columnName))
					columnString = columnString + ", wh." + columnName + " as '" + hashMap.get(columnName) + "'";
				else
					columnString = columnString + ", wh." + columnName + " as '" + columnName + "'";
			}
		}

		columnString = columnString + ", zd.name  as 'Zone Name', zd.description as 'Zone Details', t.mac_id as mac_id";
		String query = "Select " + columnString
				+ " from db_ei40.warehouse wh INNER JOIN zone_details zd ON zd.id = wh.zone_id inner join tags t on wh.tag_id=t.tag_id";


		return query;
	}
	
	@SuppressWarnings("unchecked")
	@Transactional
	public boolean updateWarehouse(String queryString) {
 
		Query<?> query = (Query<?>) entityManager.createNativeQuery(queryString);
		int count = query.executeUpdate();
		if (count > 0)
			return true;
		else 
			return false;
	}

	@SuppressWarnings("unchecked")
	public List<String> readWarehouseColumnNames() {

		String queryString = "select Column_name from Information_schema.columns where Table_name like 'warehouse' ORDER BY ordinal_position";

		Query<?> query = (Query<?>) entityManager.createNativeQuery(queryString);
		List<String> list = (List<String>) query.getResultList();

		return list;
	}

	@SuppressWarnings("unchecked")
	@Transactional
	public boolean insertWarehouse(String queryString) {

		Query<?> query = (Query<?>) entityManager.createNativeQuery(queryString);
		int count = query.executeUpdate();
		if (count > 0)
			return true;
		else 
			return false;
	}
}
