package com.entappia.ei4o.repository;

import java.util.List;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.entappia.ei4o.dbmodels.Shift;

@Repository
public interface ShiftRepository extends CrudRepository<Shift, Integer> {

	Shift findByShiftName(String shiftName);
	
	List<Shift> findAll();
}
