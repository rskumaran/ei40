package com.entappia.ei4o.repository;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.entappia.ei4o.dbmodels.Partitions;
import com.entappia.ei4o.dbmodels.Walls; 

@Repository
public interface PartitionsRepository  extends CrudRepository<Partitions, Integer> {
	
	@Query(value = "select * from partitions where outlines_id=:outlinesId", nativeQuery = true)
	List<Partitions> findByOutlinesId(@Param("outlinesId") long outlinesId);

	@Query(value = "select * from partitions where active=true", nativeQuery = true)
	List<Partitions> findAllActivePartitions();
	
	Partitions findAllById(long id); 
	
	@Modifying
	@Transactional
	@Query(value = "truncate partitions", nativeQuery = true)
	void truncateTable();
}
