package com.entappia.ei4o.repository;

import java.util.List;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.entappia.ei4o.dbmodels.AssetNonChrono;

@Repository
public interface AssetNonChronoRepository extends CrudRepository<AssetNonChrono, Integer> {
	AssetNonChrono findByAssetId(String assetId);
	
	List<AssetNonChrono> findAll();
	 

}
