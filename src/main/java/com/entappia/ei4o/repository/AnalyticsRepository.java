package com.entappia.ei4o.repository;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;

import org.hibernate.SQLQuery;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.query.Query;
import org.hibernate.transform.AliasToEntityMapResultTransformer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.entappia.ei4o.dbmodels.WorkStationChartAnalyticsResult;
import com.entappia.ei4o.constants.AppConstants;
import com.entappia.ei4o.dbmodels.AgingAnalyticsResult;
import com.entappia.ei4o.dbmodels.AttendancePeopleResult;
import com.entappia.ei4o.dbmodels.AttendanceZoneResult;
import com.entappia.ei4o.dbmodels.Location;
import com.entappia.ei4o.dbmodels.WorkOrderTimeAnalyticsResult;
import com.entappia.ei4o.dbmodels.WorkStationAnalyticsResult;
import com.entappia.ei4o.dbmodels.WorkStationInventoryResult;
import com.entappia.ei4o.dbmodels.ZoneFootFallAverageResult;
import com.entappia.ei4o.dbmodels.ZoneFootFallResult;
import com.entappia.ei4o.utils.Utils;

@Component
public class AnalyticsRepository {

	
	@Autowired
	private EntityManagerFactory entityManagerFactory;

	public List<?> SelectAllTablesInDatabase()
	{
		List<?> TableNames = null;
		String sqlQueryString = "SELECT table_name FROM INFORMATION_SCHEMA.TABLES where table_schema = 'db_ei40'";
		//System.out.println(sqlQueryString);

		Session session = null;
		try {

			session = entityManagerFactory.unwrap(SessionFactory.class).openSession();

			Transaction transaction = session.beginTransaction();

			Query fetchQuery = session.createNativeQuery(sqlQueryString);

			TableNames = fetchQuery.getResultList();
			transaction.commit();

			if (session!= null)
				session.close();

		}catch (Exception e) {
			return TableNames;
		}finally {
			if (session!= null)
				session.close();
		} 

		return TableNames;

	}
	public List<?> SelectTableNamesLike(String tableName)
	{
		List<?> TableNames = null;
		String sqlQueryString = "SELECT table_name FROM INFORMATION_SCHEMA.TABLES where table_schema = 'db_ei40' and table_name like '" + tableName + "%'";
		//String sqlQueryString = "SELECT table_name FROM INFORMATION_SCHEMA.TABLES where table_name like '" + tableName + "%'";
		//System.out.println(sqlQueryString);

		Session session = null;
		try {


			session = entityManagerFactory.unwrap(SessionFactory.class).openSession();

			Transaction transaction = session.beginTransaction();

			Query fetchQuery = session.createNativeQuery(sqlQueryString);

			TableNames = fetchQuery.getResultList();
			transaction.commit();

			if (session!= null)
				session.close();

		}catch (Exception e) {
			return TableNames;
		}finally {
			if (session!= null)
				session.close();
		} 

		return TableNames;

	}
	public List<Location> SelectAll(String tableName)
	{
		List<Location> locationRecords = null;
		String sqlQueryString = "SELECT * FROM `" + tableName + "`";
		//System.out.println(sqlQueryString);

		Session session = null;
		try {

			session = entityManagerFactory.unwrap(SessionFactory.class).openSession();

			Transaction transaction = session.beginTransaction();

			Query<Location> fetchQuery = session.createNativeQuery(sqlQueryString, Location.class);

			locationRecords = fetchQuery.getResultList();
			transaction.commit();

			if (session!= null)
				session.close();

		}catch (Exception e) {
			e.printStackTrace();
			return locationRecords;
		}finally {
			if (session!= null)
				session.close();
		} 

		return locationRecords;

	}
	public List<Location> SelectByUserId(String tableName, String userId)
	{
		List<Location> locationRecords = null;
		String sqlQueryString = "SELECT * FROM `" + tableName + "` where id = '" + userId + "' ";
		//System.out.println(sqlQueryString);

		Session session = null;
		try {


			session = entityManagerFactory.unwrap(SessionFactory.class).openSession();

			Transaction transaction = session.beginTransaction();

			Query<Location> fetchQuery = session.createNativeQuery(sqlQueryString, Location.class);

			locationRecords = fetchQuery.getResultList();
			transaction.commit();

			if (session!= null)
				session.close();

		}catch (Exception e) {
			return locationRecords;
		}finally {
			if (session!= null)
				session.close();
		} 

		return locationRecords;

	}
	public List<?> SelectHoursByEmployee(String tableName)
	{
		List<?> locationRecords = null;
		String sqlQueryString = "SELECT * FROM `" + tableName + "`";
		//System.out.println(sqlQueryString);

		Session session = null;
		try {


			session = entityManagerFactory.unwrap(SessionFactory.class).openSession();

			Transaction transaction = session.beginTransaction();

			Query fetchQuery = session.createNativeQuery(sqlQueryString);

			locationRecords = fetchQuery.getResultList();
			transaction.commit();

			if (session!= null)
				session.close();

		}catch (Exception e) {
			return locationRecords;
		}finally {
			if (session!= null)
				session.close();
		} 

		return locationRecords;

	}
	public List<Location> SelectAllAssetsByAssetId(String tableName, String assetId, String innertableName,
			String assetType) {

		List<Location> locationRecords = null;
		String sqlQueryString = "";
		if(Utils.isEmptyString(assetType))
			sqlQueryString = "SELECT * FROM `" + tableName + "` where id = '" + assetId + "'";
		else
			sqlQueryString = "SELECT * FROM `" + tableName + "` where id = '" + assetId + "' and id in (select asset_id from " + innertableName + " where type like '"+ assetType + "')";

		//System.out.println(sqlQueryString);

		Session session = null;
		try {


			session = entityManagerFactory.unwrap(SessionFactory.class).openSession();

			Transaction transaction = session.beginTransaction();

			Query<Location> fetchQuery = session.createNativeQuery(sqlQueryString, Location.class);

			locationRecords = fetchQuery.getResultList();
			transaction.commit();

			if (session!= null)
				session.close();

		}catch (Exception e) {
			return locationRecords;
		}finally {
			if (session!= null)
				session.close();
		} 

		return locationRecords;
	}
	public List<Location> SelectAllAssets(String tableName, String innertableName, String assetType) {

		List<Location> locationRecords = null;
		//String sqlQueryString = "SELECT * FROM `" + tableName + "`";

		String sqlQueryString = "";
		if(Utils.isEmptyString(assetType))
			sqlQueryString = "SELECT * FROM `" + tableName + "`";
		else
			sqlQueryString = "SELECT * FROM `" + tableName + "` where id in (select asset_id from " + innertableName + " where type like '"+ assetType + "')";

		//System.out.println(sqlQueryString);

		Session session = null;
		try {

			session = entityManagerFactory.unwrap(SessionFactory.class).openSession();

			Transaction transaction = session.beginTransaction();

			Query<Location> fetchQuery = session.createNativeQuery(sqlQueryString, Location.class);

			locationRecords = fetchQuery.getResultList();
			transaction.commit();

			if (session!= null)
				session.close();

		}catch (Exception e) {
			e.printStackTrace();
			return locationRecords;
		}finally {
			if (session!= null)
				session.close();
		} 

		return locationRecords;
	}

	public List<Location> Select(String tableName, String innertableName, String assetType) {

		List<Location> locationRecords = null;
		//String sqlQueryString = "SELECT * FROM `" + tableName + "`";

		String sqlQueryString = "SELECT * FROM `" + tableName + "` where id in (select asset_id from " + innertableName + " where type like '"+ assetType + "')";

		//System.out.println(sqlQueryString);

		Session session = null;
		try {

			session = entityManagerFactory.unwrap(SessionFactory.class).openSession();

			Transaction transaction = session.beginTransaction();

			Query<Location> fetchQuery = session.createNativeQuery(sqlQueryString, Location.class);

			locationRecords = fetchQuery.getResultList();
			transaction.commit();

			if (session!= null)
				session.close();

		}catch (Exception e) {
			e.printStackTrace();
			return locationRecords;
		}finally {
			if (session!= null)
				session.close();
		} 

		return locationRecords;
	}
	/*select sum(total) as time, id, zone, zonename
	from (
	    select count(zone) as total, id, zone, zone_name as zonename from table_name_1
	    group by zone,id,zone_name

	    union 

	    select count(zone) as total, id, zone, zone_name as zonename from table_name_2
	    group by zone, id, zone_name
	) as aliasT
	group by zone, id, zonename*/


	public List<Location> SelectGroupByWorkOrderZone(List<String> tableNames) {
		List<Location> locationRecords = null;

		if(null != tableNames && tableNames.size() > 0) {

			String sqlQueryString = "select min(time) as time, sum(duration) as duration, max(grid) as grid, grid_Pos, id, zone, zone_Name from ( ";

			for(int j=0;j<tableNames.size();j++){

				//System.out.println("In Between Table Names : " + tableNames.get(j));

				sqlQueryString += " select min(time) as time, sum(duration) as duration, max(time) as grid, grid_Pos as grid_Pos, id, zone, zone_Name as zone_Name from `" +
						tableNames.get(j) + "` group by zone, id, zone_Name, grid, grid_Pos ";

				//If it is the final table dont append union 
				//append alias variable and loop will end
				// append other query variables after the loop ends

				if(j == tableNames.size() - 1) {// If equal append alias name
					sqlQueryString += " ) as aliasT " ;
				}else{
					sqlQueryString += " union " ;
				}
			}



			sqlQueryString += " group by zone, id, zone_Name, grid, grid_Pos";

			//System.out.println("Query : " + sqlQueryString);

			Session session = null;

			try {

				session = entityManagerFactory.unwrap(SessionFactory.class).openSession();

				Transaction transaction = session.beginTransaction();

				Query<Location> fetchQuery = session.createNativeQuery(sqlQueryString, Location.class);

				locationRecords = fetchQuery.getResultList();
				transaction.commit();

				if (session!= null)
					session.close();

			}catch (Exception e) {
				e.printStackTrace();
				return locationRecords;
			}finally {
				if (session!= null)
					session.close();
			}

		}




		return locationRecords;
	}
	@SuppressWarnings("unchecked")
	public List<WorkStationAnalyticsResult> SelectAllWorkstationFlowAnalytics(List<String> tableNames) {
		List<WorkStationAnalyticsResult> WorkStationAnalyticsResultRecords = null;

		if(null != tableNames && tableNames.size() > 0) {

			String sqlQueryString = "select count(id) incoming, 0 as outgoing, 0 as waiting, station_id as stationId, Station_type as stationName from( (select distinct id,zone from  ( ";

			for(int j=0;j<tableNames.size();j++){

				//System.out.println("In Between Table Names : " + tableNames.get(j));

				sqlQueryString += " select * from `" +	tableNames.get(j) + "` ";

				//If it is the final table dont append union 
				//append alias variable and loop will end
				// append other query variables after the loop ends

				if(j == tableNames.size() - 1) {// If equal append alias name
					sqlQueryString += " ) as tabl " ;
				}else{
					sqlQueryString += " union " ;
				}
			}



			sqlQueryString += " group by id,zone) as tbl1  join  (SELECT * FROM work_station where active = 1 ) as tbl2 on  tbl1.zone = tbl2.inbound) group by station_id,tbl1.zone;";

			//System.out.println("Query : " + sqlQueryString);

			Session session = null;

			try {

				session = entityManagerFactory.unwrap(SessionFactory.class).openSession();

				Transaction transaction = session.beginTransaction();

				//Query<WorkStationAnalyticsResult> fetchQuery = session.createNativeQuery(sqlQueryString, WorkStationAnalyticsResult.class);
				//sqlQueryString = "select 1 as id, 'siva' as firstName, 'kumar' as lastName, '77' as version";
				Query<WorkStationAnalyticsResult> fetchQuery = session.createNativeQuery(sqlQueryString, "WorkStationMapping");
				
				

				WorkStationAnalyticsResultRecords = fetchQuery.getResultList();
				transaction.commit();

				if (session!= null)
					session.close();

			}catch (Exception e) {
				e.printStackTrace();
				return WorkStationAnalyticsResultRecords;
			}finally {
				if (session!= null)
					session.close();
			}
	
			return WorkStationAnalyticsResultRecords;
		}
		return null;
	}
	@SuppressWarnings("unchecked")
	public List<WorkStationAnalyticsResult> SelectWorkstationFlowAnalyticsByWorkStationId(List<String> tableNames,
			String workStationId) {
		
		List<WorkStationAnalyticsResult> WorkStationAnalyticsResultRecords = null;
		
		if(null != tableNames && tableNames.size() > 0) {

			String sqlQueryString = "select count(id) incoming, 0 as outgoing, 0 as waiting,station_id as stationId, Station_type as stationName from( (select distinct id,zone from  ( ";

			for(int j=0;j<tableNames.size();j++){

				//System.out.println("In Between Table Names : " + tableNames.get(j));

				sqlQueryString += " select * from `" +	tableNames.get(j) + "` ";

				//If it is the final table dont append union 
				//append alias variable and loop will end
				// append other query variables after the loop ends

				if(j == tableNames.size() - 1) {// If equal append alias name
					sqlQueryString += " ) as tabl " ;
				}else{
					sqlQueryString += " union " ;
				}
			}



			sqlQueryString += " group by id,zone) as tbl1  join  (SELECT * FROM work_station where active = 1 ) as tbl2 on  tbl1.zone = tbl2.inbound) where station_id = '"
					+ workStationId + "' group by station_id,tbl1.zone;";

			//System.out.println("Query : " + sqlQueryString);

			Session session = null;

			try {

				session = entityManagerFactory.unwrap(SessionFactory.class).openSession();

				Transaction transaction = session.beginTransaction();

				//Query<WorkStationAnalyticsResult> fetchQuery = session.createNativeQuery(sqlQueryString, WorkStationAnalyticsResult.class);
				//sqlQueryString = "select 1 as id, 'siva' as firstName, 'kumar' as lastName, '77' as version";
				Query<WorkStationAnalyticsResult> fetchQuery = session.createNativeQuery(sqlQueryString, "WorkStationMapping");
				
				

				WorkStationAnalyticsResultRecords = fetchQuery.getResultList();
				transaction.commit();

				if (session!= null)
					session.close();

			}catch (Exception e) {
				e.printStackTrace();
				return WorkStationAnalyticsResultRecords;
			}finally {
				if (session!= null)
					session.close();
			}
	
			return WorkStationAnalyticsResultRecords;
		}
		return null;
	}
	@SuppressWarnings("unchecked")
	public List<WorkStationAnalyticsResult> SelectAllWorkstationIncomingFlowAnalytics(List<String> tableNames) {
		
		List<WorkStationAnalyticsResult> WorkStationAnalyticsResultRecords = null;
		if(null != tableNames && tableNames.size() > 0) {

			String sqlQueryString = "select count(id) incoming, 0 as outgoing, 0 as waiting, station_id as stationId, Station_type as stationName from( (select distinct id,zone from  ( ";

			for(int j=0;j<tableNames.size();j++){

				//System.out.println("In Between Table Names : " + tableNames.get(j));

				sqlQueryString += " select * from `" +	tableNames.get(j) + "` ";

				//If it is the final table dont append union 
				//append alias variable and loop will end
				// append other query variables after the loop ends

				if(j == tableNames.size() - 1) {// If equal append alias name
					sqlQueryString += " ) as tabl " ;
				}else{
					sqlQueryString += " union " ;
				}
			}



			sqlQueryString += " group by id,zone) as tbl1  join  (SELECT * FROM work_station where active = 1 ) as tbl2 on  tbl1.zone = tbl2.inbound) group by station_id,tbl1.zone;";

			//System.out.println("Query : " + sqlQueryString);

			Session session = null;

			try {

				session = entityManagerFactory.unwrap(SessionFactory.class).openSession();

				Transaction transaction = session.beginTransaction();

				//Query<WorkStationAnalyticsResult> fetchQuery = session.createNativeQuery(sqlQueryString, WorkStationAnalyticsResult.class);
				//sqlQueryString = "select 1 as id, 'siva' as firstName, 'kumar' as lastName, '77' as version";
				Query<WorkStationAnalyticsResult> fetchQuery = session.createNativeQuery(sqlQueryString, "WorkStationMapping");
				
				

				WorkStationAnalyticsResultRecords = fetchQuery.getResultList();
				transaction.commit();

				if (session!= null)
					session.close();

			}catch (Exception e) {
				e.printStackTrace();
				return WorkStationAnalyticsResultRecords;
			}finally {
				if (session!= null)
					session.close();
			}
	
			return WorkStationAnalyticsResultRecords;
		}
		return null;
	}
	@SuppressWarnings("unchecked")
	public List<WorkStationAnalyticsResult> SelectAllWorkstationOutgoingFlowAnalytics(List<String> tableNames) {
		
		List<WorkStationAnalyticsResult> WorkStationAnalyticsResultRecords = null;
		if(null != tableNames && tableNames.size() > 0) {


			String sqlQueryString = "select count(tbl1.id) as outgoing, 0 as incoming, 0 as waiting, station_id as stationId, Station_type as stationName from (select  max(time) lasttimeinoutzone,id,tabl.zone from  ( ";

			for(int j=0;j<tableNames.size();j++){

				//System.out.println("In Between Table Names : " + tableNames.get(j));

				sqlQueryString += " select * from `" +	tableNames.get(j) + "` ";

				//If it is the final table dont append union 
				//append alias variable and loop will end
				// append other query variables after the loop ends

				if(j == tableNames.size() - 1) {// If equal append alias name
					sqlQueryString += " ) as tabl group by id,tabl.zone ) as tbl1 " ;
				}else{
					sqlQueryString += " union " ;
				}
			}



			sqlQueryString += " join  (SELECT station_id, outbound, inbound, work_area, storage, Station_type FROM work_station where active = 1 ) as tbl2 ";
			
			sqlQueryString += "join (select  time as lasttimefound,id,zone from  (";
			
			for(int j=0;j<tableNames.size();j++){

				sqlQueryString += " select * from `" +	tableNames.get(j) + "` ";

				//If it is the final table dont append union 
				//append alias variable and loop will end
				// append other query variables after the loop ends

				if(j == tableNames.size() - 1) {// If equal append alias name
					sqlQueryString += ") as tbl where time = ( select max(time) from (" ;
				}else{
					sqlQueryString += " union " ;
				}
			}
			
			for(int j=0;j<tableNames.size();j++){

				sqlQueryString += " select * from `" +	tableNames.get(j) + "` ";

				//If it is the final table dont append union 
				//append alias variable and loop will end
				// append other query variables after the loop ends

				if(j == tableNames.size() - 1) {// If equal append alias name
					sqlQueryString += ") as tbl1)) as tbl5 " ;
				}else{
					sqlQueryString += " union " ;
				}
			}
			
			sqlQueryString += "on tbl1.zone = tbl2.outbound and tbl5.zone != tbl2.outbound and tbl5.zone != tbl2.work_area and tbl5.zone != tbl2.inbound "
					+ "and tbl5.id = tbl1.id and tbl5.lasttimefound > tbl1.lasttimeinoutzone"
					+ "  group by station_id;";
			

			//System.out.println("Query : " + sqlQueryString);

			Session session = null;

			try {

				session = entityManagerFactory.unwrap(SessionFactory.class).openSession();

				Transaction transaction = session.beginTransaction();

				//Query<WorkStationAnalyticsResult> fetchQuery = session.createNativeQuery(sqlQueryString, WorkStationAnalyticsResult.class);
				//sqlQueryString = "select 1 as id, 'siva' as firstName, 'kumar' as lastName, '77' as version";
				Query<WorkStationAnalyticsResult> fetchQuery = session.createNativeQuery(sqlQueryString, "WorkStationMapping");
				
				

				WorkStationAnalyticsResultRecords = fetchQuery.getResultList();
				transaction.commit();

				if (session!= null)
					session.close();

			}catch (Exception e) {
				e.printStackTrace();
				return WorkStationAnalyticsResultRecords;
			}finally {
				if (session!= null)
					session.close();
			}
	
			return WorkStationAnalyticsResultRecords;
		}
		return null;
	}
	@SuppressWarnings("unchecked")
	public List<WorkStationAnalyticsResult> SelectAllWorkstationWaitingFlowAnalytics(List<String> tableNames) {
		
		List<WorkStationAnalyticsResult> WorkStationAnalyticsResultRecords = null;
		if(null != tableNames && tableNames.size() > 0) {

			String sqlQueryString = "select distinct count(id) as waiting, 0 as incoming, 0 as outgoing, station_id as stationId, Station_type as stationName from( select * from( ";

			for(int j=0;j<tableNames.size();j++){

				//System.out.println("In Between Table Names : " + tableNames.get(j));

				sqlQueryString += " select * from `" +	tableNames.get(j) + "` ";

				//If it is the final table dont append union 
				//append alias variable and loop will end
				// append other query variables after the loop ends

				if(j == tableNames.size() - 1) {// If equal append alias name
					sqlQueryString += " ) as tbl where time = (select max(time) from ( " ;
				}else{
					sqlQueryString += " union " ;
				}
			}
			
			for(int j=0;j<tableNames.size();j++){

				//System.out.println("In Between Table Names : " + tableNames.get(j));

				sqlQueryString += " select * from `" +	tableNames.get(j) + "` ";

				//If it is the final table dont append union 
				//append alias variable and loop will end
				// append other query variables after the loop ends

				if(j == tableNames.size() - 1) {// If equal append alias name
					sqlQueryString += " )as tbl1)) as tbl2 " ;
				}else{
					sqlQueryString += " union " ;
				}
			}



			sqlQueryString += " join  (SELECT station_id, work_area, Station_type FROM work_station where active = 1 ) as tbl3 on  tbl2.zone = tbl3.work_area  group by station_Id;";

			//System.out.println("Query : " + sqlQueryString);

			Session session = null;

			try {

				session = entityManagerFactory.unwrap(SessionFactory.class).openSession();

				Transaction transaction = session.beginTransaction();

				//Query<WorkStationAnalyticsResult> fetchQuery = session.createNativeQuery(sqlQueryString, WorkStationAnalyticsResult.class);
				//sqlQueryString = "select 1 as id, 'siva' as firstName, 'kumar' as lastName, '77' as version";
				Query<WorkStationAnalyticsResult> fetchQuery = session.createNativeQuery(sqlQueryString, "WorkStationMapping");
				
				

				WorkStationAnalyticsResultRecords = fetchQuery.getResultList();
				transaction.commit();

				if (session!= null)
					session.close();

			}catch (Exception e) {
				e.printStackTrace();
				return WorkStationAnalyticsResultRecords;
			}finally {
				if (session!= null)
					session.close();
			}
	
			return WorkStationAnalyticsResultRecords;
		}
		return null;
	}
	
	@SuppressWarnings("unchecked")
	public List<WorkStationAnalyticsResult> SelectWorkstationIncomingFlowAnalyticsByWorkStationId(
			List<String> tableNames, String workStationId) {
		List<WorkStationAnalyticsResult> WorkStationAnalyticsResultRecords = null;
		if(null != tableNames && tableNames.size() > 0) {

			String sqlQueryString = "select count(id) incoming, 0 as outgoing, 0 as waiting, station_id as stationId, Station_type as stationName from( (select distinct id,zone from  ( ";

			for(int j=0;j<tableNames.size();j++){

				//System.out.println("In Between Table Names : " + tableNames.get(j));

				sqlQueryString += " select * from `" +	tableNames.get(j) + "` ";

				//If it is the final table dont append union 
				//append alias variable and loop will end
				// append other query variables after the loop ends

				if(j == tableNames.size() - 1) {// If equal append alias name
					sqlQueryString += " ) as tabl " ;
				}else{
					sqlQueryString += " union " ;
				}
			}



			sqlQueryString += " group by id,zone) as tbl1  join  (SELECT * FROM work_station where active = 1 ) as tbl2 on  tbl1.zone = tbl2.inbound)  where station_id = '"
									+ workStationId + "' group by station_id,tbl1.zone;";

			//System.out.println("Query : " + sqlQueryString);

			Session session = null;

			try {

				session = entityManagerFactory.unwrap(SessionFactory.class).openSession();

				Transaction transaction = session.beginTransaction();

				//Query<WorkStationAnalyticsResult> fetchQuery = session.createNativeQuery(sqlQueryString, WorkStationAnalyticsResult.class);
				//sqlQueryString = "select 1 as id, 'siva' as firstName, 'kumar' as lastName, '77' as version";
				Query<WorkStationAnalyticsResult> fetchQuery = session.createNativeQuery(sqlQueryString, "WorkStationMapping");
				
				

				WorkStationAnalyticsResultRecords = fetchQuery.getResultList();
				transaction.commit();

				if (session!= null)
					session.close();

			}catch (Exception e) {
				e.printStackTrace();
				return WorkStationAnalyticsResultRecords;
			}finally {
				if (session!= null)
					session.close();
			}
	
			return WorkStationAnalyticsResultRecords;
		}
		return null;
	}
	@SuppressWarnings("unchecked")
	public List<WorkStationAnalyticsResult> SelectWorkstationOutgoingFlowAnalyticsByWorkStationId(
			List<String> tableNames, String workStationId) {
		
		List<WorkStationAnalyticsResult> WorkStationAnalyticsResultRecords = null;
		if(null != tableNames && tableNames.size() > 0) {


			String sqlQueryString = "select distinct count(tbl1.id) as outgoing, 0 as incoming, 0 as waiting, station_id as stationId, Station_type as stationName from (select  max(time) lasttimeinoutzone,id,tabl.zone from  ( ";

			for(int j=0;j<tableNames.size();j++){

				//System.out.println("In Between Table Names : " + tableNames.get(j));

				sqlQueryString += " select * from `" +	tableNames.get(j) + "` ";

				//If it is the final table dont append union 
				//append alias variable and loop will end
				// append other query variables after the loop ends

				if(j == tableNames.size() - 1) {// If equal append alias name
					sqlQueryString += " ) as tabl group by id,tabl.zone ) as tbl1 " ;
				}else{
					sqlQueryString += " union " ;
				}
			}



			sqlQueryString += " join  (SELECT station_id, outbound, inbound, work_area, storage, Station_type FROM work_station where active = 1 ) as tbl2 ";
			
			sqlQueryString += "join (select  time as lasttimefound,id,zone from  (";
			
			for(int j=0;j<tableNames.size();j++){

				sqlQueryString += " select * from `" +	tableNames.get(j) + "` ";

				//If it is the final table dont append union 
				//append alias variable and loop will end
				// append other query variables after the loop ends

				if(j == tableNames.size() - 1) {// If equal append alias name
					sqlQueryString += ") as tbl where time = ( select max(time) from (" ;
				}else{
					sqlQueryString += " union " ;
				}
			}
			
			for(int j=0;j<tableNames.size();j++){

				sqlQueryString += " select * from `" +	tableNames.get(j) + "` ";

				//If it is the final table dont append union 
				//append alias variable and loop will end
				// append other query variables after the loop ends

				if(j == tableNames.size() - 1) {// If equal append alias name
					sqlQueryString += ") as tbl1)) as tbl5 " ;
				}else{
					sqlQueryString += " union " ;
				}
			}
			
			sqlQueryString += "on tbl1.zone = tbl2.outbound and tbl5.zone != tbl2.outbound and tbl5.zone != tbl2.work_area and tbl5.zone != tbl2.inbound "
					+ "and tbl5.id = tbl1.id and tbl5.lasttimefound > tbl1.lasttimeinoutzone where station_id = '"
					+ workStationId +  "'  group by station_id;";
			

			//System.out.println("Query : " + sqlQueryString);

			Session session = null;

			try {

				session = entityManagerFactory.unwrap(SessionFactory.class).openSession();

				Transaction transaction = session.beginTransaction();

				//Query<WorkStationAnalyticsResult> fetchQuery = session.createNativeQuery(sqlQueryString, WorkStationAnalyticsResult.class);
				//sqlQueryString = "select 1 as id, 'siva' as firstName, 'kumar' as lastName, '77' as version";
				Query<WorkStationAnalyticsResult> fetchQuery = session.createNativeQuery(sqlQueryString, "WorkStationMapping");
				
				

				WorkStationAnalyticsResultRecords = fetchQuery.getResultList();
				transaction.commit();

				if (session!= null)
					session.close();

			}catch (Exception e) {
				e.printStackTrace();
				return WorkStationAnalyticsResultRecords;
			}finally {
				if (session!= null)
					session.close();
			}
	
			return WorkStationAnalyticsResultRecords;
		}
		return null;
	}
	@SuppressWarnings("unchecked")
	public List<WorkStationAnalyticsResult> SelectWorkstationWaitingFlowAnalyticsByWorkStationId(
			List<String> tableNames, String workStationId) {
		
		List<WorkStationAnalyticsResult> WorkStationAnalyticsResultRecords = null;
		if(null != tableNames && tableNames.size() > 0) {

			String sqlQueryString = "select distinct count(id) as waiting, 0 as incoming, 0 as outgoing, station_id as stationId, Station_type as stationName from( select * from( ";

			for(int j=0;j<tableNames.size();j++){

				//System.out.println("In Between Table Names : " + tableNames.get(j));

				sqlQueryString += " select * from `" +	tableNames.get(j) + "` ";

				//If it is the final table dont append union 
				//append alias variable and loop will end
				// append other query variables after the loop ends

				if(j == tableNames.size() - 1) {// If equal append alias name
					sqlQueryString += " ) as tbl where time = (select max(time) from ( " ;
				}else{
					sqlQueryString += " union " ;
				}
			}
			
			for(int j=0;j<tableNames.size();j++){

				//System.out.println("In Between Table Names : " + tableNames.get(j));

				sqlQueryString += " select * from `" +	tableNames.get(j) + "` ";

				//If it is the final table dont append union 
				//append alias variable and loop will end
				// append other query variables after the loop ends

				if(j == tableNames.size() - 1) {// If equal append alias name
					sqlQueryString += " )as tbl1)) as tbl2 " ;
				}else{
					sqlQueryString += " union " ;
				}
			}



			sqlQueryString += " join  (SELECT station_id, work_area, Station_type FROM work_station where active = 1 ) as tbl3 on  tbl2.zone = tbl3.work_area  where station_id = '"
					+ workStationId + "' group by station_id;";

			//System.out.println("Query : " + sqlQueryString);

			Session session = null;

			try {

				session = entityManagerFactory.unwrap(SessionFactory.class).openSession();

				Transaction transaction = session.beginTransaction();

				//Query<WorkStationAnalyticsResult> fetchQuery = session.createNativeQuery(sqlQueryString, WorkStationAnalyticsResult.class);
				//sqlQueryString = "select 1 as id, 'siva' as firstName, 'kumar' as lastName, '77' as version";
				Query<WorkStationAnalyticsResult> fetchQuery = session.createNativeQuery(sqlQueryString, "WorkStationMapping");
				
				

				WorkStationAnalyticsResultRecords = fetchQuery.getResultList();
				transaction.commit();

				if (session!= null)
					session.close();

			}catch (Exception e) {
				e.printStackTrace();
				return WorkStationAnalyticsResultRecords;
			}finally {
				if (session!= null)
					session.close();
			}
	
			return WorkStationAnalyticsResultRecords;
		}
		return null;
	}
	public Date getTableCreationDate(String tablename) {
		
		Date oldTableDate = new Date();
		String sqlQueryString = "SELECT min(Create_time) as lastRecordDate FROM INFORMATION_SCHEMA.TABLES where table_schema = 'db_ei40' and table_name like '" + tablename + "%'";
		//System.out.println(sqlQueryString);

		Session session = null;
		try {

			session = entityManagerFactory.unwrap(SessionFactory.class).openSession();

			Transaction transaction = session.beginTransaction();

			Query fetchQuery = session.createNativeQuery(sqlQueryString);
			
			Object TableDate = fetchQuery.getSingleResult();
			
			//maxLogId = ((short)LogId);
			
			if (TableDate instanceof Date) {
				oldTableDate =  (Date) TableDate;
			}
			
			transaction.commit();

			if (session!= null)
				session.close();

		}catch (Exception e) {
			return oldTableDate;
		}finally {
			if (session!= null)
				session.close();
		} 

		return oldTableDate;
	}
	@SuppressWarnings("unchecked")
	public List<WorkStationInventoryResult> SelectWorkstationInventoryAnalytics(List<String> tableNames,
			String workStationId) {
		
		List<WorkStationInventoryResult> workStationInventoryResultRecords = null;
		if(null != tableNames && tableNames.size() > 0) {

			String sqlQueryString = "select distinct sum(price) as waitingAmount, Station_id as stationId, Station_type as stationName from( select * from( ";

			for(int j=0;j<tableNames.size();j++){

				//System.out.println("In Between Table Names : " + tableNames.get(j));

				sqlQueryString += " select * from `" +	tableNames.get(j) + "` ";

				//If it is the final table dont append union 
				//append alias variable and loop will end
				// append other query variables after the loop ends

				if(j == tableNames.size() - 1) {// If equal append alias name
					sqlQueryString += " ) as tbl where time = (select max(time) from ( " ;
				}else{
					sqlQueryString += " union " ;
				}
			}
			
			for(int j=0;j<tableNames.size();j++){

				//System.out.println("In Between Table Names : " + tableNames.get(j));

				sqlQueryString += " select * from `" +	tableNames.get(j) + "` ";

				//If it is the final table dont append union 
				//append alias variable and loop will end
				// append other query variables after the loop ends

				if(j == tableNames.size() - 1) {// If equal append alias name
					sqlQueryString += " )as tbl1)) as tbl2 " ;
				}else{
					sqlQueryString += " union " ;
				}
			}



			sqlQueryString += " join  (SELECT station_id, work_area, Station_type FROM work_station where active = 1 ) as tbl3 "
					+ " join (SELECT ordere_no, price FROM material where active = 1) as tbl4 on  tbl2.zone = tbl3.work_area and tbl2.id = tbl4.ordere_no where station_id = '"
					+ workStationId + "' group by station_id;";

			//System.out.println("Query : " + sqlQueryString);

			Session session = null;

			try {

				session = entityManagerFactory.unwrap(SessionFactory.class).openSession();

				Transaction transaction = session.beginTransaction();

				//Query<WorkStationAnalyticsResult> fetchQuery = session.createNativeQuery(sqlQueryString, WorkStationAnalyticsResult.class);
				//sqlQueryString = "select 1 as id, 'siva' as firstName, 'kumar' as lastName, '77' as version";
				Query<WorkStationInventoryResult> fetchQuery = session.createNativeQuery(sqlQueryString, "WorkStationInventoryMapping");
				
				

				workStationInventoryResultRecords = fetchQuery.getResultList();
				transaction.commit();

				if (session!= null)
					session.close();

			}catch (Exception e) {
				e.printStackTrace();
				return workStationInventoryResultRecords;
			}finally {
				if (session!= null)
					session.close();
			}
	
			return workStationInventoryResultRecords;
		}
		return null;
	}

	@SuppressWarnings("unchecked")
	public List<WorkOrderTimeAnalyticsResult> SelectAllWorkstationTimeFlowAnalytics() {

		List<WorkOrderTimeAnalyticsResult> WorkStationTimeAnalyticsResultRecords = null;
		/*String sqlQueryString = "SELECT order_id as orderId, zone_analysis.station_id as stationId, station_type as stationName, "
				+ " work_area_start_time as startTime, work_area_end_time as endTime, "
				+ " CEIL(TIMESTAMPDIFF(SECOND,work_area_start_time,work_area_end_time)/60) as diffinMinutes "
				+ " FROM work_order_zone_analysis zone_analysis join work_station work_station on zone_analysis.station_id = work_station.station_id; ";*/


		String sqlQueryString = "SELECT order_id as orderId, zone_analysis.station_id as stationId, station_type as stationName,"
				+ "	work_area_start_time as startTime, work_area_end_time as endTime,"
				+ "	CEIL(TIMESTAMPDIFF(SECOND,work_area_start_time,work_area_end_time)/60) as diffinMinutes,"
				+ "	tbl2.workOrderStartTime, tbl2.workOrderEndTime, tbl2.totalTime"
				+ "	FROM work_order_zone_analysis zone_analysis "
				+ "	join "
				+ "	work_station work_station"
				+ " join"
				+ " (SELECT order_id as orderId,min(work_area_start_time) as workOrderStartTime, max(work_area_end_time) as workOrderEndTime,"
				+ "	sum(CEIL(TIMESTAMPDIFF(SECOND,work_area_start_time,work_area_end_time)/60)) as totalTime"
				+ " FROM work_order_zone_analysis zone_analysis group by order_id) as tbl2"
				+ " on"
				+ " zone_analysis.station_id = work_station.station_id "
				+ "	and "
				+ "	tbl2.orderId = zone_analysis.order_id;";
					
		
		Session session = null;

		try {

			session = entityManagerFactory.unwrap(SessionFactory.class).openSession();

			Transaction transaction = session.beginTransaction();

			Query<WorkOrderTimeAnalyticsResult> fetchQuery = session.createNativeQuery(sqlQueryString, "WorkOrderTimeMapping");

			WorkStationTimeAnalyticsResultRecords = fetchQuery.getResultList();
			
			transaction.commit();

			if (session!= null)
				session.close();

		}catch (Exception e) {
			e.printStackTrace();
			return WorkStationTimeAnalyticsResultRecords;
		}finally {
			if (session!= null)
				session.close();
		}

		return WorkStationTimeAnalyticsResultRecords;
	}
	@SuppressWarnings("unchecked")
	public List<WorkOrderTimeAnalyticsResult> SelectAllWorkstationTimeFlowAnalytics(Date startDate, Date endDate) {

		List<WorkOrderTimeAnalyticsResult> WorkStationTimeAnalyticsResultRecords = null;
		/*String sqlQueryString = "SELECT order_id as orderId, zone_analysis.station_id as stationId, station_type as stationName, "
				+ " work_area_start_time as startTime, work_area_end_time as endTime, "
				+ " CEIL(TIMESTAMPDIFF(SECOND,work_area_start_time,work_area_end_time)/60) as diffinMinutes "
				+ " FROM work_order_zone_analysis zone_analysis join work_station work_station on zone_analysis.station_id = work_station.station_id; ";*/
		
		SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
		

		String sqlQueryString = "SELECT order_id as orderId, zone_analysis.station_id as stationId, station_type as stationName,"
				+ "	work_area_start_time as startTime, work_area_end_time as endTime,"
				+ "	CEIL(TIMESTAMPDIFF(SECOND,work_area_start_time,work_area_end_time)/60) as diffinMinutes,"
				+ "	tbl2.workOrderStartTime, tbl2.workOrderEndTime, tbl2.totalTime"
				+ "	FROM work_order_zone_analysis zone_analysis "
				+ "	join "
				+ "	work_station work_station"
				+ " join"
				+ " (SELECT order_id as orderId,min(work_area_start_time) as workOrderStartTime, max(work_area_end_time) as workOrderEndTime,"
				+ "	sum(CEIL(TIMESTAMPDIFF(SECOND,work_area_start_time,work_area_end_time)/60)) as totalTime"
				+ " FROM work_order_zone_analysis zone_analysis "
				+ " where work_area_start_time >= '" + formatter.format(startDate) + "'"
				+ " and work_area_end_time < '" + formatter.format(endDate) + "'" 
				+ " group by order_id) as tbl2"
				+ " on"
				+ " zone_analysis.station_id = work_station.station_id "
				+ "	and "
				+ "	tbl2.orderId = zone_analysis.order_id;";
					
		
		Session session = null;

		try {

			session = entityManagerFactory.unwrap(SessionFactory.class).openSession();

			Transaction transaction = session.beginTransaction();

			Query<WorkOrderTimeAnalyticsResult> fetchQuery = session.createNativeQuery(sqlQueryString, "WorkOrderTimeMapping");

			WorkStationTimeAnalyticsResultRecords = fetchQuery.getResultList();
			
			transaction.commit();

			if (session!= null)
				session.close();

		}catch (Exception e) {
			e.printStackTrace();
			return WorkStationTimeAnalyticsResultRecords;
		}finally {
			if (session!= null)
				session.close();
		}

		return WorkStationTimeAnalyticsResultRecords;
	}
	
	@SuppressWarnings("unchecked")
	public List<WorkStationInventoryResult> SelectAllWorkstationInventoryAnalytics(List<String> tableNames) {
		
		List<WorkStationInventoryResult> workStationInventoryResultRecords = null;
		if(null != tableNames && tableNames.size() > 0) {

			String sqlQueryString = "select distinct sum(price) as waitingAmount, Station_id  as stationId, Station_type as stationName from( select * from( ";

			for(int j=0;j<tableNames.size();j++){

				//System.out.println("In Between Table Names : " + tableNames.get(j));

				sqlQueryString += " select * from `" +	tableNames.get(j) + "` ";

				//If it is the final table dont append union 
				//append alias variable and loop will end
				// append other query variables after the loop ends

				if(j == tableNames.size() - 1) {// If equal append alias name
					sqlQueryString += " ) as tbl where time = (select max(time) from ( " ;
				}else{
					sqlQueryString += " union " ;
				}
			}
			
			for(int j=0;j<tableNames.size();j++){

				//System.out.println("In Between Table Names : " + tableNames.get(j));

				sqlQueryString += " select * from `" +	tableNames.get(j) + "` ";

				//If it is the final table dont append union 
				//append alias variable and loop will end
				// append other query variables after the loop ends

				if(j == tableNames.size() - 1) {// If equal append alias name
					sqlQueryString += " )as tbl1)) as tbl2 " ;
				}else{
					sqlQueryString += " union " ;
				}
			}



			sqlQueryString += " join  (SELECT station_id, work_area, Station_type FROM work_station where active = 1 ) as tbl3 "
					+ " join (SELECT ordere_no, price FROM material where active = 1) as tbl4 on  tbl2.zone = tbl3.work_area and tbl2.id = tbl4.ordere_no group by station_Id;";

			//System.out.println("Query : " + sqlQueryString);

			Session session = null;

			try {

				session = entityManagerFactory.unwrap(SessionFactory.class).openSession();

				Transaction transaction = session.beginTransaction();

				//Query<WorkStationAnalyticsResult> fetchQuery = session.createNativeQuery(sqlQueryString, WorkStationAnalyticsResult.class);
				//sqlQueryString = "select 1 as id, 'siva' as firstName, 'kumar' as lastName, '77' as version";
				Query<WorkStationInventoryResult> fetchQuery = session.createNativeQuery(sqlQueryString, "WorkStationInventoryMapping");
				
				

				workStationInventoryResultRecords = fetchQuery.getResultList();
				transaction.commit();

				if (session!= null)
					session.close();

			}catch (Exception e) {
				e.printStackTrace();
				return workStationInventoryResultRecords;
			}finally {
				if (session!= null)
					session.close();
			}
	
			return workStationInventoryResultRecords;
		}
		return null;
	}
	
	@SuppressWarnings("unchecked")
	public List<ZoneFootFallResult> SelectAllZoneFootFallAnalytics(List<String> tableNames1 , List<String> tableNames2) {

		List<ZoneFootFallResult> zoneFootFallResultRecords = null;
		if((tableNames1 != null && tableNames1.size() > 0 ) || (tableNames2 != null && tableNames2.size() > 0 )) {

			String sqlQueryString = "select zone as zoneId, zone_name as zoneName, sum(NoOfPeople) as NoOfPersons, tq as tq from( ";

			if(null != tableNames1 && tableNames1.size() > 0) {

				sqlQueryString += " select zone, zone_name, count(tq) as NoOfPeople, tq as tq from ( "
						+ " select id, zone, zone_name, tq from( "
						+ " select a.* FROM (select *, ROW_NUMBER() OVER(PARTITION BY id) NUM from ( "
						+" select id,zone, zone_name, CONCAT('TQ',LPAD(Floor(time/15)+1,2,0)) as tq, time from ( "
						;
				
				for(int j=0;j<tableNames1.size();j++){

					sqlQueryString += " select * from `" +	tableNames1.get(j) + "` ";

					//If it is the final table dont append union 
					//append alias variable and loop will end
					// append other query variables after the loop ends

					if(j == tableNames1.size() - 1) {// If equal append alias name
						sqlQueryString += " ) as tbl1 "
								+ " ) as tbl2 order by id, time) as a "
								+ " where a.zone <> ( select b.zone from ( "
								+ " select *, ROW_NUMBER() OVER(PARTITION BY id) NUM from ( "
								+ " select id,zone, zone_name,CONCAT('TQ',LPAD(Floor(time/15)+1,2,0)) as tq, time from ( ";
					}else{
						
							sqlQueryString += " union " ;
					}
				}
				for(int j=0;j<tableNames1.size();j++){

					sqlQueryString += " select * from `" +	tableNames1.get(j) + "` ";

					//If it is the final table dont append union 
					//append alias variable and loop will end
					// append other query variables after the loop ends

					if(j == tableNames1.size() - 1) {// If equal append alias name
						sqlQueryString += " ) as tbl1 "
								+ " ) as tbl2 order by id, time) as b "
								+ " WHERE a.id = b.id "
								+ " AND a.Time > b.Time "
								+ " ORDER BY b.Time DESC "
								+ " LIMIT 1 "
								+ " ) or "
								+ " a.NUM  = 1 ";
								
					}else{
						
							sqlQueryString += " union " ;
					}
				}
				
				sqlQueryString += " ) as tbl1 "//here check condition
						+ " group by id,zone,zone_name,tq) as tbl2 "
						+ " group by tq,zone,zone_name "
						;
				
				if(null != tableNames2 && tableNames2.size() > 0) {
					sqlQueryString += " union " ;
				}
			}
			if(null != tableNames2 && tableNames2.size() > 0) {

				sqlQueryString += " select zone, zone_name, count(tq) as NoOfPeople, tq as tq from ( "
						+ " select id, zone, zone_name, tq from( "
						+ " select a.* FROM (select *, ROW_NUMBER() OVER(PARTITION BY id) NUM from ( "
						+" select id,zone, zone_name, CONCAT('TQ',LPAD(Floor(time/15)+1,2,0)) as tq, time from ( "
						;
				
				
				for(int j=0;j<tableNames2.size();j++){

					sqlQueryString += " select * from `" +	tableNames2.get(j) + "` ";

					//If it is the final table dont append union 
					//append alias variable and loop will end
					// append other query variables after the loop ends

					if(j == tableNames2.size() - 1) {// If equal append alias name
						sqlQueryString += " ) as tbl1 "
								+ " ) as tbl2 order by id, time) as a "
								+ " where a.zone <> ( select b.zone from ( "
								+ " select *, ROW_NUMBER() OVER(PARTITION BY id) NUM from ( "
								+ " select id,zone, zone_name,CONCAT('TQ',LPAD(Floor(time/15)+1,2,0)) as tq, time from ( ";
					}else{
						
							sqlQueryString += " union " ;
					}
				}
				for(int j=0;j<tableNames2.size();j++){

					sqlQueryString += " select * from `" +	tableNames2.get(j) + "` ";

					//If it is the final table dont append union 
					//append alias variable and loop will end
					// append other query variables after the loop ends

					if(j == tableNames2.size() - 1) {// If equal append alias name
						sqlQueryString += " ) as tbl1 "
								+ " ) as tbl2 order by id, time) as b "
								+ " WHERE a.id = b.id "
								+ " AND a.Time > b.Time "
								+ " ORDER BY b.Time DESC "
								+ " LIMIT 1 "
								+ " ) or "
								+ " a.NUM  = 1 ";
								
					}else{
						
							sqlQueryString += " union " ;
					}
				}
			}

				



			sqlQueryString += " ) as tbl1 "//here check condition
					+ " group by id,zone,zone_name,tq) as tbl2 "
					+ " group by tq,zone,zone_name ) as tbl3 "
					+ " group by tq,zone,zone_name ;";
			if (AppConstants.print_log)
				System.out.println("Query : " + sqlQueryString);

				Session session = null;

				try {

					session = entityManagerFactory.unwrap(SessionFactory.class).openSession();

					Transaction transaction = session.beginTransaction();

					//Query<WorkStationAnalyticsResult> fetchQuery = session.createNativeQuery(sqlQueryString, WorkStationAnalyticsResult.class);
					//sqlQueryString = "select 1 as id, 'siva' as firstName, 'kumar' as lastName, '77' as version";
					Query<ZoneFootFallResult> fetchQuery = session.createNativeQuery(sqlQueryString, "ZoneFootFallMapping");



					zoneFootFallResultRecords = fetchQuery.getResultList();
					transaction.commit();

					if (session!= null)
						session.close();

				}catch (Exception e) {
					e.printStackTrace();
					return zoneFootFallResultRecords;
				}finally {
					if (session!= null)
						session.close();
				}

				return zoneFootFallResultRecords;
			}
		return null;
	}
	
	@SuppressWarnings("unchecked")
	public List<ZoneFootFallResult> SelectZoneFootFallAnalytics(List<String> tableNames1, List<String> tableNames2,
			String zoneId) {

		List<ZoneFootFallResult> zoneFootFallResultRecords = null;
		if((tableNames1 != null && tableNames1.size() > 0 ) || (tableNames2 != null && tableNames2.size() > 0 )) {

			String sqlQueryString = "select zone as zoneId, zone_name as zoneName, sum(NoOfPeople) as NoOfPersons, tq as tq from( ";

			if(null != tableNames1 && tableNames1.size() > 0) {

				sqlQueryString += " select zone, zone_name, count(tq) as NoOfPeople, tq as tq from ( "
						+ " select id, zone, zone_name, tq from( "
						+ " select a.* FROM (select *, ROW_NUMBER() OVER(PARTITION BY id) NUM from ( "
						+" select id,zone, zone_name, CONCAT('TQ',LPAD(Floor(time/15)+1,2,0)) as tq, time from ( "
						;
				
				for(int j=0;j<tableNames1.size();j++){

					sqlQueryString += " select * from `" +	tableNames1.get(j) + "` ";

					//If it is the final table dont append union 
					//append alias variable and loop will end
					// append other query variables after the loop ends

					if(j == tableNames1.size() - 1) {// If equal append alias name
						sqlQueryString += " ) as tbl1 "
								+ " ) as tbl2 order by id, time) as a "
								+ " where a.zone <> ( select b.zone from ( "
								+ " select *, ROW_NUMBER() OVER(PARTITION BY id) NUM from ( "
								+ " select id,zone, zone_name,CONCAT('TQ',LPAD(Floor(time/15)+1,2,0)) as tq, time from ( ";
					}else{
						
							sqlQueryString += " union " ;
					}
				}
				for(int j=0;j<tableNames1.size();j++){

					sqlQueryString += " select * from `" +	tableNames1.get(j) + "` ";

					//If it is the final table dont append union 
					//append alias variable and loop will end
					// append other query variables after the loop ends

					if(j == tableNames1.size() - 1) {// If equal append alias name
						sqlQueryString += " ) as tbl1 "
								+ " ) as tbl2 order by id, time) as b "
								+ " WHERE a.id = b.id "
								+ " AND a.Time > b.Time "
								+ " ORDER BY b.Time DESC "
								+ " LIMIT 1 "
								+ " ) or "
								+ " a.NUM  = 1 ";
								
					}else{
						
							sqlQueryString += " union " ;
					}
				}
				
				sqlQueryString += " ) as tbl1 where zone = '" + zoneId + "'"//here check condition
						+ " group by id,zone,zone_name,tq) as tbl2 "
						+ " group by tq,zone,zone_name "
						;
				
				if(null != tableNames2 && tableNames2.size() > 0) {
					sqlQueryString += " union " ;
				}
			}
			if(null != tableNames2 && tableNames2.size() > 0) {

				sqlQueryString += " select zone, zone_name, count(tq) as NoOfPeople, tq as tq from ( "
						+ " select id, zone, zone_name, tq from( "
						+ " select a.* FROM (select *, ROW_NUMBER() OVER(PARTITION BY id) NUM from ( "
						+" select id,zone, zone_name, CONCAT('TQ',LPAD(Floor(time/15)+1,2,0)) as tq, time from ( "
						;
				
				
				for(int j=0;j<tableNames2.size();j++){

					sqlQueryString += " select * from `" +	tableNames2.get(j) + "` ";

					//If it is the final table dont append union 
					//append alias variable and loop will end
					// append other query variables after the loop ends

					if(j == tableNames2.size() - 1) {// If equal append alias name
						sqlQueryString += " ) as tbl1 "
								+ " ) as tbl2 order by id, time) as a "
								+ " where a.zone <> ( select b.zone from ( "
								+ " select *, ROW_NUMBER() OVER(PARTITION BY id) NUM from ( "
								+ " select id,zone, zone_name,CONCAT('TQ',LPAD(Floor(time/15)+1,2,0)) as tq, time from ( ";
					}else{
						
							sqlQueryString += " union " ;
					}
				}
				for(int j=0;j<tableNames2.size();j++){

					sqlQueryString += " select * from `" +	tableNames2.get(j) + "` ";

					//If it is the final table dont append union 
					//append alias variable and loop will end
					// append other query variables after the loop ends

					if(j == tableNames2.size() - 1) {// If equal append alias name
						sqlQueryString += " ) as tbl1 "
								+ " ) as tbl2 order by id, time) as b "
								+ " WHERE a.id = b.id "
								+ " AND a.Time > b.Time "
								+ " ORDER BY b.Time DESC "
								+ " LIMIT 1 "
								+ " ) or "
								+ " a.NUM  = 1 ";
								
					}else{
						
							sqlQueryString += " union " ;
					}
				}
			}

		


			sqlQueryString += " ) as tbl1  where zone = '" + zoneId + "'"//here check condition
					+ " group by id,zone,zone_name,tq) as tbl2 "
					+ " group by tq,zone,zone_name ) as tbl3 "
					+ " group by tq,zone,zone_name ;";
			if (AppConstants.print_log)
				System.out.println("Query : " + sqlQueryString);

				Session session = null;

				try {

					session = entityManagerFactory.unwrap(SessionFactory.class).openSession();

					Transaction transaction = session.beginTransaction();

					//Query<WorkStationAnalyticsResult> fetchQuery = session.createNativeQuery(sqlQueryString, WorkStationAnalyticsResult.class);
					//sqlQueryString = "select 1 as id, 'siva' as firstName, 'kumar' as lastName, '77' as version";
					Query<ZoneFootFallResult> fetchQuery = session.createNativeQuery(sqlQueryString, "ZoneFootFallMapping");



					zoneFootFallResultRecords = fetchQuery.getResultList();
					transaction.commit();

					if (session!= null)
						session.close();

				}catch (Exception e) {
					e.printStackTrace();
					return zoneFootFallResultRecords;
				}finally {
					if (session!= null)
						session.close();
				}

				return zoneFootFallResultRecords;
			}
		return null;
	}
	@SuppressWarnings("unchecked")
	public List<ZoneFootFallAverageResult> SelectAllZoneAverageFootFallAnalytics(List<String> tableNames1 , List<String> tableNames2) {

		List<ZoneFootFallAverageResult> zoneAverageFootFallResultRecords = null;
		if((tableNames1 != null && tableNames1.size() > 0 ) || (tableNames2 != null && tableNames2.size() > 0 )) {

			String sqlQueryString = "select zoneId, zoneName, sum(NoOfPersons) as totalpersons, max(NoOfPersons) as maximum, avg(NoOfPersons) as average  from (" 
					+ "select zone as zoneId, zone_name as zoneName, sum(NoOfPeople) as NoOfPersons, tq as tq from( ";

			if(null != tableNames1 && tableNames1.size() > 0) {

				sqlQueryString += " select zone, zone_name, count(tq) as NoOfPeople, tq as tq from ( "
						+ " select id, zone, zone_name, tq from( "
						+ " select a.* FROM (select *, ROW_NUMBER() OVER(PARTITION BY id) NUM from ( "
						+" select id,zone, zone_name, CONCAT('TQ',LPAD(Floor(time/15)+1,2,0)) as tq, time from ( "
						;
				
				for(int j=0;j<tableNames1.size();j++){

					sqlQueryString += " select * from `" +	tableNames1.get(j) + "` ";

					//If it is the final table dont append union 
					//append alias variable and loop will end
					// append other query variables after the loop ends

					if(j == tableNames1.size() - 1) {// If equal append alias name
						sqlQueryString += " ) as tbl1 "
								+ " ) as tbl2 order by id, time) as a "
								+ " where a.zone <> ( select b.zone from ( "
								+ " select *, ROW_NUMBER() OVER(PARTITION BY id) NUM from ( "
								+ " select id,zone, zone_name,CONCAT('TQ',LPAD(Floor(time/15)+1,2,0)) as tq, time from ( ";
					}else{
						
							sqlQueryString += " union " ;
					}
				}
				for(int j=0;j<tableNames1.size();j++){

					sqlQueryString += " select * from `" +	tableNames1.get(j) + "` ";

					//If it is the final table dont append union 
					//append alias variable and loop will end
					// append other query variables after the loop ends

					if(j == tableNames1.size() - 1) {// If equal append alias name
						sqlQueryString += " ) as tbl1 "
								+ " ) as tbl2 order by id, time) as b "
								+ " WHERE a.id = b.id "
								+ " AND a.Time > b.Time "
								+ " ORDER BY b.Time DESC "
								+ " LIMIT 1 "
								+ " ) or "
								+ " a.NUM  = 1 ";
								
					}else{
						
							sqlQueryString += " union " ;
					}
				}
				
				sqlQueryString += " ) as tbl1 "//here check condition
						+ " group by id,zone,zone_name,tq) as tbl2 "
						+ " group by tq,zone,zone_name "
						;
				
				if(null != tableNames2 && tableNames2.size() > 0) {
					sqlQueryString += " union " ;
				}
			}
			if(null != tableNames2 && tableNames2.size() > 0) {

				sqlQueryString += " select zone, zone_name, count(tq) as NoOfPeople, tq as tq from ( "
						+ " select id, zone, zone_name, tq from( "
						+ " select a.* FROM (select *, ROW_NUMBER() OVER(PARTITION BY id) NUM from ( "
						+" select id,zone, zone_name, CONCAT('TQ',LPAD(Floor(time/15)+1,2,0)) as tq, time from ( "
						;
				
				
				for(int j=0;j<tableNames2.size();j++){

					sqlQueryString += " select * from `" +	tableNames2.get(j) + "` ";

					//If it is the final table dont append union 
					//append alias variable and loop will end
					// append other query variables after the loop ends

					if(j == tableNames2.size() - 1) {// If equal append alias name
						sqlQueryString += " ) as tbl1 "
								+ " ) as tbl2 order by id, time) as a "
								+ " where a.zone <> ( select b.zone from ( "
								+ " select *, ROW_NUMBER() OVER(PARTITION BY id) NUM from ( "
								+ " select id,zone, zone_name,CONCAT('TQ',LPAD(Floor(time/15)+1,2,0)) as tq, time from ( ";
					}else{
						
							sqlQueryString += " union " ;
					}
				}
				for(int j=0;j<tableNames2.size();j++){

					sqlQueryString += " select * from `" +	tableNames2.get(j) + "` ";

					//If it is the final table dont append union 
					//append alias variable and loop will end
					// append other query variables after the loop ends

					if(j == tableNames2.size() - 1) {// If equal append alias name
						sqlQueryString += " ) as tbl1 "
								+ " ) as tbl2 order by id, time) as b "
								+ " WHERE a.id = b.id "
								+ " AND a.Time > b.Time "
								+ " ORDER BY b.Time DESC "
								+ " LIMIT 1 "
								+ " ) or "
								+ " a.NUM  = 1 ";
								
					}else{
						
							sqlQueryString += " union " ;
					}
				}
			}

				



			sqlQueryString += " ) as tbl1 "//here check condition
					+ " group by id,zone,zone_name,tq) as tbl2 "
					+ " group by tq,zone,zone_name ) as tbl3 "
					+ " group by tq,zone,zone_name ) as tbl5 group by zoneid, zoneName;";
			if (AppConstants.print_log)
				System.out.println("Query : " + sqlQueryString);

				Session session = null;

				try {

					session = entityManagerFactory.unwrap(SessionFactory.class).openSession();

					Transaction transaction = session.beginTransaction();

					//Query<WorkStationAnalyticsResult> fetchQuery = session.createNativeQuery(sqlQueryString, WorkStationAnalyticsResult.class);
					//sqlQueryString = "select 1 as id, 'siva' as firstName, 'kumar' as lastName, '77' as version";
					Query<ZoneFootFallAverageResult> fetchQuery = session.createNativeQuery(sqlQueryString, "ZoneFootFallAverageMapping");



					zoneAverageFootFallResultRecords = fetchQuery.getResultList();
					transaction.commit();

					if (session!= null)
						session.close();

				}catch (Exception e) {
					e.printStackTrace();
					return zoneAverageFootFallResultRecords;
				}finally {
					if (session!= null)
						session.close();
				}

				return zoneAverageFootFallResultRecords;
			}
		return null;
	}
	
	@SuppressWarnings("unchecked")
	public List<ZoneFootFallAverageResult> SelectZoneAverageFootFallAnalytics(List<String> tableNames1, List<String> tableNames2,
			String zoneId) {

		List<ZoneFootFallAverageResult> zoneAverageFootFallResultRecords = null;
		if((tableNames1 != null && tableNames1.size() > 0 ) || (tableNames2 != null && tableNames2.size() > 0 )) {

			String sqlQueryString = "select zoneId, zoneName, sum(NoOfPersons) as totalpersons, max(NoOfPersons) as maximum, avg(NoOfPersons) as average  from (" 
					+ "select zone as zoneId, zone_name as zoneName, sum(NoOfPeople) as NoOfPersons, tq as tq from( ";

			if(null != tableNames1 && tableNames1.size() > 0) {

				sqlQueryString += " select zone, zone_name, count(tq) as NoOfPeople, tq as tq from ( "
						+ " select id, zone, zone_name, tq from( "
						+ " select a.* FROM (select *, ROW_NUMBER() OVER(PARTITION BY id) NUM from ( "
						+" select id,zone, zone_name, CONCAT('TQ',LPAD(Floor(time/15)+1,2,0)) as tq, time from ( "
						;
				
				for(int j=0;j<tableNames1.size();j++){

					sqlQueryString += " select * from `" +	tableNames1.get(j) + "` ";

					//If it is the final table dont append union 
					//append alias variable and loop will end
					// append other query variables after the loop ends

					if(j == tableNames1.size() - 1) {// If equal append alias name
						sqlQueryString += " ) as tbl1 "
								+ " ) as tbl2 order by id, time) as a "
								+ " where a.zone <> ( select b.zone from ( "
								+ " select *, ROW_NUMBER() OVER(PARTITION BY id) NUM from ( "
								+ " select id,zone, zone_name,CONCAT('TQ',LPAD(Floor(time/15)+1,2,0)) as tq, time from ( ";
					}else{
						
							sqlQueryString += " union " ;
					}
				}
				for(int j=0;j<tableNames1.size();j++){

					sqlQueryString += " select * from `" +	tableNames1.get(j) + "` ";

					//If it is the final table dont append union 
					//append alias variable and loop will end
					// append other query variables after the loop ends

					if(j == tableNames1.size() - 1) {// If equal append alias name
						sqlQueryString += " ) as tbl1 "
								+ " ) as tbl2 order by id, time) as b "
								+ " WHERE a.id = b.id "
								+ " AND a.Time > b.Time "
								+ " ORDER BY b.Time DESC "
								+ " LIMIT 1 "
								+ " ) or "
								+ " a.NUM  = 1 ";
								
					}else{
						
							sqlQueryString += " union " ;
					}
				}
				
				sqlQueryString += " ) as tbl1 where zone = '" + zoneId + "'"//here check condition
						+ " group by id,zone,zone_name,tq) as tbl2 "
						+ " group by tq,zone,zone_name "
						;
				
				if(null != tableNames2 && tableNames2.size() > 0) {
					sqlQueryString += " union " ;
				}
			}
			if(null != tableNames2 && tableNames2.size() > 0) {

				sqlQueryString += " select zone, zone_name, count(tq) as NoOfPeople, tq as tq from ( "
						+ " select id, zone, zone_name, tq from( "
						+ " select a.* FROM (select *, ROW_NUMBER() OVER(PARTITION BY id) NUM from ( "
						+" select id,zone, zone_name, CONCAT('TQ',LPAD(Floor(time/15)+1,2,0)) as tq, time from ( "
						;
				
				
				for(int j=0;j<tableNames2.size();j++){

					sqlQueryString += " select * from `" +	tableNames2.get(j) + "` ";

					//If it is the final table dont append union 
					//append alias variable and loop will end
					// append other query variables after the loop ends

					if(j == tableNames2.size() - 1) {// If equal append alias name
						sqlQueryString += " ) as tbl1 "
								+ " ) as tbl2 order by id, time) as a "
								+ " where a.zone <> ( select b.zone from ( "
								+ " select *, ROW_NUMBER() OVER(PARTITION BY id) NUM from ( "
								+ " select id,zone, zone_name,CONCAT('TQ',LPAD(Floor(time/15)+1,2,0)) as tq, time from ( ";
					}else{
						
							sqlQueryString += " union " ;
					}
				}
				for(int j=0;j<tableNames2.size();j++){

					sqlQueryString += " select * from `" +	tableNames2.get(j) + "` ";

					//If it is the final table dont append union 
					//append alias variable and loop will end
					// append other query variables after the loop ends

					if(j == tableNames2.size() - 1) {// If equal append alias name
						sqlQueryString += " ) as tbl1 "
								+ " ) as tbl2 order by id, time) as b "
								+ " WHERE a.id = b.id "
								+ " AND a.Time > b.Time "
								+ " ORDER BY b.Time DESC "
								+ " LIMIT 1 "
								+ " ) or "
								+ " a.NUM  = 1 ";
								
					}else{
						
							sqlQueryString += " union " ;
					}
				}
			}

		


			sqlQueryString += " ) as tbl1  where zone = '" + zoneId + "'"//here check condition
					+ " group by id,zone,zone_name,tq) as tbl2 "
					+ " group by tq,zone,zone_name ) as tbl3 "
					+ " group by tq,zone,zone_name ) as tbl5 group by zoneid, zoneName;";
			if (AppConstants.print_log)
				System.out.println("Query : " + sqlQueryString);
		
				Session session = null;

				try {

					session = entityManagerFactory.unwrap(SessionFactory.class).openSession();

					Transaction transaction = session.beginTransaction();

					//Query<WorkStationAnalyticsResult> fetchQuery = session.createNativeQuery(sqlQueryString, WorkStationAnalyticsResult.class);
					//sqlQueryString = "select 1 as id, 'siva' as firstName, 'kumar' as lastName, '77' as version";
					Query<ZoneFootFallAverageResult> fetchQuery = session.createNativeQuery(sqlQueryString, "ZoneFootFallAverageMapping");



					zoneAverageFootFallResultRecords = fetchQuery.getResultList();
					transaction.commit();

					if (session!= null)
						session.close();

				}catch (Exception e) {
					e.printStackTrace();
					return zoneAverageFootFallResultRecords;
				}finally {
					if (session!= null)
						session.close();
				}

				return zoneAverageFootFallResultRecords;
			}
		return null;
	}

	@SuppressWarnings("unchecked")
	public List<AttendanceZoneResult> SelectAttendanceZoneAnalytics(List<String> tableNames1 , List<String> tableNames2) {

		List<AttendanceZoneResult> attendanceZoneResultRecords = null;
		if((tableNames1 != null && tableNames1.size() > 0 ) || (tableNames2 != null && tableNames2.size() > 0 )) {

			String sqlQueryString = "select zoneId, zoneName, NoOfPersons, tq, capacity as zoneRestrictions, class_name as zoneClassification from("
					+ "select * from("
					+ "select zone as zoneId, zone_name as zoneName, sum(NoOfPeople) as NoOfPersons, tq as tq from( ";

			if(null != tableNames1 && tableNames1.size() > 0) {

				sqlQueryString += " select zone, zone_name, count(tq) as NoOfPeople, tq as tq from ( "
						+ " select id,zone, zone_name,CONCAT('TQ',LPAD(Floor(time/15)+1,2,0)) as tq from ( ";
				
				for(int j=0;j<tableNames1.size();j++){

					//System.out.println("In Between Table Names : " + tableNames.get(j));

					sqlQueryString += " select * from `" +	tableNames1.get(j) + "` ";

					//If it is the final table dont append union 
					//append alias variable and loop will end
					// append other query variables after the loop ends

					if(j == tableNames1.size() - 1) {// If equal append alias name
						sqlQueryString += " ) as tbl1 "
								+ " where MOD(time,15) = 8 or (MOD(time,15) < 8 and MOD(time,15) + duration - 1 >= 8 and duration <> 1) "
								+ " group by id,zone,zone_name,tq) as tbl2 group by tq,zone,zone_name " ;
					}else{
						
							sqlQueryString += " union " ;
					}
				}
				if(null != tableNames2 && tableNames2.size() > 0) {
					sqlQueryString += " union " ;
				}
			}
			if(null != tableNames2 && tableNames2.size() > 0) {

				sqlQueryString += " select zone, zone_name, count(tq) as NoOfPeople, tq as tq from ( "
						+ " select id,zone, zone_name,CONCAT('TQ',LPAD(Floor(time/15)+1,2,0)) as tq from ( ";
				
				for(int j=0;j<tableNames2.size();j++){

					sqlQueryString += " select * from `" +	tableNames2.get(j) + "` ";

					//If it is the final table dont append union 
					//append alias variable and loop will end
					// append other query variables after the loop ends

					if(j == tableNames2.size() - 1) {// If equal append alias name
						sqlQueryString += " ) as tbl1 "
								+ " where MOD(time,15) = 8 or (MOD(time,15) < 8 and MOD(time,15) + duration - 1 >= 8 and duration <> 1) "
								+ " group by id,zone,zone_name,tq) as tbl2 group by tq,zone,zone_name " ;
					}else{
						
							sqlQueryString += " union " ;
					}
				}
			}

				



				sqlQueryString += " ) as tbl3 group by tq,zone,zone_name ) as tbl7 join "
						+ " (select id as zone_id, zone_classification, capacity from zone_details)  as tbl4 "
						+ " join "
						+ " (select id as classification_id, class_name from zone_classification) as tbl5 "
						+ " on "
						+ " tbl7.zoneId =  tbl4.zone_id and "
						+ " tbl4.zone_classification = tbl5.classification_id) "
						+ " as tbl6  order by tQ;";
				if (AppConstants.print_log)
				System.out.println("Query : " + sqlQueryString);

				Session session = null;

				try {

					session = entityManagerFactory.unwrap(SessionFactory.class).openSession();

					Transaction transaction = session.beginTransaction();

					//Query<WorkStationAnalyticsResult> fetchQuery = session.createNativeQuery(sqlQueryString, WorkStationAnalyticsResult.class);
					//sqlQueryString = "select 1 as id, 'siva' as firstName, 'kumar' as lastName, '77' as version";
					Query<AttendanceZoneResult> fetchQuery = session.createNativeQuery(sqlQueryString, "AttendanceZoneResultMapping");



					attendanceZoneResultRecords = fetchQuery.getResultList();
					transaction.commit();

					if (session!= null)
						session.close();

				}catch (Exception e) {
					e.printStackTrace();
					return attendanceZoneResultRecords;
				}finally {
					if (session!= null)
						session.close();
				}

				return attendanceZoneResultRecords;
			}
		return null;
	}
	@SuppressWarnings("unchecked")
	public List<AttendancePeopleResult> SelectAttendancePeopleAnalytics(List<String> tableNames1 , List<String> tableNames2) {

		List<AttendancePeopleResult> attendancePeopleResultRecords = null;
		if((tableNames1 != null && tableNames1.size() > 0 ) || (tableNames2 != null && tableNames2.size() > 0 )) {

			String sqlQueryString = "select zone as zoneId, zone_name as zoneName, count(id) as NoOfPersons, "
					+ "sum(case when type like 'staff' then 1 else 0 end) AS staffcount, "
					+ "sum(case when type like 'visitor' then 1 else 0 end) AS visitorcount, "
					+ "sum(case when type like 'vendor' then 1 else 0 end) AS vendorcount, "
					+ "sum(case when type like 'Contractor' then 1 else 0 end) AS contractorcount, "
					+ "tq as tq from(";

			if(null != tableNames1 && tableNames1.size() > 0) {

				sqlQueryString += " select id, 'staff' as type, zone, zone_name, CONCAT('TQ',LPAD(Floor(time/15)+1,2,0)) as tq from( "
						+ " select * from( ";
				
				for(int j=0;j<tableNames1.size();j++){

					//System.out.println("In Between Table Names : " + tableNames.get(j));

					sqlQueryString += " select * from `" +	tableNames1.get(j) + "` ";

					//If it is the final table dont append union 
					//append alias variable and loop will end
					// append other query variables after the loop ends

					if(j == tableNames1.size() - 1) {// If equal append alias name
						sqlQueryString += " ) as tbl1 "
								+ " where MOD(time,15) = 8 or (MOD(time,15) < 8 and MOD(time,15) + duration - 1 >= 8 and duration <> 1) ) as tbl2 " ;
					}else{
						
							sqlQueryString += " union " ;
					}
				}
				if(null != tableNames2 && tableNames2.size() > 0) {
					sqlQueryString += " union " ;
				}
			}
			if(null != tableNames2 && tableNames2.size() > 0) {

				sqlQueryString += " select * from( "
						+ " select id, type, zone, zone_name, CONCAT('TQ',LPAD(Floor(time/15)+1,2,0)) as tq from( "
						+" select * from( ";
				
				for(int j=0;j<tableNames2.size();j++){

					sqlQueryString += " select * from `" +	tableNames2.get(j) + "` ";

					//If it is the final table dont append union 
					//append alias variable and loop will end
					// append other query variables after the loop ends

					if(j == tableNames2.size() - 1) {// If equal append alias name
						sqlQueryString += " ) as tbl1 "
								+ " where MOD(time,15) = 8 or (MOD(time,15) < 8 and MOD(time,15) + duration - 1 >= 8 and duration <> 1)) as tbl2 " 
								+" join "
								+" (Select visitor_id, type from visitor) as tbl3 on tbl2.id = tbl3.visitor_id ) as tbl4";
					}else{
						
							sqlQueryString += " union " ;
					}
				}
			}

				



				sqlQueryString += " )  as tbl5 group by tq,zone,zone_name order by tq;";
				if (AppConstants.print_log)
				System.out.println("Query : " + sqlQueryString);

				Session session = null;

				try {

					session = entityManagerFactory.unwrap(SessionFactory.class).openSession();

					Transaction transaction = session.beginTransaction();

					//Query<WorkStationAnalyticsResult> fetchQuery = session.createNativeQuery(sqlQueryString, WorkStationAnalyticsResult.class);
					//sqlQueryString = "select 1 as id, 'siva' as firstName, 'kumar' as lastName, '77' as version";
					Query<AttendancePeopleResult> fetchQuery = session.createNativeQuery(sqlQueryString, "AttendancePeopleResultMapping");



					attendancePeopleResultRecords = fetchQuery.getResultList();
					transaction.commit();

					if (session!= null)
						session.close();

				}catch (Exception e) {
					e.printStackTrace();
					return attendancePeopleResultRecords;
				}finally {
					if (session!= null)
						session.close();
				}

				return attendancePeopleResultRecords;
			}
		return null;
	}
	@SuppressWarnings("unchecked")
	public List<AgingAnalyticsResult> SelectWorkorderAgingAnalytics(List<String> tableNames) {
		
		List<AgingAnalyticsResult> agingAnalysisResult = null;
		if(null != tableNames && tableNames.size() > 0) {

			String sqlQueryString = "select  station_id as workStationId, Station_type as workStationName, id as workOrderId from( select * from( ";

			for(int j=0;j<tableNames.size();j++){

				//System.out.println("In Between Table Names : " + tableNames.get(j));

				sqlQueryString += " select * from `" +	tableNames.get(j) + "` ";

				//If it is the final table dont append union 
				//append alias variable and loop will end
				// append other query variables after the loop ends

				if(j == tableNames.size() - 1) {// If equal append alias name
					sqlQueryString += " ) as tbl where time = (select max(time) from ( " ;
				}else{
					sqlQueryString += " union " ;
				}
			}
			
			for(int j=0;j<tableNames.size();j++){

				//System.out.println("In Between Table Names : " + tableNames.get(j));

				sqlQueryString += " select * from `" +	tableNames.get(j) + "` ";

				//If it is the final table dont append union 
				//append alias variable and loop will end
				// append other query variables after the loop ends

				if(j == tableNames.size() - 1) {// If equal append alias name
					sqlQueryString += " )as tbl1)) as tbl2 " ;
				}else{
					sqlQueryString += " union " ;
				}
			}



			sqlQueryString += " join  (SELECT station_id, work_area, Station_type FROM work_station where active = 1 ) as tbl3 on  tbl2.zone = tbl3.work_area ;";
			if (AppConstants.print_log)
				System.out.println("Query : " + sqlQueryString);

			Session session = null;

			try {

				session = entityManagerFactory.unwrap(SessionFactory.class).openSession();

				Transaction transaction = session.beginTransaction();

				Query<AgingAnalyticsResult> fetchQuery = session.createNativeQuery(sqlQueryString, "WorkStationAgingMapping");
				
				

				agingAnalysisResult = fetchQuery.getResultList();
				transaction.commit();

				if (session!= null)
					session.close();

			}catch (Exception e) {
				e.printStackTrace();
				return agingAnalysisResult;
			}finally {
				if (session!= null)
					session.close();
			}
	
			return agingAnalysisResult;
		}
		return null;
	}
	@SuppressWarnings({ "unchecked", "deprecation" })
	public void SelectUnknownColumns() {

		String sqlQueryString = "select 1 as id, 'siva' as firstName, 'kumar' as lastName, '77' as version";
		

				Session session = null;

				try {

					session = entityManagerFactory.unwrap(SessionFactory.class).openSession();

					Transaction transaction = session.beginTransaction();
					
					EntityManager entityManager = entityManagerFactory.createEntityManager();
					
					Query<?> query = (Query<?>)session.createSQLQuery(sqlQueryString);
					query.setResultTransformer(AliasToEntityMapResultTransformer.INSTANCE);
					List<Map<String,Object>> valuesList=(List<Map<String, Object>>) query.list();
					
					

					transaction.commit();

					if (session!= null)
						session.close();

				}catch (Exception e) {
					e.printStackTrace();
					return ;
				}finally {
					if (session!= null)
						session.close();
				}

				
	}
	public List<WorkStationChartAnalyticsResult> SelectAllWorkstationChartAnalytics(List<String> tableNames) {
		
		List<WorkStationChartAnalyticsResult> workStationChartAnalyticsResult = null;
		if(null != tableNames && tableNames.size() > 0) {

			String sqlQueryString = "select station_id as workStationId,tq,station_type as workStationName, "
					+ "SUM( "
					+"CASE " 
					+"WHEN tbl3.outbound = tbl3.zone THEN 1 "
					+"ELSE 0 "
					+"END) wo, "
					+ "SUM( "
					+"CASE " 
					+"WHEN tbl3.inbound = tbl3.zone THEN 1 "
					+"ELSE 0 "
					+"END) wi, "
					+ "SUM( "
					+"CASE " 
					+"WHEN tbl3.work_area = tbl3.zone THEN 1 "
					+"ELSE 0 "
					+"END) pr, "
					+ "SUM( "
					+"CASE " 
					+"WHEN tbl3.outbound = tbl3.zone THEN 1 "
					+"WHEN tbl3.inbound = tbl3.zone THEN 1 "
					+"WHEN tbl3.work_area = tbl3.zone THEN 1 "
					+"ELSE 0 "
					+"END) tot "
					+"from( "
					+"select * from( "
					+"select id,zone, zone_name, CONCAT('TQ',LPAD(Floor(time/15)+1,2,0)) as tq, time ,MOD(time,15) as remainder,duration "
					+ "from ( ";
				

			for(int j=0;j<tableNames.size();j++){

				//System.out.println("In Between Table Names : " + tableNames.get(j));

				sqlQueryString += " select * from `" +	tableNames.get(j) + "` ";

				//If it is the final table dont append union 
				//append alias variable and loop will end
				// append other query variables after the loop ends

				if(j == tableNames.size() - 1) {// If equal append alias name
					sqlQueryString += " ) as tbl  "
				+ " where MOD(time,15) = 0 or (MOD(time,15) <> 0 and MOD(time,15) + duration - 1 >= 15 and duration <> 1 )) as tbl1 ";
				}else{
					sqlQueryString += " union " ;
				}
			}
					



			sqlQueryString += " join  (SELECT station_id,station_type, outbound, inbound, work_area FROM work_station where active = 1)  as tbl2 "
							+ "on " 
							+ "tbl1.zone = tbl2.outbound or "
							+ "tbl1.zone = tbl2.inbound or "
							+ "tbl1.zone = tbl2.work_area) as tbl3 group by station_id,tq,station_type ;";
			if (AppConstants.print_log)
			System.out.println("Query : " + sqlQueryString);

			Session session = null;

			try {

				session = entityManagerFactory.unwrap(SessionFactory.class).openSession();

				Transaction transaction = session.beginTransaction();

				Query<WorkStationChartAnalyticsResult> fetchQuery = session.createNativeQuery(sqlQueryString, "WorkStationChartMapping");
				
				

				workStationChartAnalyticsResult = fetchQuery.getResultList();
				transaction.commit();

				if (session!= null)
					session.close();

			}catch (Exception e) {
				e.printStackTrace();
				return workStationChartAnalyticsResult;
			}finally {
				if (session!= null)
					session.close();
			}
	
			return workStationChartAnalyticsResult;
		}
		return null;
	}

	public List<WorkStationChartAnalyticsResult> SelectWorkstationChartAnalyticsByWorkStationId(List<String> tableNames,
			String workStationId) {
		
		List<WorkStationChartAnalyticsResult> workStationChartAnalyticsResult = null;
		if(null != tableNames && tableNames.size() > 0) {

			String sqlQueryString = "select station_id as workStationId,tq,station_type as workStationName, "
					+ "SUM( "
					+"CASE " 
					+"WHEN tbl3.outbound = tbl3.zone THEN 1 "
					+"ELSE 0 "
					+"END) wo, "
					+ "SUM( "
					+"CASE " 
					+"WHEN tbl3.inbound = tbl3.zone THEN 1 "
					+"ELSE 0 "
					+"END) wi, "
					+ "SUM( "
					+"CASE " 
					+"WHEN tbl3.work_area = tbl3.zone THEN 1 "
					+"ELSE 0 "
					+"END) pr, "
					+ "SUM( "
					+"CASE " 
					+"WHEN tbl3.outbound = tbl3.zone THEN 1 "
					+"WHEN tbl3.inbound = tbl3.zone THEN 1 "
					+"WHEN tbl3.work_area = tbl3.zone THEN 1 "
					+"ELSE 0 "
					+"END) tot "
					+"from( "
					+"select * from( "
					+"select id,zone, zone_name, CONCAT('TQ',LPAD(Floor(time/15)+1,2,0)) as tq, time ,MOD(time,15) as remainder,duration "
					+ "from ( ";
				

			for(int j=0;j<tableNames.size();j++){

				//System.out.println("In Between Table Names : " + tableNames.get(j));

				sqlQueryString += " select * from `" +	tableNames.get(j) + "` ";

				//If it is the final table dont append union 
				//append alias variable and loop will end
				// append other query variables after the loop ends

				if(j == tableNames.size() - 1) {// If equal append alias name
					sqlQueryString += " ) as tbl  "
				+ " where MOD(time,15) = 0 or (MOD(time,15) <> 0 and MOD(time,15) + duration - 1 >= 15 and duration <> 1 )) as tbl1 ";
				}else{
					sqlQueryString += " union " ;
				}
			}
					



			sqlQueryString += " join  (SELECT station_id,station_type, outbound, inbound, work_area FROM work_station where active = 1 and station_id = '"
							+ workStationId
							+"')  as tbl2 "
							+ "on " 
							+ "tbl1.zone = tbl2.outbound or "
							+ "tbl1.zone = tbl2.inbound or "
							+ "tbl1.zone = tbl2.work_area) as tbl3 group by station_id,tq,station_type ;";
			if (AppConstants.print_log)
			System.out.println("Query : " + sqlQueryString);

			Session session = null;

			try {

				session = entityManagerFactory.unwrap(SessionFactory.class).openSession();

				Transaction transaction = session.beginTransaction();

				Query<WorkStationChartAnalyticsResult> fetchQuery = session.createNativeQuery(sqlQueryString, "WorkStationChartMapping");
				
				

				workStationChartAnalyticsResult = fetchQuery.getResultList();
				transaction.commit();

				if (session!= null)
					session.close();

			}catch (Exception e) {
				e.printStackTrace();
				return workStationChartAnalyticsResult;
			}finally {
				if (session!= null)
					session.close();
			}
	
			return workStationChartAnalyticsResult;
		}
		return null;
	}
	
}
