package com.entappia.ei4o.repository;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.entappia.ei4o.dbmodels.Login;

@Repository
public interface LoginRegistrationRepository extends CrudRepository<Login, Integer> {
	Login findByEmailID(String emailID);
	
	@Query(value = "update login set password=:password where emailid=:emailId", nativeQuery = true)
	Login updatePassword(@Param("emailId") String emailId,
			@Param("password") String password);
	
	List<Login> findAll();
	
}
