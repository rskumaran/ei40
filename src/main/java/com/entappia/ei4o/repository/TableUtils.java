package com.entappia.ei4o.repository;

import javax.persistence.EntityManagerFactory;

import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.entappia.ei4o.EI4OApplication; 

@Repository
public class TableUtils {

	/*
	 * @Autowired private SessionFactory sessionFactory;
	 */

	@Autowired
	private EntityManagerFactory entityManagerFactory;
	
	@Autowired
	private EI4OApplication ei4oApplication;
	

	public static final String staff_location = "CREATE TABLE IF NOT EXISTS `staff_location` (  `staff_id` varchar(12) NOT NULL,  `time` datetime(6), `grid` smallint, zone varchar(12), PRIMARY KEY (`staff_id`, `time`)) ENGINE=InnoDB  DEFAULT CHARSET=latin1;";
	public static final String visitor_location = "CREATE TABLE IF NOT EXISTS `visitor_location` (  `visitor_id` varchar(12) NOT NULL,  `time` datetime(6), `grid` smallint, zone varchar(12), PRIMARY KEY (`visitor_id`, `time`)) ENGINE=InnoDB  DEFAULT CHARSET=latin1;";
	public static final String assetnc_location = "CREATE TABLE IF NOT EXISTS `assetnc_location` (  `asset_nc_id` varchar(12) NOT NULL,  `time` datetime(6), `grid` smallint, zone varchar(12), PRIMARY KEY (`asset_nc_id`, `time`)) ENGINE=InnoDB  DEFAULT CHARSET=latin1;";
	public static final String asset_chrono_location = "CREATE TABLE IF NOT EXISTS `asset_chrono_location` (  `asset_chrono_id` varchar(12) NOT NULL,  `time` datetime(6), `grid` smallint, zone varchar(12), PRIMARY KEY (`asset_chrono_id`, `time`)) ENGINE=InnoDB  DEFAULT CHARSET=latin1;";

	public static final String asset_vehicle_location = "CREATE TABLE IF NOT EXISTS `asset_vehicle_location` (  `vehicle_id` varchar(12) NOT NULL,  `time` datetime(6), `path_segment` varchar(20), direction varchar(20), PRIMARY KEY (`vehicle_id`, `time`)) ENGINE=InnoDB  DEFAULT CHARSET=latin1;";
	public static final String work_material_location = "CREATE TABLE IF NOT EXISTS `work_material_location` (  `work_item` varchar(12) NOT NULL,  `time` datetime(6), `station` varchar(20), state varchar(20), PRIMARY KEY (`work_item`, `time`)) ENGINE=InnoDB  DEFAULT CHARSET=latin1;";
	public static final String material_movement = "CREATE TABLE IF NOT EXISTS `material_movement` (  `asset_vehicle` varchar(12) NOT NULL,  `time` datetime(6), `grid` smallint, station varchar(12), PRIMARY KEY (`asset_vehicle`, `time`)) ENGINE=InnoDB  DEFAULT CHARSET=latin1;";

	public boolean createTable(String tableName, String date, String shiftName) {

		try {

			String tableName1 = tableName + "_" + date + "_" + shiftName;

			if (!EI4OApplication.tableNameList.contains(tableName1)) {
				Session session = entityManagerFactory.unwrap(SessionFactory.class).openSession();

				String sqlQueryString = "";

				if (tableName.equals("staff_location")) {
					sqlQueryString = staff_location;
				} else if (tableName.equals("visitor_location")) {
					sqlQueryString = visitor_location;
				} else if (tableName.equals("assetnc_location")) {
					sqlQueryString = assetnc_location;
				} else if (tableName.equals("asset_chrono_location")) {
					sqlQueryString = asset_chrono_location;
				} else if (tableName.equals("asset_vehicle_location")) {
					sqlQueryString = asset_vehicle_location;
				} else if (tableName.equals("work_material_location")) {
					sqlQueryString = work_material_location;
				} else if (tableName.equals("material_movement")) {
					sqlQueryString = material_movement;
				}

				String sqlString = sqlQueryString.replace(tableName, tableName1);
				Transaction transaction = session.beginTransaction();

				Query updateQuery = session.createNativeQuery(sqlString);

				updateQuery.executeUpdate();
				transaction.commit();
				
				ei4oApplication.getTableNames();
				
			}else
				return false;
		} catch (Exception e) {
			return false;
		}

		return true;
	}

}
