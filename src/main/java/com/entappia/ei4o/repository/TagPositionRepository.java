package com.entappia.ei4o.repository;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
 
import com.entappia.ei4o.dbmodels.TagPosition;
 

@Repository
public interface TagPositionRepository extends CrudRepository<TagPosition, Integer> {
	
    List<TagPosition> findAll();
    
    @Query(value = "select * from tag_position where last_seen>=:last_seen", nativeQuery = true)
	List<TagPosition> findAllTagDetails(@Param("last_seen") long last_seen);
    
    TagPosition findByMacId(String macId);

}
