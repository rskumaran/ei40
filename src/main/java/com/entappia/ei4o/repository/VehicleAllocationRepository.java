package com.entappia.ei4o.repository;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.entappia.ei4o.dbmodels.VehicleAllocation;

@Repository
public interface VehicleAllocationRepository extends CrudRepository<VehicleAllocation, Integer> {
	
	VehicleAllocation findByVehicleIdAndMacId(String vehicleId, String macId);
	
	@Query(value = "SELECT * FROM vehicle_allocation where vehicle_id=:vehicleId and status='Valid';", nativeQuery = true)
	VehicleAllocation findAllocation(@Param("vehicleId") String vehicleId);
	
}
