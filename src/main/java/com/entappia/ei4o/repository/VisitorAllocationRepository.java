package com.entappia.ei4o.repository;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.entappia.ei4o.dbmodels.VisitorAllocation;

@Repository
public interface VisitorAllocationRepository extends CrudRepository<VisitorAllocation, Integer> {

	VisitorAllocation findByVisitorIdAndMacId(long vistorId, String macId);
	
	@Query(value = "SELECT * FROM visitor_allocation where visitor_id=:vistorId and status='Valid';", nativeQuery = true)
	VisitorAllocation findAllocation(@Param("vistorId") long vistorId);
}
