package com.entappia.ei4o.repository;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.entappia.ei4o.dbmodels.StaffAllocation;

@Repository
public interface StaffAllocationRepository extends CrudRepository<StaffAllocation, Integer> {
	
	StaffAllocation findByStaffIdAndMacId(String staffId, String macId);
	
	@Query(value = "SELECT * FROM staff_allocation where staff_id=:staffId and status='Valid';", nativeQuery = true)
	StaffAllocation findAllocation(@Param("staffId") String staffId);

}
