package com.entappia.ei4o.repository;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import javax.persistence.EntityManagerFactory;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Component;

import com.entappia.ei4o.dbmodels.Logs;

@Component
public class LogsViewRepository {
	@Autowired
	private EntityManagerFactory entityManagerFactory;
	
	@SuppressWarnings("unchecked")
	public List<Logs> SelectAllLogs(Date logDate, int noOfRecords,	String tableName  )
	{
		List<Logs> logList = null;
		
		SimpleDateFormat sdfDate = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS");
	    String strDate = sdfDate.format(logDate);
	    
		String sqlQueryString = "SELECT * FROM `"
				+ tableName + "` where date < '" + strDate + "' order by date desc limit " + noOfRecords + "";
		//System.out.println(sqlQueryString);

		Session session = null;
		try {

			session = entityManagerFactory.unwrap(SessionFactory.class).openSession();

			Transaction transaction = session.beginTransaction();

			Query fetchQuery = session.createNativeQuery(sqlQueryString, Logs.class);

			logList = fetchQuery.getResultList();
			transaction.commit();

			if (session!= null)
				session.close();

		}catch (Exception e) {
			return logList;
		}finally {
			if (session!= null)
				session.close();
		} 

		return logList;

	}

public Date GetMaxDate(String tableName) {
		
		Date maxLogDate = null;//new Date();
		String sqlQueryString = "SELECT max(Date) FROM `"	+ tableName + "`";
		//System.out.println(sqlQueryString);

		Session session = null;
		try {

			session = entityManagerFactory.unwrap(SessionFactory.class).openSession();

			Transaction transaction = session.beginTransaction();

			Query fetchQuery = session.createNativeQuery(sqlQueryString);
			
			Object LogDate = fetchQuery.getSingleResult();
			
			maxLogDate =  (Date) LogDate;
						
			//maxLogId = ((short)LogId);
			
			/*if (LogDate instanceof Date) {
				Date logDate =  (Date) LogId;
				maxLogId = (int)Id;
			}*/
			
			
			transaction.commit();

			if (session!= null)
				session.close();

		}catch (Exception e) {
			return maxLogDate;
		}finally {
			if (session!= null)
				session.close();
		} 

		return maxLogDate;
	}

	/*public int GetMaxLogId(String tableName) {
		
		int maxLogId = 0;
		String sqlQueryString = "SELECT max(log_id) FROM `"	+ tableName + "`";
		System.out.println(sqlQueryString);

		Session session = null;
		try {

			session = entityManagerFactory.unwrap(SessionFactory.class).openSession();

			Transaction transaction = session.beginTransaction();

			Query fetchQuery = session.createNativeQuery(sqlQueryString);
			
			Object LogId = fetchQuery.getSingleResult();
			
			//maxLogId = ((short)LogId);
			
			if (LogId instanceof Short) {
				short Id =  (short) LogId;
				maxLogId = (int)Id;
			}
			else if (LogId instanceof Integer) {
				maxLogId =  (int) LogId;
			}
			
			transaction.commit();

			if (session!= null)
				session.close();

		}catch (Exception e) {
			return maxLogId;
		}finally {
			if (session!= null)
				session.close();
		} 

		return maxLogId;
	}*/
}
