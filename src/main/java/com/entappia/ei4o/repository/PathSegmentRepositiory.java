package com.entappia.ei4o.repository;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param; 
import org.springframework.stereotype.Repository;

import com.entappia.ei4o.dbmodels.Partitions;
import com.entappia.ei4o.dbmodels.PathSegments;

@Repository
public interface PathSegmentRepositiory extends CrudRepository<PathSegments, Integer> {

	@Query(value = "select * from path_segments where campus_id=:campusId", nativeQuery = true)
	List<PathSegments> findByCampusId(@Param("campusId") long campusId);

	@Query(value = "select * from path_segments where active=true", nativeQuery = true)
	List<PathSegments> findAllActivePathSegments();
	
	PathSegments findById(long id);

}
