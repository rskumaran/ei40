package com.entappia.ei4o.repository;


import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.entappia.ei4o.dbmodels.WorkOrderZoneAnalysis;

@Repository
public interface WorkOrderZoneAnalysisRepository extends CrudRepository<WorkOrderZoneAnalysis, Integer> {

	WorkOrderZoneAnalysis findByOrderIdAndStationId(String orderNo, String stationId);
	
	

}
