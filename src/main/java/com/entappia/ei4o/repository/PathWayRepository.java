package com.entappia.ei4o.repository;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.entappia.ei4o.dbmodels.PathWay;

@Repository
public interface PathWayRepository   extends CrudRepository<PathWay, Integer>{

 
	PathWay findById(long id);
	
	List<PathWay> findAll();
	
	@Modifying
	@Transactional
	@Query(value = "truncate path_way", nativeQuery = true)
	void truncateTable();
}
