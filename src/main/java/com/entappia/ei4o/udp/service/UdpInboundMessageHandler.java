package com.entappia.ei4o.udp.service;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.integration.annotation.MessageEndpoint;
import org.springframework.integration.annotation.ServiceActivator;
import org.springframework.messaging.Message;
import org.springframework.messaging.handler.annotation.Headers;

import com.entappia.ei4o.dbmodels.TagPosition;
import com.entappia.ei4o.models.SocketIOutputData;
import com.entappia.ei4o.models.TagData;
import com.entappia.ei4o.notification.NotificationMessages;
import com.entappia.ei4o.repository.TagPositionRepository;
import com.entappia.ei4o.utils.Utils;
import com.entappia.ei4o.websocket.ScheduledPushMessages;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

@MessageEndpoint
public class UdpInboundMessageHandler {

	private final static Logger LOGGER = LoggerFactory.getLogger(UdpInboundMessageHandler.class);

	@Autowired
	ScheduledPushMessages scheduledPushMessages;
	
	@Autowired
	NotificationMessages notificationMessages;

	@Autowired
	private TagPositionRepository tagPositionRepository;

	Gson gson = new Gson();
	
	@ServiceActivator(inputChannel = "inboundChannel")
	public void handeMessage(Message message, @Headers Map<String, Object> headerMap) {

		String payLoad = new String((byte[]) message.getPayload());
		try{
		if (!Utils.isEmptyString(payLoad)) {
			JSONObject jsonPayLoad = new JSONObject(payLoad);
			int status = jsonPayLoad.getInt("status");
			if (status == 200) {

				if (jsonPayLoad.getString("type").equals("tag"))
					sendData();
				else if (jsonPayLoad.getString("type").equals("locator"))
					notificationMessages.locatorStatusAlert(jsonPayLoad.getJSONArray("data")); 
				else if (jsonPayLoad.getString("type").equals("crowdedZone"))
				{
					Type listType = new TypeToken<ArrayList<HashMap<String, String>>>(){}.getType();
					ArrayList<HashMap<String, String>> zoneList = gson.fromJson(jsonPayLoad.getJSONArray("data").toString(), listType);
					notificationMessages.zoneOccupancyAlert(zoneList); 
				}
				else if (jsonPayLoad.getString("type").equals("tags")){
					Type listType = new TypeToken<ArrayList<HashMap<String, String>>>(){}.getType();
					
					ArrayList<HashMap<String, String>> tagsList = gson.fromJson(jsonPayLoad.getJSONArray("data").toString(), listType);
					notificationMessages.lostTagAlert(tagsList); 
				}else if (jsonPayLoad.getString("type").equals("lateTag")){
					Type listType = new TypeToken<HashMap<String, List<HashMap<String, String>>>>(){}.getType();
					HashMap<String, List<HashMap<String, String>>>tagsList = gson.fromJson(jsonPayLoad.getJSONObject("data").toString(), listType);
					notificationMessages.latetagAlert(tagsList); 
				}else if (jsonPayLoad.getString("type").equals("restrictedZoneVisitor")){
					Type listType = new TypeToken<HashMap<String, List<HashMap<String, String>>>>(){}.getType();
					HashMap<String, List<HashMap<String, String>>>tagsList = gson.fromJson(jsonPayLoad.getJSONObject("data").toString(), listType);
					notificationMessages.restrictedZoneTagAlert(tagsList); 
				}else if (jsonPayLoad.getString("type").equals("lateAsset")){
					Type listType = new TypeToken<HashMap<String, List<HashMap<String, String>>>>(){}.getType();
					HashMap<String, List<HashMap<String, String>>>tagsList = gson.fromJson(jsonPayLoad.getJSONObject("data").toString(), listType);
					notificationMessages.lateAssetAlert(tagsList); 
				}else if (jsonPayLoad.getString("type").equals("ShippingArea")){
					Type listType = new TypeToken<HashMap<String, List<HashMap<String, String>>>>(){}.getType();
					HashMap<String, List<HashMap<String, String>>>tagsList = gson.fromJson(jsonPayLoad.getJSONObject("data").toString(), listType);
					notificationMessages.shippingAreaAlert(tagsList); 
					
				}else if (jsonPayLoad.getString("type").equals("limitCrossed")){
					Type listType = new TypeToken<HashMap<String, List<HashMap<String, String>>>>(){}.getType();
					HashMap<String, List<HashMap<String, String>>>tagsList = gson.fromJson(jsonPayLoad.getJSONObject("data").toString(), listType);
					notificationMessages.workorderLimitCrossedZoneAlert(tagsList); 
					
				}

			}

		}
		}catch( Exception e){
			e.printStackTrace();
		}

	}

	private void sendData() {
		
		ExecutorService executorService = Executors.newFixedThreadPool(2);
		executorService.execute(new Runnable() {

			public void run() {

				try {
			HashMap<String, TagPosition> tagPostions = new HashMap<>();
			tagPositionRepository.findAll().forEach(e -> tagPostions.put("" + e.getMacId(), e));
			ArrayList<String> sessionIdList = TagData.getSessionIdList();

			if (sessionIdList.size() > 0 && tagPostions.size() > 0) {
				SocketIOutputData socketIOutputData = new SocketIOutputData("success", tagPostions); 
				scheduledPushMessages.sendCustomMessage(socketIOutputData);
				 
			}
				} catch (Exception e) {
					e.printStackTrace();
				}

			}
		});
		executorService.shutdown(); 

	}
}
