package com.entappia.ei4o.services;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;

import com.entappia.ei4o.dbmodels.Logs;
import com.entappia.ei4o.repository.LogsRepository;

@Service
public class LogsServices {

	@Autowired
	private LogsRepository logsRepository;

	static List<Logs> evntsyncQueue = new ArrayList<Logs>();
	static boolean eventlog = true;

	@Async
	public void saveLog(Logs logger) {

		try {
			evntsyncQueue.add(logger);
			if (eventlog) {
				while (evntsyncQueue.size() > 0) {
					eventlog = false;
					
					Logs logs = evntsyncQueue.get(0);
					logs.setDate(new Date());
					if (logsRepository.save(logs) != null) {

						evntsyncQueue.remove(0);
					}
				}

				eventlog = true;
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	public synchronized void addLog(String type, String major, String minor, String message, Map<String, String> parameter) { 
		
		Logs logs = new Logs();
		
		logs.setDate(new Date());
		logs.setType(type);
		logs.setMajor(major);
		logs.setMinor(minor);
		logs.setResult(message);  
		logs.setParameter(parameter);
		
		//logsRepository.save(logs);
		
		saveLog(logs);
	}
}
