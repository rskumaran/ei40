package com.entappia.ei4o.services;

import java.io.File;
import java.io.IOException;
import java.lang.reflect.Type;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import com.entappia.ei4o.constants.AppConstants;
import com.entappia.ei4o.utils.Utils;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

@Service
public class SKUEvent {

	public static volatile Hashtable<String, HashMap<String, String>> skuAssetList = new Hashtable<String, HashMap<String, String>>();

	public Gson gson = new Gson();

	public static synchronized Hashtable<String, HashMap<String, String>> getSKUAssetList() {
		return skuAssetList;
	}

	public synchronized void addSKUAsset(JSONArray jsonArray)   {

		if (skuAssetList == null)
			skuAssetList = new Hashtable<String, HashMap<String, String>>();

		skuAssetList.clear();

		if (jsonArray != null) {
			for (int i = 0; i < jsonArray.length(); i++) {
				JSONObject jsonObject;
				try {
					jsonObject = jsonArray.getJSONObject(i);
					
					String upcCode = jsonObject.getString("upcCode");

					HashMap<String, String> map = gson.fromJson(jsonObject.toString(), HashMap.class);

					skuAssetList.put(upcCode.toLowerCase(), map);
				} catch (JSONException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}

				
			}
		}

	}

	public synchronized Hashtable<String, HashMap<String, String>> getSKUAssets() {

		if (skuAssetList == null || skuAssetList.size() == 0) {
			addSKUAsset(readSKUAssets());
		}

		return skuAssetList;

	}

	public HashMap<String, String> getSKUDetails(String upcCode) {
		if (skuAssetList == null || skuAssetList.size() == 0) {
			addSKUAsset(readSKUAssets());
		}

		if (skuAssetList != null && skuAssetList.containsKey(upcCode.toLowerCase())) {
			return skuAssetList.get(upcCode.toLowerCase());
		} else
			return null;
	}

	public JSONArray readSKUAssets() {

		String dir = Utils.getServerPath();
		JSONArray jsonArray = null;
		File f1 = new File(dir + "/sku/" + "sku.json");
		if (f1.exists()) {
			Path path = Paths.get(f1.getAbsolutePath());
			String data = null;
			try {
				data = new String(Files.readAllBytes(path));
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

			if (data != null) {

				try {
					jsonArray = new JSONArray(data);
				} catch (JSONException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		}
		return jsonArray;
	}
}
