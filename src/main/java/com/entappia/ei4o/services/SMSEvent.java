package com.entappia.ei4o.services;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.concurrent.CompletableFuture;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;
import org.springframework.web.client.ResourceAccessException;
import org.springframework.web.client.RestTemplate;

import com.entappia.ei4o.constants.AppConstants;
import com.entappia.ei4o.utils.RestErrorHandler;
import com.entappia.ei4o.utils.SmsUtil;

@Service
public class SMSEvent {

	public static String SMS_URL = "https://api.msg91.com/api/v5/flow";
	public static String SMS_AUTHKEY = "352172AUBKftjitxg86006943cP1";
	public static String SMS_SENDER_ID = "IDSEIT";

	public static String OTP_FLOW_ID = "61792c5990a59e1a2932cb93";
	public static String OCC_FLOW_ID = "61739aa275d79e65530e4192";
	public static String LOST_TAG_FLOW_ID = "61c01a9785aaf3508973c5fd";
	public static String LOST_LOCATOR_FLOW_ID = "6178ebcf714fd7087610ffb2";
	public static String TAG_STATUS_FLOW_ID = "617ba78700c1c368bb1000e1";
	public static String LOST_TAG_RETURN_FLOW_ID = "617ba7d971682818ae02ac42";
	public static String ASSET_EXPIRY_FLOW_ID = "617ba6cc2135dc22ad1ada54";

	public static String ZONE_ALERT_FLOW_ID = "61c019f4b3e7de5bb2486bb5";

	/* Send zone name and occupancy level to all registered mobile numbers */
	@Async
	public CompletableFuture<JSONObject> sentOTP(String name, String emailId, String mobileNos, String otp, String time)
			throws InterruptedException {

		// String response = getResourceFileAsString("project_info.json");

		String url = SMS_URL;

		RestTemplate restTemplate = new RestTemplate();
		restTemplate.setErrorHandler(new RestErrorHandler());

		// create headers
		HttpHeaders headers = new HttpHeaders();
		headers.setContentType(MediaType.APPLICATION_JSON);
		headers.add("authkey", SMS_AUTHKEY);

		// setting up the request body
		HashMap<String, String> params = new HashMap<>();
		params.put("flow_id", OTP_FLOW_ID);
		params.put("sender", SMS_SENDER_ID);
		params.put("mobiles", mobileNos);
		params.put("otpno", otp);
		params.put("time", time);
		params.put("minutes", "30 mins");

		// request entity is created with request body and headers
		HttpEntity<HashMap<String, String>> requestEntity = new HttpEntity<>(params, headers);

		JSONObject jsonObject = new JSONObject();

		try {
			// make an HTTP GET request with headers
			ResponseEntity<String> response = restTemplate.exchange(url, HttpMethod.POST, requestEntity, String.class);

			if (response.getStatusCodeValue() == 200) {
				jsonObject = new JSONObject(response.getBody());
				SmsUtil.sendMail(name, otp, emailId);
			}
		} catch (ResourceAccessException e) {

		}
		return CompletableFuture.completedFuture(jsonObject);
	}

	/* Send zone name and occupancy level to all registered mobile numbers */
	@Async
	public CompletableFuture<JSONObject> sentOccupancyAlert(ArrayList<String> mobileNos, String zoneData)
			throws InterruptedException {

		// String response = getResourceFileAsString("project_info.json");

		String url = SMS_URL;

		RestTemplate restTemplate = new RestTemplate();
		restTemplate.setErrorHandler(new RestErrorHandler());

		// create headers
		HttpHeaders headers = new HttpHeaders();
		headers.setContentType(MediaType.APPLICATION_JSON);
		headers.add("authkey", SMS_AUTHKEY);

		// setting up the request body
		HashMap<String, Object> params = new HashMap<>();
		params.put("flow_id", OCC_FLOW_ID);
		params.put("sender", SMS_SENDER_ID);

		List<HashMap<String, String>> recipientsList = new ArrayList<>();
		mobileNos.forEach(mobileNo -> {

			HashMap<String, String> map = new HashMap<>();
			map.put("mobiles", mobileNo);
			map.put("Zone", zoneData);
			map.put("Percentage", "");
			recipientsList.add(map);
		});

		params.put("recipients", recipientsList);

		// request entity is created with request body and headers
		HttpEntity<HashMap<String, Object>> requestEntity = new HttpEntity<>(params, headers);

		JSONObject jsonObject = new JSONObject();

		try {
			// make an HTTP GET request with headers
			ResponseEntity<String> response = restTemplate.exchange(url, HttpMethod.POST, requestEntity, String.class);

			if (response.getStatusCodeValue() == 200) {
				jsonObject = new JSONObject(response.getBody());
			}
		} catch (ResourceAccessException e) {

		}
		return CompletableFuture.completedFuture(jsonObject);
	}

	/* Send tag id and lost tag found time to all registered mobile numbers */
	@Async
	public CompletableFuture<JSONObject> sentLostTagAlert(ArrayList<String> mobileNos, String lostTagData,
			String lostTagData1) throws InterruptedException {

		// String response = getResourceFileAsString("project_info.json");

		String url = SMS_URL;

		RestTemplate restTemplate = new RestTemplate();
		restTemplate.setErrorHandler(new RestErrorHandler());

		// create headers
		HttpHeaders headers = new HttpHeaders();
		headers.setContentType(MediaType.APPLICATION_JSON);
		headers.add("authkey", SMS_AUTHKEY);

		// setting up the request body
		HashMap<String, Object> params = new HashMap<>();
		params.put("flow_id", LOST_TAG_FLOW_ID);
		params.put("sender", SMS_SENDER_ID);

		List<HashMap<String, String>> recipientsList = new ArrayList<>();
		mobileNos.forEach(mobileNo -> {

			HashMap<String, String> map = new HashMap<>();
			map.put("mobiles", mobileNo);
			map.put("tagID1", lostTagData);
			map.put("tagID2", lostTagData1);
			recipientsList.add(map);
		});

		params.put("recipients", recipientsList);

		// request entity is created with request body and headers
		HttpEntity<HashMap<String, Object>> requestEntity = new HttpEntity<>(params, headers);

		JSONObject jsonObject = new JSONObject();

		try {
			// make an HTTP GET request with headers
			ResponseEntity<String> response = restTemplate.exchange(url, HttpMethod.POST, requestEntity, String.class);

			if (response.getStatusCodeValue() == 200) {
				jsonObject = new JSONObject(response.getBody());
			}
		} catch (ResourceAccessException e) {

		}
		return CompletableFuture.completedFuture(jsonObject);
	}

	/* Send list of lost Locator names to all registered mobile numbers */
	@Async
	public CompletableFuture<JSONObject> sentLostLocatorAlert(ArrayList<String> mobileNos, String locatorNames)
			throws InterruptedException {

		// String response = getResourceFileAsString("project_info.json");

		String url = SMS_URL;

		RestTemplate restTemplate = new RestTemplate();
		restTemplate.setErrorHandler(new RestErrorHandler());

		// create headers
		HttpHeaders headers = new HttpHeaders();
		headers.setContentType(MediaType.APPLICATION_JSON);
		headers.add("authkey", SMS_AUTHKEY);

		// setting up the request body
		HashMap<String, Object> params = new HashMap<>();
		params.put("flow_id", LOST_LOCATOR_FLOW_ID);
		params.put("sender", SMS_SENDER_ID);

		List<HashMap<String, String>> recipientsList = new ArrayList<>();
		mobileNos.forEach(mobileNo -> {

			HashMap<String, String> map = new HashMap<>();
			map.put("mobiles", mobileNo);
			map.put("locatorNames", locatorNames);
			recipientsList.add(map);
		});

		params.put("recipients", recipientsList);

		// request entity is created with request body and headers
		HttpEntity<HashMap<String, Object>> requestEntity = new HttpEntity<>(params, headers);

		JSONObject jsonObject = new JSONObject();

		try {
			// make an HTTP GET request with headers
			ResponseEntity<String> response = restTemplate.exchange(url, HttpMethod.POST, requestEntity, String.class);

			if (response.getStatusCodeValue() == 200) {
				jsonObject = new JSONObject(response.getBody());
			}
		} catch (ResourceAccessException e) {

		}
		return CompletableFuture.completedFuture(jsonObject);
	}

	/*
	 * Send list of Blocked/Lost/Late Visitor/Restricted Zone Visitor Tags to all
	 * registered mobile numbers
	 */
	@Async
	public CompletableFuture<JSONObject> sendTagStatusAlert(ArrayList<String> mobileNos, String tagStatus,
			String tagData) throws InterruptedException {

		// String response = getResourceFileAsString("project_info.json");

		String url = SMS_URL;

		RestTemplate restTemplate = new RestTemplate();
		restTemplate.setErrorHandler(new RestErrorHandler());

		// create headers
		HttpHeaders headers = new HttpHeaders();
		headers.setContentType(MediaType.APPLICATION_JSON);
		headers.add("authkey", SMS_AUTHKEY);

		// setting up the request body
		HashMap<String, Object> params = new HashMap<>();
		params.put("flow_id", TAG_STATUS_FLOW_ID);
		params.put("sender", SMS_SENDER_ID);

		List<HashMap<String, String>> recipientsList = new ArrayList<>();
		mobileNos.forEach(mobileNo -> {

			HashMap<String, String> map = new HashMap<>();
			map.put("mobiles", mobileNo);
			map.put("tagStatus", tagStatus);
			map.put("tagData", tagData);
			recipientsList.add(map);
		});

		params.put("recipients", recipientsList);

		// request entity is created with request body and headers
		HttpEntity<HashMap<String, Object>> requestEntity = new HttpEntity<>(params, headers);

		JSONObject jsonObject = new JSONObject();

		try {
			// make an HTTP GET request with headers
			ResponseEntity<String> response = restTemplate.exchange(url, HttpMethod.POST, requestEntity, String.class);

			if (response.getStatusCodeValue() == 200) {
				jsonObject = new JSONObject(response.getBody());
			}
		} catch (ResourceAccessException e) {

		}
		return CompletableFuture.completedFuture(jsonObject);
	}

	/*
	 * Send list of Shipping Area/Restricted Zone Visitor Tags to all registered
	 * mobile numbers
	 */
	@Async
	public CompletableFuture<JSONObject> sendZoneAlert(ArrayList<String> mobileNos, String userType, String zoneType,
			String tagIds) throws InterruptedException {

		// String response = getResourceFileAsString("project_info.json");

		String url = SMS_URL;

		RestTemplate restTemplate = new RestTemplate();
		restTemplate.setErrorHandler(new RestErrorHandler());

		// create headers
		HttpHeaders headers = new HttpHeaders();
		headers.setContentType(MediaType.APPLICATION_JSON);
		headers.add("authkey", SMS_AUTHKEY);

		// setting up the request body
		HashMap<String, Object> params = new HashMap<>();
		params.put("flow_id", ZONE_ALERT_FLOW_ID);
		params.put("sender", SMS_SENDER_ID);

		List<HashMap<String, String>> recipientsList = new ArrayList<>();
		mobileNos.forEach(mobileNo -> {

			HashMap<String, String> map = new HashMap<>();
			map.put("mobiles", mobileNo);
			map.put("userType", userType);
			map.put("zoneType", zoneType);
			map.put("tagIDs", tagIds);
			recipientsList.add(map);
		});

		params.put("recipients", recipientsList);

		// request entity is created with request body and headers
		HttpEntity<HashMap<String, Object>> requestEntity = new HttpEntity<>(params, headers);

		JSONObject jsonObject = new JSONObject();

		try {
			// make an HTTP GET request with headers
			ResponseEntity<String> response = restTemplate.exchange(url, HttpMethod.POST, requestEntity, String.class);

			if (response.getStatusCodeValue() == 200) {
				jsonObject = new JSONObject(response.getBody());
			}
		} catch (ResourceAccessException e) {

		}
		return CompletableFuture.completedFuture(jsonObject);
	}

	/* Send list of Lost Tag Status Returned to all registered mobile numbers */
	@Async
	public CompletableFuture<JSONObject> sendLostTagStatusReturnAlert(ArrayList<String> mobileNos, String tagData)
			throws InterruptedException {

		// String response = getResourceFileAsString("project_info.json");

		String url = SMS_URL;

		RestTemplate restTemplate = new RestTemplate();
		restTemplate.setErrorHandler(new RestErrorHandler());

		// create headers
		HttpHeaders headers = new HttpHeaders();
		headers.setContentType(MediaType.APPLICATION_JSON);
		headers.add("authkey", SMS_AUTHKEY);

		// setting up the request body
		HashMap<String, Object> params = new HashMap<>();
		params.put("flow_id", LOST_TAG_RETURN_FLOW_ID);
		params.put("sender", SMS_SENDER_ID);

		List<HashMap<String, String>> recipientsList = new ArrayList<>();
		mobileNos.forEach(mobileNo -> {

			HashMap<String, String> map = new HashMap<>();
			map.put("mobiles", mobileNo);
			map.put("tagData", tagData);
			recipientsList.add(map);
		});

		params.put("recipients", recipientsList);

		// request entity is created with request body and headers
		HttpEntity<HashMap<String, Object>> requestEntity = new HttpEntity<>(params, headers);

		JSONObject jsonObject = new JSONObject();

		try {
			// make an HTTP GET request with headers
			ResponseEntity<String> response = restTemplate.exchange(url, HttpMethod.POST, requestEntity, String.class);

			if (response.getStatusCodeValue() == 200) {
				jsonObject = new JSONObject(response.getBody());
			}
		} catch (ResourceAccessException e) {

		}
		return CompletableFuture.completedFuture(jsonObject);
	}

	/* Send list of assets tag expires details to all registered mobile numbers */
	@Async
	public CompletableFuture<JSONObject> sendAssetExpiryAlert(ArrayList<String> mobileNos, String day, String assetType,
			String tagData) throws InterruptedException {

		// String response = getResourceFileAsString("project_info.json");

		RestTemplate restTemplate = new RestTemplate();
		restTemplate.setErrorHandler(new RestErrorHandler());

		// create headers
		HttpHeaders headers = new HttpHeaders();
		headers.setContentType(MediaType.APPLICATION_JSON);
		headers.add("authkey", SMS_AUTHKEY);

		// setting up the request body
		HashMap<String, Object> params = new HashMap<>();
		params.put("flow_id", ASSET_EXPIRY_FLOW_ID);
		params.put("sender", SMS_SENDER_ID);

		List<HashMap<String, String>> recipientsList = new ArrayList<>();
		mobileNos.forEach(mobileNo -> {

			HashMap<String, String> map = new HashMap<>();
			map.put("mobiles", mobileNo);
			map.put("asset", day);
			map.put("detail", assetType);
			map.put("ids", tagData);
			recipientsList.add(map);
		});

		params.put("recipients", recipientsList);

		// request entity is created with request body and headers
		HttpEntity<HashMap<String, Object>> requestEntity = new HttpEntity<>(params, headers);

		JSONObject jsonObject = new JSONObject();

		try {
			// make an HTTP GET request with headers
			ResponseEntity<String> response = restTemplate.exchange(SMS_URL, HttpMethod.POST, requestEntity,
					String.class);

			if (response.getStatusCodeValue() == 200) {
				jsonObject = new JSONObject(response.getBody());
			}
		} catch (ResourceAccessException e) {

		}
		return CompletableFuture.completedFuture(jsonObject);
	}

}
