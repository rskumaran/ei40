package com.entappia.ei4o.services;

import java.text.DecimalFormat;
import java.util.HashMap;
import java.util.ResourceBundle;
import java.util.concurrent.CompletableFuture;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;
import org.springframework.web.client.ResourceAccessException;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.reactive.function.client.WebClient;

import com.entappia.ei4o.constants.AppConstants;
import com.entappia.ei4o.models.QuuppaSendAlertResponse;
import com.entappia.ei4o.utils.RestErrorHandler;

import reactor.core.publisher.Flux;

@Service
public class QuuppaAPIService {

	@Async
	public CompletableFuture<JSONObject> getLocatorInfo() throws InterruptedException {

		// String response = getResourceFileAsString("project_info.json");

		String url = getQuuppaURL(AppConstants.GET_PROJECT_INFO);

		RestTemplate restTemplate = new RestTemplate();
		restTemplate.setErrorHandler(new RestErrorHandler());

		// create headers
		HttpHeaders headers = new HttpHeaders();
		headers.setContentType(MediaType.APPLICATION_JSON);
		// build the request
		@SuppressWarnings("rawtypes")
		HttpEntity request = new HttpEntity(headers);
		JSONObject locatorInfoJsonObject = new JSONObject();

		try {
			// make an HTTP GET request with headers
			ResponseEntity<String> response = restTemplate.exchange(url, HttpMethod.GET, request, String.class);

			if (response.getStatusCodeValue() == 200) {

				String projectInfo = response.getBody();
				JSONObject json = new JSONObject();

				try {
					json = new JSONObject(projectInfo);

					if (json != null) {

						int code = json.getInt("code");
						if (code == 0) {

							JSONArray jsonArray = json.getJSONArray("coordinateSystems");
							if (jsonArray != null && jsonArray.length() > 0) {

								JSONObject json1 = jsonArray.getJSONObject(0);
								if (json1 != null) {

									JSONArray locators = new JSONArray();

									JSONArray locatorsArray = json1.getJSONArray("locators");
									if (locatorsArray != null && locatorsArray.length() > 0) {

										for (int i = 0; i < locatorsArray.length(); i++) {
											JSONObject jsonObject = new JSONObject();
											jsonObject.put("id", locatorsArray.getJSONObject(i).getString("id"));
											jsonObject.put("name", locatorsArray.getJSONObject(i).getString("name"));
											jsonObject.put("locatorType",
													locatorsArray.getJSONObject(i).getString("locatorType"));
											jsonObject.put("x",
													locatorsArray.getJSONObject(i).getJSONArray("location").get(0));
											jsonObject.put("y",
													locatorsArray.getJSONObject(i).getJSONArray("location").get(1));
											locators.put(jsonObject);
										}

										locatorInfoJsonObject.put("locators", locators);

									}

									locatorInfoJsonObject.put("status", "success");
								}
							}
						} else {
							String message = json.getString("message");
							locatorInfoJsonObject.put("status", "error");
							locatorInfoJsonObject.put("message", message);
						}
					}

				} catch (JSONException e) {
					try {
						locatorInfoJsonObject.put("status", "error");
						locatorInfoJsonObject.put("message", e.getMessage());
					} catch (JSONException e1) {
						// TODO Auto-generated catch block
						e1.printStackTrace();
					}
					// TODO Auto-generated catch block
					e.printStackTrace();

				}
			} else {

				try {
					locatorInfoJsonObject.put("status", "error");
					locatorInfoJsonObject.put("message",
							"Status Code:" + response.getStatusCodeValue() + ", Connection Failed");
				} catch (JSONException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}

			}
		} catch (ResourceAccessException e) {
			try {
				locatorInfoJsonObject.put("status", "error");
				locatorInfoJsonObject.put("message", e.getMessage());
			} catch (JSONException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
		}
		return CompletableFuture.completedFuture(locatorInfoJsonObject);
	}

	@Async
	public CompletableFuture<HashMap<String, String>> getLocatorStatusInfo() throws InterruptedException {

		// String response = getResourceFileAsString("project_info.json");

		String url = getQuuppaURL(AppConstants.GET_LOCATOR_INFO);

		RestTemplate restTemplate = new RestTemplate();
		restTemplate.setErrorHandler(new RestErrorHandler());

		// create headers
		HttpHeaders headers = new HttpHeaders();
		headers.setContentType(MediaType.APPLICATION_JSON);
		// build the request
		@SuppressWarnings("rawtypes")
		HttpEntity request = new HttpEntity(headers);
		HashMap<String, String> locatorStatus = new HashMap<String, String>();

		try {
			// make an HTTP GET request with headers
			ResponseEntity<String> response = restTemplate.exchange(url, HttpMethod.GET, request, String.class);

			if (response.getStatusCodeValue() == 200) {

				String projectInfo = response.getBody();
				JSONObject json = new JSONObject();

				try {
					json = new JSONObject(projectInfo);

					if (json != null) {

						int code = json.getInt("code");
						if (code == 0) {

							JSONArray jsonArray = json.getJSONArray("locators");
							if (jsonArray != null && jsonArray.length() > 0) {

								JSONArray locatorsArray = json.getJSONArray("locators");
								if (locatorsArray != null && locatorsArray.length() > 0) {

									for (int i = 0; i < locatorsArray.length(); i++) {
										locatorStatus.put(locatorsArray.getJSONObject(i).getString("id"),
												locatorsArray.getJSONObject(i).getString("connection"));
									}
								}
							}
						}
					}

				} catch (JSONException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();

				}
			}
		} catch (ResourceAccessException e) {
			e.printStackTrace();
		}
		return CompletableFuture.completedFuture(locatorStatus);
	}

	@Async
	public CompletableFuture<HashMap<String, HashMap<String, Object>>> getBatteryInfo() throws InterruptedException {

		// String response = getResourceFileAsString("project_info.json");

		String url = getQuuppaURL(AppConstants.GET_BATTERY_INFO);

		RestTemplate restTemplate = new RestTemplate();
		restTemplate.setErrorHandler(new RestErrorHandler());

		// create headers
		HttpHeaders headers = new HttpHeaders();
		headers.setContentType(MediaType.APPLICATION_JSON);
		// build the request
		@SuppressWarnings("rawtypes")
		HttpEntity request = new HttpEntity(headers);
		HashMap<String, HashMap<String, Object>> tagBatteryStatus = new HashMap<String, HashMap<String, Object>>();

		try {
			// make an HTTP GET request with headers
			ResponseEntity<String> response = restTemplate.exchange(url, HttpMethod.GET, request, String.class);

			if (response.getStatusCodeValue() == 200) {

				String projectInfo = response.getBody();
				JSONObject json = new JSONObject();

				try {
					json = new JSONObject(projectInfo);

					if (json != null) {

						int code = json.getInt("code");
						if (code == 0) {

							JSONArray jsonArray = json.getJSONArray("tags");
							if (jsonArray != null && jsonArray.length() > 0) {

								if (jsonArray != null && jsonArray.length() > 0) {

									for (int i = 0; i < jsonArray.length(); i++) {
										JSONObject jsonObject = jsonArray.getJSONObject(i);
										String macId = jsonObject.getString("tagId"); 
										String batteryAlarm = jsonObject.optString("batteryAlarm", "");
										Double batteryVoltage = jsonObject.optDouble("batteryVoltage", 0.0);
										
										DecimalFormat df = new DecimalFormat("0.00");

										HashMap<String, Object> map = new HashMap<>();
										map.put("macId", macId);
										map.put("batteryAlarm", batteryAlarm);
										map.put("batteryVoltage", df.format(batteryVoltage));
										if (batteryVoltage <= 1.00) {
											map.put("replaceStatus", "Yes");
										}
										else {
											map.put("replaceStatus", "No");
										}
										tagBatteryStatus.put(macId, map);
									}
								}
							}
						}
					}

				} catch (JSONException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();

				}
			}
		} catch (ResourceAccessException e) {
			e.printStackTrace();
		}
		return CompletableFuture.completedFuture(tagBatteryStatus);
	}

	@Async
	public CompletableFuture<JSONObject> getQPEInfo() throws InterruptedException, JSONException {

		String url = getQuuppaURL(AppConstants.GET_QPE_INFO);

		RestTemplate restTemplate = new RestTemplate();
		restTemplate.setErrorHandler(new RestErrorHandler());

		// create headers
		HttpHeaders headers = new HttpHeaders();
		headers.setContentType(MediaType.APPLICATION_JSON);
		// build the request
		@SuppressWarnings("rawtypes")
		HttpEntity request = new HttpEntity(headers);

		JSONObject jsonObject = new JSONObject();

		try {
			// make an HTTP GET request with headers
			ResponseEntity<String> response = restTemplate.exchange(url, HttpMethod.GET, request, String.class);

			String licenseKey = null;

			if (response.getStatusCodeValue() == 200) {

				String projectInfo = response.getBody();
				JSONObject json = new JSONObject();

				try {
					json = new JSONObject(projectInfo);
					if (json != null) {
						int code = json.getInt("code");
						if (code == 0) {
							JSONObject peJsonObject = json.getJSONObject("positioningEngine");
							if (peJsonObject != null) { 
								licenseKey = peJsonObject.optString("licenseKey", "");
								jsonObject.put("licenseKey", licenseKey);
								jsonObject.put("memoryFree", peJsonObject.optLong("memoryFree", 0));
								jsonObject.put("memoryMax", peJsonObject.optLong("memoryMax", 0));
								jsonObject.put("memoryUsed", peJsonObject.optLong("memoryUsed", 0));
								  
								jsonObject.put("memoryAllocated", peJsonObject.optLong("memoryAllocated", 0));
								jsonObject.put("diskFree", peJsonObject.optLong("diskFree", 0));
								jsonObject.put("cpuLoad", peJsonObject.optDouble("cpuLoad", 0));
								
								jsonObject.put("qpeVersion", peJsonObject.optString("qpeVersion", ""));
								jsonObject.put("status", "success");
							}
						} else {
							String message = json.getString("message");
							jsonObject.put("status", "error");
							jsonObject.put("message", message);
						}

					}

				} catch (JSONException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			} else {
				jsonObject.put("status", "error");
				jsonObject.put("type", "Connection");
				jsonObject.put("message", "Status Code:" + response.getStatusCodeValue() + ", Connection Failed");

			}
		} catch (ResourceAccessException e) {
			jsonObject.put("status", "error");
			jsonObject.put("type", "Connection");
			jsonObject.put("message", e.getMessage());
		}
		return CompletableFuture.completedFuture(jsonObject);
	}
	
	@Async
	public boolean sentBuzzerAlert(String macIds) throws InterruptedException {

		ResourceBundle bundle = ResourceBundle.getBundle("application");
		WebClient client = WebClient.create(bundle.getString("QUUPPA_HOST_NAME"));

		String url = String.format(AppConstants.SEND_BUZZER_ALERT, macIds);
		Flux<QuuppaSendAlertResponse> employeeFlux = client.get().uri(url).retrieve()
				.bodyToFlux(QuuppaSendAlertResponse.class);

		employeeFlux.subscribe(quuppaLocatorResponse -> {
			System.out.println(quuppaLocatorResponse.getMessage());
		});
		employeeFlux.doOnError(error -> {
			System.out.println(error.getLocalizedMessage());
		});
		return true;
	}

	public String getQuuppaURL(String url) {
		ResourceBundle bundle = ResourceBundle.getBundle("application");
		return bundle.getString("QUUPPA_HOST_NAME") + url;
	}

}
