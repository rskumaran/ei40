package com.entappia.ei4o.services;

import java.util.HashMap;
import java.util.Hashtable;
import java.util.List;

import org.json.JSONException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.ResourceLoader;
import org.springframework.stereotype.Service;

import com.entappia.ei4o.utils.Utils;
import com.google.gson.Gson;

@Service
public class WarehouseEvent {

	public static volatile Hashtable<String, HashMap<String, String>> warehouseAssetList = new Hashtable<String, HashMap<String, String>>();

	public Gson gson = new Gson();

	@Autowired
	private ResourceLoader resourceLoader;

	public static synchronized Hashtable<String, HashMap<String, String>> getSKUAssetList() {
		return warehouseAssetList;
	}

	public synchronized void readWarehouseAsset() {

		if (warehouseAssetList == null)
			warehouseAssetList = new Hashtable<String, HashMap<String, String>>();

		warehouseAssetList.clear();

		List<HashMap<String, String>> warehouseList = Utils.readWarehouse(resourceLoader);

		if (warehouseList != null) {
			for (int i = 0; i < warehouseList.size(); i++) {
				try {

					String skuNo = warehouseList.get(i).getOrDefault("SKU No", "");

					if(!Utils.isEmptyString(skuNo))
						warehouseAssetList.put(skuNo.toLowerCase(), warehouseList.get(i));
				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}

			}
		}

	}

	public synchronized Hashtable<String, HashMap<String, String>> getWarehouseAssets() {

		if (warehouseAssetList == null || warehouseAssetList.size() == 0) {
			readWarehouseAsset();
		}

		return warehouseAssetList;

	}

	public HashMap<String, String> getWarehouseDetails(String skuNo) {
		if (warehouseAssetList == null || warehouseAssetList.size() == 0) {
			readWarehouseAsset();
		}

		if (warehouseAssetList != null && warehouseAssetList.containsKey(skuNo.toLowerCase())) {
			return warehouseAssetList.get(skuNo.toLowerCase());
		} else
			return null;
	}

}
