package com.entappia.ei4o.models;

import java.util.ArrayList;
import java.util.List;

public class WorkStations {
	private List<WorkStationAnalytics> workStationAnalyticsList; 
	private String workStationid;
	private String workStationName;

	public String getWorkStationName() {
		return workStationName;
	}
	public void setWorkStationName(String workStationName) {
		this.workStationName = workStationName;
	}
	public WorkStations() {
		workStationAnalyticsList = new ArrayList<WorkStationAnalytics>();
	}
	public List<WorkStationAnalytics> getWorkStationAnalytics(){
		return workStationAnalyticsList; 
	}
	public void setWorkStationAnalytics(List<WorkStationAnalytics> input){
		this.workStationAnalyticsList = input;
	}
	public String getWorkStationid(){
		return workStationid; 
	}
	public void setWorkStationid(String input){
		this.workStationid = input;
	}
	public void setWorkStationAnalytics(int incoming, int outgoing, int waiting, String date) {
		
		//Check here if already exists and add to list or append according to the requirement
		boolean workStationFoundInList =  false;
		for(int i = 0; i < workStationAnalyticsList.size();i++)
		{
			WorkStationAnalytics workStationAnalytics = workStationAnalyticsList.get(i);
		
			if(workStationAnalytics.getDate().equals(date)) {
				workStationFoundInList = true;
				workStationAnalytics.setIncoming(workStationAnalytics.getIncoming() + incoming);
				workStationAnalytics.setOutgoing(workStationAnalytics.getOutgoing() + outgoing);
				workStationAnalytics.setWaiting(workStationAnalytics.getWaiting() + waiting);
				
			}
		}
		if(workStationFoundInList == false)
		{
			WorkStationAnalytics workStationAnalytics = new WorkStationAnalytics();
			workStationAnalytics.setIncoming(incoming);
			workStationAnalytics.setOutgoing(outgoing);
			workStationAnalytics.setWaiting(waiting);
			workStationAnalytics.setDate(date);
			workStationAnalyticsList.add(workStationAnalytics);
		}

	}
	public void setWorkStationAnalytics(String stationId, int incoming, int outgoing, int waiting, String date, String stationName) {
		
		setWorkStationid(stationId);
		setWorkStationName(stationName);
		setWorkStationAnalytics(incoming, outgoing, waiting, date);
	}
}
