package com.entappia.ei4o.models;

public class Zones {
	
	  private int dWellTime; 
	  private String zoneID; 
	  private String zoneName; 

	  public int getDWellTime(){
	  	return dWellTime; 
	  }
	  public void setDWellTime(int dWellTime){
	  	 this.dWellTime = dWellTime;
	  }
	  public String getZoneID(){
	  	return zoneID; 
	  }
	  public void setZoneID(String input){
	  	 this.zoneID = input;
	  }
	  public String getZoneName(){
	  	return zoneName; 
	  }
	  public void setZoneName(String input){
	  	 this.zoneName = input;
	  }
}
