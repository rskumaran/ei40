package com.entappia.ei4o.models;

import java.util.ArrayList;
import java.util.Hashtable;

import com.google.gson.Gson;

public class NotificationsEvent {

	public static volatile Hashtable<String, ArrayList<String>> sessionIdNotificationList = new Hashtable<String, ArrayList<String>>();

	public static synchronized Hashtable<String, ArrayList<String>> getSessionIdList() {
		return sessionIdNotificationList;
	}

	public static synchronized void addSessionId(String emailId, String sessionId) {

		if (sessionIdNotificationList == null)
			sessionIdNotificationList = new Hashtable<String, ArrayList<String>>();

		if (NotificationsEvent.sessionIdNotificationList.contains(emailId)) {

			ArrayList<String> sessionIdList = NotificationsEvent.sessionIdNotificationList.get(emailId);
			if (sessionIdList == null)
				sessionIdList = new ArrayList<>();

			if (!sessionIdList.contains(sessionId)) {
				sessionIdList.add(sessionId);
				NotificationsEvent.sessionIdNotificationList.put(emailId, sessionIdList);
			}
		} else {
			ArrayList<String> sessionIdList = new ArrayList<>();
			sessionIdList.add(sessionId);

			NotificationsEvent.sessionIdNotificationList.put(emailId, sessionIdList);
		}

	}

	public static synchronized void removeSessionId(String emailId, String sessionId) {

		if (sessionIdNotificationList == null)
			sessionIdNotificationList = new Hashtable<String, ArrayList<String>>();

		NotificationsEvent.sessionIdNotificationList.remove(sessionId);

		if (NotificationsEvent.sessionIdNotificationList.contains(emailId)) {

			ArrayList<String> sessionIdList = NotificationsEvent.sessionIdNotificationList.get(emailId);
			if (sessionIdList == null)
				sessionIdList = new ArrayList<>();

			if (!sessionIdList.contains(sessionId)) {
				sessionIdList.remove(sessionId);

				if (sessionIdList.size() > 0)
					NotificationsEvent.sessionIdNotificationList.put(emailId, sessionIdList);
				else
					NotificationsEvent.sessionIdNotificationList.remove(emailId);
					
			}
		}
	}

}
