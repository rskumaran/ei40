package com.entappia.ei4o.models;

public class SocketInputData {
	
	String type;
	String viewType;
	
	/*
	 * String sessionId; String emailId;
	 */

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getViewType() {
		return viewType;
	}

	public void setViewType(String viewType) {
		this.viewType = viewType;
	}
	
	

	/*
	 * public String getSessionId() { return sessionId; }
	 * 
	 * public void setSessionId(String sessionId) { this.sessionId = sessionId; }
	 * 
	 * public String getEmailId() { return emailId; }
	 * 
	 * public void setEmailId(String emailId) { this.emailId = emailId; }
	 */

}
