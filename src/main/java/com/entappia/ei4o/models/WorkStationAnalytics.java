package com.entappia.ei4o.models;

public class WorkStationAnalytics {
	
	  private int incoming = 0; 
	  private int outgoing = 0; 
	  private int waiting = 0; 
	  private String date; 

	  public int getIncoming(){
	  	return incoming; 
	  }
	  public void setIncoming(int input){
	  	 this.incoming = input;
	  }
	  public int getOutgoing(){
	  	return outgoing; 
	  }
	  public void setOutgoing(int input){
	  	 this.outgoing = input;
	  }
	  public int getWaiting(){
	  	return waiting; 
	  }
	  public void setWaiting(int input){
	  	 this.waiting = input;
	  }
	  public String getDate(){
	  	return date; 
	  }
	  public void setDate(String input){
	  	 this.date = input;
	  }
}
