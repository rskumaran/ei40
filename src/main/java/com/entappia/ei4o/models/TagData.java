package com.entappia.ei4o.models;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

import org.json.JSONArray;

import com.entappia.ei4o.dbmodels.TagPosition;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

public class TagData {
	public static volatile ArrayList<TagPosition> tagDetails;
	public static long runTime;

	public static volatile ArrayList<String> sessionIdList = new ArrayList<>();

	static Gson gson = new Gson();
	static Type mapType = new TypeToken<List<TagPosition>>() {
	}.getType();

	public static synchronized int getTagDetailsSize() {
		return tagDetails == null ? 0 : tagDetails.size();
	}

	public static synchronized ArrayList<TagPosition> getTagDetails() {
		return tagDetails;
	}

	public static synchronized void setTagDetails(JSONArray jsonArray) {

		if (tagDetails == null)
			tagDetails = new ArrayList<TagPosition>();

		if (jsonArray != null) {

			List<TagPosition> tagPositions = gson.fromJson(jsonArray.toString(), mapType);
			if (tagPositions != null && tagPositions.size() > 0)
				tagDetails.addAll(tagPositions);

		}
		TagData.tagDetails = tagDetails;
	}

	public static synchronized void clearTagDetails() {
		if (tagDetails != null)
			tagDetails.clear();
		TagData.runTime = System.currentTimeMillis();
	}

	public static synchronized ArrayList<String> getSessionIdList() {
		return sessionIdList;
	}

	public static synchronized void addSessionId(String sessionId) {

		if (sessionIdList == null)
			sessionIdList = new ArrayList<>();

		if (!TagData.sessionIdList.contains(sessionId))
			TagData.sessionIdList.add(sessionId);
	}

	public static synchronized void removeSessionId(String sessionId) {

		if (sessionIdList == null)
			sessionIdList = new ArrayList<>();

		TagData.sessionIdList.remove(sessionId);
	}

}
