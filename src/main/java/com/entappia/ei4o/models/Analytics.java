package com.entappia.ei4o.models;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class Analytics {
	
	private String date; 
	private List<DwellTime> dwellTime; 

	
	public Analytics() {
		dwellTime = new ArrayList<DwellTime>();
	}
	public String getDate(){
	  	return date; 
	  }
	  public void setDate(String input){
	  	 this.date = input;
	  }
	public List<DwellTime> getDwellTime() {
		return dwellTime;
	}
	/*public void setDwellTime(List<DwellTime> dwellTime) {
		this.dwellTime = dwellTime;
	}*/
	public void setDwellTime(String userId, int idWellTime, String zoneID,String zoneName)
	{
		//Check here if already exists and add to list or append according to the requirement
		boolean dwellTimeForUserFoundInList =  false;
		for(int i = 0; i < dwellTime.size();i++)
		{
			DwellTime dwellTimeAddToList = dwellTime.get(i);
		
			if(dwellTimeAddToList.getId().equals(userId)) {
				dwellTimeForUserFoundInList = true;
				dwellTimeAddToList.setDwellTime(userId, idWellTime, zoneID, zoneName);
			}
			
		}
		if(dwellTimeForUserFoundInList == false)
		{
			DwellTime dwellTimeAddToList = new DwellTime();
			dwellTimeAddToList.setDwellTime(userId, idWellTime, zoneID, zoneName);
			dwellTime.add(dwellTimeAddToList);
		}

	}
	public boolean IsRecordAvailable() {
		if(dwellTime.size() > 0)
			return true;
		return false;
	
	}
}
