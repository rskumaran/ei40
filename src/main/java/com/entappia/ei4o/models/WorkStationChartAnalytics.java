package com.entappia.ei4o.models;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import com.entappia.ei4o.models.AgingAnalysis.AgingWorkStations;
import com.entappia.ei4o.models.ZoneFootFallAnalysis.FootFall.Zone.DWellTime;

public class WorkStationChartAnalytics {

	List<WorkStations> workStations = new ArrayList<WorkStations>();
	
	public class WorkStations{
		private String workStationID; 
		private String workStationName; 
		private HashMap<String, TQ> tQ; 
		
		public WorkStations() {
			tQ = new HashMap<String, TQ>();
		}

		private class TQ{
			private int wo;
			private int wi;
			private int pr;
			private int tot;

			public int getWo() {
				return wo;
			}
			public void setWo(int wo) {
				this.wo = wo;
			}
			public int getWi() {
				return wi;
			}
			public void setWi(int wi) {
				this.wi = wi;
			}
			public int getPr() {
				return pr;
			}
			public void setPr(int pr) {
				this.pr = pr;
			}
			public int getTot() {
				return tot;
			}
			public void setTot(int tot) {
				this.tot = tot;
			}
		}

		public String getWorkStationID() {
			return workStationID;
		}

		public void setWorkStationID(String workStationID) {
			this.workStationID = workStationID;
		}

		public String getWorkStationName() {
			return workStationName;
		}

		public void setWorkStationName(String workStationName) {
			this.workStationName = workStationName;
		}

		public HashMap<String, TQ> gettQ() {
			return tQ;
		}

		public void addWorkStationChartAnalytics(String stationId, int wi, int wo, int pr, int tot,
				String stationName, String timeQuarter) {
			setWorkStationID(stationId);
			setWorkStationName(stationName);
			if(tQ.get(timeQuarter) != null) {
				TQ tempTQ= tQ.get(timeQuarter);
				tempTQ.setPr(tempTQ.getPr() + pr);
				tempTQ.setWo(tempTQ.getWo() + wo);
				tempTQ.setWi(tempTQ.getWi() + wi);
				tempTQ.setTot(tempTQ.getTot() + tot);
			}else {
				TQ tempTQ= new TQ();
				tempTQ.setPr(pr);
				tempTQ.setWo(wo);
				tempTQ.setWi(wi);
				tempTQ.setTot(tot);
				tQ.put(timeQuarter, tempTQ);
			}
			
		}
	}

	public void setWorkStationChartData(String stationId, int wi, int wo, int pr, int tot, String stationName, String timeQuarter)
	{
		//Check here if already exists and add to list or append according to the requirement
		boolean workStationFoundInList =  false;
		WorkStations workStationAddToList = null;
		for(int i = 0; i < workStations.size();i++)
		{
			workStationAddToList = workStations.get(i);

			if(workStationAddToList.getWorkStationID().equals(stationId)) {
				workStationFoundInList = true;
				workStationAddToList.addWorkStationChartAnalytics(stationId, wi, wo, pr, tot,stationName, timeQuarter);
			}

		}
		if(workStationFoundInList == false)
		{
			workStationAddToList = new WorkStations();
			workStationAddToList.addWorkStationChartAnalytics(stationId, wi, wo, pr, tot,stationName, timeQuarter);
			workStations.add(workStationAddToList);
		}
	
	}

	public boolean IsRecordAvailable() {
		if(workStations.size() > 0)
			return true;
		return false;	
	}

	public List<WorkStations> getWorkStations() {
		return workStations;
	}



}
