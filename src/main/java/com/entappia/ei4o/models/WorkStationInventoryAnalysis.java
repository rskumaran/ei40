package com.entappia.ei4o.models;

import java.util.ArrayList;
import java.util.List;

public class WorkStationInventoryAnalysis {

	public WorkStationInventoryAnalysis() {
		workStations = new ArrayList<WorkStations>();
	}
	
	public class WorkStationInventory{
		  private Double amount; 
		  private String date; 

		  public Double getAmount(){
		  	return amount; 
		  }
		  public void setAmount(Double input){
		  	 this.amount = input;
		  }
		  public String getDate(){
		  	return date; 
		  }
		  public void setDate(String input){
		  	 this.date = input;
		  }
	}
	public class WorkStations{
		
		public WorkStations() {
			workStationInventoryList = new ArrayList<WorkStationInventory>();
		}
		  private String workStationid;
		  private String workStationName;
		  private Double totalInventoryAmount;
		  private List<WorkStationInventory> workStationInventoryList; 

		  
		public Double getTotalInventoryAmount() {
			totalInventoryAmount = 0.0;
			for(int i = 0; i < workStationInventoryList.size(); i++ ) {
				totalInventoryAmount +=  workStationInventoryList.get(i).getAmount();
			}
				
			return totalInventoryAmount;
		}
		public String getWorkStationName() {
			return workStationName;
		}
		public void setWorkStationName(String workStationName) {
			this.workStationName = workStationName;
		}
		public String getWorkStationid(){
		  	return workStationid; 
		  }
		  public void setWorkStationid(String input){
		  	 this.workStationid = input;
		  }
		  public List<WorkStationInventory> getWorkStationInventory(){
		  	return workStationInventoryList; 
		  }
		  public void setWorkStationInventory(List<WorkStationInventory> input){
		  	 this.workStationInventoryList = input;
		  }
		  public void setWorkStationAnalytics(Double amount, String date) {
				
				//Check here if already exists and add to list or append according to the requirement
				boolean workStationFoundInList =  false;
				for(int i = 0; i < workStationInventoryList.size();i++)
				{
					WorkStationInventory workStationInventory = workStationInventoryList.get(i);
				
					if(workStationInventory.getDate().equals(date)) {
						workStationFoundInList = true;
						workStationInventory.setAmount(workStationInventory.getAmount() + amount);
					}
				}
				if(workStationFoundInList == false)
				{
					WorkStationInventory workStationInventory = new WorkStationInventory();
					workStationInventory.setAmount(amount);
					workStationInventory.setDate(date);
					workStationInventoryList.add(workStationInventory);
				}

			}
			public void setWorkStationAnalytics(String stationId, String stationName, Double amount, String date) {
				
				setWorkStationid(stationId);
				setWorkStationName(stationName);
				setWorkStationAnalytics(amount, date);
			}
	}
	private String date; 
	  private List<WorkStations> workStations; 

	  public String getDate(){
	  	return date; 
	  }
	  public void setDate(String input){
	  	 this.date = input;
	  }
	  public List<WorkStations> getworkStations(){
	  	return workStations; 
	  }
	  public void setworkStations(List<WorkStations> input){
	  	 this.workStations = input;
	  }
	  public void setInventoryAmount(String stationId, String stationName, Double amount, String date)
		{
			//Check here if already exists and add to list or append according to the requirement
			boolean workStationFoundInList =  false;
			for(int i = 0; i < workStations.size();i++)
			{
				WorkStations workStationAddToList = workStations.get(i);
			
				if(workStationAddToList.getWorkStationid().equals(stationId)) {
					workStationFoundInList = true;
					
					workStationAddToList.setWorkStationAnalytics(amount, date);
				}
				
			}
			if(workStationFoundInList == false)
			{
				WorkStations workStationAddToList = new WorkStations();
				workStationAddToList.setWorkStationAnalytics(stationId, stationName, amount, date);
				workStations.add(workStationAddToList);
			}

		}
	public boolean IsRecordAvailable() {
		if(workStations.size() > 0)
			return true;
		return false;
		
	}
}
