package com.entappia.ei4o.models;

import java.io.Serializable;

public class Alert implements Serializable {
	
	int sms;
	int mail;
	int notification;
 
	public int getSms() {
		return sms;
	}

	public void setSms(int sms) {
		this.sms = sms;
	}
 
	public int getMail() {
		return mail;
	}

	public void setMail(int mail) {
		this.mail = mail;
	}

	public int getNotification() {
		return notification;
	}

	public void setNotification(int notification) {
		this.notification = notification;
	}

	
}
