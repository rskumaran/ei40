package com.entappia.ei4o.models;

public class QuuppaSendAlertResponse {

	int code;
	long responseTS; 
	String message;
	String status;
	String version;
	public int getCode() {
		return code;
	}
	public void setCode(int code) {
		this.code = code;
	}
	public long getResponseTS() {
		return responseTS;
	}
	public void setResponseTS(long responseTS) {
		this.responseTS = responseTS;
	}
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public String getVersion() {
		return version;
	}
	public void setVersion(String version) {
		this.version = version;
	}
	
}
