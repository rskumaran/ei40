package com.entappia.ei4o.models;

import java.util.ArrayList;
import java.util.List;

public class WorkStationFlowAnalytics {

	private String date; 
	private List<WorkStations> workStations; 

	public WorkStationFlowAnalytics() {
		workStations = new ArrayList<WorkStations>();
	}
	  public String getDate(){
	  	return date; 
	  }
	  public void setDate(String input){
	  	 this.date = input;
	  }
	  public List<WorkStations> getWorkStations(){
	  	return workStations; 
	  }
	  public void setWorkStations(List<WorkStations> input){
	  	 this.workStations = input;
	  }
	  public void setWorkOrderCount(String stationId, int incoming, int outgoing, int waiting, String date, String stationName)
		{
			//Check here if already exists and add to list or append according to the requirement
			boolean workStationFoundInList =  false;
			for(int i = 0; i < workStations.size();i++)
			{
				WorkStations workStationAddToList = workStations.get(i);
			
				if(workStationAddToList.getWorkStationid().equals(stationId)) {
					workStationFoundInList = true;
					
					workStationAddToList.setWorkStationAnalytics(incoming, outgoing, waiting, date);
				}
				
			}
			if(workStationFoundInList == false)
			{
				WorkStations workStationAddToList = new WorkStations();
				workStationAddToList.setWorkStationAnalytics(stationId, incoming, outgoing, waiting, date, stationName);
				workStations.add(workStationAddToList);
			}

		}
	public boolean IsRecordAvailable() {
		if(workStations.size() > 0)
			return true;
		return false;
		
	}
}
