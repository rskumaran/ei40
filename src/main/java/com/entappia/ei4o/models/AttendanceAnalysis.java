package com.entappia.ei4o.models;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;


public class AttendanceAnalysis{
	private List<Attendance> attendance; 

	public AttendanceAnalysis() {
		attendance = new ArrayList<Attendance>();
	}
	
	public List<Attendance> getAttendance(){
		return attendance; 
	}
	public void setAttendance(List<Attendance> input){
		this.attendance = input;
	}

	public class Attendance{
		private String date; 
		private HashMap<String, OrgAttendance> orgAttendance; 
		private List<Zone> zone; 

		
		public Attendance() {
			zone = new ArrayList<Zone>();
			orgAttendance = new HashMap<String, OrgAttendance>();
		}
		public void setTQ(String date, String zoneID, String zoneName, String tQ, int tQValue, int zoneRestrictions, String zoneClassification) {
			//Here get available zone with specified date if not create new
			//Check here if already exists and add to list or append according to the requirement
			this.date = date;
			boolean zoneFoundInList =  false;
			for(int i = 0; i < zone.size();i++)
			{
				Zone zoneAddToList = zone.get(i);
			
				if(zoneAddToList.getZoneID().equals(zoneID)) {
					zoneFoundInList = true;
					
					zoneAddToList.setTQ(zoneID, zoneName, tQ, tQValue, zoneRestrictions, zoneClassification);
				}
				
			}
			if(zoneFoundInList == false)
			{
				Zone zoneAddToList = new Zone();
				zoneAddToList.setTQ(zoneID, zoneName, tQ, tQValue, zoneRestrictions, zoneClassification);
				zone.add(zoneAddToList);
			}
		}
		public void setOrgAttendance(String date, String zoneId, String zoneName, Integer noOfPersons,
				Integer staffCount, Integer visitorCount, Integer vendorCount, Integer contractorCount, String tQ) {
			//Here get available tQ with specified tQ Number if not create new
			//Check here if already exists and add to list or append according to the requirement
			this.date = date;
			boolean tQFoundInList =  orgAttendance.containsKey(tQ);
			if(true == tQFoundInList)
			{
				OrgAttendance orgAttendanceObj = orgAttendance.get(tQ);
				orgAttendanceObj.setUserValues( noOfPersons, staffCount, visitorCount, vendorCount, contractorCount);
				orgAttendance.put(tQ, orgAttendanceObj);	
			}
			else
			{
				OrgAttendance orgAttendanceObj = new OrgAttendance();
				orgAttendanceObj.setUserValues( noOfPersons, staffCount, visitorCount, vendorCount, contractorCount);
				orgAttendance.put(tQ, orgAttendanceObj);	
			}
			
			
			
		}
		public String getDate(){
			return date; 
		}
		public void setDate(String input){
			this.date = input;
		}
		public  HashMap<String, OrgAttendance> getOrgAttendance() {
			return orgAttendance;
		}
		public void setOrgAttendance(HashMap<String, OrgAttendance> orgAttendance) {
			this.orgAttendance = orgAttendance;
		}
		public List<Zone> getZone(){
			return zone; 
		}
		public void setZone(List<Zone> input){
			this.zone = input;
		}
		public class OrgAttendance {

			HashMap<String, Integer> user;
			int tot = 0;
			
			public OrgAttendance() {
				user = new HashMap<String, Integer>();
			}
			
			public HashMap<String, Integer> getUser() {
				return user;
			}

			public void setUserValues(Integer noOfPersons, Integer staffCount, Integer visitorCount,
					Integer vendorCount, Integer contractorCount) {
				this.tot += noOfPersons;// we can do this in query for now it is kept in case zone wise details required
				if(staffCount > 0)
				{
					if(user.containsKey("emp")) {
						int tempStaffCount = user.get("emp");
						user.put("emp", staffCount + tempStaffCount);
					}
					else {
						user.put("emp", staffCount);
					}
				}
				if(visitorCount > 0) {
					if(user.containsKey("vis")) {
						int tempVisitorCount = user.get("vis");
						user.put("vis", visitorCount + tempVisitorCount);
					}
					else {
						user.put("vis", visitorCount);
					}
				}
				if(vendorCount > 0) {
					if(user.containsKey("ven")) {
						int tempVendorCount = user.get("ven");
						user.put("ven", vendorCount + tempVendorCount);
					}
					else {
						user.put("ven", vendorCount);
					}
				}
				if(contractorCount > 0) {
					if(user.containsKey("con")) {
						int tempContractorCount = user.get("con");
						user.put("con", contractorCount + tempContractorCount);
					}
					else {
						user.put("con", contractorCount);
					}
				}
				
			}

			public void setUser(HashMap<String, Integer> user) {
				this.user = user;
			}

			public int getTot() {
				return tot;
			}

			public void setTot(int tot) {
				this.tot = tot;
			}

		}
		
		public class Zone{
			private String zoneID; 
			private String zoneName; 
			private Integer zoneRestrictions; 
			private HashMap<String, Integer> tQ; 
			private String zoneClassification; 

			public Zone() {
				tQ = new HashMap<String, Integer>();
			}
			public HashMap<String, Integer> gettQ() {
				return tQ;
			}
			public void settQ(HashMap<String, Integer> tQ) {
				this.tQ = tQ;
			}
			public String getZoneID(){
				return zoneID; 
			}
			public void setZoneID(String input){
				this.zoneID = input;
			}
			public String getZoneName(){
				return zoneName; 
			}
			public void setZoneName(String input){
				this.zoneName = input;
			}
			public Integer getZoneRestrictions(){
				return zoneRestrictions; 
			}
			public void setZoneRestrictions(Integer input){
				this.zoneRestrictions = input;
			}
			public String getZoneClassification(){
				return zoneClassification; 
			}
			public void setZoneClassification(String input){
				this.zoneClassification = input;
			}
			public void setTQ(String zoneID, String zoneName,String tQ, int tQValue, int zoneRestrictions, String zoneClassification) {
				this.tQ.put(tQ, tQValue);
				this.zoneID = zoneID;
				this.zoneName = zoneName;
				this.zoneRestrictions = zoneRestrictions;
				this.zoneClassification = zoneClassification;
			}
		}

		
	}

	public boolean IsRecordAvailable() {
		if(attendance.size() > 0)
			return true;
		return false;
	}
	public void setTQ(String date, String zoneID, String zoneName, String tQ, int tQValue, int zoneRestrictions, String zoneClassification)
	{
		//Here get available attendance with specified date if not create new
		//Check here if already exists and add to list or append according to the requirement
		boolean attendanceDateFoundInList =  false;
		for(int i = 0; i < attendance.size();i++)
		{
			Attendance attendanceAddToList = attendance.get(i);
		
			if(attendanceAddToList.getDate().equals(date)) {
				attendanceDateFoundInList = true;
				
				attendanceAddToList.setTQ(date, zoneID, zoneName, tQ, tQValue, zoneRestrictions, zoneClassification);
			}
			
		}
		if(attendanceDateFoundInList == false)
		{
			Attendance attendanceAddToList = new Attendance();
			attendanceAddToList.setTQ(date, zoneID, zoneName, tQ, tQValue, zoneRestrictions, zoneClassification);
			attendance.add(attendanceAddToList);
		}

		
	}

	public void setOrgAttendance(String date, String zoneId, String zoneName, Integer noOfPersons, Integer staffCount,
			Integer visitorCount, Integer vendorCount, Integer contractorCount, String tQ) {
		
		//Here get available attendance with specified date if not create new
				//Check here if already exists and add to list or append according to the requirement
				boolean attendanceDateFoundInList =  false;
				for(int i = 0; i < attendance.size();i++)
				{
					Attendance attendanceAddToList = attendance.get(i);
				
					if(attendanceAddToList.getDate().equals(date)) {
						attendanceDateFoundInList = true;
						
						attendanceAddToList.setOrgAttendance(date, zoneId, zoneName, noOfPersons, staffCount,
								visitorCount, vendorCount, contractorCount, tQ);
					}
					
				}
				if(attendanceDateFoundInList == false)
				{
					Attendance attendanceAddToList = new Attendance();
					attendanceAddToList.setOrgAttendance(date, zoneId, zoneName, noOfPersons, staffCount,
							visitorCount, vendorCount, contractorCount, tQ);
					attendance.add(attendanceAddToList);
				}
		
		
	}
	
}
  
