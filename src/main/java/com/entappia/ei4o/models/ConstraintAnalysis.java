package com.entappia.ei4o.models;

import java.util.ArrayList;
import java.util.List;

public class ConstraintAnalysis {

	public ConstraintAnalysis() {
		constraintWorkStations = new ArrayList<ConstraintWorkStations>();
	}


	public class ConstraintWorkStations{
		private String workStationid;
		private String workStationName;
		private Double constraintValue; 

		public String getWorkStationName() {
			return workStationName;
		}
		public void setWorkStationName(String workStationName) {
			this.workStationName = workStationName;
		}
		public String getWorkStationid(){
			return workStationid; 
		}
		public void setWorkStationid(String input){
			this.workStationid = input;
		}
		public Double getConstraintValue(){
			return constraintValue; 
		}
		public void setConstraintValue(Double input){
			this.constraintValue = input;
		}
	}

	private List<ConstraintWorkStations> constraintWorkStations; 


	public List<ConstraintWorkStations> getWorkStations() {
		return constraintWorkStations;
	}



	public void addConstraint(String workStationId, String workStationName, Double constraintValue) {
		ConstraintWorkStations constraintWorkStation = new ConstraintWorkStations();
		constraintWorkStation.setWorkStationid(workStationId);
		constraintWorkStation.setWorkStationName(workStationName);
		constraintWorkStation.setConstraintValue(constraintValue);
		constraintWorkStations.add(constraintWorkStation);

	}

}
