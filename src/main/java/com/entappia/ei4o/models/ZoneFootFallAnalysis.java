package com.entappia.ei4o.models;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;


public class ZoneFootFallAnalysis{
	
	List<FootFall> footFall;
	
	public ZoneFootFallAnalysis() {
		footFall = new ArrayList<FootFall>();
	}

	public class FootFall{

		private String date; 
		private List<Zone> zone; 

		public FootFall() {
			zone = new ArrayList<Zone>();
		}
		
		public boolean IsRecordAvailable() {

			
			if(zone.size() > 0)
				return true;
			return false;
		}
		
		public String getDate(){
			return date; 
		}
		public void setDate(String input){
			this.date = input;
		}
		public List<Zone> getZone(){
			return zone; 
		}
		public void setZone(List<Zone> input){
			this.zone = input;
		}
		public void setTQ(String date, String zoneID, String zoneName, String tQ, int tQValue) {
			//Here get available zone with specified date if not create new
			//Check here if already exists and add to list or append according to the requirement
			this.date = date;
			boolean zoneFoundInList =  false;
			for(int i = 0; i < zone.size();i++)
			{
				Zone zoneAddToList = zone.get(i);
			
				if(zoneAddToList.getZoneID().equals(zoneID)) {
					zoneFoundInList = true;
					
					zoneAddToList.setTQ(zoneID, zoneName, tQ, tQValue);
				}
				
			}
			if(zoneFoundInList == false)
			{
				Zone zoneAddToList = new Zone();
				zoneAddToList.setTQ(zoneID, zoneName, tQ, tQValue);
				zone.add(zoneAddToList);
			}
		}
		public void setDwellTime(String date, String zoneID, double average, int total, int maximum) {
			//Here get available zone with specified date if not create new
			//Check here if already exists and add to list or append according to the requirement
			boolean zoneFoundInList =  false;
			for(int i = 0; i < zone.size();i++)
			{
				Zone zoneAddToList = zone.get(i);
			
				if(zoneAddToList.getZoneID().equals(zoneID)) {
					zoneFoundInList = true;
					
					zoneAddToList.setDwellTime(zoneID, average, total, maximum);
				}
				
			}
			if(zoneFoundInList == false)
			{
				Zone zoneAddToList = new Zone();
				zoneAddToList.setDwellTime(zoneID, average, total, maximum);
				zone.add(zoneAddToList);
			}

			
		}
		public class Zone{
			private DWellTime dWellTime; 
			private String zoneID; 
			private String zoneName; 
			private HashMap<String, Integer> tQ; 

			public Zone() {
				tQ = new HashMap<String, Integer>();
				dWellTime = new DWellTime();
			}
			
			public DWellTime getDWellTime(){
				return dWellTime; 
			}
			
			public void setTQ(String zoneID, String zoneName,String tQ, int tQValue) {
				this.tQ.put(tQ, tQValue);
				this.zoneID = zoneID;
				this.zoneName = zoneName;
			}
			public void setDwellTime(String zoneID, double average, int total, int maximum) {
				this.zoneID = zoneID;
				dWellTime.setAverage(average);
				dWellTime.setMaximum(maximum);
				dWellTime.setTotal(total);
			}
			public void setDWellTime(DWellTime input){
				this.dWellTime = input;
			}
			public String getZoneID(){
				return zoneID; 
			}
			public void setZoneID(String input){
				this.zoneID = input;
			}
			public String getZoneName(){
				return zoneName; 
			}
			public void setZoneName(String input){
				this.zoneName = input;
			}
			public HashMap<String, Integer> getTQ(){
				return tQ; 
			}
			public void setTQ(HashMap<String, Integer> input){
				this.tQ = input;
			}

			public class DWellTime{
				private double average; 
				private int total; 
				private int maximum; 

				public double getAverage(){
					return average; 
				}
				public void setAverage(double input){
					this.average = input;
				}
				public int getTotal(){
					return total; 
				}
				public void setTotal(int input){
					this.total = input;
				}
				public int getMaximum(){
					return maximum; 
				}
				public void setMaximum(int input){
					this.maximum = input;
				}
			}
		}

	}
	
	public void setDwellTime(String date, String zoneID, double average, int total, int maximum)
	{
		//Here get available footFall with specified date if not create new
		//Check here if already exists and add to list or append according to the requirement
		boolean footFallDateFoundInList =  false;
		for(int i = 0; i < footFall.size();i++)
		{
			FootFall footFallAddToList = footFall.get(i);
		
			if(footFallAddToList.getDate().equals(date)) {
				footFallDateFoundInList = true;
				
				footFallAddToList.setDwellTime(date, zoneID, average, total, maximum);
			}
			
		}
		if(footFallDateFoundInList == false)
		{
			FootFall footFallAddToList = new FootFall();
			footFallAddToList.setDwellTime(date, zoneID, average, total, maximum);
			footFall.add(footFallAddToList);
		}

		
	}
	
	public void setTQ(String date, String zoneID, String zoneName, String tQ, int tQValue)
	{
		//Here get available footFall with specified date if not create new
		//Check here if already exists and add to list or append according to the requirement
		boolean footFallDateFoundInList =  false;
		for(int i = 0; i < footFall.size();i++)
		{
			FootFall footFallAddToList = footFall.get(i);
		
			if(footFallAddToList.getDate().equals(date)) {
				footFallDateFoundInList = true;
				
				footFallAddToList.setTQ(date, zoneID, zoneName, tQ, tQValue);
			}
			
		}
		if(footFallDateFoundInList == false)
		{
			FootFall footFallAddToList = new FootFall();
			footFallAddToList.setTQ(date, zoneID, zoneName, tQ, tQValue);
			footFall.add(footFallAddToList);
		}

		
	}
	public boolean IsRecordAvailable() {
		if(footFall.size() > 0)
			return true;
		return false;
	}

	public List<FootFall> getFootFall() {
		return footFall;
	}

	public void setFootFall(List<FootFall> footFall) {
		this.footFall = footFall;
	}
	
}
  
