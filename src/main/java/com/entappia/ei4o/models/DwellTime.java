package com.entappia.ei4o.models;

import java.util.ArrayList;
import java.util.List;

public class DwellTime {
	private List<Zones> zones; 
	private String id; 

	public DwellTime() {
		zones = new ArrayList<Zones>();
	}
	  public List<Zones> getZones(){
	  	return zones; 
	  }
	  
	  /*public void setZones(List<Zones> input){
	  	 this.zones = input;
	  }*/
	  
	  public String getId(){
	  	return id; 
	  }
	  public void setId(String input){
	  	 this.id = input;
	  }

	public void setDwellTime(String id, int dWellTime, String zoneID, String zoneName) {
		//Check here if already exists and add to list or append according to the requirement
				boolean dwellTimeForUserZoneFoundInList =  false;
				for(int i = 0; i < zones.size();i++)
				{
					Zones zoneAddToList = zones.get(i);
				
					if(zoneAddToList.getZoneID().equals(zoneID) && zoneAddToList.getZoneName().equals(zoneName)) {
						dwellTimeForUserZoneFoundInList = true;
						zoneAddToList.setDWellTime(zoneAddToList.getDWellTime() + dWellTime);
						zoneAddToList.setZoneID(zoneID);
						zoneAddToList.setZoneName(zoneName);
					}
					
				}
				if(dwellTimeForUserZoneFoundInList == false)
				{
					Zones zoneAddToList = new Zones();
					this.id = id;
					//zoneAddToList.setDWellTime(dWellTime);
					zoneAddToList.setDWellTime(dWellTime);
					zoneAddToList.setZoneID(zoneID);
					zoneAddToList.setZoneName(zoneName);
					zones.add(zoneAddToList);
				}
		/*Zones zoneAddToList = new Zones();
		this.userId = userId;
		zoneAddToList.setDWellTime(dWellTime);
		zoneAddToList.setZoneID(zoneID);
		zoneAddToList.setZoneName(zoneName);
		//if zone is already in list add to that zone or create new zone
		//check again
		zones.add(zoneAddToList);*/
		
	}

}
