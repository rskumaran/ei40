package com.entappia.ei4o.models;

import java.util.ArrayList;
import java.util.List;


public class AgingAnalysis {
	public AgingAnalysis() {
		workStations = new ArrayList<AgingWorkStations>();
	}
	public class AgingWorkStations{
		private String workStationid;
		private String workStationName;
		private int today; 
		private int last2Days; 
		private int greaterThan2Days; 

		public String getWorkStationid() {
			return workStationid;
		}
		public void setWorkStationid(String workStationid) {
			this.workStationid = workStationid;
		}
		public int getToday() {
			return today;
		}
		public void setToday(int today) {
			this.today = today;
		}
		public int getLast2Days() {
			return last2Days;
		}
		public void setLast2Days(int last2Days) {
			this.last2Days = last2Days;
		}
		public int getGreaterThan2Days() {
			return greaterThan2Days;
		}
		public void setGreaterThan2Days(int greaterThan2Days) {
			this.greaterThan2Days = greaterThan2Days;
		}
		public String getWorkStationName() {
			return workStationName;
		}
		public void setWorkStationName(String workStationName) {
			this.workStationName = workStationName;
		}

	}

	private List<AgingWorkStations> workStations; 


	public List<AgingWorkStations> getWorkStations(){
		return workStations; 
	}
	public void setWorkStations(List<AgingWorkStations> input){
		this.workStations = input;
	}
	public void addAging(String workStationId, int todayAgingValue, int last2DaysAgingValue, int greaterThanTwoDaysAgingValue, String workStationName) {
		//Check here if already exists and add to list or append according to the requirement
		boolean workStationFoundInList =  false;
		for(int i = 0; i < workStations.size();i++)
		{
			AgingWorkStations workStationAddToList = workStations.get(i);

			if(workStationAddToList.getWorkStationid().equals(workStationId)) {
				workStationFoundInList = true;

				workStationAddToList.setToday(workStationAddToList.getToday() + todayAgingValue);
				workStationAddToList.setLast2Days(workStationAddToList.getLast2Days() + last2DaysAgingValue);
				workStationAddToList.setGreaterThan2Days(workStationAddToList.getGreaterThan2Days() + greaterThanTwoDaysAgingValue);
			}

		}
		if(workStationFoundInList == false)
		{
			AgingWorkStations workStationAddToList = new AgingWorkStations();
			workStationAddToList.setWorkStationid(workStationId);
			workStationAddToList.setWorkStationName(workStationName);
			workStationAddToList.setToday(todayAgingValue);
			workStationAddToList.setLast2Days(last2DaysAgingValue);
			workStationAddToList.setGreaterThan2Days(greaterThanTwoDaysAgingValue);
			workStations.add(workStationAddToList);
		}
	}

	/*public void addAging(String workStationId, int todayAgingValue, int last2DaysAgingValue, int greaterThanTwoDaysAgingValue, String workStationName) {
		AgingWorkStations agingWorkStation = new AgingWorkStations();
		agingWorkStation.setWorkStationid(workStationId);
		agingWorkStation.setToday(todayAgingValue);
		agingWorkStation.setLast2Days(last2DaysAgingValue);
		agingWorkStation.setGreaterThan2Days(greaterThanTwoDaysAgingValue);
		agingWorkStation.setWorkStationName(workStationName);

		WorkStations.add(agingWorkStation);

	}*/
}
