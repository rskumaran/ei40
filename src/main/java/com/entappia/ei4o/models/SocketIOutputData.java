package com.entappia.ei4o.models;

import java.util.HashMap;
import java.util.List;

import com.entappia.ei4o.dbmodels.TagPosition;

public class SocketIOutputData {

	String status;
	HashMap<String, TagPosition> tagPostions;

	public SocketIOutputData(String status, HashMap<String, TagPosition> tagPostions) {
		super();
		this.status = status;
		this.tagPostions = tagPostions;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public HashMap<String, TagPosition> getTagPostions() {
		return tagPostions;
	}

	public void setTagPostions(HashMap<String, TagPosition> tagPostions) {
		this.tagPostions = tagPostions;
	}

}
