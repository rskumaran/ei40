package com.entappia.ei4o.models;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;


public class WorkOrderTimeAnalytics{

	public class Production{

		private String workStationName;
		private String workStationId;
		private String inTime; 
		private String outTime; 
		private int minutes;

		public String getWorkStationName() {
			return workStationName;
		}
		public void setWorkStationName(String workStationName) {
			this.workStationName = workStationName;
		}
		public String getInTime() {
			return inTime;
		}
		public void setInTime(String inTime) {
			this.inTime = inTime;
		}
		public String getOutTime() {
			return outTime;
		}
		public void setOutTime(String outTime) {
			this.outTime = outTime;
		}
		public int getMinutes() {
			return minutes;
		}
		public void setMinutes(int minutes) {
			this.minutes = minutes;
		}
		public String getWorkStationId() {
			return workStationId;
		}
		public void setWorkStationId(String workStationId) {
			this.workStationId = workStationId;
		}

	}
	public class WorkOrder{
		
		private String inTime; 
		private String outTime;
		private int totalTime;
		private String workOrderNumber;
		
		private List<Production> productionList = new ArrayList<Production>(); 

		public String getInTime() {
			return inTime;
		}
		public void setInTime(String inTime) {
			this.inTime = inTime;
		}
		public String getOutTime() {
			return outTime;
		}
		public void setOutTime(String outTime) {
			this.outTime = outTime;
		}
		public List<Production> getProduction(){
			return productionList; 
		}
		public void setProduction(String inTime, String outTime, int minutes, String workStationId, String workStationName){
			Production production = new Production();
			production.setInTime(inTime);
			production.setOutTime(outTime);
			production.setMinutes(minutes);
			production.setWorkStationId(workStationId);
			production.setWorkStationName(workStationName);
			productionList.add(production);
			
		}
		public String getWorkOrderNumber() {
			return workOrderNumber;
		}
		public void setWorkOrderNumber(String workOrderNumber) {
			this.workOrderNumber = workOrderNumber;
		}
		public int getTotalTime() {
			
			return totalTime;
		}
		public void setTotalTime(int totalTime) {
			this.totalTime = totalTime;
		}
		
	}
	
	private List<WorkOrder> workOrderList = new ArrayList<WorkOrder>();
	
	
	public List<WorkOrder> getWorkOrderList() {
		return workOrderList;
	}


	public void setWorkOrderList(List<WorkOrder> workOrderList) {
		this.workOrderList = workOrderList;
	}
	
	public int getWorkOrderListCount() {
		return workOrderList.size();
	}


	public void setWorkOrderTimeAnalytics(String workOrderNumber, String inTime, String outTime, int minutes, String workStationId,
			String workStationName, String workOrderInTime, String workOrderOutTime, int totalTime) {

		//Check here if already exists and add to list or append according to the requirement
		boolean workStationFoundInList =  false;
		for(int i = 0; i < workOrderList.size();i++)
		{
			WorkOrder workOrder = workOrderList.get(i);

			if(workOrder.getWorkOrderNumber().equals(workOrderNumber)) {
				workStationFoundInList = true;
				workOrder.setProduction(inTime, outTime, minutes, workStationId, workStationName);
				
			}
		}
		/*for(WorkOrder workOrder : workOrderList){
	        if(workOrder.getWorkOrderNumber() != null && workOrder.getWorkOrderNumber().contains(workOrderNumber)) {
	        	workStationFoundInList = true;
	        	workOrder.setProduction(inTime, outTime, minutes, workStationId, workStationName);
	        }
	    }*/
		if(workStationFoundInList == false)
		{
			WorkOrder workOrder = new WorkOrder();
			workOrder.setWorkOrderNumber(workOrderNumber);
			workOrder.setTotalTime(totalTime);
			workOrder.setInTime(workOrderInTime);
			workOrder.setOutTime(workOrderOutTime);
			workOrder.setProduction(inTime, outTime, minutes, workStationId, workStationName);
			workOrderList.add(workOrder);
		}

	}
}
