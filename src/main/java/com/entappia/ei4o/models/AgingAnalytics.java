package com.entappia.ei4o.models;

public class AgingAnalytics {
		private String workStationId;
		private String workStationName;
		private String workOrderId;
		private int count = 0;
		
		public String getWorkStationId() {
			return workStationId;
		}
		public void setWorkStationid(String workStationId) {
			this.workStationId = workStationId;
		}
		public String getWorkStationName() {
			return workStationName;
		}
		public void setWorkStationName(String workStationName) {
			this.workStationName = workStationName;
		}
		public int getCount() {
			return count;
		}
		public void setCount(int count) {
			this.count = count;
		}
		public String getWorkOrderId() {
			return workOrderId;
		}
		public void setWorkOrderId(String workOrderId) {
			this.workOrderId = workOrderId;
		}
		
}
