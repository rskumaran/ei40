package com.entappia.ei4o.dbmodels;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.entappia.ei4o.constants.AppConstants;
import com.fasterxml.jackson.annotation.JsonFormat;

@Entity
@Table(name = "WorkMaterialAllocation") 
public class WorkMaterialAllocation implements Serializable {

	 
	@Id
	@Column(nullable = false, columnDefinition = "MEDIUMINT")
	int orderNo;
	 
	@Column(length = 12)
	String macId;
	
	@Column(length = 20)
	String status;
	
	@Temporal(TemporalType.TIMESTAMP)
	@JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MMM-dd HH:mm:ss", timezone= AppConstants.timeZone)
	Date allocationStartTime;
	
	@Temporal(TemporalType.TIMESTAMP)
	@JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MMM-dd HH:mm:ss", timezone= AppConstants.timeZone)
	Date allocationEndTime;
  
	  
	public int getOrderNo() {
		return orderNo;
	}

	public void setOrderNo(int orderNo) {
		this.orderNo = orderNo;
	}

	public String getMacId() {
		return macId;
	}

	public void setMacId(String macId) {
		this.macId = macId;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public Date getAllocationStartTime() {
		return allocationStartTime;
	}

	public void setAllocationStartTime(Date allocationStartTime) {
		this.allocationStartTime = allocationStartTime;
	}

	public Date getAllocationEndTime() {
		return allocationEndTime;
	}

	public void setAllocationEndTime(Date allocationEndTime) {
		this.allocationEndTime = allocationEndTime;
	}
	 
 

	 
	 

}
