package com.entappia.ei4o.dbmodels;

import java.util.Date;

public class MaterialMovement {

	String assetVehicle;
	Date time;
	int grid;
	String station;

	public String getAssetVehicle() {
		return assetVehicle;
	}

	public void setAssetVehicle(String assetVehicle) {
		this.assetVehicle = assetVehicle;
	}

	public Date getTime() {
		return time;
	}

	public void setTime(Date time) {
		this.time = time;
	}

	public int getGrid() {
		return grid;
	}

	public void setGrid(int grid) {
		this.grid = grid;
	}

	public String getStation() {
		return station;
	}

	public void setStation(String station) {
		this.station = station;
	}

}
