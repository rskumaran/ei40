package com.entappia.ei4o.dbmodels;

public class WorkStationChartAnalyticsResult {
	
	private String workStationId;
	private String workStationName;
	private String tq;
	private int wo;
	private int wi;
	private int pr;
	private int tot;
	
	public WorkStationChartAnalyticsResult( String workStationId, String tq, String workStationName, 
			int wo, int wi, int pr, int tot) {
	    this.workStationId = workStationId;
	    this.workStationName = workStationName;
	    this.tq = tq;
	    this.wo = wo;
	    this.wi = wi;
	    this.pr = pr;
	    this.tot = tot;
	}
	
	public String getWorkStationId() {
		return workStationId;
	}
	public void setWorkStationId(String workStationId) {
		this.workStationId = workStationId;
	}
	public String getWorkStationName() {
		return workStationName;
	}
	public void setWorkStationName(String workStationName) {
		this.workStationName = workStationName;
	}
	public String getTq() {
		return tq;
	}
	public void setTq(String tq) {
		this.tq = tq;
	}
	public int getWo() {
		return wo;
	}
	public void setWo(int wo) {
		this.wo = wo;
	}
	public int getWi() {
		return wi;
	}
	public void setWi(int wi) {
		this.wi = wi;
	}
	public int getPr() {
		return pr;
	}
	public void setPr(int pr) {
		this.pr = pr;
	}
	public int getTot() {
		return tot;
	}
	public void setTot(int tot) {
		this.tot = tot;
	}
	
	
}
