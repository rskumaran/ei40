package com.entappia.ei4o.dbmodels;

import java.util.Date;

import javax.persistence.ColumnResult;

public class WorkOrderTimeAnalyticsResult {

	String orderId;
	String stationId;
	String stationName;
	Date startTime;
	Date endTime;
	int diffinMinutes;
	Date workOrderStartTime;
	Date workOrderEndTime;
	int totalTime;

	public WorkOrderTimeAnalyticsResult(String orderId, String stationId, Date startTime, Date endTime, int diffinMinutes, String stationName,
			Date workOrderStartTime, Date workOrderEndTime, int totalTime) {
	    this.orderId = orderId;
	    this.startTime = startTime;
	    this.endTime = endTime;
	    this.diffinMinutes = diffinMinutes;
	    this.stationName = stationName;
	    this.stationId = stationId;
	    this.workOrderStartTime = workOrderStartTime;
	    this.workOrderEndTime = workOrderEndTime;
	    this.totalTime = totalTime;
	}

	public String getOrderId() {
		return orderId;
	}

	public void setOrderId(String orderId) {
		this.orderId = orderId;
	}

	public String getStationId() {
		return stationId;
	}

	public void setStationId(String stationId) {
		this.stationId = stationId;
	}

	public String getStationName() {
		return stationName;
	}

	public void setStationName(String stationName) {
		this.stationName = stationName;
	}

	public Date getStartTime() {
		return startTime;
	}

	public void setStartTime(Date startTime) {
		this.startTime = startTime;
	}

	public Date getEndTime() {
		return endTime;
	}

	public void setEndTime(Date endTime) {
		this.endTime = endTime;
	}

	public int getDiffinMinutes() {
		return diffinMinutes;
	}

	public void setDiffinMinutes(int diffinMinutes) {
		this.diffinMinutes = diffinMinutes;
	}

	public Date getWorkOrderStartTime() {
		return workOrderStartTime;
	}

	public void setWorkOrderStartTime(Date workOrderStartTime) {
		this.workOrderStartTime = workOrderStartTime;
	}

	public Date getWorkOrderEndTime() {
		return workOrderEndTime;
	}

	public void setWorkOrderEndTime(Date workOrderEndTime) {
		this.workOrderEndTime = workOrderEndTime;
	}

	public int getTotalTime() {
		return totalTime;
	}

	public void setTotalTime(int totalTime) {
		this.totalTime = totalTime;
	}
	
}
