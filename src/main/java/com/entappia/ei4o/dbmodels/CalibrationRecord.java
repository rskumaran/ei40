package com.entappia.ei4o.dbmodels;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.entappia.ei4o.constants.AppConstants;
import com.fasterxml.jackson.annotation.JsonFormat;

@Entity
@Table(name = "CalibrationRecord")
public class CalibrationRecord {

	@Id
	@Column(length = 12)
	String calibrationId;

	@Column(length = 20)
	String asset;

	@Temporal(TemporalType.DATE)
	@JsonFormat(pattern = "yyyy-MMM-dd", timezone= AppConstants.timeZone)
	Date calibrationExpiryDate;

	@Lob
	private byte[] calibrationData;

	@Temporal(TemporalType.TIMESTAMP)
	@JsonFormat(pattern = "yyyy-MMM-dd HH:mm", timezone= AppConstants.timeZone)
	Date createdDate;

	@Temporal(TemporalType.TIMESTAMP)
	@JsonFormat(pattern = "yyyy-MMM-dd HH:mm", timezone= AppConstants.timeZone)
	Date modifiedDate;

	boolean active;

	public boolean isActive() {
		return active;
	}

	public void setActive(boolean active) {
		this.active = active;
	}

	public String getCalibrationId() {
		return calibrationId;
	}

	public void setCalibrationId(String calibrationId) {
		this.calibrationId = calibrationId;
	}

	public String getAsset() {
		return asset;
	}

	public void setAsset(String asset) {
		this.asset = asset;
	}

	public Date getCalibrationExpiryDate() {
		return calibrationExpiryDate;
	}

	public void setCalibrationExpiryDate(Date calibrationExpiryDate) {
		this.calibrationExpiryDate = calibrationExpiryDate;
	} 

	public byte[] getCalibrationData() {
		return calibrationData;
	}

	public void setCalibrationData(byte[] calibrationData) {
		this.calibrationData = calibrationData;
	}

	public Date getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}

	public Date getModifiedDate() {
		return modifiedDate;
	}

	public void setModifiedDate(Date modifiedDate) {
		this.modifiedDate = modifiedDate;
	}

}
