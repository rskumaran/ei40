package com.entappia.ei4o.dbmodels;

import java.util.Date;

public class AssetNCLocation {

	String assetNCId;
	Date time;
	String zone;
	int grid;

	public String getAssetNCId() {
		return assetNCId;
	}

	public void setAssetNCId(String assetNCId) {
		this.assetNCId = assetNCId;
	}

	public Date getTime() {
		return time;
	}

	public void setTime(Date time) {
		this.time = time;
	}

	public String getZone() {
		return zone;
	}

	public void setZone(String zone) {
		this.zone = zone;
	}

	public int getGrid() {
		return grid;
	}

	public void setGrid(int grid) {
		this.grid = grid;
	}

}
