package com.entappia.ei4o.dbmodels;


	public class AgingAnalyticsResult{
		private String workStationId;
		private String workStationName;
		private String workOrderId;
		
		public AgingAnalyticsResult( String workStationId, String workStationName, String workOrderId) {
		    this.workStationId = workStationId;
		    this.workStationName = workStationName;
		    this.workOrderId = workOrderId;
		}
		
		public String getWorkStationId() {
			return workStationId;
		}
		public void setWorkStationId(String workStationId) {
			this.workStationId = workStationId;
		}
		public String getWorkStationName() {
			return workStationName;
		}
		public void setWorkStationName(String workStationName) {
			this.workStationName = workStationName;
		}
		public String getWorkOrderId() {
			return workOrderId;
		}
		public void setWorkOrderId(String workOrderId) {
			this.workOrderId = workOrderId;
		}	
		
	}
	
	
	/*List<Aging> agingdata = new ArrayList<>(); 
	public void setAgingData(String date, String workStationid, String workStationName, String workOrderId) {
		
		Aging aging = new Aging(date, workStationid, workOrderId, workStationName);
		agingdata.add(aging);
	}*/

