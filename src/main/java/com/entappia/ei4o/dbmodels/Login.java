package com.entappia.ei4o.dbmodels;

import java.util.Date;
import java.util.HashMap;
import java.util.List;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Convert;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.entappia.ei4o.constants.AppConstants;
import com.entappia.ei4o.converters.DeviceDetailsConverter;
import com.entappia.ei4o.converters.HashMapConverter;
import com.entappia.ei4o.converters.LastSeenConverter;
import com.entappia.ei4o.converters.OrgAlertConverter;
import com.entappia.ei4o.models.Alert;
import com.entappia.ei4o.models.DeviceDetails;
import com.entappia.ei4o.models.LastSession;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;

@Entity
@Table(name = "Login")
public class Login {

	@Id
	@Column(length = 40)
	String emailID;

	@Column(length = 40)
	String displayName;

	@JsonProperty(access = JsonProperty.Access.WRITE_ONLY)
	String password;

	@Column(length = 20)
	String type;

	@Column(length = 20)
	String userType;

	@Temporal(TemporalType.TIMESTAMP)
	@JsonFormat(pattern = "yyyy-MMM-dd HH:mm", timezone = AppConstants.timeZone)
	Date createdDate;

	@Column(length = 20)
	String mobileNumber;

	@Convert(converter = DeviceDetailsConverter.class)
	@Column(name = "deviceDetails", columnDefinition = "json")
	List<DeviceDetails> deviceDetails;

	@Convert(converter = OrgAlertConverter.class)
	@Column(name = "orgAlert", columnDefinition = "json")
	HashMap<String, Alert> orgAlert;

	@Convert(converter = HashMapConverter.class)
	@Column(name = "capabilityList", columnDefinition = "json")
	HashMap<String, HashMap<String, Integer>> capabilityList;

	@Lob
	@Basic(fetch = FetchType.LAZY)
	@Column(name = "profileImage")
	private byte[] profileImage;

	@Convert(converter = LastSeenConverter.class)
	@Column(name = "lastSession", columnDefinition = "json")
	LastSession lastSession;

	public String getEmailID() {
		return emailID;
	}

	public void setEmailID(String emailID) {
		this.emailID = emailID;
	}

	public String getDisplayName() {
		return displayName;
	}

	public void setDisplayName(String displayName) {
		this.displayName = displayName;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getUserType() {
		return userType;
	}

	public void setUserType(String userType) {
		this.userType = userType;
	}

	public Date getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}

	public List<DeviceDetails> getDeviceDetails() {
		return deviceDetails;
	}

	public void setDeviceDetails(List<DeviceDetails> deviceDetails) {
		this.deviceDetails = deviceDetails;
	}

	public HashMap<String, Alert> getOrgAlert() {
		return orgAlert;
	}

	public void setOrgAlert(HashMap<String, Alert> orgAlert) {
		this.orgAlert = orgAlert;
	}

	public HashMap<String, HashMap<String, Integer>> getCapabilityList() {
		return capabilityList;
	}

	public void setCapabilityList(HashMap<String, HashMap<String, Integer>> capabilityList) {
		this.capabilityList = capabilityList;
	}

	public LastSession getLastSession() {
		return lastSession;
	}

	public void setLastSession(LastSession lastSession) {
		this.lastSession = lastSession;
	}

	public String getMobileNumber() {
		return mobileNumber;
	}

	public void setMobileNumber(String mobileNumber) {
		this.mobileNumber = mobileNumber;
	}

	public byte[] getProfileImage() {
		return profileImage;
	}

	public void setProfileImage(byte[] profileImage) {
		this.profileImage = profileImage;
	}

}
