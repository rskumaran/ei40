package com.entappia.ei4o.dbmodels;

public class AttendanceZoneResult {

	
	String zoneId;
	String zoneName;
	String tQ;
	Integer NoOfPersons;
	Integer zoneRestrictions;
	String zoneClassification;
	
	public AttendanceZoneResult(String zoneId, String zoneName, Integer NoOfPersons, String tQ, Integer zoneRestrictions, String zoneClassification) {
		this.zoneId = zoneId;
		this.zoneName = zoneName;
		this.tQ = tQ;
		this.NoOfPersons = NoOfPersons;
		this.zoneRestrictions = zoneRestrictions;
		this.zoneClassification = zoneClassification;
		
	}

	public String getZoneId() {
		return zoneId;
	}

	public void setZoneId(String zoneId) {
		this.zoneId = zoneId;
	}

	public String getZoneName() {
		return zoneName;
	}

	public void setZoneName(String zoneName) {
		this.zoneName = zoneName;
	}

	public String gettQ() {
		return tQ;
	}

	public void settQ(String tQ) {
		this.tQ = tQ;
	}

	public Integer getNoOfPersons() {
		return NoOfPersons;
	}

	public void setNoOfPersons(Integer noOfPersons) {
		NoOfPersons = noOfPersons;
	}

	public Integer getZoneRestrictions() {
		return zoneRestrictions;
	}

	public void setZoneRestrictions(Integer zoneRestrictions) {
		this.zoneRestrictions = zoneRestrictions;
	}

	public String getZoneClassification() {
		return zoneClassification;
	}

	public void setZoneClassification(String zoneClassification) {
		this.zoneClassification = zoneClassification;
	}

	


}
