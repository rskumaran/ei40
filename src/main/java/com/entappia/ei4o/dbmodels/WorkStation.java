package com.entappia.ei4o.dbmodels;

import java.io.Serializable;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Convert;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.entappia.ei4o.constants.AppConstants;
import com.entappia.ei4o.converters.ListConverter;
import com.entappia.ei4o.converters.WorkStationConverter;
import com.fasterxml.jackson.annotation.JsonFormat;

@Entity
@Table(name = "WorkStation")
public class WorkStation implements Serializable {

	@Id
	@Column(length = 12)
	String stationId;
	@Column(length = 20)
	String stationType;

	/*
	 * @Convert(converter = WorkStationConverter.class)
	 * 
	 * @Column(name = "inbound", columnDefinition = "json") HashMap<String,
	 * List<String>> inbound;
	 * 
	 * @Convert(converter = WorkStationConverter.class)
	 * 
	 * @Column(name = "outbound", columnDefinition = "json") HashMap<String,
	 * List<String>> outbound;
	 * 
	 * @Convert(converter = WorkStationConverter.class)
	 * 
	 * @Column(name = "workArea", columnDefinition = "json") HashMap<String,
	 * List<String>> workArea;
	 * 
	 * @Convert(converter = WorkStationConverter.class)
	 * 
	 * @Column(name = "storage", columnDefinition = "json") HashMap<String,
	 * List<String>> storage;
	 */

	@OneToOne
	@JoinColumn(name = "inbound", referencedColumnName = "id", nullable = false)
	ZoneDetails inbound;

	@OneToOne
	@JoinColumn(name = "outbound", referencedColumnName = "id", nullable = false)
	ZoneDetails outbound;

	@OneToOne
	@JoinColumn(name = "workArea", referencedColumnName = "id", nullable = false)
	ZoneDetails workArea;

	@OneToOne
	@JoinColumn(name = "storage", referencedColumnName = "id", nullable = false)
	ZoneDetails storage;

	@Convert(converter = ListConverter.class)
	@Column(name = "operatorList", columnDefinition = "json")
	List<String> operatorList;

	@Column(nullable = false, columnDefinition = "SMALLINT")
	int inLimit;

	@Column(nullable = false, columnDefinition = "SMALLINT")
	int outLimit;

	boolean status;

	@Column(length = 30)
	String zone;

	@Temporal(TemporalType.TIMESTAMP)
	@JsonFormat(pattern = "yyyy-MMM-dd HH:mm:ss", timezone = AppConstants.timeZone)
	Date createdDate;

	@Temporal(TemporalType.TIMESTAMP)
	@JsonFormat(pattern = "yyyy-MMM-dd HH:mm:ss", timezone = AppConstants.timeZone)
	Date modifiedDate;

	boolean active;

	public String getStationId() {
		return stationId;
	}

	public void setStationId(String stationId) {
		this.stationId = stationId;
	}

	public String getStationType() {
		return stationType;
	}

	public void setStationType(String stationType) {
		this.stationType = stationType;
	}

	public ZoneDetails getInbound() {
		return inbound;
	}

	public void setInbound(ZoneDetails inbound) {
		this.inbound = inbound;
	}

	public ZoneDetails getOutbound() {
		return outbound;
	}

	public void setOutbound(ZoneDetails outbound) {
		this.outbound = outbound;
	}

	public ZoneDetails getWorkArea() {
		return workArea;
	}

	public void setWorkArea(ZoneDetails workArea) {
		this.workArea = workArea;
	}

	public ZoneDetails getStorage() {
		return storage;
	}

	public void setStorage(ZoneDetails storage) {
		this.storage = storage;
	}

	public List<String> getOperatorList() {
		return operatorList;
	}

	public void setOperatorList(List<String> operatorList) {
		this.operatorList = operatorList;
	}

	public int getInLimit() {
		return inLimit;
	}

	public void setInLimit(int inLimit) {
		this.inLimit = inLimit;
	}

	public int getOutLimit() {
		return outLimit;
	}

	public void setOutLimit(int outLimit) {
		this.outLimit = outLimit;
	}

	public boolean isStatus() {
		return status;
	}

	public void setStatus(boolean status) {
		this.status = status;
	}

	public String getZone() {
		return zone;
	}

	public void setZone(String zone) {
		this.zone = zone;
	}

	public Date getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}

	public Date getModifiedDate() {
		return modifiedDate;
	}

	public void setModifiedDate(Date modifiedDate) {
		this.modifiedDate = modifiedDate;
	}

	public boolean isActive() {
		return active;
	}

	public void setActive(boolean active) {
		this.active = active;
	}

}
