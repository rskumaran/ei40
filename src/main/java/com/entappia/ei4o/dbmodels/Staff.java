package com.entappia.ei4o.dbmodels;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.entappia.ei4o.constants.AppConstants;
import com.fasterxml.jackson.annotation.JsonFormat;

@Entity
@Table(name = "Staff")
public class Staff {
	
	@Id 
	@Column(length = 12)
	String empId;
	@Column(length = 16)
	String lastName;
	 
	@Column(length = 16)
	String firstName;
	 
	@OneToOne
    @JoinColumn(name = "deptName", referencedColumnName = "deptName") 
	Department department;
	
	@Column(length = 40)
	String location1;
	
	@Column(length = 40)
	String location2;	
	@Column(length = 40)
	String location3;
	
	@Column(length = 15)
	String phoneNumber;
	 
	@Column(length = 20)
	String status;
	
	@OneToOne
    @JoinColumn(name = "shiftName", referencedColumnName = "shiftName")  
	Shift shiftDetails;
	 
	boolean active;
	
	@Temporal(TemporalType.TIMESTAMP)
	@JsonFormat(pattern = "yyyy-MMM-dd HH:mm", timezone= AppConstants.timeZone)
	Date createdDate;
	
	@Temporal(TemporalType.TIMESTAMP)
	@JsonFormat(pattern = "yyyy-MMM-dd HH:mm", timezone= AppConstants.timeZone)
	Date modifiedDate;

	public String getEmpId() {
		return empId;
	}

	public void setEmpId(String empId) {
		this.empId = empId;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public Department getDepartment() {
		return department;
	}

	public void setDepartment(Department department) {
		this.department = department;
	}

	public String getLocation1() {
		return location1;
	}

	public void setLocation1(String location1) {
		this.location1 = location1;
	}

	public String getLocation2() {
		return location2;
	}

	public void setLocation2(String location2) {
		this.location2 = location2;
	}

	public String getLocation3() {
		return location3;
	}

	public void setLocation3(String location3) {
		this.location3 = location3;
	}

	public String getPhoneNumber() {
		return phoneNumber;
	}

	public void setPhoneNumber(String phoneNumber) {
		this.phoneNumber = phoneNumber;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public Shift getShiftDetails() {
		return shiftDetails;
	}

	public void setShiftDetails(Shift shiftDetails) {
		this.shiftDetails = shiftDetails;
	}

	public boolean isActive() {
		return active;
	}

	public void setActive(boolean active) {
		this.active = active;
	}

	public Date getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}

	public Date getModifiedDate() {
		return modifiedDate;
	}

	public void setModifiedDate(Date modifiedDate) {
		this.modifiedDate = modifiedDate;
	}

	
}
