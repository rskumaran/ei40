package com.entappia.ei4o.dbmodels;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
@Entity(name = "TagPosition")
public class TagPosition {

	@Id
	@Column(length = 12)
	String macId;
	
	private double x;
	private double y;
	private double z;
	
	@Column(length = 17)
	String tagId;
	 
	@Column(length = 20)
	String assignmentId;
	
	int zoneId;
	
	@Column(length = 30)
	String zoneName;
	
	@Column(length = 15)
	String type;
	
	@Column(length = 15)
	String subType;
	
	long lastSeen;

	public double getX() {
		return x;
	}

	public void setX(double x) {
		this.x = x;
	}

	public double getY() {
		return y;
	}

	public void setY(double y) {
		this.y = y;
	}

	public double getZ() {
		return z;
	}

	public void setZ(double z) {
		this.z = z;
	}

	public String getMacId() {
		return macId;
	}

	public void setMacId(String macId) {
		this.macId = macId;
	}

	public String getTagId() {
		return tagId;
	}

	public void setTagId(String tagId) {
		this.tagId = tagId;
	}

	public String getAssignmentId() {
		return assignmentId;
	}

	public void setAssignmentId(String assignmentId) {
		this.assignmentId = assignmentId;
	}

	
	public int getZoneId() {
		return zoneId;
	}

	public void setZoneId(int zoneId) {
		this.zoneId = zoneId;
	}

	public String getZoneName() {
		return zoneName;
	}

	public void setZoneName(String zoneName) {
		this.zoneName = zoneName;
	}
	 
	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getSubType() {
		return subType;
	}

	public void setSubType(String subType) {
		this.subType = subType;
	}

	
	public long getLastSeen() {
		return lastSeen;
	}

	public void setLastSeen(long lastSeen) {
		this.lastSeen = lastSeen;
	}

	@Override
	public String toString() {
		return "TagPosition [x=" + x + ", y=" + y + ", z=" + z + ", zoneId=" + zoneId + ", zoneName=" + zoneName + "]";
	}
	
	

}
