package com.entappia.ei4o.dbmodels;

public class WorkStationInventoryResult {

	String stationId;
	Double waitingAmount;
	String stationName;
	
	public WorkStationInventoryResult(String stationId, Double waitingAmount, String stationName) {
		this.stationId = stationId;
		this.waitingAmount = waitingAmount;
		this.stationName = stationName;
	}

	public String getStationName() {
		return stationName;
	}

	public void setStationName(String stationName) {
		this.stationName = stationName;
	}

	public String getStationId() {
		return stationId;
	}

	public void setStationId(String stationId) {
		this.stationId = stationId;
	}

	public Double getWaitingAmount() {
		return waitingAmount;
	}

	public void setWaitingAmount(Double waitingAmount) {
		this.waitingAmount = waitingAmount;
	}
}

