package com.entappia.ei4o.dbmodels;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.entappia.ei4o.constants.AppConstants;
import com.fasterxml.jackson.annotation.JsonFormat;

@Entity
@Table(name = "Configuration")
public class Configuration {

	@Id
	@Column(length = 37)
	String orgName;

	@Column(nullable = false, columnDefinition = "SMALLINT")
	int logAge;
	@Column(nullable = false, columnDefinition = "SMALLINT")
	int transactionDataAge;

	@Column(length = 20)
	String timeZone;
	@Column(length = 8)
	String staffTagColor;
	@Column(length = 8)
	String visitorTagColor;
	@Column(length = 8)
	String gageTagColor;
	@Column(length = 8)
	String toolTagColor;
	@Column(length = 8)
	String fixtureTagColor;
	@Column(length = 8)
	String partTagColor;
	@Column(length = 8)
	String materialTagColor;
	@Column(length = 8)
	String rmaTagColor;
	@Column(length = 8)
	String vendorTagColor;
	@Column(length = 8)
	String contractorTagColor;
	@Column(length = 8)
	String vehicleTagColor;
	@Column(length = 8)
	String inventoryTagColor;
	@Column(length = 8)
	String lslmTagColor;
	@Column(length = 8)
	String wareHouseTagColor;
	@Column(length = 8)
	String suppliesTagColor;
	@Column(length = 37)
	String licenseKey;
	@Temporal(TemporalType.TIME)
	@JsonFormat(pattern = "HH:mm", timezone= AppConstants.timeZone)
	Date operationStartTime;
	
	@Temporal(TemporalType.TIME)
	@JsonFormat(pattern = "HH:mm", timezone= AppConstants.timeZone)
	Date operationEndTime;
	
	boolean active;

	@Temporal(TemporalType.TIMESTAMP)
	@JsonFormat(pattern = "yyyy-MMM-dd HH:mm:ss", timezone= AppConstants.timeZone)
	Date createdDate;

	@Temporal(TemporalType.TIMESTAMP)
	@JsonFormat(pattern = "yyyy-MMM-dd HH:mm:ss", timezone= AppConstants.timeZone)
	Date modifiedDate;

	

	public String getOrgName() {
		return orgName;
	}

	public void setOrgName(String orgName) {
		this.orgName = orgName;
	}

	public int getLogAge() {
		return logAge;
	}

	public void setLogAge(int logAge) {
		this.logAge = logAge;
	}

	public int getTransactionDataAge() {
		return transactionDataAge;
	}

	public void setTransactionDataAge(int transactionDataAge) {
		this.transactionDataAge = transactionDataAge;
	}

	public String getTimeZone() {
		return timeZone;
	}

	public void setTimeZone(String timeZone) {
		this.timeZone = timeZone;
	}

	public String getStaffTagColor() {
		return staffTagColor;
	}

	public void setStaffTagColor(String staffTagColor) {
		this.staffTagColor = staffTagColor;
	}

	public String getVisitorTagColor() {
		return visitorTagColor;
	}

	public void setVisitorTagColor(String visitorTagColor) {
		this.visitorTagColor = visitorTagColor;
	}

	public String getGageTagColor() {
		return gageTagColor;
	}

	public void setGageTagColor(String gageTagColor) {
		this.gageTagColor = gageTagColor;
	}

	public String getToolTagColor() {
		return toolTagColor;
	}

	public void setToolTagColor(String toolTagColor) {
		this.toolTagColor = toolTagColor;
	}

	public String getFixtureTagColor() {
		return fixtureTagColor;
	}

	public void setFixtureTagColor(String fixtureTagColor) {
		this.fixtureTagColor = fixtureTagColor;
	}

	public String getPartTagColor() {
		return partTagColor;
	}

	public void setPartTagColor(String partTagColor) {
		this.partTagColor = partTagColor;
	}

	public String getMaterialTagColor() {
		return materialTagColor;
	}

	public void setMaterialTagColor(String materialTagColor) {
		this.materialTagColor = materialTagColor;
	}

	public String getRmaTagColor() {
		return rmaTagColor;
	}


	public void setRmaTagColor(String rmaTagColor) {
		this.rmaTagColor = rmaTagColor;
	}

	public void setVendorTagColor(String vendorTagColor) {
		this.vendorTagColor = vendorTagColor;
	}
	public String getVendorTagColor() {
		return vendorTagColor;
	}

	public void setContractorTagColor(String contractorTagColor) {
		this.contractorTagColor = contractorTagColor;
	}
	public String getContractorTagColor() {
		return contractorTagColor;
	}

	public void setVehicleTagColor(String vehicleTagColor) {
		this.vehicleTagColor = vehicleTagColor;
	}
	public String getVehicleTagColor() {
		return vehicleTagColor;
	}

	public void setInventoryTagColor(String inventoryTagColor) {
		this.inventoryTagColor = inventoryTagColor;
	}
	public String getInventoryTagColor() {
		return inventoryTagColor;
	}

	public void setLSLMTagColor(String lslmTagColor) {
		this.lslmTagColor = lslmTagColor;
	}
	public String getLSLMTagColor() {
		return lslmTagColor;
	}
	public void setWareHouseTagColor(String wareHouseTagColor) {
		this.wareHouseTagColor = wareHouseTagColor;
	}
	public String getWareHouseTagColor() {
		return wareHouseTagColor;
	}
	public void setSuppliesTagColor(String suppliesTagColor) {
		this.suppliesTagColor = suppliesTagColor;
	}
	public String getSuppliesTagColor() {
		return suppliesTagColor;
	}
	
	public boolean isActive() {
		return active;
	}

	public void setActive(boolean active) {
		this.active = active;
	}

	public Date getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}

	public Date getModifiedDate() {
		return modifiedDate;
	}

	public void setModifiedDate(Date modifiedDate) {
		this.modifiedDate = modifiedDate;
	}


	public String getLicenseKey() {
		return licenseKey;
	}

	public void setLicenseKey(String licenseKey) {
		this.licenseKey = licenseKey;
	}

	public Date getOperationStartTime() {
		return operationStartTime;
	}

	public void setOperationStartTime(Date operationStartTime) {
		this.operationStartTime = operationStartTime;
	}

	public Date getOperationEndTime() {
		return operationEndTime;
	}

	public void setOperationEndTime(Date operationEndTime) {
		this.operationEndTime = operationEndTime;
	}


}
