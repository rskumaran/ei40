package com.entappia.ei4o.dbmodels;

import java.util.Date;

public class AssetChronoLocation {

	String assetChronoId;
	Date time;
	String zone;
	int grid;

	public String getAssetChronoId() {
		return assetChronoId;
	}

	public void setAssetChronoId(String assetChronoId) {
		this.assetChronoId = assetChronoId;
	}

	public Date getTime() {
		return time;
	}

	public void setTime(Date time) {
		this.time = time;
	}

	public String getZone() {
		return zone;
	}

	public void setZone(String zone) {
		this.zone = zone;
	}

	public int getGrid() {
		return grid;
	}

	public void setGrid(int grid) {
		this.grid = grid;
	}

}
