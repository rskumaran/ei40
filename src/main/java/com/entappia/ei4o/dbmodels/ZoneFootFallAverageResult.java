package com.entappia.ei4o.dbmodels;

public class ZoneFootFallAverageResult {

	String zoneId;
	String zoneName;
	Integer totalPersons;
	Integer maximum;
	Double average;
	
	public ZoneFootFallAverageResult(String zoneId, String zoneName, Integer totalPersons, Integer maximum, Double average) {
		this.zoneId = zoneId;
		this.zoneName = zoneName;
		this.totalPersons = totalPersons;
		this.maximum = maximum;
		this.average = average;
	}
	
	public String getZoneId() {
		return zoneId;
	}
	public void setZoneId(String zoneId) {
		this.zoneId = zoneId;
	}
	public String getZoneName() {
		return zoneName;
	}
	public void setZoneName(String zoneName) {
		this.zoneName = zoneName;
	}
	public Integer getTotalPersons() {
		return totalPersons;
	}
	public void setTotalPersons(Integer totalPersons) {
		this.totalPersons = totalPersons;
	}
	public Integer getMaximum() {
		return maximum;
	}
	public void setMaximum(Integer maximum) {
		this.maximum = maximum;
	}
	public Double getAverage() {
		return average;
	}
	public void setAverage(Double average) {
		this.average = average;
	}
	
	
}
