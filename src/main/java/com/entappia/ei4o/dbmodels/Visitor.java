package com.entappia.ei4o.dbmodels;

import java.util.Date;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Convert;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.hibernate.annotations.GenericGenerator;

import com.entappia.ei4o.constants.AppConstants;
import com.entappia.ei4o.converters.ListConverter;
import com.fasterxml.jackson.annotation.JsonFormat;

@Entity
@Table(name = "visitor")
public class Visitor {

	@Id
	@Column(nullable = false, columnDefinition = "bigint(20)")
	@GeneratedValue(strategy = GenerationType.AUTO, generator = "native")
	@GenericGenerator(name = "native", strategy = "native")
	long  visitorId;

	@Column(length = 16)
	String lastName;

	@Column(length = 16)
	String firstName;

	@Column(length = 30)
	String company;

	@Column(length = 15)
	String contactNo;
	
	@Temporal(TemporalType.DATE)
	@JsonFormat(pattern = "yyyy-MMM-dd", timezone= AppConstants.timeZone)
	Date date;

	@Temporal(TemporalType.TIME)
	@JsonFormat(pattern = "HH:mm", timezone= AppConstants.timeZone)
	Date fromTime;

	@Temporal(TemporalType.TIME)
	@JsonFormat(pattern = "HH:mm", timezone= AppConstants.timeZone)
	Date toTime;

	@Column(length = 16)
	String contactPerson;

	@Column(length = 15)
	String contactPersonNumber;

	@Column(length = 15)
	String type;

	@Convert(converter = ListConverter.class)
	@Column(name = "accessLimit", columnDefinition = "json")
	List<String> accessLimit;

	public long getVisitorId() {
		return visitorId;
	}

	public void setVisitorId(long visitorId) {
		this.visitorId = visitorId;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getCompany() {
		return company;
	}

	public void setCompany(String company) {
		this.company = company;
	}

	public String getContactNo() {
		return contactNo;
	}

	public void setContactNo(String contactNo) {
		this.contactNo = contactNo;
	}

	public Date getDate() {
		return date;
	}

	public void setDate(Date date) {
		this.date = date;
	}

	public Date getFromTime() {
		return fromTime;
	}

	public void setFromTime(Date fromTime) {
		this.fromTime = fromTime;
	}

	public Date getToTime() {
		return toTime;
	}

	public void setToTime(Date toTime) {
		this.toTime = toTime;
	}

	public String getContactPerson() {
		return contactPerson;
	}

	public void setContactPerson(String contactPerson) {
		this.contactPerson = contactPerson;
	}
 

	public String getContactPersonNumber() {
		return contactPersonNumber;
	}

	public void setContactPersonNumber(String contactPersonNumber) {
		this.contactPersonNumber = contactPersonNumber;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public List<String> getAccessLimit() {
		return accessLimit;
	}

	public void setAccessLimit(List<String> accessLimit) {
		this.accessLimit = accessLimit;
	}
}