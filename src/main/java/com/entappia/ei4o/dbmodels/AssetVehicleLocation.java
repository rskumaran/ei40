package com.entappia.ei4o.dbmodels;

import java.util.Date;

public class AssetVehicleLocation {

	String vehicleId;
	Date time;
	String pathSegment;
	String direction;

	public String getVehicleId() {
		return vehicleId;
	}

	public void setVehicleId(String vehicleId) {
		this.vehicleId = vehicleId;
	}

	public Date getTime() {
		return time;
	}

	public void setTime(Date time) {
		this.time = time;
	}

	public String getPathSegment() {
		return pathSegment;
	}

	public void setPathSegment(String pathSegment) {
		this.pathSegment = pathSegment;
	}

	public String getDirection() {
		return direction;
	}

	public void setDirection(String direction) {
		this.direction = direction;
	}

}
