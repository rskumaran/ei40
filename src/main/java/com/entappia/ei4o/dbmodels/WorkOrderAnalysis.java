package com.entappia.ei4o.dbmodels;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.entappia.ei4o.dbmodels.idclass.WorkOrderAnalysisIdClass;
import com.fasterxml.jackson.annotation.JsonFormat;

@Entity
@Table(name = "WorkOrderAnalysis")
@IdClass(WorkOrderAnalysisIdClass.class)
public class WorkOrderAnalysis {

	@Id
	@Column(nullable = false, columnDefinition = "MEDIUMINT")
	int orderNo;
	
	@Id
	@Temporal(TemporalType.DATE)
	@JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "YYYY-MMM-DD")
	Date analysisDate;
	
	@Id
	@Column(length = 12)
	String stationId;
	
	/*@Id
	@Column(length = 40)
	String shiftName;*/
			
	int inboundZoneTimeDuration;
	int outboundZoneTimeDuration;
	int workAreaZoneTimeDuration;
	int storageZoneTimeDuration;
	
	int inboundWorkOrderStartTime;
	int outboundWorkOrderStartTime;
	int workAreaWorkOrderStartTime;
	int storageWorkOrderStartTime;
	
	int inboundWorkOrderEndTime;
	int outboundWorkOrderEndTime;
	int workAreaWorkOrderEndTime;
	int storageWorkOrderEndTime;
	
	public int getInboundWorkOrderEndTime() {
		return inboundWorkOrderEndTime;
	}
	public void setInboundWorkOrderEndTime(int inboundWorkOrderEndTime) {
		this.inboundWorkOrderEndTime = inboundWorkOrderEndTime;
	}
	public int getOutboundWorkOrderEndTime() {
		return outboundWorkOrderEndTime;
	}
	public void setOutboundWorkOrderEndTime(int outboundWorkOrderEndTime) {
		this.outboundWorkOrderEndTime = outboundWorkOrderEndTime;
	}
	public int getWorkAreaWorkOrderEndTime() {
		return workAreaWorkOrderEndTime;
	}
	public void setWorkAreaWorkOrderEndTime(int workAreaWorkOrderEndTime) {
		this.workAreaWorkOrderEndTime = workAreaWorkOrderEndTime;
	}
	public int getStorageWorkOrderEndTime() {
		return storageWorkOrderEndTime;
	}
	public void setStorageWorkOrderEndTime(int storageWorkOrderEndTime) {
		this.storageWorkOrderEndTime = storageWorkOrderEndTime;
	}
		
	public int getOrderNo() {
		return orderNo;
	}
	public void setOrderNo(int orderNo) {
		this.orderNo = orderNo;
	}
	public Date getAnalysisDate() {
		return analysisDate;
	}
	public void setAnalysisDate(Date analysisDate) {
		this.analysisDate = analysisDate;
	}
	public String getStationId() {
		return stationId;
	}
	public void setStationId(String stationId) {
		this.stationId = stationId;
	}
	/*public String getShiftName() {
		return shiftName;
	}
	public void setShiftName(String shiftName) {
		this.shiftName = shiftName;
	}*/
	public int getInboundZoneTimeDuration() {
		return inboundZoneTimeDuration;
	}
	public void setInboundZoneTimeDuration(int inboundZoneTimeDuration) {
		this.inboundZoneTimeDuration = inboundZoneTimeDuration;
	}
	public int getOutboundZoneTimeDuration() {
		return outboundZoneTimeDuration;
	}
	public void setOutboundZoneTimeDuration(int outboundZoneTimeDuration) {
		this.outboundZoneTimeDuration = outboundZoneTimeDuration;
	}
	public int getWorkAreaZoneTimeDuration() {
		return workAreaZoneTimeDuration;
	}
	public void setWorkAreaZoneTimeDuration(int workAreaZoneTimeDuration) {
		this.workAreaZoneTimeDuration = workAreaZoneTimeDuration;
	}
	public int getStorageZoneTimeDuration() {
		return storageZoneTimeDuration;
	}
	public void setStorageZoneTimeDuration(int storageZoneTimeDuration) {
		this.storageZoneTimeDuration = storageZoneTimeDuration;
	}
	public int getInboundWorkOrderStartTime() {
		return inboundWorkOrderStartTime;
	}
	public void setInboundWorkOrderStartTime(int inboundWorkOrderStartTime) {
		this.inboundWorkOrderStartTime = inboundWorkOrderStartTime;
	}
	public int getOutboundWorkOrderStartTime() {
		return outboundWorkOrderStartTime;
	}
	public void setOutboundWorkOrderStartTime(int outboundWorkOrderStartTime) {
		this.outboundWorkOrderStartTime = outboundWorkOrderStartTime;
	}
	public int getWorkAreaWorkOrderStartTime() {
		return workAreaWorkOrderStartTime;
	}
	public void setWorkAreaWorkOrderStartTime(int workAreaWorkOrderStartTime) {
		this.workAreaWorkOrderStartTime = workAreaWorkOrderStartTime;
	}
	public int getStorageWorkOrderStartTime() {
		return storageWorkOrderStartTime;
	}
	public void setStorageWorkOrderStartTime(int storageWorkOrderStartTime) {
		this.storageWorkOrderStartTime = storageWorkOrderStartTime;
	}
	
	
}
