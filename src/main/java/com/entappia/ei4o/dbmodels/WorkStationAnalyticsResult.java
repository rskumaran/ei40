package com.entappia.ei4o.dbmodels;

public class WorkStationAnalyticsResult {
String stationId;
String stationName;
int incoming;
int outgoing;
int waiting;

public WorkStationAnalyticsResult(String stationId, int incoming, int outgoing, int waiting, String stationName) {
    this.stationId = stationId;
    this.incoming = incoming;
    this.outgoing = outgoing;
    this.waiting = waiting;
    this.stationName = stationName;
}

public String getStationId() {
	return stationId;
}

public String getStationName() {
	return stationName;
}

public void setStationName(String stationName) {
	this.stationName = stationName;
}

public void setStationId(String stationId) {
	this.stationId = stationId;
}

public int getIncoming() {
	return incoming;
}

public void setIncoming(int incoming) {
	this.incoming = incoming;
}

public int getOutgoing() {
	return outgoing;
}

public void setOutgoing(int outgoing) {
	this.outgoing = outgoing;
}

public int getWaiting() {
	return waiting;
}

public void setWaiting(int waiting) {
	this.waiting = waiting;
}


	
}

