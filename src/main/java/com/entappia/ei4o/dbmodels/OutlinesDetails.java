package com.entappia.ei4o.dbmodels;

import java.util.Date;
import java.util.HashMap;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Convert;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.hibernate.annotations.GenericGenerator;
import org.json.JSONObject;

import com.entappia.ei4o.constants.AppConstants;
import com.entappia.ei4o.converters.CoordinateConverter;
import com.entappia.ei4o.converters.JSONObjectConverter;
import com.fasterxml.jackson.annotation.JsonFormat;

@Entity
@Table(name = "OutlinesDetails")
public class OutlinesDetails {

	@Id
	@Column(nullable = false, columnDefinition = "SMALLINT")
	@GeneratedValue(strategy = GenerationType.AUTO, generator = "native")
	@GenericGenerator(name = "native", strategy = "native")
	private long outlinesId;

	@OneToOne
	@JoinColumn(name = "campusId", referencedColumnName = "campusId")
	CampusDetails campusDetails;

	@Column(length = 20)
	String name;

	@Column(length = 20)
	String placeType;

	@Column(length = 10)
	String wallType;
  
	@Column(length = 20)
	String outlinesArea;

	@Column(length = 10)
	String bgColor;

	@Column(length = 10)
	String geometryType;

	@Convert(converter = CoordinateConverter.class)
	@Column(name = "coordinateList", columnDefinition = "json")
	List<HashMap<String, String>> coordinateList;

	@Temporal(TemporalType.TIMESTAMP)
	@JsonFormat(pattern = "yyyy-MMM-dd HH:mm", timezone = AppConstants.timeZone)
	Date createdDate;

	@Temporal(TemporalType.TIMESTAMP)
	@JsonFormat(pattern = "yyyy-MMM-dd HH:mm", timezone = AppConstants.timeZone)
	Date modifiedDate;

	boolean isActive;

	public long getOutlinesId() {
		return outlinesId;
	}

	public void setOutlinesId(long outlinesId) {
		this.outlinesId = outlinesId;
	}

	public CampusDetails getCampusDetails() {
		return campusDetails;
	}

	public void setCampusDetails(CampusDetails campusDetails) {
		this.campusDetails = campusDetails;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getPlaceType() {
		return placeType;
	}
  
	 
	public String getWallType() {
		return wallType;
	}

	public void setWallType(String wallType) {
		this.wallType = wallType;
	}

	 

	public String getOutlinesArea() {
		return outlinesArea;
	}

	public void setOutlinesArea(String outlinesArea) {
		this.outlinesArea = outlinesArea;
	}

	public void setPlaceType(String placeType) {
		this.placeType = placeType;
	}

	public String getBgColor() {
		return bgColor;
	}

	public void setBgColor(String bgColor) {
		this.bgColor = bgColor;
	}

	public String getGeometryType() {
		return geometryType;
	}

	public void setGeometryType(String geometryType) {
		this.geometryType = geometryType;
	}

	public List<HashMap<String, String>> getCoordinateList() {
		return coordinateList;
	}

	public void setCoordinateList(List<HashMap<String, String>> coordinateList) {
		this.coordinateList = coordinateList;
	}

	public Date getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}

	public Date getModifiedDate() {
		return modifiedDate;
	}

	public void setModifiedDate(Date modifiedDate) {
		this.modifiedDate = modifiedDate;
	}

	public boolean isActive() {
		return isActive;
	}

	public void setActive(boolean isActive) {
		this.isActive = isActive;
	}

}
