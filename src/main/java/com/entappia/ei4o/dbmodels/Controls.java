package com.entappia.ei4o.dbmodels;

import java.util.Date;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Convert;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.entappia.ei4o.constants.AppConstants;
import com.entappia.ei4o.converters.ListConverter;
import com.fasterxml.jackson.annotation.JsonFormat;

@Entity
@Table(name = "Controls")
public class Controls {
	@Id
	@Column(length = 10)
	String controlId;

	@Convert(converter = ListConverter.class)
	@Column(name = "tags", columnDefinition = "json")
	List<String> tags;

	boolean active;

	@Temporal(TemporalType.TIMESTAMP)
	@JsonFormat(pattern = "yyyy-MMM-dd HH:mm:ss", timezone = AppConstants.timeZone)
	Date createdDate;

	@Temporal(TemporalType.TIMESTAMP)
	@JsonFormat(pattern = "yyyy-MMM-dd HH:mm:ss", timezone = AppConstants.timeZone)
	Date modifiedDate;

	public String getControlId() {
		return controlId;
	}

	public void setControlId(String controlId) {
		this.controlId = controlId;
	}

	public List<String> getTags() {
		return tags;
	}

	public void setTags(List<String> tags) {
		this.tags = tags;
	}

	public boolean isActive() {
		return active;
	}

	public void setActive(boolean active) {
		this.active = active;
	}

	public Date getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}

	public Date getModifiedDate() {
		return modifiedDate;
	}

	public void setModifiedDate(Date modifiedDate) {
		this.modifiedDate = modifiedDate;
	}
	
	
}
