package com.entappia.ei4o.dbmodels;

public class AttendancePeopleResult {

	String zoneId;
	String zoneName;
	String tQ;
	Integer NoOfPersons;
	Integer staffcount;
	Integer visitorcount;
	Integer vendorcount;
	Integer contractorcount;
	
	public AttendancePeopleResult(String zoneId, String zoneName, Integer NoOfPersons, Integer staffcount,
			 Integer visitorcount, Integer vendorcount,	Integer contractorcount, String tQ) {
		this.zoneId = zoneId;
		this.zoneName = zoneName;
		this.tQ = tQ;
		this.NoOfPersons = NoOfPersons;
		this.staffcount = staffcount;
		this.visitorcount = visitorcount;
		this.vendorcount = vendorcount;
		this.contractorcount = contractorcount;
		
	}

	public String getZoneId() {
		return zoneId;
	}

	public void setZoneId(String zoneId) {
		this.zoneId = zoneId;
	}

	public String getZoneName() {
		return zoneName;
	}

	public void setZoneName(String zoneName) {
		this.zoneName = zoneName;
	}

	public String gettQ() {
		return tQ;
	}

	public void settQ(String tQ) {
		this.tQ = tQ;
	}

	public Integer getNoOfPersons() {
		return NoOfPersons;
	}

	public void setNoOfPersons(Integer noOfPersons) {
		NoOfPersons = noOfPersons;
	}

	public Integer getStaffcount() {
		return staffcount;
	}

	public void setStaffcount(Integer staffcount) {
		this.staffcount = staffcount;
	}

	public Integer getVisitorcount() {
		return visitorcount;
	}

	public void setVisitorcount(Integer visitorcount) {
		this.visitorcount = visitorcount;
	}

	public Integer getVendorcount() {
		return vendorcount;
	}

	public void setVendorcount(Integer vendorcount) {
		this.vendorcount = vendorcount;
	}

	public Integer getContractorcount() {
		return contractorcount;
	}

	public void setContractorcount(Integer contractorcount) {
		this.contractorcount = contractorcount;
	}
}
