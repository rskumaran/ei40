package com.entappia.ei4o.dbmodels;

public class ZoneFootFallResult {
	
		String zoneId;
		String zoneName;
		String tQ;
		Integer NoOfPersons;
		
		public ZoneFootFallResult(String zoneId, String zoneName, Integer NoOfPersons, String tQ) {
			this.zoneId = zoneId;
			this.zoneName = zoneName;
			this.tQ = tQ;
			this.NoOfPersons = NoOfPersons;
		}

		public String getZoneId() {
			return zoneId;
		}

		public void setZoneId(String zoneId) {
			this.zoneId = zoneId;
		}

		public String getZoneName() {
			return zoneName;
		}

		public void setZoneName(String zoneName) {
			this.zoneName = zoneName;
		}

		public String gettQ() {
			return tQ;
		}

		public void settQ(String tQ) {
			this.tQ = tQ;
		}

		public Integer getNoOfPersons() {
			return NoOfPersons;
		}

		public void setNoOfPersons(Integer noOfPersons) {
			NoOfPersons = noOfPersons;
		}

		
	}
