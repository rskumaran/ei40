package com.entappia.ei4o.dbmodels;

import java.util.Date;
import java.util.HashMap;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Convert;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.hibernate.annotations.GenericGenerator;
import org.json.JSONArray;

import com.entappia.ei4o.constants.AppConstants;
import com.entappia.ei4o.converters.CoordinateConverter;
import com.entappia.ei4o.converters.JSONArrayConverter;
import com.fasterxml.jackson.annotation.JsonFormat;

@Entity
@Table(name = "Doors")
public class Doors {

	@Id
	@Column(nullable = false, columnDefinition = "SMALLINT")
	@GeneratedValue(strategy = GenerationType.AUTO, generator = "native")
	@GenericGenerator(name = "native", strategy = "native")
	private long id;
	
	@OneToOne
    @JoinColumn(name = "outlinesId", referencedColumnName = "outlinesId") 
	OutlinesDetails outlinesDetails;
	
	@Column(length = 30)
	String name;

	@Column(length = 10)
	String colorCode;
	
	@Column(length = 10)
	String wallType;

	@Convert(converter = CoordinateConverter.class)
	@Column(name = "coordinates", columnDefinition = "json")
	List<HashMap<String, String>> coordinates;

	boolean active;

	@Temporal(TemporalType.TIMESTAMP)
	@JsonFormat(pattern = "yyyy-MMM-dd HH:mm:ss", timezone = AppConstants.timeZone)
	Date createdDate;

	@Temporal(TemporalType.TIMESTAMP)
	@JsonFormat(pattern = "yyyy-MMM-dd HH:mm:ss", timezone = AppConstants.timeZone)
	Date modifiedDate;

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public OutlinesDetails getOutlinesDetails() {
		return outlinesDetails;
	}

	public void setOutlinesDetails(OutlinesDetails outlinesDetails) {
		this.outlinesDetails = outlinesDetails;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	 

	public String getWallType() {
		return wallType;
	}

	public void setWallType(String wallType) {
		this.wallType = wallType;
	}

	public String getColorCode() {
		return colorCode;
	}

	public void setColorCode(String colorCode) {
		this.colorCode = colorCode;
	}

	public List<HashMap<String, String>> getCoordinates() {
		return coordinates;
	}

	public void setCoordinates(List<HashMap<String, String>> coordinates) {
		this.coordinates = coordinates;
	}

	public boolean isActive() {
		return active;
	}

	public void setActive(boolean active) {
		this.active = active;
	}

	public Date getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}

	public Date getModifiedDate() {
		return modifiedDate;
	}

	public void setModifiedDate(Date modifiedDate) {
		this.modifiedDate = modifiedDate;
	}
	
	
}
