package com.entappia.ei4o.dbmodels;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.entappia.ei4o.constants.AppConstants;
import com.entappia.ei4o.dbmodels.idclass.VehicleAllocationIdClass;
import com.fasterxml.jackson.annotation.JsonFormat;

@Entity
@Table(name = "VehicleAllocation")
@IdClass(VehicleAllocationIdClass.class)
public class VehicleAllocation implements Serializable {
	
	@Id
	@Column(length = 12)
	String vehicleId;
	
	@Id
	@Column(length = 12)
	String macId;
	
	@Column(length = 20)
	String status;
	
	@Temporal(TemporalType.TIMESTAMP)
	@JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MMM-dd HH:mm:ss", timezone= AppConstants.timeZone)
	Date allocationStartTime;
	
	@Temporal(TemporalType.TIMESTAMP)
	@JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MMM-dd HH:mm:ss", timezone= AppConstants.timeZone)
	Date allocationEndTime;

	public String getVehicleId() {
		return vehicleId;
	}

	public void setVehicleId(String vehicleId) {
		this.vehicleId = vehicleId;
	}

	public String getMacId() {
		return macId;
	}

	public void setMacId(String macId) {
		this.macId = macId;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public Date getAllocationStartTime() {
		return allocationStartTime;
	}

	public void setAllocationStartTime(Date allocationStartTime) {
		this.allocationStartTime = allocationStartTime;
	}

	public Date getAllocationEndTime() {
		return allocationEndTime;
	}

	public void setAllocationEndTime(Date allocationEndTime) {
		this.allocationEndTime = allocationEndTime;
	}
	
	

}
