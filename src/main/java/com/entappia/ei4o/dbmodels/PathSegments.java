package com.entappia.ei4o.dbmodels;

import java.util.Date;
import java.util.HashMap;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Convert;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.hibernate.annotations.GenericGenerator;

import com.entappia.ei4o.constants.AppConstants;
import com.entappia.ei4o.converters.CoordinateConverter;
import com.fasterxml.jackson.annotation.JsonFormat;

@Entity
@Table(name = "PathSegments")
public class PathSegments {

	@Id
	@Column(nullable = false, columnDefinition = "SMALLINT")
	@GeneratedValue(strategy = GenerationType.AUTO, generator = "native")
	@GenericGenerator(name = "native", strategy = "native")
	private long id;

	@OneToOne
	@JoinColumn(name = "campusId", referencedColumnName = "campusId")
	CampusDetails campusDetails;

	@Column(length = 30)
	String name;

	@Column(length = 10)
	String colorCode;
	
	@Column(length = 10)
	String wallType;
	
	@Column(length = 20)
	String textType;

	@Column(length = 20)
	double textAngle;
	
	@Column(length = 10)
	String geometryType;

	@Convert(converter = CoordinateConverter.class)
	@Column(name = "coordinates", columnDefinition = "json")
	List<HashMap<String, String>> coordinates;

	@Temporal(TemporalType.TIMESTAMP)
	@JsonFormat(pattern = "yyyy-MMM-dd HH:mm", timezone = AppConstants.timeZone)
	Date createdDate;

	@Temporal(TemporalType.TIMESTAMP)
	@JsonFormat(pattern = "yyyy-MMM-dd HH:mm", timezone = AppConstants.timeZone)
	Date modifiedDate;

	boolean isActive;

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public CampusDetails getCampusDetails() {
		return campusDetails;
	}

	public void setCampusDetails(CampusDetails campusDetails) {
		this.campusDetails = campusDetails;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getColorCode() {
		return colorCode;
	}

	public void setColorCode(String colorCode) {
		this.colorCode = colorCode;
	}

	public String getWallType() {
		return wallType;
	}

	public void setWallType(String wallType) {
		this.wallType = wallType;
	}

	public String getTextType() {
		return textType;
	}

	public void setTextType(String textType) {
		this.textType = textType;
	}

	public double getTextAngle() {
		return textAngle;
	}

	public void setTextAngle(double textAngle) {
		this.textAngle = textAngle;
	}

	public String getGeometryType() {
		return geometryType;
	}

	public void setGeometryType(String geometryType) {
		this.geometryType = geometryType;
	}

	  
	public List<HashMap<String, String>> getCoordinates() {
		return coordinates;
	}

	public void setCoordinates(List<HashMap<String, String>> coordinates) {
		this.coordinates = coordinates;
	}

	public Date getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}

	public Date getModifiedDate() {
		return modifiedDate;
	}

	public void setModifiedDate(Date modifiedDate) {
		this.modifiedDate = modifiedDate;
	}

	public boolean isActive() {
		return isActive;
	}

	public void setActive(boolean isActive) {
		this.isActive = isActive;
	}
	
	
}
