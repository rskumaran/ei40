package com.entappia.ei4o.dbmodels;

import javax.persistence.ColumnResult;
import javax.persistence.ConstructorResult;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.SqlResultSetMapping;
import javax.persistence.Table;


import com.entappia.ei4o.dbmodels.idclass.LocationIdClass;

@SqlResultSetMapping(
		name="AttendancePeopleResultMapping",
		classes={
				@ConstructorResult(
						targetClass=AttendancePeopleResult.class,
						columns={
								@ColumnResult(name="zoneId", type=String.class),
								@ColumnResult(name="zoneName", type=String.class),
								@ColumnResult(name="NoOfPersons", type=Integer.class),
								@ColumnResult(name="staffcount", type=Integer.class),
								@ColumnResult(name="visitorcount", type=Integer.class),
								@ColumnResult(name="vendorcount", type=Integer.class),
								@ColumnResult(name="contractorcount", type=Integer.class),
								@ColumnResult(name="tQ", type=String.class)
						}
						)
		}
		)
@SqlResultSetMapping(
		name="AttendanceZoneResultMapping",
		classes={
				@ConstructorResult(
						targetClass=AttendanceZoneResult.class,
						columns={
								@ColumnResult(name="zoneId", type=String.class),
								@ColumnResult(name="zoneName", type=String.class),
								@ColumnResult(name="NoOfPersons", type=Integer.class),
								@ColumnResult(name="tQ", type=String.class),
								@ColumnResult(name="zoneRestrictions", type=Integer.class),
								@ColumnResult(name="zoneClassification", type=String.class)
						}
						)
		}
		)
@SqlResultSetMapping(
		name="ZoneFootFallAverageMapping",
		classes={
				@ConstructorResult(
						targetClass=ZoneFootFallAverageResult.class,
						columns={
								@ColumnResult(name="zoneId", type=String.class),
								@ColumnResult(name="zoneName", type=String.class),
								@ColumnResult(name="totalPersons", type=Integer.class),
								@ColumnResult(name="maximum", type=Integer.class),
								@ColumnResult(name="average", type=Double.class)
						}
						)
		}
		)
@SqlResultSetMapping(
		name="ZoneFootFallMapping",
		classes={
				@ConstructorResult(
						targetClass=ZoneFootFallResult.class,
						columns={
								@ColumnResult(name="zoneId", type=String.class),
								@ColumnResult(name="zoneName", type=String.class),
								@ColumnResult(name="NoOfPersons", type=Integer.class),
								@ColumnResult(name="tQ", type=String.class)
						}
						)
		}
		)
@SqlResultSetMapping(
		name="WorkStationInventoryMapping",
		classes={
				@ConstructorResult(
						targetClass=WorkStationInventoryResult.class,
						columns={
								@ColumnResult(name="stationId", type=String.class),
								@ColumnResult(name="waitingAmount", type=Double.class),
								@ColumnResult(name="stationName", type=String.class)
						}
						)
		}
		)
@SqlResultSetMapping(
		name="WorkStationAgingMapping",
		classes={
				@ConstructorResult(
						targetClass=AgingAnalyticsResult.class,
						columns={
								@ColumnResult(name="workStationId", type=String.class),
								@ColumnResult(name="workStationName", type=String.class),
								@ColumnResult(name="workOrderId", type=String.class)
						}
						)
		}
		)
@SqlResultSetMapping(
		name="WorkStationChartMapping",
		classes={
				@ConstructorResult(
						targetClass=WorkStationChartAnalyticsResult.class,
						columns={
								@ColumnResult(name="workStationId", type=String.class),
								@ColumnResult(name="tq", type=String.class),
								@ColumnResult(name="workStationName", type=String.class),
								@ColumnResult(name="wo", type=Integer.class),
								@ColumnResult(name="wi", type=Integer.class),
								@ColumnResult(name="pr", type=Integer.class),
								@ColumnResult(name="tot", type=Integer.class)
						}
						)
		}
		)
@SqlResultSetMapping(
		name="WorkStationMapping",
		classes={
				@ConstructorResult(
						targetClass=WorkStationAnalyticsResult.class,
						columns={
								@ColumnResult(name="stationId", type=String.class),
								@ColumnResult(name="incoming", type=Integer.class),
								@ColumnResult(name="outgoing", type=Integer.class),
								@ColumnResult(name="waiting", type=Integer.class),
								@ColumnResult(name="stationName", type=String.class)
						}
						)
		}
		)
@Entity
@Table(name = "Location")
@IdClass(LocationIdClass.class)
public class Location {
	
	@Id
	 String id;
	 String zone;
	 @Id
	 int time;
	 int grid;
	 String gridPos;
	 String zoneName;
	 int duration;
	 
	public int getDuration() {
		return duration;
	}
	public void setDuration(int duration) {
		this.duration = duration;
	}
	public String getId() {
		return id;
	}
	public void setStaffId(String id) {
		this.id = id;
	}
	public String getZone() {
		return zone;
	}
	public void setZone(String zone) {
		this.zone = zone;
	}
	public int getTime() {
		return time;
	}
	public void setTime(int time) {
		this.time = time;
	}
	public int getGrid() {
		return grid;
	}
	public void setGrid(int grid) {
		this.grid = grid;
	}
	public String getGridPos() {
		return gridPos;
	}
	public void setGridPos(String gridPos) {
		this.gridPos = gridPos;
	}
	public String getZoneName() {
		return zoneName;
	}
	public void setZoneName(String zoneName) {
		this.zoneName = zoneName;
	}
	 

}
