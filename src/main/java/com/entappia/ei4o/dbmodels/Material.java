package com.entappia.ei4o.dbmodels;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.entappia.ei4o.constants.AppConstants;
import com.fasterxml.jackson.annotation.JsonFormat;

@Entity
@Table(name = "Material")
public class Material {

	@Id
	@Column(length = 12)
	String prodId;
	
	@Column(nullable = false, columnDefinition = "MEDIUMINT")
	int ordereNo;
	
	@Temporal(TemporalType.DATE)
	@JsonFormat(pattern = "yyyy-MMM-dd", timezone= AppConstants.timeZone)
	Date issueDate;

	@Column(length = 30)
	String source;

	@Column(length = 30)
	String destination;

	@Column(length = 30)
	String processControl;
	
	@Column(precision = 15, scale = 2)
	float price;
	
	boolean active;
	
	@Temporal(TemporalType.TIMESTAMP)
	@JsonFormat(pattern = "yyyy-MMM-dd HH:mm:ss", timezone= AppConstants.timeZone)
	Date createdDate;
	
	@Temporal(TemporalType.TIMESTAMP)
	@JsonFormat(pattern = "yyyy-MMM-dd HH:mm:ss", timezone= AppConstants.timeZone)
	Date modifiedDate;


	public String getProdId() {
		return prodId;
	}

	public void setProdId(String prodId) {
		this.prodId = prodId;
	}

	public int getOrdereNo() {
		return ordereNo;
	}

	public void setOrdereNo(int ordereNo) {
		this.ordereNo = ordereNo;
	}

	public Date getIssueDate() {
		return issueDate;
	}

	public void setIssueDate(Date issueDate) {
		this.issueDate = issueDate;
	}

	public String getSource() {
		return source;
	}

	public void setSource(String source) {
		this.source = source;
	}

	public String getDestination() {
		return destination;
	}

	public void setDestination(String destination) {
		this.destination = destination;
	}

	public String getProcessControl() {
		return processControl;
	}

	public void setProcessControl(String processControl) {
		this.processControl = processControl;
	}

	public boolean isActive() {
		return active;
	}

	public void setActive(boolean active) {
		this.active = active;
	}

	public Date getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}

	public Date getModifiedDate() {
		return modifiedDate;
	}

	public void setModifiedDate(Date modifiedDate) {
		this.modifiedDate = modifiedDate;
	}

	public float getPrice() {
		return price;
	}

	public void setPrice(float price) {
		this.price = price;
	}

}
