package com.entappia.ei4o.dbmodels;

import java.util.Date;
import java.util.HashMap;

import javax.persistence.Column;
import javax.persistence.Convert;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.entappia.ei4o.constants.AppConstants;
import com.entappia.ei4o.converters.HashMapConverter;
import com.entappia.ei4o.converters.LocationHistoryDataConverter;
import com.entappia.ei4o.models.HistoryLocationData;
import com.entappia.ei4o.repository.LocationHistoryDataRepository;
import com.fasterxml.jackson.annotation.JsonFormat;

@Entity
@Table(name = "LocationHistory")
public class LocationHistory {
	
	
	@Id
	@Temporal(TemporalType.TIMESTAMP)
	@JsonFormat(pattern = "yyyy-MM-dd", timezone = AppConstants.timeZone)
	Date date;
	
	@Convert(converter = LocationHistoryDataConverter.class)
	@Column(name = "locationData", columnDefinition = "json")
	HashMap<String, HashMap<String, HistoryLocationData>> locationData;

	public Date getDate() {
		return date;
	}

	public void setDate(Date date) {
		this.date = date;
	}

	public HashMap<String, HashMap<String, HistoryLocationData>> getLocationData() {
		return locationData;
	}

	public void setLocationData(HashMap<String, HashMap<String, HistoryLocationData>> locationData) {
		this.locationData = locationData;
	}

	
}
