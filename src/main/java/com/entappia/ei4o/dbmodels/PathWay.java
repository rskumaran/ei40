package com.entappia.ei4o.dbmodels;

import java.util.Date;
import java.util.HashMap;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Convert;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.hibernate.annotations.GenericGenerator;

import com.entappia.ei4o.constants.AppConstants;
import com.entappia.ei4o.converters.CoordinateConverter;
import com.entappia.ei4o.converters.WorkStationConverter;
import com.fasterxml.jackson.annotation.JsonFormat;

@Entity
@Table(name = "PathWay")
public class PathWay {

	@Id
	@Column(nullable = false, columnDefinition = "SMALLINT")
	@GeneratedValue(strategy = GenerationType.AUTO, generator = "native")
	@GenericGenerator(name = "native", strategy = "native")
	private long id;

	@OneToOne
	@JoinColumn(name = "campusId", referencedColumnName = "campusId")
	CampusDetails campusDetails;

	@Column(length = 30)
	String name;

	@Column(length = 10)
	String colorCode;

	@Convert(converter = CoordinateConverter.class)
	@Column(name = "coordinates", columnDefinition = "json")
	List<HashMap<String, String>> coordinates;

	@Column(length = 10)
	String geometryType;

	@Column(length = 10)
	String pathwayType;
	
	@Column(length = 10)
	String pathwayPattern ;

	boolean active;

	@Temporal(TemporalType.TIMESTAMP)
	@JsonFormat(pattern = "yyyy-MMM-dd HH:mm:ss", timezone = AppConstants.timeZone)
	Date createdDate;

	@Temporal(TemporalType.TIMESTAMP)
	@JsonFormat(pattern = "yyyy-MMM-dd HH:mm:ss", timezone = AppConstants.timeZone)
	Date modifiedDate;

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public CampusDetails getCampusDetails() {
		return campusDetails;
	}

	public void setCampusDetails(CampusDetails campusDetails) {
		this.campusDetails = campusDetails;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getColorCode() {
		return colorCode;
	}

	public void setColorCode(String colorCode) {
		this.colorCode = colorCode;
	}

	public List<HashMap<String, String>> getCoordinates() {
		return coordinates;
	}

	public void setCoordinates(List<HashMap<String, String>> coordinates) {
		this.coordinates = coordinates;
	}

	public String getGeometryType() {
		return geometryType;
	}

	public void setGeometryType(String geometryType) {
		this.geometryType = geometryType;
	}

	public String getPathwayType() {
		return pathwayType;
	}

	public void setPathwayType(String pathwayType) {
		this.pathwayType = pathwayType;
	}

	public String getPathwayPattern() {
		return pathwayPattern;
	}

	public void setPathwayPattern(String pathwayPattern) {
		this.pathwayPattern = pathwayPattern;
	}

	public boolean isActive() {
		return active;
	}

	public void setActive(boolean active) {
		this.active = active;
	}

	public Date getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}

	public Date getModifiedDate() {
		return modifiedDate;
	}

	public void setModifiedDate(Date modifiedDate) {
		this.modifiedDate = modifiedDate;
	}

}
