package com.entappia.ei4o.converters;

import java.lang.reflect.Type;
import java.util.List;

import javax.persistence.AttributeConverter;
import javax.persistence.Converter;

import com.entappia.ei4o.models.ConfigData;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

@Converter
public class ConfigurationListConverter implements AttributeConverter<List<ConfigData>, String> {

	Gson gson = new Gson();
 	Type listType = new TypeToken<List<ConfigData>>() {
	}.getType();

	@Override
	public String convertToDatabaseColumn(List<ConfigData> data) {
		if (null == data) {
			// You may return null if you prefer that style
			return "{}";
		}

		try { 
			return gson.toJson(data, listType);

		} catch (Exception e) {
			throw new IllegalArgumentException("Error converting map to JSON", e);
		}
	}

	@Override
	public List<ConfigData> convertToEntityAttribute(String s) {
		if (null == s) {
			// You may return null if you prefer that style
			return null;
		}

		try { 
			List<ConfigData> nameEmployeeMap = gson.fromJson(s, listType);

			return nameEmployeeMap;
		} catch (Exception e) {
			throw new IllegalArgumentException("Error converting JSON to map", e);
		}
	}

}
