package com.entappia.ei4o.notification;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Set;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.entappia.ei4o.constants.AppConstants.NotificationType;
import com.entappia.ei4o.dbmodels.Login;
import com.entappia.ei4o.models.Alert;
import com.entappia.ei4o.repository.LoginRegistrationRepository;
import com.entappia.ei4o.services.QuuppaAPIService;
import com.entappia.ei4o.services.SMSEvent;
import com.entappia.ei4o.utils.Emailer;
import com.entappia.ei4o.utils.Utils;
import com.entappia.ei4o.websocket.ScheduledPushMessages;

@Component
public class NotificationMessages {

	@Autowired
	private LoginRegistrationRepository loginRegistrationRepository;

	@Autowired
	private ScheduledPushMessages scheduledPushMessages;

	@Autowired
	private QuuppaAPIService quuppaAPIService;

	@Autowired
	private SMSEvent smsEvent;

	public void testTagAlert() {

		ArrayList<HashMap<String, String>> listTags = new ArrayList<>();
		for (int i = 0; i < 10; i++) {
			HashMap<String, String> map = new HashMap<>();
			map.put("notificationCount", "1");
			map.put("tagName", "Tag00" + i);
			map.put("tagStatus", "lost");

			listTags.add(map);
		}

		ArrayList<String> messageList = getLostTagSMSMessage(listTags);

		for (int i = 0; i < messageList.size(); i += 2) {
			String val = messageList.get(i);
			String val1 = "";
			if (i + 1 < messageList.size() - 1)
				val1 = messageList.get(i + 1);

			System.out.println("val : " + val);
			System.out.println("val1: " + val1);
		}

		/*
		 * messageList.forEach(action -> { System.out.println(action);
		 * System.out.println("length: " + action.length()); });
		 */
	}

	public void lostTagAlert(ArrayList<HashMap<String, String>> listTags) {

		ExecutorService executorService = Executors.newFixedThreadPool(2);
		executorService.execute(new Runnable() {

			public void run() {

				try {

					List<Login> list = loginRegistrationRepository.findAll();

					ArrayList<String> mailBlockedList = new ArrayList<>();
					ArrayList<String> notificationBlockedList = new ArrayList<>();

					ArrayList<String> smsLostNumberList = new ArrayList<>();
					ArrayList<String> smsBlockedNumberList = new ArrayList<>();

					ArrayList<String> mailLostList = new ArrayList<>();
					ArrayList<String> notificationLostList = new ArrayList<>();

					if (list != null && list.size() > 0 && listTags != null && listTags.size() > 0) {

						ArrayList<HashMap<String, String>> lostTags = new ArrayList<>();
						ArrayList<HashMap<String, String>> blockedTags = new ArrayList<>();

						// Split list of tags into two lists- lost and blocked
						for (HashMap<String, String> map : listTags) {
							if (map.get("tagStatus").equalsIgnoreCase("lost")) {
								lostTags.add(map);
							} else if (map.get("tagStatus").equalsIgnoreCase("blocked")) {
								blockedTags.add(map);
							}
						}

						// Get list of mobile no and email ids to send alert from login table
						for (Login login : list) {
							HashMap<String, Alert> orgAlert = login.getOrgAlert();
							if (orgAlert != null) {

								if (blockedTags.size() > 0) {
									Alert blockedtag = orgAlert.getOrDefault("blockedTag", null);
									if (blockedtag != null) {
										if (blockedtag.getSms() == 1 && login.getMobileNumber() != null
												&& !login.getMobileNumber().equals("-")) {
											if (!smsBlockedNumberList.contains(login.getMobileNumber()))
												smsBlockedNumberList.add(login.getMobileNumber());
										}

										if (login.getEmailID() != null) {
											if (blockedtag.getMail() == 1) {
												if (!mailBlockedList.contains(login.getEmailID()))
													mailBlockedList.add(login.getEmailID());
											}

											if (blockedtag.getNotification() == 1) {
												if (!notificationBlockedList.contains(login.getEmailID()))
													notificationBlockedList.add(login.getEmailID());
											}
										}
									}
								}

								if (lostTags.size() > 0) {
									Alert lostTag = orgAlert.getOrDefault("lostTag", null);
									if (lostTag != null) {
										if (lostTag.getSms() == 1 && login.getMobileNumber() != null
												&& !login.getMobileNumber().equals("-")) {
											if (!smsLostNumberList.contains(login.getMobileNumber()))
												smsLostNumberList.add(login.getMobileNumber());

										}

										if (login.getEmailID() != null) {
											if (lostTag.getMail() == 1) {
												if (!mailLostList.contains(login.getEmailID()))
													mailLostList.add(login.getEmailID());
											}

											if (lostTag.getNotification() == 1) {
												if (!notificationLostList.contains(login.getEmailID()))
													notificationLostList.add(login.getEmailID());
											}
										}
									}
								}

							}

						}

						ArrayList<String> commonMailList = new ArrayList<String>(mailLostList);
						commonMailList.retainAll(mailBlockedList);

						for (String commenId : commonMailList) {
							if (mailLostList.contains(commenId))
								mailLostList.remove(commenId);

							if (mailBlockedList.contains(commenId))
								mailBlockedList.remove(commenId);

						}

						// Lost tag alert start
						if (smsLostNumberList.size() > 0) {
							ArrayList<String> messageList = getLostTagSMSMessage(lostTags);
							if (messageList.size() > 0) {
								for (int i = 0; i < messageList.size(); i += 2) {

									String tagId1 = messageList.get(i);
									String tagId2 = "";
									if (i + 1 < messageList.size() - 1)
										tagId2 = messageList.get(i + 1);

									JSONObject jsonObj = null;
									CompletableFuture<JSONObject> occupancyInfoCompletableFuture = smsEvent
											.sentLostTagAlert(smsLostNumberList, tagId1, tagId2);
									CompletableFuture.allOf(occupancyInfoCompletableFuture).join();
									if (occupancyInfoCompletableFuture.isDone()) {
										jsonObj = occupancyInfoCompletableFuture.get();
									}
								}
							}

						}

						if (mailLostList.size() > 0)
							sendMailAlert(lostTags, mailLostList, "Lost");

						if (notificationLostList.size() > 0) {
							String message = "EI40, " + notificationLostList.size() + " lost tags found.";

							scheduledPushMessages.sendWSMessage(message, notificationLostList,
									NotificationType.EMERGENCY);
						}

						// Lost tag alert end

						// Blocked tag alert start
						if (smsBlockedNumberList.size() > 0) {
							ArrayList<String> messageList = getBlockedSMSMessage(blockedTags);
							if (messageList.size() > 0) {
								for (int i = 0; i < messageList.size(); i++) {
									if (!Utils.isEmptyString(messageList.get(i))) {
										JSONObject jsonObj = null;
										CompletableFuture<JSONObject> blockedTagsInfoCompletableFuture = smsEvent
												.sendTagStatusAlert(smsBlockedNumberList, "Blocked",
														messageList.get(i));
										CompletableFuture.allOf(blockedTagsInfoCompletableFuture).join();
										if (blockedTagsInfoCompletableFuture.isDone()) {
											jsonObj = blockedTagsInfoCompletableFuture.get();

										}
									}
								}
							}
						}

						if (mailBlockedList.size() > 0)
							sendMailAlert(blockedTags, mailBlockedList, "Blocked");

						if (notificationBlockedList.size() > 0) {
							String message = "EI40, " + notificationBlockedList.size() + " blocked tags found.";

							scheduledPushMessages.sendWSMessage(message, notificationBlockedList,
									NotificationType.EMERGENCY);
						}

						// Blocked Blocked alert start

						if (commonMailList.size() > 0) {
							sendMailAlert(lostTags, blockedTags, commonMailList);
						}

					}

				} catch (Exception e) {
					e.printStackTrace();
				}

			}
		});
		executorService.shutdown();
	}

	public void latetagAlert(HashMap<String, List<HashMap<String, String>>> lateVisitorTags) {

		ExecutorService executorService = Executors.newFixedThreadPool(2);
		executorService.execute(new Runnable() {

			public void run() {

				try {

					List<Login> list = loginRegistrationRepository.findAll();

					ArrayList<String> smsNumberList = new ArrayList<>();
					ArrayList<String> mailLateVisitorList = new ArrayList<>();
					ArrayList<String> notifictionLateVisitorList = new ArrayList<>();

					if (list != null && list.size() > 0) {
						for (Login login : list) {
							HashMap<String, Alert> orgAlert = login.getOrgAlert();
							if (orgAlert != null) {

								Alert lateVisitor = orgAlert.getOrDefault("lateVisitor", null);
								if (lateVisitor != null) {
									if (lateVisitor.getSms() == 1 && login.getMobileNumber() != null
											&& !login.getMobileNumber().equals("-")) {
										if (!smsNumberList.contains(login.getMobileNumber()))
											smsNumberList.add(login.getMobileNumber());
									}

									if (login.getEmailID() != null) {
										if (lateVisitor.getMail() == 1) {
											mailLateVisitorList.add(login.getEmailID());
										}

										if (lateVisitor.getNotification() == 1) {
											if (!notifictionLateVisitorList.contains(login.getEmailID()))
												notifictionLateVisitorList.add(login.getEmailID());
										}
									}
								}
							}

						}

						if (lateVisitorTags.size() > 0) {
							if (smsNumberList.size() > 0) {
								ArrayList<String> messageList = getSMSMessage(lateVisitorTags);
								if (messageList.size() > 0) {
									for (int i = 0; i < messageList.size(); i++) {
										if (!Utils.isEmptyString(messageList.get(i))) {
											JSONObject jsonObj = null;
											CompletableFuture<JSONObject> lateVisitorInfoCompletableFuture = smsEvent
													.sendTagStatusAlert(smsNumberList, "Late Visitor",
															messageList.get(i));
											CompletableFuture.allOf(lateVisitorInfoCompletableFuture).join();
											if (lateVisitorInfoCompletableFuture.isDone()) {
												jsonObj = lateVisitorInfoCompletableFuture.get();

											}
										}
									}
								}

							}

							if (mailLateVisitorList.size() > 0)
								sendMailAlert(lateVisitorTags, mailLateVisitorList, "Late Visitor");

							if (notifictionLateVisitorList.size() > 0) {
								String message = "EI40, " + notifictionLateVisitorList.size()
										+ " Late Visitor tags found.";

								scheduledPushMessages.sendWSMessage(message, notifictionLateVisitorList,
										NotificationType.EMERGENCY);
							}

						}

					}

				} catch (Exception e) {
					e.printStackTrace();
				}

			}
		});
		executorService.shutdown();
	}

	public void assetValidityAlert(List<HashMap<String, String>> todayMap, List<HashMap<String, String>> next2DaysMap,
			List<HashMap<String, String>> inweekMap, boolean isLSLM, boolean isMonthlyReport) {

		ExecutorService executorService = Executors.newFixedThreadPool(2);
		executorService.execute(new Runnable() {

			public void run() {

				try {
					String type = "assetExpiryAlert";
					if (isLSLM)
						type = "lslmExpiryAlert";

					List<Login> list = loginRegistrationRepository.findAll();

					ArrayList<String> smsNumbersList = new ArrayList<>();
					ArrayList<String> mailList = new ArrayList<>();
					ArrayList<String> notificationList = new ArrayList<>();

					if (list != null && list.size() > 0) {
						for (Login login : list) {
							HashMap<String, Alert> orgAlert = login.getOrgAlert();
							if (orgAlert != null) {

								Alert blockedtag = orgAlert.getOrDefault(type, null);
								if (blockedtag != null) {
									if (login.getMobileNumber() != null && !login.getMobileNumber().equals("-")) {
										if (blockedtag.getSms() == 1) {
											if (!smsNumbersList.contains(login.getMobileNumber())) {
												smsNumbersList.add(login.getMobileNumber());
											}
										}

									}

									if (login.getEmailID() != null) {
										if (blockedtag.getMail() == 1) {
											if (!mailList.contains(login.getEmailID()))
												mailList.add(login.getEmailID());
										}

										if (blockedtag.getNotification() == 1) {
											if (!notificationList.contains(login.getEmailID()))
												notificationList.add(login.getEmailID());
										}
									}
								}

							}

						}

						if (smsNumbersList.size() > 0) {
							sendAssetSMSAlert(todayMap, next2DaysMap, inweekMap, smsNumbersList, isLSLM,
									isMonthlyReport);
						}

						if (mailList.size() > 0) {
							sendAssetMailAlert(todayMap, next2DaysMap, inweekMap, mailList, isLSLM, isMonthlyReport);
						}

						if (notificationList.size() > 0) {
							String message = "EI40, ";

							NotificationType notificationType = NotificationType.INFORMATION;
							if (todayMap.size() > 0) {
								message = message + (isLSLM ? " SKU" : "") + " Asset valitity expire today";
								notificationType = NotificationType.EMERGENCY;
							} else if (next2DaysMap.size() > 0) {
								message = message + (isLSLM ? " SKU" : "") + " Asset valitity expire tomorrow";
								notificationType = NotificationType.EMERGENCY;
							} else {
								message = message + (isLSLM ? " SKU" : "") + " Asset valitity expire in a week";
								notificationType = NotificationType.INFORMATION;
							}

							scheduledPushMessages.sendWSMessage(message, notificationList, notificationType);
						}

					}

				} catch (Exception e) {
					e.printStackTrace();
				}

			}

		});
		executorService.shutdown();

	}

	protected void sendAssetSMSAlert(List<HashMap<String, String>> todayMap, List<HashMap<String, String>> next2DaysMap,
			List<HashMap<String, String>> next2DaysMap2, ArrayList<String> smsNumbersList, boolean isLSLM,
			boolean isMonthlyReport) {
		// TODO Auto-generated method stub

		sendAssetSMSMessage(todayMap, 0, isLSLM, smsNumbersList);
		sendAssetSMSMessage(next2DaysMap, 1, isLSLM, smsNumbersList);
		sendAssetSMSMessage(next2DaysMap2, 2, isLSLM, smsNumbersList);

	}

	private void sendAssetMailAlert(List<HashMap<String, String>> todayMap, List<HashMap<String, String>> next2DaysMap,
			List<HashMap<String, String>> next2DaysMap2, ArrayList<String> mailList, boolean isLSLM,
			boolean isMonthlyReport) {
		// TODO Auto-generated method stub

		String message = getAssetMessage(todayMap, isLSLM, "#ff0000");
		message = message + getAssetMessage(next2DaysMap, isLSLM, "#ff8b00");
		message = message + getAssetMessage(next2DaysMap2, isLSLM, "#00ff00");

		if (!Utils.isEmptyString(message)) {
			Emailer.sendAssetAlert(message, mailList, isLSLM, isMonthlyReport);
		}

	}

	public void zoneOccupancyAlert(ArrayList<HashMap<String, String>> mapList) {

		ExecutorService executorService = Executors.newFixedThreadPool(2);
		executorService.execute(new Runnable() {

			public void run() {

				try {

					List<Login> list = loginRegistrationRepository.findAll();
					ArrayList<String> mailIdList = new ArrayList<>();
					ArrayList<String> notificationList = new ArrayList<>();
					ArrayList<String> smsNumberList = new ArrayList<>();

					if (list != null && list.size() > 0) {
						for (Login login : list) {
							HashMap<String, Alert> orgAlert = login.getOrgAlert();
							if (orgAlert.containsKey("crowdedZones")) {
								Alert alert = orgAlert.get("crowdedZones");
								if (alert != null) {
									if (alert.getMail() == 1) {
										if (!Utils.isEmptyString(login.getEmailID()))
											notificationList.add(login.getEmailID());
									}
									if (alert.getNotification() == 1) {
										if (!Utils.isEmptyString(login.getEmailID()))
											mailIdList.add(login.getEmailID());
									}

									if (alert.getSms() == 1) {
										if (!Utils.isEmptyString(login.getMobileNumber())) {
											if (!smsNumberList.contains(login.getMobileNumber()))
												smsNumberList.add(login.getMobileNumber());

										}
									}
								}
							}

						}
					}

					if (notificationList.size() > 0) {
						String message = getCrowdedZoneNotificationMessage(mapList);
						scheduledPushMessages.sendWSMessage("EI40, Occupancy Alert Zone:" + message, notificationList,
								NotificationType.EMERGENCY);
					}

					if (mailIdList.size() > 0) {
						String mailMessgae = getCrowdedZoneMailMessage(mapList);
						Emailer.sendCrowdedZoneAlert("Occupancy Alert", mailMessgae, mailIdList);
					}

					if (smsNumberList.size() > 0) {

						ArrayList<String> messageList = getCrowdedZoneSMSMessage(mapList);

						if (messageList.size() > 0) {
							for (int i = 0; i < messageList.size(); i++) {
								if (!Utils.isEmptyString(messageList.get(i))) {
									JSONObject jsonObj = null;
									CompletableFuture<JSONObject> occupancyInfoCompletableFuture = smsEvent
											.sentOccupancyAlert(smsNumberList, messageList.get(i));
									CompletableFuture.allOf(occupancyInfoCompletableFuture).join();
									if (occupancyInfoCompletableFuture.isDone()) {
										jsonObj = occupancyInfoCompletableFuture.get();

									}
								}
							}
						}
					}

				} catch (Exception e) {
					e.printStackTrace();
				}

			}
		});
		executorService.shutdown();
	}

	/*
	 * protected boolean sendSMS(ArrayList<HashMap<String, String>> list) {
	 * 
	 * if (list != null) { for (int i = 0; i < list.size(); i++) {
	 * 
	 * HashMap<String, String> map = list.get(i); String notificationCount =
	 * map.get("notificationCount"); if (!Utils.isEmptyString(notificationCount)) {
	 * int count = Integer.parseInt(notificationCount); if (1 == count) return true;
	 * } } } return false; }
	 * 
	 * protected boolean sendSMS(HashMap<String, List<HashMap<String, String>>>
	 * list) { if (list != null) {
	 * 
	 * Set<String> keys = list.keySet();
	 * 
	 * for (String key : keys) {
	 * 
	 * List<HashMap<String, String>> mapList = list.get(key); for (int i = 0; i <
	 * mapList.size(); i++) {
	 * 
	 * HashMap<String, String> map = mapList.get(i); String notificationCount =
	 * map.get("notificationCount"); if (!Utils.isEmptyString(notificationCount)) {
	 * int count = Integer.parseInt(notificationCount); if (1 == count) return true;
	 * }
	 * 
	 * }
	 * 
	 * } } return false; }
	 */

	public void missingTagAlert(HashMap<String, HashMap<String, String>> missingTags) {

		ExecutorService executorService = Executors.newFixedThreadPool(2);
		executorService.execute(new Runnable() {

			public void run() {

				try {

					List<Login> list = loginRegistrationRepository.findAll();
					ArrayList<String> mailIdList = new ArrayList<>();
					ArrayList<String> smsNumberList = new ArrayList<>();

					ArrayList<String> notificationList = new ArrayList<>();

					if (list != null && list.size() > 0) {
						for (Login login : list) {
							HashMap<String, Alert> orgAlert = login.getOrgAlert();
							if (orgAlert.containsKey("missingTag")) {
								Alert alert = orgAlert.get("missingTag");
								if (alert != null) {
									if (alert.getMail() == 1) {
										if (!Utils.isEmptyString(login.getEmailID()))
											mailIdList.add(login.getEmailID());
									}

									if (alert.getNotification() == 1) {
										if (!Utils.isEmptyString(login.getEmailID()))
											notificationList.add(login.getEmailID());
									}

									if (alert.getSms() == 1) {
										if (!Utils.isEmptyString(login.getMobileNumber())
												&& !smsNumberList.contains(login.getMobileNumber())) {
											smsNumberList.add(login.getMobileNumber());
										}
									}
								}
							}

						}
					}

					
					
					Set<String> key = missingTags.keySet();
					String tagIds = String.join(",", key);

					if (smsNumberList.size() > 0) {
						JSONObject jsonObj = null;
						CompletableFuture<JSONObject> lostLocatorInfoCompletableFuture = smsEvent
								.sendTagStatusAlert(smsNumberList, "Missing",
										"Tag Id:" + tagIds );
						CompletableFuture.allOf(lostLocatorInfoCompletableFuture).join();
						if (lostLocatorInfoCompletableFuture.isDone()) {
							jsonObj = lostLocatorInfoCompletableFuture.get();
						}
					}

					if (notificationList.size() > 0)
					{

						String message = "EI40, Missing Tags found, Tag Id:" + tagIds  ;
	 
						scheduledPushMessages.sendWSMessage(message, notificationList, NotificationType.EMERGENCY);
					}

					if (mailIdList.size() > 0) {

						String message = getMissingTagMailMessage(missingTags) ;
						
						Emailer.sendMissingTagAlert("Missing Tags found", message, mailIdList);
					}

				} catch (Exception e) {
					e.printStackTrace();
				}

			}
		});
		executorService.shutdown();
	}

	public void locatorStatusAlert(JSONArray locatorJSONArray) {

		ExecutorService executorService = Executors.newFixedThreadPool(2);
		executorService.execute(new Runnable() {

			public void run() {

				try {

					List<Login> list = loginRegistrationRepository.findAll();
					ArrayList<String> mailIdList = new ArrayList<>();
					ArrayList<String> smsNumberList = new ArrayList<>();
					ArrayList<String> notificationList = new ArrayList<>();

					if (list != null && list.size() > 0) {
						for (Login login : list) {
							HashMap<String, Alert> orgAlert = login.getOrgAlert();
							if (orgAlert.containsKey("lostConnection")) {
								Alert alert = orgAlert.get("lostConnection");
								if (alert != null) {
									if (alert.getMail() == 1) {
										if (!Utils.isEmptyString(login.getEmailID()))
											mailIdList.add(login.getEmailID());
									}

									if (alert.getNotification() == 1) {
										if (!Utils.isEmptyString(login.getEmailID()))
											notificationList.add(login.getEmailID());
									}

									if (alert.getSms() == 1) {
										if (!Utils.isEmptyString(login.getMobileNumber())) {
											if (!smsNumberList.contains(login.getMobileNumber()))
												smsNumberList.add(login.getMobileNumber());
										}
									}
								}
							}

						}
					}

					String smsMessage = getQuuppaLocatorLostMessage(locatorJSONArray);
					String message = "EI40, Lost Locator Connection, Locator Names:-" + smsMessage;

					if (smsNumberList.size() > 0 && !Utils.isEmptyString(smsMessage)) {
						JSONObject jsonObj = null;
						CompletableFuture<JSONObject> lostLocatorInfoCompletableFuture = smsEvent
								.sentLostLocatorAlert(smsNumberList, smsMessage);
						CompletableFuture.allOf(lostLocatorInfoCompletableFuture).join();
						if (lostLocatorInfoCompletableFuture.isDone()) {
							jsonObj = lostLocatorInfoCompletableFuture.get();
						}
					}

					if (notificationList.size() > 0)
						scheduledPushMessages.sendWSMessage(message, notificationList, NotificationType.EMERGENCY);

					if (mailIdList.size() > 0) {

						Emailer.sendLocatorStatusAlert("Lost Locator Connection",
								getQuuppaMailMessage(locatorJSONArray), mailIdList);
					}

				} catch (Exception e) {
					e.printStackTrace();
				}

			}
		});
		executorService.shutdown();
	}

	/*
	 * private void sendSMSAlert(HashMap<String, List<HashMap<String, String>>>
	 * arrayList, ArrayList<String> smsList, String status) { // TODO Auto-generated
	 * method stub
	 * 
	 * String message = "EI40, " + status + " Tags found"; message = message +
	 * getSMSMessage(arrayList); SmsUtil.sendAlertMessage(message, smsList);
	 * 
	 * }
	 */

	/*
	 * private void sendSMSAlert(ArrayList<HashMap<String, String>> arrayList,
	 * ArrayList<String> smsList, String status) { // TODO Auto-generated method
	 * stub
	 * 
	 * String message = "EI40, " + status + " Tags found"; message = message +
	 * getSMSMessage(arrayList); SmsUtil.sendAlertMessage(message, smsList);
	 * 
	 * }
	 */

	public ArrayList<String> getSMSMessage(HashMap<String, List<HashMap<String, String>>> list) {

		String message = "";

		ArrayList<String> messageList = new ArrayList<>();
		if (list != null) {

			Set<String> keys = list.keySet();

			for (String key : keys) {

				List<HashMap<String, String>> mapList = list.get(key);
				for (int i = 0; i < mapList.size(); i++) {

					HashMap<String, String> map = mapList.get(i);

					String notificationCount = map.get("notificationCount");
					if (!Utils.isEmptyString(notificationCount)) {
						if (1 == Integer.parseInt(notificationCount)) {
							String val = map.get("tagName") + ",";
							if (message.length() + val.length() > 30) {
								messageList.add(message.substring(0, message.length() - 1));
								message = val;
							} else
								message = message + val;

						}
					}

					// message = message + ", Tags: " + map.get("tagName");
					// message = message + ", Time: " + map.get("time");
				}

			}

			if (!Utils.isEmptyString(message))
				messageList.add(message.substring(0, message.length() - 1));

		}

		return messageList;
	}

	public ArrayList<String> getLostTagSMSMessage(ArrayList<HashMap<String, String>> list) {

		String message = "";

		ArrayList<String> messageList = new ArrayList<>();

		if (list != null) {

			for (int i = 0; i < list.size(); i++) {

				HashMap<String, String> map = list.get(i);

				String notificationCount = map.get("notificationCount");
				if (!Utils.isEmptyString(notificationCount)) {
					if (1 == Integer.parseInt(notificationCount)) {
						String val = map.get("tagName") + ",";

						if (message.length() + val.length() > 30) {
							messageList.add(message.substring(0, message.length() - 1));
							message = val;
						} else
							message = message + val;

						if (list.size() - 1 == i) {
							messageList.add(message.substring(0, message.length() - 1));
						}
					}

				}
			}

		}

		return messageList;
	}

	public ArrayList<String> getBlockedSMSMessage(ArrayList<HashMap<String, String>> list) {

		String message = "";
		ArrayList<String> messageList = new ArrayList<>();

		if (list != null) {
			for (int i = 0; i < list.size(); i++) {

				HashMap<String, String> map = list.get(i);
				String notificationCount = map.get("notificationCount");
				if (!Utils.isEmptyString(notificationCount)) {
					if (1 == Integer.parseInt(notificationCount)) {
						String val = map.get("tagName") + ",";
						if (message.length() + val.length() > 30) {
							messageList.add(message.substring(0, message.length() - 1));
							message = val;
						} else
							message = message + val;

						if (list.size() - 1 == i) {
							messageList.add(message.substring(0, message.length() - 1));
						}
					}
				}

				/*
				 * message = message + " Zone: " + map.get("zoneName"); message = message +
				 * ", Tags: " + map.get("tagName"); message = message + ", Time: " +
				 * map.get("time");
				 */
			}

		}

		return messageList;
	}

	private void sendMailAlert(ArrayList<HashMap<String, String>> alertLostTags,
			ArrayList<HashMap<String, String>> alertBlockedTags, ArrayList<String> mailBlockedList) {

		String tags = "";

		if (alertLostTags.size() > 0)
			tags = "Lost";

		if (alertBlockedTags.size() > 0) {
			if (Utils.isEmptyString(tags))
				tags = "Blocked";
			else
				tags = tags + ", Blocked";
		}

		String message = "";

		if (alertLostTags.size() > 0)
			message = getMailMessage(alertLostTags, "Lost");

		if (alertBlockedTags.size() > 0)
			message = message + getMailMessage(alertBlockedTags, "Blocked");

		if (!Utils.isEmptyString(tags) && !Utils.isEmptyString(message)) {
			Emailer.sendTagAlert(tags, message, mailBlockedList);
		}

	}

	private void sendMailAlert(HashMap<String, List<HashMap<String, String>>> arrayList,
			ArrayList<String> mailBlockedList, String status) {
		// TODO Auto-generated method stub

		String message = getMailMessage(arrayList, status);
		Emailer.sendTagAlert(status, message, mailBlockedList);

	}

	private void sendMailAlert(ArrayList<HashMap<String, String>> arrayList, ArrayList<String> mailBlockedList,
			String status) {
		// TODO Auto-generated method stub

		String message = getMailMessage(arrayList, status);
		Emailer.sendTagAlert(status, message, mailBlockedList);

	}

	public String getAssetMessage(List<HashMap<String, String>> assetMap, boolean isLSLM, String bgColor) {
		String message = "";

		if (assetMap != null && assetMap.size() > 0) {
			for (HashMap<String, String> map : assetMap) {

				if(isLSLM) {
					message = message + "<tr style='background-color:" + bgColor + ";'>"
							+ "<td style='border: 1px solid #dddddd; padding: 8px; text-align: left'>"
							+  map.get("upcCode") + "</td>"
							+ "<td style='border: 1px solid #dddddd; padding: 8px; text-align: left'>"
							+  map.get("skuNo") + "</td>"
							+ "<td style='border: 1px solid #dddddd; padding: 8px; text-align: left'>"
							+  map.get("batchNo") + "</td>"
							+ "<td style='border: 1px solid #dddddd; padding: 8px; text-align: left'>"
							+  map.get("availableQuantity") + "</td>"
							+ "<td style='border: 1px solid #dddddd; padding: 8px; text-align: left'>"
							+  map.get("description") + "</td>"
							+ "<td style='border: 1px solid #dddddd; padding: 8px; text-align: left'>"
							+ map.get("validityDate") + "</td>" 
							+ "<td style='border: 1px solid #dddddd; padding: 8px; text-align: left'>"
							+ map.get("zoneName") + "</td>" +"</tr>";
				}else
				{
					message = message + "<tr style='background-color:" + bgColor + ";'>"
							+ "<td style='border: 1px solid #dddddd; padding: 8px; text-align: left'>"
							+  map.get("assetId") + "</td>"
							+ "<td style='border: 1px solid #dddddd; padding: 8px; text-align: left'>"
							+  map.get("assetType") + "</td>"
							+ "<td style='border: 1px solid #dddddd; padding: 8px; text-align: left'>"
							+  map.get("subType") + "</td>"
							+ "<td style='border: 1px solid #dddddd; padding: 8px; text-align: left'>"
							+  map.get("type") + "</td>"
							+ "<td style='border: 1px solid #dddddd; padding: 8px; text-align: left'>"
							+  map.get("manufacturer") + "</td>"
							+ "<td style='border: 1px solid #dddddd; padding: 8px; text-align: left'>"
							+ map.get("validityDate") + "</td>"   
							+ "<td style='border: 1px solid #dddddd; padding: 8px; text-align: left'>"
							+ map.get("zoneName") + "</td>" +"</tr>";
				} 
			}
		}

		return message;
	}

	public void sendAssetSMSMessage(List<HashMap<String, String>> assetMap, int dayValue, boolean isLSLM,
			ArrayList<String> smsNumbersList) {
		String message = "";

		if (assetMap != null && assetMap.size() > 0) {

			String assetType = isLSLM ? "SKU Assets" : "Tags";
			String dayStringValue = "";

			if (dayValue == 0)
				dayStringValue = "Today";
			else if (dayValue == 1)
				dayStringValue = "Tomorrow";
			else if (dayValue == 2)
				dayStringValue = "This Week";

			ArrayList<String> messageList = new ArrayList<>();

			for (HashMap<String, String> map : assetMap) {

				String val = "";

				if (isLSLM) {
					val = map.get("upcCode") + ",";
				} else {
					val = map.get("assetId") + ",";
				}

				if (message.length() + val.length() > 30) {
					messageList.add(message.substring(0, message.length() - 1));
					message = val;
				} else
					message = message + val;
			}

			if (!Utils.isEmptyString(message)) {
				messageList.add(message.substring(0, message.length() - 1));
			}

			if (messageList.size() > 0) {

				for (int i = 0; i < messageList.size(); i++) {
					if (!Utils.isEmptyString(messageList.get(i))) {
						JSONObject jsonObj = null;
						CompletableFuture<JSONObject> lateVisitorInfoCompletableFuture = null;
						try {
							lateVisitorInfoCompletableFuture = smsEvent.sendAssetExpiryAlert(smsNumbersList,
									dayStringValue, assetType, messageList.get(i));
							CompletableFuture.allOf(lateVisitorInfoCompletableFuture).join();
							if (lateVisitorInfoCompletableFuture.isDone()) {
								jsonObj = lateVisitorInfoCompletableFuture.get();

							}
						} catch (InterruptedException | ExecutionException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
					}
				}

			}

		}

	}

	public String getMailMessage(ArrayList<HashMap<String, String>> list, String status) {
		String message = "";

		if (list != null) {

			String zoneName = "";
			String tags = "";
			String time = "";

			for (int i = 0; i < list.size(); i++) {

				HashMap<String, String> map = list.get(i);
				if (i == 0) {
					zoneName = map.get("zoneName");
				}

				tags = tags + "<br>" + map.get("tagName") + " ";
				time = time + "<br>" + map.get("time") + " ";
			}

			message = message + "<tr>" + "<td style='border: 1px solid #dddddd; padding: 8px; text-align: left'>"
					+ zoneName + "</td>" + "<td style='border: 1px solid #dddddd; padding: 8px; text-align: left'>"
					+ tags + "</td>" + "<td style='border: 1px solid #dddddd; padding: 8px; text-align: left'>" + time
					+ "</td>" + "<td style='border: 1px solid #dddddd; padding: 8px; text-align: left'>" + status
					+ "</td>" + "</tr>";

		}

		return message;
	}

	public String getMailMessage(HashMap<String, List<HashMap<String, String>>> alertTags, String status) {
		String message = "";

		Set<String> zoneNameSet = alertTags.keySet();
		for (String zoneName : zoneNameSet) {
			List<HashMap<String, String>> list = alertTags.get(zoneName);
			if (list != null) {

				String tags = "";
				String time = "";

				for (int i = 0; i < list.size(); i++) {

					HashMap<String, String> map = list.get(i);

					tags = tags + "<br>" + map.get("tagName") + " ";
					time = time + "<br>" + map.get("time") + " ";
				}

				message = message + "<tr>" + "<td style='border: 1px solid #dddddd; padding: 8px; text-align: left'>"
						+ zoneName + "</td>" + "<td style='border: 1px solid #dddddd; padding: 8px; text-align: left'>"
						+ tags + "</td>" + "<td style='border: 1px solid #dddddd; padding: 8px; text-align: left'>"
						+ time + "</td>" + "<td style='border: 1px solid #dddddd; padding: 8px; text-align: left'>"
						+ status + "</td>" + "</tr>";

			}
		}

		return message;
	}

	public String getSMSMessage(HashMap<String, ArrayList<HashMap<String, String>>> mapValue, String status) {

		String message = "";

		Set<String> zoneIdSet = mapValue.keySet();
		for (String zoneId : zoneIdSet) {
			ArrayList<HashMap<String, String>> list = mapValue.get(zoneId);
			if (list != null) {
				for (int i = 0; i < list.size(); i++) {

					HashMap<String, String> map = list.get(i);
					if (i == 0) {
						message = message + " Zone: " + map.get("zoneName");
						message = message + " Status:" + status;
						message = message + ", Tags: " + map.get("tagName");
						message = message + ", Time: " + map.get("time");
					} else
						message = message + "," + map.get("tagName");

				}
			}
		}

		return message;
	}

	public String getQuuppaLocatorLostMessage(JSONArray jsonArray) {
		String message = "";
		String name = " ";
		if (jsonArray != null) {
			for (int i = 0; i < jsonArray.length(); i++) {
				JSONObject jsonObject;
				try {
					jsonObject = jsonArray.getJSONObject(i);
					if (jsonObject != null) {
						if (Utils.isEmptyString(name))
							name = jsonObject.optString("name");
						else
							name = name + ", " + jsonObject.optString("name");
					}
				} catch (JSONException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}

			}

		}

		return message;
	}
	
	
	public String getMissingTagMailMessage(HashMap<String, HashMap<String, String>> missingTags) {
		String message = "";

		if (missingTags != null) {
			
			Set<String> missingTagIdList = missingTags.keySet(); 
			for (String tagId : missingTagIdList) {
			 
				try {
					HashMap<String, String> map = missingTags.get(tagId);
					if (map != null) {
						message = message + "<tr>"
								+ "<td style='border: 1px solid #dddddd; padding: 8px; text-align: left'>"
								+ tagId + "</td>"
								+ "<td style='border: 1px solid #dddddd; padding: 8px; text-align: left'>"
								+ map.get("macId") + "</td>"
								+ "<td style='border: 1px solid #dddddd; padding: 8px; text-align: left'>"
								+ map.get("lastSeen") + "</td>" + "</tr>";
					}
				} catch (JSONException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}

			}
		}

		return message;
	}

	public String getQuuppaMailMessage(JSONArray jsonArray) {
		String message = "";

		if (jsonArray != null) {
			for (int i = 0; i < jsonArray.length(); i++) {
				JSONObject jsonObject;
				try {
					jsonObject = jsonArray.getJSONObject(i);
					if (jsonObject != null) {
						message = message + "<tr>"
								+ "<td style='border: 1px solid #dddddd; padding: 8px; text-align: left'>"
								+ jsonObject.optString("id") + "</td>"
								+ "<td style='border: 1px solid #dddddd; padding: 8px; text-align: left'>"
								+ jsonObject.optString("name") + "</td>"
								+ "<td style='border: 1px solid #dddddd; padding: 8px; text-align: left'>"
								+ jsonObject.optString("status") + "</td>" + "</tr>";
					}
				} catch (JSONException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}

			}
		}

		return message;
	}

	public ArrayList<String> getCrowdedZoneSMSMessage(ArrayList<HashMap<String, String>> mapList) {
		String message = "";

		ArrayList<String> messageList = new ArrayList<>();

		for (int i = 0; i < mapList.size(); i++) {

			HashMap<String, String> map = mapList.get(i);

			String notificationCount = map.get("notificationCount");
			if (!Utils.isEmptyString(notificationCount)) {
				if (1 == Integer.parseInt(notificationCount)) {
					String val = map.get("zoneName") + "(" + map.get("percentage") + "%)" + ",";
					if (message.length() + val.length() > 30) {
						messageList.add(message.substring(0, message.length() - 1));
						message = val;
					} else
						message = message + val;
				}
			}

			if (!Utils.isEmptyString(message))
				messageList.add(message.substring(0, message.length() - 1));

		}

		return messageList;
	}

	public String getCrowdedZoneNotificationMessage(ArrayList<HashMap<String, String>> mapList) {
		String message = "";

		for (int i = 0; i < mapList.size(); i++) {

			HashMap<String, String> map = mapList.get(i);
			if (i == 0) {
				message = map.get("zoneName") + "(" + map.get("percentage") + "%)";
			} else
				message = message + ", " + map.get("zoneName") + "(" + map.get("percentage") + "%)";

		}

		return message;
	}

	public String getCrowdedZoneMailMessage(ArrayList<HashMap<String, String>> mapList) {
		String message = "";

		for (int i = 0; i < mapList.size(); i++) {

			HashMap<String, String> map = mapList.get(i);

			message = message + "<tr>" + "<td style='border: 1px solid #dddddd; padding: 8px; text-align: left'>"
					+ map.get("zoneName") + "</td>"
					+ "<td style='border: 1px solid #dddddd; padding: 8px; text-align: left'>" + map.get("percentage")
					+ "%</td>" + "</tr>";
		}

		return message;
	}

	// lateAsset functions start from here
	public void lateAssetAlert(HashMap<String, List<HashMap<String, String>>> lateAssetTags) {

		ExecutorService executorService = Executors.newFixedThreadPool(2);
		executorService.execute(new Runnable() {

			public void run() {

				try {

					List<Login> list = loginRegistrationRepository.findAll();

					ArrayList<String> smsNumberList = new ArrayList<>();
					ArrayList<String> mailLateAssetList = new ArrayList<>();
					ArrayList<String> notifictionLateAssetList = new ArrayList<>();

					if (list != null && list.size() > 0) {
						for (Login login : list) {
							HashMap<String, Alert> orgAlert = login.getOrgAlert();
							if (orgAlert != null) {

								Alert lateAsset = orgAlert.getOrDefault("lateAsset", null);
								if (lateAsset != null) {
									if (login.getMobileNumber() != null && !login.getMobileNumber().equals("-")) {
										if (lateAsset.getSms() == 1) {
											if (!smsNumberList.contains(login.getMobileNumber()))
												smsNumberList.add(login.getMobileNumber());
										}
									}

									if (login.getEmailID() != null) {
										if (lateAsset.getMail() == 1) {
											mailLateAssetList.add(login.getEmailID());
										}

										if (lateAsset.getNotification() == 1) {
											if (!notifictionLateAssetList.contains(login.getEmailID()))
												notifictionLateAssetList.add(login.getEmailID());
										}
									}
								}
							}

						}

						if (lateAssetTags.size() > 0) {

							if (smsNumberList.size() > 0) {
								ArrayList<String> messageList = getLateAssetSMSMessage(lateAssetTags);
								if (messageList.size() > 0) {
									for (int i = 0; i < messageList.size(); i++) {
										if (!Utils.isEmptyString(messageList.get(i))) {
											JSONObject jsonObj = null;
											CompletableFuture<JSONObject> lostLocatorInfoCompletableFuture = smsEvent
													.sendTagStatusAlert(smsNumberList, "Late Asset",
															messageList.get(i));
											CompletableFuture.allOf(lostLocatorInfoCompletableFuture).join();
											if (lostLocatorInfoCompletableFuture.isDone()) {
												jsonObj = lostLocatorInfoCompletableFuture.get();
											}
										}
									}
								}
							}

							if (mailLateAssetList.size() > 0)
								sendLateAssetMailAlert(lateAssetTags, mailLateAssetList, "Late Asset");

							if (notifictionLateAssetList.size() > 0) {
								String message = "EI40, " + notifictionLateAssetList.size() + " Late Asset tags found.";

								scheduledPushMessages.sendWSMessage(message, notifictionLateAssetList,
										NotificationType.EMERGENCY);
							}

						}

					}

				} catch (Exception e) {
					e.printStackTrace();
				}

			}
		});
		executorService.shutdown();
	}

	private void sendLateAssetMailAlert(HashMap<String, List<HashMap<String, String>>> arrayList,
			ArrayList<String> mailBlockedList, String status) {
		String message = getLateAssetMailMessage(arrayList, status);
		Emailer.sendLateAssetAlert(status, message, mailBlockedList);

	}

	private ArrayList<String> getLateAssetSMSMessage(HashMap<String, List<HashMap<String, String>>> list) {

		String message = "";

		ArrayList<String> messageList = new ArrayList<>();

		if (list != null) {

			Set<String> keys = list.keySet();

			for (String key : keys) {

				List<HashMap<String, String>> mapList = list.get(key);
				for (int i = 0; i < mapList.size(); i++) {

					HashMap<String, String> map = mapList.get(i);

					String notificationCount = map.get("notificationCount");
					if (!Utils.isEmptyString(notificationCount)) {
						if (1 == Integer.parseInt(notificationCount)) {
							String val = map.get("id") + ",";
							if (message.length() + val.length() > 30) {
								messageList.add(message.substring(0, message.length() - 1));
								message = val;
							} else
								message = message + val;

						}
					}

					// message = message + " id:" + map.get("id");
					// message = message + ", uid: " + map.get("uid");
					// message = message + ", Type: " + map.get("type");
					// message = message + ", Time: " + map.get("time");
				}

			}

			if (!Utils.isEmptyString(message))
				messageList.add(message.substring(0, message.length() - 1));

		}

		return messageList;
	}

	public String getLateAssetMailMessage(HashMap<String, List<HashMap<String, String>>> alertTags, String status) {
		String message = "";

		Set<String> assetTypeSet = alertTags.keySet();
		for (String assetType : assetTypeSet) {
			List<HashMap<String, String>> list = alertTags.get(assetType);
			if (list != null) {

				String assetId = "";
				String time = "";
				String assignedId = "";
				String assignedType = "";

				for (int i = 0; i < list.size(); i++) {

					HashMap<String, String> map = list.get(i);

					assetId = assetId + "<br>" + map.get("id") + " ";
					time = time + "<br>" + map.get("time") + " ";
					assignedId = assignedId + "<br>" + map.get("uid") + " ";
					assignedType = assignedType + "<br>" + map.get("type") + " ";
				}

				message = message + "<tr>" + "<td style='border: 1px solid #dddddd; padding: 8px; text-align: left'>"
						+ assetType + "</td>" + "<td style='border: 1px solid #dddddd; padding: 8px; text-align: left'>"
						+ assetId + "</td>" + "<td style='border: 1px solid #dddddd; padding: 8px; text-align: left'>"
						+ assignedType + "</td>"
						+ "<td style='border: 1px solid #dddddd; padding: 8px; text-align: left'>" + assignedId
						+ "</td>" + "<td style='border: 1px solid #dddddd; padding: 8px; text-align: left'>" + time
						+ "</td>" + "<td style='border: 1px solid #dddddd; padding: 8px; text-align: left'>" + status
						+ "</td>" + "</tr>";

			}
		}

		return message;
	}

	public void restrictedZoneTagAlert(HashMap<String, List<HashMap<String, String>>> restrictedVisitorTags) {

		ExecutorService executorService = Executors.newFixedThreadPool(2);
		executorService.execute(new Runnable() {

			public void run() {

				try {

					List<Login> list = loginRegistrationRepository.findAll();

					ArrayList<String> smsNumberList = new ArrayList<>();
					ArrayList<String> mailRestrictedVisitorList = new ArrayList<>();
					ArrayList<String> notifictionRestrictedVisitorList = new ArrayList<>();

					if (list != null && list.size() > 0) {
						for (Login login : list) {
							HashMap<String, Alert> orgAlert = login.getOrgAlert();
							if (orgAlert != null) {

								Alert restrictedVisitor = orgAlert.getOrDefault("restrictedZoneVisitor", null);
								if (restrictedVisitor != null) {
									if (login.getMobileNumber() != null && !login.getMobileNumber().equals("-")) {
										if (restrictedVisitor.getSms() == 1) {
											if (!smsNumberList.contains(login.getMobileNumber()))
												smsNumberList.add(login.getMobileNumber());
										}
									}

									if (login.getEmailID() != null) {
										if (restrictedVisitor.getMail() == 1) {
											mailRestrictedVisitorList.add(login.getEmailID());
										}

										if (restrictedVisitor.getNotification() == 1) {
											if (!notifictionRestrictedVisitorList.contains(login.getEmailID()))
												notifictionRestrictedVisitorList.add(login.getEmailID());
										}
									}
								}
							}

						}

						if (restrictedVisitorTags.size() > 0) {

							if (smsNumberList.size() > 0) {

								// String message = getSMSMessage(restrictedVisitorTags);

								ArrayList<String> messageList = getSMSMessage(restrictedVisitorTags);
								if (messageList.size() > 0) {
									for (int i = 0; i < messageList.size(); i++) {
										if (!Utils.isEmptyString(messageList.get(i))) {
											JSONObject jsonObj = null;
											CompletableFuture<JSONObject> restrictedZoneInfoCompletableFuture = smsEvent
													.sendZoneAlert(smsNumberList, "Visitor", "Restricted Zone",
															messageList.get(i));
											CompletableFuture.allOf(restrictedZoneInfoCompletableFuture).join();
											if (restrictedZoneInfoCompletableFuture.isDone()) {
												jsonObj = restrictedZoneInfoCompletableFuture.get();

											}
										}
									}
								}

							}

							if (mailRestrictedVisitorList.size() > 0)
								sendMailAlert(restrictedVisitorTags, mailRestrictedVisitorList,
										"Restricted Zone Visitor");

							if (notifictionRestrictedVisitorList.size() > 0) {
								String message = "EI40, " + notifictionRestrictedVisitorList.size()
										+ " Restricted Zone Visitor tags found.";

								scheduledPushMessages.sendWSMessage(message, notifictionRestrictedVisitorList,
										NotificationType.EMERGENCY);
							}

							String macIds = getBuzzerAlertTags(restrictedVisitorTags);
							if (!Utils.isEmptyString(macIds)) {
								quuppaAPIService.sentBuzzerAlert(macIds);
							}
						}

					}

				} catch (Exception e) {
					e.printStackTrace();
				}

			}
		});
		executorService.shutdown();
	}

	public String getBuzzerAlertTags(HashMap<String, List<HashMap<String, String>>> alertTags) {
		String macIds = "";
		Set<String> zoneNameSet = alertTags.keySet();
		for (String zoneName : zoneNameSet) {
			List<HashMap<String, String>> list = alertTags.get(zoneName);
			if (list != null) {
				for (int i = 0; i < list.size(); i++) {

					HashMap<String, String> map = list.get(i);

					if (Utils.isEmptyString(macIds))
						macIds = map.get("macId");
					else
						macIds = macIds + "," + map.get("macId");
				}
			}
		}

		return macIds;
	}

	// Tags found in shipping area functions start from here
	public void shippingAreaAlert(HashMap<String, List<HashMap<String, String>>> shippingAreaTags) {

		ExecutorService executorService = Executors.newFixedThreadPool(2);
		executorService.execute(new Runnable() {

			public void run() {

				try {

					List<Login> list = loginRegistrationRepository.findAll();

					ArrayList<String> smsNumberList = new ArrayList<>();
					ArrayList<String> mailShippingAreaTagsList = new ArrayList<>();
					ArrayList<String> notificationShippingAreaList = new ArrayList<>();

					if (list != null && list.size() > 0) {
						for (Login login : list) {
							HashMap<String, Alert> orgAlert = login.getOrgAlert();
							if (orgAlert != null) {

								Alert shippingAreaTagAlert = orgAlert.getOrDefault("WOshippingZone", null);
								if (shippingAreaTagAlert != null) {
									if (login.getMobileNumber() != null && !login.getMobileNumber().equals("-")) {
										if (shippingAreaTagAlert.getSms() == 1) {
											if (!smsNumberList.contains(login.getMobileNumber()))
												smsNumberList.add(login.getMobileNumber());
										}
									}

									if (login.getEmailID() != null) {
										if (shippingAreaTagAlert.getMail() == 1) {
											mailShippingAreaTagsList.add(login.getEmailID());
										}

										if (shippingAreaTagAlert.getNotification() == 1) {
											if (!notificationShippingAreaList.contains(login.getEmailID()))
												notificationShippingAreaList.add(login.getEmailID());
										}
									}
								}
							}

						}

						if (shippingAreaTags.size() > 0) {

							if (smsNumberList.size() > 0) {
								ArrayList<String> messageList = getShippingAreaTagSMSMessage(shippingAreaTags);

								if (messageList.size() > 0) {
									for (int i = 0; i < messageList.size(); i++) {
										if (!Utils.isEmptyString(messageList.get(i))) {
											JSONObject jsonObj = null;
											CompletableFuture<JSONObject> shippingZoneTagInfoCompletableFuture = smsEvent
													.sendZoneAlert(smsNumberList, "", "Shipping Area",
															messageList.get(i));
											CompletableFuture.allOf(shippingZoneTagInfoCompletableFuture).join();
											if (shippingZoneTagInfoCompletableFuture.isDone()) {
												jsonObj = shippingZoneTagInfoCompletableFuture.get();
											}
										}
									}
								}

							}

							if (mailShippingAreaTagsList.size() > 0)
								sendShippingAreaTagMailAlert(shippingAreaTags, mailShippingAreaTagsList,
										"Shipping Area ");

							if (notificationShippingAreaList.size() > 0) {
								String message = "EI40, " + notificationShippingAreaList.size() + " Shipping Area ";

								scheduledPushMessages.sendWSMessage(message, notificationShippingAreaList,
										NotificationType.EMERGENCY);
							}

						}

					}

				} catch (Exception e) {
					e.printStackTrace();
				}

			}
		});
		executorService.shutdown();
	}

	private ArrayList<String> getShippingAreaTagSMSMessage(HashMap<String, List<HashMap<String, String>>> list) {

		String message = "";
		ArrayList<String> messageList = new ArrayList<>();
		if (list != null) {

			Set<String> keys = list.keySet();

			for (String key : keys) {

				List<HashMap<String, String>> mapList = list.get(key);
				for (int i = 0; i < mapList.size(); i++) {

					HashMap<String, String> map = mapList.get(i);

					String notificationCount = map.get("notificationCount");
					if (!Utils.isEmptyString(notificationCount)) {
						if (1 == Integer.parseInt(notificationCount)) {
							String val = map.get("tagId") + ",";

							if (message.length() + val.length() > 30) {
								messageList.add(message.substring(0, message.length() - 1));
								message = val;
							} else
								message = message + val;
						}
					}
				}

			}

			if (!Utils.isEmptyString(message)) {
				messageList.add(message.substring(0, message.length() - 1));
			}

		}

		return messageList;
	}

	private void sendShippingAreaTagMailAlert(HashMap<String, List<HashMap<String, String>>> arrayList,
			ArrayList<String> mailBlockedList, String status) {
		String message = getShippingZoneTagMailMessage(arrayList, status);
		Emailer.sendShippingAreaTagAlert(status, message, mailBlockedList);

	}

	public String getShippingZoneTagMailMessage(HashMap<String, List<HashMap<String, String>>> alertTags,
			String status) {
		String message = "";

		Set<String> shippingTagsTypeSet = alertTags.keySet();
		for (String workOrderIdKey : shippingTagsTypeSet) {
			List<HashMap<String, String>> list = alertTags.get(workOrderIdKey);
			if (list != null) {

				String workOrderId = "";
				String time = "";
				String tagId = "";
				String zone = "";

				for (int i = 0; i < list.size(); i++) {

					HashMap<String, String> map = list.get(i);

					workOrderId = workOrderId + "<br>" + map.get("workOrderId") + " ";
					time = time + "<br>" + map.get("time") + " ";
					tagId = tagId + "<br>" + map.get("tagId") + " ";
					zone = zone + "<br>" + map.get("zoneName") + " ";
				}

				message = message + "<tr>" + "<td style='border: 1px solid #dddddd; padding: 8px; text-align: left'>"
						+ workOrderIdKey + "</td>"
						+ "<td style='border: 1px solid #dddddd; padding: 8px; text-align: left'>" + tagId + "</td>"
						+ "<td style='border: 1px solid #dddddd; padding: 8px; text-align: left'>" + zone + "</td>"
						+ "<td style='border: 1px solid #dddddd; padding: 8px; text-align: left'>" + time + "</td>"
						+ "<td style='border: 1px solid #dddddd; padding: 8px; text-align: left'>" + status + "</td>"
						+ "</tr>";

			}
		}

		return message;
	}

	// workorder limit crossed area functions start from here
		public void workorderLimitCrossedZoneAlert(HashMap<String, List<HashMap<String, String>>> workOrderAreaTags) {

			ExecutorService executorService = Executors.newFixedThreadPool(2);
			executorService.execute(new Runnable() {

				public void run() {

					try {

						List<Login> list = loginRegistrationRepository.findAll();

						ArrayList<String> smsNumberList = new ArrayList<>();
						ArrayList<String> mailLimitCrossedAreaTagsList = new ArrayList<>();
						ArrayList<String> notificationLimitCrossedAreaList = new ArrayList<>();

						if (list != null && list.size() > 0) {
							for (Login login : list) {
								HashMap<String, Alert> orgAlert = login.getOrgAlert();
								if (orgAlert != null) {

									Alert workorderLimitCrossedAreaTagAlert = orgAlert.getOrDefault("WOLimitInputZoneOutputZone", null);
									if (workorderLimitCrossedAreaTagAlert != null) {
										if (login.getMobileNumber() != null && !login.getMobileNumber().equals("-")) {
											if (workorderLimitCrossedAreaTagAlert.getSms() == 1) {
												if (!smsNumberList.contains(login.getMobileNumber()))
													smsNumberList.add(login.getMobileNumber());
											}
										}

										if (login.getEmailID() != null) {
											if (workorderLimitCrossedAreaTagAlert.getMail() == 1) {
												mailLimitCrossedAreaTagsList.add(login.getEmailID());
											}

											if (workorderLimitCrossedAreaTagAlert.getNotification() == 1) {
												if (!notificationLimitCrossedAreaList.contains(login.getEmailID()))
													notificationLimitCrossedAreaList.add(login.getEmailID());
											}
										}
									}
								}

							}

							if (workOrderAreaTags.size() > 0) {

								if (smsNumberList.size() > 0) {
									ArrayList<String> messageList = getworkorderLimitCrossedAreaTagSMSMessage(workOrderAreaTags);

									if (messageList.size() > 0) {
										for (int i = 0; i < messageList.size(); i++) {
											if (!Utils.isEmptyString(messageList.get(i))) {
												JSONObject jsonObj = null;
												CompletableFuture<JSONObject> workorderLimitZoneTagInfoCompletableFuture = smsEvent
														.sendZoneAlert(smsNumberList, "", "Limit Crossed Area",
																messageList.get(i));
												CompletableFuture.allOf(workorderLimitZoneTagInfoCompletableFuture).join();
												if (workorderLimitZoneTagInfoCompletableFuture.isDone()) {
													jsonObj = workorderLimitZoneTagInfoCompletableFuture.get();
												}
											}
										}
									}

								}

								if (mailLimitCrossedAreaTagsList.size() > 0)
									sendworkorderLimitCrossedAreaTagMailAlert(workOrderAreaTags, mailLimitCrossedAreaTagsList,
											" Limit Crossed Area ");

								if (notificationLimitCrossedAreaList.size() > 0) {
									String message = "EI40, " + notificationLimitCrossedAreaList.size() + " Limit Crossed Area ";

									scheduledPushMessages.sendWSMessage(message, notificationLimitCrossedAreaList,
											NotificationType.EMERGENCY);
								}

							}

						}

					} catch (Exception e) {
						e.printStackTrace();
					}

				}
			});
			executorService.shutdown();
		}

		private ArrayList<String> getworkorderLimitCrossedAreaTagSMSMessage(HashMap<String, List<HashMap<String, String>>> list) {

			String message = "";
			ArrayList<String> messageList = new ArrayList<>();
			if (list != null) {

				Set<String> keys = list.keySet();

				for (String key : keys) {

					List<HashMap<String, String>> mapList = list.get(key);
					for (int i = 0; i < mapList.size(); i++) {

						HashMap<String, String> map = mapList.get(i);

						String notificationCount = map.get("notificationCount");
						if (!Utils.isEmptyString(notificationCount)) {
							if (1 == Integer.parseInt(notificationCount)) {
								String val = map.get("tagId") + ",";

								if (message.length() + val.length() > 30) {
									messageList.add(message.substring(0, message.length() - 1));
									message = val;
								} else
									message = message + val;
							}
						}
					}

				}

				if (!Utils.isEmptyString(message)) {
					messageList.add(message.substring(0, message.length() - 1));
				}

			}

			return messageList;
		}

		private void sendworkorderLimitCrossedAreaTagMailAlert(HashMap<String, List<HashMap<String, String>>> arrayList,
				ArrayList<String> mailBlockedList, String status) {
			String message = getworkorderLimitCrossedTagMailMessage(arrayList, status);
			Emailer.sendworkorderLimitCrossedAreaTagAlert(status, message, mailBlockedList);

		}

		public String getworkorderLimitCrossedTagMailMessage(HashMap<String, List<HashMap<String, String>>> alertTags,
				String status) {
			String message = "";

			Set<String> shippingTagsTypeSet = alertTags.keySet();
			for (String workOrderIdKey : shippingTagsTypeSet) {
				List<HashMap<String, String>> list = alertTags.get(workOrderIdKey);
				if (list != null) {

					String workOrderId = "";
					String time = "";
					String tagId = "";
					String zone = "";

					for (int i = 0; i < list.size(); i++) {

						HashMap<String, String> map = list.get(i);

						workOrderId = workOrderId + "<br>" + map.get("workOrderId") + " ";
						time = time + "<br>" + map.get("time") + " ";
						tagId = tagId + "<br>" + map.get("tagId") + " ";
						zone = zone + "<br>" + map.get("zoneName") + " ";
					}

					message = message + "<tr>" + "<td style='border: 1px solid #dddddd; padding: 8px; text-align: left'>"
							+ workOrderIdKey + "</td>"
							+ "<td style='border: 1px solid #dddddd; padding: 8px; text-align: left'>" + tagId + "</td>"
							+ "<td style='border: 1px solid #dddddd; padding: 8px; text-align: left'>" + zone + "</td>"
							+ "<td style='border: 1px solid #dddddd; padding: 8px; text-align: left'>" + time + "</td>"
							+ "<td style='border: 1px solid #dddddd; padding: 8px; text-align: left'>" + status + "</td>"
							+ "</tr>";

				}
			}

			return message;
		}

		
	

}
