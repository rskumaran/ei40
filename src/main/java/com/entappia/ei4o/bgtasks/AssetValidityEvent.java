package com.entappia.ei4o.bgtasks;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import com.entappia.ei4o.constants.AppConstants;
import com.entappia.ei4o.constants.AppConstants.AssignmentStatus;
import com.entappia.ei4o.constants.AppConstants.AssignmentType;
import com.entappia.ei4o.dbmodels.AssetChrono;
import com.entappia.ei4o.dbmodels.AssetChronoAllocation;
import com.entappia.ei4o.dbmodels.SKUAllocation;
import com.entappia.ei4o.dbmodels.TagPosition;
import com.entappia.ei4o.dbmodels.Tags;
import com.entappia.ei4o.notification.NotificationMessages;
import com.entappia.ei4o.repository.AssetChronoAllocationRepository;
import com.entappia.ei4o.repository.AssetChronoRepository;
import com.entappia.ei4o.repository.AssetNonChronoAllocationRepository;
import com.entappia.ei4o.repository.LocationHistoryDataRepository;
import com.entappia.ei4o.repository.SKUAllocationRepository;
import com.entappia.ei4o.repository.TagPositionRepository;
import com.entappia.ei4o.repository.TagsRepository;
import com.entappia.ei4o.services.LogsServices;
import com.entappia.ei4o.utils.Preference;

@Component
public class AssetValidityEvent {

	@Autowired
	private LogsServices logsServices;

	@Autowired
	private AssetChronoRepository assetChronoRepository;

	@Autowired
	private AssetChronoAllocationRepository assetChronoAllocationRepository;

	@Autowired
	private TagPositionRepository tagPositionRepository;

	@Autowired
	private LocationHistoryDataRepository locationHistoryDataRepository;

	@Autowired
	private SKUAllocationRepository skuAllocationRepository;

	@Autowired
	private TagsRepository tagsRepository;

	@Autowired
	private NotificationMessages notificationMessages;

	SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MMM-dd");
	Preference preference = new Preference();


	@Scheduled(cron = "0 0 10 * * *")
	//@Scheduled(cron = "5 * * * * *")
	public void getTagPositions() {

		ExecutorService executorService = Executors.newFixedThreadPool(2);
		executorService.execute(new Runnable() {

			public void run() {

				try {

					//Remove Location History data by TRANSACTION_TABLE_AGE Date 
					Calendar calendar = Calendar.getInstance();
					calendar.add(Calendar.DAY_OF_MONTH, -preference.getIntValue(AppConstants.TRANSACTION_TABLE_AGE, 30));
					Date curDate = simpleDateFormat.parse(simpleDateFormat.format(calendar.getTime()));
					locationHistoryDataRepository.deleteByDateLessThan(curDate);

					Calendar cal = Calendar.getInstance();

					int date = cal.get(Calendar.DAY_OF_MONTH);
					Date fromDate = simpleDateFormat.parse(simpleDateFormat.format(cal.getTime()));
					cal.add(Calendar.DAY_OF_MONTH, 6);
					Date toDate = simpleDateFormat.parse(simpleDateFormat.format(cal.getTime()));

					Calendar cal1 = Calendar.getInstance();
					cal1.add(Calendar.DAY_OF_MONTH, 1);
					Date nxt2Date = simpleDateFormat.parse(simpleDateFormat.format(cal1.getTime()));

					List<HashMap<String, String>> todayMap = new ArrayList<>();
					List<HashMap<String, String>> next2DaysMap = new ArrayList<>();
					List<HashMap<String, String>> inweekMap = new ArrayList<>();

					List<TagPosition> tagPositionListFromDB = tagPositionRepository.findAll();
					HashMap<String, String> zoneMapDetails = new HashMap<String, String>();
					for (int i = 0; i < tagPositionListFromDB.size(); i++) {

						TagPosition tagPosition = tagPositionListFromDB.get(i);
						zoneMapDetails.put(tagPosition.getMacId(), tagPosition.getZoneName());
					}

					List<AssetChrono> assetChronoList = assetChronoRepository.getAssetChronos(fromDate, toDate);
					if (assetChronoList != null) {

						List<AssetChronoAllocation> assetChronoAllocationList = assetChronoAllocationRepository
								.findByAssignmentTypeAndStatus(AssignmentType.TAG, "Valid");

						HashMap<String, String> assetIdZoneMap = new HashMap<String, String>();
						for (int i = 0; i < assetChronoAllocationList.size(); i++) {
							AssetChronoAllocation assetChronoAllocation = assetChronoAllocationList.get(i);
							if (zoneMapDetails.containsKey(assetChronoAllocation.getMacId()))
								assetIdZoneMap.put(assetChronoAllocation.getAssetId(),
										zoneMapDetails.get(assetChronoAllocation.getMacId()));
						}

						for (AssetChrono assetChrono : assetChronoList) {
							if (assetChrono != null) {

								HashMap<String, String> map = new HashMap<>();
								map.put("assetId", assetChrono.getAssetId());
								map.put("type", assetChrono.getType());
								map.put("subType", assetChrono.getSubType());
								map.put("assetType", assetChrono.getAssetType());
								map.put("manufacturer", assetChrono.getManufacturer());

								if (assetChrono.getAssignmentStatus() == AssignmentStatus.ASSIGNED) {
									if (assetIdZoneMap.containsKey(assetChrono.getAssetId()))
										map.put("zoneName", assetIdZoneMap.get(assetChrono.getAssetId()));
									else
										map.put("zoneName", "");
								} else {
									map.put("zoneName", "");
								}

								if (fromDate.equals(assetChrono.getValidityDate())) {
									map.put("validityDate", "Today");
									todayMap.add(map);
								} else {

									if (assetChrono.getValidityDate().before(nxt2Date)
											|| assetChrono.getValidityDate().equals(nxt2Date)) {
										map.put("validityDate", "Tomorrow");
										next2DaysMap.add(map);
									} else {

										map.put("validityDate", simpleDateFormat.format(assetChrono.getValidityDate()));
										inweekMap.add(map);
									}
								}
							}
						}

					}

					if (todayMap.size() > 0 || next2DaysMap.size() > 0 || inweekMap.size() > 0) {
						notificationMessages.assetValidityAlert(todayMap, next2DaysMap, inweekMap, false, false);
					}

					List<HashMap<String, String>> todaySKUMap = new ArrayList<>();
					List<HashMap<String, String>> next2DaysSKUMap = new ArrayList<>();
					List<HashMap<String, String>> inweekSKUMap = new ArrayList<>();

					List<SKUAllocation> SKUAllocationList = skuAllocationRepository.findBySKUAllocations(fromDate,
							toDate);
					if (SKUAllocationList != null) {

						List<Tags> tagsListFromDB = tagsRepository.findAll();
						HashMap<String, String> zoneMapSKUDetails = new HashMap<String, String>();
						for (int i = 0; i < tagsListFromDB.size(); i++) {

							Tags tags = tagsListFromDB.get(i);
							if (zoneMapDetails.containsKey(tags.getMacId()))
								zoneMapSKUDetails.put(tags.getTagId(), zoneMapDetails.get(tags.getMacId()));

						}

						for (SKUAllocation skuAllocation : SKUAllocationList) {
							if (skuAllocation != null) {

								HashMap<String, String> map = new HashMap<>();
								map.put("upcCode", skuAllocation.getUpcCode());
								map.put("skuNo", skuAllocation.getSkuNo());
								map.put("batchNo", skuAllocation.getBatchNo());
								map.put("availableQuantity", "" + skuAllocation.getAvailableQuantity());
								map.put("description", skuAllocation.getDescription());

								if (zoneMapSKUDetails.containsKey(skuAllocation.getTagId()))
									map.put("zoneName", zoneMapSKUDetails.get(skuAllocation.getTagId()));
								else
									map.put("zoneName", "");

								if (fromDate.equals(skuAllocation.getExpiryDate())) {
									map.put("validityDate", "Today");
									todaySKUMap.add(map);
								} else {

									if (skuAllocation.getExpiryDate().before(nxt2Date)
											|| skuAllocation.getExpiryDate().equals(nxt2Date)) {
										map.put("validityDate", "Tomorrow");
										next2DaysSKUMap.add(map);
									} else {

										map.put("validityDate", simpleDateFormat.format(skuAllocation.getExpiryDate()));
										inweekSKUMap.add(map);
									}
								}

							}
						}
					}

					if (todaySKUMap.size() > 0 || next2DaysSKUMap.size() > 0 || inweekSKUMap.size() > 0) {
						notificationMessages.assetValidityAlert(todaySKUMap, next2DaysSKUMap, inweekSKUMap, true,
								false);
					}

					if (date == 1) {
						monthlyReport();
					}

				} catch (ParseException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		});
		executorService.shutdown();
	}

	public void monthlyReport() {
		try {

			Calendar nextDayCal = Calendar.getInstance();
			nextDayCal.set(Calendar.DAY_OF_MONTH, 1);
			Date nxtDate = simpleDateFormat.parse(simpleDateFormat.format(nextDayCal.getTime()));

			Calendar cal = Calendar.getInstance();

			Date fromDate = simpleDateFormat.parse(simpleDateFormat.format(cal.getTime()));

			cal.set(Calendar.DAY_OF_MONTH, 1);
			cal.add(Calendar.MONTH, 1);
			Date nxtMonth = simpleDateFormat.parse(simpleDateFormat.format(cal.getTime()));

			cal.add(Calendar.MONTH, 1);
			Date thirdMonth = simpleDateFormat.parse(simpleDateFormat.format(cal.getTime()));

			List<HashMap<String, String>> thisMonthMap = new ArrayList<>();
			List<HashMap<String, String>> nextMonthMap = new ArrayList<>();
			List<HashMap<String, String>> otherMonthsMap = new ArrayList<>();

			List<TagPosition> tagPositionListFromDB = tagPositionRepository.findAll();
			HashMap<String, String> zoneMapDetails = new HashMap<String, String>();
			for (int i = 0; i < tagPositionListFromDB.size(); i++) {

				TagPosition tagPosition = tagPositionListFromDB.get(i);
				zoneMapDetails.put(tagPosition.getMacId(), tagPosition.getZoneName());
			}

			List<AssetChrono> assetChronoList = assetChronoRepository.getAssetChronos(fromDate);
			if (assetChronoList != null) {

				List<AssetChronoAllocation> assetChronoAllocationList = assetChronoAllocationRepository
						.findByAssignmentTypeAndStatus(AssignmentType.TAG, "Valid");

				HashMap<String, String> assetIdZoneMap = new HashMap<String, String>();
				for (int i = 0; i < assetChronoAllocationList.size(); i++) {
					AssetChronoAllocation assetChronoAllocation = assetChronoAllocationList.get(i);
					if (zoneMapDetails.containsKey(assetChronoAllocation.getMacId()))
						assetIdZoneMap.put(assetChronoAllocation.getAssetId(),
								zoneMapDetails.get(assetChronoAllocation.getMacId()));
				}

				for (AssetChrono assetChrono : assetChronoList) {
					if (assetChrono != null) {

						HashMap<String, String> map = new HashMap<>();
						map.put("assetId", assetChrono.getAssetId());
						map.put("type", assetChrono.getType());
						map.put("subType", assetChrono.getSubType());
						map.put("assetType", assetChrono.getAssetType());
						map.put("manufacturer", assetChrono.getManufacturer());

						if (assetChrono.getAssignmentStatus() == AssignmentStatus.ASSIGNED) {
							if (assetIdZoneMap.containsKey(assetChrono.getAssetId()))
								map.put("zoneName", assetIdZoneMap.get(assetChrono.getAssetId()));
							else
								map.put("zoneName", "");
						} else {
							map.put("zoneName", "");
						}

						if (assetChrono.getValidityDate().before(nxtMonth)) {
							if (fromDate.equals(assetChrono.getValidityDate())) {
								map.put("validityDate", "Today");
								thisMonthMap.add(map);
							} else if (nxtDate.equals(assetChrono.getValidityDate())) {
								map.put("validityDate", "Tomorrow");
								thisMonthMap.add(map);
							} else {
								map.put("validityDate", simpleDateFormat.format(assetChrono.getValidityDate()));
								thisMonthMap.add(map);
							}
						} else if (assetChrono.getValidityDate().before(thirdMonth)) {
							map.put("validityDate", simpleDateFormat.format(assetChrono.getValidityDate()));
							nextMonthMap.add(map);
						} else {
							map.put("validityDate", simpleDateFormat.format(assetChrono.getValidityDate()));
							otherMonthsMap.add(map);
						}
					}
				}

			}

			if (thisMonthMap.size() > 0 || nextMonthMap.size() > 0 || otherMonthsMap.size() > 0) {
				notificationMessages.assetValidityAlert(thisMonthMap, nextMonthMap, otherMonthsMap, false, true);
			}

		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}
}
