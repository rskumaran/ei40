package com.entappia.ei4o.bgtasks;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Set;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.concurrent.ThreadPoolTaskScheduler;
import org.springframework.scheduling.support.CronTrigger;
import org.springframework.stereotype.Component;

import com.entappia.ei4o.constants.AppConstants;
import com.entappia.ei4o.constants.AppConstants.NotificationType;
import com.entappia.ei4o.dbmodels.Notifications;
import com.entappia.ei4o.dbmodels.ProfileConfiguration;
import com.entappia.ei4o.dbmodels.TagPosition;
import com.entappia.ei4o.dbmodels.Tags;
import com.entappia.ei4o.models.ConfigData;
import com.entappia.ei4o.models.NotificationDetailMessages;
import com.entappia.ei4o.notification.NotificationMessages;
import com.entappia.ei4o.repository.NotificationsRepository;
import com.entappia.ei4o.repository.ProfileConfigurationRepository;
import com.entappia.ei4o.repository.TagPositionRepository;
import com.entappia.ei4o.repository.TagsRepository;
import com.entappia.ei4o.services.LogsServices;
import com.entappia.ei4o.services.QuuppaAPIService;
import com.entappia.ei4o.utils.Preference;
import com.entappia.ei4o.utils.Utils;

@Component
public class MissingTagAlertTask {

	private static ThreadPoolTaskScheduler missingTagAlertTaskcheduler;
	private static boolean isRunning;

	@Autowired
	private TagPositionRepository tagPositionRepository;

	@Autowired
	private TagsRepository tagsRepository;

	@Autowired
	private ProfileConfigurationRepository profileConfigurationRepository;

	@Autowired
	private LogsServices logsServices;

	@Autowired
	private NotificationMessages notificationMessages;

	@Autowired
	private NotificationsRepository notificationRepository;

	@Autowired
	private QuuppaAPIService quuppaAPIService;

	SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MMM-dd hh:mm a");

	Preference preference = new Preference();

	public void createSchedule() {

		if (missingTagAlertTaskcheduler == null) {
			missingTagAlertTaskcheduler = new ThreadPoolTaskScheduler();
			missingTagAlertTaskcheduler.setPoolSize(5);
			missingTagAlertTaskcheduler.setBeanName("asyncExecutor");
			missingTagAlertTaskcheduler.initialize();
		}

		startSchedule();
	}

	private void startSchedule() {

		if (!isRunning) {

			String cronTime = "0 0 0/" + preference.getIntValue(AppConstants.MISSING_TAG_ALERT, 2) + " * * *";

			// String cronTime = "1 * * * * *";

			if (AppConstants.print_log)
				System.out.println("MissingTagAlertTask cronTime: " + cronTime);

			if (missingTagAlertTaskcheduler != null)
				missingTagAlertTaskcheduler.schedule(new RunnableTask(), new CronTrigger(cronTime));

			logsServices.addLog(AppConstants.event, AppConstants.BG_TASK, "MissingTagAlert",
					"Missing Tag Alert Taskcheduler started, Interval time is "
							+ preference.getIntValue(AppConstants.MISSING_TAG_ALERT, 2),
					null);

			isRunning = true;
		}
	}

	public void stopSchedule() {
		if (missingTagAlertTaskcheduler != null) {
			missingTagAlertTaskcheduler.getScheduledThreadPoolExecutor().shutdown();
			missingTagAlertTaskcheduler.shutdown();
			logsServices.addLog(AppConstants.event, AppConstants.BG_TASK, "MissingTagAlert",
					"Missing Tag Alert Taskcheduler stopped", null);

		}

		missingTagAlertTaskcheduler = null;
		isRunning = false;
	}

	public void changeScheduleTime() {
		stopSchedule();
		createSchedule();
		startSchedule();
	}

	class RunnableTask implements Runnable {

		public RunnableTask() {
		}

		@SuppressWarnings("unused")
		@Override
		public void run() {

			try {

				Calendar calendar = Calendar.getInstance();
				calendar.set(Calendar.HOUR_OF_DAY, -preference.getIntValue(AppConstants.MISSING_TAG_ALERT, 2));

				long timeInMillis = calendar.getTimeInMillis();

				List<String> preAlertedMacIdList = new ArrayList<>();
				String macIds = preference.getStringValue(AppConstants.MISSING_TAG_MAC_IDS, "");
				if (!Utils.isEmptyString(macIds)) {
					preAlertedMacIdList = Arrays.asList(macIds.split(","));

					if (preAlertedMacIdList == null)
						preAlertedMacIdList = new ArrayList<>();
				}

				List<TagPosition> tagPositionListFromDB = tagPositionRepository.findAll();
				HashMap<String, Long> missingMacIdList = new HashMap<String, Long>();
				for (int i = 0; i < tagPositionListFromDB.size(); i++) {

					TagPosition tagPosition = tagPositionListFromDB.get(i);
					if (tagPosition.getLastSeen() < timeInMillis) {
						missingMacIdList.put(tagPosition.getMacId(), tagPosition.getLastSeen());
					} else {
						if (!preAlertedMacIdList.contains(tagPosition.getMacId())) {
							preAlertedMacIdList.remove(tagPosition.getMacId());
						}
					}
				}

				HashMap<String, HashMap<String, String>> alertMacIdList = new HashMap<String, HashMap<String, String>>();

				List<Tags> tagList = tagsRepository.findAll();
				HashMap<String, Tags> tagsMap = new HashMap<String, Tags>();

				if (tagList != null) {
					tagList.forEach(tag -> {
						tagsMap.put(tag.getMacId(), tag);
					});
				}

				if (missingMacIdList.size() > 0) {

					Set<String> macIdSet = missingMacIdList.keySet();

					for (String macId : macIdSet) {
						if (!preAlertedMacIdList.contains(macId) && missingMacIdList.containsKey(macId)) {
							HashMap<String, String> map = new HashMap<>();
							map.put("macId", macId);
							map.put("lastSeen", simpleDateFormat.format(new Date(missingMacIdList.get(macId))));
							if (tagsMap.get(macId) != null) {
								alertMacIdList.put(tagsMap.get(macId).getTagId(), map);
								preAlertedMacIdList.add(macId);
							}
						}
					}
				}

				if (alertMacIdList.size() > 0) {
					Set<String> key = alertMacIdList.keySet();

					List<HashMap<String, String>> data = new ArrayList<>();
					for (String tagId : key) {
						HashMap<String, String> map = new HashMap<>();
						map.put("tagId", tagId);
						map.put("macId", alertMacIdList.get(tagId).get("macId"));
						map.put("lastSeen", alertMacIdList.get(tagId).get("lastSeen"));
						data.add(map);
					}

					Notifications notification = new Notifications();
					notification.setMessage(key.size() + " Missing Tag ids found");
					notification.setNotifyTime(new Date());
					notification.setType(NotificationType.WARNING);

					NotificationDetailMessages notificationDetailMessages = new NotificationDetailMessages();
					notificationDetailMessages.setType("Missing Tags");
					notificationDetailMessages.setData(data);
					notification.setMessageObj(notificationDetailMessages);
					notification.setSource("EI40");
					notificationRepository.save(notification);

					notificationMessages.missingTagAlert(alertMacIdList);
				}

				String alertedMacId = String.join(",", preAlertedMacIdList);
				preference.setStringValue(AppConstants.MISSING_TAG_MAC_IDS, alertedMacId);

				updateBatteryStatus();

				// Check config data for schedule time
				List<ProfileConfiguration> profileConfigurationList = profileConfigurationRepository.findAll();

				if (profileConfigurationList != null && profileConfigurationList.size() > 0) {
					ProfileConfiguration profileConfiguration = profileConfigurationList.get(0);
					if (profileConfiguration != null) {

						List<ConfigData> configDataList = profileConfiguration.getConfigData();
						if (configDataList != null) {
							for (ConfigData configData : configDataList) {
								if (configData != null) {
									if (configData.getName().equalsIgnoreCase("Missing Tag Alert")) {

										if (preference.getIntValue(AppConstants.MISSING_TAG_ALERT, 2) != Integer
												.valueOf(configData.getDefaultval())) {
											preference.setIntValue(AppConstants.MISSING_TAG_ALERT,
													Integer.valueOf(configData.getDefaultval()));
											changeScheduleTime();
										}

									} else if (configData.getName().equalsIgnoreCase("Transaction Table Age")) {

										if (preference.getIntValue(AppConstants.TRANSACTION_TABLE_AGE, 2) != Integer
												.valueOf(configData.getDefaultval())) {
											preference.setIntValue(AppConstants.TRANSACTION_TABLE_AGE,
													Integer.valueOf(configData.getDefaultval()));
										}

									}
								}
							}
						}
					}
				}

			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

		}
	}


	// Checking tag Battery status and updating tag table 
	public void updateBatteryStatus() {

		ExecutorService executorService = Executors.newFixedThreadPool(2);
		executorService.execute(new Runnable() {

			public void run() {

				CompletableFuture<HashMap<String, HashMap<String, Object>>> batteryStatusInfoCompletableFuture;
				try {
					batteryStatusInfoCompletableFuture = quuppaAPIService.getBatteryInfo();

					CompletableFuture.allOf(batteryStatusInfoCompletableFuture).join();
					if (batteryStatusInfoCompletableFuture.isDone()) {

						HashMap<String, HashMap<String, Object>> batteryInfoMap;

						batteryInfoMap = batteryStatusInfoCompletableFuture.get();

						if (batteryInfoMap != null && batteryInfoMap.size()>0) {

							List<Tags> tagsList = tagsRepository.findAll();
							for (Tags tags : tagsList) {
								HashMap<String, Object> batteryInfo = batteryInfoMap.getOrDefault(tags.getMacId(),
										null);
								if (batteryInfo != null) {
									if (Double.valueOf(batteryInfo.get("batteryVoltage").toString()) <= 0) {
										tags.setBatteryOn(false);
									} else
										tags.setBatteryOn(true);
								}
							}
							tagsRepository.saveAll(tagsList);
						}
					}
				} catch (ExecutionException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		});
		executorService.shutdown();
	}

}
/*
 * if tag positions need to be send by fetching from qpe then use this code
 * QuuppaApiService quuppaApiService = new QuuppaApiService();
 * CompletableFuture<JSONObject> getTagCompletableFuture; try {
 * getTagCompletableFuture = quuppaApiService.getTagDetails();
 * 
 * CompletableFuture.allOf(getTagCompletableFuture).join();
 * 
 * if (getTagCompletableFuture.isDone()) { JSONObject jsonObject =
 * getTagCompletableFuture.get(); if (jsonObject != null) {
 * 
 * String status = jsonObject.optString("status");
 * 
 * if (!Utils.isEmptyString(status)) { if (status.equals("success")) {
 * 
 * JSONObject jsonTagObject = jsonObject.optJSONObject("response"); if
 * (jsonTagObject != null) { int code = jsonTagObject.getInt("code"); if (code
 * == 0) {
 * 
 * //alertsRepository.deleteByType(AppConstants.ERROR_CONECTION_TYPE_QPE);
 * JSONArray tagsJsonArray = jsonTagObject.getJSONArray("tags"); if
 * (tagsJsonArray != null) {
 * 
 * HashMap<String, Object> tagPositionMap = new HashMap<String, Object>();
 * 
 * DecimalFormat df = new DecimalFormat("#.##"); for (int i = 0; i <
 * tagsJsonArray.length(); i++) { JSONObject tagJsonObject =
 * tagsJsonArray.getJSONObject(i); String id = tagJsonObject.optString("tagId",
 * ""); JSONArray smoothedPosition = tagJsonObject .optJSONArray("location");
 * 
 * if (smoothedPosition != null) { double x = smoothedPosition.getDouble(0);
 * double y = smoothedPosition.getDouble(1); double z =
 * smoothedPosition.getDouble(2);
 * 
 * double[] arr = Utils.getPoint(x, y); if (arr != null) {
 * 
 * HashMap<String, Object> posMap = new HashMap<>(); posMap.put("x",
 * Double.valueOf(df.format(arr[0]))); posMap.put("y",
 * Double.valueOf(df.format(arr[1]))); posMap.put("z", z);
 * posMap.put("lastSeen", tagJsonObject.optLong("locationTS", 0));
 * 
 * tagPositionMap.put(id, posMap); }
 * 
 * } }
 * 
 * if (tagPositionMap != null && tagPositionMap.size() > 0)
 * sendTagPositionAsyncTask.sendTagPostionAsyncTask(tagPositionMap); }
 * 
 * } else { String message = jsonTagObject.optString("message");
 * //alertsRepository.addAlerts(AppConstants.ERROR_CONECTION_TYPE_QPE, message);
 * } }
 * 
 * } else if (status.equals("error")) { String message =
 * jsonObject.getString("message");
 * //alertsRepository.addAlerts(AppConstants.ERROR_CONECTION_TYPE_QPE, message);
 * 
 * } }
 * 
 * } }
 */