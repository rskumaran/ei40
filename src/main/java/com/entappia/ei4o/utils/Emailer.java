package com.entappia.ei4o.utils;

import java.util.ArrayList;
import java.util.Properties;

import javax.mail.Authenticator;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

import com.entappia.ei4o.constants.AppConstants;

public class Emailer {

	/*
	 * private static final String username = "support@i-design.in"; private static
	 * final String password = "$upport@design";
	 */

	private static final String username = "support@entappiaplatform.com";
	private static final String password = "SupportForEntappiaPlatform100%";

	private static final String entappiaLogoImageURL = "https://drive.google.com/uc?export=view&id=1mjZHtk-epRle4r3wzmwwjHn8x5BMUPqX";
	private static final String ei40ImageURL = "https://drive.google.com/uc?export=view&id=1xVuiiQH8M9uYEw2Xe0TeShIZrDzu7tM1";

	private static String subject = "";
	private static String messageText = "";

	private static String mailBodyHeader = "<table cellpadding='0' cellspacing='15' border='0' bgcolor='#f0f4f5' width='100%'style='margin: 0 auto; max-width: 440px; font-family: arial'>"
			+ "<tbody><tr bgcolor='#ffffff'><td><table cellpadding='15' cellspacing='0' border='0' width='100%'><tbody><tr><td style='text-align: center;' >"
			+ "<img src='" + entappiaLogoImageURL + "' alt='Entappia' width='106' style='vertical-align: middle' ></td>"
			+ "</tr></tbody></table></td></tr><tr bgcolor='#ffffff'><td>"
			+ "<table cellpadding='0' cellspacing='0' border='0'><tbody><tr><td style='text-align: center;'><img src='"
			+ ei40ImageURL + "' style='width: 209px; vertical-align: middle; margin-top:20px;' alt='banner'  >"
			+ "</td></tr> <tr><td><table cellpadding='15' cellspacing='0' border='0' width='100%'>" + "<tbody><tr><td>";

	private static String mailBodyFooter = "</td></tr></tbody></table></td></tr></tbody></table></td></tr>"
			+ "<tr bgcolor='#ffffff'><td><table cellpadding='20' cellspacing='0' border='0' width='100%'><tbody> "
			+ "</tbody></table></td></tr>"
			+ "<tr bgcolor='#ffffff'><td><table cellpadding='0' cellspacing='0' border='0'><tbody><tr><td>"
			+ "<table cellpadding='0' cellspacing='0' border='0' width='100%'><tbody><tr bgcolor='#ffffff'>"
			+ "<td><table cellpadding='15' cellspacing='0' border='0'><tbody><tr><td>" 
			+ "<table cellpadding='0' cellspacing='0' border='0' width='100%'><tbody><tr>"
			+ "<td><p style='margin: 0; padding: 0px; foaant-family: arial; font-size: 13px; color: #121212; line-height: 20px; padding-bottom: 10px'>"
			+ "For any queries or concerns, write to us at <a href='mailto:support@entappiaplatform.com'style='color: #38bef0; text-decoration: none'target='_blank'>"
			+ "support@<span class='il'>entappiaplatform</span>.com</a></p></td></tr><tr><td>"
			+ "<p style='margin: 0; padding: 0px; font-family: arial; font-size: 13px; color: #121212; line-height: 20px'>Best,<br>"
			+ " Team <span class='il'>Entappia</span></p> </td></tr></tbody></table></td></tr></tbody></table> </td></tr></tbody> </table> </td></tr></tbody>"
			+ "</table></td></tr></tbody></table>" ;

	/*
	 * static String emailTo = "prakash.idesign@gmail.com"; static String
	 * emailsupport = "support@i-design.in";
	 */
	public static void sendWelcomeEmail(String name, String emailid, String Password) {

		subject = "iPlace Notes - Welcome ";

		messageText = "Hello " + name + ", \n\n" + "<h3>Welcome to entappiaplatform.com </h3>\n <br>"
				+ "Your Username : " + emailid + " <br>" + "Your Password : " + Password + " <br> <br>"
				+ "Thank you for registering into iPlaceNotes.com. \n"
				+ "Please login and explore into the website <a href='https://www.iplacenotes.com'>iPlaceNotes.com</a>";

		sendMail(subject, messageText, emailid);
	}

	public static void sendRegWelcomeEmail(String name, String emailid) {

		subject = "Welcome to iplacenotes ";

		messageText = "Hello " + name + ", \n\n<br><br>"
				+ "Thank you for downloading and registering iPlaceNotes. <br><br>"
				+ "Now you will be able to use this application for conveniently organizing your data and receive location specific alerts and information.<br><br>"
				+ "For overview of the application  select the <b>'Settings-->Demo'</b> option in your application<br><br>"
				+ "Please visit <a href='https://www.iplacenotes.com'>iPlaceNotes.com</a> for additional information. <br><br><br>"
				+ "Thanks,<br>Team iPlaceNotes<br>";

		sendMail(subject, messageText, emailid);
	}

	public static void sendPassword(String name, String password, String emailid) {

		subject = "Password to login to your EI4.0 Account";

		messageText = mailBodyHeader
				+ "<p style='margin: 0; padding: 0px; font-family: arial; font-size: 13px; color: #121212; line-height: 18px; padding-bottom: 10px'>Hi "
				+ name + ","
				+ "</p><p style='margin: 0; padding: 0px; font-family: arial; font-size: 13px; color: #121212; line-height: 18px'>"
				+ "Here is your password for EI4.0 Account : <b>" + password + "</b><br>"
				 

				+ mailBodyFooter;

		sendMail(subject, messageText, emailid);
	}

	public static void sendOTP(String name, String otp, String emailid) {

		subject = otp + " is your OTP to login to your EI4.0 Account";

		messageText = mailBodyHeader
				+ "<p style='margin: 0; padding: 0px; font-family: arial; font-size: 13px; color: #121212; line-height: 18px; padding-bottom: 10px'>Hi "
				+ name + ","
				+ "</p><p style='margin: 0; padding: 0px; font-family: arial; font-size: 13px; color: #121212; line-height: 18px'>"
				+ "Here is your one time passcode code for EI4.0 Registration : " + otp
				+ "<br>This OTP valid for next 30 minutes</p>"  
				+ mailBodyFooter ;

		sendMail(subject, messageText, emailid);
	}

	public static void sendLateAssetAlert(String subject, String message, ArrayList<String> emailids) {

		subject = "EI40:- " + subject + " Tags found";

		messageText = mailBodyHeader

				+ "<p style='margin-left:10px;'>Event Information</p>"
				+ "<table  bgcolor='#f0f4f5' style='width: 100%; border-collapse: collapse'>"
				+ "<tr  style='border: 1px solid #dddddd; padding: 8px; text-align: left'>"
				+ "<th style='border: 1px solid #dddddd; padding: 8px; text-align: left'>AssetType</th>"
				+ "<th style='border: 1px solid #dddddd; padding: 8px; text-align: left'>AssetId</th> "
				+ "<th style='border: 1px solid #dddddd; padding: 8px; text-align: left'>Type</th>"
				+ "<th style='border: 1px solid #dddddd; padding: 8px; text-align: left'>AssignedTo</th> "
				+ "<th style='border: 1px solid #dddddd; padding: 8px; text-align: left white-space:nowrap'>Time</th>"
				+ "<th style='border: 1px solid #dddddd; padding: 8px; text-align: left'>Status</th>" + "</tr>"
				+ message + "</table>"

				+ mailBodyFooter;

		sendMail(subject, messageText, emailids);
	}

	public static void sendTagAlert(String subject, String message, ArrayList<String> emailids) {

		subject = "EI40:- " + subject + " Tags found";

		messageText = mailBodyHeader + "<p style='margin-left:10px;'>Event Information</p>"
				+ "<table  bgcolor='#f0f4f5' style='width: 100%; border-collapse: collapse'>"
				+ "<tr  style='border: 1px solid #dddddd; padding: 8px; text-align: left'>"
				+ "<th style='border: 1px solid #dddddd; padding: 8px; text-align: left'>Zone</th>"
				+ "<th style='border: 1px solid #dddddd; padding: 8px; text-align: left'>Tags</th> "
				+ "<th style='border: 1px solid #dddddd; padding: 8px; text-align: left'>Time</th>"
				+ "<th style='border: 1px solid #dddddd; padding: 8px; text-align: left'>Status</th>" + "</tr>"
				+ message + "</table>"

				+ mailBodyFooter;

		sendMail(subject, messageText, emailids);
	}

	public static void sendAssetAlert(String message, ArrayList<String> emailids, boolean isLSLM,
			boolean isMonthlyReport) {

		if (!isMonthlyReport) {
			if (isLSLM)
				subject = "EI40:- Expired SKU Asset Details ";
			else
				subject = "EI40:- Expired Asset Details";
		} else
			subject = "EI40:- Expired Asset Details monthly report";
		if (AppConstants.print_log)
			System.out.println(subject);
		
		if(isLSLM) {
			messageText = mailBodyHeader + "<p style='margin-left:10px;'>"
					+ "SKU Asset Valitity Details" + "</p>"
					+ "<table  bgcolor='#007bff' style='width: 700px; border-collapse: collapse; color:white'>"
					+ "<tr  style='border: 1px solid #dddddd; padding: 8px; text-align: left'>"//Table Headers
					+ "<th style='border: 1px solid #dddddd; padding: 8px; text-align: left'>UPC Code </th>"
					+ "<th style='border: 1px solid #dddddd; padding: 8px; text-align: left'>SKU No </th>"
					+ "<th style='border: 1px solid #dddddd; padding: 8px; text-align: left'>Batch No </th>"
					+ "<th style='border: 1px solid #dddddd; padding: 8px; text-align: left'>Available Quantity </th>"
					+ "<th style='border: 1px solid #dddddd; padding: 8px; text-align: left'>Description </th>"
					+ "<th style='border: 1px solid #dddddd; padding: 8px; text-align: left'>Expire Date</th> " 
					+ "<th style='border: 1px solid #dddddd; padding: 8px; text-align: left'>Zone Name</th> " 
					+ "</tr>"
					+ message + "</table>";
		}else
		{
			messageText = mailBodyHeader + "<p style='margin-left:10px;'>"
					+ "Asset Valitity Details" + "</p>"
					+ "<table  bgcolor='#007bff' style='width: 700px; border-collapse: collapse; color:white'>"
					+ "<tr  style='border: 1px solid #dddddd; padding: 8px; text-align: left'>"
					+ "<th style='border: 1px solid #dddddd; padding: 8px; text-align: left'>Asset Id </th>"
					+ "<th style='border: 1px solid #dddddd; padding: 8px; text-align: left'>Type </th>"
					+ "<th style='border: 1px solid #dddddd; padding: 8px; text-align: left'>Subtype </th>"
					+ "<th style='border: 1px solid #dddddd; padding: 8px; text-align: left'>Asset Type </th>"
					+ "<th style='border: 1px solid #dddddd; padding: 8px; text-align: left'>Manufacturer </th>"
					+ "<th style='border: 1px solid #dddddd; padding: 8px; text-align: left'>Expire Date </th> " 
					+ "<th style='border: 1px solid #dddddd; padding: 8px; text-align: left'>Zone Name</th> " 
					+ "</tr>"
					+ message + "</table>";
		}

		/*messageText = mailBodyHeader + "<p style='margin-left:10px;'>"
				+ (isLSLM ? "SKU Asset Valitity Details" : "Asset Valitity Details") + "</p>"
				+ "<table  bgcolor='#007bff' style='width: 100%; border-collapse: collapse; color:white'>"
				+ "<tr  style='border: 1px solid #dddddd; padding: 8px; text-align: left'>"
				+ "<th style='border: 1px solid #dddddd; padding: 8px; text-align: left'>"
				+ (isLSLM ? "UPC Code" : "Asset Id") + "</th>"
				+ "<th style='border: 1px solid #dddddd; padding: 8px; text-align: left'>Expire Date</th> " + "</tr>"
				+ message + "</table>";*/

		messageText = messageText+mailBodyFooter;

		sendMail(subject, messageText, emailids);
	}

	public static void sendCrowdedZoneAlert(String subject, String message, ArrayList<String> emailids) {

		subject = "EI40:- " + subject;

		messageText = mailBodyHeader + "<p style='margin-left:10px;'>Occupancy Alert Information</p>"
				+ "<table  bgcolor='#f0f4f5' style='width: 100%; border-collapse: collapse'>"
				+ "<tr  style='border: 1px solid #dddddd; padding: 8px; text-align: left'>"
				+ "<th style='border: 1px solid #dddddd; padding: 8px; text-align: left'>Zone Name</th>"
				+ "<th style='border: 1px solid #dddddd; padding: 8px; text-align: left'>Occupancy</th> " + "</tr>"
				+ message + "</table>" + mailBodyFooter;

		sendMail(subject, messageText, emailids);
	}

	public static void sendShippingAreaTagAlert(String subject, String message, ArrayList<String> emailids) {

		subject = "EI40:- " + subject + " Tags found";

		messageText = mailBodyHeader

				+ "<p style='margin-left:10px;'>We assume that the following work order(s) are completed. Please remove the tags.</p>"
				+ "<table  bgcolor='#f0f4f5' style='width: 100%; border-collapse: collapse'>"
				+ "<tr  style='border: 1px solid #dddddd; padding: 8px; text-align: left'>"
				+ "<th style='border: 1px solid #dddddd; padding: 8px; text-align: left'>WorkOrderId</th>"
				+ "<th style='border: 1px solid #dddddd; padding: 8px; text-align: left'>TagID</th> "
				+ "<th style='border: 1px solid #dddddd; padding: 8px; text-align: left'>Zone Name</th>"
				+ "<th style='border: 1px solid #dddddd; padding: 8px; text-align: left white-space:nowrap'>Time</th>"
				+ "<th style='border: 1px solid #dddddd; padding: 8px; text-align: left'>Zone Type</th>" + "</tr>"
				+ message + "</table>"

				+ mailBodyFooter;

		sendMail(subject, messageText, emailids);
	}
	public static void sendworkorderLimitCrossedAreaTagAlert(String subject, String message, ArrayList<String> emailids) {

		subject = "EI40:- " + subject + " Tags found";

		messageText = mailBodyHeader

				+ "<p style='margin-left:10px;'>The following work order(s) inbound or outbound max limits are crossed.</p>"
				+ "<table  bgcolor='#f0f4f5' style='width: 100%; border-collapse: collapse'>"
				+ "<tr  style='border: 1px solid #dddddd; padding: 8px; text-align: left'>"
				+ "<th style='border: 1px solid #dddddd; padding: 8px; text-align: left'>WorkOrderId</th>"
				+ "<th style='border: 1px solid #dddddd; padding: 8px; text-align: left'>TagID</th> "
				+ "<th style='border: 1px solid #dddddd; padding: 8px; text-align: left'>Zone Name</th>"
				+ "<th style='border: 1px solid #dddddd; padding: 8px; text-align: left white-space:nowrap'>Time</th>"
				+ "<th style='border: 1px solid #dddddd; padding: 8px; text-align: left'>Zone Type</th>" + "</tr>"
				+ message + "</table>"

				+ mailBodyFooter;

		sendMail(subject, messageText, emailids);
	}
	
	
	public static void sendLocatorStatusAlert(String subject, String message, ArrayList<String> emailids) {

		subject = "EI40:- " + subject;

		messageText = mailBodyHeader + "<p style='margin-left:10px;'>Locator Status Information</p>"
				+ "<table  bgcolor='#f0f4f5' style='width: 100%; border-collapse: collapse'>"
				+ "<tr  style='border: 1px solid #dddddd; padding: 8px; text-align: left'>"
				+ "<th style='border: 1px solid #dddddd; padding: 8px; text-align: left'>Locator Id</th>"
				+ "<th style='border: 1px solid #dddddd; padding: 8px; text-align: left'>Locator Name</th>"
				+ "<th style='border: 1px solid #dddddd; padding: 8px; text-align: left'>Status</th> " + "</tr>"
				+ message 
				+ "</table>" + mailBodyFooter;

		sendMail(subject, messageText, emailids);
	}

	public static void sendLostConnectionAlert(String subject, String message, ArrayList<String> emailids) {

		subject = "EI40:- " + subject;

		messageText = mailBodyHeader + "<p style='margin-left:10px;'>Event Information</p>"
				+ "<p style='margin-left:10px;'>" + message + "</p>"
				+ mailBodyFooter;

		sendMail(subject, messageText, emailids);
	}
	
	
	public static void sendMissingTagAlert(String subject, String message, ArrayList<String> emailids) {

		subject = "EI40:- " + subject;

		messageText = mailBodyHeader + "<p style='margin-left:10px;'>Missing Tags Information</p>"
				+ "<table  bgcolor='#f0f4f5' style='width: 100%; border-collapse: collapse'>"
				+ "<tr  style='border: 1px solid #dddddd; padding: 8px; text-align: left'>"
				+ "<th style='border: 1px solid #dddddd; padding: 8px; text-align: left'>Tag Id</th>"
				+ "<th style='border: 1px solid #dddddd; padding: 8px; text-align: left'>Mac Id</th>" 
				+ "<th style='border: 1px solid #dddddd; padding: 8px; text-align: left'>Last Seen</th> " + "</tr>"
				+ message 
				+ "</table>" + mailBodyFooter;

		sendMail(subject, messageText, emailids);
	}

	private static void sendMail(String subject, String messageText, String emailTo) {

		Session session = Session.getInstance(getProperties(), new Authenticator() {
			protected PasswordAuthentication getPasswordAuthentication() {
				return new PasswordAuthentication(username, password);
			}
		});

		try {
			// String emailCc = "rajar.idesign@gmail.com";

			Message message = new MimeMessage(session);
			message.setFrom(new InternetAddress(username));
			message.setRecipients(Message.RecipientType.TO, InternetAddress.parse(emailTo));
			// message.addRecipients(Message.RecipientType.CC,
			// InternetAddress.parse(emailCc));

			message.setSubject(subject);
			message.setContent(messageText, "text/html");

			Transport.send(message);

		} catch (MessagingException e) {
			e.printStackTrace();
		}
	}

	private static void sendMail(String subject, String messageText, ArrayList<String> emailList) {

		Session session = Session.getInstance(getProperties(), new Authenticator() {
			protected PasswordAuthentication getPasswordAuthentication() {
				return new PasswordAuthentication(username, password);
			}
		});

		try {

			Message message = new MimeMessage(session);
			message.setFrom(new InternetAddress(username));

			message.setSubject(subject);
			message.setContent(messageText, "text/html");

			for (String mailId : emailList) {
				message.setRecipients(Message.RecipientType.TO, InternetAddress.parse(mailId));
				Transport.send(message);
			}

		} catch (MessagingException e) {
			e.printStackTrace();
		}
	}

	private static Properties getProperties() {
		Properties props = new Properties();

		props.put("mail.smtp.auth", "true");
		props.put("mail.smtp.starttls.enable", "true");
		// props.put("mail.smtp.socketFactory.fallback", "true");
		props.put("mail.smtp.host",
				"smtp.gmail.com");/*
									 * props.put("mail.smtp.socketFactory.class", "javax.net.ssl.SSLSocketFactory");
									 * props.put("mail.smtp.socketFactory.port", "587");
									 */
		props.put("mail.smtp.port", "587");

		return props;
	}

}
