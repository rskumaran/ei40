package com.entappia.ei4o.utils;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URL;
import java.net.URLConnection;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import com.entappia.ei4o.constants.AppConstants;
 

public class SmsUtil {


	public static void sendOTP(String name,String mobileOtp, String emailOtp, String emailid, String mobile) {
		String apikey = AppConstants.SMSOTPAPIKEY;
		// Prepare Url
		URLConnection myURLConnection = null;
		URL myURL = null;
		BufferedReader reader = null;
		String encoded_message = "";
//		if(platform!=null && platform.equalsIgnoreCase("Android")) { 
//			encoded_message=URLEncoder.encode("<#> Your OTP for Entappia Contact Tracing is "+otp+" : App Hash UMhTZsoiAMz");    
//		} else {     
		encoded_message = URLEncoder
				.encode("Your OTP for Entappia Contact Tracing is " + mobileOtp + ". It is valid for next 30 minutes");
//		}

		// Send SMS API
		String mainUrl = "http://api.msg91.com/api/sendotp.php?";

		// Prepare parameter string
		StringBuilder sbPostData = new StringBuilder(mainUrl);

		sbPostData.append("&authkey=" + apikey);
		sbPostData.append("&message=" + encoded_message);
		sbPostData.append("&mobile=" + mobile);
		sbPostData.append("&sender=IDESIG");
		sbPostData.append("&otp=" + mobileOtp);
		sbPostData.append("&country=0");
		mainUrl = sbPostData.toString();
		try {
			// prepare connection
			myURL = new URL(mainUrl);
			myURLConnection = myURL.openConnection();
			myURLConnection.connect();
			reader = new BufferedReader(new InputStreamReader(myURLConnection.getInputStream()));

		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			sendMail(name, emailOtp, emailid);
			if (reader != null) {
				try {
					reader.close();
				} catch (IOException ignoreMe) {
					System.out.println("ignoreMe" + ignoreMe);
				}
			}
		}
	}

	public static void sendAlertMessage(String message, ArrayList<String> mobileNumberList) {
		String apikey = AppConstants.SMSOTPAPIKEY;
		// Prepare Url
		URLConnection myURLConnection = null;
		URL myURL = null;
		BufferedReader reader = null;
		String encoded_message = "";
//		if(platform!=null && platform.equalsIgnoreCase("Android")) { 
//			encoded_message=URLEncoder.encode("<#> Your OTP for Entappia Contact Tracing is "+otp+" : App Hash UMhTZsoiAMz");    
//		} else {     
		encoded_message = URLEncoder.encode("1234 "+ message);
//		}

		String mobileNumbers = String.join(",", mobileNumberList);

		// Send SMS API
		String mainUrl = "http://api.msg91.com/api/sendotp.php?";

		// Prepare parameter string
		StringBuilder sbPostData = new StringBuilder(mainUrl);

		sbPostData.append("&authkey=" + apikey);
		sbPostData.append("&message=" + encoded_message);
		sbPostData.append("&mobile=" + mobileNumbers);
		sbPostData.append("&sender=IDESIG");
		sbPostData.append("&otp=1234" );
		sbPostData.append("&country=0");
		mainUrl = sbPostData.toString();
		try {
			// prepare connection
			myURL = new URL(mainUrl);
			myURLConnection = myURL.openConnection();
			myURLConnection.connect();
			reader = new BufferedReader(new InputStreamReader(myURLConnection.getInputStream()));

		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			if (reader != null) {
				try {
					reader.close();
				} catch (IOException ignoreMe) {
					System.out.println("ignoreMe" + ignoreMe);
				}
			}
		}
	}

	public static boolean sendMail(String name, String otp, String emailId) {
		try {
			ExecutorService executorService = Executors.newFixedThreadPool(2);
			executorService.execute(new Runnable() {

				public void run() {

					try {
						Emailer.sendOTP(name, otp, emailId);
					} catch (Exception e) {
						e.printStackTrace();
					}

				}
			});
			executorService.shutdown();
			// executorService.awaitTermination(20, TimeUnit.SECONDS);

		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}
		return true;
	}
	
	public static boolean sendPassword(String name, String password, String emailId) {
		try {
			ExecutorService executorService = Executors.newFixedThreadPool(2);
			executorService.execute(new Runnable() {

				public void run() {

					try {
						Emailer.sendPassword(name, password, emailId);
					} catch (Exception e) {
						e.printStackTrace();
					}

				}
			});
			executorService.shutdown();
			// executorService.awaitTermination(20, TimeUnit.SECONDS);

		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}
		return true;
	}
}