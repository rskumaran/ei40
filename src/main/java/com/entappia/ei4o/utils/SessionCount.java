package com.entappia.ei4o.utils;

import java.util.ArrayList;
import java.util.HashMap;

import javax.servlet.annotation.WebListener;
import javax.servlet.http.HttpSession;
import javax.servlet.http.HttpSessionEvent;
import javax.servlet.http.HttpSessionListener;

import org.springframework.stereotype.Component;

@WebListener
@Component
public class SessionCount implements HttpSessionListener {

	public static int numberOfUsersOnline;

	public static HashMap<String, HttpSession> sessionMap = new HashMap<>();

	public static ArrayList<String> sessionIdList = new ArrayList<>();

	public SessionCount() {
		numberOfUsersOnline = 0;
	}

	public static int getNumberOfUsersOnline() {
		return numberOfUsersOnline;
	}

	public void sessionCreated(HttpSessionEvent event) {

		/*synchronized (this) {

			HttpSession httpSession = event.getSession();
			if (httpSession != null) {

				String userType = (String) httpSession.getAttribute("userType");

				if (!Utils.isEmptyString(userType) && userType.equals("genie")) {
					String sessionId = httpSession.getId();
					if (!Utils.isEmptyString(userType))
						sessionMap.put(sessionId, httpSession);
				}

				if (!sessionIdList.contains(httpSession.getId())) {
					numberOfUsersOnline++;
					sessionIdList.add(httpSession.getId());
				}

			}
		}*/
	}

	public void addSession(HttpSession httpSession) {

		synchronized (this) {
			if (httpSession != null) {

				String userType = (String) httpSession.getAttribute("userType");

				if (!Utils.isEmptyString(userType) && userType.equals("genie")) {
					String sessionId = httpSession.getId();
					if (!Utils.isEmptyString(userType))
						sessionMap.put(sessionId, httpSession);
				}

				if (!sessionIdList.contains(httpSession.getId())) {
					numberOfUsersOnline++;
					sessionIdList.add(httpSession.getId());
				}

			}

		}

	}

	public void sessionDestroyed(HttpSessionEvent event) {

		synchronized (this) {

			HttpSession httpSession = event.getSession();
			if (httpSession != null) {

				String userType = (String) httpSession.getAttribute("userType");

				if (!Utils.isEmptyString(userType) && userType.equals("genie")) {
					String sessionId = httpSession.getId();
					if (!Utils.isEmptyString(userType)) {
						if (sessionMap.containsKey(sessionId))
							sessionMap.remove(sessionId);
					}
				}

				if (sessionIdList.contains(httpSession.getId())) {
					if (numberOfUsersOnline > 0) {
						numberOfUsersOnline--;
					} else {
						numberOfUsersOnline = 0;
					}

					sessionIdList.remove(httpSession.getId());
				}

			}
			// event.getSession().getAttribute("timeZone")
		}

	}

}