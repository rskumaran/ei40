package com.entappia.ei4o.utils;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.lang.reflect.Type;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.ResourceBundle;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;

import org.springframework.core.io.Resource;
import org.springframework.core.io.ResourceLoader;
import org.springframework.util.FileCopyUtils;

import com.entappia.ei4o.constants.AppConstants;
import com.google.common.io.CharStreams;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

public class Utils {

	private static final String DATE_FORMAT = "yyyy-MM-dd HH:mm";

	public static boolean isEmptyString(String value) {
		return value == null || value.trim().isEmpty() || value.equals("null");
	}

	public static String encryption(String value) {
		return AES.encrypt(value, AppConstants.seckretKey);
	}

	public static String decryption(String value) {
		return AES.decrypt(value, AppConstants.seckretKey);
	}

	public static String getDate(Date date) {

		SimpleDateFormat orgDateFormat = new SimpleDateFormat(DATE_FORMAT);

		Calendar calendar = Calendar.getInstance();
		calendar.setTime(date);
		String sDate = orgDateFormat.format(calendar.getTime());

		return sDate;

	}

	public static String getServerPath() {
		ResourceBundle bundle = ResourceBundle.getBundle("application");
		String os = System.getProperty("os.name").toLowerCase();
		String dir = "";
		if (os.contains("windows")) {
			dir = bundle.getString("filepath_windows");
		} else if (os.contains("mac os")) {
			dir = bundle.getString("filepath_ios");
		} else
			dir = bundle.getString("filepath_linux");

		return dir;
	}

	public static boolean validateDate(String strDate) {

		String strDateRegEx = "\\d{4}-(Jan|Feb|Mar|Apr|May|Jun|Jul|Aug|Sep|Oct|Nov|Dec)-(0[1-9]|[12][0-9]|[3][01])";

		if (strDate.matches(strDateRegEx)) {
			return true;
		} else {
			return false;
		}

	}

	public static boolean validateDateTime(String strDate) {

		try {
			Date fromTime = new SimpleDateFormat("yyyy-MMM-dd HH:mm").parse(strDate);
			return true;
		} catch (ParseException e) {
		}

		return false;

	}

	public static String formatDateTime(Date date) {

		String dateTime = "";
		if (date != null)
			dateTime = new SimpleDateFormat("yyyy-MMM-dd HH:mm:ss").format(date);

		return dateTime;

	}
	
	public static String formatDateTime1(Date date) {

		String dateTime = "";
		if (date != null)
			dateTime = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(date);

		return dateTime;

	}
	
	private static  SimpleDateFormat DATE_FORMAT1 = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.S");
	private static  SimpleDateFormat DATE_FORMAT2 = new SimpleDateFormat("yyyy-MMM-dd HH:mm");
	 

 	
	public static String formatDateTime(String date) {

		String dateTime = "";
		if (!isEmptyString(date))
		{
			try {
				Date date1 = DATE_FORMAT1.parse(date);
				if(date1!=null) {
					dateTime = DATE_FORMAT2.format(date1);
				}
			} catch (ParseException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}

		return dateTime;

	}

	public static String getCookie(HttpServletRequest request, String name) {
		Cookie[] cookies = request.getCookies();
		if (cookies != null) {
			for (Cookie cookie : cookies) {
				if (cookie.getName().equals(name)) {
					return cookie.getValue();
				}
			}
		}
		return null;
	}

	public static HashMap<String, List<String>> getGridHashmap(int width, int height) {
		HashMap<String, List<String>> campusGridZoneMap = new HashMap<>();
		HashMap<String, List<HashMap<String, String>>> campusGridMap = new HashMap<>();

		for (int i = 0; i < width; i++) {

			for (int j = 0; j < height; j++) {

				List<HashMap<String, String>> mapList = new ArrayList<>();
				HashMap<String, String> map = new HashMap<>();
				map.put("x", "" + i);
				map.put("y", "" + j);
				mapList.add(map);

				HashMap<String, String> map1 = new HashMap<>();
				map1.put("x", "" + i);
				map1.put("y", "" + (j + 1));
				mapList.add(map1);

				HashMap<String, String> map2 = new HashMap<>();
				map2.put("x", "" + (i + 1));
				map2.put("y", "" + (j + 1));
				mapList.add(map2);

				HashMap<String, String> map3 = new HashMap<>();
				map3.put("x", "" + (i + 1));
				map3.put("y", "" + j);
				mapList.add(map3);

				campusGridMap.put(i + "_" + j, mapList);
				campusGridZoneMap.put(i + "_" + j, new ArrayList());
			}

		}

		return campusGridZoneMap;
	}

	public static HashMap<String, HashMap<String, Integer>> readCapability(ResourceLoader resourceLoader,
			String fileName) {

		String filedata = null;
		try {

			final Resource resource = resourceLoader
					.getResource("classpath:static/assets/Capability/" + fileName + ".json");
			Reader reader = new InputStreamReader(resource.getInputStream());
			filedata = FileCopyUtils.copyToString(reader);
		} catch (Exception e) {
			e.printStackTrace();
		}

		Gson gson = new Gson();
		Type mapType = new TypeToken<HashMap<String, HashMap<String, Integer>>>() {
		}.getType();

		HashMap<String, HashMap<String, Integer>> cabData = gson.fromJson(filedata, mapType);
		return cabData;
	}
	
	public static List<HashMap<String, String>> readWarehouse(ResourceLoader resourceLoader) {

		String filedata = null;
		try {

			final Resource resource = resourceLoader
					.getResource("classpath:static/WareHouseMaterial.json");
			Reader reader = new InputStreamReader(resource.getInputStream());
			filedata = FileCopyUtils.copyToString(reader);
		} catch (Exception e) {
			e.printStackTrace();
		}

		Gson gson = new Gson();
		Type mapType = new TypeToken<List<HashMap<String, String>>>() {
		}.getType();

		List<HashMap<String, String>> cabData = gson.fromJson(filedata, mapType);
		return cabData;
	}
	
	public static String getDataFromFile(String filePath) {
		try {
			InputStream resourcee = new FileInputStream(new File(filePath));

			String text = null;
			try (final Reader reader = new InputStreamReader(resourcee)) {
				text = CharStreams.toString(reader);
			}
			return text;
		} catch (IOException e) {
			e.printStackTrace();
		}

		return null;
	}

}
