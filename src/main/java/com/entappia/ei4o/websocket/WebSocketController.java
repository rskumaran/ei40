package com.entappia.ei4o.websocket;

import java.util.HashMap;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.messaging.handler.annotation.MessageMapping;
import org.springframework.messaging.simp.SimpMessageHeaderAccessor;
import org.springframework.stereotype.Controller;

import com.entappia.ei4o.constants.AppConstants;
import com.entappia.ei4o.dbmodels.TagPosition;
import com.entappia.ei4o.models.NotificationsEvent;
import com.entappia.ei4o.models.SocketIOutputData;
import com.entappia.ei4o.models.SocketInputData;
import com.entappia.ei4o.models.TagData;
import com.entappia.ei4o.repository.TagPositionRepository;
import com.entappia.ei4o.services.LogsServices;
import com.entappia.ei4o.utils.BaseController;
import com.entappia.ei4o.utils.Utils;

@Controller
public class WebSocketController extends BaseController {

	@Autowired
	private LogsServices logsServices;

	@Autowired
	ScheduledPushMessages scheduledPushMessages;

	@Autowired
	private TagPositionRepository tagPositionRepository;

	@MessageMapping("/subscribe")
	public JSONObject subscribeLiveData(SimpMessageHeaderAccessor headerAccessor, SocketInputData message)
			throws Exception {
		Thread.sleep(1000); // simulated delay
		JSONObject json = new JSONObject();

		String sessionId = headerAccessor.getSessionAttributes().get("HTTPSESSIONID").toString();
		HttpSession httpSession = (HttpSession) headerAccessor.getSessionAttributes().get("HTTPSESSION");

		ResponseEntity<?> responseEntity = checkSessionKey(httpSession, sessionId);
		if (responseEntity != null) {
			json.put("status", "error");
			json.put("message", "Invalid session id");

			logsServices.addLog(AppConstants.error, AppConstants.SOCKET, "subscribeLiveData",
					AppConstants.sessiontimeout, null);

		} else {

			if (Utils.isEmptyString(message.getType())) {
				json.put("status", "error");
				json.put("message", "Invalid Type");
			} else if (Utils.isEmptyString(message.getViewType())) {
				json.put("status", "error");
				json.put("message", "Invalid View Type");
			} else {

				String code = "";
				String subType = message.getViewType();
				if (subType.equalsIgnoreCase("live")) {
					code = "CLVW";
				} else if (subType.equalsIgnoreCase("peopleLiveView")) {
					code = "PLVW";
				} else if (subType.equalsIgnoreCase("assetLiveView")) {
					code = "ASLV";
				} else if (subType.equalsIgnoreCase("workorderLiveView")) {
					code = "WLVW";
				} else if (subType.equalsIgnoreCase("skuLiveView")) {
					code = "SKLV";
				} else if (subType.equalsIgnoreCase("warehouseLiveView")) {
					code = "HKLV";
				} else {
					json.put("status", "error");
					json.put("message", "Invalid view type");
				}

				if (!Utils.isEmptyString(code)) {
					ResponseEntity<?> responseEntity1 = checkSessionKey(httpSession, sessionId, code);
					if (responseEntity1 != null) {
						json.put("status", "error");
						json.put("message", AppConstants.sessiontimeout);

						logsServices.addLog(AppConstants.error, AppConstants.SOCKET, "subscribeLiveData",
								AppConstants.sessiontimeout, null);

					} else {

						if (message.getType().equalsIgnoreCase("subscribe")) {
							json.put("status", "success");
							json.put("message", "Subscribed successfully.");
							TagData.addSessionId(sessionId);

							logsServices.addLog(AppConstants.event, AppConstants.WEBSOCKET, "subscribe",
									"Id:-" + sessionId + " " + json.getString("message"), null);
						} else {
							json.put("status", "success");
							json.put("message", "Unsubscribed successfully.");
							TagData.removeSessionId(sessionId);

							logsServices.addLog(AppConstants.event, AppConstants.WEBSOCKET, "subscribe",
									"Id:-" + sessionId + " " + json.getString("message"), null);
						}
					}
				}
			}
		}
		json.put("data", new JSONArray());

		HashMap<String, TagPosition> tagPostions = new HashMap<>();
		tagPositionRepository.findAll().forEach(e -> tagPostions.put("" + e.getMacId(), e));
		SocketIOutputData socketIOutputData = new SocketIOutputData(json.getString("status"), tagPostions);
		scheduledPushMessages.sendCustomMessage(socketIOutputData);//sessionId, 
		return json;
	}

	@MessageMapping("/subscribeNotification")
	public JSONObject subscribeNotification(SimpMessageHeaderAccessor headerAccessor, SocketInputData message)
			throws Exception {
		Thread.sleep(1000); // simulated delay
		JSONObject json = new JSONObject();

		String sessionId = headerAccessor.getSessionAttributes().get("HTTPSESSIONID").toString();
		HttpSession httpSession = (HttpSession) headerAccessor.getSessionAttributes().get("HTTPSESSION");

		ResponseEntity<?> responseEntity = checkSessionKey(httpSession, sessionId);
		if (responseEntity != null) {
			json.put("status", "error");
			json.put("message", "Invalid session id");

			logsServices.addLog(AppConstants.error, AppConstants.SOCKET, "subscribeLiveData",
					AppConstants.sessiontimeout, null);

		} else {
			if (Utils.isEmptyString(message.getType())) {
				json.put("status", "error");
				json.put("message", "Invalid Type");
			} /*
				 * else if (Utils.isEmptyString(message.getViewType())) { json.put("status",
				 * "error"); json.put("message", "Invalid View Type"); }
				 */ else {
				if (message.getType().equalsIgnoreCase("subscribe")) {
					json.put("status", "success");
					json.put("message", "Subscribed successfully.");
					NotificationsEvent.addSessionId(httpSession.getAttribute("mailId").toString(), sessionId);

					logsServices.addLog(AppConstants.event, AppConstants.WEBSOCKET, "subscribe",
							"Id:-" + sessionId + " " + json.getString("message"), null);
				} else {
					json.put("status", "success");
					json.put("message", "Unsubscribed successfully.");
					NotificationsEvent.removeSessionId(httpSession.getAttribute("mailId").toString(), sessionId);

					logsServices.addLog(AppConstants.event, AppConstants.WEBSOCKET, "subscribe",
							"Id:-" + sessionId + " " + json.getString("message"), null);
				}
			}
		}
		json.put("data", new JSONArray());

		HashMap<String, Object> response = new HashMap<>();
		response.put("status", "success");
		response.put("type", "notification");
		scheduledPushMessages.sendNotificationMessage(response);//sessionId,
		return json;
	}

}
