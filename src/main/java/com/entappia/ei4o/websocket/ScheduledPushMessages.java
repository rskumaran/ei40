package com.entappia.ei4o.websocket;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.stereotype.Service;

import com.entappia.ei4o.constants.AppConstants.NotificationType;
import com.entappia.ei4o.models.NotificationsEvent;
import com.entappia.ei4o.models.SocketIOutputData;

@Service
public class ScheduledPushMessages {

	private final SimpMessagingTemplate simpMessagingTemplate;
	public static int count = 0;

	public ScheduledPushMessages(SimpMessagingTemplate simpMessagingTemplate) {
		this.simpMessagingTemplate = simpMessagingTemplate;
	}
 
	public void sendCustomMessage(SocketIOutputData socketIOutputData) {
		simpMessagingTemplate.convertAndSend("/topic/livedata", socketIOutputData);
	}

	public void sendNotificationMessage(HashMap<String, Object> socketIOutputData) {
		simpMessagingTemplate.convertAndSend("/topic/notification", socketIOutputData);
	}

	public void sendWSMessage(String message, ArrayList<String> emailIds, NotificationType type) {
		ExecutorService executor = Executors.newFixedThreadPool(10);
		executor.submit(() -> {
			Hashtable<String, ArrayList<String>> sessionMapList = NotificationsEvent.getSessionIdList();

			HashMap<String, Object> response = new HashMap<>();
			response.put("status", "success");

			if (type == NotificationType.INFORMATION)
				response.put("notificationType", 0);
			else if (type == NotificationType.WARNING)
				response.put("notificationType", 1);
			else if (type == NotificationType.EMERGENCY)
				response.put("notificationType", 2);

			response.put("message", message);

			if (sessionMapList.size() > 0 && emailIds.size() > 0) { 
				
				sendNotificationMessage(response);
				
				/*
				 * for (String emailId : emailIds) {
				 * 
				 * if (sessionMapList.containsKey(emailId)) { ArrayList<String> sessionIdList =
				 * sessionMapList.get(emailId);
				 * 
				 * for (String sessionId : sessionIdList) { sendNotificationMessage(response); }
				 * } }
				 */
			}
		});
	}

}
