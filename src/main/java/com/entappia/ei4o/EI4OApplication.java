package com.entappia.ei4o;

import java.sql.DatabaseMetaData;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.ResourceBundle;

import javax.annotation.PostConstruct;
import javax.persistence.EntityManager;
import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.core.io.ResourceLoader;
import org.springframework.integration.config.EnableIntegration;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.concurrent.ThreadPoolTaskScheduler;

import com.entappia.ei4o.bgtasks.MissingTagAlertTask;
import com.entappia.ei4o.constants.AppConstants;
import com.entappia.ei4o.dbmodels.CampusDetails;
import com.entappia.ei4o.dbmodels.FirmwareSettings;
import com.entappia.ei4o.dbmodels.Login;
import com.entappia.ei4o.dbmodels.WarehouseColumnName;
import com.entappia.ei4o.repository.CampusDetailsRepository;
import com.entappia.ei4o.repository.FirmwareSettingsRepository;
import com.entappia.ei4o.repository.LoginRegistrationRepository;
import com.entappia.ei4o.repository.WarehouseColumnNameRepository;
import com.entappia.ei4o.services.LogsServices;
import com.entappia.ei4o.services.SKUEvent;
import com.entappia.ei4o.services.WarehouseEvent;
import com.entappia.ei4o.utils.Utils;

@SpringBootApplication
@EnableIntegration
@EnableScheduling
public class EI4OApplication {

	@Autowired
	protected DataSource dataSource;

	@Autowired
	protected EntityManager entityManager;

	public static ArrayList<String> tableNameList = new ArrayList<>();

	@Autowired
	protected CampusDetailsRepository compusDetailsRepository;

	@Autowired
	private LoginRegistrationRepository loginRegistrationRepository;

	@Autowired
	private ResourceLoader resourceLoader;

	@Autowired
	private LogsServices logsServices;
 
	@Autowired
	private SKUEvent skuEvent;

	@Autowired
	private WarehouseEvent warehouseEvent;
	
	@Autowired
	private MissingTagAlertTask missingTagAlertTask;

	@Autowired
	private WarehouseColumnNameRepository warehouseColumnNameRepository;
	
	@Autowired
	private FirmwareSettingsRepository firmwareSettingsRepository;
  

	private String columnNames[] = { "SKU No" };// , "issue_date", "manufacture_date"
	private String columnValues[] = { "sku_no", "Issue Date", "Manufacture Date" };

	public static void main(String[] args) {
		SpringApplication.run(EI4OApplication.class, args);
	}

	/*
	 * @Override public void addResourceHandlers(ResourceHandlerRegistry registry) {
	 * if (!registry.hasMappingForPattern("/webjars/**")) {
	 * registry.addResourceHandler("/webjars/**").addResourceLocations(
	 * "classpath:/META-INF/resources/webjars/"); }
	 * 
	 * }
	 */

	@PostConstruct
	void getQPEInfo() {

		try { 
			
			getTableNames();
			saveCompusDetails();
			insertAdminUser();
			addApplicationVersion();
			skuEvent.readSKUAssets();
			warehouseEvent.readWarehouseAsset(); 
			
			List<WarehouseColumnName> warehouseColumnNameList = warehouseColumnNameRepository.findAll();
			if (warehouseColumnNameList == null || warehouseColumnNameList.size() == 0) {

				for (int i = 0; i < columnNames.length; i++) {
					WarehouseColumnName warehouseColumnName = new WarehouseColumnName();
					warehouseColumnName.setColumnName(columnNames[i]);
					warehouseColumnName.setFieldName(columnValues[i]);
					warehouseColumnNameRepository.save(warehouseColumnName);
				}

			}

			/*
			 * JSONObject jsonObj = null; CompletableFuture<JSONObject>
			 * locatorInfoCompletableFuture =
			 * smsEvent.sentOccupancyAlert("918220172575, 919944953584",
			 * "MD Room - 70%, MD Room 1-10%");
			 * CompletableFuture.allOf(locatorInfoCompletableFuture).join(); if
			 * (locatorInfoCompletableFuture.isDone()) {
			 * 
			 * jsonObj = locatorInfoCompletableFuture.get();
			 * 
			 * }
			 */
			
			missingTagAlertTask.createSchedule();
			
			Calendar calendar = Calendar.getInstance();
			calendar.add(Calendar.MINUTE, 5);

			ThreadPoolTaskScheduler threadPoolTaskScheduler = new ThreadPoolTaskScheduler();
			threadPoolTaskScheduler.initialize();
			threadPoolTaskScheduler.schedule(new Runnable() {

				@Override
				public void run() {
					// TODO Auto-generated method stub  
					missingTagAlertTask.updateBatteryStatus();
				}
			}, calendar.getTime());
			

		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		// getQPEData();
	}

	private void addApplicationVersion() {
		ResourceBundle bundle = ResourceBundle.getBundle("application");

		FirmwareSettings firmwareSettings = firmwareSettingsRepository.findByApplication("ei40");
		
		if (firmwareSettings == null) {
			firmwareSettings = new FirmwareSettings();
			firmwareSettings.setApplication("ei40");
			firmwareSettings.setDownloadedVersion("");
			firmwareSettings.setCreatedDate(new Date());
			firmwareSettings.setModifiedDate(new Date());
			firmwareSettings.setVersion(bundle.getString("ei40version"));
		}else {

			String versionInDB = firmwareSettings.getVersion();

			if(versionInDB == null || !versionInDB.equals(bundle.getString("ei40version"))) {

				firmwareSettings.setVersion(bundle.getString("ei40version"));
				firmwareSettings.setCreatedDate(new Date());
				firmwareSettings.setModifiedDate(new Date());
			}
			if( firmwareSettings.getDownloadedVersion() == null || firmwareSettings.getDownloadedVersion().isEmpty()) {
				firmwareSettings.setDownloadedVersion("");
			}
			if( firmwareSettings.getCreatedDate() == null || firmwareSettings.getCreatedDate().toString().isEmpty()) {
				firmwareSettings.setCreatedDate(new Date());
			}

			if( firmwareSettings.getModifiedDate() == null || firmwareSettings.getModifiedDate().toString().isEmpty()) {
				firmwareSettings.setModifiedDate(new Date());
			}


		}
		
		
		firmwareSettingsRepository.save(firmwareSettings);
		
	}

	private void saveCompusDetails() {
		// TODO Auto-generated method stub

		/*
		 * ArrayList<HashMap<String, String>> mapList = new ArrayList<>();
		 * 
		 * HashMap<String, String> map1 = new HashMap<>(); map1.put("zoneName",
		 * "Entappia"); map1.put("percentage", "80.00"); mapList.add(map1);
		 * 
		 * HashMap<String, String> map = new HashMap<>(); map.put("zoneName",
		 * "Idesign"); map.put("percentage", "70.00"); mapList.add(map);
		 * 
		 * notificationMessages.zoneAlert(mapList);
		 */

		List<CampusDetails> list = compusDetailsRepository.findAll();
		if (list == null || list.size() == 0) {
			CampusDetails compusDetails = new CampusDetails();

			compusDetails.setCreatedDate(new Date());
			compusDetails.setModifiedDate(new Date());
			compusDetails.setWidth(100);
			compusDetails.setHeight(100);
			compusDetails.setName("Campus");
			compusDetails.setActive(true);
			compusDetails.setCreatedDate(new Date());
			compusDetails.setModifiedDate(new Date());
			compusDetails.setGridZoneDetails(Utils.getGridHashmap((int) Math.ceil(compusDetails.getWidth()),
					(int) Math.ceil(compusDetails.getHeight())));
			compusDetails.setColorCode("#3388ff");
			compusDetailsRepository.save(compusDetails);
		} /*
			 * else { CampusDetails compusDetails = list.get(0);
			 * compusDetails.setGridZoneDetails(Utils.getGridHashmap((int)
			 * Math.ceil(compusDetails.getWidth()), (int)
			 * Math.ceil(compusDetails.getHeight())));
			 * compusDetailsRepository.save(compusDetails); }
			 */

	}

	private void insertAdminUser() {

		if (loginRegistrationRepository.count() == 0) {
			String fileName = "SystemAdmin";

			HashMap<String, HashMap<String, Integer>> capabilityList = Utils.readCapability(resourceLoader, fileName);

			Login login = new Login();
			login.setPassword(Utils.encryption("Entappia123"));
			login.setDisplayName("Admin");
			login.setType("dashboard");
			login.setUserType(fileName);
			login.setMobileNumber("");
			login.setEmailID("admin@ei.com");
			login.setCreatedDate(new Date());
			login.setCapabilityList(capabilityList);
			login.setOrgAlert(null);
			login.setDeviceDetails(null);
			login.setLastSession(null);
			login.setProfileImage("".getBytes());

			loginRegistrationRepository.save(login);

			Login loginDemo = new Login();
			loginDemo.setPassword(Utils.encryption("Demo@123"));
			loginDemo.setDisplayName("DemoUser");
			loginDemo.setType("dashboard");
			loginDemo.setUserType("DemoUser");
			loginDemo.setMobileNumber("");
			loginDemo.setEmailID("demo@ei.com");
			loginDemo.setCreatedDate(new Date());
			loginDemo.setCapabilityList(capabilityList);
			loginDemo.setOrgAlert(null);
			loginDemo.setDeviceDetails(null);
			loginDemo.setProfileImage(null);
			loginDemo.setLastSession(null);
			loginDemo.setProfileImage("".getBytes());

			loginRegistrationRepository.save(loginDemo);

			logsServices.addLog(AppConstants.event, AppConstants.credential, "registration",
					"Admin and Demo user details created", null);

		}
		
		Login loginDemo = loginRegistrationRepository.findByEmailID("demo@ei.com");
		if(loginDemo==null) {
			
			String fileName = "SystemAdmin";

			HashMap<String, HashMap<String, Integer>> capabilityList = Utils.readCapability(resourceLoader, fileName);
			loginDemo = new Login();
			
			loginDemo.setPassword(Utils.encryption("Demo@123"));
			loginDemo.setDisplayName("DemoUser");
			loginDemo.setType("dashboard");
			loginDemo.setUserType("DemoUser");
			loginDemo.setMobileNumber("");
			loginDemo.setEmailID("demo@ei.com");
			loginDemo.setCreatedDate(new Date());
			loginDemo.setCapabilityList(capabilityList);
			loginDemo.setOrgAlert(null);
			loginDemo.setDeviceDetails(null);
			loginDemo.setProfileImage(null);
			loginDemo.setLastSession(null);
			loginDemo.setProfileImage("".getBytes());

			loginRegistrationRepository.save(loginDemo);
		}
		
		Login login = loginRegistrationRepository.findByEmailID("admin@ei.com");
		if(login==null) { 
			String fileName = "SystemAdmin";

		HashMap<String, HashMap<String, Integer>> capabilityList = Utils.readCapability(resourceLoader, fileName);
		
			login = new Login();
			login.setPassword(Utils.encryption("Entappia123"));
			login.setDisplayName("Admin");
			login.setType("dashboard");
			login.setUserType(fileName);
			login.setMobileNumber("");
			login.setEmailID("admin@ei.com");
			login.setCreatedDate(new Date());
			login.setCapabilityList(capabilityList);
			login.setOrgAlert(null);
			login.setDeviceDetails(null);
			login.setLastSession(null);
			login.setProfileImage("".getBytes());
			loginRegistrationRepository.save(login);

		}

	}

	public void getQPEData() {

		Calendar calendar = Calendar.getInstance();
		calendar.add(Calendar.MINUTE, 1);

		ThreadPoolTaskScheduler threadPoolTaskScheduler = new ThreadPoolTaskScheduler();
		threadPoolTaskScheduler.initialize();
		threadPoolTaskScheduler.schedule(new Runnable() {

			@Override
			public void run() {
				// TODO Auto-generated method stub
				// getQPEInfo();
			}
		}, calendar.getTime());
	}

	public void getTableNames() throws Exception {
		tableNameList.clear();

		DatabaseMetaData metaData = dataSource.getConnection().getMetaData();
		ResultSet tables = metaData.getTables(null, null, null, new String[] { "TABLE" });
		while (tables.next()) {

			String dbName = tables.getString("TABLE_CAT");
			if (dbName != null && dbName.equals("db_ei40")) {
				String tableName = tables.getString("TABLE_NAME");
				System.out.println(tableName);
				tableNameList.add(tableName);
			}
		}
	}

}
