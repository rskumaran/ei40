package com.entappia.ei4o.controllers;

import java.text.SimpleDateFormat;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.TimeZone;
import java.util.concurrent.ExecutionException;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RestController;

import com.entappia.ei4o.repository.LogsRepository;
import com.entappia.ei4o.repository.LogsViewRepository;
import com.entappia.ei4o.services.LogsServices;
import com.entappia.ei4o.utils.BaseController;
import com.entappia.ei4o.constants.AppConstants;
import com.entappia.ei4o.dbmodels.Logs;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import com.entappia.ei4o.utils.Utils;

@RestController
public class LogsController extends BaseController {

	@Autowired
	private LogsRepository logsRepository;

	@Autowired
	private LogsViewRepository logsViewRepository;

	@Autowired
	private LogsServices logsServices;

	public static final int listViewCount = 20;

	public static enum logType {
		server, vehicle, staff, material, visitor, asset, genie
	}

	/*@RequestMapping(path = { "getServerLog" }, method = RequestMethod.POST)
	public ResponseEntity<?> getServerLog(HttpServletRequest request, HttpSession httpSession,
			@RequestBody HashMap<String, Object> requestmap) throws InterruptedException, ExecutionException {
		HashMap<String, Object> returnmap = new HashMap<>();

		ResponseEntity<?> responseEntity = checkSessionKey(httpSession, Utils.getCookie(request, "JSESSIONID"), "LSLG");
		if (responseEntity != null) {
			logsServices.addLog(AppConstants.error, AppConstants.logApi, "getServerLog", AppConstants.sessiontimeout,
					null);
			return responseEntity;
		}

		// System.out.print("Inside getServer log");
		// The start record count is the record number that we have to fetch from the
		// start
		// The logId is incrementing so the logId is sorted descending and record number
		// is fetched
		try {
			/*int logId = 0, noOfRecords = 0;
			if (requestmap.containsKey("logId")) {
				String logIdStr = (String) requestmap.get("logId");
				if (isNumeric(logIdStr)) {
					logId = Integer.valueOf("" + requestmap.get("logId"));
				} else {
					logId = (int) logsRepository.GetMaxLogId() + 1;
				}
			} else {
				logId = (int) logsRepository.GetMaxLogId() + 1;
			}

			if (requestmap.containsKey("noOfRecords")) {
				String noOfRecordsStr = (String) requestmap.get("noOfRecords");
				if (isNumeric(noOfRecordsStr)) {
					noOfRecords = Integer.valueOf("" + requestmap.get("noOfRecords"));
				} else {
					noOfRecords = listViewCount; // Magic
				}
			} else {
				noOfRecords = listViewCount; // Magic
			}

			if (logId <= 0)
				logId = (int) logsRepository.GetMaxLogId() + 1;/
			
			int noOfRecords = 0;
			Date logDate = new Date();
			
			if (requestmap.containsKey("logId")) {
				String logIdStr = (String) requestmap.get("logId");
				if (Utils.validateDate(requestmap.getOrDefault("logId", "").toString())) {
					logDate = new SimpleDateFormat(AppConstants.RequestDateFormat)
							.parse(requestmap.getOrDefault("logId", "").toString());
				} else {
					logDate =  logsRepository.GetMaxDate();
				}
			} else {
				logDate =  logsRepository.GetMaxDate();
			}

			if (requestmap.containsKey("noOfRecords")) {
				String noOfRecordsStr = (String) requestmap.get("noOfRecords");
				if (isNumeric(noOfRecordsStr)) {
					noOfRecords = Integer.valueOf("" + requestmap.get("noOfRecords"));
				} else {
					noOfRecords = listViewCount; // Magic
				}
			} else {
				noOfRecords = listViewCount; // Magic
			}

			if (!Utils.validateDate(requestmap.getOrDefault("logId", "").toString()))
				logDate =  logsRepository.GetMaxDate();

			if (noOfRecords <= 0)
				noOfRecords = listViewCount; // Magic

			Iterable<Logs> logList = new ArrayList<>();
			// logList = logsRepository.findAll();
			logList = (Iterable<Logs>) logsRepository.selectLogs(logDate, noOfRecords);
			return new ResponseEntity<>(logList, HttpStatus.OK);
		} catch (Exception e) {
			returnmap.put("status", "error");
			returnmap.put("message", e.getMessage());
			return new ResponseEntity<>(returnmap, HttpStatus.INTERNAL_SERVER_ERROR);
		}

	}*/

	@RequestMapping(path = { "getLogs" }, method = RequestMethod.POST)
	public ResponseEntity<?> getLogs(HttpServletRequest request, HttpSession httpSession,
			@RequestBody HashMap<String, Object> requestmap) throws InterruptedException, ExecutionException {
		HashMap<String, Object> returnmap = new HashMap<>();

		ResponseEntity<?> responseEntity = checkSessionKey(httpSession, Utils.getCookie(request, "JSESSIONID"), "LSLG");
		if (responseEntity != null) {
			logsServices.addLog(AppConstants.error, AppConstants.logApi, "getLogs", AppConstants.sessiontimeout, null);
			return responseEntity;
		}

		// System.out.print("Inside getLogs ");
		// The start record count is the record number that we have to fetch from the
		// start
		// The logId is incrementing so the logId is sorted descending and record number
		// is fetched
		try {
			//int logId = 0, noOfRecords = 0;
			String logDateStr = new Date().toString();
			String logTypeStr;
			if (requestmap.containsKey("logType")) {
				logTypeStr = requestmap.getOrDefault("logType", "").toString();
				if (Utils.isEmptyString(logTypeStr)) {

					throw new Exception("Log type not found");
				}
			} else {
				throw new Exception("Log type not found");
			}

			if (logTypeStr.equalsIgnoreCase(logType.server.name().toString())) {
				logTypeStr = "logs";
			} else if (logTypeStr.equalsIgnoreCase(logType.vehicle.name().toString())) {
				logTypeStr = "vehicle_logs";
			} else if (logTypeStr.equalsIgnoreCase(logType.staff.name().toString())) {
				logTypeStr = "staff_logs";
			} else if (logTypeStr.equalsIgnoreCase(logType.material.name().toString())) {
				logTypeStr = "material_logs";
			} else if (logTypeStr.equalsIgnoreCase(logType.visitor.name().toString())) {
				logTypeStr = "visitor_logs";
			} else if (logTypeStr.equalsIgnoreCase(logType.asset.name().toString())) {
				logTypeStr = "asset_logs";
			}  else if (logTypeStr.equalsIgnoreCase(logType.genie.name().toString())) {
				logTypeStr = "genie_logs";
			} else {
				System.out.print("Log type not found");
				throw new Exception("Log type not found");
			}

			int noOfRecords = 0;
			Date logDate = new Date();

			if (requestmap.containsKey("logDate")) {
				logDateStr = (String) requestmap.get("logDate");
				if(!Utils.isEmptyString(logDateStr)) { 

					logDate = new SimpleDateFormat(AppConstants.LogRequestDateFormat)
							.parse(logDateStr);
					if (Utils.isEmptyString(logDateStr)) {
						logDate =  new Date();
					} 
				} else {
					logDate =  new Date();
				}

			} else {
				logDate =  new Date();
			}

			if (requestmap.containsKey("noOfRecords")) {
				String noOfRecordsStr = (String) requestmap.get("noOfRecords");
				if (isNumeric(noOfRecordsStr)) {
					noOfRecords = Integer.valueOf("" + requestmap.get("noOfRecords"));
				} else {
					noOfRecords = listViewCount; // Magic
				}
			} else {
				noOfRecords = listViewCount; // Magic
			}

			if (noOfRecords <= 0)
				noOfRecords = listViewCount; // Magic

			List<Logs> logList = new ArrayList<>();
			
			//ZoneId zone = ZoneId.systemDefault();
		    SimpleDateFormat sdfDate = new SimpleDateFormat("yyyy-MMM-dd HH:mm:ss.SSS z");
		    //System.out.println(zone);
		    
			logList = logsViewRepository.SelectAllLogs(logDate, noOfRecords, logTypeStr);
			if (logList == null) {
				returnmap.put("status", "error");
				returnmap.put("message", "No Data Available");

				return new ResponseEntity<>(returnmap, HttpStatus.INTERNAL_SERVER_ERROR);
			}
						
			List<HashMap<String, Object>> returnmapList = new ArrayList<>();
			for(int i=0; i< logList.size();i++)
			{
				HashMap<String, Object> localMap = new HashMap<>();
				localMap.put("date", sdfDate.format(logList.get(i).getDate()));
				localMap.put("result", logList.get(i).getResult());
				localMap.put("minor", logList.get(i).getMinor());
				localMap.put("major", logList.get(i).getMajor());
				localMap.put("type", logList.get(i).getType());
				localMap.put("parameter", logList.get(i).getParameter());
				returnmapList.add(localMap);
			}
			
			return new ResponseEntity<>(returnmapList, HttpStatus.OK);
		} catch (Exception e) {
			returnmap.put("status", "error");
			returnmap.put("message", e.getMessage());
			return new ResponseEntity<>(returnmap, HttpStatus.INTERNAL_SERVER_ERROR);
		}

	}

	public static boolean isNumeric(String value) {
		return isNumeric(value, value.length());
	}

	public static boolean isNumeric(String value, int stringLength) {
		if (Utils.isEmptyString(value))
			return false;
		try {
			Integer.parseInt(value);
		} catch (NumberFormatException ex) {
			return false;
		} catch (NullPointerException ex) {
			return false;
		}
		return true;
	}

}
