package com.entappia.ei4o.controllers;

import java.lang.management.ManagementFactory;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
 
import com.entappia.ei4o.constants.AppConstants;
import com.entappia.ei4o.dbmodels.Department;
import com.entappia.ei4o.services.LogsServices;
import com.entappia.ei4o.services.QuuppaAPIService;
import com.entappia.ei4o.utils.BaseController;
import com.entappia.ei4o.utils.Utils;
import com.sun.management.OperatingSystemMXBean;

@RestController
public class DeviceDetailsController extends BaseController {

	@Autowired
	private QuuppaAPIService quuppaAPIService;
	
	@Autowired
	private LogsServices logsServices;

	@RequestMapping(path = { "deviceDetails" }, method = RequestMethod.GET)
	public ResponseEntity<?> getDeviceDetails(HttpServletRequest request, HttpSession httpSession,
			@PathVariable Optional<String> deptName) throws InterruptedException, ExecutionException {
		Map<String, Object> returnmap = new HashMap<>();
	 
		try {
			// "CDPT"
			ResponseEntity<?> responseEntity = checkSessionKey(httpSession, Utils.getCookie(request, "JSESSIONID"));
			if (responseEntity != null) {
				logsServices.addLog(AppConstants.error, AppConstants.DEVICEDETAILS, "getDeviceDetails",
						AppConstants.sessiontimeout, null);
				return responseEntity;
			}

			logsServices.addLog(AppConstants.event, AppConstants.DEVICEDETAILS, "getDeviceDetails",
					"DeviceDetails get API Call", null);

			CompletableFuture<JSONObject> qpeCompletableFuture;
			try {
				qpeCompletableFuture = quuppaAPIService.getQPEInfo();
				CompletableFuture.allOf(qpeCompletableFuture).join();

				if (qpeCompletableFuture.isDone()) {

					try {
						JSONObject jsonObject = qpeCompletableFuture.get();
						if (jsonObject != null) {
							String status = jsonObject.optString("status");
							if (AppConstants.print_log)
								System.out.println("getQPEStart status==" + status);

							
							HashMap<String, Object> map = new HashMap<>();
							
							if (!Utils.isEmptyString(status) && status.equals("success")) {
								 
								long memoryFree = jsonObject.getLong("memoryFree");
								long memoryMax = jsonObject.getLong("memoryMax");
								long memoryUsed = jsonObject.getLong("memoryUsed");
								long memoryAllocated = jsonObject.getLong("memoryAllocated");
								double cpuLoad = jsonObject.getDouble("cpuLoad") * 100;
								long diskFree = jsonObject.getLong("diskFree");

								DecimalFormat df = new DecimalFormat("#.##");

								//String qpeVersion = jsonObject.getString("qpeVersion");

								HashMap<String, Object> jvmmap = new HashMap<>();
								jvmmap.put("free", memoryFree);
								jvmmap.put("total", memoryMax);
								jvmmap.put("used", memoryUsed);
								jvmmap.put("allocated", memoryAllocated);
								jvmmap.put("cpuLoad", Double.valueOf(df.format(cpuLoad)));
								jvmmap.put("diskFree", diskFree);
 

								map.put("jvmMemory", jvmmap);
							}
							
							OperatingSystemMXBean osBean = ManagementFactory
									.getPlatformMXBean(OperatingSystemMXBean.class);

							long total = osBean.getTotalPhysicalMemorySize();
							long free = osBean.getFreePhysicalMemorySize();
							long used = total - free;

							HashMap<String, Object> systemMemory = new HashMap<>();
							systemMemory.put("free", free);
							systemMemory.put("total", total);
							systemMemory.put("used", used);
							
							map.put("systemMemory", systemMemory);

							returnmap.put("data", map);
						}
					} catch (ExecutionException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}

				}

			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

			logsServices.addLog(AppConstants.success, AppConstants.DEVICEDETAILS, "getDeviceDetails",
					"Department details", null);

			returnmap.put("status", "success");

			return new ResponseEntity<>(returnmap, HttpStatus.OK);
		} catch (Exception e) {
			returnmap.put("status", "error");
			returnmap.put("message", AppConstants.errormsg);
			logsServices.addLog(AppConstants.error, AppConstants.DEVICEDETAILS, "getDeviceDetails",
					AppConstants.errormsg, null);

			return new ResponseEntity<>(returnmap, HttpStatus.OK);
		}

	}

}
