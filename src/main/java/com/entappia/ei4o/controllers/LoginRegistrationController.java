package com.entappia.ei4o.controllers;

import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ExecutionException;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.apache.commons.codec.binary.Base64;
import org.json.JSONException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.core.io.ResourceLoader;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartResolver;

import com.entappia.ei4o.constants.AppConstants;
import com.entappia.ei4o.dbmodels.Login;
import com.entappia.ei4o.dbmodels.Settings;
import com.entappia.ei4o.models.Alert;
import com.entappia.ei4o.repository.LoginRegistrationRepository;
import com.entappia.ei4o.repository.SettingsRepository;
import com.entappia.ei4o.repository.TableUtils;
import com.entappia.ei4o.services.LogsServices;
import com.entappia.ei4o.utils.BaseController;
import com.entappia.ei4o.utils.SessionCount;
import com.entappia.ei4o.utils.SmsUtil;
import com.entappia.ei4o.utils.Utils;

@RestController
public class LoginRegistrationController extends BaseController {
	@Autowired
	private LoginRegistrationRepository loginRegistrationRepository;

	@Autowired
	TableUtils tableUtils;

	@Autowired
	private LogsServices logsServices;

	@Autowired
	private SettingsRepository settingsRepository;

	@Autowired
	private ResourceLoader resourceLoader;

	@Autowired
	private SessionCount sessionCount;

	@CrossOrigin(origins = "*")
	@RequestMapping(path = { "/test" }, method = RequestMethod.POST)
	public ResponseEntity<?> test(@RequestBody HashMap<String, String> requestMap, HttpSession session)
			throws InterruptedException, ExecutionException, JSONException {
		Map<String, Object> returnmap = new HashMap<>();
		returnmap.put("status", "success");
		boolean bool = tableUtils.createTable(requestMap.get("name"), requestMap.get("date"),
				requestMap.get("shiftName"));

		if (bool) {
			returnmap.put("message", "table created!!!");
		} else
			returnmap.put("message", "table creation failed!!!");

		return new ResponseEntity<>(returnmap, HttpStatus.OK);
	}

	@CrossOrigin(origins = "*")
	@RequestMapping(path = { "/login" }, method = RequestMethod.POST)
	public ResponseEntity<?> login(@RequestBody Login login, HttpSession session)
			throws InterruptedException, ExecutionException, JSONException {
		Map<String, Object> returnmap = new HashMap<>();
		try {
			if (loginRegistrationRepository != null && login != null) {

				if (Utils.isEmptyString(login.getEmailID()) || Utils.isEmptyString(login.getPassword())) {

					returnmap.put("status", "error");
					returnmap.put("message", AppConstants.fieldsmissing);
					logsServices.addLog(AppConstants.error, AppConstants.credential, "login", "Tag ID does not exist",
							null);

					return new ResponseEntity<>(returnmap, HttpStatus.UNAUTHORIZED);

				}
				Login login1 = loginRegistrationRepository.findByEmailID(login.getEmailID());
				if (login1 == null) {
					returnmap.put("status", "error");
					returnmap.put("message", "Email ID does not exist");
					logsServices.addLog(AppConstants.error, AppConstants.credential, "login", "Email ID does not exist",
							null);

					return new ResponseEntity<>(returnmap, HttpStatus.UNAUTHORIZED);
				}
				if (!login1.getPassword().contentEquals(Utils.encryption(login.getPassword()))) {
					returnmap.put("status", "error");
					returnmap.put("message", "Invalid Password");
					logsServices.addLog(AppConstants.error, AppConstants.credential, "login", "Invalid Password", null);
					return new ResponseEntity<>(returnmap, HttpStatus.UNAUTHORIZED);
				} else {
					returnmap.put("status", "success");
					long readNotificationCount = 0;
					String profileColor = "";
					boolean showNotification = false;

					Settings settings = settingsRepository.findByEmailId(login1.getEmailID());
					if (settings != null) {
						readNotificationCount = settings.getNotificationId();
						profileColor = settings.getProfileColor();
						showNotification = settings.isShowNotification();

					}
					HashMap<String, Object> profileMap = new HashMap<>();
					profileMap.put("userEmail", "" + login1.getEmailID());
					profileMap.put("userName", "" + login1.getDisplayName());
					profileMap.put("userType", "" + login1.getUserType());
					profileMap.put("userloginType", "" + login1.getUserType());
					profileMap.put("userMobile", "" + login1.getMobileNumber());
					profileMap.put("capabilityList", login1.getCapabilityList());
					profileMap.put("orgAlert", login1.getOrgAlert());

					if (login1.getProfileImage() != null)
						profileMap.put("profileImage", Base64.encodeBase64String(login1.getProfileImage()));
					else
						profileMap.put("profileImage", "");
					profileMap.put("readNotificationCount", readNotificationCount);
					profileMap.put("profileColor", profileColor);
					profileMap.put("showNotification", showNotification);
					returnmap.put("profile", profileMap);

					String userType = "";
					if (login.getUserType() != null && login.getUserType().equalsIgnoreCase("genie")) {
						userType = "genie";
					} else
						userType = login1.getType().equalsIgnoreCase("Engineering") ? "Engineering" : "dashboard";

					HttpSession httpSession = handleSession(session, userType, login1.getEmailID().toLowerCase(),
							login1.getCapabilityList().get("allowedMenu"), login1.getDisplayName());

					sessionCount.addSession(httpSession);
					logsServices.addLog(AppConstants.event, AppConstants.credential, "login",
							login1.getEmailID() + " Logged in successfully", null);
					return new ResponseEntity<>(returnmap, HttpStatus.OK);

				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			returnmap.put("status", "error");
			returnmap.put("message", AppConstants.errormsg);
			logsServices.addLog(AppConstants.error, AppConstants.credential, "login", AppConstants.errormsg, null);
			return new ResponseEntity<>(returnmap, HttpStatus.INTERNAL_SERVER_ERROR);
		}
		return null;

	}

	@CrossOrigin(origins = "*")
	@RequestMapping(path = { "/registration" }, method = RequestMethod.POST)
	public ResponseEntity<?> registration(HttpServletRequest request, @RequestBody Login signup, HttpSession session)
			throws InterruptedException, ExecutionException, JSONException {

		Map<String, Object> returnmap = new HashMap<>();
		try {
			if (loginRegistrationRepository != null && signup != null) {
				if (Utils.isEmptyString(signup.getType()) || Utils.isEmptyString(signup.getDisplayName())
						|| Utils.isEmptyString(signup.getEmailID()) || Utils.isEmptyString(signup.getPassword())
						|| Utils.isEmptyString(signup.getUserType()) || Utils.isEmptyString(signup.getMobileNumber())) {

					returnmap.put("status", "error");
					returnmap.put("message", AppConstants.fieldsmissing);

					logsServices.addLog(AppConstants.error, AppConstants.credential, "registration",
							returnmap.get("message").toString(), null);
					return new ResponseEntity<>(returnmap, HttpStatus.UNAUTHORIZED);
				}
				if (loginRegistrationRepository.findByEmailID(signup.getEmailID().toLowerCase()) != null) {
					returnmap.put("status", "error");
					returnmap.put("message", "Email ID already exist");
					logsServices.addLog(AppConstants.error, AppConstants.credential, "registration",
							returnmap.get("message").toString(), null);

					return new ResponseEntity<>(returnmap, HttpStatus.UNAUTHORIZED);
				}
				if (!signup.getType().equals("dashboard") && !signup.getType().equals("mobile")) {
					returnmap.put("status", "error");
					returnmap.put("message", "Invalid Type");
					logsServices.addLog(AppConstants.error, AppConstants.credential, "registration",
							returnmap.get("message").toString(), null);

					return new ResponseEntity<>(returnmap, HttpStatus.UNAUTHORIZED);
				}

				if (!signup.getUserType().equals("SuperUser") && !signup.getUserType().equals("User")
						&& !signup.getUserType().equals("Manager") && !signup.getUserType().equals("SystemAdmin")) {
					returnmap.put("status", "error");
					returnmap.put("message", "Invalid user type");
					logsServices.addLog(AppConstants.error, AppConstants.credential, "registration",
							returnmap.get("message").toString(), null);

					return new ResponseEntity<>(returnmap, HttpStatus.UNAUTHORIZED);
				}

				String fileName = signup.getUserType();
				/*
				 * if (signup.getUserType().equals("SuperUser")) fileName = "SuperUser"; else if
				 * (signup.getUserType().equals("User")) fileName = "User"; else if
				 * (signup.getUserType().equals("Manager")) fileName = "Manager"; else if
				 * (signup.getUserType().equals("SystemAdmin")) fileName = "SystemAdmin";
				 */

				HashMap<String, HashMap<String, Integer>> capabilityList = Utils.readCapability(resourceLoader,
						fileName);

				signup.setPassword(Utils.encryption(signup.getPassword()));
				signup.setEmailID(signup.getEmailID().toLowerCase());
				signup.setCreatedDate(new Date());
				signup.setCapabilityList(capabilityList);
				signup.setOrgAlert(null);
				signup.setDeviceDetails(null);
				signup.setLastSession(null);
				signup.setProfileImage("".getBytes());

				loginRegistrationRepository.save(signup);

				returnmap.put("status", "success");
				returnmap.put("message", signup.getEmailID().toLowerCase() + " Created");
				logsServices.addLog(AppConstants.event, AppConstants.credential, "registration",
						returnmap.get("message").toString(), null);

				return new ResponseEntity<>(returnmap, HttpStatus.OK);
			}
		} catch (Exception e) {
			returnmap.put("status", "error");
			returnmap.put("message", AppConstants.errormsg);
			logsServices.addLog(AppConstants.error, AppConstants.credential, "registration",
					returnmap.get("message").toString(), null);

			return new ResponseEntity<>(returnmap, HttpStatus.INTERNAL_SERVER_ERROR);
		}

		return null;

	}

	@GetMapping(value = "/logoutUser")
	public ResponseEntity<?> logout(HttpServletRequest request, HttpSession httpSession)
			throws InterruptedException, ExecutionException, JSONException {
		Map<String, Object> returnmap = new HashMap<>();
		try {

			returnmap.put("status", "success");
			httpSession.invalidate();
			return new ResponseEntity<>(returnmap, HttpStatus.OK);
		} catch (Exception e) {
			returnmap.put("status", "error");
			returnmap.put("message", AppConstants.errormsg);
			return new ResponseEntity<>(returnmap, HttpStatus.OK);
		}

	}

	@PostMapping(value = "/forgotpassword")
	public ResponseEntity<?> forgotPassword(@RequestBody HashMap<String, Object> resetpasswordMap)
			throws InterruptedException {

		Map<String, Object> returnmap = new HashMap<>();
		if (!resetpasswordMap.containsKey("userid") || Utils.isEmptyString(resetpasswordMap.get("userid").toString())) {

			returnmap.put("status", "error");
			returnmap.put("message", AppConstants.fieldsmissing);
			logsServices.addLog(AppConstants.error, AppConstants.credential, "forgotpassword",
					returnmap.get("message").toString(), null);

			return new ResponseEntity<>(returnmap, HttpStatus.OK);

		}

		Login login = loginRegistrationRepository.findByEmailID(resetpasswordMap.get("userid").toString());
		if (login == null) {
			returnmap.put("status", "error");
			returnmap.put("message", "Invalid User ID");
			logsServices.addLog(AppConstants.error, AppConstants.credential, "forgotpassword",
					returnmap.get("message").toString(), null);

			return new ResponseEntity<>(returnmap, HttpStatus.OK);
		}

		String password = Utils.decryption(login.getPassword());

		SmsUtil.sendPassword(login.getDisplayName(), password, login.getEmailID());

		returnmap.put("status", "success");
		returnmap.put("message", "Your password has been sent to " + login.getEmailID());
		logsServices.addLog(AppConstants.event, AppConstants.credential, "forgotpassword",
				"Success, password has been sent to " + login.getEmailID(), null);

		return new ResponseEntity<>(returnmap, HttpStatus.OK);

	}

	@PostMapping(value = "/changepassword")
	public ResponseEntity<?> changePassword(HttpServletRequest request, HttpSession httpSession,
			@RequestBody HashMap<String, Object> changePasswordMap) throws InterruptedException {

		Map<String, Object> returnmap = new HashMap<>();

		if (!changePasswordMap.containsKey("userid") || Utils.isEmptyString(changePasswordMap.get("userid").toString())
				|| !changePasswordMap.containsKey("password")
				|| Utils.isEmptyString(changePasswordMap.get("password").toString())
				|| !changePasswordMap.containsKey("old_password")
				|| Utils.isEmptyString(changePasswordMap.get("old_password").toString())) {

			returnmap.put("status", "error");
			returnmap.put("message", AppConstants.fieldsmissing);
			logsServices.addLog(AppConstants.error, AppConstants.credential, "changepassword",
					returnmap.get("message").toString(), null);

			return new ResponseEntity<>(returnmap, HttpStatus.OK);

		}

		Login signup = loginRegistrationRepository
				.findByEmailID(changePasswordMap.get("userid").toString().toLowerCase());
		if (signup == null) {
			returnmap.put("status", "error");
			returnmap.put("message", "Invalid User ID");
			logsServices.addLog(AppConstants.error, AppConstants.credential, "changepassword",
					returnmap.get("message").toString(), null);

			return new ResponseEntity<>(returnmap, HttpStatus.OK);
		}

		String oldDbPassword = Utils.decryption(signup.getPassword());

		if (!oldDbPassword.equals(changePasswordMap.get("old_password").toString())) {
			returnmap.put("status", "error");
			returnmap.put("message", "Invalid old password");
			logsServices.addLog(AppConstants.error, AppConstants.credential, "changepassword",
					returnmap.get("message").toString(), null);

			return new ResponseEntity<>(returnmap, HttpStatus.OK);
		}

		String password = changePasswordMap.get("password").toString();

		signup.setPassword(Utils.encryption(password));
		loginRegistrationRepository.save(signup);

		returnmap.put("status", "success");
		returnmap.put("message", "Password changed successfully");
		logsServices.addLog(AppConstants.event, AppConstants.credential, "changepassword",
				returnmap.get("message").toString(), null);

		return new ResponseEntity<>(returnmap, HttpStatus.OK);

	}

	@CrossOrigin(origins = "*")
	@RequestMapping(path = { "/getRegisteredUsers" }, method = RequestMethod.GET)
	public ResponseEntity<?> getRegisteredUsers(HttpSession session)
			throws InterruptedException, ExecutionException, JSONException {
		Map<String, Object> returnmap = new HashMap<>();

		List<Login> loginList = loginRegistrationRepository.findAll();
		if (loginList == null || loginList.size() <= 0) {
			returnmap.put("status", "error");
			returnmap.put("message", "No data found.");
			returnmap.put("data", loginList);
			logsServices.addLog(AppConstants.error, AppConstants.credential, "getRegisteredUsers",
					returnmap.get("message").toString(), null);

			return new ResponseEntity<>(returnmap, HttpStatus.OK);
		}

		returnmap.put("status", "success");
		returnmap.put("message", loginList.size() + " users found");
		returnmap.put("data", loginList);
		logsServices.addLog(AppConstants.event, AppConstants.credential, "getRegisteredUsers",
				returnmap.get("message").toString(), null);

		return new ResponseEntity<>(returnmap, HttpStatus.OK);
	}

	@CrossOrigin(origins = "*")
	@RequestMapping(path = { "/updateCapabilityDetails" }, method = RequestMethod.PUT)
	public ResponseEntity<?> updateCapabilityDetails(@RequestBody Login login, HttpSession session)
			throws InterruptedException, ExecutionException, JSONException {
		Map<String, Object> returnmap = new HashMap<>();

		if (Utils.isEmptyString(login.getEmailID()) || login.getCapabilityList() == null
				|| login.getCapabilityList().size() <= 0) {

			returnmap.put("status", "error");
			returnmap.put("message", AppConstants.fieldsmissing);

			logsServices.addLog(AppConstants.error, AppConstants.credential, "updateCapabilityDetails",
					returnmap.get("message").toString(), null);
			return new ResponseEntity<>(returnmap, HttpStatus.UNAUTHORIZED);
		}

		Login dbLogin = loginRegistrationRepository.findByEmailID(login.getEmailID());
		if (dbLogin == null) {
			returnmap.put("status", "error");
			returnmap.put("message", "Email id not found.");
			logsServices.addLog(AppConstants.error, AppConstants.credential, "updateCapabilityDetails",
					returnmap.get("message").toString(), null);

			return new ResponseEntity<>(returnmap, HttpStatus.OK);
		}

		dbLogin.setCapabilityList(login.getCapabilityList());
		loginRegistrationRepository.save(dbLogin);

		returnmap.put("status", "success");
		returnmap.put("message", "Capability Details updated successfully.");
		logsServices.addLog(AppConstants.event, AppConstants.credential, "updateCapabilityDetails",
				returnmap.get("message").toString(), null);

		return new ResponseEntity<>(returnmap, HttpStatus.OK);
	}

	@CrossOrigin(origins = "*")
	@RequestMapping(path = { "/updateAlertDetails" }, method = RequestMethod.PUT)
	public ResponseEntity<?> updateAlertDetails(@RequestBody Login login, HttpSession session)
			throws InterruptedException, ExecutionException, JSONException {
		Map<String, Object> returnmap = new HashMap<>();

		if (Utils.isEmptyString(login.getEmailID()) || login.getOrgAlert() == null || login.getOrgAlert().size() <= 0) {

			returnmap.put("status", "error");
			returnmap.put("message", AppConstants.fieldsmissing);

			logsServices.addLog(AppConstants.error, AppConstants.credential, "updateAlertDetails",
					returnmap.get("message").toString(), null);
			return new ResponseEntity<>(returnmap, HttpStatus.UNAUTHORIZED);
		}

		Login dbLogin = loginRegistrationRepository.findByEmailID(login.getEmailID());
		if (dbLogin == null) {
			returnmap.put("status", "error");
			returnmap.put("message", "Email id not found.");
			logsServices.addLog(AppConstants.error, AppConstants.credential, "updateAlertDetails",
					returnmap.get("message").toString(), null);

			return new ResponseEntity<>(returnmap, HttpStatus.OK);
		}

		List<String> alertKeysList = Arrays.asList(AppConstants.alertKeys);

		HashMap<String, Alert> alertMap = login.getOrgAlert();
		Set<String> alertKeys = alertMap.keySet();
		for (String key : alertKeys) {

			if (!alertKeysList.contains(key)) {

				returnmap.put("status", "error");
				returnmap.put("message", "Invalid key value");

				logsServices.addLog(AppConstants.error, AppConstants.credential, "updateAlertDetails",
						returnmap.get("message").toString(), null);
				return new ResponseEntity<>(returnmap, HttpStatus.UNAUTHORIZED);
			}

		}

		dbLogin.setOrgAlert(login.getOrgAlert());
		loginRegistrationRepository.save(dbLogin);

		returnmap.put("status", "success");
		returnmap.put("message", "Alert info updated successfully.");
		logsServices.addLog(AppConstants.event, AppConstants.credential, "updateAlertDetails",
				returnmap.get("message").toString(), null);

		return new ResponseEntity<>(returnmap, HttpStatus.OK);
	}

	@PostMapping(value = "/uploadProfileImage")
	public ResponseEntity<?> uploadProfileImage(HttpSession session, @RequestBody HashMap<String, String> requestMap)
			throws InterruptedException, ExecutionException, JSONException {

		Map<String, Object> returnmap = new HashMap<>();

		if (Utils.isEmptyString(requestMap.getOrDefault("emailId", ""))
				|| Utils.isEmptyString(requestMap.getOrDefault("profileImage", ""))) {

			returnmap.put("status", "error");
			returnmap.put("message", AppConstants.fieldsmissing);

			logsServices.addLog(AppConstants.error, AppConstants.credential, "uploadProfileImage",
					returnmap.get("message").toString(), null);
			return new ResponseEntity<>(returnmap, HttpStatus.UNAUTHORIZED);
		}

		Login dbLogin = loginRegistrationRepository.findByEmailID(requestMap.getOrDefault("emailId", ""));
		if (dbLogin == null) {
			returnmap.put("status", "error");
			returnmap.put("message", "Email id not found.");
			logsServices.addLog(AppConstants.error, AppConstants.credential, "uploadProfileImage",
					returnmap.get("message").toString(), null);

			return new ResponseEntity<>(returnmap, HttpStatus.OK);
		}

		byte[] profileImage = Base64.decodeBase64(requestMap.getOrDefault("profileImage", "").split(",")[1]);

		// byte[] profileImage = file.getBytes();
		dbLogin.setProfileImage(profileImage);

		loginRegistrationRepository.save(dbLogin);

		returnmap.put("status", "success");
		returnmap.put("message", "Profile Image uploaded successfully.");
		logsServices.addLog(AppConstants.event, AppConstants.credential, "updateAlertDetails",
				returnmap.get("message").toString(), null);

		return new ResponseEntity<>(returnmap, HttpStatus.OK);
	}

	/*
	 * @PostMapping(value = "/uploadProfileImage", consumes = "multipart/form-data")
	 * public ResponseEntity<?> uploadProfileImage(@RequestParam("emailId") String
	 * emailId,
	 * 
	 * @RequestParam("file") MultipartFile file, HttpSession session) throws
	 * InterruptedException, ExecutionException, JSONException {
	 * 
	 * Map<String, Object> returnmap = new HashMap<>();
	 * 
	 * if (Utils.isEmptyString(emailId) || file == null) {
	 * 
	 * returnmap.put("status", "error"); returnmap.put("message",
	 * AppConstants.fieldsmissing);
	 * 
	 * logsServices.addLog(AppConstants.error, AppConstants.credential,
	 * "uploadProfileImage", returnmap.get("message").toString(), null); return new
	 * ResponseEntity<>(returnmap, HttpStatus.UNAUTHORIZED); }
	 * 
	 * Login dbLogin = loginRegistrationRepository.findByEmailID(emailId); if
	 * (dbLogin == null) { returnmap.put("status", "error");
	 * returnmap.put("message", "Email id not found.");
	 * logsServices.addLog(AppConstants.error, AppConstants.credential,
	 * "uploadProfileImage", returnmap.get("message").toString(), null);
	 * 
	 * return new ResponseEntity<>(returnmap, HttpStatus.OK); }
	 * 
	 * try { byte[] profileImage = file.getBytes();
	 * dbLogin.setProfileImage(profileImage); } catch (IOException e) { // TODO
	 * Auto-generated catch block e.printStackTrace(); }
	 * 
	 * loginRegistrationRepository.save(dbLogin);
	 * 
	 * returnmap.put("status", "success"); returnmap.put("message",
	 * "Profile Image uploaded successfully.");
	 * logsServices.addLog(AppConstants.event, AppConstants.credential,
	 * "updateAlertDetails", returnmap.get("message").toString(), null);
	 * 
	 * return new ResponseEntity<>(returnmap, HttpStatus.OK); }
	 */

	@Bean
	public MultipartResolver multipartResolver() {
		org.springframework.web.multipart.commons.CommonsMultipartResolver multipartResolver = new org.springframework.web.multipart.commons.CommonsMultipartResolver();
		multipartResolver.setMaxUploadSize(157286400);
		return multipartResolver;
	}

}
