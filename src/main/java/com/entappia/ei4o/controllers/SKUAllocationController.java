package com.entappia.ei4o.controllers;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.concurrent.ExecutionException;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.entappia.ei4o.constants.AppConstants;
import com.entappia.ei4o.dbmodels.SKUAllocation;
import com.entappia.ei4o.dbmodels.Tags;
import com.entappia.ei4o.repository.SKUAllocationRepository;
import com.entappia.ei4o.repository.TagsRepository;
import com.entappia.ei4o.services.LogsServices;
import com.entappia.ei4o.services.SKUEvent;
import com.entappia.ei4o.utils.BaseController;
import com.entappia.ei4o.utils.Utils;

@RestController
public class SKUAllocationController extends BaseController {

	@Autowired
	private LogsServices logsServices;

	@Autowired
	private SKUAllocationRepository skuAllocationRepository;

	@Autowired
	private TagsRepository tagsRepository;

	@Autowired
	private SKUEvent skuEvent;

	@PostMapping(value = "/skuAllocation")
	public ResponseEntity<?> skuAllocation(HttpServletRequest request, HttpSession httpSession,
			@RequestBody HashMap<String, Object> requestMap) throws InterruptedException, ExecutionException {

		ResponseEntity<?> responseEntity = checkSessionKey(httpSession, Utils.getCookie(request, "JSESSIONID"), "STAR");
		if (responseEntity != null) { 
			logsServices.addLog(AppConstants.error, AppConstants.SKUALLOCATION, "skuAllocation",
					AppConstants.sessiontimeout, null);
			return responseEntity; 
		}
		
		Map<String, String> reuestMap = new HashMap<>();
		reuestMap.put("params", new JSONObject(requestMap).toString()); 

		
		logsServices.addLog(AppConstants.event, AppConstants.SKUALLOCATION, "skuAllocation",
				"SKU Allocation api call", reuestMap);
		
		
		Map<String, String> returnmap = new HashMap<>();
		if (Utils.isEmptyString(requestMap.getOrDefault("tagId", "").toString())
				|| Utils.isEmptyString(requestMap.getOrDefault("upcCode", "").toString()) 
				|| Utils.isEmptyString(requestMap.getOrDefault("skuNo", "").toString()) 
				|| Utils.isEmptyString(requestMap.getOrDefault("description", "").toString())
				|| Utils.isEmptyString(requestMap.getOrDefault("transaction", "").toString())
				|| Utils.isEmptyString(requestMap.getOrDefault("quantity", "").toString())
				|| Utils.isEmptyString(requestMap.getOrDefault("isLSLM", "").toString())) {

			returnmap.put("status", "error");
			returnmap.put("message", AppConstants.fieldsmissing);

			logsServices.addLog(AppConstants.error, AppConstants.SKUALLOCATION, "skuAllocation",
					AppConstants.fieldsmissing, null);
			return new ResponseEntity<>(returnmap, HttpStatus.OK);
		}

		if (Boolean.valueOf(requestMap.get("isLSLM").toString())) {

			if (Utils.isEmptyString(requestMap.getOrDefault("expiryDate", "").toString())) {

				returnmap.put("status", "error");
				returnmap.put("message", AppConstants.fieldsmissing);

				logsServices.addLog(AppConstants.error, AppConstants.SKUALLOCATION, "skuAllocation",
						AppConstants.fieldsmissing, null);
				return new ResponseEntity<>(returnmap, HttpStatus.OK);
			} else if (!Utils.validateDate(requestMap.getOrDefault("expiryDate", "").toString())) {

				returnmap.put("status", "error");
				returnmap.put("message", AppConstants.errorDatemsg);

				logsServices.addLog(AppConstants.error, AppConstants.SKUALLOCATION, "skuAllocation",
						AppConstants.errorDatemsg, null);
				return new ResponseEntity<>(returnmap, HttpStatus.OK);
			}
		}

		Tags tags = tagsRepository.findByTagId(requestMap.getOrDefault("tagId", "").toString());

		if (tags == null) {
			returnmap.put("status", "error");
			returnmap.put("message", "Tag id not found.");

			logsServices.addLog(AppConstants.error, AppConstants.SKUALLOCATION, "skuAllocation",
					returnmap.get("message"), null);
			return new ResponseEntity<>(returnmap, HttpStatus.OK);
		} else if (!Utils.isEmptyString(tags.getStatus()) && !tags.getStatus().equalsIgnoreCase("free")) {
			returnmap.put("status", "error");

			if (tags.getStatus().equalsIgnoreCase("assigned"))
				returnmap.put("message", "Tag already assigned to " + tags.getType());
			else
				returnmap.put("message", "Can not assign " + tags.getStatus() + " tag.");

			logsServices.addLog(AppConstants.error, AppConstants.SKUALLOCATION, "skuAllocation",
					returnmap.get("message"), null);
			return new ResponseEntity<>(returnmap, HttpStatus.OK);
		}

		HashMap<String, String> skuMap = skuEvent.getSKUDetails(requestMap.getOrDefault("upcCode", "").toString());
		if (skuMap == null) {
			returnmap.put("status", "error");
			returnmap.put("message", "UPC Code not found.");

			logsServices.addLog(AppConstants.error, AppConstants.SKUALLOCATION, "skuAllocation",
					returnmap.get("message"), null);
			return new ResponseEntity<>(returnmap, HttpStatus.OK);
		}

		SKUAllocation skuAllocation = skuAllocationRepository.findByUPCCode(skuMap.get("upcCode"));
		if (skuAllocation != null && !skuAllocation.getStatus().equalsIgnoreCase("completed")) {
			returnmap.put("status", "error");
			returnmap.put("message", "UPC Code already assigned to "+skuAllocation.getTagId());

			logsServices.addLog(AppConstants.error, AppConstants.SKUALLOCATION, "skuAllocation",
					returnmap.get("message"), null);
			return new ResponseEntity<>(returnmap, HttpStatus.OK);
		}

		SKUAllocation skuAllocation1 = skuAllocationRepository.findByValidTagId(requestMap.getOrDefault("tagId", "").toString());

		if (skuAllocation1 == null) {
			if (skuAllocation == null) {
				skuAllocation = new SKUAllocation();
				skuAllocation.setUpcCode(skuMap.get("upcCode"));
				skuAllocation.setTagId(requestMap.getOrDefault("tagId", "").toString());
			}

			skuAllocation.setTagId(requestMap.getOrDefault("tagId", "").toString());
			skuAllocation.setIssueDate(new Date());
			skuAllocation.setSkuNo(skuMap.get("skuNo"));
			skuAllocation.setBatchNo(requestMap.getOrDefault("batchNo", "").toString());
			skuAllocation.setDescription(requestMap.getOrDefault("description", "").toString());
			skuAllocation.setTransaction(Boolean.valueOf(requestMap.get("transaction").toString()));
			skuAllocation.setQuantity(Float.valueOf(requestMap.getOrDefault("quantity", "0").toString()));
			skuAllocation.setAvailableQuantity(Float.valueOf(requestMap.getOrDefault("quantity", "0").toString()));
			skuAllocation.setLSLM(Boolean.valueOf(requestMap.get("isLSLM").toString()));
			skuAllocation.setStatus("Valid");
			if (Boolean.valueOf(requestMap.get("isLSLM").toString())) {
				try {
					Date expiryDate = new SimpleDateFormat("yyyy-MMM-dd")
							.parse(requestMap.getOrDefault("expiryDate", "").toString());
					skuAllocation.setExpiryDate(expiryDate);
				} catch (ParseException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}

			} else
				skuAllocation.setExpiryDate(null);

			skuAllocationRepository.save(skuAllocation);

			if (skuAllocation.isLSLM()) {
				tags.setType("LSLM");
			} else
				tags.setType("Inventory");

			tags.setSubType("");
			tags.setAssignmentId(skuAllocation.getUpcCode());
			tags.setAssignmentDate(new Date());
			tags.setModifiedDate(new Date());
			tags.setStatus("Assigned");
			

			tagsRepository.save(tags);

			returnmap.put("status", "success");
			returnmap.put("message", "Tag assigned successfully to " + skuAllocation.getUpcCode());

			logsServices.addLog(AppConstants.event, AppConstants.SKUALLOCATION, "skuAllocation",
					returnmap.get("message"), null);
			return new ResponseEntity<>(returnmap, HttpStatus.OK);

		} else {

			returnmap.put("status", "error");
			returnmap.put("message", "Tag already assigned to " + skuAllocation1.getUpcCode());

			logsServices.addLog(AppConstants.error, AppConstants.SKUALLOCATION, "skuAllocation",
					returnmap.get("message"), null);
			return new ResponseEntity<>(returnmap, HttpStatus.OK);
		}

	}

	@PostMapping(value = "/updateSKUAssetAvailableQuantity")
	public ResponseEntity<?> updateSKUAssetAvailableQuantity(HttpServletRequest request, HttpSession httpSession,
			@RequestBody HashMap<String, Object> requestMap) throws InterruptedException, ExecutionException {

		ResponseEntity<?> responseEntity = checkSessionKey(httpSession, Utils.getCookie(request, "JSESSIONID"), "SDET");
		if (responseEntity != null) { 
			logsServices.addLog(AppConstants.error, AppConstants.SKUALLOCATION, "updateSKUAssetAvailableQuantity",
					AppConstants.sessiontimeout, null);
			return responseEntity; 
		}
		

		Map<String, String> reuestMap = new HashMap<>();
		reuestMap.put("params", new JSONObject(requestMap).toString()); 
		
		logsServices.addLog(AppConstants.event, AppConstants.SKUALLOCATION, "updateSKUAssetAvailableQuantity",
				"SKU Asset available quantity update api call", reuestMap);
		
		
		Map<String, String> returnmap = new HashMap<>();
		if (Utils.isEmptyString(requestMap.getOrDefault("tagId", "").toString())
				|| Utils.isEmptyString(requestMap.getOrDefault("upcCode", "").toString())
				|| Utils.isEmptyString(requestMap.getOrDefault("quantity", "").toString())) {

			returnmap.put("status", "error");
			returnmap.put("message", AppConstants.fieldsmissing);

			logsServices.addLog(AppConstants.error, AppConstants.SKUALLOCATION, "updateSKUAssetAvailableQuantity",
					AppConstants.fieldsmissing, null);
			return new ResponseEntity<>(returnmap, HttpStatus.OK);
		}

		Tags tags = tagsRepository.findByTagId(requestMap.getOrDefault("tagId", "").toString());

		if (tags == null) {
			returnmap.put("status", "error");
			returnmap.put("message", "Tag id not found.");

			logsServices.addLog(AppConstants.error, AppConstants.SKUALLOCATION, "updateSKUAssetAvailableQuantity",
					returnmap.get("message"), null);
			return new ResponseEntity<>(returnmap, HttpStatus.OK);
		} else if (tags.getStatus().equalsIgnoreCase("Free")) {
			returnmap.put("status", "error");
			returnmap.put("message", "Tag not assigned");

			logsServices.addLog(AppConstants.error, AppConstants.SKUALLOCATION, "updateSKUAssetAvailableQuantity",
					returnmap.get("message"), null);
			return new ResponseEntity<>(returnmap, HttpStatus.OK);
		}

		HashMap<String, String> skuMap = skuEvent.getSKUDetails(requestMap.getOrDefault("upcCode", "").toString());
		if (skuMap == null) {
			returnmap.put("status", "error");
			returnmap.put("message", "UPC Code not found.");

			logsServices.addLog(AppConstants.error, AppConstants.SKUALLOCATION, "updateSKUAssetAvailableQuantity",
					returnmap.get("message"), null);
			return new ResponseEntity<>(returnmap, HttpStatus.OK);
		}

		SKUAllocation skuAllocation = skuAllocationRepository
				.findByTagIdAndUpcCode(requestMap.getOrDefault("tagId", "").toString(), skuMap.get("upcCode"));

		if (skuAllocation == null || skuAllocation.getStatus().equalsIgnoreCase("Completed")) {

			returnmap.put("status", "error");
			returnmap.put("message", "No assignment for given tag id and UPC Code");

			logsServices.addLog(AppConstants.error, AppConstants.SKUALLOCATION, "updateSKUAssetAvailableQuantity",
					returnmap.get("message"), null);
			return new ResponseEntity<>(returnmap, HttpStatus.OK);
		} else {

			if (!skuAllocation.isTransaction()) {
				returnmap.put("status", "error");
				returnmap.put("message", "Your can not update quantity for non transaction UPC Code");

				logsServices.addLog(AppConstants.error, AppConstants.SKUALLOCATION, "updateSKUAssetAvailableQuantity",
						returnmap.get("message"), null);
				return new ResponseEntity<>(returnmap, HttpStatus.OK);
			}

			float availableQuantity = skuAllocation.getAvailableQuantity();
			float quantity1 = Float.valueOf(requestMap.getOrDefault("quantity", "0").toString());

			availableQuantity = availableQuantity - quantity1;
			if (availableQuantity <= 0) {
				availableQuantity = 0;
			}
			skuAllocation.setAvailableQuantity(availableQuantity);
			skuAllocationRepository.save(skuAllocation);

			returnmap.put("status", "success");
			returnmap.put("message", "Available Quantity updated successfully");

			logsServices.addLog(AppConstants.event, AppConstants.SKUALLOCATION, "updateSKUAssetAvailableQuantity",
					returnmap.get("message"), null);
			return new ResponseEntity<>(returnmap, HttpStatus.OK);
		}
	}

	@PostMapping(value = "/returnSKUAsset")
	public ResponseEntity<?> returnSKUAsset(HttpServletRequest request, HttpSession httpSession,
			@RequestBody HashMap<String, Object> requestMap) throws InterruptedException, ExecutionException {

		ResponseEntity<?> responseEntity = checkSessionKey(httpSession, Utils.getCookie(request, "JSESSIONID"), "STAR");
		if (responseEntity != null) { 
			logsServices.addLog(AppConstants.error, AppConstants.SKUALLOCATION, "returnSKUAsset",
					AppConstants.sessiontimeout, null);
			return responseEntity; 
		}
		

		Map<String, String> reuestMap = new HashMap<>();
		reuestMap.put("params", new JSONObject(requestMap).toString()); 
		
		logsServices.addLog(AppConstants.event, AppConstants.SKUALLOCATION, "returnSKUAsset",
				"SKU Asset return api call", reuestMap);
		
		
		Map<String, String> returnmap = new HashMap<>();
		if (Utils.isEmptyString(requestMap.getOrDefault("tagId", "").toString())
				|| Utils.isEmptyString(requestMap.getOrDefault("upcCode", "").toString())) {

			returnmap.put("status", "error");
			returnmap.put("message", AppConstants.fieldsmissing);

			logsServices.addLog(AppConstants.error, AppConstants.SKUALLOCATION, "returnSKUAsset",
					AppConstants.fieldsmissing, null);
			return new ResponseEntity<>(returnmap, HttpStatus.OK);
		}

		Tags tags = tagsRepository.findByTagId(requestMap.getOrDefault("tagId", "").toString());

		if (tags == null) {
			returnmap.put("status", "error");
			returnmap.put("message", "Tag id not found.");

			logsServices.addLog(AppConstants.error, AppConstants.SKUALLOCATION, "returnSKUAsset",
					returnmap.get("message"), null);
			return new ResponseEntity<>(returnmap, HttpStatus.OK);
		} else if (tags.getStatus().equalsIgnoreCase("Free")) {
			returnmap.put("status", "error");
			returnmap.put("message", "Tag not assigned");

			logsServices.addLog(AppConstants.error, AppConstants.SKUALLOCATION, "returnSKUAsset",
					returnmap.get("message"), null);
			return new ResponseEntity<>(returnmap, HttpStatus.OK);
		}

		HashMap<String, String> skuMap = skuEvent.getSKUDetails(requestMap.getOrDefault("upcCode", "").toString());
		if (skuMap == null) {
			returnmap.put("status", "error");
			returnmap.put("message", "UPC Code not found.");

			logsServices.addLog(AppConstants.error, AppConstants.SKUALLOCATION, "returnSKUAsset",
					returnmap.get("message"), null);
			return new ResponseEntity<>(returnmap, HttpStatus.OK);
		}

		SKUAllocation skuAllocation = skuAllocationRepository
				.findByTagIdAndUpcCode(requestMap.getOrDefault("tagId", "").toString(), skuMap.get("upcCode"));

		if (skuAllocation == null || skuAllocation.getStatus().equalsIgnoreCase("Completed")) {

			returnmap.put("status", "error");
			returnmap.put("message", "No assignment for given tag id and UPC Code");

			logsServices.addLog(AppConstants.error, AppConstants.SKUALLOCATION, "returnSKUAsset",
					returnmap.get("message"), null);
			return new ResponseEntity<>(returnmap, HttpStatus.OK);
		} else {

			skuAllocation.setStatus("Completed");
			skuAllocationRepository.save(skuAllocation);

			tags.setType("");
            tags.setSubType("");
            tags.setAssignmentId("");
            tags.setAssignmentDate(null);
			tags.setStatus("Free"); 
			tags.setModifiedDate(new Date());

			tagsRepository.save(tags);

			returnmap.put("status", "success");
			returnmap.put("message", "Tag returned successfully.");

			logsServices.addLog(AppConstants.event, AppConstants.SKUALLOCATION, "returnSKUAsset",
					returnmap.get("message"), null);
			return new ResponseEntity<>(returnmap, HttpStatus.OK);
		}
	}

	@PostMapping(value = "/checkSKUAsset")
	public ResponseEntity<?> checkSKUAsset(HttpServletRequest request, HttpSession httpSession,
			@RequestBody HashMap<String, Object> requestMap) throws InterruptedException, ExecutionException {

		ResponseEntity<?> responseEntity = checkSessionKey(httpSession, Utils.getCookie(request, "JSESSIONID"), "STAR");
		if (responseEntity != null) { 
			logsServices.addLog(AppConstants.error, AppConstants.SKUALLOCATION, "checkSKUAsset",
					AppConstants.sessiontimeout, null);
			return responseEntity; 
		}
		

		Map<String, String> reuestMap = new HashMap<>();
		reuestMap.put("params", new JSONObject(requestMap).toString()); 
		
		logsServices.addLog(AppConstants.event, AppConstants.SKUALLOCATION, "checkSKUAsset",
				"SKU Asset check api call", reuestMap);
		
		Map<String, Object> returnmap = new HashMap<>();
		if (Utils.isEmptyString(requestMap.getOrDefault("tagId", "").toString())
				|| Utils.isEmptyString(requestMap.getOrDefault("skuNo", "").toString())) {

			returnmap.put("status", "error");
			returnmap.put("message", AppConstants.fieldsmissing);

			logsServices.addLog(AppConstants.error, AppConstants.SKUALLOCATION, "checkSKUAsset",
					AppConstants.fieldsmissing, null);
			return new ResponseEntity<>(returnmap, HttpStatus.OK);
		}

		Tags tags = tagsRepository.findByTagId(requestMap.getOrDefault("tagId", "").toString());

		if (tags == null) {
			returnmap.put("status", "error");
			returnmap.put("message", "Tag id not found.");

			logsServices.addLog(AppConstants.error, AppConstants.SKUALLOCATION, "checkSKUAsset",
					returnmap.get("message").toString(), null);
			return new ResponseEntity<>(returnmap, HttpStatus.OK);
		}

		HashMap<String, String> skuMap = skuEvent.getSKUDetails(requestMap.getOrDefault("upcCode", "").toString());
		if (skuMap == null) {
			returnmap.put("status", "error");
			returnmap.put("message", "UPC Code not found.");

			logsServices.addLog(AppConstants.error, AppConstants.SKUALLOCATION, "checkSKUAsset",
					returnmap.get("message").toString(), null);
			return new ResponseEntity<>(returnmap, HttpStatus.OK);
		}

		SKUAllocation skuAllocation = skuAllocationRepository
				.findByTagIdAndUpcCode(requestMap.getOrDefault("tagId", "").toString(), skuMap.get("upcCode"));

		if (skuAllocation == null || skuAllocation.getStatus().equalsIgnoreCase("Completed")) {

			returnmap.put("status", "success");
			returnmap.put("assignmentStatus", "Free");
			returnmap.put("message", "No assignment for given tag id and UPC Code");

			logsServices.addLog(AppConstants.event, AppConstants.SKUALLOCATION, "checkSKUAsset",
					returnmap.get("message").toString(), null);
			return new ResponseEntity<>(returnmap, HttpStatus.OK);
		} else {

			returnmap.put("status", "success");
			returnmap.put("assignmentStatus", "Assigned");
			returnmap.put("message", "SKU Allocation details");
			returnmap.put("data", skuAllocation);

			logsServices.addLog(AppConstants.event, AppConstants.SKUALLOCATION, "checkSKUAsset",
					returnmap.get("message").toString(), null);
			return new ResponseEntity<>(returnmap, HttpStatus.OK);
		}
	}

	@GetMapping(value = "/getSKUAvailableQuantity/{tagId}")
	public ResponseEntity<?> getSKUAvailableQuantity(HttpServletRequest request, HttpSession httpSession,
			@PathVariable Optional<String> tagId) throws InterruptedException, ExecutionException {

		ResponseEntity<?> responseEntity = checkSessionKey(httpSession, Utils.getCookie(request, "JSESSIONID"), "SDET");
		if (responseEntity != null) { 
			logsServices.addLog(AppConstants.error, AppConstants.SKUALLOCATION, "getSKUAvailableQuantity",
					AppConstants.sessiontimeout, null);
			return responseEntity; 
		}
		

		JSONObject jsonObject = new JSONObject();
		try {
			jsonObject.put("tagId", tagId.get());
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		Map<String, String> reuestMap = new HashMap<>();
		reuestMap.put("params", jsonObject.toString()); 


		logsServices.addLog(AppConstants.event, AppConstants.SKUALLOCATION, "checkSKUAsset",
				"Get SKU Asset available quantity api call", reuestMap);
		
		
		Map<String, Object> returnmap = new HashMap<>();
		if (!tagId.isPresent()) {

			returnmap.put("status", "error");
			returnmap.put("message", AppConstants.fieldsmissing);

			logsServices.addLog(AppConstants.error, AppConstants.SKUALLOCATION, "getSKUAvailableQuantity",
					AppConstants.fieldsmissing, null);
			return new ResponseEntity<>(returnmap, HttpStatus.OK);
		}

		Tags tags = tagsRepository.findByTagId(tagId.get());

		if (tags == null) {
			returnmap.put("status", "error");
			returnmap.put("message", "Tag id not found.");

			logsServices.addLog(AppConstants.error, AppConstants.SKUALLOCATION, "getSKUAvailableQuantity",
					returnmap.get("message").toString(), null);
			return new ResponseEntity<>(returnmap, HttpStatus.OK);
		}

		SKUAllocation skuAllocation = skuAllocationRepository.findByValidTagId(tagId.get());

		if (skuAllocation == null) {

			returnmap.put("status", "error");
			returnmap.put("message", "No assignment for given tag id");

			logsServices.addLog(AppConstants.event, AppConstants.SKUALLOCATION, "getSKUAvailableQuantity",
					returnmap.get("message").toString(), null);
			return new ResponseEntity<>(returnmap, HttpStatus.OK);
		} else {

 
			HashMap<String, String> skuMap = skuEvent.getSKUDetails(skuAllocation.getUpcCode());

			HashMap<String, Object> dataMap = new HashMap<>();
			dataMap.put("skuAllocation", skuAllocation);
			dataMap.put("skuDetails", skuMap);

			returnmap.put("status", "success");
			returnmap.put("message", "SKU Available Quantity details");
			returnmap.put("data", dataMap);

			logsServices.addLog(AppConstants.event, AppConstants.SKUALLOCATION, "getSKUAvailableQuantity",
					returnmap.get("message").toString(), null);
			return new ResponseEntity<>(returnmap, HttpStatus.OK);
		}
	}

}
