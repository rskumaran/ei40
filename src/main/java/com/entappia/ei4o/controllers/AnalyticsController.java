package com.entappia.ei4o.controllers;


import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ExecutionException;


import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.entappia.ei4o.constants.AppConstants;
import com.entappia.ei4o.models.AgingAnalysis;
import com.entappia.ei4o.models.AgingAnalysis.AgingWorkStations;
import com.entappia.ei4o.models.AgingAnalytics;
import com.entappia.ei4o.models.Analytics;
import com.entappia.ei4o.models.AttendanceAnalysis;
import com.entappia.ei4o.models.ConstraintAnalysis;
import com.entappia.ei4o.models.WorkStationAnalytics;
import com.entappia.ei4o.models.WorkStationChartAnalytics;
import com.entappia.ei4o.models.WorkStationFlowAnalytics;
import com.entappia.ei4o.models.WorkStationInventoryAnalysis;
import com.entappia.ei4o.models.WorkStations;
import com.entappia.ei4o.models.ZoneFootFallAnalysis;
import com.entappia.ei4o.models.ConstraintAnalysis.ConstraintWorkStations;
import com.entappia.ei4o.models.WorkOrderTimeAnalytics;
import com.entappia.ei4o.repository.AnalyticsRepository;
import com.entappia.ei4o.dbmodels.AgingAnalyticsResult;
import com.entappia.ei4o.dbmodels.AttendancePeopleResult;
import com.entappia.ei4o.dbmodels.AttendanceZoneResult;
import com.entappia.ei4o.dbmodels.Location;
import com.entappia.ei4o.dbmodels.WorkOrderTimeAnalyticsResult;
import com.entappia.ei4o.services.LogsServices;
import com.entappia.ei4o.utils.BaseController;
import com.entappia.ei4o.utils.Utils;
import com.entappia.ei4o.repository.WorkStationRepository;
import com.entappia.ei4o.repository.WorkOrderAnalysisRepository;
import com.entappia.ei4o.dbmodels.WorkStationAnalyticsResult;
import com.entappia.ei4o.dbmodels.WorkStationChartAnalyticsResult;
import com.entappia.ei4o.dbmodels.WorkStationInventoryResult;
import com.entappia.ei4o.dbmodels.ZoneFootFallAverageResult;
import com.entappia.ei4o.dbmodels.ZoneFootFallResult;

@RestController
public class AnalyticsController extends BaseController {

	@Autowired
	private LogsServices logsServices;

	@Autowired
	private AnalyticsRepository analyticsRepository;

	@Autowired
	private WorkStationRepository workStationRepository;

	@Autowired
	private WorkOrderAnalysisRepository workOrderAnalysisRepository;

	public static enum profileType {
		PEOPLE, ASSETCHRONO,ASSETNONCHRONO, WORKORDER,VECHICLE
	}

	@RequestMapping(path = { "getAnalytics" }, method = RequestMethod.GET)
	public ResponseEntity<?> getAnalytics(HttpServletRequest request, HttpSession httpSession,
			@RequestBody HashMap<String, Object> requestmap) throws InterruptedException, ExecutionException {
		
		
		HashMap<String, Object> returnmap = new HashMap<>();
		
		ResponseEntity<?> responseEntity = checkSessionKey(httpSession, Utils.getCookie(request, "JSESSIONID"), "CTPE");
		if (responseEntity != null) { 
			logsServices.addLog(AppConstants.error, AppConstants.ANALYTICS, "getAnalytics",
				AppConstants.sessiontimeout, null);
			return responseEntity; 
		}
		
		Map<String, String> requestLogMap = new HashMap<>(); 
		requestLogMap.put("params", new JSONObject(requestmap).toString());
		
		logsServices.addLog(AppConstants.event, AppConstants.ANALYTICS, "getAnalytics",
				"Analytics Get API Details", requestLogMap);

		System.out.print("Inside getAnalytics");
		try {
			if (requestmap.containsKey("type"))
			{
				String analyticsType = (String) requestmap.get("type");
				if(analyticsType.equalsIgnoreCase(profileType.PEOPLE.name().toString())) {
					System.out.print("Inside People profile type");
				}
				else if(analyticsType.equalsIgnoreCase(profileType.ASSETCHRONO.name().toString())) {
					System.out.print("Inside People profile type");
				}
				else if(analyticsType.equalsIgnoreCase(profileType.ASSETNONCHRONO.name().toString())) {
					System.out.print("Inside People profile type");
				}
				else if(analyticsType.equalsIgnoreCase(profileType.WORKORDER.name().toString())) {
					System.out.print("Inside People profile type");
				}
				else if(analyticsType.equalsIgnoreCase(profileType.VECHICLE.name().toString())) {
					System.out.print("Inside People profile type");
				}
				else {
					System.out.print("Profile not found");
				}
				returnmap.put("status", "success");
				returnmap.put("data", "put data here");

				return new ResponseEntity<>(returnmap, HttpStatus.OK);
			}
		}catch(Exception e){
			returnmap.put("status", "error");
			returnmap.put("message", AppConstants.errormsg);

			return new ResponseEntity<>(returnmap, HttpStatus.OK);
		}
		returnmap.put("status", "error");
		returnmap.put("message", AppConstants.errormsg);

		return new ResponseEntity<>(returnmap, HttpStatus.OK);

	}
	@RequestMapping(path = { "peopleDwellTime" }, method = RequestMethod.POST)
	public ResponseEntity<?> getPeopleDwellTime(HttpServletRequest request, HttpSession httpSession,
			@RequestBody HashMap<String, Object> requestmap) throws InterruptedException, ExecutionException {
		HashMap<String, Object> returnmap = new HashMap<>();

		System.out.print("Inside getPeopleDwellTime");
		
		
		ResponseEntity<?> responseEntity = checkSessionKey(httpSession, Utils.getCookie(request, "JSESSIONID"), "PTAL");
		if (responseEntity != null) { 
			logsServices.addLog(AppConstants.error, AppConstants.ANALYTICS, "peopleDwellTime",
				AppConstants.sessiontimeout, null);
			return responseEntity; 
		}
		
		Map<String, String> requestLogMap = new HashMap<>(); 
		requestLogMap.put("params", new JSONObject(requestmap).toString());
		
		logsServices.addLog(AppConstants.event, AppConstants.ANALYTICS, "peopleDwellTime",
				"People DwellTime Get API Details", requestLogMap);
		
		try {
			//validate start
			if (requestmap.containsKey("userId") && requestmap.containsKey("startDate") && requestmap.containsKey("endDate"))
			{
				//validation
				if(Utils.isEmptyString(requestmap.getOrDefault("userId", "").toString())
						|| Utils.isEmptyString(requestmap.getOrDefault("startDate", "").toString())
						|| Utils.isEmptyString(requestmap.getOrDefault("endDate", "").toString())) {
					//any one are some of the request parameters is empty
					returnmap.put("status", "error");
					returnmap.put("message", AppConstants.fieldsmissing);

					logsServices.addLog(AppConstants.error, AppConstants.ANALYTICS, "peopleDwellTime",
							AppConstants.fieldsmissing, null);
					return new ResponseEntity<>(returnmap, HttpStatus.OK);
				}
				else {

					//validation
					if (!Utils.validateDate(requestmap.getOrDefault("startDate", "").toString())) {
						returnmap.put("status", "error");
						returnmap.put("message", AppConstants.errorDatemsg);

						logsServices.addLog(AppConstants.error, AppConstants.ANALYTICS, "peopleDwellTime",
								AppConstants.errorDatemsg, null);
						return new ResponseEntity<>(returnmap, HttpStatus.OK);
					}
					if (!Utils.validateDate(requestmap.getOrDefault("endDate", "").toString())) {
						returnmap.put("status", "error");
						returnmap.put("message", AppConstants.errorDatemsg);

						logsServices.addLog(AppConstants.error, AppConstants.ANALYTICS, "peopleDwellTime",
								AppConstants.errorDatemsg, null);
						return new ResponseEntity<>(returnmap, HttpStatus.OK);
					}
					//Fetch variables after validation
					String sUserId = (String) requestmap.get("userId");
					Date dStartDate = new SimpleDateFormat(AppConstants.RequestDateFormat)
							.parse(requestmap.getOrDefault("startDate", "").toString());

					Date dEndDate = new SimpleDateFormat(AppConstants.RequestDateFormat)
							.parse(requestmap.getOrDefault("endDate", "").toString());

					if(dStartDate.compareTo(dEndDate) > 0 ) {
						returnmap.put("status", "error");
						returnmap.put("message", AppConstants.errorDatemismatch);

						logsServices.addLog(AppConstants.error, AppConstants.ANALYTICS, "peopleDwellTime",
								AppConstants.errorDatemismatch, null);
						return new ResponseEntity<>(returnmap, HttpStatus.OK);
					}

					//System.out.println("UserId : " + sUserId);
					//System.out.println("Start Date : " + dStartDate);
					//System.out.println("End date : " + dEndDate);


					//Fetch all datas according to the format required
					Analytics peopleAnalytics = getPeopleDwellTime(sUserId, dStartDate, dEndDate);

					if(peopleAnalytics == null) {

						returnmap.put("status", "error");
						returnmap.put("message", AppConstants.errorNoRecordsFound);
						logsServices.addLog(AppConstants.error, AppConstants.ANALYTICS, "peopleDwellTime",
								AppConstants.errorNoRecordsFound, null);
						return new ResponseEntity<>(returnmap, HttpStatus.OK);


					}

					returnmap.put("status", "success");
					returnmap.put("data", "fetched records");

					return new ResponseEntity<>(peopleAnalytics, HttpStatus.OK);
				}
			}
			else
			{
				//key does not exist
			}
		}catch(Exception e){
			returnmap.put("status", "error");
			returnmap.put("message", AppConstants.errormsg);

			return new ResponseEntity<>(returnmap, HttpStatus.OK);
		}
		returnmap.put("status", "error");
		returnmap.put("message", AppConstants.errormsg);

		return new ResponseEntity<>(returnmap, HttpStatus.OK);

	}
	@RequestMapping(path = { "visitorDwellTime" }, method = RequestMethod.POST)
	public ResponseEntity<?> getVisitorDwellTime(HttpServletRequest request, HttpSession httpSession,
			@RequestBody HashMap<String, Object> requestmap) throws InterruptedException, ExecutionException {
		HashMap<String, Object> returnmap = new HashMap<>();

		System.out.print("Inside getVisitorDwellTime");

		try {
			
			ResponseEntity<?> responseEntity = checkSessionKey(httpSession, Utils.getCookie(request, "JSESSIONID"), "PTAL");
			if (responseEntity != null) { 
				logsServices.addLog(AppConstants.error, AppConstants.ANALYTICS, "visitorDwellTime",
					AppConstants.sessiontimeout, null);
				return responseEntity; 
			}
			
			Map<String, String> requestLogMap = new HashMap<>(); 
			requestLogMap.put("params", new JSONObject(requestmap).toString());
			
			logsServices.addLog(AppConstants.event, AppConstants.ANALYTICS, "visitorDwellTime",
					"Visitor DwellTime Get API Details", requestLogMap);
			
			//validate start
			if (requestmap.containsKey("userId") && requestmap.containsKey("startDate") && requestmap.containsKey("endDate"))
			{
				//validation
				if(Utils.isEmptyString(requestmap.getOrDefault("userId", "").toString())
						|| Utils.isEmptyString(requestmap.getOrDefault("startDate", "").toString())
						|| Utils.isEmptyString(requestmap.getOrDefault("endDate", "").toString())) {
					//any one are some of the request parameters is empty
					returnmap.put("status", "error");
					returnmap.put("message", AppConstants.fieldsmissing);

					logsServices.addLog(AppConstants.error, AppConstants.ANALYTICS, "visitorDwellTime",
							AppConstants.fieldsmissing, null);
					return new ResponseEntity<>(returnmap, HttpStatus.OK);
				}
				else {

					//validation
					if (!Utils.validateDate(requestmap.getOrDefault("startDate", "").toString())) {
						returnmap.put("status", "error");
						returnmap.put("message", AppConstants.errorDatemsg);

						logsServices.addLog(AppConstants.error, AppConstants.ANALYTICS, "visitorDwellTime",
								AppConstants.errorDatemsg, null);
						return new ResponseEntity<>(returnmap, HttpStatus.OK);
					}
					if (!Utils.validateDate(requestmap.getOrDefault("endDate", "").toString())) {
						returnmap.put("status", "error");
						returnmap.put("message", AppConstants.errorDatemsg);

						logsServices.addLog(AppConstants.error, AppConstants.ANALYTICS, "visitorDwellTime",
								AppConstants.errorDatemsg, null);
						return new ResponseEntity<>(returnmap, HttpStatus.OK);
					}
					//Fetch variables after validation
					String sUserId = (String) requestmap.get("userId");
					Date dStartDate = new SimpleDateFormat(AppConstants.RequestDateFormat)
							.parse(requestmap.getOrDefault("startDate", "").toString());

					Date dEndDate = new SimpleDateFormat(AppConstants.RequestDateFormat)
							.parse(requestmap.getOrDefault("endDate", "").toString());

					if(dStartDate.compareTo(dEndDate) > 0 ) {
						returnmap.put("status", "error");
						returnmap.put("message", AppConstants.errorDatemismatch);

						logsServices.addLog(AppConstants.error, AppConstants.ANALYTICS, "visitorDwellTime",
								AppConstants.errorDatemismatch, null);
						return new ResponseEntity<>(returnmap, HttpStatus.OK);
					}

					//System.out.println("UserId : " + sUserId);
					//System.out.println("Start Date : " + dStartDate);
					//System.out.println("End date : " + dEndDate);

					//Fetch all datas according to the format required
					Analytics peopleAnalytics = getVisitorDwellTime(sUserId, dStartDate, dEndDate);

					if(peopleAnalytics == null) {

						returnmap.put("status", "error");
						returnmap.put("message", AppConstants.errorNoRecordsFound);
						logsServices.addLog(AppConstants.error, AppConstants.ANALYTICS, "visitorDwellTime",
								AppConstants.errorNoRecordsFound, null);
						return new ResponseEntity<>(returnmap, HttpStatus.OK);


					}

					returnmap.put("status", "success");
					returnmap.put("data", "fetched records");

					return new ResponseEntity<>(peopleAnalytics, HttpStatus.OK);
				}
			}
			else
			{
				//key does not exist
			}
		}catch(Exception e){
			returnmap.put("status", "error");
			returnmap.put("message", AppConstants.errormsg);

			return new ResponseEntity<>(returnmap, HttpStatus.OK);
		}
		returnmap.put("status", "error");
		returnmap.put("message", AppConstants.errormsg);

		return new ResponseEntity<>(returnmap, HttpStatus.OK);

	}
	@RequestMapping(path = { "assetDwellTime" }, method = RequestMethod.POST)
	public ResponseEntity<?> getAssetDwellTime(HttpServletRequest request, HttpSession httpSession,
			@RequestBody HashMap<String, Object> requestmap) throws InterruptedException, ExecutionException {
		HashMap<String, Object> returnmap = new HashMap<>();

		System.out.print("Inside getAssetDwellTime");

		try {
			
			ResponseEntity<?> responseEntity = checkSessionKey(httpSession, Utils.getCookie(request, "JSESSIONID"), "ASTA");
			if (responseEntity != null) { 
				logsServices.addLog(AppConstants.error, AppConstants.ANALYTICS, "assetDwellTime",
					AppConstants.sessiontimeout, null);
				return responseEntity; 
			}
			
			Map<String, String> requestLogMap = new HashMap<>(); 
			requestLogMap.put("params", new JSONObject(requestmap).toString());
			
			logsServices.addLog(AppConstants.event, AppConstants.ANALYTICS, "assetDwellTime",
					"Asset DwellTime Get API Details", requestLogMap);
			
			//validate start
			if (requestmap.containsKey("assetId") && requestmap.containsKey("startDate") && requestmap.containsKey("endDate"))
			{
				//validation
				if(Utils.isEmptyString(requestmap.getOrDefault("assetId", "").toString())
						|| Utils.isEmptyString(requestmap.getOrDefault("startDate", "").toString())
						|| Utils.isEmptyString(requestmap.getOrDefault("endDate", "").toString())) {
					//any one are some of the request parameters is empty
					returnmap.put("status", "error");
					returnmap.put("message", AppConstants.fieldsmissing);

					logsServices.addLog(AppConstants.error, AppConstants.ANALYTICS, "AssetDwellTime",
							AppConstants.fieldsmissing, null);
					return new ResponseEntity<>(returnmap, HttpStatus.OK);
				}
				else {

					//validation
					if (!Utils.validateDate(requestmap.getOrDefault("startDate", "").toString())) {
						returnmap.put("status", "error");
						returnmap.put("message", AppConstants.errorDatemsg);

						logsServices.addLog(AppConstants.error, AppConstants.ANALYTICS, "AssetDwellTime",
								AppConstants.errorDatemsg, null);
						return new ResponseEntity<>(returnmap, HttpStatus.OK);
					}
					if (!Utils.validateDate(requestmap.getOrDefault("endDate", "").toString())) {
						returnmap.put("status", "error");
						returnmap.put("message", AppConstants.errorDatemsg);

						logsServices.addLog(AppConstants.error, AppConstants.ANALYTICS, "AssetDwellTime",
								AppConstants.errorDatemsg, null);
						return new ResponseEntity<>(returnmap, HttpStatus.OK);
					}
					//Fetch variables after validation
					String sUserId = (String) requestmap.get("assetId");
					Date dStartDate = new SimpleDateFormat(AppConstants.RequestDateFormat)
							.parse(requestmap.getOrDefault("startDate", "").toString());

					Date dEndDate = new SimpleDateFormat(AppConstants.RequestDateFormat)
							.parse(requestmap.getOrDefault("endDate", "").toString());

					if(dStartDate.compareTo(dEndDate) > 0 ) {
						returnmap.put("status", "error");
						returnmap.put("message", AppConstants.errorDatemismatch);

						logsServices.addLog(AppConstants.error, AppConstants.ANALYTICS, "AssetDwellTime",
								AppConstants.errorDatemismatch, null);
						return new ResponseEntity<>(returnmap, HttpStatus.OK);
					}

					//validation for asset types
					/*if(Utils.isEmptyString(requestmap.getOrDefault("assetProfile", "").toString())
							|| Utils.isEmptyString(requestmap.getOrDefault("assetType", "").toString())) {
						//any one are some of the request parameters is empty
						returnmap.put("status", "error");
						returnmap.put("message", AppConstants.fieldsmissing);

						logsServices.addLog(AppConstants.error, AppConstants.ANALYTICS, "AssetDwellTime",
								AppConstants.fieldsmissing, null);
						return new ResponseEntity<>(returnmap, HttpStatus.OK);
					}*/
					if(Utils.isEmptyString(requestmap.getOrDefault("assetProfile", "").toString())) {
						//any one are some of the request parameters is empty
						returnmap.put("status", "error");
						returnmap.put("message", AppConstants.fieldsmissing);

						logsServices.addLog(AppConstants.error, AppConstants.ANALYTICS, "AssetDwellTime",
								AppConstants.fieldsmissing, null);
						return new ResponseEntity<>(returnmap, HttpStatus.OK);
					}

					String assetProfile = (String) requestmap.get("assetProfile");
					//String assetType = (String) requestmap.get("assetType");
					String assetType = requestmap.getOrDefault("assetType", "").toString();




					//System.out.println("UserId : " + sUserId);
					//System.out.println("Start Date : " + dStartDate);
					//System.out.println("End date : " + dEndDate);


					//Fetch all datas according to the format required
					Analytics peopleAnalytics = getAssetDwellTime(sUserId, dStartDate, dEndDate, assetProfile, assetType);

					if(peopleAnalytics == null) {

						returnmap.put("status", "error");
						returnmap.put("message", AppConstants.errorNoRecordsFound);
						logsServices.addLog(AppConstants.error, AppConstants.ANALYTICS, "AssetDwellTime",
								AppConstants.errorNoRecordsFound, null);
						return new ResponseEntity<>(returnmap, HttpStatus.OK);


					}

					returnmap.put("status", "success");
					returnmap.put("data", "fetched records");

					return new ResponseEntity<>(peopleAnalytics, HttpStatus.OK);
				}
			}
			else
			{
				//key does not exist
			}
		}catch(Exception e){
			returnmap.put("status", "error");
			returnmap.put("message", AppConstants.errormsg);

			return new ResponseEntity<>(returnmap, HttpStatus.OK);
		}
		returnmap.put("status", "error");
		returnmap.put("message", AppConstants.errormsg);

		return new ResponseEntity<>(returnmap, HttpStatus.OK);

	}
	private Analytics getVisitorDwellTime(String userId, Date startDate, Date endDate) {
		return getDwellTime(userId, startDate, endDate, "Visitor" );
	}
	private Analytics getPeopleDwellTime(String userId, Date startDate, Date endDate) {
		return getDwellTime(userId, startDate, endDate, "Staff" );
	}
	private Analytics getAssetDwellTime(String userId, Date startDate, Date endDate,String assetProfile,String assetType) {
		if(assetProfile.equalsIgnoreCase("Chrono")) {
			return getChronoAssetDwellTime(userId, startDate, endDate, "Asset Chrono", assetType );
		}
		else if(assetProfile.equalsIgnoreCase("NonChrono")) {
			return getNonChronoAssetDwellTime(userId, startDate, endDate, "Asset NonChrono", assetType );
		}
		return null;
	}
	@SuppressWarnings("unchecked")
	private Analytics getChronoAssetDwellTime(String assetId, Date startDate, Date endDate, String tableName, String assetType) {

		Analytics peopleAnalytics = new Analytics();
		SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MMM-dd");

		String analyticsTimeDuration = formatter.format(startDate) + " to " + formatter.format(endDate);
		String chronoTableName = "asset_chrono";
		peopleAnalytics.setDate(analyticsTimeDuration);
		//String Date
		List<Date> listOfDates = getDaysBetweenDates(startDate,endDate);
		for(int i=0;i<listOfDates.size();i++)
		{
			//System.out.println("In Between Dates : " + listOfDates.get(i) );
			////System.out.println("In Between Table Names : " + GetTableName(listOfDates.get(i)) );

			List<String> tableNames = (List<String>)analyticsRepository.SelectTableNamesLike(GetTableName(listOfDates.get(i), tableName));
			if(tableNames != null)
			{
				for(int j=0;j<tableNames.size();j++)
				{
					//System.out.println("In Between Table Names : " + tableNames.get(j));
					List<Location> result = null;
					if(assetId.equalsIgnoreCase("All")) { 
						result = (List<Location>) analyticsRepository.SelectAllAssets(tableNames.get(j), chronoTableName, assetType);
					}
					else {
						result = analyticsRepository.SelectAllAssetsByAssetId(tableNames.get(j), assetId, chronoTableName, assetType);
					}
					if(result != null) {
						for(int k=0;k<result.size();k++)
						{
							Location fetchedLocation = result.get(k); 

							if(fetchedLocation != null)
							{

								////System.out.println("tableName : " +  fetchedLocation.getId());

								String staffid = fetchedLocation.getId();

								int  time = fetchedLocation.getDuration();  
								int grid =  fetchedLocation.getGrid();
								String zone = fetchedLocation.getZone(); 
								String zoneName = fetchedLocation.getZoneName();
								String gridPos = fetchedLocation.getGridPos();


								peopleAnalytics.setDwellTime(staffid, time, zone, zoneName);

							}
						}
					}
				}
			}
		}
		if(peopleAnalytics.IsRecordAvailable())
			return peopleAnalytics;
		else
			return null;
	}
	@SuppressWarnings("unchecked")
	private Analytics getNonChronoAssetDwellTime(String assetId, Date startDate, Date endDate, String tableName, String assetType) {

		Analytics peopleAnalytics = new Analytics();

		SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MMM-dd");

		String analyticsTimeDuration = formatter.format(startDate) + " to " + formatter.format(endDate);
		String nonChronoTableName = "asset_non_chrono";
		peopleAnalytics.setDate(analyticsTimeDuration);
		//String Date
		List<Date> listOfDates = getDaysBetweenDates(startDate,endDate);
		for(int i=0;i<listOfDates.size();i++)
		{
			//System.out.println("In Between Dates : " + listOfDates.get(i) );
			////System.out.println("In Between Table Names : " + GetTableName(listOfDates.get(i)) );

			List<String> tableNames = (List<String>)analyticsRepository.SelectTableNamesLike(GetTableName(listOfDates.get(i), tableName));
			if(tableNames != null)
			{
				for(int j=0;j<tableNames.size();j++)
				{
					//System.out.println("In Between Table Names : " + tableNames.get(j));
					List<Location> result = null;
					if(assetId.equalsIgnoreCase("All")) { 
						result = (List<Location>) analyticsRepository.SelectAllAssets(tableNames.get(j), nonChronoTableName, assetType);
					}
					else {
						result = analyticsRepository.SelectAllAssetsByAssetId(tableNames.get(j), assetId, nonChronoTableName, assetType);
					}
					if(result != null) {
						for(int k=0;k<result.size();k++)
						{
							Location fetchedLocation = result.get(k); 

							if(fetchedLocation != null)
							{

								////System.out.println("tableName : " +  fetchedLocation.getId());

								String staffid = fetchedLocation.getId();

								int  time = fetchedLocation.getDuration();  
								int grid =  fetchedLocation.getGrid();
								String zone = fetchedLocation.getZone(); 
								String zoneName = fetchedLocation.getZoneName();
								String gridPos = fetchedLocation.getGridPos();


								peopleAnalytics.setDwellTime(staffid, time, zone, zoneName);

							}
						}
					}
				}
			}
		}
		if(peopleAnalytics.IsRecordAvailable())
			return peopleAnalytics;
		else
			return null;
	}
	/*private Analytics getNonChronoAssetDwellTime(String userId, Date startDate, Date endDate) {
		return getDwellTime(userId, startDate, endDate, "Asset Nonchrono" );
	}*/
	@SuppressWarnings("unchecked")
	private Analytics getDwellTime(String userId, Date startDate, Date endDate, String tableName) {

		Analytics peopleAnalytics = new Analytics();

		SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MMM-dd");

		String analyticsTimeDuration = formatter.format(startDate) + " to " + formatter.format(endDate);

		peopleAnalytics.setDate(analyticsTimeDuration);
		//String Date
		List<Date> listOfDates = getDaysBetweenDates(startDate,endDate);
		for(int i=0;i<listOfDates.size();i++)
		{
			//System.out.println("In Between Dates : " + listOfDates.get(i) );
			////System.out.println("In Between Table Names : " + GetTableName(listOfDates.get(i)) );

			List<String> tableNames = (List<String>)analyticsRepository.SelectTableNamesLike(GetTableName(listOfDates.get(i), tableName));
			if(tableNames != null)
			{
				for(int j=0;j<tableNames.size();j++)
				{
					//System.out.println("In Between Table Names : " + tableNames.get(j));
					List<Location> result = null;
					if(userId.equalsIgnoreCase("All")) { 
						result = (List<Location>) analyticsRepository.SelectAll(tableNames.get(j));
					}
					else {
						result = analyticsRepository.SelectByUserId(tableNames.get(j), userId);
					}
					if(result != null) {
						for(int k=0;k<result.size();k++)
						{
							Location fetchedLocation = result.get(k); 

							if(fetchedLocation != null)
							{

								////System.out.println("tableName : " +  fetchedLocation.getId());

								String staffid = fetchedLocation.getId();

								int  time = fetchedLocation.getDuration();  
								int grid =  fetchedLocation.getGrid();
								String zone = fetchedLocation.getZone(); 
								String zoneName = fetchedLocation.getZoneName();
								String gridPos = fetchedLocation.getGridPos();


								peopleAnalytics.setDwellTime(staffid, time, zone, zoneName);

							}
						}
					}
				}
			}
		}
		if(peopleAnalytics.IsRecordAvailable())
			return peopleAnalytics;
		else
			return null;
	}
	//GetTableName
	//converts string according to the table name rules
	//need to handle this issue if no shift is found 
	private String GetTableName(Date createdDate, String tableName){
		String currentTableName = "";
		String profileName = tableName + "_Location";
		Date currentDate = createdDate;

		//Table name hours and minutes can be used to find out in case of issues
		//For only date is used we can change it later
		//SimpleDateFormat formatter = new SimpleDateFormat("ddMMyyyy HH:mm:ss");
		SimpleDateFormat formatter = new SimpleDateFormat("ddMMyyyy");
		String scurrentDate = formatter.format(currentDate);

		currentTableName = profileName + "_" + scurrentDate + "_";

		return currentTableName;

	}
	//GetTableName
	//converts string according to the table name rules
	//need to handle this issue if no shift is found 
	private String GetTableName(Date createdDate, String tableName, String shiftName){
		String currentTableName = "";
		String profileName = tableName + "_Location";
		Date currentDate = createdDate;

		//Table name hours and minutes can be used to find out in case of issues
		//For only date is used we can change it later
		//SimpleDateFormat formatter = new SimpleDateFormat("ddMMyyyy HH:mm:ss");
		SimpleDateFormat formatter = new SimpleDateFormat("ddMMyyyy");
		String scurrentDate = formatter.format(currentDate);

		currentTableName = profileName + "_" + scurrentDate + "_" + shiftName;

		return currentTableName;

	}
	public static List<Date> getDaysBetweenDates(Date startDate, Date endDate){
		List<Date> dates = new ArrayList<Date>();
		Calendar startDateCalendar = new GregorianCalendar();
		startDateCalendar.setTime(startDate);


		//This is done to 
		Calendar endDatecalendar = new GregorianCalendar();
		endDatecalendar.setTime(endDate);
		endDatecalendar.add(Calendar.DATE, 1);
		endDate = endDatecalendar.getTime();

		while (startDateCalendar.getTime().before(endDate)){
			Date result = startDateCalendar.getTime();
			dates.add(result);
			startDateCalendar.add(Calendar.DATE, 1);
		}
		return dates;
	}
	public static int getNumberDaysBetweenDates(Date startDate, Date endDate){
		List<Date> dates = new ArrayList<Date>();
		Calendar startDateCalendar = new GregorianCalendar();
		startDateCalendar.setTime(startDate);


		//This is done to 
		Calendar endDatecalendar = new GregorianCalendar();
		endDatecalendar.setTime(endDate);
		endDatecalendar.add(Calendar.DATE, 1);
		endDate = endDatecalendar.getTime();

		while (startDateCalendar.getTime().before(endDate)){
			Date result = startDateCalendar.getTime();
			dates.add(result);
			startDateCalendar.add(Calendar.DATE, 1);
		}
		return dates.size();
	}

	private String GetTableName(String formattedDateString, String tableName){
		String currentTableName = "";
		String profileName = tableName + "_Location";
		String scurrentDate = formattedDateString;

		currentTableName = profileName + "_" + scurrentDate + "_";

		return currentTableName;

	}
	@RequestMapping(path = { "WorkStationFlowAnalytics" }, method = RequestMethod.POST)
	public ResponseEntity<?> getWorkStationFlowAnalytics(HttpServletRequest request, HttpSession httpSession,
			@RequestBody HashMap<String, Object> requestmap) throws InterruptedException, ExecutionException {
		HashMap<String, Object> returnmap = new HashMap<>();

		System.out.print("Inside getWorkStationFlowAnalytics");

		try {
			
			ResponseEntity<?> responseEntity = checkSessionKey(httpSession, Utils.getCookie(request, "JSESSIONID"), "WOFA");
			if (responseEntity != null) { 
				logsServices.addLog(AppConstants.error, AppConstants.ANALYTICS, "WorkStationFlowAnalytics",
					AppConstants.sessiontimeout, null);
				return responseEntity; 
			}
			
			Map<String, String> requestLogMap = new HashMap<>(); 
			requestLogMap.put("params", new JSONObject(requestmap).toString());
			
			logsServices.addLog(AppConstants.event, AppConstants.ANALYTICS, "WorkStationFlowAnalytics",
					"WorkStation Flow Analytics Get API Details", requestLogMap);
			
			//validate start
			if (requestmap.containsKey("WorkStationId") && requestmap.containsKey("startDate") && requestmap.containsKey("endDate"))
			{
				//validation
				if(Utils.isEmptyString(requestmap.getOrDefault("WorkStationId", "").toString())
						|| Utils.isEmptyString(requestmap.getOrDefault("startDate", "").toString())
						|| Utils.isEmptyString(requestmap.getOrDefault("endDate", "").toString())) {
					//any one are some of the request parameters is empty
					returnmap.put("status", "error");
					returnmap.put("message", AppConstants.fieldsmissing);

					logsServices.addLog(AppConstants.error, AppConstants.ANALYTICS, "WorkStationFlowAnalytics",
							AppConstants.fieldsmissing, null);
					return new ResponseEntity<>(returnmap, HttpStatus.OK);
				}
				else {

					//validation
					if (!Utils.validateDate(requestmap.getOrDefault("startDate", "").toString())) {
						returnmap.put("status", "error");
						returnmap.put("message", AppConstants.errorDatemsg);

						logsServices.addLog(AppConstants.error, AppConstants.ANALYTICS, "WorkStationFlowAnalytics",
								AppConstants.errorDatemsg, null);
						return new ResponseEntity<>(returnmap, HttpStatus.OK);
					}
					if (!Utils.validateDate(requestmap.getOrDefault("endDate", "").toString())) {
						returnmap.put("status", "error");
						returnmap.put("message", AppConstants.errorDatemsg);

						logsServices.addLog(AppConstants.error, AppConstants.ANALYTICS, "WorkStationFlowAnalytics",
								AppConstants.errorDatemsg, null);
						return new ResponseEntity<>(returnmap, HttpStatus.OK);
					}
					//Fetch variables after validation
					String sWorkStationId = (String) requestmap.get("WorkStationId");
					Date dStartDate = new SimpleDateFormat(AppConstants.RequestDateFormat)
							.parse(requestmap.getOrDefault("startDate", "").toString());

					Date dEndDate = new SimpleDateFormat(AppConstants.RequestDateFormat)
							.parse(requestmap.getOrDefault("endDate", "").toString());

					if(dStartDate.compareTo(dEndDate) > 0 ) {
						returnmap.put("status", "error");
						returnmap.put("message", AppConstants.errorDatemismatch);

						logsServices.addLog(AppConstants.error, AppConstants.ANALYTICS, "WorkStationFlowAnalytics",
								AppConstants.errorDatemismatch, null);
						return new ResponseEntity<>(returnmap, HttpStatus.OK);
					}

					//System.out.println("WorkStationId : " + sWorkStationId);
					//System.out.println("Start Date : " + dStartDate);
					//System.out.println("End date : " + dEndDate);


					//Fetch all datas according to the format required
					WorkStationFlowAnalytics workStationFlowAnalytics = getWorkStationFlowAnalytics(sWorkStationId, dStartDate, dEndDate);

					if(workStationFlowAnalytics == null) {

						returnmap.put("status", "error");
						returnmap.put("message", AppConstants.errorNoRecordsFound);
						logsServices.addLog(AppConstants.error, AppConstants.ANALYTICS, "WorkStationFlowAnalytics",
								AppConstants.errorNoRecordsFound, null);
						return new ResponseEntity<>(returnmap, HttpStatus.OK);


					}

					returnmap.put("status", "success");
					returnmap.put("data", "fetched records");

					return new ResponseEntity<>(workStationFlowAnalytics, HttpStatus.OK);
				}
			}
			else
			{
				//key does not exist
			}
		}catch(Exception e){
			returnmap.put("status", "error");
			returnmap.put("message", AppConstants.errormsg);

			return new ResponseEntity<>(returnmap, HttpStatus.OK);
		}
		returnmap.put("status", "error");
		returnmap.put("message", AppConstants.errormsg);

		return new ResponseEntity<>(returnmap, HttpStatus.OK);

	}
	private WorkStationFlowAnalytics getWorkStationFlowAnalytics(String workStationId, Date startDate, Date endDate) {
		return getWorkStationFlowAnalytics(workStationId, startDate, endDate, "Materials" );
	}

	@SuppressWarnings({ "unchecked"})
	private WorkStationFlowAnalytics getWorkStationFlowAnalytics(String workStationId, Date startDate, Date endDate, String tableName) {

		WorkStationFlowAnalytics workStationFlowAnalytics = new WorkStationFlowAnalytics();


		SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MMM-dd");

		String analyticsTimeDuration = formatter.format(startDate) + " to " + formatter.format(endDate);

		workStationFlowAnalytics.setDate(analyticsTimeDuration);
		//String Date
		List<Date> listOfDates = getDaysBetweenDates(startDate,endDate);
		for(int i=0;i<listOfDates.size();i++)
		{
			//System.out.println("In Between Dates : " + listOfDates.get(i) );
			////System.out.println("In Between Table Names : " + GetTableName(listOfDates.get(i)) );
			List<String> tableNames = (List<String>)analyticsRepository.SelectTableNamesLike(GetTableName(listOfDates.get(i), tableName));
			if(tableNames != null)
			{
				//Here find incoming,outgoing, waiting, and storage area items for one day
				//and add them or send all days and fetch bulk
				//we can use procedure or simple query whichever preferred
				//for all workstation or one workstation

				// use this loop when we want to get records for particular shift
				/*for(int j=0;j<tableNames.size();j++)
				{
					//System.out.println("In Between Table Names : " + tableNames.get(j));
				}*///This ends down this is to check table names

				List<WorkStationAnalyticsResult> resultIncoming = null;
				List<WorkStationAnalyticsResult> resultOutgoing = null;
				List<WorkStationAnalyticsResult> resultWaiting = null;


				if(workStationId.equalsIgnoreCase("All")) { 
					resultIncoming = (List<WorkStationAnalyticsResult>) analyticsRepository.SelectAllWorkstationIncomingFlowAnalytics(tableNames);
					resultOutgoing = (List<WorkStationAnalyticsResult>) analyticsRepository.SelectAllWorkstationOutgoingFlowAnalytics(tableNames);
					resultWaiting = (List<WorkStationAnalyticsResult>) analyticsRepository.SelectAllWorkstationWaitingFlowAnalytics(tableNames);
				}
				else {
					resultIncoming = (List<WorkStationAnalyticsResult>) analyticsRepository.SelectWorkstationIncomingFlowAnalyticsByWorkStationId(tableNames, workStationId);
					resultOutgoing = (List<WorkStationAnalyticsResult>) analyticsRepository.SelectWorkstationOutgoingFlowAnalyticsByWorkStationId(tableNames, workStationId);
					resultWaiting = (List<WorkStationAnalyticsResult>) analyticsRepository.SelectWorkstationWaitingFlowAnalyticsByWorkStationId(tableNames, workStationId);
				}





				if(resultIncoming != null) {
					for(int k=0;k<resultIncoming.size();k++)
					{
						WorkStationAnalyticsResult fetchedWorkStationAnalyticsResult = resultIncoming.get(k); 

						if(fetchedWorkStationAnalyticsResult != null)
						{

							workStationFlowAnalytics.setWorkOrderCount(fetchedWorkStationAnalyticsResult.getStationId(),
									fetchedWorkStationAnalyticsResult.getIncoming(), fetchedWorkStationAnalyticsResult.getOutgoing(),
									fetchedWorkStationAnalyticsResult.getWaiting(), formatter.format(listOfDates.get(i)),
									fetchedWorkStationAnalyticsResult.getStationName());  
						}
					}
				}
				if(resultOutgoing != null) {
					for(int k=0;k<resultOutgoing.size();k++)
					{
						WorkStationAnalyticsResult fetchedWorkStationAnalyticsResult = resultOutgoing.get(k); 

						if(fetchedWorkStationAnalyticsResult != null)
						{

							workStationFlowAnalytics.setWorkOrderCount(fetchedWorkStationAnalyticsResult.getStationId(),
									fetchedWorkStationAnalyticsResult.getIncoming(), fetchedWorkStationAnalyticsResult.getOutgoing(),
									fetchedWorkStationAnalyticsResult.getWaiting(), formatter.format(listOfDates.get(i)), fetchedWorkStationAnalyticsResult.getStationName());
						}
					}
				}
				if(resultWaiting != null) {
					for(int k=0;k<resultWaiting.size();k++)
					{
						WorkStationAnalyticsResult fetchedWorkStationAnalyticsResult = resultWaiting.get(k); 

						if(fetchedWorkStationAnalyticsResult != null)
						{

							workStationFlowAnalytics.setWorkOrderCount(fetchedWorkStationAnalyticsResult.getStationId(),
									fetchedWorkStationAnalyticsResult.getIncoming(), fetchedWorkStationAnalyticsResult.getOutgoing(),
									fetchedWorkStationAnalyticsResult.getWaiting(), formatter.format(listOfDates.get(i)), fetchedWorkStationAnalyticsResult.getStationName());
						}
					}
				}
				//}shift for loop ends here
			}
		}
		if(workStationFlowAnalytics.IsRecordAvailable())
			return workStationFlowAnalytics;
		else
			return null;

	}

	@RequestMapping(path = { "WorkStationConstraintAnalytics" }, method = RequestMethod.POST)
	public ResponseEntity<?> getWorkStationConstraintAnalytics(HttpServletRequest request, HttpSession httpSession,
			@RequestBody HashMap<String, Object> requestmap) throws InterruptedException, ExecutionException {
		HashMap<String, Object> returnmap = new HashMap<>();

		System.out.print("Inside WorkStationConstraintAnalytics");

		try {
			
			ResponseEntity<?> responseEntity = checkSessionKey(httpSession, Utils.getCookie(request, "JSESSIONID"), "WCAL");
			if (responseEntity != null) { 
				logsServices.addLog(AppConstants.error, AppConstants.ANALYTICS, "WorkStationConstraintAnalytics",
					AppConstants.sessiontimeout, null);
				return responseEntity; 
			}
			
			Map<String, String> requestLogMap = new HashMap<>(); 
			requestLogMap.put("params", new JSONObject(requestmap).toString());
			
			logsServices.addLog(AppConstants.event, AppConstants.ANALYTICS, "WorkStationConstraintAnalytics",
					"WorkStation Constraint Analytics Get API Details", requestLogMap);
			
			//validate start
			if (requestmap.containsKey("startDate") && requestmap.containsKey("endDate"))
			{
				//validation
				if( Utils.isEmptyString(requestmap.getOrDefault("startDate", "").toString())
						|| Utils.isEmptyString(requestmap.getOrDefault("endDate", "").toString())) {
					//any one are some of the request parameters is empty
					returnmap.put("status", "error");
					returnmap.put("message", AppConstants.fieldsmissing);

					logsServices.addLog(AppConstants.error, AppConstants.ANALYTICS, "WorkStationConstraintAnalytics",
							AppConstants.fieldsmissing, null);
					return new ResponseEntity<>(returnmap, HttpStatus.OK);
				}
				else {

					//validation
					if (!Utils.validateDate(requestmap.getOrDefault("startDate", "").toString())) {
						returnmap.put("status", "error");
						returnmap.put("message", AppConstants.errorDatemsg);

						logsServices.addLog(AppConstants.error, AppConstants.ANALYTICS, "WorkStationConstraintAnalytics",
								AppConstants.errorDatemsg, null);
						return new ResponseEntity<>(returnmap, HttpStatus.OK);
					}
					if (!Utils.validateDate(requestmap.getOrDefault("endDate", "").toString())) {
						returnmap.put("status", "error");
						returnmap.put("message", AppConstants.errorDatemsg);

						logsServices.addLog(AppConstants.error, AppConstants.ANALYTICS, "WorkStationConstraintAnalytics",
								AppConstants.errorDatemsg, null);
						return new ResponseEntity<>(returnmap, HttpStatus.OK);
					}
					//Fetch variables after validation
					String sWorkStationId = "All";
					Date dStartDate = new SimpleDateFormat(AppConstants.RequestDateFormat)
							.parse(requestmap.getOrDefault("startDate", "").toString());

					Date dEndDate = new SimpleDateFormat(AppConstants.RequestDateFormat)
							.parse(requestmap.getOrDefault("endDate", "").toString());

					Date dToday = new Date();

					if(dStartDate.compareTo(dEndDate) > 0 ) {
						returnmap.put("status", "error");
						returnmap.put("message", AppConstants.errorDatemismatch);

						logsServices.addLog(AppConstants.error, AppConstants.ANALYTICS, "WorkStationConstraintAnalytics",
								AppConstants.errorDatemismatch, null);
						return new ResponseEntity<>(returnmap, HttpStatus.OK);
					}

					if(dEndDate.compareTo(dToday) > 0 ) {
						returnmap.put("status", "error");
						returnmap.put("message", AppConstants.errorEndDateOverflow);

						logsServices.addLog(AppConstants.error, AppConstants.ANALYTICS, "WorkStationConstraintAnalytics",
								AppConstants.errorEndDateOverflow, null);
						return new ResponseEntity<>(returnmap, HttpStatus.OK);
					}
					LocalDate startDate = dStartDate.toInstant()
							.atZone(ZoneId.systemDefault())
							.toLocalDate();
					LocalDate endDate = dEndDate.toInstant()
							.atZone(ZoneId.systemDefault())
							.toLocalDate();
					LocalDate todayDate = dToday.toInstant()
							.atZone(ZoneId.systemDefault())
							.toLocalDate();

					if(endDate.isEqual(todayDate) && !startDate.isEqual(todayDate)) {
						//Change dEndDate to previous date
						Calendar cal = Calendar.getInstance();
						cal.setTime(dEndDate);
						cal.add(Calendar.DAY_OF_MONTH, -1);
						dEndDate = cal.getTime();
					}

					//Fetch all datas according to the format required
					WorkStationFlowAnalytics workStationFlowAnalytics = getWorkStationFlowAnalytics(sWorkStationId, dStartDate, dEndDate);

					if(workStationFlowAnalytics == null) {

						returnmap.put("status", "error");
						returnmap.put("message", AppConstants.errorNoRecordsFound);
						logsServices.addLog(AppConstants.error, AppConstants.ANALYTICS, "WorkStationConstraintAnalytics",
								AppConstants.errorNoRecordsFound, null);
						return new ResponseEntity<>(returnmap, HttpStatus.OK);


					}
					//Fetch all datas according to the format required
					//Date dToday = new Date();
					WorkStationFlowAnalytics workStationFlowAnalyticsForToday = getWorkStationFlowAnalytics(sWorkStationId, dToday, dToday);

					if(workStationFlowAnalyticsForToday == null) {

						returnmap.put("status", "error");
						returnmap.put("message", AppConstants.errorNoRecordsFound);
						logsServices.addLog(AppConstants.error, AppConstants.ANALYTICS, "WorkStationConstraintAnalytics",
								AppConstants.errorNoRecordsFound, null);
						return new ResponseEntity<>(returnmap, HttpStatus.OK);
					}



					ConstraintAnalysis constraintAnalysis = GetConstraintAnalytics(workStationFlowAnalytics, workStationFlowAnalyticsForToday, dStartDate, dEndDate, dToday);

					List<ConstraintWorkStations> constraintWorkstations = constraintAnalysis.getWorkStations();

					if(constraintWorkstations!= null && constraintWorkstations.size() > 0) {
						returnmap.put("status", "success");
						returnmap.put("data", "fetched records");
						return new ResponseEntity<>(constraintAnalysis, HttpStatus.OK);
					}
					else {
						returnmap.put("status", "error");
						returnmap.put("message", AppConstants.errorNoRecordsFound);
						logsServices.addLog(AppConstants.error, AppConstants.ANALYTICS, "WorkStationConstraintAnalytics",
								AppConstants.errorNoRecordsFound, null);
						return new ResponseEntity<>(returnmap, HttpStatus.OK);
					}
				}
			}
			else
			{
				//key does not exist
			}
		}catch(Exception e){
			returnmap.put("status", "error");
			returnmap.put("message", AppConstants.errormsg);

			return new ResponseEntity<>(returnmap, HttpStatus.OK);
		}
		returnmap.put("status", "error");
		returnmap.put("message", AppConstants.errormsg);

		return new ResponseEntity<>(returnmap, HttpStatus.OK);

	}
	private ConstraintAnalysis GetConstraintAnalytics(WorkStationFlowAnalytics workStationFlowAnalytics,
			WorkStationFlowAnalytics workStationFlowAnalyticsForToday, Date dStartDate, Date dEndDate, Date dToday) {

		List<WorkStations> previousDaysAnalysis = workStationFlowAnalytics.getWorkStations();
		List<WorkStations> todayAnalysis = workStationFlowAnalyticsForToday.getWorkStations();
		int noOfAnalysisDates = getNumberDaysBetweenDates(dStartDate, dEndDate);

		class Constraint{
			Integer todayWaiting;
			Double averageOutgoing;
			String workStationName;

			public Integer getTodayWaiting() {
				return todayWaiting;
			}
			public void setTodayWaiting(Integer todayWaiting) {
				this.todayWaiting = todayWaiting;
			}
			public Double getAverageOutgoing() {
				return averageOutgoing;
			}
			public void setAverageOutgoing(Double averageOutgoing) {
				this.averageOutgoing = averageOutgoing;
			}
			public String getWorkStationName() {
				return workStationName;
			}
			public void setWorkStationName(String workstationName) {
				this.workStationName = workstationName;			
			}

		};

		HashMap<String, Constraint> averageMap = new HashMap<>();


		for(int i = 0; i < previousDaysAnalysis.size(); i++)
		{
			//This loop runs for number of workstations
			WorkStations workstation = previousDaysAnalysis.get(i);
			String workstationId = workstation.getWorkStationid();
			List<WorkStationAnalytics> workStationAnalyticsList = workstation.getWorkStationAnalytics();
			int noOfOutgoingWork = 0;
			double averageWorkPerDay = 0.00;
			for(int j = 0; j < workStationAnalyticsList.size(); j++)
			{
				//this loop runs for number of days
				//the days where no records fetched eg:- non working day, 
				//day where no outgoing is present will not be added here

				WorkStationAnalytics workStationAnalytics  = workStationAnalyticsList.get(j);
				noOfOutgoingWork += workStationAnalytics.getOutgoing();
			}

			if(noOfAnalysisDates != 0)
				averageWorkPerDay = (double) noOfOutgoingWork / noOfAnalysisDates;

			if(averageMap.containsKey(workstationId))
			{
				Constraint constraint = averageMap.get(workstationId);

				constraint.setAverageOutgoing(averageWorkPerDay);				 

			}else
			{
				Constraint constraint = new Constraint();
				constraint.setAverageOutgoing(averageWorkPerDay);

				averageMap.put(workstationId, constraint);
			}

		}
		if (AppConstants.print_log)
			System.out.println("Today : ");

		for(int i = 0; i < todayAnalysis.size(); i++)
		{
			WorkStations workstation = todayAnalysis.get(i);
			String workstationId = workstation.getWorkStationid();
			String workstationName = workstation.getWorkStationName();
			List<WorkStationAnalytics> workStationAnalyticsList = workstation.getWorkStationAnalytics();
			int noOfWaitingWork = 0;
			for(int j = 0; j < workStationAnalyticsList.size(); j++)
			{
				WorkStationAnalytics workStationAnalytics  = workStationAnalyticsList.get(j);

				noOfWaitingWork += workStationAnalytics.getWaiting();


			}

			if(averageMap.containsKey(workstationId))
			{
				Constraint constraint = averageMap.get(workstationId);

				constraint.setTodayWaiting(noOfWaitingWork);
				constraint.setWorkStationName(workstationName);
				averageMap.put(workstationId, constraint);

			}else
			{
				Constraint constraint = new Constraint();
				constraint.setTodayWaiting(noOfWaitingWork);
				constraint.setWorkStationName(workstationName);
				averageMap.put(workstationId, constraint);


			}

		}

		//here calculate constraint analysis data and store it in another class for output
		ConstraintAnalysis constraintAnalysis = new ConstraintAnalysis();
		Set<String> keys = averageMap.keySet();

		for (String key : keys) {
			Constraint constraint = averageMap.get(key);
			Integer todayWaiting = constraint.getTodayWaiting();
			String constraintWorkstationName = constraint.getWorkStationName();
			Double averageOutgoing = constraint.getAverageOutgoing();
			Double constraintValue = 0.00;
			if(averageOutgoing != 0) {
				constraintValue = todayWaiting / averageOutgoing;
			}
			//here is workstation id
			constraintAnalysis.addConstraint(key, constraintWorkstationName, constraintValue);

		}

		return constraintAnalysis;

	}

	@SuppressWarnings("unchecked")
	@RequestMapping(path = { "WorkStationAgingAnalytics" }, method = RequestMethod.POST)
	public ResponseEntity<?> getWorkStationAgingAnalytics(HttpServletRequest request, HttpSession httpSession)
			throws InterruptedException, ExecutionException {
		HashMap<String, Object> returnmap = new HashMap<>();

		System.out.print("Inside WorkStationAgingAnalytics");

		try {

			ResponseEntity<?> responseEntity = checkSessionKey(httpSession, Utils.getCookie(request, "JSESSIONID"), "WACT");
			if (responseEntity != null) { 
				logsServices.addLog(AppConstants.error, AppConstants.ANALYTICS, "WorkStationAgingAnalytics",
					AppConstants.sessiontimeout, null);
				return responseEntity; 
			}
					
			logsServices.addLog(AppConstants.event, AppConstants.ANALYTICS, "WorkStationAgingAnalytics",
					"WorkStation Aging Analytics Get API Details", null);
			
			//Can use this but need to think again 
			Date dStartDate = new Date();
			Date dEndDate = new Date();

			//Change dStartDate to two days before date
			Calendar cal = Calendar.getInstance();
			cal.setTime(dStartDate);
			cal.add(Calendar.DAY_OF_MONTH, -2);
			dStartDate = cal.getTime();
			
			//Fetch all datas according to the format required
			AgingAnalysis agingAnalysis = getWorkOrderAgingAnalysis(dStartDate, dEndDate);

			if(agingAnalysis == null) {

				returnmap.put("status", "error");
				returnmap.put("message", AppConstants.errorNoRecordsFound);
				logsServices.addLog(AppConstants.error, AppConstants.ANALYTICS, "WorkStationAgingAnalytics",
						AppConstants.errorNoRecordsFound, null);
				return new ResponseEntity<>(returnmap, HttpStatus.OK);


			}

			List<AgingWorkStations> agingWorkstations = agingAnalysis.getWorkStations();

			if(agingWorkstations!= null && agingWorkstations.size() > 0) {
				returnmap.put("status", "success");
				returnmap.put("data", "fetched records");
				return new ResponseEntity<>(agingAnalysis, HttpStatus.OK);
			}
			else {
				returnmap.put("status", "error");
				returnmap.put("message", AppConstants.errorNoRecordsFound);
				logsServices.addLog(AppConstants.error, AppConstants.ANALYTICS, "WorkStationAgingAnalytics",
						AppConstants.errorNoRecordsFound, null);
				return new ResponseEntity<>(returnmap, HttpStatus.OK);
			}

		}catch(Exception e){
			returnmap.put("status", "error");
			returnmap.put("message", AppConstants.errormsg);

			return new ResponseEntity<>(returnmap, HttpStatus.OK);
		}
	}
	private AgingAnalysis getWorkOrderAgingAnalysis(Date dStartDate, Date dEndDate) {
		
		return getWorkOrderAgingAnalysis(dStartDate,  dEndDate, "Materials");
	}
	@SuppressWarnings({ "unchecked"})
	private AgingAnalysis getWorkOrderAgingAnalysis(Date startDate, Date endDate, String tableName) {

		AgingAnalysis agingAnalysis = new AgingAnalysis();
		List<AgingAnalytics> agingAnalyticsList = new ArrayList<>();

		HashMap<Date, List<AgingAnalyticsResult>> agingAnalyticsResultMap = new HashMap<>();

		//String Date
		SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MMM-dd");
		List<Date> listOfDates = getDaysBetweenDates(startDate,endDate);
		for(int i=0;i<listOfDates.size();i++)
		{
			List<String> tableNames = (List<String>)analyticsRepository.SelectTableNamesLike(GetTableName(listOfDates.get(i), tableName));
			if(tableNames != null)
			{
				Date analyticsDate = listOfDates.get(i);
				List<AgingAnalyticsResult> agingAnalyticsResult = new ArrayList<>();

				agingAnalyticsResult = analyticsRepository.SelectWorkorderAgingAnalytics(tableNames);

				if(null != agingAnalyticsResult) {
					agingAnalyticsResultMap.put(analyticsDate, agingAnalyticsResult);

					String analyticsDateStr = formatter.format(analyticsDate);
					String endDateStr = formatter.format(endDate);

					if(analyticsDateStr.equals(endDateStr)) {
						for(int j=0;j<agingAnalyticsResult.size();j++) {
							AgingAnalytics agingAnalytics = new AgingAnalytics();
							agingAnalytics.setWorkOrderId(agingAnalyticsResult.get(j).getWorkOrderId());
							agingAnalytics.setWorkStationid(agingAnalyticsResult.get(j).getWorkStationId());
							agingAnalytics.setWorkStationName(agingAnalyticsResult.get(j).getWorkStationName());
							agingAnalytics.setCount(1);
							agingAnalyticsList.add(agingAnalytics);
						}

					}
				}
			}
		}
		for(int j=0;j<agingAnalyticsList.size();j++) {
			/*System.out.println(agingAnalyticsList.get(j).getCount());
			System.out.println(agingAnalyticsList.get(j).getWorkStationId());
			System.out.println(agingAnalyticsList.get(j).getWorkStationName());
			System.out.println(agingAnalyticsList.get(j).getWorkOrderId());*/


			for(int i=0;i<listOfDates.size();i++)
			{
				Date analyticsDate = listOfDates.get(i);
				List<AgingAnalyticsResult> agingAnalyticsResult = agingAnalyticsResultMap.get(analyticsDate);
				String analyticsDateStr = formatter.format(analyticsDate);
				String endDateStr = formatter.format(endDate);


				if(!analyticsDateStr.equals(endDateStr)) {
					for(int k=0;k<agingAnalyticsResult.size();k++) {
						// check whether workorderid is equal
						if(agingAnalyticsResult.get(k).getWorkOrderId().equals( agingAnalyticsList.get(j).getWorkOrderId())){
							agingAnalyticsList.get(j).setCount(agingAnalyticsList.get(j).getCount() + 1);
						}
					}
				}
			}

		}		
		
		
		for(int j=0;j<agingAnalyticsList.size();j++) {

			int todayAging = 0;
			int twoDaysAging = 0;
			int greaterThantwodaysAging = 0;

			if(agingAnalyticsList.get(j).getCount() == 1) {
				todayAging = 1;

			}else if(agingAnalyticsList.get(j).getCount() == 2) {
				twoDaysAging = 1;

			}else if(agingAnalyticsList.get(j).getCount() == 3) {
				greaterThantwodaysAging = 1;
			}

			agingAnalysis.addAging(agingAnalyticsList.get(j).getWorkStationId(),
					todayAging,
					twoDaysAging,
					greaterThantwodaysAging,
					agingAnalyticsList.get(j).getWorkStationName()
					);
			

		}
		return agingAnalysis;
	}

			/*HashMap<String, AgingWorkStations> finalResultMap = new HashMap<>();
			

			for(int j=0;j<agingAnalyticsList.size();j++) {

				int todayAging = 0;
				int twoDaysAging = 0;
				int greaterThantwodaysAging = 0;

				if(finalResultMap.containsKey(agingAnalyticsList.get(j).getWorkStationId())){
					AgingWorkStations agingWorkStationLocal = finalResultMap.get(agingAnalyticsList.get(j).getWorkStationId());

					if(agingAnalyticsList.get(j).getCount() == 1) {
						todayAging = 1;

					}else if(agingAnalyticsList.get(j).getCount() == 2) {
						twoDaysAging = 1;

					}else if(agingAnalyticsList.get(j).getCount() == 3) {
						greaterThantwodaysAging = 1;
					}

					agingAnalysis.addAging(agingWorkStationLocal.getWorkStationid(),
							agingWorkStationLocal.getToday() + todayAging,
							agingWorkStationLocal.getLast2Days() + todayAging,
							agingWorkStationLocal.getGreaterThan2Days()+ greaterThantwodaysAging,
							agingWorkStationLocal.getWorkStationName()
							);

				}
				else {

				}
				if
				System.out.println("count" + agingAnalyticsList.get(j).getCount());
				System.out.println("workstationId" + agingAnalyticsList.get(j).getWorkStationId());
				System.out.println("workStationName" + agingAnalyticsList.get(j).getWorkStationName());
			}*/
			/*AgingAnalytics agingAnalytics = new AgingAnalytics();agingAnalytics.setWorkOrderId(agingAnalyticsResult.get(j).getWorkOrderId());
					agingAnalytics.setWorkStationid(agingAnalyticsResult.get(j).getWorkStationId());
					agingAnalytics.setWorkStationName(agingAnalyticsResult.get(j).getWorkStationName());
					agingAnalytics.setCount(1);
					agingAnalyticsList.add(agingAnalytics);
				}

				List<AgingAnalyticsResult> agingAnalyticsResultLocal = agingAnalyticsResultMap.get(listOfDates.get(i));
				for(int j=0;j<agingAnalyticsList.size();j++) {
					for(int k=0;k<agingAnalyticsResultLocal.size();k++) {
						String workOrderId = agingAnalyticsResultLocal.get(i).getWorkOrderId();
						if(!agingAnalyticsList.get(j).getWorkOrderId().equals(workOrderId)) {
							agingAnalyticsList.get(j).setCount(agingAnalyticsList.get(j).getCount() + 1);
						}
					}
				}
				for(int j=0;j<agingAnalyticsResultLocal.size();j++) {
					System.out.println(analyticsDate);
					System.out.println((agingAnalyticsResultLocal.get(j)).getWorkOrderId());
					System.out.println(agingAnalyticsResultLocal.get(j).getWorkStationId());
					System.out.println(agingAnalyticsResultLocal.get(j).getWorkStationName());
				}

				for(int j=0;j<agingAnalyticsList.size();j++) {
					System.out.println(analyticsDate + " "+ j);
					System.out.println(agingAnalyticsList.get(j).getCount());
					System.out.println(agingAnalyticsList.get(j).getWorkStationId());
					System.out.println(agingAnalyticsList.get(j).getWorkStationName());
				}*/
			/*if(agingAnalyticsResultMap.containsKey(listOfDates.get(i))){
					List<AgingAnalyticsResult> agingAnalyticsResultLocal = agingAnalyticsResultMap.get(listOfDates.get(i));

					System.out.println(listOfDates.get(i));


					for(int j=0;j<agingAnalyticsResultLocal.size();j++) {
						AgingAnalytics agingAnalytics = new AgingAnalytics();
						agingAnalytics.setWorkStationid(agingAnalyticsResult.get(i).getWorkStationId());
						agingAnalytics.setWorkStationName(agingAnalyticsResult.get(i).getWorkStationName());
						agingAnalytics.setCount(1);
					}

				}
			}
		}*/

	/*private AgingAnalysis GetAgingAnalytics(WorkStationFlowAnalytics workStationFlowAnalytics) {
		//here all analytics according to date and workstation will be in input workStationAnalytics
		//need to parse and calculate and store in AgingAnalysis data

		DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MMM-dd");

		Date dToday = new Date();
		Date dTwoDaysBefore;

		//Change dStartDate to two days before date
		Calendar cal = Calendar.getInstance();
		cal.setTime(dToday);
		cal.add(Calendar.DAY_OF_MONTH, -2);
		dTwoDaysBefore = cal.getTime();



		LocalDate todayDate = dToday.toInstant()
				.atZone(ZoneId.systemDefault())
				.toLocalDate();
		LocalDate twoDaysBeforeDate = dTwoDaysBefore.toInstant()
				.atZone(ZoneId.systemDefault())
				.toLocalDate();


		AgingAnalysis agingAnalysis = new AgingAnalysis();

		List<WorkStations> allWorkStationAnalytics = workStationFlowAnalytics.getWorkStations();

		/*class Aging{

			int todayAging;
			int twoDaysAging;
			int greaterThantwodaysAging;

			public int getTodayAging() {
				return todayAging;
			}
			public void setTodayAging(int todayAging) {
				this.todayAging = todayAging;
			}
			public int getTwoDaysAging() {
				return twoDaysAging;
			}
			public void setTwoDaysAging(int twoDaysAging) {
				this.twoDaysAging = twoDaysAging;
			}
			public int getGreaterThantwodaysAging() {
				return greaterThantwodaysAging;
			}
			public void setGreaterThantwodaysAging(int greaterThantwodaysAging) {
				this.greaterThantwodaysAging = greaterThantwodaysAging;
			}
		};

		HashMap<String, Aging> agingMap = new HashMap<>(); //*


		for(int i = 0; i < allWorkStationAnalytics.size(); i++)
		{
			//This loop runs for number of workstations
			WorkStations workstation = allWorkStationAnalytics.get(i);
			String workstationId = workstation.getWorkStationid();
			String workstationName = workstation.getWorkStationName();
			List<WorkStationAnalytics> workStationAnalyticsList = workstation.getWorkStationAnalytics();

			int todayAging = 0;
			int twoDaysAging = 0;
			int greaterThantwodaysAging = 0;
			String date = "";
			for(int j = 0; j < workStationAnalyticsList.size(); j++)
			{
				//this loop runs for number of days
				//the days where no records fetched eg:- non working day, 
				//day where no outgoing is present will not be added here

				WorkStationAnalytics workStationAnalytics  = workStationAnalyticsList.get(j);


				date = workStationAnalytics.getDate();
				//convert String to LocalDate
				LocalDate localDate = LocalDate.parse(date, formatter);

				//System.out.print("Today date: " + todayDate);
				//System.out.print("Two days before date:" + twoDaysBeforeDate);
				//System.out.print("Analysis Date: " + date);
				//System.out.print("Parsed Analysis Date: " + localDate);


				//Here the date is checked for today or within two days or more than 2 days
				if(localDate.isEqual(todayDate)) {
					todayAging += workStationAnalytics.getWaiting();
					//System.out.println("Inside today");
				}else if(localDate.isEqual(twoDaysBeforeDate) || localDate.isAfter(twoDaysBeforeDate)) {
					twoDaysAging += workStationAnalytics.getWaiting();
					//System.out.println("Inside two days");
				}else {
					greaterThantwodaysAging += workStationAnalytics.getWaiting();
					//System.out.println("Inside greater than two days");
				}

			}

			agingAnalysis.addAging(workstationId, todayAging, twoDaysAging, greaterThantwodaysAging, workstationName);
		}

		return agingAnalysis;
	}*/

	@RequestMapping(path = { "WorkStationInventory" }, method = RequestMethod.POST)
	public ResponseEntity<?> getWorkStationInventory(HttpServletRequest request, HttpSession httpSession,
			@RequestBody HashMap<String, Object> requestmap) throws InterruptedException, ExecutionException {
		HashMap<String, Object> returnmap = new HashMap<>();

		System.out.print("Inside WorkStationInventoryAnalytics");

		try {
			
			ResponseEntity<?> responseEntity = checkSessionKey(httpSession, Utils.getCookie(request, "JSESSIONID"), "WCND");
			if (responseEntity != null) { 
				logsServices.addLog(AppConstants.error, AppConstants.ANALYTICS, "WorkStationInventory",
					AppConstants.sessiontimeout, null);
				return responseEntity; 
			}
			
			Map<String, String> requestLogMap = new HashMap<>(); 
			requestLogMap.put("params", new JSONObject(requestmap).toString());
			
			logsServices.addLog(AppConstants.event, AppConstants.ANALYTICS, "WorkStationInventory",
					"WorkStation Inventory Get API Details", requestLogMap);
			
			
			//validate start
			if (requestmap.containsKey("WorkStationId") && requestmap.containsKey("startDate") && requestmap.containsKey("endDate"))
			{
				//validation
				if( Utils.isEmptyString(requestmap.getOrDefault("WorkStationId", "").toString())
						||Utils.isEmptyString(requestmap.getOrDefault("startDate", "").toString())
						|| Utils.isEmptyString(requestmap.getOrDefault("endDate", "").toString())) {
					//any one are some of the request parameters is empty
					returnmap.put("status", "error");
					returnmap.put("message", AppConstants.fieldsmissing);

					logsServices.addLog(AppConstants.error, AppConstants.ANALYTICS, "WorkStationInventory",
							AppConstants.fieldsmissing, null);
					return new ResponseEntity<>(returnmap, HttpStatus.OK);
				}
				else {

					//validation
					if (!Utils.validateDate(requestmap.getOrDefault("startDate", "").toString())) {
						returnmap.put("status", "error");
						returnmap.put("message", AppConstants.errorDatemsg);

						logsServices.addLog(AppConstants.error, AppConstants.ANALYTICS, "WorkStationInventory",
								AppConstants.errorDatemsg, null);
						return new ResponseEntity<>(returnmap, HttpStatus.OK);
					}
					if (!Utils.validateDate(requestmap.getOrDefault("endDate", "").toString())) {
						returnmap.put("status", "error");
						returnmap.put("message", AppConstants.errorDatemsg);

						logsServices.addLog(AppConstants.error, AppConstants.ANALYTICS, "WorkStationInventory",
								AppConstants.errorDatemsg, null);
						return new ResponseEntity<>(returnmap, HttpStatus.OK);
					}
					//Fetch variables after validation
					String sWorkStationId = (String) requestmap.get("WorkStationId");

					Date dStartDate = new SimpleDateFormat(AppConstants.RequestDateFormat)
							.parse(requestmap.getOrDefault("startDate", "").toString());

					Date dEndDate = new SimpleDateFormat(AppConstants.RequestDateFormat)
							.parse(requestmap.getOrDefault("endDate", "").toString());

					Date dToday = new Date();

					if(dStartDate.compareTo(dEndDate) > 0 ) {
						returnmap.put("status", "error");
						returnmap.put("message", AppConstants.errorDatemismatch);

						logsServices.addLog(AppConstants.error, AppConstants.ANALYTICS, "WorkStationInventory",
								AppConstants.errorDatemismatch, null);
						return new ResponseEntity<>(returnmap, HttpStatus.OK);
					}

					if(dEndDate.compareTo(dToday) > 0 ) {
						returnmap.put("status", "error");
						returnmap.put("message", AppConstants.errorEndDateOverflow);

						logsServices.addLog(AppConstants.error, AppConstants.ANALYTICS, "WorkStationInventory",
								AppConstants.errorEndDateOverflow, null);
						return new ResponseEntity<>(returnmap, HttpStatus.OK);
					}
					/*LocalDate firstDate = dEndDate.toInstant()
						      .atZone(ZoneId.systemDefault())
						      .toLocalDate();
					LocalDate secondDate = dToday.toInstant()
						      .atZone(ZoneId.systemDefault())
						      .toLocalDate();

					if(firstDate.isEqual(secondDate)) {
						//Change dEndDate to previous date
						Calendar cal = Calendar.getInstance();
						cal.setTime(dEndDate);
						cal.add(Calendar.DAY_OF_MONTH, -1);
						dEndDate = cal.getTime();
					}*/

					//Fetch all datas according to the format required
					WorkStationInventoryAnalysis workStationInventoryAnalysis = getWorkStationInventoryAnalytics(sWorkStationId, dStartDate, dEndDate);

					if(workStationInventoryAnalysis == null) {

						returnmap.put("status", "error");
						returnmap.put("message", AppConstants.errorNoRecordsFound);
						logsServices.addLog(AppConstants.error, AppConstants.ANALYTICS, "WorkStationInventory",
								AppConstants.errorNoRecordsFound, null);
						return new ResponseEntity<>(returnmap, HttpStatus.OK);


					}else{
						returnmap.put("status", "success");
						returnmap.put("data", "fetched records");
						return new ResponseEntity<>(workStationInventoryAnalysis, HttpStatus.OK);
					}					
				}
			}
			else
			{
				//key does not exist
			}
		}catch(Exception e){
			returnmap.put("status", "error");
			returnmap.put("message", AppConstants.errormsg);

			return new ResponseEntity<>(returnmap, HttpStatus.OK);
		}
		returnmap.put("status", "error");
		returnmap.put("message", AppConstants.errormsg);

		return new ResponseEntity<>(returnmap, HttpStatus.OK);

	}
	private WorkStationInventoryAnalysis getWorkStationInventoryAnalytics(String workStationId, Date startDate,
			Date endDate) {

		return getWorkStationInventoryAnalytics(workStationId, startDate, endDate, "Materials" );
	}
	@SuppressWarnings({ "unchecked"})
	private WorkStationInventoryAnalysis getWorkStationInventoryAnalytics(String workStationId, Date startDate, Date endDate, String tableName) {

		WorkStationInventoryAnalysis workStationInventoryAnalytics = new WorkStationInventoryAnalysis();


		SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MMM-dd");

		String analyticsTimeDuration = formatter.format(startDate) + " to " + formatter.format(endDate);

		workStationInventoryAnalytics.setDate(analyticsTimeDuration);
		//String Date
		List<Date> listOfDates = getDaysBetweenDates(startDate,endDate);
		for(int i=0;i<listOfDates.size();i++)
		{
			//System.out.println("In Between Dates : " + listOfDates.get(i) );
			////System.out.println("In Between Table Names : " + GetTableName(listOfDates.get(i)) );
			List<String> tableNames = (List<String>)analyticsRepository.SelectTableNamesLike(GetTableName(listOfDates.get(i), tableName));
			if(tableNames != null)
			{
				//Here find incoming,outgoing, waiting, and storage area items for one day
				//and add them or send all days and fetch bulk
				//we can use procedure or simple query whichever preferred
				//for all workstation or one workstation

				// use this loop when we want to get records for particular shift
				/*for(int j=0;j<tableNames.size();j++)
				{
					//System.out.println("In Between Table Names : " + tableNames.get(j));
				}*///This ends down this is to check table names

				List<WorkStationInventoryResult> resultInventory = null;



				if(workStationId.equalsIgnoreCase("All")) { 
					resultInventory = (List<WorkStationInventoryResult>) analyticsRepository.SelectAllWorkstationInventoryAnalytics(tableNames);

				}
				else {
					resultInventory = (List<WorkStationInventoryResult>) analyticsRepository.SelectWorkstationInventoryAnalytics(tableNames, workStationId);
				}





				if(resultInventory != null) {
					for(int k=0;k<resultInventory.size();k++)
					{
						WorkStationInventoryResult fetchedWorkStationInventoryResult = resultInventory.get(k); 

						if(fetchedWorkStationInventoryResult != null)
						{

							workStationInventoryAnalytics.setInventoryAmount(fetchedWorkStationInventoryResult.getStationId(),
									fetchedWorkStationInventoryResult.getStationName(), fetchedWorkStationInventoryResult.getWaitingAmount(),
									formatter.format(listOfDates.get(i)));  
						}
					}
				}


				//}shift for loop ends here
			}
		}
		if(workStationInventoryAnalytics.IsRecordAvailable())
			return workStationInventoryAnalytics;
		else
			return null;

	}

	@RequestMapping(path = { "WorkOrderTimeAnalytics" }, method = RequestMethod.POST)
	public ResponseEntity<?> getWorkOrderTimeAnalyticsWithDate(HttpServletRequest request, HttpSession httpSession,
			@RequestBody HashMap<String, Object> requestmap)throws InterruptedException, ExecutionException {
		HashMap<String, Object> returnmap = new HashMap<>();

		System.out.print("Inside WorkOrderTimeAnalytics");

		try {
			
			ResponseEntity<?> responseEntity = checkSessionKey(httpSession, Utils.getCookie(request, "JSESSIONID"), "DTAN");
			if (responseEntity != null) { 
				logsServices.addLog(AppConstants.error, AppConstants.ANALYTICS, "WorkOrderTimeAnalytics",
					AppConstants.sessiontimeout, null);
				return responseEntity; 
			}
			
			Map<String, String> requestLogMap = new HashMap<>(); 
			requestLogMap.put("params", new JSONObject(requestmap).toString());
			
			logsServices.addLog(AppConstants.event, AppConstants.ANALYTICS, "WorkOrderTimeAnalytics",
					"WorkOrder Time Analytics API Details", requestLogMap);
			
			//validation
			if(Utils.isEmptyString(requestmap.getOrDefault("startDate", "").toString())
					|| Utils.isEmptyString(requestmap.getOrDefault("endDate", "").toString())) {
				//any one are some of the request parameters is empty
				returnmap.put("status", "error");
				returnmap.put("message", AppConstants.fieldsmissing);

				logsServices.addLog(AppConstants.error, AppConstants.ANALYTICS, "WorkOrderTimeAnalytics",
						AppConstants.fieldsmissing, null);
				return new ResponseEntity<>(returnmap, HttpStatus.OK);
			}
			else {

				//validation
				if (!Utils.validateDate(requestmap.getOrDefault("startDate", "").toString())) {
					returnmap.put("status", "error");
					returnmap.put("message", AppConstants.errorDatemsg);

					logsServices.addLog(AppConstants.error, AppConstants.ANALYTICS, "WorkOrderTimeAnalytics",
							AppConstants.errorDatemsg, null);
					return new ResponseEntity<>(returnmap, HttpStatus.OK);
				}
				if (!Utils.validateDate(requestmap.getOrDefault("endDate", "").toString())) {
					returnmap.put("status", "error");
					returnmap.put("message", AppConstants.errorDatemsg);

					logsServices.addLog(AppConstants.error, AppConstants.ANALYTICS, "WorkOrderTimeAnalytics",
							AppConstants.errorDatemsg, null);
					return new ResponseEntity<>(returnmap, HttpStatus.OK);
				}
				
				Date dStartDate = new SimpleDateFormat(AppConstants.RequestDateFormat)
						.parse(requestmap.getOrDefault("startDate", "").toString());

				Date dEndDate = new SimpleDateFormat(AppConstants.RequestDateFormat)
						.parse(requestmap.getOrDefault("endDate", "").toString());

				if(dStartDate.compareTo(dEndDate) > 0 ) {
					returnmap.put("status", "error");
					returnmap.put("message", AppConstants.errorDatemismatch);

					logsServices.addLog(AppConstants.error, AppConstants.ANALYTICS, "WorkOrderTimeAnalytics",
							AppConstants.errorDatemismatch, null);
					return new ResponseEntity<>(returnmap, HttpStatus.OK);
				}

				//System.out.println("UserId : " + sUserId);
				//System.out.println("Start Date : " + dStartDate);
				//System.out.println("End date : " + dEndDate);

				Calendar endDateCalendar = new GregorianCalendar();
				endDateCalendar.setTime(dEndDate);
				endDateCalendar.add(Calendar.DATE, 1);
				dEndDate = endDateCalendar.getTime();
				
				//Fetch all datas according to the format required
				WorkOrderTimeAnalytics workOrderTimeAnalytics = getWorkOrderTimeAnalytics(dStartDate, dEndDate);

				if(workOrderTimeAnalytics == null || workOrderTimeAnalytics.getWorkOrderListCount() <= 0) {

					returnmap.put("status", "error");
					returnmap.put("message", AppConstants.errorNoRecordsFound);
					logsServices.addLog(AppConstants.error, AppConstants.ANALYTICS, "WorkOrderTimeAnalytics",
							AppConstants.errorNoRecordsFound, null);
					return new ResponseEntity<>(returnmap, HttpStatus.OK);

				}

				returnmap.put("status", "success");
				returnmap.put("data", "fetched records");

				return new ResponseEntity<>(workOrderTimeAnalytics, HttpStatus.OK);
			}
		}catch(Exception e){
			returnmap.put("status", "error");
			returnmap.put("message", AppConstants.errormsg);

			return new ResponseEntity<>(returnmap, HttpStatus.OK);
		}
	}
	@RequestMapping(path = { "AllWorkOrderTimeAnalytics" }, method = RequestMethod.POST)
	public ResponseEntity<?> getWorkOrderTimeAnalyticsNoDate(HttpServletRequest request, HttpSession httpSession)
			throws InterruptedException, ExecutionException {
		HashMap<String, Object> returnmap = new HashMap<>();

		System.out.print("Inside getWorkOrderTimeAnalytics");

		try {
			
			ResponseEntity<?> responseEntity = checkSessionKey(httpSession, Utils.getCookie(request, "JSESSIONID"), "DTAN");
			if (responseEntity != null) { 
				logsServices.addLog(AppConstants.error, AppConstants.ANALYTICS, "AllWorkOrderTimeAnalytics",
					AppConstants.sessiontimeout, null);
				return responseEntity; 
			}
			 
			
			logsServices.addLog(AppConstants.event, AppConstants.ANALYTICS, "AllWorkOrderTimeAnalytics",
					"All WorkOrders Time Analytics API Details", null);
			
			//Fetch all datas according to the format required
			WorkOrderTimeAnalytics workOrderTimeAnalytics = getWorkOrderTimeAnalytics();

			if(workOrderTimeAnalytics == null) {

				returnmap.put("status", "error");
				returnmap.put("message", AppConstants.errorNoRecordsFound);
				logsServices.addLog(AppConstants.error, AppConstants.ANALYTICS, "WorkOrderTimeAnalytics",
						AppConstants.errorNoRecordsFound, null);
				return new ResponseEntity<>(returnmap, HttpStatus.OK);


			}

			returnmap.put("status", "success");
			returnmap.put("data", "fetched records");

			return new ResponseEntity<>(workOrderTimeAnalytics, HttpStatus.OK);

		}catch(Exception e){
			returnmap.put("status", "error");
			returnmap.put("message", AppConstants.errormsg);

			return new ResponseEntity<>(returnmap, HttpStatus.OK);
		}
	}

	private WorkOrderTimeAnalytics getWorkOrderTimeAnalytics() {

		WorkOrderTimeAnalytics workOrderTimeAnalytics = null;
		List<WorkOrderTimeAnalyticsResult> workOrderTimeAnalyticsResultList = analyticsRepository.SelectAllWorkstationTimeFlowAnalytics();
		if( workOrderTimeAnalyticsResultList != null) {
			SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MMM-dd HH:mm");
			workOrderTimeAnalytics = new WorkOrderTimeAnalytics();

			for(int i =0; i < workOrderTimeAnalyticsResultList.size(); i++) {
				WorkOrderTimeAnalyticsResult workOrderTimeAnalyticsResult = workOrderTimeAnalyticsResultList.get(i);

				workOrderTimeAnalytics.setWorkOrderTimeAnalytics(workOrderTimeAnalyticsResult.getOrderId(), 
						formatter.format(workOrderTimeAnalyticsResult.getStartTime()),
						formatter.format(workOrderTimeAnalyticsResult.getEndTime()), workOrderTimeAnalyticsResult.getDiffinMinutes(),
						workOrderTimeAnalyticsResult.getStationId(), workOrderTimeAnalyticsResult.getStationName(), 
						formatter.format(workOrderTimeAnalyticsResult.getWorkOrderStartTime()),
						formatter.format(workOrderTimeAnalyticsResult.getWorkOrderEndTime()),
						workOrderTimeAnalyticsResult.getTotalTime());
			}
		}
		return workOrderTimeAnalytics;
	}
	private WorkOrderTimeAnalytics getWorkOrderTimeAnalytics(Date startDate, Date endDate) {

		WorkOrderTimeAnalytics workOrderTimeAnalytics = null;
		List<WorkOrderTimeAnalyticsResult> workOrderTimeAnalyticsResultList = analyticsRepository.SelectAllWorkstationTimeFlowAnalytics(startDate, endDate);
		if( workOrderTimeAnalyticsResultList != null) {
			SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MMM-dd HH:mm");
			workOrderTimeAnalytics = new WorkOrderTimeAnalytics();

			for(int i =0; i < workOrderTimeAnalyticsResultList.size(); i++) {
				WorkOrderTimeAnalyticsResult workOrderTimeAnalyticsResult = workOrderTimeAnalyticsResultList.get(i);

				workOrderTimeAnalytics.setWorkOrderTimeAnalytics(workOrderTimeAnalyticsResult.getOrderId(), 
						formatter.format(workOrderTimeAnalyticsResult.getStartTime()),
						formatter.format(workOrderTimeAnalyticsResult.getEndTime()), workOrderTimeAnalyticsResult.getDiffinMinutes(),
						workOrderTimeAnalyticsResult.getStationId(), workOrderTimeAnalyticsResult.getStationName(), 
						formatter.format(workOrderTimeAnalyticsResult.getWorkOrderStartTime()),
						formatter.format(workOrderTimeAnalyticsResult.getWorkOrderEndTime()),
						workOrderTimeAnalyticsResult.getTotalTime());
			}
		}
		return workOrderTimeAnalytics;
	}

	@RequestMapping(path = { "ZoneFootFall" }, method = RequestMethod.POST)
	public ResponseEntity<?> getZoneFootFall(HttpServletRequest request, HttpSession httpSession,
			@RequestBody HashMap<String, Object> requestmap) throws InterruptedException, ExecutionException {
		HashMap<String, Object> returnmap = new HashMap<>();

		System.out.print("Inside getZoneFootFall");

		try {
			
			
			ResponseEntity<?> responseEntity = checkSessionKey(httpSession, Utils.getCookie(request, "JSESSIONID"), "DZFA");
			if (responseEntity != null) { 
				logsServices.addLog(AppConstants.error, AppConstants.ANALYTICS, "ZoneFootFall",
					AppConstants.sessiontimeout, null);
				return responseEntity; 
			}
			
			  Map<String, String> requestLogMap = new HashMap<>();
			  requestLogMap.put("params", new JSONObject(requestmap).toString());
			 
			
			logsServices.addLog(AppConstants.event, AppConstants.ANALYTICS, "ZoneFootFall",
					"Zone FootFall API Details", requestLogMap);
			
			
			//validate start
			if (requestmap.containsKey("zoneId") && requestmap.containsKey("startDate") && requestmap.containsKey("endDate"))
			{
				//validation
				if( Utils.isEmptyString(requestmap.getOrDefault("zoneId", "").toString())
						||Utils.isEmptyString(requestmap.getOrDefault("startDate", "").toString())
						|| Utils.isEmptyString(requestmap.getOrDefault("endDate", "").toString())) {
					//any one are some of the request parameters is empty
					returnmap.put("status", "error");
					returnmap.put("message", AppConstants.fieldsmissing);

					logsServices.addLog(AppConstants.error, AppConstants.ANALYTICS, "ZoneFootFall",
							AppConstants.fieldsmissing, null);
					return new ResponseEntity<>(returnmap, HttpStatus.OK);
				}
				else {

					//validation
					if (!Utils.validateDate(requestmap.getOrDefault("startDate", "").toString())) {
						returnmap.put("status", "error");
						returnmap.put("message", AppConstants.errorDatemsg);

						logsServices.addLog(AppConstants.error, AppConstants.ANALYTICS, "ZoneFootFall",
								AppConstants.errorDatemsg, null);
						return new ResponseEntity<>(returnmap, HttpStatus.OK);
					}
					if (!Utils.validateDate(requestmap.getOrDefault("endDate", "").toString())) {
						returnmap.put("status", "error");
						returnmap.put("message", AppConstants.errorDatemsg);

						logsServices.addLog(AppConstants.error, AppConstants.ANALYTICS, "ZoneFootFall",
								AppConstants.errorDatemsg, null);
						return new ResponseEntity<>(returnmap, HttpStatus.OK);
					}
					//Fetch variables after validation
					String sZoneId = (String) requestmap.get("zoneId");

					Date dStartDate = new SimpleDateFormat(AppConstants.RequestDateFormat)
							.parse(requestmap.getOrDefault("startDate", "").toString());

					Date dEndDate = new SimpleDateFormat(AppConstants.RequestDateFormat)
							.parse(requestmap.getOrDefault("endDate", "").toString());

					Date dToday = new Date();

					if(dStartDate.compareTo(dEndDate) > 0 ) {
						returnmap.put("status", "error");
						returnmap.put("message", AppConstants.errorDatemismatch);

						logsServices.addLog(AppConstants.error, AppConstants.ANALYTICS, "ZoneFootFall",
								AppConstants.errorDatemismatch, null);
						return new ResponseEntity<>(returnmap, HttpStatus.OK);
					}

					if(dEndDate.compareTo(dToday) > 0 ) {
						returnmap.put("status", "error");
						returnmap.put("message", AppConstants.errorEndDateOverflow);

						logsServices.addLog(AppConstants.error, AppConstants.ANALYTICS, "ZoneFootFall",
								AppConstants.errorEndDateOverflow, null);
						return new ResponseEntity<>(returnmap, HttpStatus.OK);
					}

					//Fetch all datas according to the format required
					ZoneFootFallAnalysis zoneFootFallAnalysis = getZoneFootFallAnalytics(sZoneId, dStartDate, dEndDate);

					if(zoneFootFallAnalysis == null) {

						returnmap.put("status", "error");
						returnmap.put("message", AppConstants.errorNoRecordsFound);
						logsServices.addLog(AppConstants.error, AppConstants.ANALYTICS, "ZoneFootFall",
								AppConstants.errorNoRecordsFound, null);
						return new ResponseEntity<>(returnmap, HttpStatus.OK);


					}else{
						returnmap.put("status", "success");
						returnmap.put("data", "fetched records");
						return new ResponseEntity<>(zoneFootFallAnalysis, HttpStatus.OK);
					}					
				}
			}
			else
			{
				//key does not exist
			}
		}catch(Exception e){
			returnmap.put("status", "error");
			returnmap.put("message", AppConstants.errormsg);

			return new ResponseEntity<>(returnmap, HttpStatus.OK);
		}
		returnmap.put("status", "error");
		returnmap.put("message", AppConstants.errormsg);

		return new ResponseEntity<>(returnmap, HttpStatus.OK);

	}
	@SuppressWarnings("unchecked")
	private ZoneFootFallAnalysis getZoneFootFallAnalytics(String zoneId, Date startDate, Date endDate) {

		ZoneFootFallAnalysis zoneFootFallAnalytics = new ZoneFootFallAnalysis();
		String tableName1 = "Staff";
		String tableName2 = "Visitor";

		SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MMM-dd");

		//String Date
		List<Date> listOfDates = getDaysBetweenDates(startDate,endDate);
		for(int i=0;i<listOfDates.size();i++)
		{
			//System.out.println("In Between Dates : " + listOfDates.get(i) );
			////System.out.println("In Between Table Names : " + GetTableName(listOfDates.get(i)) );
			List<String> tableNames1 = (List<String>)analyticsRepository.SelectTableNamesLike(GetTableName(listOfDates.get(i), tableName1));
			List<String> tableNames2 = (List<String>)analyticsRepository.SelectTableNamesLike(GetTableName(listOfDates.get(i), tableName2));

			if(tableNames1 == null && tableNames2 == null) {
				continue;
			}
			//Here find incoming,outgoing, waiting, and storage area items for one day
			//and add them or send all days and fetch bulk
			//we can use procedure or simple query whichever preferred
			//for all workstation or one workstation

			// use this loop when we want to get records for particular shift
			/*for(int j=0;j<tableNames.size();j++)
				{
					//System.out.println("In Between Table Names : " + tableNames.get(j));
				}*///This ends down this is to check table names

			List<ZoneFootFallResult> resultFootFall = null;
			List<ZoneFootFallAverageResult> resultAverageFootFall = null;



			if(zoneId.equalsIgnoreCase("All")) { 
				resultFootFall = (List<ZoneFootFallResult>) analyticsRepository.SelectAllZoneFootFallAnalytics(tableNames1, tableNames2);
				resultAverageFootFall = (List<ZoneFootFallAverageResult>) analyticsRepository.SelectAllZoneAverageFootFallAnalytics(tableNames1, tableNames2);
			}
			else {
				resultFootFall = (List<ZoneFootFallResult>) analyticsRepository.SelectZoneFootFallAnalytics(tableNames1, tableNames2, zoneId);
				resultAverageFootFall = (List<ZoneFootFallAverageResult>) analyticsRepository.SelectZoneAverageFootFallAnalytics(tableNames1, tableNames2, zoneId);
			}





			if(resultFootFall != null) {
				for(int k=0;k<resultFootFall.size();k++)
				{
					ZoneFootFallResult zoneFootFallResult = resultFootFall.get(k); 

					if(zoneFootFallResult != null)
					{

						zoneFootFallAnalytics.setTQ(formatter.format(listOfDates.get(i)), zoneFootFallResult.getZoneId(),
								zoneFootFallResult.getZoneName(), zoneFootFallResult.gettQ(), zoneFootFallResult.getNoOfPersons());
					}
				}
			}
			if(resultAverageFootFall != null) {
				for(int k=0;k<resultAverageFootFall.size();k++)
				{
					ZoneFootFallAverageResult zoneAverageFootFallResult = resultAverageFootFall.get(k); 

					if(zoneAverageFootFallResult != null)
					{

						zoneFootFallAnalytics.setDwellTime(formatter.format(listOfDates.get(i)), zoneAverageFootFallResult.getZoneId(),
								zoneAverageFootFallResult.getAverage(), zoneAverageFootFallResult.getTotalPersons(), 
								zoneAverageFootFallResult.getMaximum());
					}
				}
			}



			//}shift for loop ends here
		}

		if(zoneFootFallAnalytics.IsRecordAvailable())
			return zoneFootFallAnalytics;
		else
			return null;

	}
	@RequestMapping(path = { "attendance" }, method = RequestMethod.POST)
	public ResponseEntity<?> getAttendance(HttpServletRequest request, HttpSession httpSession,
			@RequestBody HashMap<String, Object> requestmap) throws InterruptedException, ExecutionException {
		HashMap<String, Object> returnmap = new HashMap<>();

		System.out.print("Inside getAttendance");

		try {
			
			ResponseEntity<?> responseEntity = checkSessionKey(httpSession, Utils.getCookie(request, "JSESSIONID"), "DZFA");
			if (responseEntity != null) { 
				logsServices.addLog(AppConstants.error, AppConstants.ANALYTICS, "attendance",
					AppConstants.sessiontimeout, null);
				return responseEntity; 
			}
			
			  Map<String, String> requestLogMap = new HashMap<>();
			  requestLogMap.put("params", new JSONObject(requestmap).toString());
			 
			
			logsServices.addLog(AppConstants.event, AppConstants.ANALYTICS, "attendance",
					"Attendance API Details", requestLogMap);
			
			
			//validate start
			if (requestmap.containsKey("startDate"))
			{
				//validation
				if( Utils.isEmptyString(requestmap.getOrDefault("startDate", "").toString())) {
					//any one are some of the request parameters is empty
					returnmap.put("status", "error");
					returnmap.put("message", AppConstants.fieldsmissing);

					logsServices.addLog(AppConstants.error, AppConstants.ANALYTICS, "attendance",
							AppConstants.fieldsmissing, null);
					return new ResponseEntity<>(returnmap, HttpStatus.OK);
				}
				else {

					//validation
					if (!Utils.validateDate(requestmap.getOrDefault("startDate", "").toString())) {
						returnmap.put("status", "error");
						returnmap.put("message", AppConstants.errorDatemsg);

						logsServices.addLog(AppConstants.error, AppConstants.ANALYTICS, "attendance",
								AppConstants.errorDatemsg, null);
						return new ResponseEntity<>(returnmap, HttpStatus.OK);
					}
					//Fetch variables after validation
					Date dStartDate = new SimpleDateFormat(AppConstants.RequestDateFormat)
							.parse(requestmap.getOrDefault("startDate", "").toString());

					Date dToday = new Date();

					if(dStartDate.compareTo(dToday) > 0 ) {
						returnmap.put("status", "error");
						returnmap.put("message", AppConstants.errorEndDateOverflow);

						logsServices.addLog(AppConstants.error, AppConstants.ANALYTICS, "attendance",
								AppConstants.errorEndDateOverflow, null);
						return new ResponseEntity<>(returnmap, HttpStatus.OK);
					}

					Date dEndDate = dStartDate; 

					//Fetch all datas according to the format required
					AttendanceAnalysis attendanceAnalysis = getAttendanceAnalytics(dStartDate, dEndDate);

					if(attendanceAnalysis == null) {

						returnmap.put("status", "error");
						returnmap.put("message", AppConstants.errorNoRecordsFound);
						logsServices.addLog(AppConstants.error, AppConstants.ANALYTICS, "attendance",
								AppConstants.errorNoRecordsFound, null);
						return new ResponseEntity<>(returnmap, HttpStatus.OK);


					}else{
						returnmap.put("status", "success");
						returnmap.put("data", "fetched records");
						return new ResponseEntity<>(attendanceAnalysis, HttpStatus.OK);
					}					
				}
			}
			else
			{
				//key does not exist
			}
		}catch(Exception e){
			returnmap.put("status", "error");
			returnmap.put("message", AppConstants.errormsg);

			return new ResponseEntity<>(returnmap, HttpStatus.OK);
		}
		returnmap.put("status", "error");
		returnmap.put("message", AppConstants.errormsg);

		return new ResponseEntity<>(returnmap, HttpStatus.OK);

	}
	@SuppressWarnings("unchecked")
	private AttendanceAnalysis getAttendanceAnalytics(Date startDate, Date endDate) {

		AttendanceAnalysis AttendanceAnalytics = new AttendanceAnalysis();
		String tableName1 = "Staff";
		String tableName2 = "Visitor";

		SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MMM-dd");

		//String Date
		List<Date> listOfDates = getDaysBetweenDates(startDate,endDate);
		for(int i=0;i<listOfDates.size();i++)
		{
			//System.out.println("In Between Dates : " + listOfDates.get(i) );
			////System.out.println("In Between Table Names : " + GetTableName(listOfDates.get(i)) );
			List<String> tableNames1 = (List<String>)analyticsRepository.SelectTableNamesLike(GetTableName(listOfDates.get(i), tableName1));
			List<String> tableNames2 = (List<String>)analyticsRepository.SelectTableNamesLike(GetTableName(listOfDates.get(i), tableName2));

			if(tableNames1 == null && tableNames2 == null) {
				continue;
			}
			//Here find incoming,outgoing, waiting, and storage area items for one day
			//and add them or send all days and fetch bulk
			//we can use procedure or simple query whichever preferred
			//for all workstation or one workstation


			List<AttendanceZoneResult> resultAttendanceZone = null;
			List<AttendancePeopleResult> resultAttendancePeople = null;



			resultAttendanceZone = (List<AttendanceZoneResult>) analyticsRepository.SelectAttendanceZoneAnalytics(tableNames1, tableNames2);
			resultAttendancePeople = (List<AttendancePeopleResult>) analyticsRepository.SelectAttendancePeopleAnalytics(tableNames1, tableNames2);

			if(resultAttendanceZone != null) {
				for(int k=0;k<resultAttendanceZone.size();k++)
				{
					AttendanceZoneResult attendanceZoneResult = resultAttendanceZone.get(k); 

					if(attendanceZoneResult != null)
					{
						AttendanceAnalytics.setTQ(formatter.format(listOfDates.get(i)), attendanceZoneResult.getZoneId(),
								attendanceZoneResult.getZoneName(), attendanceZoneResult.gettQ(), attendanceZoneResult.getNoOfPersons(),
								attendanceZoneResult.getZoneRestrictions(), attendanceZoneResult.getZoneClassification() );
					}
				}
			}
			if(resultAttendancePeople != null) {
				for(int k=0;k<resultAttendancePeople.size();k++)
				{
					AttendancePeopleResult attendancePeopleResult = resultAttendancePeople.get(k); 

					if(attendancePeopleResult != null)
					{

						AttendanceAnalytics.setOrgAttendance(formatter.format(listOfDates.get(i)), attendancePeopleResult.getZoneId(),
								attendancePeopleResult.getZoneName(), attendancePeopleResult.getNoOfPersons(),
								attendancePeopleResult.getStaffcount(), attendancePeopleResult.getVisitorcount(),
								attendancePeopleResult.getVendorcount(), attendancePeopleResult.getContractorcount(), attendancePeopleResult.gettQ());
					}
				}
			}



			//}shift for loop ends here
		}

		if(AttendanceAnalytics.IsRecordAvailable())
			return AttendanceAnalytics;
		else
			return null;

	}
	@RequestMapping(path = { "TestQuery" }, method = RequestMethod.GET)
	public ResponseEntity<?> TestQuery(HttpServletRequest request, HttpSession httpSession,
			@RequestBody HashMap<String, Object> requestmap) throws InterruptedException, ExecutionException {
		HashMap<String, Object> returnmap = new HashMap<>();

		try {
			Date dStartDate = new SimpleDateFormat(AppConstants.RequestDateFormat)
					.parse(requestmap.getOrDefault("startDate", "").toString());
			if (AppConstants.print_log)
				System.out.println("Start Date :" + dStartDate);
			Calendar startDateCalendar = new GregorianCalendar();
			startDateCalendar.setTime(dStartDate);
			startDateCalendar.add(Calendar.DATE, 1);
			dStartDate = startDateCalendar.getTime();
			if (AppConstants.print_log)
				System.out.println("new Start Date :" + dStartDate);

		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		analyticsRepository.SelectUnknownColumns();
		return new ResponseEntity<>(returnmap, HttpStatus.OK);

	}

	@RequestMapping(path = { "WorkStationChartAnalytics" }, method = RequestMethod.POST)
	public ResponseEntity<?> getWorkStationChartAnalytics(HttpServletRequest request, HttpSession httpSession,
			@RequestBody HashMap<String, Object> requestmap) throws InterruptedException, ExecutionException {
		HashMap<String, Object> returnmap = new HashMap<>();

		System.out.print("Inside getWorkStationChartAnalytics");

		try {
			
			ResponseEntity<?> responseEntity = checkSessionKey(httpSession, Utils.getCookie(request, "JSESSIONID"), "WOFA");
			if (responseEntity != null) { 
				logsServices.addLog(AppConstants.error, AppConstants.ANALYTICS, "WorkStationChartAnalytics",
					AppConstants.sessiontimeout, null);
				return responseEntity; 
			}
			
			Map<String, String> requestLogMap = new HashMap<>(); 
			requestLogMap.put("params", new JSONObject(requestmap).toString());
			
			logsServices.addLog(AppConstants.event, AppConstants.ANALYTICS, "WorkStationChartAnalytics",
					"WorkStation Chart Analytics Get API Details", requestLogMap);
			
			//validate start
			if (requestmap.containsKey("WorkStationId") && requestmap.containsKey("startDate") && requestmap.containsKey("endDate"))
			{
				//validation
				if(Utils.isEmptyString(requestmap.getOrDefault("WorkStationId", "").toString())
						|| Utils.isEmptyString(requestmap.getOrDefault("startDate", "").toString())
						|| Utils.isEmptyString(requestmap.getOrDefault("endDate", "").toString())) {
					//any one are some of the request parameters is empty
					returnmap.put("status", "error");
					returnmap.put("message", AppConstants.fieldsmissing);

					logsServices.addLog(AppConstants.error, AppConstants.ANALYTICS, "WorkStationChartAnalytics",
							AppConstants.fieldsmissing, null);
					return new ResponseEntity<>(returnmap, HttpStatus.OK);
				}
				else {

					//validation
					if (!Utils.validateDate(requestmap.getOrDefault("startDate", "").toString())) {
						returnmap.put("status", "error");
						returnmap.put("message", AppConstants.errorDatemsg);

						logsServices.addLog(AppConstants.error, AppConstants.ANALYTICS, "WorkStationChartAnalytics",
								AppConstants.errorDatemsg, null);
						return new ResponseEntity<>(returnmap, HttpStatus.OK);
					}
					if (!Utils.validateDate(requestmap.getOrDefault("endDate", "").toString())) {
						returnmap.put("status", "error");
						returnmap.put("message", AppConstants.errorDatemsg);

						logsServices.addLog(AppConstants.error, AppConstants.ANALYTICS, "WorkStationChartAnalytics",
								AppConstants.errorDatemsg, null);
						return new ResponseEntity<>(returnmap, HttpStatus.OK);
					}
					//Fetch variables after validation
					String sWorkStationId = (String) requestmap.get("WorkStationId");
					Date dStartDate = new SimpleDateFormat(AppConstants.RequestDateFormat)
							.parse(requestmap.getOrDefault("startDate", "").toString());

					Date dEndDate = new SimpleDateFormat(AppConstants.RequestDateFormat)
							.parse(requestmap.getOrDefault("endDate", "").toString());

					if(dStartDate.compareTo(dEndDate) > 0 ) {
						returnmap.put("status", "error");
						returnmap.put("message", AppConstants.errorDatemismatch);

						logsServices.addLog(AppConstants.error, AppConstants.ANALYTICS, "WorkStationChartAnalytics",
								AppConstants.errorDatemismatch, null);
						return new ResponseEntity<>(returnmap, HttpStatus.OK);
					}

					//System.out.println("WorkStationId : " + sWorkStationId);
					//System.out.println("Start Date : " + dStartDate);
					//System.out.println("End date : " + dEndDate);


					//Fetch all datas according to the format required
					WorkStationChartAnalytics workStationChartAnalytics = getWorkStationChartAnalytics(sWorkStationId, dStartDate, dEndDate);

					if(workStationChartAnalytics == null) {

						returnmap.put("status", "error");
						returnmap.put("message", AppConstants.errorNoRecordsFound);
						logsServices.addLog(AppConstants.error, AppConstants.ANALYTICS, "WorkStationChartAnalytics",
								AppConstants.errorNoRecordsFound, null);
						return new ResponseEntity<>(returnmap, HttpStatus.OK);


					}

					returnmap.put("status", "success");
					returnmap.put("data", "fetched records");

					return new ResponseEntity<>(workStationChartAnalytics, HttpStatus.OK);
				}
			}
			else
			{
				//key does not exist
			}
		}catch(Exception e){
			returnmap.put("status", "error");
			returnmap.put("message", AppConstants.errormsg);

			return new ResponseEntity<>(returnmap, HttpStatus.OK);
		}
		returnmap.put("status", "error");
		returnmap.put("message", AppConstants.errormsg);

		return new ResponseEntity<>(returnmap, HttpStatus.OK);

	}
	
	private WorkStationChartAnalytics getWorkStationChartAnalytics(String workStationId, Date startDate, Date endDate) {
		return getWorkStationChartAnalytics(workStationId, startDate, endDate, "Materials" );
	}
	@SuppressWarnings({ "unchecked"})
	private WorkStationChartAnalytics getWorkStationChartAnalytics(String workStationId, Date startDate, Date endDate, String tableName) {

		WorkStationChartAnalytics workStationChartAnalytics = new WorkStationChartAnalytics();


		SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MMM-dd");

		/*String analyticsTimeDuration = formatter.format(startDate) + " to " + formatter.format(endDate);

		workStationChartAnalytics.setDate(analyticsTimeDuration);*/
		//String Date
		List<Date> listOfDates = getDaysBetweenDates(startDate,endDate);
		for(int i=0;i<listOfDates.size();i++)
		{
			
			List<String> tableNames = (List<String>)analyticsRepository.SelectTableNamesLike(GetTableName(listOfDates.get(i), tableName));
			if(tableNames != null)
			{
				List<WorkStationChartAnalyticsResult> chartAnalyticsResult = null;
				

				if(workStationId.equalsIgnoreCase("All")) { 
					chartAnalyticsResult = (List<WorkStationChartAnalyticsResult>) analyticsRepository.SelectAllWorkstationChartAnalytics(tableNames);
				}
				else {
					chartAnalyticsResult = (List<WorkStationChartAnalyticsResult>) analyticsRepository.SelectWorkstationChartAnalyticsByWorkStationId(tableNames, workStationId);
				}





				if(chartAnalyticsResult != null) {
					for(int k=0;k<chartAnalyticsResult.size();k++)
					{
						WorkStationChartAnalyticsResult fetchedWorkStationChartAnalyticsResult = chartAnalyticsResult.get(k); 

						if(fetchedWorkStationChartAnalyticsResult != null)
						{

							workStationChartAnalytics.setWorkStationChartData(fetchedWorkStationChartAnalyticsResult.getWorkStationId(),
									fetchedWorkStationChartAnalyticsResult.getWi(), fetchedWorkStationChartAnalyticsResult.getWo(),
									fetchedWorkStationChartAnalyticsResult.getPr(), fetchedWorkStationChartAnalyticsResult.getTot(),
									fetchedWorkStationChartAnalyticsResult.getWorkStationName(), fetchedWorkStationChartAnalyticsResult.getTq());  
						}
					}
				}
			}
		}
		if(workStationChartAnalytics.IsRecordAvailable())
			return workStationChartAnalytics;
		else
			return null;

	}

}


