package com.entappia.ei4o.controllers;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.concurrent.ExecutionException;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.entappia.ei4o.constants.AppConstants;
import com.entappia.ei4o.dbmodels.PathSegments;
import com.entappia.ei4o.dbmodels.ZoneClassification;
import com.entappia.ei4o.repository.ZoneClassificationRepository;
import com.entappia.ei4o.services.LogsServices;
import com.entappia.ei4o.utils.BaseController;
import com.entappia.ei4o.utils.Utils;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

@RestController
public class ZoneClassificationController extends BaseController {
	@Autowired
	ZoneClassificationRepository zoneClassificationRepository;
	@Autowired
	private LogsServices logsServices;
	
	Gson gson = new Gson();
	Type mapType = new TypeToken<ZoneClassification>() {
	}.getType();

	@RequestMapping(path = { "zoneClassification", "zoneClassification/{className}" }, method = RequestMethod.GET)
	public ResponseEntity<?> getZoneClassification(HttpServletRequest request, HttpSession httpSession,
			@PathVariable Optional<String> className) throws InterruptedException, ExecutionException {
		Map<String, Object> returnmap = new HashMap<>(); 
		try {
			//, "CCLA"
			ResponseEntity<?> responseEntity = checkSessionKey(httpSession, Utils.getCookie(request, "JSESSIONID"));
			if (responseEntity != null) { 
				logsServices.addLog(AppConstants.error, AppConstants.zoneClassification, "getZoneClassification",
						AppConstants.sessiontimeout, null);
				return responseEntity; 
			}
			
			JSONObject jsonObject = new JSONObject();
			if (className.isPresent()) {
				jsonObject.put("className", className.get());
			} else {
				jsonObject.put("className", "all");
			}
			Map<String, String> reuestMap = new HashMap<>();
			reuestMap.put("params", jsonObject.toString()); 
			
			
			logsServices.addLog(AppConstants.event, AppConstants.zoneClassification, "getZoneClassification",
					"Zone Classification get api call", reuestMap);
			  

			List<ZoneClassification> zoneClassificationsDetails = new ArrayList<>();

			if (className.isPresent()) {
				ZoneClassification zoneClassification = zoneClassificationRepository.findByClassName(className.get());
				if (zoneClassification == null) {
					returnmap.put("status", "error");
					returnmap.put("message", "Zone Classification name does not exist");
					logsServices.addLog(AppConstants.error, AppConstants.zoneClassification, "getZoneClassification",
							"Zone Classification name does not exist", null);

					return new ResponseEntity<>(returnmap, HttpStatus.OK);
				}

				zoneClassificationsDetails.add(zoneClassification);
			} else {
				zoneClassificationsDetails = zoneClassificationRepository.findAll();
			}
			logsServices.addLog(AppConstants.success, AppConstants.zoneClassification, "getZoneClassification",
					"ZoneClassification details", null);

			returnmap.put("status", "success");
			returnmap.put("data", zoneClassificationsDetails);

			return new ResponseEntity<>(returnmap, HttpStatus.OK);
		} catch (Exception e) {
			returnmap.put("status", "error");
			returnmap.put("message", AppConstants.errormsg);
			logsServices.addLog(AppConstants.error, AppConstants.zoneClassification, "getZoneClassification",
					AppConstants.errormsg, null);

			return new ResponseEntity<>(returnmap, HttpStatus.OK);
		}

	}

	@PostMapping(value = "/zoneClassification")
	public ResponseEntity<?> AddZoneClassification(HttpServletRequest request, HttpSession httpSession,

			@RequestBody ZoneClassification zoneClassification) throws InterruptedException, ExecutionException {
		Map<String, String> returnmap = new HashMap<>(); 
		try {
			ResponseEntity<?> responseEntity = checkSessionKey(httpSession, Utils.getCookie(request, "JSESSIONID"), "CCLA");
			if (responseEntity != null) { 
				logsServices.addLog(AppConstants.error, AppConstants.zoneClassification, "AddzoneClassification",
						AppConstants.sessiontimeout, null);
				return responseEntity; 
			}
			
			Map<String, String> reuestMap = new HashMap<>();
			reuestMap.put("params", gson.toJson(zoneClassification, mapType));
			 
			logsServices.addLog(AppConstants.event, AppConstants.zoneClassification, "AddzoneClassification",
					"Zone Classification add api call", reuestMap);
			
			HashMap<String, Object> mapTags = new HashMap<>();
			if (zoneClassification != null) {
				mapTags.put("zoneClassification name", zoneClassification.getClassName());
				mapTags.put("zoneClassification color", zoneClassification.getColor());
			}

			if (Utils.isEmptyString(zoneClassification.getClassName())
					|| Utils.isEmptyString(zoneClassification.getColor())) {

				returnmap.put("status", "error");
				returnmap.put("message", AppConstants.fieldsmissing);

				logsServices.addLog(AppConstants.error, AppConstants.zoneClassification, "AddzoneClassification",
						AppConstants.fieldsmissing, null);
				return new ResponseEntity<>(returnmap, HttpStatus.OK);

			}

			ZoneClassification zoneClassification1 = zoneClassificationRepository
					.findByClassName(zoneClassification.getClassName());

			if (zoneClassification1 != null && zoneClassification1.isActive()) {
				returnmap.put("status", "error");
				returnmap.put("message", "zoneClassification name already exist");

				logsServices.addLog(AppConstants.error, AppConstants.zoneClassification, "AddzoneClassification",
						"zone Classification Name already exist", null);
				return new ResponseEntity<>(returnmap, HttpStatus.OK);
			}

			zoneClassification.setCreatedDate(new Date());
			zoneClassification.setModifiedDate(new Date());
			zoneClassification.setActive(true);
			zoneClassificationRepository.save(zoneClassification);

			returnmap.put("status", "success");
			returnmap.put("message", zoneClassification.getClassName() + " added");

			logsServices.addLog(AppConstants.event, AppConstants.zoneClassification, "AddZoneClassification",
					"Zone Classification -" + zoneClassification.getClassName() + " added successfully, ", null);

			return new ResponseEntity<>(returnmap, HttpStatus.OK);
		} catch (Exception e) {
			returnmap.put("status", "error");
			returnmap.put("message", AppConstants.errormsg);

			logsServices.addLog(AppConstants.error, AppConstants.zoneClassification, "AddzoneClassification",
					AppConstants.errormsg, null);
			return new ResponseEntity<>(returnmap, HttpStatus.OK);
		}
	}

	@PutMapping(value = "/zoneClassification")
	public ResponseEntity<?> UpdateZoneClassification(HttpServletRequest request, HttpSession httpSession,

			@RequestBody ZoneClassification zoneClassification) throws InterruptedException, ExecutionException {
		Map<String, String> returnmap = new HashMap<>(); 
		try {
			
			ResponseEntity<?> responseEntity = checkSessionKey(httpSession, Utils.getCookie(request, "JSESSIONID"), "CCLA");
			if (responseEntity != null) { 
				logsServices.addLog(AppConstants.error, AppConstants.zoneClassification, "UpdatezoneClassification",
						AppConstants.sessiontimeout, null);
				return responseEntity; 
			}


			Map<String, String> reuestMap = new HashMap<>();
			reuestMap.put("params", gson.toJson(zoneClassification, mapType));
			
			logsServices.addLog(AppConstants.event, AppConstants.zoneClassification, "UpdatezoneClassification",
					"Zone Classification Update api call", reuestMap);
			
			
			HashMap<String, Object> mapTags = new HashMap<>();

			if (zoneClassification != null) {
				mapTags.put("zoneClassification name", zoneClassification.getClassName());
				mapTags.put("zoneClassification color", zoneClassification.getColor());
				mapTags.put("active", zoneClassification.isActive());

			}

			if (Utils.isEmptyString("" + zoneClassification.getId())
					|| Utils.isEmptyString(zoneClassification.getClassName())
					|| Utils.isEmptyString(zoneClassification.getColor())) {

				returnmap.put("status", "error");
				returnmap.put("message", AppConstants.fieldsmissing);

				logsServices.addLog(AppConstants.error, AppConstants.zoneClassification, "UpdatezoneClassification",
						AppConstants.fieldsmissing, null);
				return new ResponseEntity<>(returnmap, HttpStatus.OK);

			}
			
			ZoneClassification zoneClassification1 = zoneClassificationRepository
					.findById(zoneClassification.getId());

			if (zoneClassification1 == null) {
				returnmap.put("status", "error");
				returnmap.put("message", "zoneClassification id not exist");

				logsServices.addLog(AppConstants.error, AppConstants.zoneClassification, "UpdatezoneClassification",
						"zoneClassification id not exist", null);
				return new ResponseEntity<>(returnmap, HttpStatus.OK);
			}else 	if (!zoneClassification1.isActive()) {
				returnmap.put("status", "error");
				returnmap.put("message", "zoneClassification id is not active");

				logsServices.addLog(AppConstants.error, AppConstants.zoneClassification, "UpdatezoneClassification",
						"zoneClassification id is not active", null);
				return new ResponseEntity<>(returnmap, HttpStatus.OK);
			}
		 
			zoneClassification1.setClassName(zoneClassification.getClassName());
			zoneClassification1.setColor(zoneClassification.getColor());
			zoneClassification1.setModifiedDate(new Date());
			zoneClassificationRepository.save(zoneClassification1);

			returnmap.put("status", "success");
			returnmap.put("message", zoneClassification1.getClassName() + " updated");

			logsServices.addLog(AppConstants.event, AppConstants.zoneClassification, "UpdatezoneClassification",
					"Zone Classification -" + zoneClassification1.getClassName() + " Update successfully, ", null);

			return new ResponseEntity<>(returnmap, HttpStatus.OK);
		} catch (Exception e) {
			returnmap.put("status", "error");
			returnmap.put("message", AppConstants.errormsg);

			logsServices.addLog(AppConstants.error, AppConstants.zoneClassification, "UpdatezoneClassification",
					AppConstants.errormsg, null);
			return new ResponseEntity<>(returnmap, HttpStatus.OK);
		}
	}

	@DeleteMapping(value = "/zoneClassification")
	public ResponseEntity<?> DeleteZoneClassification(HttpServletRequest request, HttpSession httpSession,
			@RequestBody HashMap<String, Object> requestMap) throws InterruptedException, ExecutionException {
		Map<String, String> returnmap = new HashMap<>(); 
		try {
			
			ResponseEntity<?> responseEntity = checkSessionKey(httpSession, Utils.getCookie(request, "JSESSIONID"), "CCLA");
			if (responseEntity != null) { 
				logsServices.addLog(AppConstants.error, AppConstants.zoneClassification, "DeletezoneClassification",
						AppConstants.sessiontimeout, null);
				return responseEntity; 
			} 

			Map<String, String> reuestMap = new HashMap<>();
			reuestMap.put("params", new JSONObject(requestMap).toString()); 
			
			logsServices.addLog(AppConstants.event, AppConstants.zoneClassification, "DeletezoneClassification",
					"Zone Classification delete api call", reuestMap);
			
			HashMap<String, Object> mapTags = new HashMap<>();

			if (!requestMap.containsKey("id")) {

				returnmap.put("status", "error");
				returnmap.put("message", AppConstants.fieldsmissing);

				logsServices.addLog(AppConstants.error, AppConstants.zoneClassification, "DeletezoneClassification",
						AppConstants.fieldsmissing, null);
				return new ResponseEntity<>(returnmap, HttpStatus.OK);

			} else {
				mapTags.put("zoneClassification id", requestMap.getOrDefault("id", "").toString());
				mapTags.put("active", false);
			}

			ZoneClassification zoneClassification = zoneClassificationRepository.findById((int) requestMap.get("id"));

			if (zoneClassification == null) {
				returnmap.put("status", "error");
				returnmap.put("message", "zoneClassification id not exist");

				logsServices.addLog(AppConstants.error, AppConstants.zoneClassification, "DeletezoneClassification",
						"zoneClassification Name not exist", null);
				return new ResponseEntity<>(returnmap, HttpStatus.OK);
			}
			zoneClassification.setModifiedDate(new Date());
			zoneClassification.setActive(false);
			zoneClassificationRepository.save(zoneClassification);

			returnmap.put("status", "success");
			returnmap.put("message", zoneClassification.getClassName() + " Deleted");

			logsServices.addLog(AppConstants.event, AppConstants.zoneClassification, "DeletezoneClassification",
					"Zone Classification -" + zoneClassification.getId() + " Deleted successfully, ", null);

			return new ResponseEntity<>(returnmap, HttpStatus.OK);
		} catch (Exception e) {
			returnmap.put("status", "error");
			returnmap.put("message", AppConstants.errormsg);

			logsServices.addLog(AppConstants.error, AppConstants.zoneClassification, "DeletezoneClassification",
					AppConstants.errormsg, null);
			return new ResponseEntity<>(returnmap, HttpStatus.OK);
		}
	}
}
