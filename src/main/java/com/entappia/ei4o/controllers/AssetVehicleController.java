package com.entappia.ei4o.controllers;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.concurrent.ExecutionException;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.entappia.ei4o.constants.AppConstants;
import com.entappia.ei4o.dbmodels.AssetChrono;
import com.entappia.ei4o.dbmodels.AssetVehicle;
import com.entappia.ei4o.dbmodels.OutlinesDetails;
import com.entappia.ei4o.dbmodels.Tags;
import com.entappia.ei4o.repository.AssetVehicleRepository;
import com.entappia.ei4o.repository.TagsRepository;
import com.entappia.ei4o.services.LogsServices;
import com.entappia.ei4o.utils.BaseController;
import com.entappia.ei4o.utils.Utils;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

@RestController
public class AssetVehicleController extends BaseController {
	
	@Autowired
	AssetVehicleRepository assetVehicleRepository; 

	@Autowired
	private TagsRepository tagsRepository;
	
	@Autowired
	private LogsServices logsServices;
	
	Gson gson = new Gson();
	Type mapType = new TypeToken<AssetVehicle>() {
	}.getType();
	

	@RequestMapping(path = { "assetVehicle", "assetVehicle/{assetId}" }, method = RequestMethod.GET)
	public ResponseEntity<?> getAssetVehicle(HttpServletRequest request, HttpSession httpSession,
			@PathVariable Optional<String> assetId) throws InterruptedException, ExecutionException {
		Map<String, Object> returnmap = new HashMap<>();
		HashMap<String, String> reuestMap = new HashMap<>();
		try {
			
			//"AAVH"
			ResponseEntity<?> responseEntity = checkSessionKey(httpSession, Utils.getCookie(request, "JSESSIONID"));
			if (responseEntity != null) { 
				logsServices.addLog(AppConstants.error, AppConstants.ASSTVCE, "getAssetVehicle",
						AppConstants.sessiontimeout, null);
				return responseEntity; 
			}
			
			JSONObject jsonObject = new JSONObject();
			if (assetId.isPresent()) {
				jsonObject.put("assetVehicleId", assetId.get());
			} else {
				jsonObject.put("assetVehicleId", "all");
			}

			reuestMap.put("params", jsonObject.toString());
			
			logsServices.addLog(AppConstants.event, AppConstants.ASSTVCE, "getAssetVehicle",
					"Asset Vehicle get API Call", reuestMap);
			
			if (assetId.isPresent()) {
				reuestMap.put("assetvehicle id", assetId.get());
			} else {
				reuestMap.put("params", "getallasset");
			}
 
			
			List<Tags> tagsList = tagsRepository.listOfAssignedTagsByType("Vehicles");

			HashMap<String, Tags> tagsMap = new HashMap<>();

			tagsList.forEach(tags -> {
				tagsMap.put(tags.getAssignmentId(), tags);
			});

			List<AssetVehicle> assetVechileDetails = new ArrayList<>();

			if (assetId.isPresent()) {
				AssetVehicle assetVehicle = assetVehicleRepository.findByAssetId(assetId.get());
				if (assetVehicle == null) {
					returnmap.put("status", "error");
					returnmap.put("message", "AssetVehicle id does not exist");
					logsServices.addLog(AppConstants.error, AppConstants.ASSTVCE, "getAssetVehicle",
							"Vehicle id does not exist", null);

					return new ResponseEntity<>(returnmap, HttpStatus.OK);
				}

				assetVechileDetails.add(assetVehicle);
			} else {
				assetVechileDetails = assetVehicleRepository.findAll();
			}
			
			
			List<Map<String, Object>> vehiclesMapDetails = new ArrayList<>();
			
			final ObjectMapper mapper = new ObjectMapper();
			for(AssetVehicle assetVehicle : assetVechileDetails) {
				 Map<String, Object> map = mapper.convertValue(assetVehicle, new TypeReference<Map<String, Object>>() {});
				  Tags tags = tagsMap.get(assetVehicle.getAssetId());
				  if(tags!=null) {
					  map.put("assignedTagMacId", tags.getMacId());
				  }else 
					  map.put("assignedTagMacId", "");
				  
				  vehiclesMapDetails.add(map);
			}
			logsServices.addLog(AppConstants.success, AppConstants.ASSTVCE, "getAssetVehicle", "AssetVehicle details",
					null);

			returnmap.put("status", "success");
			returnmap.put("data", vehiclesMapDetails);
			
			return new ResponseEntity<>(returnmap, HttpStatus.OK);
		} catch (Exception e) {
			returnmap.put("status", "error");
			returnmap.put("message", AppConstants.errormsg);
			logsServices.addLog(AppConstants.error, AppConstants.ASSTVCE, "getAssetVehicle", AppConstants.errormsg,
					null);

			return new ResponseEntity<>(returnmap, HttpStatus.OK);
		}

	}

	@PostMapping(value = "/assetVehicle")
	public ResponseEntity<?> AddAssetVehicle(HttpServletRequest request, HttpSession httpSession,

			@RequestBody AssetVehicle assetVehicle) throws InterruptedException, ExecutionException {
		Map<String, String> returnmap = new HashMap<>(); 
		try {
			
			ResponseEntity<?> responseEntity = checkSessionKey(httpSession, Utils.getCookie(request, "JSESSIONID"), "AAVH");
			if (responseEntity != null) { 
				logsServices.addLog(AppConstants.error, AppConstants.ASSTVCE, "AddassetVehicle",
					AppConstants.sessiontimeout, null);
				return responseEntity; 
			}
			
			Map<String, String> requestLogMap = new HashMap<>(); 
			requestLogMap.put("params", gson.toJson(assetVehicle, mapType));

			
			logsServices.addLog(AppConstants.event, AppConstants.ASSTVCE, "AddassetVehicle",
					"Asset Vehicle add API Call", requestLogMap);
			
			HashMap<String, Object> mapTags = new HashMap<>();
			if (assetVehicle != null) {
				mapTags.put("assetVehicle type", assetVehicle.getAssetType());
				mapTags.put("assetVehicle id", assetVehicle.getAssetId());
				mapTags.put("assetVehicle sub type", assetVehicle.getSubType());

			}

			if (Utils.isEmptyString(assetVehicle.getAssetId()) || Utils.isEmptyString(assetVehicle.getAssetType())
					|| Utils.isEmptyString(assetVehicle.getSubType())) {

				returnmap.put("status", "error");
				returnmap.put("message", AppConstants.fieldsmissing);

				logsServices.addLog(AppConstants.error, AppConstants.ASSTVCE, "AddassetVehicle",
						AppConstants.fieldsmissing, null);
				return new ResponseEntity<>(returnmap, HttpStatus.OK);

			}

			AssetVehicle assetVehicle1 = assetVehicleRepository.findByAssetId(assetVehicle.getAssetId());

			if (assetVehicle1 != null&&assetVehicle1.isActive()) {
				returnmap.put("status", "error");
				returnmap.put("message", "Vehicle id already exist");

				logsServices.addLog(AppConstants.error, AppConstants.ASSTVCE, "AddassetVehicle",
						"Vehicle id already exist", null);
				return new ResponseEntity<>(returnmap, HttpStatus.OK);
			}

			assetVehicle.setCreatedDate(new Date());
			assetVehicle.setModifiedDate(new Date());
			assetVehicle.setActive(true);
			assetVehicleRepository.save(assetVehicle);

			returnmap.put("status", "success");
			returnmap.put("message", assetVehicle.getAssetId() + " added successfully");

			logsServices.addLog(AppConstants.event, AppConstants.ASSTVCE, "AddTag",
					"Vehicle -" + assetVehicle.getAssetId() + " added successfully, ", null);

			return new ResponseEntity<>(returnmap, HttpStatus.OK);
		} catch (Exception e) {
			returnmap.put("status", "error");
			returnmap.put("message", AppConstants.errormsg);

			logsServices.addLog(AppConstants.error, AppConstants.ASSTVCE, "AddassetVehicle", AppConstants.errormsg,
					null);
			return new ResponseEntity<>(returnmap, HttpStatus.OK);
		}
	}

	@PutMapping(value = "/assetVehicle")
	public ResponseEntity<?> UpdateAssetVehicle(HttpServletRequest request, HttpSession httpSession,

			@RequestBody AssetVehicle assetVehicle) throws InterruptedException, ExecutionException {
		Map<String, String> returnmap = new HashMap<>(); 
		try {
			
			ResponseEntity<?> responseEntity = checkSessionKey(httpSession, Utils.getCookie(request, "JSESSIONID"), "AAVH");
			if (responseEntity != null) { 
				logsServices.addLog(AppConstants.error, AppConstants.ASSTVCE, "UpdateassetVehicle",
						AppConstants.sessiontimeout, null);
				return responseEntity; 
			}
			
			Map<String, String> requestLogMap = new HashMap<>(); 
			requestLogMap.put("params", gson.toJson(assetVehicle, mapType));
			
			logsServices.addLog(AppConstants.event, AppConstants.ASSTVCE, "UpdateassetVehicle",
					"Asset Vehicle Update asset API Call", requestLogMap);
			
			HashMap<String, Object> mapTags = new HashMap<>();
			if (assetVehicle != null) {
				mapTags.put("assetVehicle type", assetVehicle.getAssetType());
				mapTags.put("assetVehicle id", assetVehicle.getAssetId());
				mapTags.put("assetVehicle sub type", assetVehicle.getSubType());

			}

			if (Utils.isEmptyString(assetVehicle.getAssetId()) || Utils.isEmptyString(assetVehicle.getAssetType())
					|| Utils.isEmptyString(assetVehicle.getSubType())) {

				returnmap.put("status", "error");
				returnmap.put("message", AppConstants.fieldsmissing);

				logsServices.addLog(AppConstants.error, AppConstants.ASSTVCE, "UpdateassetVehicle",
						AppConstants.fieldsmissing, null);
				return new ResponseEntity<>(returnmap, HttpStatus.OK);

			}

			AssetVehicle assetVehicle1 = assetVehicleRepository.findByAssetId(assetVehicle.getAssetId());

			if (assetVehicle1 == null) {
				returnmap.put("status", "error");
				returnmap.put("message", "Vehicle id not exist");

				logsServices.addLog(AppConstants.error, AppConstants.ASSTVCE, "UpdateassetVehicle",
						"Vehicle id not exist", null);
				return new ResponseEntity<>(returnmap, HttpStatus.OK);
			}
			assetVehicle1.setAssetType(assetVehicle.getAssetType());
			assetVehicle1.setSubType(assetVehicle.getSubType());
			assetVehicle1.setModifiedDate(new Date());
			assetVehicleRepository.save(assetVehicle1);

			returnmap.put("status", "success");
			returnmap.put("message", assetVehicle.getAssetId() + " updated successfully");

			logsServices.addLog(AppConstants.event, AppConstants.ASSTVCE, "UpdateAssetVehicle",
					"Vehicle -" + assetVehicle.getAssetId() + " Updated successfully, ", null);

			return new ResponseEntity<>(returnmap, HttpStatus.OK);
		} catch (Exception e) {
			returnmap.put("status", "error");
			returnmap.put("message", AppConstants.errormsg);

			logsServices.addLog(AppConstants.error, AppConstants.ASSTVCE, "UpdateassetVehicle", AppConstants.errormsg,
					null);
			return new ResponseEntity<>(returnmap, HttpStatus.OK);
		}
	}

	@DeleteMapping(value = "/assetVehicle")
	public ResponseEntity<?> DeleteAssetVehicle(HttpServletRequest request, HttpSession httpSession, 
			@RequestBody HashMap<String, Object> requestMap)
			throws InterruptedException, ExecutionException {
		Map<String, String> returnmap = new HashMap<>(); 
		try {
			
			ResponseEntity<?> responseEntity = checkSessionKey(httpSession, Utils.getCookie(request, "JSESSIONID"), "AAVH");
			if (responseEntity != null) { 
				logsServices.addLog(AppConstants.error, AppConstants.ASSTVCE, "DeleteassetVehicle",
						AppConstants.sessiontimeout, null);
				return responseEntity; 
			}
			 
			Map<String, String> requestLogMap = new HashMap<>(); 
			requestLogMap.put("params", new JSONObject(requestMap).toString());
			
			logsServices.addLog(AppConstants.event, AppConstants.ASSTVCE, "DeleteassetVehicle",
					"Asset Vehicle delete asset API Call", requestLogMap);
			
			HashMap<String, Object> mapTags = new HashMap<>();

			if (Utils.isEmptyString(requestMap.getOrDefault("assetId", "").toString())) {

				returnmap.put("status", "error");
				returnmap.put("message", AppConstants.fieldsmissing);

				logsServices.addLog(AppConstants.error, AppConstants.ASSTVCE, "DeleteassetVehicle",
						AppConstants.fieldsmissing, null);
				return new ResponseEntity<>(returnmap, HttpStatus.OK);

			} else {
				mapTags.put("assetVehicle id", requestMap.getOrDefault("assetId", "").toString());
				mapTags.put("active", false);
			}

			AssetVehicle assetVehicle1 = assetVehicleRepository.findByAssetId(requestMap.getOrDefault("assetId", "").toString());

			if (assetVehicle1 == null) {
				returnmap.put("status", "error");
				returnmap.put("message", "Vehicle id not exist");

				logsServices.addLog(AppConstants.error, AppConstants.ASSTVCE, "DeleteassetVehicle",
						"AssetVehicle id not exist", null);
				return new ResponseEntity<>(returnmap, HttpStatus.OK);
			}
			assetVehicle1.setModifiedDate(new Date());
			assetVehicle1.setActive(false);
			assetVehicleRepository.save(assetVehicle1);

			returnmap.put("status", "success");
			returnmap.put("message", assetVehicle1.getAssetId() + " deleted successfully");

			logsServices.addLog(AppConstants.event, AppConstants.ASSTVCE, "DeleteAssetVehicle",
					"AssetVehicle -" + assetVehicle1.getAssetId() + " Deleted successfully, ", null);

			return new ResponseEntity<>(returnmap, HttpStatus.OK);
		} catch (Exception e) {
			returnmap.put("status", "error");
			returnmap.put("message", AppConstants.errormsg);

			logsServices.addLog(AppConstants.error, AppConstants.ASSTVCE, "DeleteassetVehicle", AppConstants.errormsg,
					null);
			return new ResponseEntity<>(returnmap, HttpStatus.OK);
		}
	}
}
