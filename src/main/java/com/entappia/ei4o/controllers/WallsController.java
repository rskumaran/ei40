package com.entappia.ei4o.controllers;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.concurrent.ExecutionException;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.entappia.ei4o.constants.AppConstants;
import com.entappia.ei4o.dbmodels.OutlinesDetails;
import com.entappia.ei4o.dbmodels.Walls;
import com.entappia.ei4o.repository.OutLineRepository;
import com.entappia.ei4o.repository.WallsRepository;
import com.entappia.ei4o.services.LogsServices;
import com.entappia.ei4o.utils.BaseController;
import com.entappia.ei4o.utils.Utils;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

@RestController
public class WallsController extends BaseController {

	@Autowired
	private LogsServices logsServices;

	@Autowired
	private OutLineRepository outLineRepository;

	@Autowired
	private WallsRepository wallsRepository;

	@RequestMapping(path = { "wallsDetails", "wallsDetails/{outlinesId}" }, method = RequestMethod.GET)
	public ResponseEntity<?> getWallsDetails(HttpServletRequest request, HttpSession httpSession,
			@PathVariable Optional<Long> outlinesId) throws InterruptedException, ExecutionException {
		Map<String, Object> returnmap = new HashMap<>();
		try {

			ResponseEntity<?> responseEntity = checkSessionKey(httpSession, Utils.getCookie(request, "JSESSIONID"));
			if (responseEntity != null) { 
				logsServices.addLog(AppConstants.error, AppConstants.WALLS, "getWallsDetails",
						AppConstants.sessiontimeout, null);
				return responseEntity; 
			}
			
			
			JSONObject jsonObject = new JSONObject();
			if (outlinesId.isPresent()) {
				jsonObject.put("outlinesId", outlinesId.get());
			} else {
				jsonObject.put("outlinesId", "all");
			}
			Map<String, String> reuestMap = new HashMap<>();
			reuestMap.put("params", jsonObject.toString()); 
 
			
			logsServices.addLog(AppConstants.event, AppConstants.WALLS, "getWallsDetails",
					"Get Walls Details api call" , reuestMap);
			
			List<Walls> wallsDetailsList = new ArrayList<>();

			if (outlinesId.isPresent()) {
				wallsDetailsList = wallsRepository.findByOutlinesId(outlinesId.get());
				if (wallsDetailsList == null) {
					returnmap.put("status", "error");
					returnmap.put("message", "Walls Details does not exist");
					logsServices.addLog(AppConstants.error, AppConstants.WALLS, "getWallsDetails",
							returnmap.get("message").toString(), null);

					return new ResponseEntity<>(returnmap, HttpStatus.OK);
				}

			} else {
				wallsDetailsList = wallsRepository.findAllActiveWalls();
				if (wallsDetailsList == null) {
					returnmap.put("status", "error");
					returnmap.put("message", "Walls Details does not exist");
					logsServices.addLog(AppConstants.error, AppConstants.WALLS, "getWallsDetails",
							returnmap.get("message").toString(), null);

					return new ResponseEntity<>(returnmap, HttpStatus.OK);
				}
			}
			logsServices.addLog(AppConstants.success, AppConstants.WALLS, "getWallsDetails", "Walls details", null);

			returnmap.put("status", "success");
			returnmap.put("data", wallsDetailsList);

			return new ResponseEntity<>(returnmap, HttpStatus.OK);
		} catch (Exception e) {
			returnmap.put("status", "error");
			returnmap.put("message", AppConstants.errormsg);
			logsServices.addLog(AppConstants.error, AppConstants.WALLS, "getWallsDetails", AppConstants.errormsg, null);

			return new ResponseEntity<>(returnmap, HttpStatus.OK);
		}

	}

	@PostMapping(value = "/wallsDetails")
	public ResponseEntity<?> addWallsDetails(HttpServletRequest request, HttpSession httpSession,

			@RequestBody HashMap<String, Object> requestMap) throws InterruptedException, ExecutionException {
		Map<String, String> returnmap = new HashMap<>();

		try {
			
			ResponseEntity<?> responseEntity = checkSessionKey(httpSession, Utils.getCookie(request, "JSESSIONID"), "CTPE");
			if (responseEntity != null) { 
				logsServices.addLog(AppConstants.error, AppConstants.WALLS, "addWallsDetails",
						AppConstants.sessiontimeout, null);
				return responseEntity; 
			}
			 
			 
			Map<String, String> reuestMap = new HashMap<>();
			reuestMap.put("params", new JSONObject(requestMap).toString()); 
			
			logsServices.addLog(AppConstants.event, AppConstants.WALLS, "addWallsDetails",
					"Add Walls Details api call" , reuestMap);

			if (Utils.isEmptyString(requestMap.getOrDefault("outlinesId", "").toString())
					|| Utils.isEmptyString(requestMap.getOrDefault("name", "").toString())
					|| Utils.isEmptyString(requestMap.getOrDefault("colorCode", "").toString())
					|| Utils.isEmptyString(requestMap.getOrDefault("geometryType", "").toString())
					|| Utils.isEmptyString(requestMap.getOrDefault("wallType", "").toString()) 
					|| Utils.isEmptyString(requestMap.getOrDefault("coordinates", "").toString())) {

				returnmap.put("status", "error");
				returnmap.put("message", AppConstants.fieldsmissing);

				logsServices.addLog(AppConstants.error, AppConstants.WALLS, "addWallsDetails",
						AppConstants.fieldsmissing, null);
				return new ResponseEntity<>(returnmap, HttpStatus.OK);

			}

			OutlinesDetails outlinesDetails = outLineRepository
					.findByOutlinesId(Long.valueOf(requestMap.getOrDefault("outlinesId", "").toString()));
			if (outlinesDetails == null) {
				returnmap.put("status", "error");
				returnmap.put("message", "Outlines id not found");

				logsServices.addLog(AppConstants.error, AppConstants.WALLS, "addWallsDetails",
						returnmap.get("message").toString(), null);
				return new ResponseEntity<>(returnmap, HttpStatus.OK);
			} else if (!outlinesDetails.isActive()) {
				returnmap.put("status", "error");
				returnmap.put("message", "Outlines Details is not active");

				logsServices.addLog(AppConstants.error, AppConstants.WALLS, "addWallsDetails",
						returnmap.get("message").toString(), null);
				return new ResponseEntity<>(returnmap, HttpStatus.OK);
			}


			Gson gson = new Gson();
			Type mapType = new TypeToken<List<HashMap<String, String>>>() {
			}.getType();

			List<HashMap<String, String>> coordinateList = gson
					.fromJson(requestMap.getOrDefault("coordinates", "").toString(), mapType);
			Walls walls = new Walls();
			walls.setName(requestMap.getOrDefault("name", "").toString());
			walls.setOutlinesDetails(outlinesDetails);
			walls.setColorCode(requestMap.getOrDefault("colorCode", "").toString());
			walls.setGeometryType(requestMap.getOrDefault("geometryType", "").toString());
			walls.setWallType(requestMap.getOrDefault("wallType", "").toString());
			walls.setCoordinates(coordinateList);
			walls.setActive(true);
			walls.setCreatedDate(new Date());
			walls.setModifiedDate(new Date());
			wallsRepository.save(walls);

			returnmap.put("status", "success");
			returnmap.put("message", walls.getName() + " added successfully");

			logsServices.addLog(AppConstants.event, AppConstants.WALLS, "addWallsDetails",
					"Walls -" + walls.getName() + " added successfully, ", null);

			return new ResponseEntity<>(returnmap, HttpStatus.OK);
		} catch (Exception e) {
			returnmap.put("status", "error");
			returnmap.put("message", AppConstants.errormsg);

			logsServices.addLog(AppConstants.error, AppConstants.WALLS, "addWallsDetails", AppConstants.errormsg, null);
			return new ResponseEntity<>(returnmap, HttpStatus.OK);
		}
	}

	@PutMapping(value = "/wallsDetails")
	public ResponseEntity<?> updateWallsDetails(HttpServletRequest request, HttpSession httpSession,

			@RequestBody HashMap<String, Object> requestMap) throws InterruptedException, ExecutionException {
		Map<String, String> returnmap = new HashMap<>();

		try {
			
			ResponseEntity<?> responseEntity = checkSessionKey(httpSession, Utils.getCookie(request, "JSESSIONID"), "CTPE");
			if (responseEntity != null) { 
				logsServices.addLog(AppConstants.error, AppConstants.WALLS, "updateWallsDetails",
						AppConstants.sessiontimeout, null);
				return responseEntity; 
			}
			 
			 
			Map<String, String> reuestMap = new HashMap<>();
			reuestMap.put("params", new JSONObject(requestMap).toString()); 

			logsServices.addLog(AppConstants.event, AppConstants.WALLS, "updateWallsDetails",
					"Update Walls Details api call" , reuestMap);

			if (Utils.isEmptyString(requestMap.getOrDefault("id", "").toString())
					||Utils.isEmptyString(requestMap.getOrDefault("outlinesId", "").toString())
					|| Utils.isEmptyString(requestMap.getOrDefault("name", "").toString())
					|| Utils.isEmptyString(requestMap.getOrDefault("colorCode", "").toString())
					|| Utils.isEmptyString(requestMap.getOrDefault("geometryType", "").toString())
					|| Utils.isEmptyString(requestMap.getOrDefault("wallType", "").toString()) 
					|| Utils.isEmptyString(requestMap.getOrDefault("coordinates", "").toString())) {

				returnmap.put("status", "error");
				returnmap.put("message", AppConstants.fieldsmissing);

				logsServices.addLog(AppConstants.error, AppConstants.WALLS, "updateWallsDetails", AppConstants.fieldsmissing,
						null);
				return new ResponseEntity<>(returnmap, HttpStatus.OK);

			}

			OutlinesDetails outlinesDetails = outLineRepository
					.findByOutlinesId(Long.valueOf(requestMap.getOrDefault("outlinesId", "").toString()));
			if (outlinesDetails == null) {
				returnmap.put("status", "error");
				returnmap.put("message", "Outlines id not found");

				logsServices.addLog(AppConstants.error, AppConstants.WALLS, "updateWallsDetails",
						returnmap.get("message").toString(), null);
				return new ResponseEntity<>(returnmap, HttpStatus.OK);
			}else if (!outlinesDetails.isActive()) {
				returnmap.put("status", "error");
				returnmap.put("message", "Outlines Details is not active");

				logsServices.addLog(AppConstants.error, AppConstants.WALLS, "updateWallsDetails",
						returnmap.get("message").toString(), null);
				return new ResponseEntity<>(returnmap, HttpStatus.OK);
			}

		 
			Walls walls = wallsRepository.findAllById(Long.valueOf(requestMap.getOrDefault("id", "").toString()));
			
			if (walls == null) {
				returnmap.put("status", "error");
				returnmap.put("message", "Walls id not found");

				logsServices.addLog(AppConstants.error, AppConstants.WALLS, "updateWallsDetails",
						returnmap.get("message").toString(), null);
				return new ResponseEntity<>(returnmap, HttpStatus.OK);
			}else if (!walls.isActive()) {
				returnmap.put("status", "error");
				returnmap.put("message", "Walls is not active");

				logsServices.addLog(AppConstants.error, AppConstants.WALLS, "updateWallsDetails",
						returnmap.get("message").toString(), null);
				return new ResponseEntity<>(returnmap, HttpStatus.OK);
			}
			
			Gson gson = new Gson();
			Type mapType = new TypeToken<List<HashMap<String, String>>>() {
			}.getType();

			List<HashMap<String, String>> coordinateList = gson
					.fromJson(requestMap.getOrDefault("coordinates", "").toString(), mapType);	
			
			walls.setName(requestMap.getOrDefault("name", "").toString());
			walls.setOutlinesDetails(outlinesDetails); 
			walls.setColorCode(requestMap.getOrDefault("colorCode", "").toString());
			walls.setGeometryType(requestMap.getOrDefault("geometryType", "").toString());
			walls.setWallType(requestMap.getOrDefault("wallType", "").toString());
			walls.setCoordinates(coordinateList); 
			walls.setModifiedDate(new Date());
			wallsRepository.save(walls);

			returnmap.put("status", "success");
			returnmap.put("message", walls.getName() + " updated successfully");

			logsServices.addLog(AppConstants.event, AppConstants.WALLS, "updateWallsDetails",
					"Walls -" + walls.getName() + " updated successfully, ", null);

			return new ResponseEntity<>(returnmap, HttpStatus.OK);
		} catch (Exception e) {
			returnmap.put("status", "error");
			returnmap.put("message", AppConstants.errormsg);

			logsServices.addLog(AppConstants.error, AppConstants.WALLS, "updateWallsDetails", AppConstants.errormsg, null);
			return new ResponseEntity<>(returnmap, HttpStatus.OK);
		}
	}
	
	
	@DeleteMapping(value = "/wallsDetails")
	public ResponseEntity<?> deleteWallsDetails(HttpServletRequest request, HttpSession httpSession,

			@RequestBody HashMap<String, Object> requestMap) throws InterruptedException, ExecutionException {
		Map<String, String> returnmap = new HashMap<>();

		try {

			ResponseEntity<?> responseEntity = checkSessionKey(httpSession, Utils.getCookie(request, "JSESSIONID"), "CTPE");
			if (responseEntity != null) { 
				logsServices.addLog(AppConstants.error, AppConstants.WALLS, "deleteWallsDetails",
						AppConstants.sessiontimeout, null);
				return responseEntity; 
			}
			 
			 
			Map<String, String> reuestMap = new HashMap<>();
			reuestMap.put("params", new JSONObject(requestMap).toString()); 
			
			logsServices.addLog(AppConstants.event, AppConstants.WALLS, "deleteWallsDetails",
					"Delete Walls Details api call" , reuestMap);
			
			if (Utils.isEmptyString(requestMap.getOrDefault("id", "").toString()) ) {

				returnmap.put("status", "error");
				returnmap.put("message", AppConstants.fieldsmissing);

				logsServices.addLog(AppConstants.error, AppConstants.WALLS, "deleteWallsDetails", AppConstants.fieldsmissing,
						null);
				return new ResponseEntity<>(returnmap, HttpStatus.OK);

			}
 
			Walls walls = wallsRepository.findAllById(Long.valueOf(requestMap.getOrDefault("id", "").toString()));
			
			if (walls == null) {
				returnmap.put("status", "error");
				returnmap.put("message", "Walls id not found");

				logsServices.addLog(AppConstants.error, AppConstants.WALLS, "deleteWallsDetails",
						returnmap.get("message").toString(), null);
				return new ResponseEntity<>(returnmap, HttpStatus.OK);
			}else if (!walls.isActive()) {
				returnmap.put("status", "error");
				returnmap.put("message", "Walls is not active");

				logsServices.addLog(AppConstants.error, AppConstants.WALLS, "deleteWallsDetails",
						returnmap.get("message").toString(), null);
				return new ResponseEntity<>(returnmap, HttpStatus.OK);
			}
			
 			 
			walls.setActive(false);
			walls.setModifiedDate(new Date());
			wallsRepository.save(walls);

			returnmap.put("status", "success");
			returnmap.put("message", walls.getName() + " status updated successfully");

			logsServices.addLog(AppConstants.event, AppConstants.WALLS, "deleteWallsDetails",
					"Walls -" + walls.getName() + " status updated successfully, ", null);

			return new ResponseEntity<>(returnmap, HttpStatus.OK);
		} catch (Exception e) {
			returnmap.put("status", "error");
			returnmap.put("message", AppConstants.errormsg);

			logsServices.addLog(AppConstants.error, AppConstants.WALLS, "deleteWallsDetails", AppConstants.errormsg, null);
			return new ResponseEntity<>(returnmap, HttpStatus.OK);
		}
	}

}
