package com.entappia.ei4o.controllers;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.concurrent.ExecutionException;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.entappia.ei4o.constants.AppConstants;
import com.entappia.ei4o.dbmodels.CampusDetails;
import com.entappia.ei4o.dbmodels.Department;
import com.entappia.ei4o.dbmodels.OutlinesDetails;
import com.entappia.ei4o.repository.CampusDetailsRepository;
import com.entappia.ei4o.repository.OutLineRepository;
import com.entappia.ei4o.services.LogsServices;
import com.entappia.ei4o.utils.BaseController;
import com.entappia.ei4o.utils.Utils;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

@RestController
public class OutLineController extends BaseController {

	@Autowired
	private LogsServices logsServices;

	@Autowired
	private OutLineRepository outLineRepository;

	@Autowired
	private CampusDetailsRepository campusDetailsRepository;
	

	Gson gson = new Gson();
	Type mapType = new TypeToken<OutlinesDetails>() {
	}.getType();
	

	@RequestMapping(path = { "outlineDetails/{campusId}" }, method = RequestMethod.GET)
	public ResponseEntity<?> getOutlineDetails(HttpServletRequest request, HttpSession httpSession,
			@PathVariable Optional<Long> campusId) throws InterruptedException, ExecutionException {
		Map<String, Object> returnmap = new HashMap<>(); 
		try { 

			Map<String, String> reuestMap = new HashMap<>();
			ResponseEntity<?> responseEntity = checkSessionKey(httpSession, Utils.getCookie(request, "JSESSIONID"));
			if (responseEntity != null) { 
				logsServices.addLog(AppConstants.error, AppConstants.OUTLINE, "getOutlineDetails",
						AppConstants.sessiontimeout, null);
				return responseEntity; 
			}
			
			
			JSONObject jsonObject = new JSONObject();
			if (campusId.isPresent()) {
				jsonObject.put("campusId", campusId.get());
			} else {
				jsonObject.put("campusId", "all");
			}

			reuestMap.put("params", jsonObject.toString()); 

			logsServices.addLog(AppConstants.event, AppConstants.OUTLINE, "getOutlineDetails",
					"Outline Details Get API Details", reuestMap);
			
			List<OutlinesDetails> outlinesDetailsList = new ArrayList<>();

			if (campusId.isPresent()) {
				outlinesDetailsList = outLineRepository.findByCampusId(campusId.get());
				if (outlinesDetailsList == null) {
					returnmap.put("status", "error");
					returnmap.put("message", "Outlines Details does not exist");
					logsServices.addLog(AppConstants.error, AppConstants.OUTLINE, "getOutlineDetails",
							"Outlines Details does not exist", null);

					return new ResponseEntity<>(returnmap, HttpStatus.OK);
				}

			} else {
				returnmap.put("status", "error");
				returnmap.put("message", "Campus id is missing");
				logsServices.addLog(AppConstants.error, AppConstants.OUTLINE, "getOutlineDetails",
						"Campus id is missing", null);

				return new ResponseEntity<>(returnmap, HttpStatus.OK);
			}
			logsServices.addLog(AppConstants.success, AppConstants.OUTLINE, "getOutlineDetails", "Work Station details",
					null);

			returnmap.put("status", "success");
			returnmap.put("data", outlinesDetailsList);

			return new ResponseEntity<>(returnmap, HttpStatus.OK);
		} catch (Exception e) {
			returnmap.put("status", "error");
			returnmap.put("message", AppConstants.errormsg);
			logsServices.addLog(AppConstants.error, AppConstants.OUTLINE, "getOutlineDetails", AppConstants.errormsg,
					null);

			return new ResponseEntity<>(returnmap, HttpStatus.OK);
		}

	}

	@PostMapping(value = "/outlineDetails/{campusId}")
	public ResponseEntity<?> addOutlineDetails(HttpServletRequest request, HttpSession httpSession,

			@PathVariable Optional<Long> campusId, @RequestBody OutlinesDetails outlinesDetail)
			throws InterruptedException, ExecutionException {
		Map<String, String> returnmap = new HashMap<>();

		try {
			 
			ResponseEntity<?> responseEntity = checkSessionKey(httpSession, Utils.getCookie(request, "JSESSIONID"), "CTPE");
			if (responseEntity != null) { 
				logsServices.addLog(AppConstants.error, AppConstants.OUTLINE, "addOutlineDetails",
						AppConstants.sessiontimeout, null);
				return responseEntity; 
			}
			
			Map<String, String> reuestMap = new HashMap<>();
			reuestMap.put("params", gson.toJson(outlinesDetail, mapType));


			logsServices.addLog(AppConstants.event, AppConstants.OUTLINE, "addOutlineDetails",
					"Outline Details add API Details", reuestMap);

			if (!campusId.isPresent() || Utils.isEmptyString(outlinesDetail.getName())
					|| Utils.isEmptyString(outlinesDetail.getPlaceType())
					|| Utils.isEmptyString(outlinesDetail.getWallType())
					|| Utils.isEmptyString(outlinesDetail.getOutlinesArea())  
					|| Utils.isEmptyString(outlinesDetail.getBgColor())
							|| Utils.isEmptyString(outlinesDetail.getGeometryType())
					|| outlinesDetail.getCoordinateList() == null) {

				returnmap.put("status", "error");
				returnmap.put("message", AppConstants.fieldsmissing);

				logsServices.addLog(AppConstants.error, AppConstants.OUTLINE, "addOutlineDetails",
						AppConstants.fieldsmissing, null);
				return new ResponseEntity<>(returnmap, HttpStatus.OK);

			}

			CampusDetails campusDetails = campusDetailsRepository.findByCampusId(campusId.get());
			if (campusDetails == null) {
				returnmap.put("status", "error");
				returnmap.put("message", "Campus id not found");

				logsServices.addLog(AppConstants.error, AppConstants.OUTLINE, "addOutlineDetails",
						"Campus id not found", null);
				return new ResponseEntity<>(returnmap, HttpStatus.OK);
			} else if (!campusDetails.isActive()) {
				returnmap.put("status", "error");
				returnmap.put("message", "Campus is not active");

				logsServices.addLog(AppConstants.error, AppConstants.OUTLINE, "addOutlineDetails",
						"Campus is not active", null);
				return new ResponseEntity<>(returnmap, HttpStatus.OK);
			}

			OutlinesDetails outlinesDetails = outlinesDetail; 
			outlinesDetails.setCampusDetails(campusDetails);
			outlinesDetails.setActive(true);
			outlinesDetails.setCreatedDate(new Date());
			outlinesDetails.setModifiedDate(new Date());
			outLineRepository.save(outlinesDetails);

			returnmap.put("status", "success");
			returnmap.put("message", outlinesDetails.getName() + " added successfully");

			logsServices.addLog(AppConstants.event, AppConstants.OUTLINE, "addOutlineDetails",
					"Outlines -" + outlinesDetails.getName() + " added successfully, ", null);

			return new ResponseEntity<>(returnmap, HttpStatus.OK);
		} catch (Exception e) {
			returnmap.put("status", "error");
			returnmap.put("message", AppConstants.errormsg);

			logsServices.addLog(AppConstants.error, AppConstants.OUTLINE, "addOutlineDetails", AppConstants.errormsg,
					null);
			return new ResponseEntity<>(returnmap, HttpStatus.OK);
		}
	}

	@PutMapping(value = "/outlineDetails/{campusId}")
	public ResponseEntity<?> updateOutlineDetails(HttpServletRequest request, HttpSession httpSession,

			@PathVariable Optional<Long> campusId, @RequestBody OutlinesDetails outlinesRequestBody)
			throws InterruptedException, ExecutionException {
		Map<String, String> returnmap = new HashMap<>();

		try {

			ResponseEntity<?> responseEntity = checkSessionKey(httpSession, Utils.getCookie(request, "JSESSIONID"), "CTPE");
			if (responseEntity != null) { 
				logsServices.addLog(AppConstants.error, AppConstants.OUTLINE, "updateOutlineDetails",
						AppConstants.sessiontimeout, null);
				return responseEntity; 
			}
			
			Map<String, String> reuestMap = new HashMap<>();
			reuestMap.put("params", gson.toJson(outlinesRequestBody, mapType));

			logsServices.addLog(AppConstants.event, AppConstants.OUTLINE, "updateOutlineDetails",
					"Outline Details update API Details", reuestMap);
			
			if (!campusId.isPresent() || outlinesRequestBody.getOutlinesId() <= 0
					|| Utils.isEmptyString(outlinesRequestBody.getName())
					|| Utils.isEmptyString(outlinesRequestBody.getPlaceType())
					|| Utils.isEmptyString(outlinesRequestBody.getWallType())
					|| Utils.isEmptyString(outlinesRequestBody.getOutlinesArea())  
					|| Utils.isEmptyString(outlinesRequestBody.getBgColor())
							| Utils.isEmptyString(outlinesRequestBody.getGeometryType())
					|| outlinesRequestBody.getCoordinateList() == null) {

				returnmap.put("status", "error");
				returnmap.put("message", AppConstants.fieldsmissing);

				logsServices.addLog(AppConstants.error, AppConstants.OUTLINE, "updateOutlineDetails",
						AppConstants.fieldsmissing, null);
				return new ResponseEntity<>(returnmap, HttpStatus.OK);

			}

			OutlinesDetails outlinesDetails = outLineRepository.findByOutlinesId(outlinesRequestBody.getOutlinesId());
			if (outlinesDetails == null) {
				returnmap.put("status", "error");
				returnmap.put("message", "Outlines id not found");

				logsServices.addLog(AppConstants.error, AppConstants.OUTLINE, "updateOutlineDetails",
						"Outlines id not found", null);
				return new ResponseEntity<>(returnmap, HttpStatus.OK);
			} else if (!outlinesDetails.isActive()) {
				returnmap.put("status", "error");
				returnmap.put("message", "Outlines is not active");

				logsServices.addLog(AppConstants.error, AppConstants.OUTLINE, "updateOutlineDetails",
						"Outlines is not active", null);
				return new ResponseEntity<>(returnmap, HttpStatus.OK);
			}

			CampusDetails campusDetails = campusDetailsRepository.findByCampusId(campusId.get());
			if (campusDetails == null) {
				returnmap.put("status", "error");
				returnmap.put("message", "Campus id not found");

				logsServices.addLog(AppConstants.error, AppConstants.OUTLINE, "updateOutlineDetails",
						"Campus id not found", null);
				return new ResponseEntity<>(returnmap, HttpStatus.OK);
			} else if (!campusDetails.isActive()) {
				returnmap.put("status", "error");
				returnmap.put("message", "Campus is not active");

				logsServices.addLog(AppConstants.error, AppConstants.OUTLINE, "updateOutlineDetails",
						"Campus is not active", null);
				return new ResponseEntity<>(returnmap, HttpStatus.OK);
			}

			outlinesDetails.setName(outlinesRequestBody.getName());
			outlinesDetails.setCampusDetails(campusDetails); 
			outlinesDetails.setOutlinesArea(outlinesRequestBody.getOutlinesArea());
			outlinesDetails.setWallType(outlinesRequestBody.getWallType());
			outlinesDetails.setGeometryType(outlinesRequestBody.getGeometryType()); 
			outlinesDetails.setBgColor(outlinesRequestBody.getBgColor());
			outlinesDetails.setPlaceType(outlinesRequestBody.getPlaceType());
			outlinesDetails.setCoordinateList(outlinesRequestBody.getCoordinateList());

			outlinesDetails.setModifiedDate(new Date());
			outLineRepository.save(outlinesDetails);

			returnmap.put("status", "success");
			returnmap.put("message", outlinesDetails.getName() + " updated successfully");

			logsServices.addLog(AppConstants.event, AppConstants.OUTLINE, "updateOutlineDetails",
					"Outlines -" + outlinesDetails.getName() + " updated successfully, ", null);

			return new ResponseEntity<>(returnmap, HttpStatus.OK);
		} catch (Exception e) {
			returnmap.put("status", "error");
			returnmap.put("message", AppConstants.errormsg);

			logsServices.addLog(AppConstants.error, AppConstants.OUTLINE, "updateOutlineDetails", AppConstants.errormsg,
					null);
			return new ResponseEntity<>(returnmap, HttpStatus.OK);
		}
	}

	@DeleteMapping(value = "/outlineDetails")
	public ResponseEntity<?> DeleteOutlineDetails(HttpServletRequest request, HttpSession httpSession,
			@RequestBody HashMap<String, Object> requestMap) throws InterruptedException, ExecutionException {
		Map<String, String> returnmap = new HashMap<>();

		try {

			ResponseEntity<?> responseEntity = checkSessionKey(httpSession, Utils.getCookie(request, "JSESSIONID"), "CTPE");
			if (responseEntity != null) { 
				logsServices.addLog(AppConstants.error, AppConstants.OUTLINE, "DeleteOutlineDetails",
						AppConstants.sessiontimeout, null);
				return responseEntity; 
			}
			
			Map<String, String> reuestMap = new HashMap<>();
			reuestMap.put("params", new JSONObject(requestMap).toString()); 

			logsServices.addLog(AppConstants.event, AppConstants.OUTLINE, "DeleteOutlineDetails",
					"Outline Details update API Details", reuestMap);
			
			if (Utils.isEmptyString(requestMap.getOrDefault("outlinesId", "").toString())) {

				returnmap.put("status", "error");
				returnmap.put("message", AppConstants.fieldsmissing);

				logsServices.addLog(AppConstants.error, AppConstants.OUTLINE, "DeleteOutlineDetails",
						AppConstants.fieldsmissing, null);
				return new ResponseEntity<>(returnmap, HttpStatus.OK);

			}

			OutlinesDetails outlinesDetails = outLineRepository
					.findByOutlinesId(Long.valueOf(requestMap.getOrDefault("outlinesId", "").toString()));
			if (outlinesDetails == null) {
				returnmap.put("status", "error");
				returnmap.put("message", "Outlines id not found");

				logsServices.addLog(AppConstants.error, AppConstants.OUTLINE, "DeleteOutlineDetails",
						"Outlines id not found", null);
				return new ResponseEntity<>(returnmap, HttpStatus.OK);
			} else if (!outlinesDetails.isActive()) {
				returnmap.put("status", "error");
				returnmap.put("message", "Outlines is not active");

				logsServices.addLog(AppConstants.error, AppConstants.OUTLINE, "DeleteOutlineDetails",
						"Outlines is not active", null);
				return new ResponseEntity<>(returnmap, HttpStatus.OK);
			}

			outlinesDetails.setActive(false);
			outlinesDetails.setModifiedDate(new Date());
			outLineRepository.save(outlinesDetails);

			returnmap.put("status", "success");
			returnmap.put("message", outlinesDetails.getName() + " removed successfully");

			logsServices.addLog(AppConstants.event, AppConstants.OUTLINE, "DeleteOutlineDetails",
					"Outlines -" + outlinesDetails.getName() + " removed successfully, ", null);

			return new ResponseEntity<>(returnmap, HttpStatus.OK);
		} catch (Exception e) {
			returnmap.put("status", "error");
			returnmap.put("message", AppConstants.errormsg);

			logsServices.addLog(AppConstants.error, AppConstants.OUTLINE, "DeleteOutlineDetails", AppConstants.errormsg,
					null);
			return new ResponseEntity<>(returnmap, HttpStatus.OK);
		}
	}
}
