package com.entappia.ei4o.controllers;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.concurrent.ExecutionException;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.entappia.ei4o.constants.AppConstants;
import com.entappia.ei4o.dbmodels.Login;
import com.entappia.ei4o.dbmodels.Notifications;
import com.entappia.ei4o.dbmodels.Settings;
import com.entappia.ei4o.repository.SettingsRepository;
import com.entappia.ei4o.repository.LoginRegistrationRepository;
import com.entappia.ei4o.repository.NotificationsRepository;
import com.entappia.ei4o.services.LogsServices;
import com.entappia.ei4o.utils.BaseController;
import com.entappia.ei4o.utils.Utils;

@RestController
public class NotificationController extends BaseController {

	@Autowired
	private LogsServices logsServices;

	@Autowired
	private NotificationsRepository notificationsRepository;

	@Autowired
	private SettingsRepository settingsRepository;
	
	@Autowired
	private LoginRegistrationRepository loginRegistrationRepository;
	

	@RequestMapping(path = { "notificationCount/{notificationId}" }, method = RequestMethod.GET)
	public ResponseEntity<?> getNotificationCount(HttpServletRequest request, HttpSession httpSession,
			@PathVariable Optional<Long> notificationId) throws InterruptedException, ExecutionException {
		Map<String, Object> returnmap = new HashMap<>();
		try {

			Map<String, String> reuestMap = new HashMap<>();
			ResponseEntity<?> responseEntity = checkSessionKey(httpSession, Utils.getCookie(request, "JSESSIONID"));
			if (responseEntity != null) { 
				logsServices.addLog(AppConstants.error, AppConstants.NOTIFICATION, "getNotificationCount",
						AppConstants.sessiontimeout, null);
				return responseEntity; 
			}
			
			JSONObject jsonObject = new JSONObject();
			jsonObject.put("notificationId", notificationId.get());

			reuestMap.put("params", jsonObject.toString()); 

			
			logsServices.addLog(AppConstants.event, AppConstants.NOTIFICATION, "getNotificationCount",
					"Notification get API call", reuestMap);
			
			long notificationCount = notificationsRepository.getMaxNotificationId();

			List<Notifications> emergenyList = notificationsRepository.findUnreadNotifications(notificationId.get(), 2);
			List<Notifications> warningList = notificationsRepository.findUnreadNotifications(notificationId.get(), 1);
			List<Notifications> informationList = notificationsRepository.findUnreadNotifications(notificationId.get(),
					0);

			HashMap<String, Object> dataMap = new HashMap<>();
			dataMap.put("total", notificationCount);
			dataMap.put("emergeny", emergenyList != null ? emergenyList.size() : 0);
			dataMap.put("warning", warningList != null ? warningList.size() : 0);
			dataMap.put("information", informationList != null ? informationList.size() : 0);
			dataMap.put("totalUnRead",
					(int) dataMap.get("emergeny") + (int) dataMap.get("warning") + (int) dataMap.get("information"));

			returnmap.put("status", "success");
			returnmap.put("data", dataMap);

			logsServices.addLog(AppConstants.event, AppConstants.NOTIFICATION, "getNotificationCount",
					"notificationCount", null);
			return new ResponseEntity<>(returnmap, HttpStatus.OK);
		} catch (Exception e) {
			returnmap.put("status", "error");
			returnmap.put("message", AppConstants.errormsg);
			logsServices.addLog(AppConstants.error, AppConstants.NOTIFICATION, "getNotificationCount",
					AppConstants.errormsg, null);

			return new ResponseEntity<>(returnmap, HttpStatus.OK);
		}

	}

	@RequestMapping(path = { "notifications" }, method = RequestMethod.POST)
	public ResponseEntity<?> getNotifications(HttpServletRequest request, HttpSession httpSession,
			@RequestBody HashMap<String, String> requestMap) throws InterruptedException, ExecutionException {
		Map<String, Object> returnmap = new HashMap<>();
		try {
			
			
			ResponseEntity<?> responseEntity = checkSessionKey(httpSession, Utils.getCookie(request, "JSESSIONID"));
			if (responseEntity != null) { 
				logsServices.addLog(AppConstants.error, AppConstants.NOTIFICATION, "getNotifications",
						AppConstants.sessiontimeout, null);
				return responseEntity; 
			}
			
			Map<String, String> reuestMap = new HashMap<>();
			reuestMap.put("params", new JSONObject(requestMap).toString());
   
			logsServices.addLog(AppConstants.event, AppConstants.NOTIFICATION, "getNotifications",
					"Notification get All API call", reuestMap);

			if (Utils.isEmptyString(requestMap.getOrDefault("emailId", "")) 
					|| Utils.isEmptyString(requestMap.getOrDefault("notificationId", ""))
					|| Utils.isEmptyString(requestMap.getOrDefault("noOfRecords", ""))
					|| Utils.isEmptyString(requestMap.getOrDefault("isFirstTime", ""))) {
				returnmap.put("status", "error");
				returnmap.put("message", AppConstants.errormsg);
				logsServices.addLog(AppConstants.error, AppConstants.NOTIFICATION, "getNotifications",
						AppConstants.errormsg, null);
			}
			
			long notificationId = Integer.valueOf(requestMap.get("notificationId"));
			if(notificationId==0)
				notificationId = notificationsRepository.getMaxNotificationId()+1;

			if (requestMap.get("isFirstTime").equals("0")) {
				notificationId = notificationId+1;
				long notificationCount = notificationsRepository.getMaxNotificationId();

				Settings settings = settingsRepository
						.findByEmailId(requestMap.get("emailId"));
				if (settings == null) {
					settings = new Settings();
					settings.setEmailId(requestMap.get("emailId"));
					settings.setCreatedDate(new Date());
				}
				settings.setModifiedDate(new Date());
				settings.setNotificationId(notificationCount);
				settingsRepository.save(settings);
			}

			
			int noOfRecords = Integer.valueOf(requestMap.get("noOfRecords"));
			if(noOfRecords==0)
				noOfRecords = LogsController.listViewCount;
			
			List<Notifications> notificationsList = notificationsRepository.getNotifications(notificationId,
					noOfRecords);

			returnmap.put("status", "success");
			returnmap.put("data", notificationsList);
			logsServices.addLog(AppConstants.error, AppConstants.NOTIFICATION, "getNotifications", "getNotifications",
					null);

			return new ResponseEntity<>(returnmap, HttpStatus.OK);
		} catch (Exception e) {
			returnmap.put("status", "error");
			returnmap.put("message", AppConstants.errormsg);
			logsServices.addLog(AppConstants.error, AppConstants.NOTIFICATION, "getNotifications",
					AppConstants.errormsg, null);

			return new ResponseEntity<>(returnmap, HttpStatus.OK);
		}

	}
	
	@RequestMapping(path = { "updateProfile" }, method = RequestMethod.POST)
	public ResponseEntity<?> updateProfile(HttpServletRequest request, HttpSession httpSession,
			@RequestBody HashMap<String, String> requestMap) throws InterruptedException, ExecutionException {
		Map<String, Object> returnmap = new HashMap<>();
		try {


			ResponseEntity<?> responseEntity = checkSessionKey(httpSession, Utils.getCookie(request, "JSESSIONID"));
			if (responseEntity != null) { 
				logsServices.addLog(AppConstants.error, AppConstants.NOTIFICATION, "updateProfile",
						AppConstants.sessiontimeout, null);
				return responseEntity; 
			}
			
			Map<String, String> reuestMap = new HashMap<>();
			reuestMap.put("params", new JSONObject(requestMap).toString());
   
			logsServices.addLog(AppConstants.event, AppConstants.NOTIFICATION, "updateProfile",
					"Notification Update API call", reuestMap);
			
			if (Utils.isEmptyString(requestMap.getOrDefault("emailId", "")) 
					|| Utils.isEmptyString(requestMap.getOrDefault("showNotification", ""))
					|| Utils.isEmptyString(requestMap.getOrDefault("profileColor", ""))) {
				returnmap.put("status", "error");
				returnmap.put("message", AppConstants.errormsg);
				logsServices.addLog(AppConstants.error, AppConstants.PROFILE, "updateProfile",
						AppConstants.errormsg, null);
				return new ResponseEntity<>(returnmap, HttpStatus.OK);
			}

			 
			Login login = loginRegistrationRepository.findByEmailID(requestMap.getOrDefault("emailId", ""));
			if(login==null) {
				returnmap.put("status", "error");
				returnmap.put("message", "Invalid email id.");
				logsServices.addLog(AppConstants.error, AppConstants.PROFILE, "updateProfile",
						"Invalid email id.", null);
				return new ResponseEntity<>(returnmap, HttpStatus.OK);
			}
			
			Settings settings = settingsRepository
					.findByEmailId(requestMap.get("emailId"));
			if (settings == null) {
				settings = new Settings();
				settings.setEmailId(requestMap.get("emailId"));
				settings.setCreatedDate(new Date());
			}
			
			settings.setShowNotification(Boolean.valueOf(requestMap.get("showNotification")));
			settings.setProfileColor(requestMap.getOrDefault("profileColor", ""));
			settings.setModifiedDate(new Date()); 
			settingsRepository.save(settings); 
			  
			returnmap.put("status", "success");
			returnmap.put("message", "Profile details updated successfully");
			logsServices.addLog(AppConstants.error, AppConstants.PROFILE, "updateProfile", "updateProfile",
					null);

			return new ResponseEntity<>(returnmap, HttpStatus.OK);
		} catch (Exception e) {
			returnmap.put("status", "error");
			returnmap.put("message", AppConstants.errormsg);
			logsServices.addLog(AppConstants.error, AppConstants.PROFILE, "updateProfile",
					AppConstants.errormsg, null);

			return new ResponseEntity<>(returnmap, HttpStatus.OK);
		}

	}

}
