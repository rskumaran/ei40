package com.entappia.ei4o.controllers;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.concurrent.ExecutionException;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.entappia.ei4o.constants.AppConstants;
import com.entappia.ei4o.dbmodels.AssetChrono;
import com.entappia.ei4o.dbmodels.Material;
import com.entappia.ei4o.dbmodels.PathSegments;
import com.entappia.ei4o.dbmodels.Staff;
import com.entappia.ei4o.dbmodels.Tags;
import com.entappia.ei4o.repository.MaterialRepository;
import com.entappia.ei4o.repository.TagsRepository;
import com.entappia.ei4o.services.LogsServices;
import com.entappia.ei4o.utils.BaseController;
import com.entappia.ei4o.utils.Utils;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

@RestController
public class MaterialController extends BaseController {

	@Autowired
	MaterialRepository materialRepository;

	@Autowired
	private LogsServices logsServices;

	@Autowired
	private TagsRepository tagsRepository;
	Gson gson = new Gson();
	Type mapType = new TypeToken<Material>() {
	}.getType();

	@RequestMapping(path = { "material", "material/{prodId}" }, method = RequestMethod.GET)
	public ResponseEntity<?> getmeterial(HttpServletRequest request, HttpSession httpSession,
			@PathVariable Optional<String> prodId) throws InterruptedException, ExecutionException {
		Map<String, Object> returnmap = new HashMap<>();
		HashMap<String, String> reuestMap = new HashMap<>();
		try {
			
			ResponseEntity<?> responseEntity = checkSessionKey(httpSession, Utils.getCookie(request, "JSESSIONID"), "WMVA");
			if (responseEntity != null) { 
				logsServices.addLog(AppConstants.error, AppConstants.MTL, "getmaterial",
						AppConstants.sessiontimeout, null);
				return responseEntity; 
			}
			
			JSONObject jsonObject = new JSONObject();
			if (prodId.isPresent()) {
				jsonObject.put("prodId", prodId.get());
			} else {
				jsonObject.put("prodId", "all");
			}

			reuestMap.put("params", jsonObject.toString()); 

			
			logsServices.addLog(AppConstants.event, AppConstants.MTL, "getmaterial",
					"Material get API call", reuestMap);
			
			
			if (prodId.isPresent()) {
				reuestMap.put("material prodId", prodId.get());
			} else {
				reuestMap.put("params", "getalldept");
			}

			 

			List<Tags> tagsList = tagsRepository.listOfAssignedTagsByType("Materials");

			HashMap<String, Tags> tagsMap = new HashMap<>();

			tagsList.forEach(tags -> {
				tagsMap.put(tags.getAssignmentId(), tags);
			});

			List<Material> materialDetails = new ArrayList<>();

			if (prodId.isPresent()) {
				Material material = materialRepository.findByProdId(prodId.get());
				if (material == null) {
					returnmap.put("status", "error");
					returnmap.put("message", "Material id does not exist");
					logsServices.addLog(AppConstants.error, AppConstants.MTL, "getmaterial",
							"material id does not exist", null);

					return new ResponseEntity<>(returnmap, HttpStatus.OK);
				}

				materialDetails.add(material);
			} else {
				materialDetails = materialRepository.findAll();
			}

			List<Map<String, Object>> materialMapDetails = new ArrayList<>();

			final ObjectMapper mapper = new ObjectMapper();
			for (Material material : materialDetails) {

				Map<String, Object> map = mapper.convertValue(material, new TypeReference<Map<String, Object>>() {
				});
				Tags tags = tagsMap.get(String.valueOf(material.getOrdereNo()));
				if (tags != null) {
					map.put("assignedTagMacId", tags.getMacId());
					map.put("assignedStatus", tags.getStatus());
				} else
				{

					map.put("assignedStatus", "");
					map.put("assignedTagMacId", "");
				}

				materialMapDetails.add(map);
			}

			logsServices.addLog(AppConstants.success, AppConstants.MTL, "getmaterial", "Material details", null);

			return new ResponseEntity<>(materialMapDetails, HttpStatus.OK);
		} catch (Exception e) {
			returnmap.put("status", "error");
			returnmap.put("message", AppConstants.errormsg);
			logsServices.addLog(AppConstants.error, AppConstants.MTL, "getmaterial", AppConstants.errormsg, null);

			return new ResponseEntity<>(returnmap, HttpStatus.OK);
		}

	}

	@PostMapping(value = "/material")
	public ResponseEntity<?> AddMaterial(HttpServletRequest request, HttpSession httpSession,

			@RequestBody Material material) throws InterruptedException, ExecutionException {
		Map<String, String> returnmap = new HashMap<>();
		try {
			
			ResponseEntity<?> responseEntity = checkSessionKey(httpSession, Utils.getCookie(request, "JSESSIONID"), "WMAD");
			if (responseEntity != null) { 
				logsServices.addLog(AppConstants.error, AppConstants.MTL, "Addmaterial",
						AppConstants.sessiontimeout, null);
				return responseEntity; 
			}

			Map<String, String> requestLogMap = new HashMap<>(); 
			requestLogMap.put("params", gson.toJson(material, mapType));
			
			logsServices.addLog(AppConstants.event, AppConstants.MTL, "Addmaterial",
					"Material add API call", requestLogMap);
			
			HashMap<String, Object> mapTags = new HashMap<>();
			if (material != null) {
				mapTags.put("material prodid", material.getProdId());
				mapTags.put("material orderno", material.getOrdereNo());
				mapTags.put("issue date", material.getIssueDate());
				mapTags.put("source", material.getSource());
				mapTags.put("destination", material.getDestination());
				mapTags.put("processcontrol", material.getProcessControl());

			}

			if (Utils.isEmptyString(material.getProdId()) || Utils.isEmptyString(String.valueOf(material.getOrdereNo()))
					|| Utils.isEmptyString(material.getIssueDate().toString())
					|| Utils.isEmptyString(material.getSource()) || Utils.isEmptyString(material.getDestination())
					|| Utils.isEmptyString(material.getProcessControl()) || material.getPrice() <= 0) {

				returnmap.put("status", "error");
				returnmap.put("message", AppConstants.fieldsmissing);

				logsServices.addLog(AppConstants.error, AppConstants.MTL, "Addmaterial", AppConstants.fieldsmissing,
						null);
				return new ResponseEntity<>(returnmap, HttpStatus.OK);

			}

			Material material1 = materialRepository.findByProdId(material.getProdId());

			if (material1 != null && material1.isActive()) {
				returnmap.put("status", "error");
				returnmap.put("message", "material id already exist");

				logsServices.addLog(AppConstants.error, AppConstants.MTL, "Addmaterial", "Material id already exist",
						null);
				return new ResponseEntity<>(returnmap, HttpStatus.OK);
			}

			material.setCreatedDate(new Date());
			material.setModifiedDate(new Date());
			material.setActive(true);
			materialRepository.save(material);

			returnmap.put("status", "success");
			returnmap.put("message", material.getProdId() + " added");

			logsServices.addLog(AppConstants.event, AppConstants.MTL, "AddTag",
					"Material-" + material.getProdId() + " added successfully, ", null);

			return new ResponseEntity<>(returnmap, HttpStatus.OK);
		} catch (Exception e) {
			returnmap.put("status", "error");
			returnmap.put("message", AppConstants.errormsg);

			logsServices.addLog(AppConstants.error, AppConstants.MTL, "Addmaterial", AppConstants.errormsg, null);
			return new ResponseEntity<>(returnmap, HttpStatus.OK);
		}
	}

	@PutMapping(value = "/material")
	public ResponseEntity<?> UpdateMaterial(HttpServletRequest request, HttpSession httpSession,

			@RequestBody Material material) throws InterruptedException, ExecutionException {
		Map<String, String> returnmap = new HashMap<>(); 
		try {
			
			ResponseEntity<?> responseEntity = checkSessionKey(httpSession, Utils.getCookie(request, "JSESSIONID"), "WMED");
			if (responseEntity != null) { 
				logsServices.addLog(AppConstants.error, AppConstants.MTL, "Updatematerial",
						AppConstants.sessiontimeout, null);
				return responseEntity; 
			} 
			
			Map<String, String> requestLogMap = new HashMap<>(); 
			requestLogMap.put("params", gson.toJson(material, mapType));
			
			logsServices.addLog(AppConstants.event, AppConstants.MTL, "Updatematerial",
					"Material update API call", requestLogMap);
			
			HashMap<String, Object> mapTags = new HashMap<>();
			if (material != null) {
				mapTags.put("material prodid", material.getProdId());
				mapTags.put("material orderno", material.getOrdereNo());
				mapTags.put("issue date", material.getIssueDate());
				mapTags.put("source", material.getSource());
				mapTags.put("destination", material.getDestination());
				mapTags.put("processcontrol", material.getProcessControl());

			}

			if (Utils.isEmptyString(material.getProdId()) || Utils.isEmptyString(String.valueOf(material.getOrdereNo()))
					|| Utils.isEmptyString(material.getIssueDate().toString())
					|| Utils.isEmptyString(material.getSource()) || Utils.isEmptyString(material.getDestination())
					|| Utils.isEmptyString(material.getProcessControl()) || material.getPrice() <= 0) {

				returnmap.put("status", "error");
				returnmap.put("message", AppConstants.fieldsmissing);

				logsServices.addLog(AppConstants.error, AppConstants.MTL, "Updatematerial", AppConstants.fieldsmissing,
						null);
				return new ResponseEntity<>(returnmap, HttpStatus.OK);

			}

			Material material1 = materialRepository.findByProdId(material.getProdId());

			if (material1 == null) {
				returnmap.put("status", "error");
				returnmap.put("message", "material id not exist");

				logsServices.addLog(AppConstants.error, AppConstants.MTL, "Updatematerial", "Material id not exist",
						null);
				return new ResponseEntity<>(returnmap, HttpStatus.OK);
			}

			material1.setOrdereNo(material.getOrdereNo());
			material1.setPrice(material.getPrice());
			material1.setIssueDate(material.getIssueDate());
			material1.setSource(material.getSource());
			material1.setDestination(material.getDestination());
			material1.setProcessControl(material.getProcessControl());
			material1.setModifiedDate(new Date());
			materialRepository.save(material1);

			returnmap.put("status", "success");
			returnmap.put("message", material.getProdId() + " updated");

			logsServices.addLog(AppConstants.event, AppConstants.MTL, "UpdateMaterial",
					"Material-" + material.getProdId() + " Update successfully, ", null);

			return new ResponseEntity<>(returnmap, HttpStatus.OK);
		} catch (Exception e) {
			returnmap.put("status", "error");
			returnmap.put("message", AppConstants.errormsg);

			logsServices.addLog(AppConstants.error, AppConstants.MTL, "Updatematerial", AppConstants.errormsg, null);
			return new ResponseEntity<>(returnmap, HttpStatus.OK);
		}
	}

	@DeleteMapping(value = "/material")
	public ResponseEntity<?> DeleteMetrial(HttpServletRequest request, HttpSession httpSession,
			@RequestBody HashMap<String, Object> requestMap) throws InterruptedException, ExecutionException {
		Map<String, String> returnmap = new HashMap<>(); 
		try {
			
			ResponseEntity<?> responseEntity = checkSessionKey(httpSession, Utils.getCookie(request, "JSESSIONID"), "WMED");
			if (responseEntity != null) { 
				logsServices.addLog(AppConstants.error, AppConstants.MTL, "Deletematerial",
						AppConstants.sessiontimeout, null);
				return responseEntity; 
			} 
			
			Map<String, String> requestLogMap = new HashMap<>(); 
			requestLogMap.put("params", new JSONObject(requestMap).toString());
			
			logsServices.addLog(AppConstants.event, AppConstants.MTL, "Deletematerial",
					"Material delete API call", requestLogMap);
			
			HashMap<String, Object> mapTags = new HashMap<>();

			if (Utils.isEmptyString(requestMap.getOrDefault("prodId", "").toString())) {

				returnmap.put("status", "error");
				returnmap.put("message", AppConstants.fieldsmissing);

				logsServices.addLog(AppConstants.error, AppConstants.MTL, "Deletematerial", AppConstants.fieldsmissing,
						null);
				return new ResponseEntity<>(returnmap, HttpStatus.OK);

			} else {
				mapTags.put("material id", requestMap.getOrDefault("prodId", "").toString());
				mapTags.put("active", false);
			}

			Material material1 = materialRepository.findByProdId(requestMap.getOrDefault("prodId", "").toString());

			if (material1 == null) {
				returnmap.put("status", "error");
				returnmap.put("message", "material id not exist");

				logsServices.addLog(AppConstants.error, AppConstants.MTL, "Deletematerial", "Material id not exist",
						null);
				return new ResponseEntity<>(returnmap, HttpStatus.OK);
			}
			material1.setModifiedDate(new Date());
			material1.setActive(false);
			materialRepository.save(material1);

			returnmap.put("status", "success");
			returnmap.put("message", material1.getProdId() + " Deleted");

			logsServices.addLog(AppConstants.event, AppConstants.MTL, "DeleteMaterial",
					"Material-" + material1.getProdId() + " Deleted successfully, ", null);

			return new ResponseEntity<>(returnmap, HttpStatus.OK);
		} catch (Exception e) {
			returnmap.put("status", "error");
			returnmap.put("message", AppConstants.errormsg);

			logsServices.addLog(AppConstants.error, AppConstants.MTL, "Deletematerial", AppConstants.errormsg, null);
			return new ResponseEntity<>(returnmap, HttpStatus.OK);
		}
	}

	@PostMapping(value = "/multipleMaterial")
	public ResponseEntity<?> AddMultipleMaterial(HttpServletRequest request, HttpSession httpSession,

			@RequestBody List<Material> materialReqList) throws InterruptedException, ExecutionException {
		Map<String, Object> returnmap = new HashMap<>();
		try {
			
			ResponseEntity<?> responseEntity = checkSessionKey(httpSession, Utils.getCookie(request, "JSESSIONID"), "WMMA");
			if (responseEntity != null) { 
				logsServices.addLog(AppConstants.error, AppConstants.MTL, "AddMultipleMaterial",
						AppConstants.sessiontimeout, null);
				return responseEntity; 
			}

			ObjectMapper objectMapper = new ObjectMapper(); 
			String json = objectMapper.writeValueAsString(materialReqList);
			 
			Map<String, String> requestLogMap = new HashMap<>(); 
			requestLogMap.put("params", json);
			
			logsServices.addLog(AppConstants.event, AppConstants.MTL, "AddMultipleMaterial",
					"Material add Multiple API call", requestLogMap);
			
			if (materialReqList == null || materialReqList.size() == 0) {
				returnmap.put("status", "error");
				returnmap.put("message", AppConstants.fieldsmissing);

				logsServices.addLog(AppConstants.error, AppConstants.MTL, "AddMultipleMaterial",
						AppConstants.fieldsmissing, null);
				return new ResponseEntity<>(returnmap, HttpStatus.OK);
			}

			HashMap<String, Material> materialMap = getMaterialDetails();
			List<Material> materialList = new ArrayList<>();
			List<String> errorIds = new ArrayList<>();
			List<String> savedIds = new ArrayList<>();
			for (Material material : materialReqList) {
				if (Utils.isEmptyString(material.getProdId())
						|| Utils.isEmptyString(String.valueOf(material.getOrdereNo()))
						|| Utils.isEmptyString(material.getIssueDate().toString())
						|| Utils.isEmptyString(material.getSource()) || Utils.isEmptyString(material.getDestination())
						|| Utils.isEmptyString(material.getProcessControl()) || material.getPrice() <= 0) {

					errorIds.add(material.getProdId());
				} else {
					Material material1 = null;

					if (materialMap.containsKey(material.getProdId())) {
						material1 = materialMap.get(material.getProdId());
						if (material1 == null) {
							material1 = new Material();
							material1.setProdId(material.getProdId());
							material1.setActive(true);
							material1.setCreatedDate(new Date());
						}
						material1.setModifiedDate(new Date());
					} else {
						material1 = new Material();
						material1.setCreatedDate(new Date());
						material1.setModifiedDate(new Date());
						material1.setProdId(material.getProdId());
						material1.setActive(true);
					}

					if (material1.isActive()) {

						material1.setOrdereNo(material.getOrdereNo());
						material1.setIssueDate(material.getIssueDate());
						material1.setSource(material.getSource());
						material1.setDestination(material.getDestination());
						material1.setProcessControl(material.getProcessControl());
						material1.setPrice(material.getPrice());
						materialList.add(material1);

						savedIds.add(material.getProdId());
					} else {
						errorIds.add(material.getProdId());
					}
				}
			}

			if (materialList.size() > 0)
				materialRepository.saveAll(materialList);

			returnmap.put("status", "success");
			returnmap.put("message", savedIds.size() + " Materials added successfully.");

			returnmap.put("savedIds", savedIds);
			returnmap.put("errorIds", errorIds);

			logsServices.addLog(AppConstants.event, AppConstants.MTL, "AddMultipleMaterial", "Material- saved ids ["
					+ String.join(",", savedIds) + "], error ids [" + String.join(",", errorIds) + "].", null);

			return new ResponseEntity<>(returnmap, HttpStatus.OK);
		} catch (Exception e) {
			returnmap.put("status", "error");
			returnmap.put("message", AppConstants.errormsg);

			logsServices.addLog(AppConstants.error, AppConstants.MTL, "AddMultipleMaterial", AppConstants.errormsg,
					null);
			return new ResponseEntity<>(returnmap, HttpStatus.OK);
		}
	}

	private HashMap<String, Material> getMaterialDetails() {
		// TODO Auto-generated method stub

		HashMap<String, Material> map = new HashMap<String, Material>();

		materialRepository.findAll().stream().forEach(material -> {
			map.put(material.getProdId(), material);
		});

		return map;
	}
}
