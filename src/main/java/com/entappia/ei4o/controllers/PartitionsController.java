package com.entappia.ei4o.controllers;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.concurrent.ExecutionException;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.entappia.ei4o.constants.AppConstants;
import com.entappia.ei4o.dbmodels.OutlinesDetails;
import com.entappia.ei4o.dbmodels.Partitions;
import com.entappia.ei4o.repository.OutLineRepository;
import com.entappia.ei4o.repository.PartitionsRepository;
import com.entappia.ei4o.services.LogsServices;
import com.entappia.ei4o.utils.BaseController;
import com.entappia.ei4o.utils.Utils;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

@RestController
public class PartitionsController extends BaseController {

	@Autowired
	private LogsServices logsServices;

	@Autowired
	private OutLineRepository outLineRepository;

	@Autowired
	private PartitionsRepository partitionsRepository;

	@RequestMapping(path = { "partitionsDetails", "partitionsDetails/{outlinesId}" }, method = RequestMethod.GET)
	public ResponseEntity<?> getPartitionsDetails(HttpServletRequest request, HttpSession httpSession,
			@PathVariable Optional<Long> outlinesId) throws InterruptedException, ExecutionException {
		Map<String, Object> returnmap = new HashMap<>();
		try {
 

			
			ResponseEntity<?> responseEntity = checkSessionKey(httpSession, Utils.getCookie(request, "JSESSIONID"));
			if (responseEntity != null) { 
				logsServices.addLog(AppConstants.error, AppConstants.PARTITIONS, "getPartitionsDetails",
						AppConstants.sessiontimeout, null);
				return responseEntity; 
			}
			
			
			JSONObject jsonObject = new JSONObject();
			if (outlinesId.isPresent()) {
				jsonObject.put("outlinesId", outlinesId.get());
			} else {
				jsonObject.put("outlinesId", "all");
			}
			Map<String, String> reuestMap = new HashMap<>();
			reuestMap.put("params", jsonObject.toString()); 

			logsServices.addLog(AppConstants.event, AppConstants.PARTITIONS, "getPartitionsDetails",
					"Partitions Details Get API Details", reuestMap);
			
			List<Partitions> partitionsDetailsList = new ArrayList<>();

			if (outlinesId.isPresent()) {
				partitionsDetailsList = partitionsRepository.findByOutlinesId(outlinesId.get());
				if (partitionsDetailsList == null) {
					returnmap.put("status", "error");
					returnmap.put("message", "Partitions Details does not exist");
					logsServices.addLog(AppConstants.error, AppConstants.PARTITIONS, "getPartitionsDetails",
							returnmap.get("message").toString(), null);

					return new ResponseEntity<>(returnmap, HttpStatus.OK);
				}

			} else {
				
				partitionsDetailsList = partitionsRepository.findAllActivePartitions();
 
				if (partitionsDetailsList == null) {
					returnmap.put("status", "error");
					returnmap.put("message", "Partitions Details does not exist");
					logsServices.addLog(AppConstants.error, AppConstants.PARTITIONS, "getPartitionsDetails",
							returnmap.get("message").toString(), null);

					return new ResponseEntity<>(returnmap, HttpStatus.OK);
				}
				
			}
			logsServices.addLog(AppConstants.event, AppConstants.PARTITIONS, "getPartitionsDetails", "Partitions details", null);

			returnmap.put("status", "success");
			returnmap.put("data", partitionsDetailsList);

			return new ResponseEntity<>(returnmap, HttpStatus.OK);
		} catch (Exception e) {
			returnmap.put("status", "error");
			returnmap.put("message", AppConstants.errormsg);
			logsServices.addLog(AppConstants.error, AppConstants.PARTITIONS, "getPartitionsDetails", AppConstants.errormsg, null);

			return new ResponseEntity<>(returnmap, HttpStatus.OK);
		}

	}

	@PostMapping(value = "/partitionsDetails")
	public ResponseEntity<?> addPartitionsDetails(HttpServletRequest request, HttpSession httpSession,

			@RequestBody HashMap<String, Object> requestMap) throws InterruptedException, ExecutionException {
		Map<String, String> returnmap = new HashMap<>();

		try {
			 
			ResponseEntity<?> responseEntity = checkSessionKey(httpSession, Utils.getCookie(request, "JSESSIONID"), "CTPE");
			if (responseEntity != null) { 
				logsServices.addLog(AppConstants.error, AppConstants.PARTITIONS, "addPartitionsDetails",
						AppConstants.sessiontimeout, null);
				return responseEntity; 
			}
			 
			 
			Map<String, String> reuestMap = new HashMap<>();
			reuestMap.put("params", new JSONObject(requestMap).toString()); 

			logsServices.addLog(AppConstants.event, AppConstants.PARTITIONS, "addPartitionsDetails",
					"Partitions Details Add API Details", reuestMap);
			

			if (Utils.isEmptyString(requestMap.getOrDefault("outlinesId", "").toString())
					|| Utils.isEmptyString(requestMap.getOrDefault("name", "").toString())
					|| Utils.isEmptyString(requestMap.getOrDefault("geometryType", "").toString())
					|| Utils.isEmptyString(requestMap.getOrDefault("wallType", "").toString())
					|| Utils.isEmptyString(requestMap.getOrDefault("colorCode", "").toString())
					|| Utils.isEmptyString(requestMap.getOrDefault("coordinates", "").toString())) {
				
				returnmap.put("status", "error");
				returnmap.put("message", AppConstants.fieldsmissing);

				logsServices.addLog(AppConstants.error, AppConstants.PARTITIONS, "addPartitionsDetails",
						AppConstants.fieldsmissing, null);
				return new ResponseEntity<>(returnmap, HttpStatus.OK);

			}

			OutlinesDetails outlinesDetails = outLineRepository
					.findByOutlinesId(Long.valueOf(requestMap.getOrDefault("outlinesId", "").toString()));
			if (outlinesDetails == null) {
				returnmap.put("status", "error");
				returnmap.put("message", "Outlines id not found");

				logsServices.addLog(AppConstants.error, AppConstants.PARTITIONS, "addPartitionsDetails",
						returnmap.get("message").toString(), null);
				return new ResponseEntity<>(returnmap, HttpStatus.OK);
			} else if (!outlinesDetails.isActive()) {
				returnmap.put("status", "error");
				returnmap.put("message", "Outlines Details is not active");

				logsServices.addLog(AppConstants.error, AppConstants.PARTITIONS, "addPartitionsDetails",
						returnmap.get("message").toString(), null);
				return new ResponseEntity<>(returnmap, HttpStatus.OK);
			}

			Gson gson = new Gson();
			Type mapType = new TypeToken<List<HashMap<String, String>>>() {
			}.getType();

			List<HashMap<String, String>> coordinateList = gson
					.fromJson(requestMap.getOrDefault("coordinates", "").toString(), mapType);	
			
			Partitions partitions = new Partitions();
			partitions.setName(requestMap.getOrDefault("name", "").toString());
			partitions.setOutlinesDetails(outlinesDetails);
			partitions.setGeometryType(requestMap.getOrDefault("geometryType", "").toString());
			partitions.setWallType(requestMap.getOrDefault("wallType", "").toString());
			partitions.setColorCode(requestMap.getOrDefault("colorCode", "").toString());
			partitions.setCoordinates(coordinateList);
			partitions.setActive(true);
			partitions.setCreatedDate(new Date());
			partitions.setModifiedDate(new Date());
			partitionsRepository.save(partitions);

			returnmap.put("status", "success");
			returnmap.put("message", partitions.getName() + " added successfully");

			logsServices.addLog(AppConstants.event, AppConstants.PARTITIONS, "addPartitionsDetails",
					"Partitions -" + partitions.getName() + " added successfully, ", null);

			return new ResponseEntity<>(returnmap, HttpStatus.OK);
		} catch (Exception e) {
			returnmap.put("status", "error");
			returnmap.put("message", AppConstants.errormsg);

			logsServices.addLog(AppConstants.error, AppConstants.PARTITIONS, "addPartitionsDetails", AppConstants.errormsg, null);
			return new ResponseEntity<>(returnmap, HttpStatus.OK);
		}
	}

	@PutMapping(value = "/partitionsDetails")
	public ResponseEntity<?> updatePartitionsDetails(HttpServletRequest request, HttpSession httpSession,

			@RequestBody HashMap<String, Object> requestMap) throws InterruptedException, ExecutionException {
		Map<String, String> returnmap = new HashMap<>();

		try {
			
			ResponseEntity<?> responseEntity = checkSessionKey(httpSession, Utils.getCookie(request, "JSESSIONID"), "CTPE");
			if (responseEntity != null) { 
				logsServices.addLog(AppConstants.error, AppConstants.PARTITIONS, "updatePartitionsDetails",
						AppConstants.sessiontimeout, null);
				return responseEntity; 
			}
			 
			 
			Map<String, String> reuestMap = new HashMap<>();
			reuestMap.put("params", new JSONObject(requestMap).toString()); 

			logsServices.addLog(AppConstants.event, AppConstants.PARTITIONS, "updatePartitionsDetails",
					"Partitions Details update API Details", reuestMap);

			if (Utils.isEmptyString(requestMap.getOrDefault("id", "").toString())
					||Utils.isEmptyString(requestMap.getOrDefault("outlinesId", "").toString())
					|| Utils.isEmptyString(requestMap.getOrDefault("name", "").toString())
					|| Utils.isEmptyString(requestMap.getOrDefault("geometryType", "").toString())
					|| Utils.isEmptyString(requestMap.getOrDefault("wallType", "").toString())
					|| Utils.isEmptyString(requestMap.getOrDefault("colorCode", "").toString())
					|| Utils.isEmptyString(requestMap.getOrDefault("coordinates", "").toString())) {

				returnmap.put("status", "error");
				returnmap.put("message", AppConstants.fieldsmissing);

				logsServices.addLog(AppConstants.error, AppConstants.PARTITIONS, "updatePartitionsDetails", AppConstants.fieldsmissing,
						null);
				return new ResponseEntity<>(returnmap, HttpStatus.OK);

			}

			OutlinesDetails outlinesDetails = outLineRepository
					.findByOutlinesId(Long.valueOf(requestMap.getOrDefault("outlinesId", "").toString()));
			if (outlinesDetails == null) {
				returnmap.put("status", "error");
				returnmap.put("message", "Outlines id not found");

				logsServices.addLog(AppConstants.error, AppConstants.PARTITIONS, "updatePartitionsDetails",
						returnmap.get("message").toString(), null);
				return new ResponseEntity<>(returnmap, HttpStatus.OK);
			}else if (!outlinesDetails.isActive()) {
				returnmap.put("status", "error");
				returnmap.put("message", "Outlines Details is not active");

				logsServices.addLog(AppConstants.error, AppConstants.PARTITIONS, "updatePartitionsDetails",
						returnmap.get("message").toString(), null);
				return new ResponseEntity<>(returnmap, HttpStatus.OK);
			}

		 
			Partitions partitions = partitionsRepository.findAllById(Long.valueOf(requestMap.getOrDefault("id", "").toString()));
			
			if (partitions == null) {
				returnmap.put("status", "error");
				returnmap.put("message", "Partitions id not found");

				logsServices.addLog(AppConstants.error, AppConstants.PARTITIONS, "updatePartitionsDetails",
						returnmap.get("message").toString(), null);
				return new ResponseEntity<>(returnmap, HttpStatus.OK);
			}else if (!partitions.isActive()) {
				returnmap.put("status", "error");
				returnmap.put("message", "Partitions Details is not active");

				logsServices.addLog(AppConstants.error, AppConstants.PARTITIONS, "updatePartitionsDetails",
						returnmap.get("message").toString(), null);
				return new ResponseEntity<>(returnmap, HttpStatus.OK);
			}
			
			Gson gson = new Gson();
			Type mapType = new TypeToken<List<HashMap<String, String>>>() {
			}.getType();

			List<HashMap<String, String>> coordinateList = gson
					.fromJson(requestMap.getOrDefault("coordinates", "").toString(), mapType);	
						 
			partitions.setName(requestMap.getOrDefault("name", "").toString());
			partitions.setOutlinesDetails(outlinesDetails); 
			partitions.setGeometryType(requestMap.getOrDefault("geometryType", "").toString());
			partitions.setWallType(requestMap.getOrDefault("wallType", "").toString());
			partitions.setColorCode(requestMap.getOrDefault("colorCode", "").toString());
			partitions.setCoordinates(coordinateList); 
			partitions.setModifiedDate(new Date());
			partitionsRepository.save(partitions);

			returnmap.put("status", "success");
			returnmap.put("message", partitions.getName() + " updated successfully");

			logsServices.addLog(AppConstants.event, AppConstants.PARTITIONS, "updatePartitionsDetails",
					"Partitions -" + partitions.getName() + " updated successfully, ", null);

			return new ResponseEntity<>(returnmap, HttpStatus.OK);
		} catch (Exception e) {
			returnmap.put("status", "error");
			returnmap.put("message", AppConstants.errormsg);

			logsServices.addLog(AppConstants.error, AppConstants.PARTITIONS, "updatePartitionsDetails", AppConstants.errormsg, null);
			return new ResponseEntity<>(returnmap, HttpStatus.OK);
		}
	}
	
	
	@DeleteMapping(value = "/partitionsDetails")
	public ResponseEntity<?> deletePartitionsDetails(HttpServletRequest request, HttpSession httpSession, 
			@RequestBody HashMap<String, Object> requestMap) throws InterruptedException, ExecutionException {
		Map<String, String> returnmap = new HashMap<>();

		try {

			ResponseEntity<?> responseEntity = checkSessionKey(httpSession, Utils.getCookie(request, "JSESSIONID"), "CTPE");
			if (responseEntity != null) { 
				logsServices.addLog(AppConstants.error, AppConstants.PARTITIONS, "deletePartitionsDetails",
						AppConstants.sessiontimeout, null);
				return responseEntity; 
			}
			 
			 
			Map<String, String> reuestMap = new HashMap<>();
			reuestMap.put("params", new JSONObject(requestMap).toString()); 

			logsServices.addLog(AppConstants.event, AppConstants.PARTITIONS, "deletePartitionsDetails",
					"Partitions Details delete API Details", reuestMap);
			
			if (Utils.isEmptyString(requestMap.getOrDefault("id", "").toString()) ) {

				returnmap.put("status", "error");
				returnmap.put("message", AppConstants.fieldsmissing);

				logsServices.addLog(AppConstants.error, AppConstants.PARTITIONS, "deletePartitionsDetails", AppConstants.fieldsmissing,
						null);
				return new ResponseEntity<>(returnmap, HttpStatus.OK);

			}
 
			Partitions partitions = partitionsRepository.findAllById(Long.valueOf(requestMap.getOrDefault("id", "").toString()));
			
			if (partitions == null) {
				returnmap.put("status", "error");
				returnmap.put("message", "Partitions id not found");

				logsServices.addLog(AppConstants.error, AppConstants.PARTITIONS, "deletePartitionsDetails",
						returnmap.get("message").toString(), null);
				return new ResponseEntity<>(returnmap, HttpStatus.OK);
			}else if (!partitions.isActive()) {
				returnmap.put("status", "error");
				returnmap.put("message", "Partitions is not active");

				logsServices.addLog(AppConstants.error, AppConstants.PARTITIONS, "deletePartitionsDetails",
						returnmap.get("message").toString(), null);
				return new ResponseEntity<>(returnmap, HttpStatus.OK);
			}
			
 			 
			partitions.setActive(false);
			partitions.setModifiedDate(new Date());
			partitionsRepository.save(partitions);

			returnmap.put("status", "success");
			returnmap.put("message", partitions.getName() + " status updated successfully");

			logsServices.addLog(AppConstants.event, AppConstants.PARTITIONS, "deletePartitionsDetails",
					"Partitions -" + partitions.getName() + " status updated successfully, ", null);

			return new ResponseEntity<>(returnmap, HttpStatus.OK);
		} catch (Exception e) {
			returnmap.put("status", "error");
			returnmap.put("message", AppConstants.errormsg);

			logsServices.addLog(AppConstants.error, AppConstants.PARTITIONS, "deletePartitionsDetails", AppConstants.errormsg, null);
			return new ResponseEntity<>(returnmap, HttpStatus.OK);
		}
	}

}
