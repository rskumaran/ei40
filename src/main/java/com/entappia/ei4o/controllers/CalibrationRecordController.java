package com.entappia.ei4o.controllers;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.concurrent.ExecutionException;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import com.entappia.ei4o.constants.AppConstants;
import com.entappia.ei4o.dbmodels.CalibrationRecord;
import com.entappia.ei4o.repository.CalibrationRecordRepository;
import com.entappia.ei4o.services.LogsServices;
import com.entappia.ei4o.utils.BaseController;
import com.entappia.ei4o.utils.Utils;

@RestController
public class CalibrationRecordController extends BaseController {

	@Autowired
	CalibrationRecordRepository calibrationRecordRepository;

	@Autowired
	private LogsServices logsServices;

	@RequestMapping(path = { "calibrationRecord", "calibrationRecord/{calibrationId}" }, method = RequestMethod.GET)
	public ResponseEntity<?> getCalibrationRecord(HttpServletRequest request, HttpSession httpSession,
			@PathVariable Optional<String> calibrationId) throws InterruptedException, ExecutionException {
		Map<String, Object> returnmap = new HashMap<>();
		HashMap<String, String> reuestMap = new HashMap<>();
		try {
			// "ACAL"
			ResponseEntity<?> responseEntity = checkSessionKey(httpSession, Utils.getCookie(request, "JSESSIONID"));
			if (responseEntity != null) {

				logsServices.addLog(AppConstants.error, AppConstants.calibrationRecord, "getCalibrationRecord",
						AppConstants.sessiontimeout, null);
				return responseEntity;
			}

			JSONObject jsonObject = new JSONObject();
			if (calibrationId.isPresent()) {
				jsonObject.put("calibrationId", calibrationId.get());
			} else {
				jsonObject.put("calibrationId", "all");
			}
			reuestMap.put("params", jsonObject.toString());

			logsServices.addLog(AppConstants.event, AppConstants.calibrationRecord, "getCalibrationRecord",
					"Calibration Record get API Call", reuestMap);

			List<CalibrationRecord> calibrationRecords = new ArrayList<>();

			if (calibrationId.isPresent()) {
				CalibrationRecord calibrationRecord = calibrationRecordRepository
						.findByCalibrationId(calibrationId.get());
				if (calibrationRecord == null) {
					returnmap.put("status", "error");
					returnmap.put("message", "Department name does not exist");
					logsServices.addLog(AppConstants.error, AppConstants.calibrationRecord, "getCalibrationRecord",
							"department name does not exist", null);

					return new ResponseEntity<>(returnmap, HttpStatus.OK);
				}

				calibrationRecords.add(calibrationRecord);
			} else {
				calibrationRecords = calibrationRecordRepository.findAll();
			}
			logsServices.addLog(AppConstants.success, AppConstants.calibrationRecord, "getCalibrationRecord",
					"get Calibration Record details", null);

			returnmap.put("status", "success");
			returnmap.put("data", calibrationRecords);

			return new ResponseEntity<>(returnmap, HttpStatus.OK);
		} catch (Exception e) {
			returnmap.put("status", "error");
			returnmap.put("message", AppConstants.errormsg);
			logsServices.addLog(AppConstants.error, AppConstants.calibrationRecord, "getCalibrationRecord",
					AppConstants.errormsg, null);

			return new ResponseEntity<>(returnmap, HttpStatus.OK);
		}

	}

	@DeleteMapping(value = "/calibrationRecord")
	public ResponseEntity<?> deleteCalibrationRecord(HttpServletRequest request, HttpSession httpSession,
			@RequestBody HashMap<String, Object> requestMap) throws InterruptedException, ExecutionException {

		Map<String, String> returnmap = new HashMap<>();
		try {

			ResponseEntity<?> responseEntity = checkSessionKey(httpSession, Utils.getCookie(request, "JSESSIONID"),
					"ACAL");
			if (responseEntity != null) {

				logsServices.addLog(AppConstants.error, AppConstants.calibrationRecord, "deleteCalibrationRecord",
						AppConstants.sessiontimeout, null);
				return responseEntity;
			}

			Map<String, String> requestLogMap = new HashMap<>();
			requestLogMap.put("params", new JSONObject(requestMap).toString());

			logsServices.addLog(AppConstants.event, AppConstants.calibrationRecord, "deleteCalibrationRecord",
					"Calibration Record delete API Call", requestLogMap);

			if (Utils.isEmptyString(requestMap.getOrDefault("calibrationId", "").toString())) {

				returnmap.put("status", "error");
				returnmap.put("message", AppConstants.fieldsmissing);

				logsServices.addLog(AppConstants.error, AppConstants.calibrationRecord, "deleteCalibrationRecord",
						AppConstants.fieldsmissing, null);
				return new ResponseEntity<>(returnmap, HttpStatus.OK);

			}

			CalibrationRecord calibrationRecord = calibrationRecordRepository
					.findByCalibrationId(requestMap.getOrDefault("calibrationId", "").toString());

			if (calibrationRecord == null) {
				returnmap.put("status", "error");
				returnmap.put("message", "Calibration Id not found");

				logsServices.addLog(AppConstants.error, AppConstants.calibrationRecord, "deleteCalibrationRecord",
						returnmap.get("message").toString(), null);
				return new ResponseEntity<>(returnmap, HttpStatus.OK);
			} else if (!calibrationRecord.isActive()) {
				returnmap.put("status", "error");
				returnmap.put("message", "Calibration is not active");

				logsServices.addLog(AppConstants.error, AppConstants.calibrationRecord, "deleteCalibrationRecord",
						returnmap.get("message").toString(), null);
				return new ResponseEntity<>(returnmap, HttpStatus.OK);
			}

			calibrationRecord.setActive(false);
			calibrationRecordRepository.save(calibrationRecord);

			returnmap.put("status", "success");
			returnmap.put("message", "Calibration status updated successfully.");

			logsServices.addLog(AppConstants.event, AppConstants.calibrationRecord, "deleteCalibrationRecord",
					"Calibration Id-" + calibrationRecord.getCalibrationId() + " status updated successfully, ", null);

			return new ResponseEntity<>(returnmap, HttpStatus.OK);
		} catch (Exception e) {
			returnmap.put("status", "error");
			returnmap.put("message", AppConstants.errormsg);

			logsServices.addLog(AppConstants.error, AppConstants.calibrationRecord, "deleteCalibrationRecord",
					returnmap.get("message").toString(), null);
			return new ResponseEntity<>(returnmap, HttpStatus.OK);
		}
	}

	@PostMapping(value = "/calibrationRecord", consumes = { MediaType.MULTIPART_FORM_DATA_VALUE })
	public ResponseEntity<?> addCalibrationRecord(HttpServletRequest request, HttpSession httpSession,
			@RequestParam(name = "file", required = false) MultipartFile file,
			@RequestParam("calibrationId") String calibrationId, @RequestParam("asset") String asset,
			@RequestParam("calibrationExpiryDate") String calibrationExpiryDate)
			throws InterruptedException, ExecutionException {

		Map<String, String> returnmap = new HashMap<>();
		try {

			ResponseEntity<?> responseEntity = checkSessionKey(httpSession, Utils.getCookie(request, "JSESSIONID"),
					"ACAL");
			if (responseEntity != null) {
				logsServices.addLog(AppConstants.error, AppConstants.calibrationRecord, "addCalibrationRecord",
						AppConstants.sessiontimeout, null);
				return responseEntity;
			}

			JSONObject jsonObject = new JSONObject();
			jsonObject.put("calibrationId", calibrationId);
			jsonObject.put("asset", asset);
			jsonObject.put("calibrationExpiryDate", calibrationExpiryDate);

			Map<String, String> requestLogMap = new HashMap<>();
			requestLogMap.put("params", jsonObject.toString());

			logsServices.addLog(AppConstants.event, AppConstants.calibrationRecord, "addCalibrationRecord",
					"Calibration Record add API Call", requestLogMap);

			if (Utils.isEmptyString(calibrationId) || Utils.isEmptyString(asset)
					|| Utils.isEmptyString(calibrationExpiryDate)) {

				returnmap.put("status", "error");
				returnmap.put("message", AppConstants.fieldsmissing);

				logsServices.addLog(AppConstants.error, AppConstants.calibrationRecord, "addCalibrationRecord",
						AppConstants.fieldsmissing, null);
				return new ResponseEntity<>(returnmap, HttpStatus.OK);

			}

			if (!Utils.validateDate(calibrationExpiryDate)) {
				returnmap.put("status", "error");
				returnmap.put("message", AppConstants.errorDatemsg);

				logsServices.addLog(AppConstants.error, AppConstants.calibrationRecord, "addCalibrationRecord",
						AppConstants.fieldsmissing, null);
				return new ResponseEntity<>(returnmap, HttpStatus.OK);
			}

			CalibrationRecord calibrationRecord = calibrationRecordRepository.findByCalibrationId(calibrationId);

			if (calibrationRecord != null && calibrationRecord.isActive()) {
				returnmap.put("status", "error");
				returnmap.put("message", "Calibration details already exist");

				logsServices.addLog(AppConstants.error, AppConstants.calibrationRecord, "addCalibrationRecord",
						returnmap.get("message").toString(), null);
				return new ResponseEntity<>(returnmap, HttpStatus.OK);
			}

			if (calibrationRecord == null)
				calibrationRecord = new CalibrationRecord();
			calibrationRecord.setCalibrationId(calibrationId);
			calibrationRecord.setAsset(asset);

			Date date1 = new SimpleDateFormat("yyyy-MMM-dd").parse(calibrationExpiryDate);

			calibrationRecord.setCalibrationExpiryDate(date1);
			if (file != null)
				calibrationRecord.setCalibrationData(file.getBytes());
			calibrationRecord.setCreatedDate(new Date());
			calibrationRecord.setModifiedDate(new Date());
			calibrationRecord.setActive(true);
			calibrationRecordRepository.save(calibrationRecord);

			returnmap.put("status", "success");
			returnmap.put("message", "Calibration added successfully.");

			logsServices.addLog(AppConstants.event, AppConstants.calibrationRecord, "addCalibrationRecord",
					"Calibration Id-" + calibrationRecord.getCalibrationId() + " Calibration added successfully, ",
					null);

			return new ResponseEntity<>(returnmap, HttpStatus.OK);
		} catch (Exception e) {
			returnmap.put("status", "error");
			returnmap.put("message", AppConstants.errormsg);

			logsServices.addLog(AppConstants.error, AppConstants.calibrationRecord, "addCalibrationRecord",
					returnmap.get("message").toString(), null);
			return new ResponseEntity<>(returnmap, HttpStatus.OK);
		}
	}

	@PutMapping(value = "/calibrationRecord", consumes = { MediaType.MULTIPART_FORM_DATA_VALUE })
	public ResponseEntity<?> updateCalibrationRecord(HttpServletRequest request, HttpSession httpSession,
			@RequestParam(name = "file", required = false) MultipartFile file,
			@RequestParam("calibrationId") String calibrationId, @RequestParam("asset") String asset,
			@RequestParam("calibrationExpiryDate") String calibrationExpiryDate)
			throws InterruptedException, ExecutionException {

		Map<String, String> returnmap = new HashMap<>();
		try {

			ResponseEntity<?> responseEntity = checkSessionKey(httpSession, Utils.getCookie(request, "JSESSIONID"),
					"ACAL");
			if (responseEntity != null) {
				logsServices.addLog(AppConstants.error, AppConstants.calibrationRecord, "updateCalibrationRecord",
						AppConstants.sessiontimeout, null);
				return responseEntity;
			}
			
			JSONObject jsonObject = new JSONObject();
			jsonObject.put("calibrationId", calibrationId);
			jsonObject.put("asset", asset);
			jsonObject.put("calibrationExpiryDate", calibrationExpiryDate);

			Map<String, String> requestLogMap = new HashMap<>();
			requestLogMap.put("params", jsonObject.toString());

			logsServices.addLog(AppConstants.event, AppConstants.calibrationRecord, "updateCalibrationRecord",
					"Calibration Record update API Call", requestLogMap);

			if (Utils.isEmptyString(calibrationId) || Utils.isEmptyString(asset)
					|| Utils.isEmptyString(calibrationExpiryDate)) {

				returnmap.put("status", "error");
				returnmap.put("message", AppConstants.fieldsmissing);

				logsServices.addLog(AppConstants.error, AppConstants.calibrationRecord, "updateCalibrationRecord",
						AppConstants.fieldsmissing, null);
				return new ResponseEntity<>(returnmap, HttpStatus.OK);

			}

			if (!Utils.validateDate(calibrationExpiryDate)) {
				returnmap.put("status", "error");
				returnmap.put("message", AppConstants.errormsg);

				logsServices.addLog(AppConstants.error, AppConstants.calibrationRecord, "addCalibrationRecord",
						AppConstants.fieldsmissing, null);
				return new ResponseEntity<>(returnmap, HttpStatus.OK);
			}

			CalibrationRecord calibrationRecord = calibrationRecordRepository.findByCalibrationId(calibrationId);

			if (calibrationRecord == null) {
				returnmap.put("status", "error");
				returnmap.put("message", "Calibration id not found");

				logsServices.addLog(AppConstants.error, AppConstants.calibrationRecord, "updateCalibrationRecord",
						returnmap.get("message").toString(), null);
				return new ResponseEntity<>(returnmap, HttpStatus.OK);
			} else if (!calibrationRecord.isActive()) {
				returnmap.put("status", "error");
				returnmap.put("message", "Calibration details is not active");

				logsServices.addLog(AppConstants.error, AppConstants.calibrationRecord, "updateCalibrationRecord",
						returnmap.get("message").toString(), null);
				return new ResponseEntity<>(returnmap, HttpStatus.OK);
			}

			calibrationRecord.setCalibrationId(calibrationId);
			calibrationRecord.setAsset(asset);

			Date date1 = new SimpleDateFormat("yyyy-MMM-dd").parse(calibrationExpiryDate);

			calibrationRecord.setCalibrationExpiryDate(date1);
			if (file != null)
				calibrationRecord.setCalibrationData(file.getBytes());

			calibrationRecord.setModifiedDate(new Date());
			calibrationRecord.setActive(true);
			calibrationRecordRepository.save(calibrationRecord);

			returnmap.put("status", "success");
			returnmap.put("message", "Calibration updated successfully.");

			logsServices.addLog(AppConstants.event, AppConstants.calibrationRecord, "updateCalibrationRecord",
					"Calibration Id-" + calibrationRecord.getCalibrationId() + " Calibration updated successfully, ",
					null);

			return new ResponseEntity<>(returnmap, HttpStatus.OK);
		} catch (Exception e) {
			returnmap.put("status", "error");
			returnmap.put("message", AppConstants.errormsg);

			logsServices.addLog(AppConstants.error, AppConstants.calibrationRecord, "updateCalibrationRecord",
					returnmap.get("message").toString(), null);
			return new ResponseEntity<>(returnmap, HttpStatus.OK);
		}
	}
}
