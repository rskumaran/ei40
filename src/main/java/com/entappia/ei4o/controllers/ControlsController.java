package com.entappia.ei4o.controllers;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.concurrent.ExecutionException;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.entappia.ei4o.constants.AppConstants;
import com.entappia.ei4o.dbmodels.Controls;
import com.entappia.ei4o.repository.ControlsRepository;
import com.entappia.ei4o.services.LogsServices;
import com.entappia.ei4o.utils.BaseController;
import com.entappia.ei4o.utils.Utils;

@RestController
public class ControlsController extends BaseController {

	@Autowired
	private LogsServices logsServices;

	@Autowired
	private ControlsRepository controlsRepository;

	@RequestMapping(path = { "controls", "controls/{controlsId}" }, method = RequestMethod.GET)
	public ResponseEntity<?> getControls(HttpServletRequest request, HttpSession httpSession,
			@PathVariable Optional<String> controlsId) throws InterruptedException, ExecutionException {
		Map<String, Object> returnmap = new HashMap<>();
		HashMap<String, String> reuestMap = new HashMap<>();
		try {
			 
			
			ResponseEntity<?> responseEntity = checkSessionKey(httpSession, Utils.getCookie(request, "JSESSIONID"));
			if (responseEntity != null) {
				
				logsServices.addLog(AppConstants.error, AppConstants.CONTROLS, "getControls",
						AppConstants.sessiontimeout, null);
				return responseEntity; 
			}
			
			JSONObject jsonObject = new JSONObject();
			if (controlsId.isPresent()) {
				jsonObject.put("controlsId", controlsId.get());
			} else {
				jsonObject.put("controlsId", "all");
			}

			reuestMap.put("params", jsonObject.toString()); 
			logsServices.addLog(AppConstants.event, AppConstants.CONTROLS, "getControls",
					"Controls get API Details", reuestMap);
			

			List<Controls> controlsList = new ArrayList<>();

			if (controlsId.isPresent()) {
				Controls controls = controlsRepository.findByControlId(controlsId.get());
				if (controls == null) {
					returnmap.put("status", "error");
					returnmap.put("message", "control id does not exist");
					logsServices.addLog(AppConstants.error, AppConstants.CONTROLS, "getControls",
							"control id does not exist", null);

					return new ResponseEntity<>(returnmap, HttpStatus.OK);
				}

				controlsList.add(controls);
			} else {
				controlsList = controlsRepository.findAll();
			}
			logsServices.addLog(AppConstants.success, AppConstants.CONTROLS, "getControls", "control details", null);

			returnmap.put("status", "success");
			returnmap.put("data", controlsList);

			return new ResponseEntity<>(returnmap, HttpStatus.OK);
		} catch (Exception e) {
			returnmap.put("status", "error");
			returnmap.put("message", AppConstants.errormsg);
			logsServices.addLog(AppConstants.error, AppConstants.CONTROLS, "getControls", AppConstants.errormsg, null);

			return new ResponseEntity<>(returnmap, HttpStatus.OK);
		}

	}

	@PostMapping(value = "/controls")
	public ResponseEntity<?> addControls(HttpServletRequest request, HttpSession httpSession,

			@RequestBody HashMap<String, Object> requestMap) throws InterruptedException, ExecutionException {
		Map<String, String> returnmap = new HashMap<>();

		try {

			ResponseEntity<?> responseEntity = checkSessionKey(httpSession, Utils.getCookie(request, "JSESSIONID"));
			if (responseEntity != null) {
				
				logsServices.addLog(AppConstants.error, AppConstants.CONTROLS, "addControls",
						AppConstants.sessiontimeout, null);
				return responseEntity; 
			}
			
			Map<String, String> requestLogMap = new HashMap<>(); 
			requestLogMap.put("params", new JSONObject(requestMap).toString());
 
			logsServices.addLog(AppConstants.event, AppConstants.CONTROLS, "addControls",
					"Controls add API Details", requestLogMap);
			
			if (Utils.isEmptyString(requestMap.getOrDefault("controlId", "").toString())
					|| Utils.isEmptyString(requestMap.getOrDefault("tags", "").toString())) {

				returnmap.put("status", "error");
				returnmap.put("message", AppConstants.fieldsmissing);

				logsServices.addLog(AppConstants.error, AppConstants.CONTROLS, "addControls",
						AppConstants.fieldsmissing, null);
				return new ResponseEntity<>(returnmap, HttpStatus.OK);

			}

			Controls controls = controlsRepository.findByControlId(requestMap.getOrDefault("controlId", "").toString());
			if (controls != null) {
				returnmap.put("status", "error");
				if (!controls.isActive())
					returnmap.put("message", "Control is not active");
				else
					returnmap.put("message", "Control id already exist");

				logsServices.addLog(AppConstants.error, AppConstants.CONTROLS, "addControls",
						returnmap.get("message").toString(), null);
				return new ResponseEntity<>(returnmap, HttpStatus.OK);
			}

			JSONArray operatorJSONArray = new JSONArray(requestMap.getOrDefault("tags", "").toString());

			List<String> tagsList = new ArrayList<>();

			if (operatorJSONArray != null && operatorJSONArray.length() > 0) {

				for (int i = 0; i < operatorJSONArray.length(); i++) {
					String val = operatorJSONArray.getString(i);
					tagsList.add(val);
				}
			}

			controls = new Controls();
			controls.setControlId(requestMap.getOrDefault("controlId", "").toString());
			controls.setTags(tagsList);
			controls.setActive(true);
			controls.setCreatedDate(new Date());
			controls.setModifiedDate(new Date());

			controlsRepository.save(controls);

			returnmap.put("status", "success");
			returnmap.put("message", controls.getControlId() + " added successfully");

			logsServices.addLog(AppConstants.event, AppConstants.CONTROLS, "addControls",
					"Controls Id -" + controls.getControlId() + " added successfully, ", null);

			return new ResponseEntity<>(returnmap, HttpStatus.OK);
		} catch (Exception e) {
			returnmap.put("status", "error");
			returnmap.put("message", AppConstants.errormsg);

			logsServices.addLog(AppConstants.error, AppConstants.CONTROLS, "addControls", AppConstants.errormsg, null);
			return new ResponseEntity<>(returnmap, HttpStatus.OK);
		}
	}

	@PutMapping(value = "/controls")
	public ResponseEntity<?> updateControls(HttpServletRequest request, HttpSession httpSession,

			@RequestBody HashMap<String, Object> requestMap) throws InterruptedException, ExecutionException {
		Map<String, String> returnmap = new HashMap<>();

		try {
			
			ResponseEntity<?> responseEntity = checkSessionKey(httpSession, Utils.getCookie(request, "JSESSIONID"));
			if (responseEntity != null) {
				
				logsServices.addLog(AppConstants.error, AppConstants.CONTROLS, "updateControls",
						AppConstants.sessiontimeout, null);
				return responseEntity; 
			}
			
			Map<String, String> requestLogMap = new HashMap<>(); 
			requestLogMap.put("params", new JSONObject(requestMap).toString());
 
			logsServices.addLog(AppConstants.event, AppConstants.CONTROLS, "updateControls",
					"Controls update API Details", requestLogMap);
 
			if (Utils.isEmptyString(requestMap.getOrDefault("controlId", "").toString())
					|| Utils.isEmptyString(requestMap.getOrDefault("tags", "").toString())) {

				returnmap.put("status", "error");
				returnmap.put("message", AppConstants.fieldsmissing);

				logsServices.addLog(AppConstants.error, AppConstants.CONTROLS, "updateControls",
						AppConstants.fieldsmissing, null);
				return new ResponseEntity<>(returnmap, HttpStatus.OK);

			}

			Controls controls = controlsRepository.findByControlId(requestMap.getOrDefault("controlId", "").toString());
			if (controls == null) {
				returnmap.put("status", "error");
				returnmap.put("message", "Control id not exist");

				logsServices.addLog(AppConstants.error, AppConstants.CONTROLS, "updateControls",
						returnmap.get("message").toString(), null);
				return new ResponseEntity<>(returnmap, HttpStatus.OK);
			} else if (!controls.isActive()) {
				returnmap.put("status", "error");
				returnmap.put("message", "Control is not active");

				logsServices.addLog(AppConstants.error, AppConstants.CONTROLS, "updateControls",
						returnmap.get("message").toString(), null);
				return new ResponseEntity<>(returnmap, HttpStatus.OK);
			}

			 
			JSONArray operatorJSONArray = new JSONArray(requestMap.getOrDefault("tags", "").toString());

			List<String> tagsList = new ArrayList<>();

			if (operatorJSONArray != null && operatorJSONArray.length() > 0) {

				for (int i = 0; i < operatorJSONArray.length(); i++) {
					String val = operatorJSONArray.getString(i);
					tagsList.add(val);
				}
			}
 
			controls.setControlId(requestMap.getOrDefault("controlId", "").toString());
			controls.setTags(tagsList); 
			controls.setModifiedDate(new Date());

			controlsRepository.save(controls);
			
			
			returnmap.put("status", "success");
			returnmap.put("message", controls.getControlId() + " updated successfully");

			logsServices.addLog(AppConstants.event, AppConstants.CONTROLS, "updateControls",
					"Controls ID -" + controls.getControlId() + " updated successfully, ", null);

			return new ResponseEntity<>(returnmap, HttpStatus.OK);
		} catch (Exception e) {
			returnmap.put("status", "error");
			returnmap.put("message", AppConstants.errormsg);

			logsServices.addLog(AppConstants.error, AppConstants.CONTROLS, "updateControls", AppConstants.errormsg, null);
			return new ResponseEntity<>(returnmap, HttpStatus.OK);
		}
	}

	@DeleteMapping(value = "/controls")
	public ResponseEntity<?> deleteControls(HttpServletRequest request, HttpSession httpSession,

			@RequestBody HashMap<String, Object> requestMap) throws InterruptedException, ExecutionException {
		Map<String, String> returnmap = new HashMap<>();

		try {

			ResponseEntity<?> responseEntity = checkSessionKey(httpSession, Utils.getCookie(request, "JSESSIONID"));
			if (responseEntity != null) {
				
				logsServices.addLog(AppConstants.error, AppConstants.CONTROLS, "deleteControls",
						AppConstants.sessiontimeout, null);
				return responseEntity; 
			}
			
			Map<String, String> requestLogMap = new HashMap<>(); 
			requestLogMap.put("params", new JSONObject(requestMap).toString());
 
			logsServices.addLog(AppConstants.event, AppConstants.CONTROLS, "deleteControls",
					"Controls delete API Details", requestLogMap);
			
			if (Utils.isEmptyString(requestMap.getOrDefault("controlId", "").toString())) {

				returnmap.put("status", "error");
				returnmap.put("message", AppConstants.fieldsmissing);
 
				logsServices.addLog(AppConstants.error, AppConstants.CONTROLS, "deleteControls",
						AppConstants.fieldsmissing, null);
				return new ResponseEntity<>(returnmap, HttpStatus.OK);

			}

			Controls controls = controlsRepository.findByControlId(requestMap.getOrDefault("controlId", "").toString());
			if (controls == null) {
				returnmap.put("status", "error");
				returnmap.put("message", "Control id not exist");

				logsServices.addLog(AppConstants.error, AppConstants.CONTROLS, "deleteControls",
						returnmap.get("message").toString(), null);
				return new ResponseEntity<>(returnmap, HttpStatus.OK);
			} else if (!controls.isActive()) {
				returnmap.put("status", "error");
				returnmap.put("message", "Control is not active");

				logsServices.addLog(AppConstants.error, AppConstants.CONTROLS, "deleteControls",
						returnmap.get("message").toString(), null);
				return new ResponseEntity<>(returnmap, HttpStatus.OK);
			}

			controls.setActive(false);
			controls.setModifiedDate(new Date());

			controlsRepository.save(controls);

			returnmap.put("status", "success");
			returnmap.put("message", controls.getControlId() + " status Deleted successfully");

			logsServices.addLog(AppConstants.event, AppConstants.CONTROLS, "deleteControls",
					"Control ID -" + controls.getControlId() + " status Deleted successfully, ", null);

			return new ResponseEntity<>(returnmap, HttpStatus.OK);
		} catch (Exception e) {
			returnmap.put("status", "error");
			returnmap.put("message", AppConstants.errormsg);

			logsServices.addLog(AppConstants.error, AppConstants.CONTROLS, "deleteControls", AppConstants.errormsg,
					null);
			return new ResponseEntity<>(returnmap, HttpStatus.OK);
		}
	}
}
