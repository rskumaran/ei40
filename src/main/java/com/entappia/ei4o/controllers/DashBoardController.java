package com.entappia.ei4o.controllers;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ExecutionException;

import javax.mail.Session;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.entappia.ei4o.constants.AppConstants;
import com.entappia.ei4o.dbmodels.Tags;
import com.entappia.ei4o.repository.TagsRepository;
import com.entappia.ei4o.services.LogsServices;
import com.entappia.ei4o.utils.BaseController;
import com.entappia.ei4o.utils.SessionCount;
import com.entappia.ei4o.utils.Utils;

@RestController
public class DashBoardController extends BaseController {

	@Autowired
	private LogsServices logsServices;
	
	@Autowired
	private TagsRepository tagsRepository;
	
	@GetMapping(value = "/dashboardInfo")
	public ResponseEntity<?> dashboardInfo(HttpServletRequest request, HttpSession httpSession) throws InterruptedException, ExecutionException {
		Map<String, Object> returnmap = new HashMap<>();
		try {

			ResponseEntity<?> responseEntity = checkSessionKey(httpSession, Utils.getCookie(request, "JSESSIONID"));
			if (responseEntity != null) { 
				logsServices.addLog(AppConstants.error, AppConstants.dashboardApi, "dashboardInfo",
						AppConstants.sessiontimeout, null);
				return responseEntity; 
			}
			

			Map<String, String> reuestMap = new HashMap<>();
			reuestMap.put("params", "Dashboard Info get API call"); 
			
			logsServices.addLog(AppConstants.event, AppConstants.dashboardApi, "dashboardInfo",
					"Dashboard Info get API call", reuestMap);
			
			
			
			List<Tags> tagList = tagsRepository.findAll();
			
			int employeeCount = 0;
			int visitorCount = 0;
			int vendorCount = 0;
			int contractorCount = 0;
			int assetChronoCount = 0;
			int assetNonChronoCount = 0;
			int vehiclesCount = 0;
			int materialsCount = 0;
			int skuCount = 0;
			 
			if(tagList!=null) {
				for(Tags tag: tagList) {
					if(tag!=null && tag.getStatus()!=null && tag.getType()!=null 
							&& tag.getStatus().equalsIgnoreCase("Assigned"))
					{
						if(tag.getType().equalsIgnoreCase("Employee"))
						{
							employeeCount++;
						}else if(tag.getType().equalsIgnoreCase("Visitor"))
						{
							visitorCount++;
						}else if(tag.getType().equalsIgnoreCase("Vendor"))
						{
							vendorCount++;
						}else if(tag.getType().equalsIgnoreCase("Contractor"))
						{
							contractorCount++;
						}else if(tag.getType().equalsIgnoreCase("Asset Chrono"))
						{
							assetChronoCount++;
						}else if(tag.getType().equalsIgnoreCase("Asset NonChrono"))
						{
							assetNonChronoCount++;
						}else if(tag.getType().equalsIgnoreCase("Vehicles"))
						{
							vehiclesCount++;
						}else if(tag.getType().equalsIgnoreCase("Materials"))
						{
							materialsCount++;
						}else if(tag.getType().equalsIgnoreCase("LSLM") 
								||tag.getType().equalsIgnoreCase("Inventory"))
						{
							skuCount++;
						}
					}
				}
			}
			
			HashMap<String, Integer> mapCount = new HashMap<>();
			mapCount.put("employee", employeeCount);
			mapCount.put("visitor", visitorCount);
			mapCount.put("vendor", vendorCount);
			mapCount.put("contractor", contractorCount);
			mapCount.put("assetChrono", assetChronoCount);
			mapCount.put("assetNonChrono", assetNonChronoCount);
			mapCount.put("vehicles", vehiclesCount);
			mapCount.put("materials", materialsCount); 
			mapCount.put("sku", skuCount);
			 
			  
			HashMap<String, Object> dataMap = new HashMap<>();
			dataMap.put("activeSessions", SessionCount.numberOfUsersOnline);
			dataMap.put("assignedTags", mapCount);
			returnmap.put("status", "success");
			returnmap.put("data", dataMap);
			 

			logsServices.addLog(AppConstants.event, AppConstants.dashboardApi, "dashboardInfo",
					"Dashboard Info get API data sent successfully", null);
			
			return new ResponseEntity<>(returnmap, HttpStatus.OK);
			
		} catch (Exception e) {
			e.printStackTrace();
			returnmap.put("status", "error");
			returnmap.put("message", AppConstants.errormsg);

			logsServices.addLog(AppConstants.error, AppConstants.dashboardApi, "dashboardInfo", AppConstants.errormsg,
					null);
			return new ResponseEntity<>(returnmap, HttpStatus.OK);
		}
	}

}
