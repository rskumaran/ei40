package com.entappia.ei4o.controllers;

import java.io.File;
import java.util.HashMap;
import java.util.Optional;
import java.util.Random;
import java.util.ResourceBundle;
import java.util.concurrent.ExecutionException;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.entappia.ei4o.constants.AppConstants;
import com.entappia.ei4o.utils.BaseController;
import com.entappia.ei4o.utils.Utils;

@RestController
public class DemoController extends BaseController {

	@PostMapping(value = "demo/productivity")
	public ResponseEntity<?> getProductivityDetails(HttpServletRequest request, HttpSession httpSession,
			@RequestBody HashMap<String, Object> reqMap) throws InterruptedException, ExecutionException {
		HashMap<String, Object> returnmap = new HashMap<>();

		/*
		 * ResponseEntity<?> responseEntity = checkSessionKey(httpSession,
		 * Utils.getCookie(request, "JSESSIONID"), "", ""); if (responseEntity != null)
		 * { LoggerTable logger = new LoggerTable(Utils.getFormatedDateLog(new Date()),
		 * AppConstants.error, httpSession, AppConstants.sessiontimeout,
		 * AppConstants.demo, "demo_mapdetails", returnmap, "",
		 * request.getHeader("orgName")); Utils.AddLog(amazonDynamoDB, logger, false);
		 * return responseEntity; }
		 */

		String orgName = request.getHeader("orgName");
		if (Utils.isEmptyString(orgName)) {
			returnmap.put("status", "error");
			returnmap.put("message", AppConstants.fieldsmissing);
			return new ResponseEntity<>(returnmap, HttpStatus.OK);
		}

		if (!reqMap.containsKey("zone") || !reqMap.containsKey("days")) {
			returnmap.put("status", "error");
			returnmap.put("message", AppConstants.fieldsmissing);
			return new ResponseEntity<>(returnmap, HttpStatus.OK);
		}

		String zone = reqMap.get("zone").toString();
		String days = reqMap.get("days").toString();

		String dir = getDirectory();
		String dirPath = "";
		if (zone.equalsIgnoreCase("all")) {
			dirPath = dir + "/" + orgName + "/productivity/productivityall/" + days;
		} else
			dirPath = dir + "/" + orgName + "/productivity/productivityzone/" + days;
		File f = new File(dirPath);

		if (!f.isDirectory()) {
			returnmap.put("status", "error");
			returnmap.put("message", AppConstants.fieldsmissing);
			return new ResponseEntity<>(returnmap, HttpStatus.OK);
		}

		String filePath = "";
		if (zone.equalsIgnoreCase("all")) {
			filePath = dirPath + "/allzones.json";
		} else
			filePath = dirPath + "/" + zone + ".json";

		File f1 = new File(filePath);
		if (!f1.isFile()) {
			returnmap.put("status", "error");
			returnmap.put("message", "No data found for " + zone);
			return new ResponseEntity<>(returnmap, HttpStatus.OK);
		}

		String details = Utils.getDataFromFile(f1.getAbsolutePath());

		return new ResponseEntity<>(details, HttpStatus.OK);
	}

	@PostMapping(value = "demo/ctdetails")
	public ResponseEntity<?> getctDetails(HttpServletRequest request, HttpSession httpSession,
			@RequestBody HashMap<String, Object> reqMap) throws InterruptedException, ExecutionException {
		HashMap<String, Object> returnmap = new HashMap<>();

		/*
		 * ResponseEntity<?> responseEntity = checkSessionKey(httpSession,
		 * Utils.getCookie(request, "JSESSIONID"), "", ""); if (responseEntity != null)
		 * { LoggerTable logger = new LoggerTable(Utils.getFormatedDateLog(new Date()),
		 * AppConstants.error, httpSession, AppConstants.sessiontimeout,
		 * AppConstants.demo, "demo_mapdetails", returnmap, "",
		 * request.getHeader("orgName")); Utils.AddLog(amazonDynamoDB, logger, false);
		 * return responseEntity; }
		 */

		String orgName = request.getHeader("orgName");
		if (Utils.isEmptyString(orgName)) {
			returnmap.put("status", "error");
			returnmap.put("message", AppConstants.fieldsmissing);
			return new ResponseEntity<>(returnmap, HttpStatus.OK);
		}

		if (!reqMap.containsKey("userId") || !reqMap.containsKey("days")) {
			returnmap.put("status", "error");
			returnmap.put("message", AppConstants.fieldsmissing);
			return new ResponseEntity<>(returnmap, HttpStatus.OK);
		}

		String userId = reqMap.get("userId").toString();
		String days = reqMap.get("days").toString();

		String dir = getDirectory();
		File f = new File(dir + "/" + orgName + "/ctdays/" + days);

		if (!f.isDirectory()) {
			returnmap.put("status", "error");
			returnmap.put("message", AppConstants.fieldsmissing);
			return new ResponseEntity<>(returnmap, HttpStatus.OK);
		}

		File f1 = new File(dir + "/" + orgName + "/ctdays/" + days + "/" + userId + ".json");
		if (!f1.isFile()) {
			returnmap.put("status", "error");
			returnmap.put("message", AppConstants.fieldsmissing);
			return new ResponseEntity<>(returnmap, HttpStatus.OK);
		}

		String details = Utils.getDataFromFile(f1.getAbsolutePath());

		return new ResponseEntity<>(details, HttpStatus.OK);
	}

	@PostMapping(value = "demo/ctData")
	public ResponseEntity<?> getCtData(HttpServletRequest request, HttpSession httpSession)
			throws InterruptedException, ExecutionException {
		HashMap<String, Object> returnmap = new HashMap<>();
		/*
		 * ResponseEntity<?> responseEntity = checkSessionKey(httpSession,
		 * Utils.getCookie(request, "JSESSIONID"), "", ""); if (responseEntity != null)
		 * { LoggerTable logger = new
		 * 
		 * LoggerTable(Utils.getFormatedDateLog(new Date()), AppConstants.error,
		 * httpSession, AppConstants.sessiontimeout, AppConstants.demo,
		 * "demo_mapdetails", returnmap, "", request.getHeader("orgName"));
		 * Utils.AddLog(amazonDynamoDB, logger, false); return responseEntity; }
		 */

		String orgName = request.getHeader("orgName");
		if (Utils.isEmptyString(orgName)) {
			returnmap.put("status", "error");
			returnmap.put("message", AppConstants.fieldsmissing);
			return new ResponseEntity<>(returnmap, HttpStatus.OK);
		}

		String dir = getDirectory();
		File f = new File(dir + "/" + orgName);

		if (!f.isDirectory()) {
			returnmap.put("status", "error");
			returnmap.put("message", AppConstants.fieldsmissing);
			return new ResponseEntity<>(returnmap, HttpStatus.OK);
		}

		File f1 = new File(dir + "/" + orgName + "/Emp1.json");
		if (!f1.isFile()) {
			returnmap.put("status", "error");
			returnmap.put("message", AppConstants.fieldsmissing);
			return new ResponseEntity<>(returnmap, HttpStatus.OK);
		}

		String details = Utils.getDataFromFile(f1.getAbsolutePath());

		return new ResponseEntity<>(details, HttpStatus.OK);
	}

	@PostMapping(value = "demo/dwelldetails")
	public ResponseEntity<?> getDemDwellDetails(HttpServletRequest request, HttpSession httpSession,
			@RequestBody HashMap<String, Object> reqMap) throws InterruptedException, ExecutionException {
		HashMap<String, Object> returnmap = new HashMap<>();

		/*
		 * ResponseEntity<?> responseEntity = checkSessionKey(httpSession,
		 * Utils.getCookie(request, "JSESSIONID"), "", ""); if (responseEntity != null)
		 * { LoggerTable logger = new LoggerTable(Utils.getFormatedDateLog(new Date()),
		 * AppConstants.error, httpSession, AppConstants.sessiontimeout,
		 * AppConstants.demo, "demo_mapdetails", returnmap, "",
		 * request.getHeader("orgName")); Utils.AddLog(amazonDynamoDB, logger, false);
		 * return responseEntity; }
		 */

		String orgName = request.getHeader("orgName");
		if (Utils.isEmptyString(orgName)) {
			returnmap.put("status", "error");
			returnmap.put("message", AppConstants.fieldsmissing);
			return new ResponseEntity<>(returnmap, HttpStatus.OK);
		}

		if (!reqMap.containsKey("userId") || !reqMap.containsKey("days")) {
			returnmap.put("status", "error");
			returnmap.put("message", AppConstants.fieldsmissing);
			return new ResponseEntity<>(returnmap, HttpStatus.OK);
		}

		String userId = reqMap.get("userId").toString();
		String days = reqMap.get("days").toString();

		String dir = getDirectory();
		File f = new File(dir + "/" + orgName + "/dwellop/" + days);

		if (!f.isDirectory()) {
			returnmap.put("status", "error");
			returnmap.put("message", AppConstants.fieldsmissing);
			return new ResponseEntity<>(returnmap, HttpStatus.OK);
		}

		File f1 = new File(dir + "/" + orgName + "/dwellop/" + days + "/" + userId + ".json");
		if (!f1.isFile()) {
			returnmap.put("status", "error");
			returnmap.put("message", AppConstants.fieldsmissing);
			return new ResponseEntity<>(returnmap, HttpStatus.OK);
		}

		String details = Utils.getDataFromFile(f1.getAbsolutePath());

		return new ResponseEntity<>(details, HttpStatus.OK);
	}
	
	@PostMapping(value = "demo/arrivaldetails")
	public ResponseEntity<?> getDemArrivalDetails(HttpServletRequest request, HttpSession httpSession,
			@RequestBody HashMap<String, Object> reqMap) throws InterruptedException, ExecutionException {
		HashMap<String, Object> returnmap = new HashMap<>();

		/*
		 * ResponseEntity<?> responseEntity = checkSessionKey(httpSession,
		 * Utils.getCookie(request, "JSESSIONID"), "", ""); if (responseEntity != null)
		 * { LoggerTable logger = new LoggerTable(Utils.getFormatedDateLog(new Date()),
		 * AppConstants.error, httpSession, AppConstants.sessiontimeout,
		 * AppConstants.demo, "demo_mapdetails", returnmap, "",
		 * request.getHeader("orgName")); Utils.AddLog(amazonDynamoDB, logger, false);
		 * return responseEntity; }
		 */

		String orgName = request.getHeader("orgName");
		if (Utils.isEmptyString(orgName)) {
			returnmap.put("status", "error");
			returnmap.put("message", AppConstants.fieldsmissing);
			return new ResponseEntity<>(returnmap, HttpStatus.OK);
		}

		if (!reqMap.containsKey("zoneId") || !reqMap.containsKey("days")) {
			returnmap.put("status", "error");
			returnmap.put("message", AppConstants.fieldsmissing);
			return new ResponseEntity<>(returnmap, HttpStatus.OK);
		}

		String zoneId = reqMap.get("zoneId").toString();
		String days = reqMap.get("days").toString();

		String dir = getDirectory();
		File f = new File(dir + "/" + orgName + "/arrival/" + days);

		if (!f.isDirectory()) {
			returnmap.put("status", "error");
			returnmap.put("message", AppConstants.fieldsmissing);
			return new ResponseEntity<>(returnmap, HttpStatus.OK);
		}

		File f1 = new File(dir + "/" + orgName + "/arrival/" + days + "/" + zoneId + ".json");
		if (!f1.isFile()) {
			returnmap.put("status", "error");
			returnmap.put("message", AppConstants.fieldsmissing);
			return new ResponseEntity<>(returnmap, HttpStatus.OK);
		}

		String details = Utils.getDataFromFile(f1.getAbsolutePath());

		return new ResponseEntity<>(details, HttpStatus.OK);
	}


	@GetMapping(value = "demo/allusers")
	public ResponseEntity<?> getAllusers(HttpServletRequest request, HttpSession httpSession)
			throws InterruptedException, ExecutionException {
		HashMap<String, Object> returnmap = new HashMap<>();

		String orgName = request.getHeader("orgName");
		if (Utils.isEmptyString(orgName)) {
			returnmap.put("status", "error");
			returnmap.put("message", AppConstants.fieldsmissing);
			return new ResponseEntity<>(returnmap, HttpStatus.OK);
		}

		String dir = getDirectory();
		File f = new File(dir + "/" + orgName);

		if (!f.isDirectory()) {
			returnmap.put("status", "error");
			returnmap.put("message", AppConstants.fieldsmissing);
			return new ResponseEntity<>(returnmap, HttpStatus.OK);
		}

		File f1 = new File(dir + "/" + orgName + "/Allusers.json");
		if (!f1.isFile()) {
			returnmap.put("status", "error");
			returnmap.put("message", AppConstants.fieldsmissing);
			return new ResponseEntity<>(returnmap, HttpStatus.OK);
		}

		String details = Utils.getDataFromFile(f1.getAbsolutePath());

		return new ResponseEntity<>(details, HttpStatus.OK);
	}

	@GetMapping(value = "demo/occupancy")
	public ResponseEntity<?> getZoneoccupancy(HttpServletRequest request, HttpSession httpSession)
			throws InterruptedException, ExecutionException {
		HashMap<String, Object> returnmap = new HashMap<>();

		String orgName = request.getHeader("orgName");
		if (Utils.isEmptyString(orgName)) {
			returnmap.put("status", "error");
			returnmap.put("message", AppConstants.fieldsmissing);
			return new ResponseEntity<>(returnmap, HttpStatus.OK);
		}

		String dir = getDirectory();
		File f = new File(dir + "/" + orgName);

		if (!f.isDirectory()) {
			returnmap.put("status", "error");
			returnmap.put("message", AppConstants.fieldsmissing);
			return new ResponseEntity<>(returnmap, HttpStatus.OK);
		}

		File f1 = new File(dir + "/" + orgName + "/occupancy.json");
		if (!f1.isFile()) {
			returnmap.put("status", "error");
			returnmap.put("message", AppConstants.fieldsmissing);
			return new ResponseEntity<>(returnmap, HttpStatus.OK);
		}

		String details = Utils.getDataFromFile(f1.getAbsolutePath());

		return new ResponseEntity<>(details, HttpStatus.OK);
	}

	@GetMapping(value = "demo/Footfall_ArrivalRate")
	public ResponseEntity<?> getZonefootfall(HttpServletRequest request, HttpSession httpSession)
			throws InterruptedException, ExecutionException {
		HashMap<String, Object> returnmap = new HashMap<>();

		String orgName = request.getHeader("orgName");
		if (Utils.isEmptyString(orgName)) {
			returnmap.put("status", "error");
			returnmap.put("message", AppConstants.fieldsmissing);
			return new ResponseEntity<>(returnmap, HttpStatus.OK);
		}

		String dir = getDirectory();
		File f = new File(dir + "/" + orgName);

		if (!f.isDirectory()) {
			returnmap.put("status", "error");
			returnmap.put("message", AppConstants.fieldsmissing);
			return new ResponseEntity<>(returnmap, HttpStatus.OK);
		}

		Random rn = new Random();
		int answer = rn.nextInt(2) + 1; 
		File f1 = new File(dir + "/" + orgName + "/Footfall_ArrivalRate" + answer + ".json");

		if (!f1.isFile()) {
			returnmap.put("status", "error");
			returnmap.put("message", AppConstants.fieldsmissing);
			return new ResponseEntity<>(returnmap, HttpStatus.OK);
		}

		String details = Utils.getDataFromFile(f1.getAbsolutePath());

		return new ResponseEntity<>(details, HttpStatus.OK);
	}

	@GetMapping(value = "demo/Attendance")
	public ResponseEntity<?> getZoneAttendance(HttpServletRequest request, HttpSession httpSession)
			throws InterruptedException, ExecutionException {
		HashMap<String, Object> returnmap = new HashMap<>();

		String orgName = request.getHeader("orgName");
		if (Utils.isEmptyString(orgName)) {
			returnmap.put("status", "error");
			returnmap.put("message", AppConstants.fieldsmissing);
			return new ResponseEntity<>(returnmap, HttpStatus.OK);
		}

		String dir = getDirectory();
		File f = new File(dir + "/" + orgName);

		if (!f.isDirectory()) {
			returnmap.put("status", "error");
			returnmap.put("message", AppConstants.fieldsmissing);
			return new ResponseEntity<>(returnmap, HttpStatus.OK);
		}

		File f1 = new File(dir + "/" + orgName + "/Attendance.json");
		if (!f1.isFile()) {
			returnmap.put("status", "error");
			returnmap.put("message", AppConstants.fieldsmissing);
			return new ResponseEntity<>(returnmap, HttpStatus.OK);
		}

		String details = Utils.getDataFromFile(f1.getAbsolutePath());

		return new ResponseEntity<>(details, HttpStatus.OK);
	}

	@GetMapping(value = "demo/Attendance1")
	public ResponseEntity<?> getZoneAttendance1(HttpServletRequest request, HttpSession httpSession)
			throws InterruptedException, ExecutionException {
		HashMap<String, Object> returnmap = new HashMap<>();

		String orgName = request.getHeader("orgName");
		if (Utils.isEmptyString(orgName)) {
			returnmap.put("status", "error");
			returnmap.put("message", AppConstants.fieldsmissing);
			return new ResponseEntity<>(returnmap, HttpStatus.OK);
		}

		String dir = getDirectory();
		File f = new File(dir + "/" + orgName);

		if (!f.isDirectory()) {
			returnmap.put("status", "error");
			returnmap.put("message", AppConstants.fieldsmissing);
			return new ResponseEntity<>(returnmap, HttpStatus.OK);
		}

		File f1 = new File(dir + "/" + orgName + "/Attendance1.json");
		if (!f1.isFile()) {
			returnmap.put("status", "error");
			returnmap.put("message", AppConstants.fieldsmissing);
			return new ResponseEntity<>(returnmap, HttpStatus.OK);
		}

		String details = Utils.getDataFromFile(f1.getAbsolutePath());

		return new ResponseEntity<>(details, HttpStatus.OK);
	}

	@GetMapping(value = "demo/zones")
	public ResponseEntity<?> getZones(HttpServletRequest request, HttpSession httpSession)
			throws InterruptedException, ExecutionException {
		HashMap<String, Object> returnmap = new HashMap<>();

		String orgName = request.getHeader("orgName");
		if (Utils.isEmptyString(orgName)) {
			returnmap.put("status", "error");
			returnmap.put("message", AppConstants.fieldsmissing);
			return new ResponseEntity<>(returnmap, HttpStatus.OK);
		}

		String dir = getDirectory();
		File f = new File(dir + "/" + orgName);

		if (!f.isDirectory()) {
			returnmap.put("status", "error");
			returnmap.put("message", AppConstants.fieldsmissing);
			return new ResponseEntity<>(returnmap, HttpStatus.OK);
		}

		File f1 = new File(dir + "/" + orgName + "/zones.json");
		if (!f1.isFile()) {
			returnmap.put("status", "error");
			returnmap.put("message", AppConstants.fieldsmissing);
			return new ResponseEntity<>(returnmap, HttpStatus.OK);
		}

		String details = Utils.getDataFromFile(f1.getAbsolutePath());

		return new ResponseEntity<>(details, HttpStatus.OK);
	}

	@GetMapping(value = "demo/mapdetails")
	public ResponseEntity<?> getDemoMapDetails(HttpServletRequest request, HttpSession httpSession)
			throws InterruptedException, ExecutionException {
		HashMap<String, Object> returnmap = new HashMap<>();

		/*
		 * ResponseEntity<?> responseEntity = checkSessionKey(httpSession,
		 * Utils.getCookie(request, "JSESSIONID"), "", ""); if (responseEntity != null)
		 * { LoggerTable logger = new LoggerTable(Utils.getFormatedDateLog(new Date()),
		 * AppConstants.error, httpSession, AppConstants.sessiontimeout,
		 * AppConstants.demo, "demo_mapdetails", returnmap, "",
		 * request.getHeader("orgName")); Utils.AddLog(amazonDynamoDB, logger, false);
		 * return responseEntity; }
		 */

		String orgName = request.getHeader("orgName");
		if (Utils.isEmptyString(orgName)) {
			returnmap.put("status", "error");
			returnmap.put("message", AppConstants.fieldsmissing);
			return new ResponseEntity<>(returnmap, HttpStatus.OK);
		}

		String dir = getDirectory();
		File f = new File(dir + "/" + orgName);

		if (!f.isDirectory()) {
			returnmap.put("status", "error");
			returnmap.put("message", AppConstants.fieldsmissing);
			return new ResponseEntity<>(returnmap, HttpStatus.OK);
		}

		File f1 = new File(dir + "/" + orgName + "/map_details.json");
		if (!f1.isFile()) {
			returnmap.put("status", "error");
			returnmap.put("message", AppConstants.fieldsmissing);
			return new ResponseEntity<>(returnmap, HttpStatus.OK);
		}

		String details = Utils.getDataFromFile(f1.getAbsolutePath());

		return new ResponseEntity<>(details, HttpStatus.OK);
	}

	@GetMapping(value = "demo/zonedetails")
	public ResponseEntity<?> getDemoZoneDetails(HttpServletRequest request, HttpSession httpSession)
			throws InterruptedException, ExecutionException {
		HashMap<String, Object> returnmap = new HashMap<>();

		/*
		 * ResponseEntity<?> responseEntity = checkSessionKey(httpSession,
		 * Utils.getCookie(request, "JSESSIONID"), "", ""); if (responseEntity != null)
		 * { LoggerTable logger = new LoggerTable(Utils.getFormatedDateLog(new Date()),
		 * AppConstants.error, httpSession, AppConstants.sessiontimeout,
		 * AppConstants.demo, "demo_mapdetails", returnmap, "",
		 * request.getHeader("orgName")); Utils.AddLog(amazonDynamoDB, logger, false);
		 * return responseEntity; }
		 */

		String orgName = request.getHeader("orgName");
		if (Utils.isEmptyString(orgName)) {
			returnmap.put("status", "error");
			returnmap.put("message", AppConstants.fieldsmissing);
			return new ResponseEntity<>(returnmap, HttpStatus.OK);
		}

		String dir = getDirectory();
		File f = new File(dir + "/" + orgName);

		if (!f.isDirectory()) {
			returnmap.put("status", "error");
			returnmap.put("message", AppConstants.fieldsmissing);
			return new ResponseEntity<>(returnmap, HttpStatus.OK);
		}

		File f1 = new File(dir + "/" + orgName + "/zones.json");
		if (!f1.isFile()) {
			returnmap.put("status", "error");
			returnmap.put("message", AppConstants.fieldsmissing);
			return new ResponseEntity<>(returnmap, HttpStatus.OK);
		}

		String details = Utils.getDataFromFile(f1.getAbsolutePath());

		return new ResponseEntity<>(details, HttpStatus.OK);
	}

	@GetMapping(value = "demo/tag_details")
	public ResponseEntity<?> getTagDetails(HttpServletRequest request, HttpSession httpSession)
			throws InterruptedException, ExecutionException {
		HashMap<String, Object> returnmap = new HashMap<>();

		/*
		 * ResponseEntity<?> responseEntity = checkSessionKey(httpSession,
		 * Utils.getCookie(request, "JSESSIONID"), "", ""); if (responseEntity != null)
		 * { LoggerTable logger = new LoggerTable(Utils.getFormatedDateLog(new Date()),
		 * AppConstants.error, httpSession, AppConstants.sessiontimeout,
		 * AppConstants.demo, "demo_mapdetails", returnmap, "",
		 * request.getHeader("orgName")); Utils.AddLog(amazonDynamoDB, logger, false);
		 * return responseEntity; }
		 */

		String orgName = request.getHeader("orgName");
		if (Utils.isEmptyString(orgName)) {
			returnmap.put("status", "error");
			returnmap.put("message", AppConstants.fieldsmissing);
			return new ResponseEntity<>(returnmap, HttpStatus.OK);
		}

		String dir = getDirectory();
		File f = new File(dir + "/" + orgName);

		if (!f.isDirectory()) {
			returnmap.put("status", "error");
			returnmap.put("message", AppConstants.fieldsmissing);
			return new ResponseEntity<>(returnmap, HttpStatus.OK);
		}

		File f1 = new File(dir + "/" + orgName + "/Live_Analytics.json");
		if (!f1.isFile()) {
			returnmap.put("status", "error");
			returnmap.put("message", AppConstants.fieldsmissing);
			return new ResponseEntity<>(returnmap, HttpStatus.OK);
		}

		String details = Utils.getDataFromFile(f1.getAbsolutePath());

		return new ResponseEntity<>(details, HttpStatus.OK);
	}

	@GetMapping(value = "demo/classification")
	public ResponseEntity<?> getDemoClassification(HttpServletRequest request, HttpSession httpSession)
			throws InterruptedException, ExecutionException {
		HashMap<String, Object> returnmap = new HashMap<>();

		/*
		 * ResponseEntity<?> responseEntity = checkSessionKey(httpSession,
		 * Utils.getCookie(request, "JSESSIONID"), "", ""); if (responseEntity != null)
		 * { LoggerTable logger = new LoggerTable(Utils.getFormatedDateLog(new Date()),
		 * AppConstants.error, httpSession, AppConstants.sessiontimeout,
		 * AppConstants.demo, "demo_mapdetails", returnmap, "",
		 * request.getHeader("orgName")); Utils.AddLog(amazonDynamoDB, logger, false);
		 * return responseEntity; }
		 */

		String orgName = request.getHeader("orgName");
		if (Utils.isEmptyString(orgName)) {
			returnmap.put("status", "error");
			returnmap.put("message", AppConstants.fieldsmissing);
			return new ResponseEntity<>(returnmap, HttpStatus.OK);
		}
		String dir = getDirectory();
		File f = new File(dir + "/" + orgName);

		if (!f.isDirectory()) {
			returnmap.put("status", "error");
			returnmap.put("message", AppConstants.fieldsmissing);
			return new ResponseEntity<>(returnmap, HttpStatus.OK);
		}

		File f1 = new File(dir + "/" + orgName + "/classification.json");
		if (!f1.isFile()) {
			returnmap.put("status", "error");
			returnmap.put("message", AppConstants.fieldsmissing);
			return new ResponseEntity<>(returnmap, HttpStatus.OK);
		}

		String details = Utils.getDataFromFile(f1.getAbsolutePath());

		return new ResponseEntity<>(details, HttpStatus.OK);
	}

	@GetMapping(value = "demo/tag_details/{type}")
	public ResponseEntity<?> getTagDetails(HttpServletRequest request, HttpSession httpSession,
			@PathVariable Optional<String> type) throws InterruptedException, ExecutionException {
		HashMap<String, Object> returnmap = new HashMap<>();

		/*
		 * ResponseEntity<?> responseEntity = checkSessionKey(httpSession,
		 * Utils.getCookie(request, "JSESSIONID"), "", ""); if (responseEntity != null)
		 * { LoggerTable logger = new LoggerTable(Utils.getFormatedDateLog(new Date()),
		 * AppConstants.error, httpSession, AppConstants.sessiontimeout,
		 * AppConstants.demo, "demo_mapdetails", returnmap, "",
		 * request.getHeader("orgName")); Utils.AddLog(amazonDynamoDB, logger, false);
		 * return responseEntity; }
		 */

		String orgName = request.getHeader("orgName");
		if (Utils.isEmptyString(orgName)) {
			returnmap.put("status", "error");
			returnmap.put("message", AppConstants.fieldsmissing);
			return new ResponseEntity<>(returnmap, HttpStatus.OK);
		}

		String dir = getDirectory();
		File f = new File(dir + "/" + orgName);

		if (!f.isDirectory()) {
			returnmap.put("status", "error");
			returnmap.put("message", AppConstants.fieldsmissing);
			return new ResponseEntity<>(returnmap, HttpStatus.OK);
		}

		String fileName = "/tag_details.json";
		if (type.get().toString().equals("live")) {
			fileName = "/Live_Analytics.json";
		} else {
			Random rn = new Random();
			int answer = rn.nextInt(6) + 1;
			fileName = "/tag_details" + answer + ".json";
		}

		File f1 = new File(dir + "/" + orgName + fileName);
		if (!f1.isFile()) {
			returnmap.put("status", "error");
			returnmap.put("message", AppConstants.fieldsmissing);
			return new ResponseEntity<>(returnmap, HttpStatus.OK);
		}

		String details = Utils.getDataFromFile(f1.getAbsolutePath());

		return new ResponseEntity<>(details, HttpStatus.OK);
	}

	@GetMapping(value = "demo/workOrderTable")
	public ResponseEntity<?> workOrderTable(HttpServletRequest request, HttpSession httpSession)
			throws InterruptedException, ExecutionException {
		HashMap<String, Object> returnmap = new HashMap<>();

		String orgName = request.getHeader("orgName");
		if (Utils.isEmptyString(orgName)) {
			returnmap.put("status", "error");
			returnmap.put("message", AppConstants.fieldsmissing);
			return new ResponseEntity<>(returnmap, HttpStatus.OK);
		}
		String dir = getDirectory();
		File f = new File(dir + "/" + orgName);

		if (!f.isDirectory()) {
			returnmap.put("status", "error");
			returnmap.put("message", AppConstants.fieldsmissing);
			return new ResponseEntity<>(returnmap, HttpStatus.OK);
		}

		File f1 = new File(dir + "/" + orgName + "/WorkOrderTrace/workorder.json");

		if (!f1.isFile()) {
			returnmap.put("status", "error");
			returnmap.put("message", AppConstants.fieldsmissing);
			return new ResponseEntity<>(returnmap, HttpStatus.OK);
		}

		String details = Utils.getDataFromFile(f1.getAbsolutePath());

		return new ResponseEntity<>(details, HttpStatus.OK);
	}

	@PostMapping(value = "demo/workOrderchart")
	public ResponseEntity<?> getWorkOrderChart(HttpServletRequest request, HttpSession httpSession,
			@RequestBody HashMap<String, Object> reqMap) throws InterruptedException, ExecutionException {
		HashMap<String, Object> returnmap = new HashMap<>();

		String orgName = request.getHeader("orgName");
		if (Utils.isEmptyString(orgName)) {
			returnmap.put("status", "error");
			returnmap.put("message", AppConstants.fieldsmissing);
			return new ResponseEntity<>(returnmap, HttpStatus.OK);
		}

		if (!reqMap.containsKey("days")) {
			returnmap.put("status", "error");
			returnmap.put("message", AppConstants.fieldsmissing);
			return new ResponseEntity<>(returnmap, HttpStatus.OK);
		}

		String days = reqMap.get("days").toString();

		String dir = getDirectory();
		String dirPath = "";

			dirPath = dir + "/" + orgName + "/WorkOrderChart/" + days;

		File f = new File(dirPath);

		if (!f.isDirectory()) {
			returnmap.put("status", "error");
			returnmap.put("message", AppConstants.fieldsmissing);
			return new ResponseEntity<>(returnmap, HttpStatus.OK);
		}

		String filePath = "";
	
			filePath = dirPath + "/allzones" + ".json";

		File f1 = new File(filePath);
		if (!f1.isFile()) {
			returnmap.put("status", "error");
			returnmap.put("message", "No data found for " );
			return new ResponseEntity<>(returnmap, HttpStatus.OK);
		}

		String details = Utils.getDataFromFile(f1.getAbsolutePath());

		return new ResponseEntity<>(details, HttpStatus.OK);
	}

	public String getDirectory() {
		ResourceBundle bundle = ResourceBundle.getBundle("application");
		String os = System.getProperty("os.name").toLowerCase();
		String dir = "";
		if (os.contains("windows")) {
			dir = bundle.getString("filepath_windows");
		} else if (os.contains("mac os")) {
			dir = bundle.getString("filepath_ios");
		} else
			dir = bundle.getString("filepath_linux");

		return dir;
	}
}
