package com.entappia.ei4o.controllers;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.concurrent.ExecutionException;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.entappia.ei4o.constants.AppConstants;
import com.entappia.ei4o.dbmodels.Department;
import com.entappia.ei4o.repository.DepartmentRepository;
import com.entappia.ei4o.services.LogsServices;
import com.entappia.ei4o.utils.BaseController;
import com.entappia.ei4o.utils.Utils;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
@RestController
public class DepartmentController extends BaseController {
	@Autowired
	DepartmentRepository departmentRepository;
	
	@Autowired
	private LogsServices logsServices;
	
	Gson gson = new Gson();
	Type mapType = new TypeToken<Department>() {
	}.getType();
	
	@RequestMapping(path = { "department", "department/{deptName}" }, method = RequestMethod.GET)
	public ResponseEntity<?> getdept(HttpServletRequest request, HttpSession httpSession,
			@PathVariable Optional<String> deptName) throws InterruptedException, ExecutionException {
		Map<String, Object> returnmap = new HashMap<>();
		HashMap<String, String> reuestMap = new HashMap<>();
		try {
			//"CDPT"
			ResponseEntity<?> responseEntity = checkSessionKey(httpSession, Utils.getCookie(request, "JSESSIONID"));
			if (responseEntity != null) { 
				logsServices.addLog(AppConstants.error, AppConstants.Dept, "getdepartment",
						AppConstants.sessiontimeout, null);
				return responseEntity; 
			}
			
			JSONObject jsonObject = new JSONObject();
			if (deptName.isPresent()) {
				jsonObject.put("deptName", deptName.get());
			} else {
				jsonObject.put("deptName", "all");
			}

			reuestMap.put("params", jsonObject.toString()); 
			
			logsServices.addLog(AppConstants.event, AppConstants.Dept, "getdepartment",
					"Department get API Call", reuestMap);
			
			  
			
			List<Department> departmentDetails = new ArrayList<>();

			if (deptName.isPresent()) {
				Department department = departmentRepository.findByDeptName(deptName.get());
				if (department == null) {
					returnmap.put("status", "error");
					returnmap.put("message", "Department name does not exist");
					logsServices.addLog(AppConstants.error, AppConstants.Dept, "getdepartment", "department name does not exist", null);

					return new ResponseEntity<>(returnmap, HttpStatus.OK);
				}

				departmentDetails.add(department);
			} else {
				departmentDetails = departmentRepository.findAll();
			}
			logsServices.addLog(AppConstants.success, AppConstants.Dept, "getdepartment", "Department details", null);

			returnmap.put("status", "success");
			returnmap.put("data", departmentDetails);
			
		return new ResponseEntity<>(departmentDetails, HttpStatus.OK);
		} catch (Exception e) {
			returnmap.put("status", "error");
			returnmap.put("message", AppConstants.errormsg);
			logsServices.addLog(AppConstants.error, AppConstants.Dept, "getdepartment", AppConstants.errormsg, null);

			return new ResponseEntity<>(returnmap, HttpStatus.OK);
		}
			
		}
	
	@PostMapping(value = "/department")
	public ResponseEntity<?> AddDept(HttpServletRequest request, HttpSession httpSession,

			@RequestBody Department department) throws InterruptedException, ExecutionException {
		Map<String, String> returnmap = new HashMap<>(); 
		try {
			
			ResponseEntity<?> responseEntity = checkSessionKey(httpSession, Utils.getCookie(request, "JSESSIONID"), "CDPT");
			if (responseEntity != null) { 
				logsServices.addLog(AppConstants.error, AppConstants.Dept, "Adddepartment",
						AppConstants.sessiontimeout, null);
				return responseEntity; 
			}
			
			Map<String, String> requestLogMap = new HashMap<>(); 
			requestLogMap.put("params", gson.toJson(department, mapType));

			
 
			
			logsServices.addLog(AppConstants.event, AppConstants.Dept, "Adddepartment",
					"Department Add API Call", requestLogMap);
			
			HashMap<String, Object> mapTags = new HashMap<>();
			if (department != null) {
				mapTags.put("department name", department.getDeptName());
				mapTags.put("deparment id", department.getDeptNumber());
				mapTags.put("head", department.getDeptHead());
				mapTags.put("active", department.isActive()); 

			}

			if (Utils.isEmptyString(department.getDeptName()) || Utils.isEmptyString(String.valueOf(department.getDeptNumber()))
					|| Utils.isEmptyString( department.getDeptHead())) {

				returnmap.put("status", "error");
				returnmap.put("message", AppConstants.fieldsmissing);

				logsServices.addLog(AppConstants.error, AppConstants.Dept, "Adddepartment", AppConstants.fieldsmissing, null);
				return new ResponseEntity<>(returnmap, HttpStatus.OK);

			}

			Department department1 = departmentRepository.findByDeptName(department.getDeptName());

			if (department1 != null && department1.isActive() ) {
				returnmap.put("status", "error");
				returnmap.put("message", "department name already exist");

				logsServices.addLog(AppConstants.error, AppConstants.Dept, "Adddepartment", "Department Name already exist", null);
				return new ResponseEntity<>(returnmap, HttpStatus.OK);
			}
   
			department.setCreatedDate(new Date());
			department.setModifiedDate(new Date());
			department.setActive(true);  
			departmentRepository.save(department);
 

			returnmap.put("status", "success");
			returnmap.put("message", department.getDeptName() + " added successfully");

			logsServices.addLog(AppConstants.event, AppConstants.Dept, "AddTag",
					"DepartMent-" + department.getDeptName() + " added successfully, ", null);

			return new ResponseEntity<>(returnmap, HttpStatus.OK);
		} catch (Exception e) {
			returnmap.put("status", "error");
			returnmap.put("message", AppConstants.errormsg);

			logsServices.addLog(AppConstants.error, AppConstants.Dept, "Adddepartment", AppConstants.errormsg, null);
			return new ResponseEntity<>(returnmap, HttpStatus.OK);
		}
	}
	
	@PutMapping(value = "/department")
	public ResponseEntity<?> updateDepartment(HttpServletRequest request, HttpSession httpSession, 
			@RequestBody Department department) throws InterruptedException, ExecutionException {
		Map<String, String> returnmap = new HashMap<>();
		 
		try {
			
			ResponseEntity<?> responseEntity = checkSessionKey(httpSession, Utils.getCookie(request, "JSESSIONID"), "CDPT");
			if (responseEntity != null) {
				logsServices.addLog(AppConstants.error, AppConstants.Dept, "updateDepartment",
						AppConstants.sessiontimeout, null);
				return responseEntity; 
			}
			 
			Map<String, String> requestLogMap = new HashMap<>(); 
			requestLogMap.put("params", gson.toJson(department, mapType));
			
			logsServices.addLog(AppConstants.event, AppConstants.Dept, "updateDepartment",
					"Department update API Call", requestLogMap);
			
			HashMap<String, Object> mapTags = new HashMap<>();
			if (department != null) {
				mapTags.put("department name", department.getDeptName());
				mapTags.put("deparment id", department.getDeptNumber());
				mapTags.put("head", department.getDeptHead()); 

			}

			if (Utils.isEmptyString(department.getDeptName()) || Utils.isEmptyString(String.valueOf(department.getDeptNumber()))
					|| Utils.isEmptyString( department.getDeptHead())) {

				returnmap.put("status", "error");
				returnmap.put("message", AppConstants.fieldsmissing);

				logsServices.addLog(AppConstants.error, AppConstants.Dept, "updateDepartment", AppConstants.fieldsmissing, null);
				return new ResponseEntity<>(returnmap, HttpStatus.OK);

			}

			Department department1 = departmentRepository.findByDeptName(department.getDeptName());

			if (department1 == null) {
				returnmap.put("status", "error");
				returnmap.put("message", "department name not exist");

				logsServices.addLog(AppConstants.error, AppConstants.Dept, "updateDepartment", "Department Name not exist", null);
				return new ResponseEntity<>(returnmap, HttpStatus.OK);
			} 
			
			department1.setDeptNumber(department.getDeptNumber());
			department1.setDeptHead(department.getDeptHead());
			department1.setModifiedDate(new Date()); 
			departmentRepository.save(department1);
 

			returnmap.put("status", "success");
			returnmap.put("message", department1.getDeptName() + " updated successfully");

			logsServices.addLog(AppConstants.event, AppConstants.Dept, "updateDepartment",
					"DepartMent-" + department1.getDeptName() + " Update successfully, ", null);

			return new ResponseEntity<>(returnmap, HttpStatus.OK);
		} catch (Exception e) {
			returnmap.put("status", "error");
			returnmap.put("message", AppConstants.errormsg);

			logsServices.addLog(AppConstants.error, AppConstants.Dept, "updateDepartment", AppConstants.errormsg, null);
			return new ResponseEntity<>(returnmap, HttpStatus.OK);
		}
	}
	
	@DeleteMapping(value = "/department")
	public ResponseEntity<?> deleteDepartment(HttpServletRequest request, HttpSession httpSession, 
			@RequestBody HashMap<String, Object> requestMap) throws InterruptedException, ExecutionException {
		Map<String, String> returnmap = new HashMap<>();
		 
		try {
			
			ResponseEntity<?> responseEntity = checkSessionKey(httpSession, Utils.getCookie(request, "JSESSIONID"), "CDPT");
			if (responseEntity != null) { 
				logsServices.addLog(AppConstants.error, AppConstants.Dept, "deleteDepartment",
						AppConstants.sessiontimeout, null);
				return responseEntity; 
			}
			
			Map<String, String> requestLogMap = new HashMap<>(); 
			requestLogMap.put("params", new JSONObject(requestMap).toString());
			
			logsServices.addLog(AppConstants.event, AppConstants.Dept, "deleteDepartment",
					"Department delete API Call", requestLogMap);
			
			HashMap<String, Object> mapTags = new HashMap<>();

			if (Utils.isEmptyString(requestMap.getOrDefault("deptName", "").toString()) ) {

				returnmap.put("status", "error");
				returnmap.put("message", AppConstants.fieldsmissing);

				logsServices.addLog(AppConstants.error, AppConstants.Dept, "deleteDepartment", AppConstants.fieldsmissing, null);
				return new ResponseEntity<>(returnmap, HttpStatus.OK);

			}else{
				mapTags.put("department name", requestMap.getOrDefault("deptName", "").toString()); 
				mapTags.put("active", false); 
			}

			Department department1 = departmentRepository.findByDeptName(requestMap.getOrDefault("deptName", "").toString());

			if (department1 == null) {
				returnmap.put("status", "error");
				returnmap.put("message", "department name not exist");

				logsServices.addLog(AppConstants.error, AppConstants.Dept, "deleteDepartment", "Department Name not exist", null);
				return new ResponseEntity<>(returnmap, HttpStatus.OK);
			} 
			department1.setModifiedDate(new Date());
			department1.setActive(false);
			departmentRepository.save(department1);
 

			returnmap.put("status", "success");
			returnmap.put("message", department1.getDeptName() + " Deleted successfully");

			logsServices.addLog(AppConstants.event, AppConstants.Dept, "deleteDepartment",
					"DepartMent-" + department1.getDeptName() + " Deleted successfully, ", null);

			return new ResponseEntity<>(returnmap, HttpStatus.OK);
		} catch (Exception e) {
			returnmap.put("status", "error");
			returnmap.put("message", AppConstants.errormsg);

			logsServices.addLog(AppConstants.error, AppConstants.Dept, "deleteDepartment", AppConstants.errormsg, null);
			return new ResponseEntity<>(returnmap, HttpStatus.OK);
		}
	}
	
	}

