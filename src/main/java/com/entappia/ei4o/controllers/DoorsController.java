package com.entappia.ei4o.controllers;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.concurrent.ExecutionException;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.entappia.ei4o.constants.AppConstants;
import com.entappia.ei4o.dbmodels.Doors;
import com.entappia.ei4o.dbmodels.OutlinesDetails;
import com.entappia.ei4o.repository.DoorsRepository;
import com.entappia.ei4o.repository.OutLineRepository;
import com.entappia.ei4o.services.LogsServices;
import com.entappia.ei4o.utils.BaseController;
import com.entappia.ei4o.utils.Utils;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

@RestController
public class DoorsController extends BaseController {

	@Autowired
	private LogsServices logsServices;

	@Autowired
	private OutLineRepository outLineRepository;

	@Autowired
	private DoorsRepository doorsRepository;

	@RequestMapping(path = {  "doorsDetails", "doorsDetails/{outlinesId}" }, method = RequestMethod.GET)
	public ResponseEntity<?> getDoorsDetails(HttpServletRequest request, HttpSession httpSession,
			@PathVariable Optional<Long> outlinesId) throws InterruptedException, ExecutionException {
		Map<String, Object> returnmap = new HashMap<>();
		Map<String, String> reuestMap = new HashMap<>();
		
		try {

			ResponseEntity<?> responseEntity = checkSessionKey(httpSession, Utils.getCookie(request, "JSESSIONID"));
			if (responseEntity != null) { 
				logsServices.addLog(AppConstants.error, AppConstants.DOORS, "getDoorsDetails",
						AppConstants.sessiontimeout, null);
				return responseEntity; 
			}
			
			
			JSONObject jsonObject = new JSONObject();
			if (outlinesId.isPresent()) {
				jsonObject.put("outlinesId", outlinesId.get());
			} else {
				jsonObject.put("outlinesId", "all");
			}

			reuestMap.put("params", jsonObject.toString()); 

			logsServices.addLog(AppConstants.event, AppConstants.DOORS, "getDoorsDetails",
					"Doors Details Get API Details", reuestMap);

			List<Doors> doorsDetailsList = new ArrayList<>();

			if (outlinesId.isPresent()) {
				doorsDetailsList = doorsRepository.findByOutlinesId(outlinesId.get());
				if (doorsDetailsList == null) {
					returnmap.put("status", "error");
					returnmap.put("message", "Doors Details does not exist");
					logsServices.addLog(AppConstants.error, AppConstants.DOORS, "getDoorsDetails",
							returnmap.get("message").toString(), null);

					return new ResponseEntity<>(returnmap, HttpStatus.OK);
				}

			} else {
				
				doorsDetailsList = doorsRepository.findAllActiveDoors();
				if (doorsDetailsList == null) {
					returnmap.put("status", "error");
					returnmap.put("message", "Doors Details does not exist");
					logsServices.addLog(AppConstants.error, AppConstants.DOORS, "getDoorsDetails",
							returnmap.get("message").toString(), null);

					return new ResponseEntity<>(returnmap, HttpStatus.OK);
				} 
				 
			}
			logsServices.addLog(AppConstants.success, AppConstants.DOORS, "getDoorsDetails", "Doors details", null);

			returnmap.put("status", "success");
			returnmap.put("data", doorsDetailsList);

			return new ResponseEntity<>(returnmap, HttpStatus.OK);
		} catch (Exception e) {
			returnmap.put("status", "error");
			returnmap.put("message", AppConstants.errormsg);
			logsServices.addLog(AppConstants.error, AppConstants.DOORS, "getDoorsDetails", AppConstants.errormsg, null);

			return new ResponseEntity<>(returnmap, HttpStatus.OK);
		}

	}

	@PostMapping(value = "/doorsDetails")
	public ResponseEntity<?> addDoorsDetails(HttpServletRequest request, HttpSession httpSession,

			@RequestBody HashMap<String, Object> requestMap) throws InterruptedException, ExecutionException {
		Map<String, String> returnmap = new HashMap<>();

		try {

			ResponseEntity<?> responseEntity = checkSessionKey(httpSession, Utils.getCookie(request, "JSESSIONID"), "CTPE");
			if (responseEntity != null) { 
				logsServices.addLog(AppConstants.error, AppConstants.DOORS, "addDoorsDetails",
						AppConstants.sessiontimeout, null);
				return responseEntity; 
			}
			
			Map<String, String> requestLogMap = new HashMap<>(); 
			requestLogMap.put("params", new JSONObject(requestMap).toString());
 
			logsServices.addLog(AppConstants.event, AppConstants.DOORS, "addDoorsDetails",
					"Doors Details Add API Details", requestLogMap);
			
			if (Utils.isEmptyString(requestMap.getOrDefault("outlinesId", "").toString())
					|| Utils.isEmptyString(requestMap.getOrDefault("name", "").toString())
					|| Utils.isEmptyString(requestMap.getOrDefault("colorCode", "").toString())
					|| Utils.isEmptyString(requestMap.getOrDefault("wallType", "").toString())
					|| Utils.isEmptyString(requestMap.getOrDefault("coordinates", "").toString())) {

				returnmap.put("status", "error");
				returnmap.put("message", AppConstants.fieldsmissing);

				logsServices.addLog(AppConstants.error, AppConstants.DOORS, "addDoorsDetails",
						AppConstants.fieldsmissing, null);
				return new ResponseEntity<>(returnmap, HttpStatus.OK);

			}

			OutlinesDetails outlinesDetails = outLineRepository
					.findByOutlinesId(Long.valueOf(requestMap.getOrDefault("outlinesId", "").toString()));
			if (outlinesDetails == null) {
				returnmap.put("status", "error");
				returnmap.put("message", "Outlines id not found");

				logsServices.addLog(AppConstants.error, AppConstants.DOORS, "addDoorsDetails",
						returnmap.get("message").toString(), null);
				return new ResponseEntity<>(returnmap, HttpStatus.OK);
			} else if (!outlinesDetails.isActive()) {
				returnmap.put("status", "error");
				returnmap.put("message", "Outlines Details is not active");

				logsServices.addLog(AppConstants.error, AppConstants.DOORS, "addDoorsDetails",
						returnmap.get("message").toString(), null);
				return new ResponseEntity<>(returnmap, HttpStatus.OK);
			}

 
			
			Gson gson = new Gson();
			Type mapType = new TypeToken<List<HashMap<String, String>>>() {
			}.getType();

			List<HashMap<String, String>> coordinateList = gson
					.fromJson(requestMap.getOrDefault("coordinates", "").toString(), mapType);
			
			Doors doors = new Doors();
			doors.setName(requestMap.getOrDefault("name", "").toString());
			doors.setOutlinesDetails(outlinesDetails);
			doors.setColorCode(requestMap.getOrDefault("colorCode", "").toString());
			doors.setWallType(requestMap.getOrDefault("wallType", "").toString());
			doors.setCoordinates(coordinateList);
			doors.setActive(true);
			doors.setCreatedDate(new Date());
			doors.setModifiedDate(new Date());
			doorsRepository.save(doors);

			returnmap.put("status", "success");
			returnmap.put("message", doors.getName() + " added successfully");

			logsServices.addLog(AppConstants.event, AppConstants.DOORS, "addDoorsDetails",
					"Doors -" + doors.getName() + " added successfully, ", null);

			return new ResponseEntity<>(returnmap, HttpStatus.OK);
		} catch (Exception e) {
			returnmap.put("status", "error");
			returnmap.put("message", AppConstants.errormsg);

			logsServices.addLog(AppConstants.error, AppConstants.DOORS, "addDoorsDetails", AppConstants.errormsg, null);
			return new ResponseEntity<>(returnmap, HttpStatus.OK);
		}
	}

	@PutMapping(value = "/doorsDetails")
	public ResponseEntity<?> updateDoorsDetails(HttpServletRequest request, HttpSession httpSession,

			@RequestBody HashMap<String, Object> requestMap) throws InterruptedException, ExecutionException {
		Map<String, String> returnmap = new HashMap<>();

		try {
			
			ResponseEntity<?> responseEntity = checkSessionKey(httpSession, Utils.getCookie(request, "JSESSIONID"), "CTPE");
			if (responseEntity != null) { 
				logsServices.addLog(AppConstants.error, AppConstants.DOORS, "updateDoorsDetails",
						AppConstants.sessiontimeout, null);
				return responseEntity; 
			}
			
			Map<String, String> requestLogMap = new HashMap<>(); 
			requestLogMap.put("params", new JSONObject(requestMap).toString());
 
			logsServices.addLog(AppConstants.event, AppConstants.DOORS, "updateDoorsDetails",
					"Doors Details Update API Details", requestLogMap);

			if (Utils.isEmptyString(requestMap.getOrDefault("id", "").toString())
					||Utils.isEmptyString(requestMap.getOrDefault("outlinesId", "").toString())
					|| Utils.isEmptyString(requestMap.getOrDefault("name", "").toString())
					|| Utils.isEmptyString(requestMap.getOrDefault("colorCode", "").toString())
					|| Utils.isEmptyString(requestMap.getOrDefault("wallType", "").toString())
					|| Utils.isEmptyString(requestMap.getOrDefault("coordinates", "").toString())) {

				returnmap.put("status", "error");
				returnmap.put("message", AppConstants.fieldsmissing);

				logsServices.addLog(AppConstants.error, AppConstants.DOORS, "updateDoorsDetails", AppConstants.fieldsmissing,
						null);
				return new ResponseEntity<>(returnmap, HttpStatus.OK);

			}

			OutlinesDetails outlinesDetails = outLineRepository
					.findByOutlinesId(Long.valueOf(requestMap.getOrDefault("outlinesId", "").toString()));
			if (outlinesDetails == null) {
				returnmap.put("status", "error");
				returnmap.put("message", "Outlines id not found");

				logsServices.addLog(AppConstants.error, AppConstants.DOORS, "updateDoorsDetails",
						returnmap.get("message").toString(), null);
				return new ResponseEntity<>(returnmap, HttpStatus.OK);
			}else if (!outlinesDetails.isActive()) {
				returnmap.put("status", "error");
				returnmap.put("message", "Outlines Details is not active");

				logsServices.addLog(AppConstants.error, AppConstants.DOORS, "updateDoorsDetails",
						returnmap.get("message").toString(), null);
				return new ResponseEntity<>(returnmap, HttpStatus.OK);
			}

		 
			Doors doors = doorsRepository.findAllById(Long.valueOf(requestMap.getOrDefault("id", "").toString()));
			
			if (doors == null) {
				returnmap.put("status", "error");
				returnmap.put("message", "Doors id not found");

				logsServices.addLog(AppConstants.error, AppConstants.DOORS, "updateDoorsDetails",
						returnmap.get("message").toString(), null);
				return new ResponseEntity<>(returnmap, HttpStatus.OK);
			}else if (!doors.isActive()) {
				returnmap.put("status", "error");
				returnmap.put("message", "Doors Details is not active");

				logsServices.addLog(AppConstants.error, AppConstants.DOORS, "updateDoorsDetails",
						returnmap.get("message").toString(), null);
				return new ResponseEntity<>(returnmap, HttpStatus.OK);
			}
			 
			Gson gson = new Gson();
			Type mapType = new TypeToken<List<HashMap<String, String>>>() {
			}.getType();

			List<HashMap<String, String>> coordinateList = gson
					.fromJson(requestMap.getOrDefault("coordinates", "").toString(), mapType);
			
			doors.setName(requestMap.getOrDefault("name", "").toString());
			doors.setOutlinesDetails(outlinesDetails); 
			doors.setColorCode(requestMap.getOrDefault("colorCode", "").toString());
			doors.setWallType(requestMap.getOrDefault("wallType", "").toString());
			doors.setCoordinates(coordinateList); 
			doors.setModifiedDate(new Date());
			doorsRepository.save(doors);

			returnmap.put("status", "success");
			returnmap.put("message", doors.getName() + " updated successfully");

			logsServices.addLog(AppConstants.event, AppConstants.DOORS, "updateDoorsDetails",
					"Walls -" + doors.getName() + " updated successfully, ", null);

			return new ResponseEntity<>(returnmap, HttpStatus.OK);
		} catch (Exception e) {
			returnmap.put("status", "error");
			returnmap.put("message", AppConstants.errormsg);

			logsServices.addLog(AppConstants.error, AppConstants.DOORS, "updateDoorsDetails", AppConstants.errormsg, null);
			return new ResponseEntity<>(returnmap, HttpStatus.OK);
		}
	}
	
	
	@DeleteMapping(value = "/doorsDetails")
	public ResponseEntity<?> deleteDoorsDetails(HttpServletRequest request, HttpSession httpSession,

			@RequestBody HashMap<String, Object> requestMap) throws InterruptedException, ExecutionException {
		Map<String, String> returnmap = new HashMap<>();

		try {

			ResponseEntity<?> responseEntity = checkSessionKey(httpSession, Utils.getCookie(request, "JSESSIONID"), "CTPE");
			if (responseEntity != null) { 
				logsServices.addLog(AppConstants.error, AppConstants.DOORS, "deleteDoorsDetails",
						AppConstants.sessiontimeout, null);
				return responseEntity; 
			}
			
			Map<String, String> requestLogMap = new HashMap<>(); 
			requestLogMap.put("params", new JSONObject(requestMap).toString());
 
			logsServices.addLog(AppConstants.event, AppConstants.DOORS, "deleteDoorsDetails",
					"Doors Details delete API Details", requestLogMap);
			
			if (Utils.isEmptyString(requestMap.getOrDefault("id", "").toString()) ) {

				returnmap.put("status", "error");
				returnmap.put("message", AppConstants.fieldsmissing);

				logsServices.addLog(AppConstants.error, AppConstants.DOORS, "deleteDoorsDetails", AppConstants.fieldsmissing,
						null);
				return new ResponseEntity<>(returnmap, HttpStatus.OK);

			}
 
			Doors doors = doorsRepository.findAllById(Long.valueOf(requestMap.getOrDefault("id", "").toString()));
			
			if (doors == null) {
				returnmap.put("status", "error");
				returnmap.put("message", "Doors id not found");

				logsServices.addLog(AppConstants.error, AppConstants.DOORS, "deleteDoorsDetails",
						returnmap.get("message").toString(), null);
				return new ResponseEntity<>(returnmap, HttpStatus.OK);
			}else if (!doors.isActive()) {
				returnmap.put("status", "error");
				returnmap.put("message", "Doors is not active");

				logsServices.addLog(AppConstants.error, AppConstants.DOORS, "deleteDoorsDetails",
						returnmap.get("message").toString(), null);
				return new ResponseEntity<>(returnmap, HttpStatus.OK);
			}
			
 			 
			doors.setActive(false);
			doors.setModifiedDate(new Date());
			doorsRepository.save(doors);

			returnmap.put("status", "success");
			returnmap.put("message", doors.getName() + " status updated successfully");

			logsServices.addLog(AppConstants.event, AppConstants.DOORS, "deleteDoorsDetails",
					"Doors -" + doors.getName() + " status updated successfully, ", null);

			return new ResponseEntity<>(returnmap, HttpStatus.OK);
		} catch (Exception e) {
			returnmap.put("status", "error");
			returnmap.put("message", AppConstants.errormsg);

			logsServices.addLog(AppConstants.error, AppConstants.DOORS, "deleteDoorsDetails", AppConstants.errormsg, null);
			return new ResponseEntity<>(returnmap, HttpStatus.OK);
		}
	}

}
