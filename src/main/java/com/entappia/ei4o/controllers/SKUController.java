package com.entappia.ei4o.controllers;

import java.io.File;
import java.io.IOException;
import java.lang.reflect.Type;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.concurrent.ExecutionException;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.entappia.ei4o.constants.AppConstants;
import com.entappia.ei4o.dbmodels.SKUAllocation;
import com.entappia.ei4o.dbmodels.Tags;
import com.entappia.ei4o.repository.SKUAllocationRepository;
import com.entappia.ei4o.repository.TagsRepository;
import com.entappia.ei4o.services.LogsServices;
import com.entappia.ei4o.services.SKUEvent;
import com.entappia.ei4o.utils.BaseController;
import com.entappia.ei4o.utils.Utils;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

@RestController
public class SKUController extends BaseController {

	@Autowired
	private LogsServices logsServices;

	@Autowired
	private SKUEvent skuEvent;

	@Autowired
	private SKUAllocationRepository skuAllocationRepository;

	@Autowired
	private TagsRepository tagsRepository;

	@PostMapping(value = "/skuDetails")
	public ResponseEntity<?> skuDetails(HttpServletRequest request, HttpSession httpSession,
			@RequestBody List<HashMap<String, Object>> requestMap) throws InterruptedException, ExecutionException {

		ResponseEntity<?> responseEntity = checkSessionKey(httpSession, Utils.getCookie(request, "JSESSIONID"), "SDET");
		if (responseEntity != null) { 
			logsServices.addLog(AppConstants.error, AppConstants.SKUDETAILS, "skuDetails",
					AppConstants.sessiontimeout, null);
			return responseEntity; 
		}
		

		logsServices.addLog(AppConstants.event, AppConstants.SKUDETAILS, "skuDetails",
				"Add SKU details api call", null);
		
		Map<String, String> returnmap = new HashMap<>();

		for (HashMap<String, Object> map : requestMap) {
			if (Utils.isEmptyString(map.getOrDefault("upcCode", "").toString())
					|| Utils.isEmptyString(map.getOrDefault("skuNo", "").toString())
					|| Utils.isEmptyString(map.getOrDefault("batchNo", "").toString())) {

				returnmap.put("status", "error");
				returnmap.put("message", AppConstants.fieldsmissing);

				logsServices.addLog(AppConstants.error, AppConstants.SKUDETAILS, "skuDetails",
						AppConstants.fieldsmissing, null);
				return new ResponseEntity<>(returnmap, HttpStatus.OK);
			}
		}

		JSONArray jsonArray = new JSONArray();
		for (int i = 0; i < requestMap.size(); i++) {
			HashMap<String, Object> map = requestMap.get(i);
			JSONObject jsonObject = new JSONObject();
			try {
				jsonObject.put("upcCode", map.get("upcCode").toString());
				jsonObject.put("skuNo", map.get("skuNo").toString());
				jsonObject.put("batchNo", map.get("batchNo").toString());
				jsonArray.put(jsonObject);

			} catch (JSONException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		String dir = Utils.getServerPath();

		File f = new File(dir + "/sku");
		if (!f.exists())
			f.mkdirs();

		File f1 = new File(f + "/" + "sku.json");
		try {
			if (f1.createNewFile()) {

			}
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		byte[] bytes = jsonArray.toString().getBytes();
		Path path = Paths.get(f + "/" + "sku.json");
		try {
			Files.write(path, bytes);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		skuEvent.addSKUAsset(jsonArray);
		returnmap.put("status", "success");
		returnmap.put("message", "SKU details updated sucessfully");

		logsServices.addLog(AppConstants.event, AppConstants.SKUDETAILS, "skuDetails", returnmap.get("message"), null);
		return new ResponseEntity<>(returnmap, HttpStatus.OK);
	}

	@RequestMapping(path = { "skuDetails", "skuDetails/{upcCode}" }, method = RequestMethod.GET)
	public ResponseEntity<?> getSKUDetails(HttpServletRequest request, HttpSession httpSession,
			@PathVariable Optional<String> upcCode) throws InterruptedException, ExecutionException {

		ResponseEntity<?> responseEntity = checkSessionKey(httpSession, Utils.getCookie(request, "JSESSIONID"), "SDET");
		if (responseEntity != null) { 
			logsServices.addLog(AppConstants.error, AppConstants.SKUDETAILS, "getSKUDetails",
					AppConstants.sessiontimeout, null);
			return responseEntity; 
		}
		
		JSONObject jsonObject = new JSONObject();
		try {
		if (upcCode.isPresent()) {
		
				jsonObject.put("upcCode", upcCode.get());
			
		} else {
			jsonObject.put("upcCode", "all");
		}
		
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		Map<String, String> reuestMap = new HashMap<>();
		reuestMap.put("params", jsonObject.toString()); 

		
		logsServices.addLog(AppConstants.event, AppConstants.SKUDETAILS, "getSKUDetails",
				"Get SKU details api call", reuestMap);
		
		Map<String, Object> returnmap = new HashMap<>();

		String dir = Utils.getServerPath();

		File f1 = new File(dir + "/sku/" + "sku.json");
		if (!f1.exists()) {
			returnmap.put("status", "error");
			returnmap.put("message", "SKU details not found");

			logsServices.addLog(AppConstants.error, AppConstants.SKUDETAILS, "getSKUDetails",
					AppConstants.fieldsmissing, null);
			return new ResponseEntity<>(returnmap, HttpStatus.OK);
		}

		JSONArray data = skuEvent.readSKUAssets();
		if (data != null) {
			Gson gson = new Gson();
			Type type = new TypeToken<List<HashMap<String, Object>>>() {
			}.getType();
			List<HashMap<String, Object>> listMap = gson.fromJson(data.toString(), type);

			HashMap<String, SKUAllocation> hashMap = new HashMap<>();
			List<SKUAllocation> skuAllocationList = skuAllocationRepository.findAllAllocations();
			skuAllocationList.forEach(akuAllocation -> {
				hashMap.put(akuAllocation.getUpcCode(), akuAllocation);
			});
			if (upcCode.isPresent()) {

				for (HashMap<String, Object> map : listMap) {
					if (map.get("upcCode").equals(upcCode.get())) {

						if (hashMap.containsKey(upcCode.get())) {
							SKUAllocation skuAllocation = hashMap.get(upcCode.get());
							map.put("skuAllocationData", skuAllocation);
							if (skuAllocation != null) {
								Tags tags = tagsRepository.findByTagId(skuAllocation.getTagId());
								if (tags != null)
									map.put("assignedTagMacId", tags.getMacId());
							}
						}

						returnmap.put("data", map);
						break;
					}
				}

				if (!returnmap.containsKey("data")) {
					returnmap.put("data", null);
					returnmap.put("message", "UPC Code not found.");
					returnmap.put("status", "error");
				} else {
					returnmap.put("message", "SKU details");
					returnmap.put("status", "success");
				}
			} else {

				for (HashMap<String, Object> map : listMap) {
					if (hashMap.containsKey(map.get("upcCode"))) {
						SKUAllocation skuAllocation = hashMap.get(map.get("upcCode"));
						map.put("skuAllocationData", skuAllocation);
						Tags tags = tagsRepository.findByTagId(skuAllocation.getTagId());
						if (tags != null)
							map.put("assignedTagMacId", tags.getMacId());
					}
				}
				returnmap.put("data", listMap);
				returnmap.put("message", "SKU details");
				returnmap.put("status", "success");
			}

			logsServices.addLog(AppConstants.event, AppConstants.SKUDETAILS, "getSKUDetails",
					returnmap.get("message").toString(), null);

		} else {
			returnmap.put("status", "error");
			returnmap.put("message", AppConstants.errormsg);

			logsServices.addLog(AppConstants.event, AppConstants.SKUDETAILS, "getSKUDetails",
					returnmap.get("message").toString(), null);
		}

		return new ResponseEntity<>(returnmap, HttpStatus.OK);
	}

}
