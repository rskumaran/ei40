package com.entappia.ei4o.controllers;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.concurrent.ExecutionException;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.entappia.ei4o.constants.AppConstants;
import com.entappia.ei4o.dbmodels.CampusDetails;
import com.entappia.ei4o.dbmodels.PathWay;
import com.entappia.ei4o.repository.CampusDetailsRepository;
import com.entappia.ei4o.repository.PathWayRepository;
import com.entappia.ei4o.services.LogsServices;
import com.entappia.ei4o.utils.BaseController;
import com.entappia.ei4o.utils.Utils;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

@RestController
public class PathWayController  extends BaseController {

	@Autowired
	private LogsServices logsServices;

	@Autowired
	private PathWayRepository pathWayRepository;
  
	@Autowired
	private CampusDetailsRepository campusDetailsRepository;
	
	
	@RequestMapping(path = { "pathWay", "pathWay/{pathwayId}" }, method = RequestMethod.GET)
	public ResponseEntity<?> getPathWay(HttpServletRequest request, HttpSession httpSession,
			@PathVariable Optional<String> pathwayId) throws InterruptedException, ExecutionException {
		Map<String, Object> returnmap = new HashMap<>();
 		try {
			 

			
			ResponseEntity<?> responseEntity = checkSessionKey(httpSession, Utils.getCookie(request, "JSESSIONID"));
			if (responseEntity != null) { 
				logsServices.addLog(AppConstants.error, AppConstants.PATHWAY, "getPathWay",
						AppConstants.sessiontimeout, null);
				return responseEntity; 
			}
			 
			JSONObject jsonObject = new JSONObject();
			if (pathwayId.isPresent()) {
				jsonObject.put("pathwayId", pathwayId.get());
			} else {
				jsonObject.put("pathwayId", "all");
			}
			Map<String, String> reuestMap = new HashMap<>();
			reuestMap.put("params", jsonObject.toString()); 

			logsServices.addLog(AppConstants.event, AppConstants.PATHWAY, "getPathWay",
					"Path Segment Details Get API Details", reuestMap);
			 

			List<PathWay> pathWayList = new ArrayList<>();

			if (pathwayId.isPresent()) {
				PathWay pathWay = pathWayRepository.findById(Long.valueOf(pathwayId.get()));
				if (pathWay == null) {
					returnmap.put("status", "error");
					returnmap.put("message", "Segment id does not exist");
					logsServices.addLog(AppConstants.error, AppConstants.PATHWAY, "getPathWay",
							"Segment id does not exist", null);

					return new ResponseEntity<>(returnmap, HttpStatus.OK);
				}

				pathWayList.add(pathWay);
			} else {
				pathWayList = pathWayRepository.findAll();
			}
			logsServices.addLog(AppConstants.success, AppConstants.PATHWAY, "getPathWay",
					"Work Station details", null);

			returnmap.put("status", "success");
			returnmap.put("data", pathWayList);

			return new ResponseEntity<>(returnmap, HttpStatus.OK);
		} catch (Exception e) {
			returnmap.put("status", "error");
			returnmap.put("message", AppConstants.errormsg);
			logsServices.addLog(AppConstants.error, AppConstants.PATHWAY, "getPathWay", AppConstants.errormsg,
					null);

			return new ResponseEntity<>(returnmap, HttpStatus.OK);
		}

	}
	
	@PostMapping(value = "/pathWay")
	public ResponseEntity<?> addPathWay(HttpServletRequest request, HttpSession httpSession,

			@RequestBody HashMap<String, Object> requestMap) throws InterruptedException, ExecutionException {
		Map<String, String> returnmap = new HashMap<>();

		try {

			ResponseEntity<?> responseEntity = checkSessionKey(httpSession, Utils.getCookie(request, "JSESSIONID"), "CTPE");
			if (responseEntity != null) { 
				logsServices.addLog(AppConstants.error, AppConstants.PATHWAY, "addPathWay",
						AppConstants.sessiontimeout, null);
				return responseEntity; 
			}
			 
			 
			Map<String, String> reuestMap = new HashMap<>();
			reuestMap.put("params", new JSONObject(requestMap).toString()); 

			logsServices.addLog(AppConstants.event, AppConstants.PATHWAY, "addPathWay",
					"Path Way Details Add API Details", reuestMap);
			
			if (Utils.isEmptyString(requestMap.getOrDefault("campusId", "").toString())
					|| Utils.isEmptyString(requestMap.getOrDefault("name", "").toString())
					|| Utils.isEmptyString(requestMap.getOrDefault("geometryType", "").toString())
					|| Utils.isEmptyString(requestMap.getOrDefault("pathwayType", "").toString())
					|| Utils.isEmptyString(requestMap.getOrDefault("pathwayPattern", "").toString())
					|| Utils.isEmptyString(requestMap.getOrDefault("colorCode", "").toString())
					|| Utils.isEmptyString(requestMap.getOrDefault("coordinates", "").toString())) {

				returnmap.put("status", "error");
				returnmap.put("message", AppConstants.fieldsmissing);
 
				logsServices.addLog(AppConstants.error, AppConstants.PATHWAY, "addPathWay",
						AppConstants.fieldsmissing, null);
				return new ResponseEntity<>(returnmap, HttpStatus.OK);

			}
			
			
			CampusDetails campusDetails = campusDetailsRepository
					.findByCampusId(Long.valueOf(requestMap.getOrDefault("campusId", "").toString()));
			if (campusDetails == null) {
				returnmap.put("status", "error");
				returnmap.put("message", "Campus id not found");

				logsServices.addLog(AppConstants.error, AppConstants.PATHWAY, "addPathWay",
						returnmap.get("message").toString(), null);
				return new ResponseEntity<>(returnmap, HttpStatus.OK);
			} else if (!campusDetails.isActive()) {
				returnmap.put("status", "error");
				returnmap.put("message", "Campus is not active");

				logsServices.addLog(AppConstants.error, AppConstants.PATHWAY, "addPathWay",
						returnmap.get("message").toString(), null);
				return new ResponseEntity<>(returnmap, HttpStatus.OK);
			}
			

			Gson gson = new Gson();
			Type mapType = new TypeToken<List<HashMap<String, String>>>() {
			}.getType();

			List<HashMap<String, String>> coordinateList = gson
					.fromJson(requestMap.getOrDefault("coordinates", "").toString(), mapType);	
			
			PathWay pathWay = new PathWay();
			pathWay.setName(requestMap.getOrDefault("name", "").toString());
			pathWay.setCampusDetails(campusDetails); 
			pathWay.setGeometryType(requestMap.getOrDefault("geometryType", "").toString());
			pathWay.setPathwayType(requestMap.getOrDefault("pathwayType", "").toString());
			pathWay.setPathwayPattern(requestMap.getOrDefault("pathwayPattern", "").toString());
			pathWay.setColorCode(requestMap.getOrDefault("colorCode", "").toString());
			pathWay.setCoordinates(coordinateList);
			pathWay.setActive(true);
			pathWay.setCreatedDate(new Date());
			pathWay.setModifiedDate(new Date()); 

			pathWayRepository.save(pathWay);

			returnmap.put("status", "success");
			returnmap.put("message", pathWay.getName() + " added successfully");

			logsServices.addLog(AppConstants.event, AppConstants.PATHWAY, "addPathWay",
					"Pathway Id -" + pathWay.getId() + " added successfully, ", null);

			return new ResponseEntity<>(returnmap, HttpStatus.OK);
		} catch (Exception e) {
			returnmap.put("status", "error");
			returnmap.put("message", AppConstants.errormsg);

			logsServices.addLog(AppConstants.error, AppConstants.PATHWAY, "addPathWay", AppConstants.errormsg,
					null);
			return new ResponseEntity<>(returnmap, HttpStatus.OK);
		}
	}
	
	@PutMapping(value = "/pathWay")
	public ResponseEntity<?> updatePathWay(HttpServletRequest request, HttpSession httpSession,

			@RequestBody HashMap<String, Object> requestMap) throws InterruptedException, ExecutionException {
		Map<String, String> returnmap = new HashMap<>();

		try {
			
			ResponseEntity<?> responseEntity = checkSessionKey(httpSession, Utils.getCookie(request, "JSESSIONID"), "CTPE");
			if (responseEntity != null) { 
				logsServices.addLog(AppConstants.error, AppConstants.PATHWAY, "updatePathWay",
						AppConstants.sessiontimeout, null);
				return responseEntity; 
			}
			 
			 
			Map<String, String> reuestMap = new HashMap<>();
			reuestMap.put("params", new JSONObject(requestMap).toString()); 

			logsServices.addLog(AppConstants.event, AppConstants.PATHWAY, "updatePathWay",
					"Path Way Details Update API Details", reuestMap); 

			if (Utils.isEmptyString(requestMap.getOrDefault("id", "").toString())
					||Utils.isEmptyString(requestMap.getOrDefault("campusId", "").toString())
					|| Utils.isEmptyString(requestMap.getOrDefault("name", "").toString())
					|| Utils.isEmptyString(requestMap.getOrDefault("geometryType", "").toString())
					|| Utils.isEmptyString(requestMap.getOrDefault("pathwayType", "").toString())
					|| Utils.isEmptyString(requestMap.getOrDefault("pathwayPattern", "").toString())
					|| Utils.isEmptyString(requestMap.getOrDefault("colorCode", "").toString())
					|| Utils.isEmptyString(requestMap.getOrDefault("coordinates", "").toString())) {

				returnmap.put("status", "error");
				returnmap.put("message", AppConstants.fieldsmissing);
 

				logsServices.addLog(AppConstants.error, AppConstants.PATHWAY, "updatePathWay",
						AppConstants.fieldsmissing, null);
				return new ResponseEntity<>(returnmap, HttpStatus.OK);

			}
			
			CampusDetails campusDetails = campusDetailsRepository
					.findByCampusId(Long.valueOf(requestMap.getOrDefault("campusId", "").toString()));
			if (campusDetails == null) { 
				returnmap.put("status", "error");
				returnmap.put("message", "Campus id not found");

				logsServices.addLog(AppConstants.error, AppConstants.PATHWAY, "updatePathWay",
						returnmap.get("message").toString(), null);
				return new ResponseEntity<>(returnmap, HttpStatus.OK);
			}else if (!campusDetails.isActive()) {
				returnmap.put("status", "error");
				returnmap.put("message", "Campus is not active");

				logsServices.addLog(AppConstants.error, AppConstants.PATHWAY, "updatePathWay",
						returnmap.get("message").toString(), null);
				return new ResponseEntity<>(returnmap, HttpStatus.OK);
			}

			PathWay pathWay = pathWayRepository.findById(Long.valueOf(requestMap.getOrDefault("id", "").toString()));
			if (pathWay == null) {
				returnmap.put("status", "error");
				returnmap.put("message", "PathWay Id not found");

				logsServices.addLog(AppConstants.error, AppConstants.PATHWAY, "updatePathWay",
						"Work Station id already exist", null);
				return new ResponseEntity<>(returnmap, HttpStatus.OK);
			}else if (!pathWay.isActive()) {
				returnmap.put("status", "error");
				returnmap.put("message", "Segment Id is not active");

				logsServices.addLog(AppConstants.error, AppConstants.PATHWAY, "updatePathWay",
						"Work Station id already exist", null);
				return new ResponseEntity<>(returnmap, HttpStatus.OK);
			}
 
			  
			Gson gson = new Gson();
			Type mapType = new TypeToken<List<HashMap<String, String>>>() {
			}.getType();

			List<HashMap<String, String>> coordinateList = gson
					.fromJson(requestMap.getOrDefault("coordinates", "").toString(), mapType);	
						 
			pathWay.setName(requestMap.getOrDefault("name", "").toString());
			pathWay.setCampusDetails(campusDetails);
			pathWay.setGeometryType(requestMap.getOrDefault("geometryType", "").toString());
			pathWay.setPathwayType(requestMap.getOrDefault("pathwayType", "").toString());
			pathWay.setPathwayPattern(requestMap.getOrDefault("pathwayPattern", "").toString());
			pathWay.setColorCode(requestMap.getOrDefault("colorCode", "").toString());
			pathWay.setCoordinates(coordinateList); 
			pathWay.setModifiedDate(new Date());  

			pathWayRepository.save(pathWay);

			returnmap.put("status", "success");
			returnmap.put("message", pathWay.getId() + " updated successfully");

			logsServices.addLog(AppConstants.event, AppConstants.PATHWAY, "updatePathWay",
					"Pathway ID -" + pathWay.getId() + " updated successfully, ", null);

			return new ResponseEntity<>(returnmap, HttpStatus.OK);
		} catch (Exception e) {
			returnmap.put("status", "error");
			returnmap.put("message", AppConstants.errormsg);

			logsServices.addLog(AppConstants.error, AppConstants.PATHWAY, "updatePathWay", AppConstants.errormsg,
					null);
			return new ResponseEntity<>(returnmap, HttpStatus.OK);
		}
	}
	
	@DeleteMapping(value = "/pathWay")
	public ResponseEntity<?> deletePathWay(HttpServletRequest request, HttpSession httpSession,

			@RequestBody HashMap<String, Object> requestMap) throws InterruptedException, ExecutionException {
		Map<String, String> returnmap = new HashMap<>();

		try {
			
			ResponseEntity<?> responseEntity = checkSessionKey(httpSession, Utils.getCookie(request, "JSESSIONID"), "CTPE");
			if (responseEntity != null) { 
				logsServices.addLog(AppConstants.error, AppConstants.PATHWAY, "deletePathWay",
						AppConstants.sessiontimeout, null);
				return responseEntity; 
			}
			 
			 
			Map<String, String> reuestMap = new HashMap<>();
			reuestMap.put("params", new JSONObject(requestMap).toString()); 

			logsServices.addLog(AppConstants.event, AppConstants.PATHWAY, "deletePathWay",
					"Path Way Details Delete API Details", reuestMap); 


			if (Utils.isEmptyString(requestMap.getOrDefault("pathwayId", "").toString())  ) {

				returnmap.put("status", "error");
				returnmap.put("message", AppConstants.fieldsmissing);
				if (AppConstants.print_log)
					System.out.println("pathwayId : " + requestMap.getOrDefault("pathwayId", "").toString()); 
				logsServices.addLog(AppConstants.error, AppConstants.PATHWAY, "deletePathWay",
						AppConstants.fieldsmissing, null);
				return new ResponseEntity<>(returnmap, HttpStatus.OK);

			}

			PathWay pathWay = pathWayRepository.findById(Long.valueOf(requestMap.getOrDefault("pathwayId", "").toString()));
			if (pathWay == null) {
				returnmap.put("status", "error");
				returnmap.put("message", "Segment Id not found");

				logsServices.addLog(AppConstants.error, AppConstants.PATHWAY, "deletePathWay",
						"Work Station id already exist", null);
				return new ResponseEntity<>(returnmap, HttpStatus.OK);
			}else if (!pathWay.isActive()) {
				returnmap.put("status", "error");
				returnmap.put("message", "Segment Id is not active");

				logsServices.addLog(AppConstants.error, AppConstants.PATHWAY, "deletePathWay",
						"Work Station id already exist", null);
				return new ResponseEntity<>(returnmap, HttpStatus.OK);
			}

			pathWay.setActive(false);
			pathWay.setModifiedDate(new Date());

			pathWayRepository.save(pathWay);

			returnmap.put("status", "success");
			returnmap.put("message", pathWay.getId() + " status Deleted successfully");

			logsServices.addLog(AppConstants.event, AppConstants.PATHWAY, "deletePathWay",
					"Pathway ID -" + pathWay.getId() + " status Deleted successfully, ", null);

			return new ResponseEntity<>(returnmap, HttpStatus.OK);
		} catch (Exception e) {
			returnmap.put("status", "error");
			returnmap.put("message", AppConstants.errormsg);

			logsServices.addLog(AppConstants.error, AppConstants.PATHWAY, "deletePathWay", AppConstants.errormsg,
					null);
			return new ResponseEntity<>(returnmap, HttpStatus.OK);
		}
	}
	
}
