package com.entappia.ei4o.controllers;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.concurrent.ExecutionException;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.entappia.ei4o.constants.AppConstants;
import com.entappia.ei4o.constants.AppConstants.AssignmentStatus;
import com.entappia.ei4o.constants.AppConstants.AssignmentType;
import com.entappia.ei4o.dbmodels.AssetChrono;
import com.entappia.ei4o.dbmodels.AssetNonChrono;
import com.entappia.ei4o.dbmodels.AssetNonChronoAllocation;
import com.entappia.ei4o.dbmodels.CalibrationRecord;
import com.entappia.ei4o.dbmodels.Department;
import com.entappia.ei4o.dbmodels.Staff;
import com.entappia.ei4o.dbmodels.Tags;
import com.entappia.ei4o.models.AssetAssignmentDetails;
import com.entappia.ei4o.repository.AssetChronoAllocationRepository;
import com.entappia.ei4o.repository.AssetNonChronoAllocationRepository;
import com.entappia.ei4o.repository.AssetNonChronoRepository;
import com.entappia.ei4o.repository.CalibrationRecordRepository;
import com.entappia.ei4o.repository.DepartmentRepository;
import com.entappia.ei4o.repository.StaffRepository;
import com.entappia.ei4o.repository.TagsRepository;
import com.entappia.ei4o.services.LogsServices;
import com.entappia.ei4o.utils.BaseController;
import com.entappia.ei4o.utils.Utils;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;

@RestController
public class AssetNonChronoController extends BaseController {

	@Autowired
	private LogsServices logsServices;

	@Autowired
	private AssetNonChronoRepository assetNonChronoRepository;

	@Autowired
	private DepartmentRepository departmentRepository;

	@Autowired
	private CalibrationRecordRepository calibrationRecordRepository;

	@Autowired
	private StaffRepository staffRepository;

	@Autowired
	private TagsRepository tagsRepository;

	@Autowired
	private AssetNonChronoAllocationRepository assetNonChronoAllocationRepository;

	@RequestMapping(path = { "assetNonChrono", "assetNonChrono/{assetId}" }, method = RequestMethod.GET)
	public ResponseEntity<?> getAssetNonChrono(HttpServletRequest request, HttpSession httpSession,
			@PathVariable Optional<String> assetId) throws InterruptedException, ExecutionException {
		Map<String, Object> returnmap = new HashMap<>();
		HashMap<String, String> reuestMap = new HashMap<>();
		try {
			
			//"ANVA"
			ResponseEntity<?> responseEntity = checkSessionKey(httpSession, Utils.getCookie(request, "JSESSIONID"));
			if (responseEntity != null) { 
				
				logsServices.addLog(AppConstants.error, AppConstants.ASSTNCHO, "getAssetNonChrono",
						AppConstants.sessiontimeout, null);
				
				return responseEntity; 
			}
			 
			
			JSONObject jsonObject = new JSONObject();
			if (assetId.isPresent()) {
				jsonObject.put("assetId", assetId.get());
			} else {
				jsonObject.put("assetId", "all");
			}

			reuestMap.put("params", jsonObject.toString());
 
			
			logsServices.addLog(AppConstants.event, AppConstants.ASSTNCHO, "getAssetNonChrono",
					"Asset NonChrono get API Call", reuestMap);

			List<Tags> tagsList = tagsRepository.listOfAssignedTagsByType("Asset NonChrono");

			HashMap<String, Tags> tagsMap = new HashMap<>();

			tagsList.forEach(tags -> {
				tagsMap.put(tags.getAssignmentId(), tags);
			});

			List<AssetNonChrono> assetNonChronoDetails = new ArrayList<>();

			if (assetId.isPresent()) {
				AssetNonChrono assetNonChrono = assetNonChronoRepository.findByAssetId(assetId.get());
				if (assetNonChrono == null) {
					returnmap.put("status", "error");
					returnmap.put("message", "AssetNonChrono id does not exist");
					logsServices.addLog(AppConstants.error, AppConstants.ASSTNCHO, "getAssetNonChrono",
							"AssetVehicle id does not exist", null);

					return new ResponseEntity<>(returnmap, HttpStatus.OK);
				}

				assetNonChronoDetails.add(assetNonChrono);
			} else {
				assetNonChronoDetails = assetNonChronoRepository.findAll();
			}
			logsServices.addLog(AppConstants.success, AppConstants.ASSTNCHO, "getAssetNonChrono",
					"AssetNonChrono details", null);

			List<Map<String, Object>> assetNonChronoMapDetails = new ArrayList<>();

			final ObjectMapper mapper = new ObjectMapper();
			for (AssetNonChrono assetNonChrono : assetNonChronoDetails) {
				Map<String, Object> map = mapper.convertValue(assetNonChrono, new TypeReference<Map<String, Object>>() {
				});
 
				Tags tags = tagsMap.get(assetNonChrono.getAssetId());
				if (tags != null) {
					map.put("assignedTagMacId", tags.getMacId());
					map.put("assignedStatus", tags.getStatus());
				} else {
					map.put("assignedTagMacId", "");
					map.put("assignedStatus", "");
				}
			

				assetNonChronoMapDetails.add(map);
			}
			returnmap.put("status", "success");
			returnmap.put("data", assetNonChronoMapDetails);

			return new ResponseEntity<>(returnmap, HttpStatus.OK);
		} catch (Exception e) {
			returnmap.put("status", "error");
			returnmap.put("message", AppConstants.errormsg);
			logsServices.addLog(AppConstants.error, AppConstants.ASSTNCHO, "getAssetNonChrono", AppConstants.errormsg,
					null);

			return new ResponseEntity<>(returnmap, HttpStatus.OK);
		}

	}

	@PostMapping(value = "/assetNonChrono")
	public ResponseEntity<?> AddAssetNonChrono(HttpServletRequest request, HttpSession httpSession,

			@RequestBody HashMap<String, Object> requestMap) throws InterruptedException, ExecutionException {
		Map<String, String> returnmap = new HashMap<>();
		try {

			ResponseEntity<?> responseEntity = checkSessionKey(httpSession, Utils.getCookie(request, "JSESSIONID"), "ANAD");
			if (responseEntity != null) { 
				
				logsServices.addLog(AppConstants.error, AppConstants.ASSTNCHO, "AddAssetNonChrono",
						AppConstants.sessiontimeout, null);
				
				return responseEntity; 
			}
			
			Map<String, String> requestLogMap = new HashMap<>(); 
			requestLogMap.put("params", new JSONObject(requestMap).toString());
			
			logsServices.addLog(AppConstants.event, AppConstants.ASSTNCHO, "AddAssetNonChrono",
					"Asset NonChrono add API Call", requestLogMap);
			
			if (Utils.isEmptyString(requestMap.getOrDefault("assetId", "").toString())
					|| Utils.isEmptyString(requestMap.getOrDefault("assetType", "").toString())
					|| Utils.isEmptyString(requestMap.getOrDefault("subType", "").toString())
					|| Utils.isEmptyString(requestMap.getOrDefault("type", "").toString())
					|| Utils.isEmptyString(requestMap.getOrDefault("rangeValue", "").toString())
					|| Utils.isEmptyString(requestMap.getOrDefault("manufacturer", "").toString())
					|| Utils.isEmptyString(requestMap.getOrDefault("calibrationId", "").toString())) {

				returnmap.put("status", "error");
				returnmap.put("message", AppConstants.fieldsmissing);

				logsServices.addLog(AppConstants.error, AppConstants.ASSTNCHO, "AddAssetNonChrono",
						AppConstants.fieldsmissing, null);
				return new ResponseEntity<>(returnmap, HttpStatus.OK);

			}

			AssetNonChrono assetNonChrono = assetNonChronoRepository
					.findByAssetId(requestMap.getOrDefault("assetId", "").toString());
			if (assetNonChrono != null && assetNonChrono.isActive()) {
				returnmap.put("status", "error");
				returnmap.put("message", "AssetNonChrono id already exist");

				logsServices.addLog(AppConstants.error, AppConstants.ASSTNCHO, "AddAssetNonChrono",
						returnmap.get("message"), null);
				return new ResponseEntity<>(returnmap, HttpStatus.OK);
			}

			 
			CalibrationRecord calibrationRecord = null;

			if (!requestMap.getOrDefault("calibrationId", "").toString().equals("0")) {
				calibrationRecord = calibrationRecordRepository
						.findByCalibrationId(requestMap.getOrDefault("calibrationId", "").toString());
				if (calibrationRecord == null) {
					returnmap.put("status", "error");
					returnmap.put("message", "Calibration Id is not exist");

					logsServices.addLog(AppConstants.error, AppConstants.ASSTNCHO, "AddAssetNonChrono",
							returnmap.get("message"), null);
					return new ResponseEntity<>(returnmap, HttpStatus.OK);
				} else if (!calibrationRecord.isActive()) {
					returnmap.put("status", "error");
					returnmap.put("message", "Calibration Id is not active");

					logsServices.addLog(AppConstants.error, AppConstants.ASSTNCHO, "AddAssetNonChrono",
							returnmap.get("message"), null);
					return new ResponseEntity<>(returnmap, HttpStatus.OK);
				}
			}

			assetNonChrono = new AssetNonChrono();

			assetNonChrono.setAssetId(requestMap.getOrDefault("assetId", "").toString());
			assetNonChrono.setAssetType(requestMap.getOrDefault("assetType", "").toString());
			assetNonChrono.setSubType(requestMap.getOrDefault("subType", "").toString());
			assetNonChrono.setType(requestMap.getOrDefault("type", "").toString());
			assetNonChrono.setRangeValue(requestMap.getOrDefault("rangeValue", "").toString());
			assetNonChrono.setManufacturer(requestMap.getOrDefault("manufacturer", "").toString());
			assetNonChrono.setCalibrationRecord(calibrationRecord);
			assetNonChrono.setAssignmentStatus(AssignmentStatus.NOT_ASSIGNED);
			assetNonChrono.setCreatedDate(new Date());
			assetNonChrono.setModifiedDate(new Date());
			assetNonChrono.setActive(true);

			assetNonChronoRepository.save(assetNonChrono);

			returnmap.put("status", "success");
			returnmap.put("message", assetNonChrono.getAssetId() + " added successfully");

			logsServices.addLog(AppConstants.event, AppConstants.ASSTNCHO, "AddAssetNonChrono",
					"AssetNonChrono ID -" + assetNonChrono.getAssetId() + " added successfully, ", null);

			return new ResponseEntity<>(returnmap, HttpStatus.OK);
		} catch (Exception e) {
			returnmap.put("status", "error");
			returnmap.put("message", AppConstants.errormsg);

			logsServices.addLog(AppConstants.error, AppConstants.ASSTNCHO, "AddAssetNonChrono", AppConstants.errormsg,
					null);
			return new ResponseEntity<>(returnmap, HttpStatus.OK);
		}
	}

	@PutMapping(value = "/assetNonChrono")
	public ResponseEntity<?> updateAssetNonChrono(HttpServletRequest request, HttpSession httpSession,

			@RequestBody HashMap<String, Object> requestMap) throws InterruptedException, ExecutionException {
		Map<String, String> returnmap = new HashMap<>();
		try {

			ResponseEntity<?> responseEntity = checkSessionKey(httpSession, Utils.getCookie(request, "JSESSIONID"), "ANED");
			if (responseEntity != null) { 
				logsServices.addLog(AppConstants.error, AppConstants.ASSTNCHO, "updateAssetNonChrono",
						AppConstants.sessiontimeout, null);
				return responseEntity; 
			}
			
			Map<String, String> requestLogMap = new HashMap<>(); 
			requestLogMap.put("params", new JSONObject(requestMap).toString());
			
			logsServices.addLog(AppConstants.event, AppConstants.ASSTNCHO, "updateAssetNonChrono",
					"Asset NonChrono Update API Call", requestLogMap);
			
			if (Utils.isEmptyString(requestMap.getOrDefault("assetId", "").toString())
					|| Utils.isEmptyString(requestMap.getOrDefault("assetType", "").toString())
					|| Utils.isEmptyString(requestMap.getOrDefault("subType", "").toString())
					|| Utils.isEmptyString(requestMap.getOrDefault("type", "").toString())
					|| Utils.isEmptyString(requestMap.getOrDefault("rangeValue", "").toString())
					|| Utils.isEmptyString(requestMap.getOrDefault("manufacturer", "").toString())
					|| Utils.isEmptyString(requestMap.getOrDefault("calibrationId", "").toString())) {

				returnmap.put("status", "error");
				returnmap.put("message", AppConstants.fieldsmissing);

				logsServices.addLog(AppConstants.error, AppConstants.ASSTNCHO, "updateAssetNonChrono",
						AppConstants.fieldsmissing, null);
				return new ResponseEntity<>(returnmap, HttpStatus.OK);

			}

			AssetNonChrono assetNonChrono = assetNonChronoRepository
					.findByAssetId(requestMap.getOrDefault("assetId", "").toString());
			if (assetNonChrono == null) {
				returnmap.put("status", "error");
				returnmap.put("message", "AssetNonChrono id not exist");

				logsServices.addLog(AppConstants.error, AppConstants.ASSTNCHO, "updateAssetNonChrono",
						"AssetNonChrono id not exist", null);
				return new ResponseEntity<>(returnmap, HttpStatus.OK);
			} else if (!assetNonChrono.isActive()) {
				returnmap.put("status", "error");
				returnmap.put("message", "AssetNonChrono id is not active");

				logsServices.addLog(AppConstants.error, AppConstants.ASSTNCHO, "updateAssetNonChrono",
						returnmap.get("message").toString(), null);
				return new ResponseEntity<>(returnmap, HttpStatus.OK);
			}

			CalibrationRecord calibrationRecord = null;

			if (!requestMap.getOrDefault("calibrationId", "").toString().equals("0")) {

				calibrationRecord = calibrationRecordRepository
						.findByCalibrationId(requestMap.getOrDefault("calibrationId", "").toString());
				if (calibrationRecord == null) {
					returnmap.put("status", "error");
					returnmap.put("message", "Calibration Id is not exist");

					logsServices.addLog(AppConstants.error, AppConstants.ASSTNCHO, "updateAssetNonChrono",
							returnmap.get("message"), null);
					return new ResponseEntity<>(returnmap, HttpStatus.OK);
				} else if (!calibrationRecord.isActive()) {
					returnmap.put("status", "error");
					returnmap.put("message", "Calibration Id is not active");

					logsServices.addLog(AppConstants.error, AppConstants.ASSTNCHO, "updateAssetNonChrono",
							returnmap.get("message"), null);
					return new ResponseEntity<>(returnmap, HttpStatus.OK);
				}

			}

			assetNonChrono.setAssetId(requestMap.getOrDefault("assetId", "").toString());
			assetNonChrono.setAssetType(requestMap.getOrDefault("assetType", "").toString());
			assetNonChrono.setSubType(requestMap.getOrDefault("subType", "").toString());
			assetNonChrono.setType(requestMap.getOrDefault("type", "").toString());
			assetNonChrono.setRangeValue(requestMap.getOrDefault("rangeValue", "").toString());
			assetNonChrono.setManufacturer(requestMap.getOrDefault("manufacturer", "").toString());

			assetNonChrono.setCalibrationRecord(calibrationRecord);

			assetNonChrono.setModifiedDate(new Date());

			assetNonChronoRepository.save(assetNonChrono);

			returnmap.put("status", "success");
			returnmap.put("message", assetNonChrono.getAssetId() + " updated successfully");

			logsServices.addLog(AppConstants.event, AppConstants.ASSTNCHO, "updateAssetNonChrono",
					"AssetNonChrono ID -" + assetNonChrono.getAssetId() + " updated successfully, ", null);

			return new ResponseEntity<>(returnmap, HttpStatus.OK);
		} catch (Exception e) {
			returnmap.put("status", "error");
			returnmap.put("message", AppConstants.errormsg);

			logsServices.addLog(AppConstants.error, AppConstants.ASSTNCHO, "updateAssetNonChrono",
					AppConstants.errormsg, null);
			return new ResponseEntity<>(returnmap, HttpStatus.OK);
		}
	}

	@PutMapping(value = "/assignAssetNonChrono")
	public ResponseEntity<?> assignAssetNonChrono(HttpServletRequest request, HttpSession httpSession,

			@RequestBody HashMap<String, Object> requestMap) throws InterruptedException, ExecutionException {
		Map<String, String> returnmap = new HashMap<>();
		try {


			ResponseEntity<?> responseEntity = checkSessionKey(httpSession, Utils.getCookie(request, "JSESSIONID"), "ANAR");
			if (responseEntity != null) {
				logsServices.addLog(AppConstants.error, AppConstants.ASSTNCHO, "assignAssetNonChrono",
						AppConstants.sessiontimeout, null);
				return responseEntity; 
			}
			
			Map<String, String> requestLogMap = new HashMap<>(); 
			requestLogMap.put("params", new JSONObject(requestMap).toString());
			
			
			logsServices.addLog(AppConstants.event, AppConstants.ASSTNCHO, "assignAssetNonChrono",
					"Asset NonChrono assignment API Call", requestLogMap);
			
			if (Utils.isEmptyString(requestMap.getOrDefault("assetId", "").toString())
					|| Utils.isEmptyString(requestMap.getOrDefault("fromTime", "").toString())
					|| Utils.isEmptyString(requestMap.getOrDefault("toTime", "").toString())
					|| Utils.isEmptyString(requestMap.getOrDefault("assignmentType", "").toString())) {

				returnmap.put("status", "error");
				returnmap.put("message", AppConstants.fieldsmissing);

				logsServices.addLog(AppConstants.error, AppConstants.ASSTNCHO, "assignAssetNonChrono",
						AppConstants.fieldsmissing, null);
				return new ResponseEntity<>(returnmap, HttpStatus.OK);

			}

			if (!Utils.validateDateTime(requestMap.getOrDefault("fromTime", "").toString())
					|| !Utils.validateDateTime(requestMap.getOrDefault("toTime", "").toString())) {
				returnmap.put("status", "error");
				returnmap.put("message", AppConstants.errorDateTimemsg);

				logsServices.addLog(AppConstants.error, AppConstants.ASSTNCHO, "assignAssetNonChrono",
						returnmap.get("message"), null);
				return new ResponseEntity<>(returnmap, HttpStatus.OK);
			}

			AssignmentType assignmentType = null;
			String a_type = requestMap.getOrDefault("assignmentType", "").toString();
			if (a_type.equalsIgnoreCase("PERSON"))
				assignmentType = AssignmentType.PERSON;
			else if (a_type.equalsIgnoreCase("DEPT"))
				assignmentType = AssignmentType.DEPT;
			else {
				returnmap.put("status", "error");
				returnmap.put("message", "Invalid assignment type");

				logsServices.addLog(AppConstants.error, AppConstants.ASSTNCHO, "assignAssetNonChrono",
						returnmap.get("message"), null);
				return new ResponseEntity<>(returnmap, HttpStatus.OK);
			}

			AssetNonChrono assetNonChrono = assetNonChronoRepository
					.findByAssetId(requestMap.getOrDefault("assetId", "").toString());
			if (assetNonChrono == null) {
				returnmap.put("status", "error");
				returnmap.put("message", "AssetNonChrono id not exist");

				logsServices.addLog(AppConstants.error, AppConstants.ASSTNCHO, "assignAssetNonChrono",
						"AssetNonChrono id not exist", null);
				return new ResponseEntity<>(returnmap, HttpStatus.OK);
			} else if (!assetNonChrono.isActive()) {
				returnmap.put("status", "error");
				returnmap.put("message", "AssetNonChrono id is not active");

				logsServices.addLog(AppConstants.error, AppConstants.ASSTNCHO, "assignAssetNonChrono",
						returnmap.get("message").toString(), null);
				return new ResponseEntity<>(returnmap, HttpStatus.OK);
			}

			Department department = null;
			Staff staff = null;
			String macId = "";

			if (assignmentType == AssignmentType.DEPT) {
				if (Utils.isEmptyString(requestMap.getOrDefault("deptName", "").toString())) {
					returnmap.put("status", "error");
					returnmap.put("message", "Missing department name");

					logsServices.addLog(AppConstants.error, AppConstants.ASSTNCHO, "assignAssetNonChrono",
							returnmap.get("message"), null);
					return new ResponseEntity<>(returnmap, HttpStatus.OK);
				} else {
					department = departmentRepository
							.findByDeptName(requestMap.getOrDefault("deptName", "").toString());
					if (department == null) {
						returnmap.put("status", "error");
						returnmap.put("message", "Department Name is not exist");

						logsServices.addLog(AppConstants.error, AppConstants.ASSTNCHO, "assignAssetNonChrono",
								returnmap.get("message"), null);
						return new ResponseEntity<>(returnmap, HttpStatus.OK);
					} else if (!department.isActive()) {
						returnmap.put("status", "error");
						returnmap.put("message", "Department Name is not active");

						logsServices.addLog(AppConstants.error, AppConstants.ASSTNCHO, "assignAssetNonChrono",
								returnmap.get("message"), null);
						return new ResponseEntity<>(returnmap, HttpStatus.OK);
					}

					macId = department.getDeptName();

				}
			} else if (assignmentType == AssignmentType.PERSON) {
				try {
					if (Utils.isEmptyString(requestMap.getOrDefault("empId", "").toString())) {
						returnmap.put("status", "error");
						returnmap.put("message", "Missing Employee id");

						logsServices.addLog(AppConstants.error, AppConstants.ASSTNCHO, "assignAssetNonChrono",
								returnmap.get("message"), null);
						return new ResponseEntity<>(returnmap, HttpStatus.OK);
					} else {
						staff = staffRepository.findByEmpId(requestMap.getOrDefault("empId", "").toString());
						if (staff == null) {
							returnmap.put("status", "error");
							returnmap.put("message", "Employee id is not exist");

							logsServices.addLog(AppConstants.error, AppConstants.ASSTNCHO, "assignAssetNonChrono",
									returnmap.get("message"), null);
							return new ResponseEntity<>(returnmap, HttpStatus.OK);
						} else if (!staff.isActive()) {
							returnmap.put("status", "error");
							returnmap.put("message", "Employee id is not active");

							logsServices.addLog(AppConstants.error, AppConstants.ASSTNCHO, "assignAssetNonChrono",
									returnmap.get("message"), null);
							return new ResponseEntity<>(returnmap, HttpStatus.OK);
						}

						macId = staff.getEmpId();
					}
				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
			AssetNonChronoAllocation assetNonChronoAllocation = null;
			if (assignmentType == AssignmentType.PERSON) {
				assetNonChronoAllocation = assetNonChronoAllocationRepository.findByAssetIdAndAssignmentTypeAndMacId(
						assetNonChrono.getAssetId(), AppConstants.AssignmentType.PERSON, macId);

				if (assetNonChronoAllocation != null
						&& assetNonChronoAllocation.getStatus().equalsIgnoreCase("Valid")) {

					returnmap.put("status", "error");
					returnmap.put("message", "AssetNonChrono id is already assigned to Employee ("
							+ assetNonChronoAllocation.getMacId() + ")");

					logsServices.addLog(AppConstants.error, AppConstants.ASSTNCHO, "assignAssetNonChrono",
							returnmap.get("message"), null);
					return new ResponseEntity<>(returnmap, HttpStatus.OK);
				}
			} else if (assignmentType == AssignmentType.PERSON) {
				assetNonChronoAllocation = assetNonChronoAllocationRepository.findByAssetIdAndAssignmentTypeAndMacId(
						assetNonChrono.getAssetId(), AppConstants.AssignmentType.DEPT, macId);

				if (assetNonChronoAllocation != null
						&& assetNonChronoAllocation.getStatus().equalsIgnoreCase("Valid")) {

					returnmap.put("status", "error");
					returnmap.put("message", "AssetNonChrono id is already assigned to Department ("
							+ assetNonChronoAllocation.getMacId() + ")");

					logsServices.addLog(AppConstants.error, AppConstants.ASSTCHO, "assignAssetChrono",
							returnmap.get("message"), null);
					return new ResponseEntity<>(returnmap, HttpStatus.OK);
				}
			}

			HashMap<String, AssetAssignmentDetails> assignmentDetails = assetNonChrono.getAssetAssignmentDetails();
			if (assignmentDetails == null) {
				assignmentDetails = new HashMap<>();
			}

			AssetAssignmentDetails assetAssignmentDetails = null;
			if (assignmentDetails.containsKey(macId)) {
				assetAssignmentDetails = assignmentDetails.get(macId);
			}

			if (assetAssignmentDetails == null) {
				assetAssignmentDetails = new AssetAssignmentDetails();
			}

			if (assignmentType == AssignmentType.PERSON) {
				assetAssignmentDetails.setEmpId(macId);
				assetAssignmentDetails.setDeptName("");
				assetAssignmentDetails.setAssignmentType("PERSON");
			} else {

				assetAssignmentDetails.setEmpId("");
				assetAssignmentDetails.setDeptName(macId);
				assetAssignmentDetails.setAssignmentType("DEPT");
			}

			assetAssignmentDetails.setFromTime(requestMap.getOrDefault("fromTime", "").toString());
			assetAssignmentDetails.setToTime(requestMap.getOrDefault("toTime", "").toString());

			assignmentDetails.put(macId, assetAssignmentDetails);
			assetNonChrono.setModifiedDate(new Date());
			assetNonChrono.setAssignmentStatus(AssignmentStatus.ASSIGNED);

			assetNonChronoRepository.save(assetNonChrono);

			if (assetNonChronoAllocation == null) {
				assetNonChronoAllocation = new AssetNonChronoAllocation();
				assetNonChronoAllocation.setAssetId(assetNonChrono.getAssetId());
				assetNonChronoAllocation.setMacId(macId);
				assetNonChronoAllocation.setAssignmentType(assignmentType);
			}

			assetNonChronoAllocation.setStatus("Valid");
			assetNonChronoAllocation.setAllocationStartTime(new Date());
			assetNonChronoAllocation.setAllocationEndTime(null);

			assetNonChronoAllocationRepository.save(assetNonChronoAllocation);

			returnmap.put("status", "success");
			returnmap.put("message", assetNonChrono.getAssetId() + " assigned successfully");

			logsServices.addLog(AppConstants.event, AppConstants.ASSTNCHO, "assignAssetNonChrono",
					"AssetNonChrono ID -" + assetNonChrono.getAssetId() + " assigned successfully, ", null);

			return new ResponseEntity<>(returnmap, HttpStatus.OK);
		} catch (Exception e) {
			returnmap.put("status", "error");
			returnmap.put("message", AppConstants.errormsg);

			logsServices.addLog(AppConstants.error, AppConstants.ASSTNCHO, "assignAssetNonChrono",
					AppConstants.errormsg, null);
			return new ResponseEntity<>(returnmap, HttpStatus.OK);
		}
	}

	@PostMapping(value = "/returnAssetNonChrono")
	public ResponseEntity<?> returnAssetNonChrono(HttpServletRequest request, HttpSession httpSession,

			@RequestBody HashMap<String, Object> requestMap) throws InterruptedException, ExecutionException {
		Map<String, String> returnmap = new HashMap<>();
		try {

			ResponseEntity<?> responseEntity = checkSessionKey(httpSession, Utils.getCookie(request, "JSESSIONID"), "ANAR");
			if (responseEntity != null) { 
				logsServices.addLog(AppConstants.error, AppConstants.ASSTNCHO, "returnAssetNonChrono",
						AppConstants.sessiontimeout, null);
				return responseEntity; 
			}
			
			Map<String, String> requestLogMap = new HashMap<>(); 
			requestLogMap.put("params", new JSONObject(requestMap).toString());
			 
			logsServices.addLog(AppConstants.event, AppConstants.ASSTNCHO, "returnAssetNonChrono",
					"Asset NonChrono return asset API Call", requestLogMap);
			
			if (Utils.isEmptyString(requestMap.getOrDefault("assetId", "").toString())
					|| Utils.isEmptyString(requestMap.getOrDefault("assignmentType", "").toString())) {

				returnmap.put("status", "error");
				returnmap.put("message", AppConstants.fieldsmissing);

				logsServices.addLog(AppConstants.error, AppConstants.ASSTNCHO, "returnAssetNonChrono",
						AppConstants.fieldsmissing, null);
				return new ResponseEntity<>(returnmap, HttpStatus.OK);

			}

			AssignmentType assignmentType = null;
			String a_type = requestMap.getOrDefault("assignmentType", "").toString();
			if (a_type.equalsIgnoreCase("PERSON"))
				assignmentType = AssignmentType.PERSON;
			else if (a_type.equalsIgnoreCase("DEPT"))
				assignmentType = AssignmentType.DEPT;
			else {
				returnmap.put("status", "error");
				returnmap.put("message", "Invalid assignment type");

				logsServices.addLog(AppConstants.error, AppConstants.ASSTNCHO, "returnAssetNonChrono",
						returnmap.get("message"), null);
				return new ResponseEntity<>(returnmap, HttpStatus.OK);
			}

			if (assignmentType == AssignmentType.PERSON
					&& Utils.isEmptyString(requestMap.getOrDefault("empId", "").toString())) {
				returnmap.put("status", "error");
				returnmap.put("message", "Missing Employee id");

				logsServices.addLog(AppConstants.error, AppConstants.ASSTNCHO, "returnAssetNonChrono",
						returnmap.get("message"), null);
				return new ResponseEntity<>(returnmap, HttpStatus.OK);
			} else if (assignmentType == AssignmentType.DEPT
					&& Utils.isEmptyString(requestMap.getOrDefault("deptName", "").toString())) {
				returnmap.put("status", "error");
				returnmap.put("message", "Missing Dept Name");

				logsServices.addLog(AppConstants.error, AppConstants.ASSTNCHO, "returnAssetNonChrono",
						returnmap.get("message"), null);
				return new ResponseEntity<>(returnmap, HttpStatus.OK);
			}

			String macId = "";
			AssetNonChrono assetNonChrono = assetNonChronoRepository
					.findByAssetId(requestMap.getOrDefault("assetId", "").toString());
			if (assetNonChrono == null) {
				returnmap.put("status", "error");
				returnmap.put("message", "AssetNonChrono id not exist");

				logsServices.addLog(AppConstants.error, AppConstants.ASSTNCHO, "returnAssetNonChrono",
						"AssetNonChrono id not exist", null);
				return new ResponseEntity<>(returnmap, HttpStatus.OK);
			} else if (!assetNonChrono.isActive()) {
				returnmap.put("status", "error");
				returnmap.put("message", "AssetNonChrono id is not active");

				logsServices.addLog(AppConstants.error, AppConstants.ASSTNCHO, "returnAssetNonChrono",
						returnmap.get("message").toString(), null);
				return new ResponseEntity<>(returnmap, HttpStatus.OK);
			}

			if (assignmentType == AssignmentType.PERSON) {
				Staff staff = staffRepository.findByEmpId(requestMap.getOrDefault("empId", "").toString());
				if (staff == null) {
					returnmap.put("status", "error");
					returnmap.put("message", "Assigned Person id is not exist");

					logsServices.addLog(AppConstants.error, AppConstants.ASSTNCHO, "returnAssetNonChrono",
							returnmap.get("message"), null);
					return new ResponseEntity<>(returnmap, HttpStatus.OK);
				} else if (!staff.isActive()) {
					returnmap.put("status", "error");
					returnmap.put("message", "Assigned Person id is not active");

					logsServices.addLog(AppConstants.error, AppConstants.ASSTNCHO, "returnAssetNonChrono",
							returnmap.get("message"), null);
					return new ResponseEntity<>(returnmap, HttpStatus.OK);
				}
				macId = staff.getEmpId();
			} else if (assignmentType == AssignmentType.DEPT) {

				Department department = departmentRepository
						.findByDeptName(requestMap.getOrDefault("deptName", "").toString());
				if (department == null) {
					returnmap.put("status", "error");
					returnmap.put("message", "Department Name is not exist");

					logsServices.addLog(AppConstants.error, AppConstants.ASSTNCHO, "returnAssetNonChrono",
							returnmap.get("message"), null);
					return new ResponseEntity<>(returnmap, HttpStatus.OK);
				} else if (!department.isActive()) {
					returnmap.put("status", "error");
					returnmap.put("message", "Department Name is not active");

					logsServices.addLog(AppConstants.error, AppConstants.ASSTNCHO, "returnAssetNonChrono",
							returnmap.get("message"), null);
					return new ResponseEntity<>(returnmap, HttpStatus.OK);
				}
				macId = department.getDeptName();
			}

			AssetNonChronoAllocation assetNonChronoAllocation = assetNonChronoAllocationRepository
					.findByAssetIdAndMacId(assetNonChrono.getAssetId(), macId);

			if (assetNonChronoAllocation == null || !assetNonChronoAllocation.getStatus().equalsIgnoreCase("valid")) {
				returnmap.put("status", "error");
				returnmap.put("message", "No assignment found for given asset");

				logsServices.addLog(AppConstants.error, AppConstants.ASSTNCHO, "returnAssetNonChrono",
						returnmap.get("message"), null);
				return new ResponseEntity<>(returnmap, HttpStatus.OK);
			}

			assetNonChronoAllocation.setStatus("Completed");
			assetNonChronoAllocation.setAllocationEndTime(new Date());

			assetNonChronoAllocationRepository.save(assetNonChronoAllocation);

			HashMap<String, AssetAssignmentDetails> assignmentDetails = assetNonChrono.getAssetAssignmentDetails();
			if (assignmentDetails != null) {
				if (assignmentDetails.containsKey(macId)) {
					assignmentDetails.remove(macId);
				}

				if (assignmentDetails.size() > 0)
					assetNonChrono.setAssignmentStatus(AssignmentStatus.ASSIGNED);
				else
					assetNonChrono.setAssignmentStatus(AssignmentStatus.NOT_ASSIGNED);

				assetNonChrono.setAssetAssignmentDetails(assignmentDetails);
			} else
				assetNonChrono.setAssignmentStatus(AssignmentStatus.NOT_ASSIGNED);

			assetNonChrono.setModifiedDate(new Date());
			assetNonChronoRepository.save(assetNonChrono);

			returnmap.put("status", "success");
			returnmap.put("message", assetNonChrono.getAssetId() + " returned successfully");

			logsServices.addLog(AppConstants.event, AppConstants.ASSTNCHO, "returnAssetNonChrono",
					"AssetNonChrono ID -" + assetNonChrono.getAssetId() + " updated successfully, ", null);

			return new ResponseEntity<>(returnmap, HttpStatus.OK);
		} catch (Exception e) {
			returnmap.put("status", "error");
			returnmap.put("message", AppConstants.errormsg);

			logsServices.addLog(AppConstants.error, AppConstants.ASSTNCHO, "returnAssetNonChrono",
					AppConstants.errormsg, null);
			return new ResponseEntity<>(returnmap, HttpStatus.OK);
		}
	}

	@DeleteMapping(value = "/assetNonChrono")
	public ResponseEntity<?> deleteAssetNonChrono(HttpServletRequest request, HttpSession httpSession,
			@RequestBody HashMap<String, Object> requestMap) throws InterruptedException, ExecutionException {

		Map<String, String> returnmap = new HashMap<>();
		try {

			ResponseEntity<?> responseEntity = checkSessionKey(httpSession, Utils.getCookie(request, "JSESSIONID"), "ANED");
			if (responseEntity != null) { 
				logsServices.addLog(AppConstants.error, AppConstants.ASSTNCHO, "deleteAssetNonChrono",
						AppConstants.sessiontimeout, null);
				return responseEntity; 
			}
			
			Map<String, String> requestLogMap = new HashMap<>(); 
			requestLogMap.put("params", new JSONObject(requestMap).toString());
			 
			logsServices.addLog(AppConstants.event, AppConstants.ASSTNCHO, "deleteAssetNonChrono",
					"Asset NonChrono delete asset API Call", requestLogMap);
			
			if (Utils.isEmptyString(requestMap.getOrDefault("assetId", "").toString())) {

				returnmap.put("status", "error");
				returnmap.put("message", AppConstants.fieldsmissing);

				logsServices.addLog(AppConstants.error, AppConstants.ASSTNCHO, "deleteAssetNonChrono",
						AppConstants.fieldsmissing, null);
				return new ResponseEntity<>(returnmap, HttpStatus.OK);

			}

			AssetNonChrono assetNonChrono = assetNonChronoRepository
					.findByAssetId(requestMap.getOrDefault("assetId", "").toString());

			if (assetNonChrono == null) {
				returnmap.put("status", "error");
				returnmap.put("message", "Asset Non Chrono Id not found");

				logsServices.addLog(AppConstants.error, AppConstants.ASSTNCHO, "deleteAssetNonChrono",
						returnmap.get("message").toString(), null);
				return new ResponseEntity<>(returnmap, HttpStatus.OK);
			} else if (!assetNonChrono.isActive()) {
				returnmap.put("status", "error");
				returnmap.put("message", "Asset Non Chrono id is not active");

				logsServices.addLog(AppConstants.error, AppConstants.ASSTNCHO, "deleteAssetNonChrono",
						returnmap.get("message").toString(), null);
				return new ResponseEntity<>(returnmap, HttpStatus.OK);
			}

			assetNonChrono.setActive(false);
			assetNonChrono.setModifiedDate(new Date());
			assetNonChronoRepository.save(assetNonChrono);

			returnmap.put("status", "success");
			returnmap.put("message", "Asset Non Chrono status deleted successfully.");

			logsServices.addLog(AppConstants.event, AppConstants.ASSTNCHO, "deleteAssetNonChrono",
					"Asset Non Chrono Id-" + assetNonChrono.getAssetId() + " status deleted successfully, ", null);

			return new ResponseEntity<>(returnmap, HttpStatus.OK);
		} catch (Exception e) {
			returnmap.put("status", "error");
			returnmap.put("message", AppConstants.errormsg);

			logsServices.addLog(AppConstants.error, AppConstants.ASSTNCHO, "deleteAssetNonChrono",
					returnmap.get("message").toString(), null);
			return new ResponseEntity<>(returnmap, HttpStatus.OK);
		}
	}

	@PostMapping(value = "/multipleAssetNonChronos")
	public ResponseEntity<?> addMultipleAssetNonChronos(HttpServletRequest request, HttpSession httpSession,
			@RequestBody List<HashMap<String, Object>> requestMap) throws InterruptedException, ExecutionException {

		Map<String, Object> returnmap = new HashMap<>();
		try {

			ResponseEntity<?> responseEntity = checkSessionKey(httpSession, Utils.getCookie(request, "JSESSIONID"), "ANAD");
			if (responseEntity != null) { 
				logsServices.addLog(AppConstants.error, AppConstants.ASSTNCHO, "addMultipleAssetNonChronos",
						AppConstants.sessiontimeout, null);
				return responseEntity; 
			}
			
			ObjectMapper objectMapper = new ObjectMapper(); 
			String json = objectMapper.writeValueAsString(requestMap);
			

			Map<String, String> requestLogMap = new HashMap<>(); 
			requestLogMap.put("params", json); 
			
			logsServices.addLog(AppConstants.event, AppConstants.ASSTNCHO, "addMultipleAssetNonChronos",
					"Asset NonChrono add multiple asset API Call", requestLogMap);
			
			if (requestMap == null || requestMap.size() == 0) {
				returnmap.put("status", "error");
				returnmap.put("message", AppConstants.fieldsmissing);

				logsServices.addLog(AppConstants.error, AppConstants.ASSTCHO, "addMultipleAssetNonChronos",
						AppConstants.fieldsmissing, null);
				return new ResponseEntity<>(returnmap, HttpStatus.OK);
			}

			List<String> departmentList = getDepartmentNames();
			List<String> calibrationList = getCalibrationId();

			List<String> errorIds = new ArrayList<>();
			List<String> savedIds = new ArrayList<>();
			HashMap<String, AssetNonChrono> assetNonChronoMap = getAssetNonChronosNames();

			List<AssetNonChrono> assetNonChronoList = new ArrayList<>();
			for (HashMap<String, Object> map : requestMap) {
				if (Utils.isEmptyString(map.getOrDefault("assetId", "").toString())
						|| Utils.isEmptyString(map.getOrDefault("assetType", "").toString())
						|| Utils.isEmptyString(map.getOrDefault("subType", "").toString())
						|| Utils.isEmptyString(map.getOrDefault("type", "").toString())
						|| Utils.isEmptyString(map.getOrDefault("rangeValue", "").toString())
						|| Utils.isEmptyString(map.getOrDefault("manufacturer", "").toString())
						|| Utils.isEmptyString(map.getOrDefault("calibrationId", "").toString())) {

					errorIds.add(map.getOrDefault("assetId", "").toString());

				} else {
					if (!calibrationList.contains(map.getOrDefault("calibrationId", "").toString())
							|| !Utils.validateDate(map.getOrDefault("validityDate", "").toString())) {
						errorIds.add(map.getOrDefault("assetId", "").toString());
					} else {

						AssetNonChrono assetNonChrono = null;

						if (assetNonChronoMap.containsKey(map.getOrDefault("assetId", "").toString())) {
							assetNonChrono = assetNonChronoMap.get(map.getOrDefault("assetId", "").toString());
							if (assetNonChrono == null) {
								assetNonChrono = new AssetNonChrono();
								assetNonChrono.setAssetId(map.getOrDefault("assetId", "").toString());
								assetNonChrono.setActive(true);
								assetNonChrono.setCreatedDate(new Date());
							}
							assetNonChrono.setModifiedDate(new Date());
						} else {
							assetNonChrono = new AssetNonChrono();
							assetNonChrono.setCreatedDate(new Date());
							assetNonChrono.setModifiedDate(new Date());
							assetNonChrono.setAssetId(map.getOrDefault("assetId", "").toString());
							assetNonChrono.setActive(true);
						}

						if (assetNonChrono.isActive()) {

							CalibrationRecord calibrationRecord = calibrationRecordRepository
									.findByCalibrationId(map.getOrDefault("calibrationId", "").toString());

							assetNonChrono.setAssetType(map.getOrDefault("assetType", "").toString());
							assetNonChrono.setSubType(map.getOrDefault("subType", "").toString());
							assetNonChrono.setType(map.getOrDefault("type", "").toString());
							assetNonChrono.setRangeValue(map.getOrDefault("rangeValue", "").toString());
							assetNonChrono.setManufacturer(map.getOrDefault("manufacturer", "").toString());
							assetNonChrono.setCalibrationRecord(calibrationRecord);
							assetNonChronoList.add(assetNonChrono);

							savedIds.add(map.getOrDefault("assetId", "").toString());
						} else {
							errorIds.add(map.getOrDefault("assetId", "").toString());
						}
					}
				}
			}

			if (assetNonChronoList.size() > 0)
				assetNonChronoRepository.saveAll(assetNonChronoList);

			returnmap.put("status", "success");
			returnmap.put("message", savedIds.size() + " AssetNonChronos added successfully.");

			returnmap.put("savedIds", savedIds);
			returnmap.put("errorIds", errorIds);

			logsServices.addLog(AppConstants.event, AppConstants.ASSTCHO, "addMultipleAssetNonChronos",
					"AssetNonChrono- saved ids [" + String.join(",", savedIds) + "], error ids ["
							+ String.join(",", errorIds) + "].",
					null);

			return new ResponseEntity<>(returnmap, HttpStatus.OK);
		} catch (Exception e) {
			returnmap.put("status", "error");
			returnmap.put("message", AppConstants.errormsg);

			logsServices.addLog(AppConstants.error, AppConstants.ASSTCHO, "addMultipleAssetNonChronos",
					returnmap.get("message").toString(), null);
			return new ResponseEntity<>(returnmap, HttpStatus.OK);
		}
	}

	private List<String> getCalibrationId() {
		// TODO Auto-generated method stub

		List<String> list = new ArrayList<>();

		calibrationRecordRepository.findAll().stream().forEach(calibration -> {
			if (calibration.isActive()) {
				list.add(calibration.getCalibrationId());
			}
		});

		return list;
	}

	private List<String> getDepartmentNames() {
		// TODO Auto-generated method stub

		List<String> list = new ArrayList<>();

		departmentRepository.findAll().stream().forEach(department -> {
			if (department.isActive()) {
				list.add(department.getDeptName());
			}
		});

		return list;
	}

	private HashMap<String, AssetNonChrono> getAssetNonChronosNames() {
		// TODO Auto-generated method stub

		HashMap<String, AssetNonChrono> map = new HashMap<String, AssetNonChrono>();

		assetNonChronoRepository.findAll().stream().forEach(assetNonChrono -> {
			if (assetNonChrono.isActive()) {
				map.put(assetNonChrono.getAssetId(), assetNonChrono);
			}
		});

		return map;
	}

}
