package com.entappia.ei4o.controllers;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;
import java.util.concurrent.ExecutionException;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.entappia.ei4o.constants.AppConstants;
import com.entappia.ei4o.dbmodels.AssetChrono;
import com.entappia.ei4o.dbmodels.Tags;
import com.entappia.ei4o.dbmodels.WarehouseColumnName;
import com.entappia.ei4o.dbmodels.ZoneDetails;
import com.entappia.ei4o.repository.TagsRepository;
import com.entappia.ei4o.repository.WarehouseColumnNameRepository;
import com.entappia.ei4o.repository.WarehouseRepository;
import com.entappia.ei4o.repository.ZoneRepository;
import com.entappia.ei4o.services.LogsServices;
import com.entappia.ei4o.services.WarehouseEvent;
import com.entappia.ei4o.utils.BaseController;
import com.entappia.ei4o.utils.Utils;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;

@RestController
public class WarehouseController extends BaseController {

	@Autowired
	private WarehouseEvent warehouseEvent;

	@Autowired
	private WarehouseColumnNameRepository warehouseColumnNameRepository;

	@Autowired
	private WarehouseRepository warehouseRepository;

	@Autowired
	private TagsRepository tagsRepository;

	@Autowired
	private LogsServices logsServices;

	@Autowired
	private ZoneRepository zoneRepository;

	@RequestMapping(path = { "wareHouse", "wareHouse/{skuNo}" }, method = RequestMethod.GET)
	public ResponseEntity<?> getWareHouse(HttpServletRequest request, HttpSession httpSession,
			@PathVariable Optional<String> skuNo) throws InterruptedException, ExecutionException {
		Map<String, Object> returnmap = new HashMap<>();
 		try {
			 
			//, "HMVA"
			ResponseEntity<?> responseEntity = checkSessionKey(httpSession, Utils.getCookie(request, "JSESSIONID"));
			if (responseEntity != null) { 
				logsServices.addLog(AppConstants.error, AppConstants.WAREHOUSE, "getWareHouse",
						AppConstants.sessiontimeout, null);
				return responseEntity; 
			}
			
			
			JSONObject jsonObject = new JSONObject();
			if (skuNo.isPresent()) {
				jsonObject.put("skuNo", skuNo.get());
			} else {
				jsonObject.put("skuNo", "all");
			}
			Map<String, String> reuestMap = new HashMap<>();
			reuestMap.put("params", jsonObject.toString()); 
 

			logsServices.addLog(AppConstants.event, AppConstants.WAREHOUSE, "getWareHouse",
					"Get WareHouse data api call", reuestMap);
			
			
		  
			String query = warehouseRepository.getWarehouseQuery();

			List<HashMap<String, Object>> warehouseDetails = new ArrayList<>();

			String sSKUNo = "";
			if (skuNo.isPresent()) {
				sSKUNo = skuNo.get();
			}
			if (!Utils.isEmptyString(sSKUNo) && !sSKUNo.equalsIgnoreCase("all") && !sSKUNo.equalsIgnoreCase("valid")
					&& !sSKUNo.equalsIgnoreCase("completed")) {

				query = query + " where wh.sku_no='" + skuNo.get() + "'";

				warehouseDetails = warehouseRepository.readWarehouseData(query);
				if (warehouseDetails == null || warehouseDetails.size() == 0) {
					returnmap.put("status", "error");
					returnmap.put("message", "SKU Number does not exist");
					logsServices.addLog(AppConstants.error, AppConstants.WAREHOUSE, "getWareHouse",
							"AssetVehicle id does not exist", null);

					return new ResponseEntity<>(returnmap, HttpStatus.OK);
				}

			} else {

				if (sSKUNo.equalsIgnoreCase("valid")) {
					query = query + " where wh.status='Valid'";

				} else if (sSKUNo.equalsIgnoreCase("completed")) {
					query = query + " where wh.status='Completed'";
				}
				warehouseDetails = warehouseRepository.readWarehouseData(query);
			}

			logsServices.addLog(AppConstants.success, AppConstants.WAREHOUSE, "getWareHouse", "WareHouse details",
					null);

			returnmap.put("status", "success");
			returnmap.put("data", warehouseDetails);

			return new ResponseEntity<>(returnmap, HttpStatus.OK);
		} catch (Exception e) {
			returnmap.put("status", "error");
			returnmap.put("message", AppConstants.errormsg);
			logsServices.addLog(AppConstants.error, AppConstants.WAREHOUSE, "getWareHouse", AppConstants.errormsg,
					null);

			return new ResponseEntity<>(returnmap, HttpStatus.OK);
		}

	}

	@PostMapping(value = "/assignWareHouse")
	public ResponseEntity<?> assignWareHouse(HttpServletRequest request, HttpSession httpSession, 
			@RequestBody HashMap<String, String> requestMap)
			throws InterruptedException, ExecutionException, JSONException {

		Map<String, Object> returnmap = new HashMap<>();

		ResponseEntity<?> responseEntity = checkSessionKey(httpSession, Utils.getCookie(request, "JSESSIONID"), "HTAR");
		if (responseEntity != null) { 
			logsServices.addLog(AppConstants.error, AppConstants.WAREHOUSE, "assignWareHouse",
					AppConstants.sessiontimeout, null);
			return responseEntity; 
		}
		
		Map<String, String> reuestMap = new HashMap<>();
		reuestMap.put("params", new JSONObject(requestMap).toString()); 
		

		logsServices.addLog(AppConstants.event, AppConstants.WAREHOUSE, "assignWareHouse",
				"Assign WareHouse api call", reuestMap);
		
		if (Utils.isEmptyString(requestMap.getOrDefault("skuNo", ""))
				|| Utils.isEmptyString(requestMap.getOrDefault("tagId", ""))
				|| Utils.isEmptyString(requestMap.getOrDefault("zoneId", ""))) {

			returnmap.put("status", "error");
			returnmap.put("message", AppConstants.fieldsmissing);

			logsServices.addLog(AppConstants.error, AppConstants.WAREHOUSE, "assignWareHouse",
					returnmap.get("message").toString(), null);
			return new ResponseEntity<>(returnmap, HttpStatus.UNAUTHORIZED);
		}

		HashMap<String, String> map = warehouseEvent.getWarehouseDetails(requestMap.getOrDefault("skuNo", ""));
		if (map == null) {

			returnmap.put("status", "error");
			returnmap.put("message", "SKU Number not found");

			logsServices.addLog(AppConstants.error, AppConstants.WAREHOUSE, "assignWareHouse",
					returnmap.get("message").toString(), null);
			return new ResponseEntity<>(returnmap, HttpStatus.OK);
		}

		HashMap<String, Object> warehouseMap = warehouseRepository
				.getWarehouseBySKUNo(requestMap.getOrDefault("skuNo", ""));
		if (warehouseMap != null) {
			returnmap.put("status", "error");

			if (warehouseMap.get("status").toString().equalsIgnoreCase("valid")) {
				String arrival_date = warehouseMap.get("arrival_date").toString();
				String arrivalTime = Utils.formatDateTime(arrival_date);
				if (!Utils.isEmptyString(arrivalTime))
					returnmap.put("message",
							"SKU Number " + requestMap.getOrDefault("skuNo", "") + " already assigned at " + arrivalTime);
				else
					returnmap.put("message", "SKU Number " + requestMap.getOrDefault("skuNo", "") + " already assigned ");
			} else {
				String dispatch_date = warehouseMap.get("dispatch_date").toString();
				String dispatchTime = Utils.formatDateTime(dispatch_date);
				if (!Utils.isEmptyString(dispatchTime))
					returnmap.put("message", "SKU Number " + requestMap.getOrDefault("skuNo", "")
							+ " already dispatched at " + dispatchTime);
				else
					returnmap.put("message", "SKU Number " + requestMap.getOrDefault("skuNo", "") + " already dispatched");
			}

			logsServices.addLog(AppConstants.error, AppConstants.WAREHOUSE, "assignWareHouse",
					returnmap.get("message").toString(), null);
			return new ResponseEntity<>(returnmap, HttpStatus.OK);
		}

		Tags tags = tagsRepository.findByTagId(requestMap.getOrDefault("tagId", "").toString());

		if (tags == null) {
			returnmap.put("status", "error");
			returnmap.put("message", "Tag id not found.");

			logsServices.addLog(AppConstants.error, AppConstants.WAREHOUSE, "assignWareHouse",
					returnmap.get("message").toString(), null);
			return new ResponseEntity<>(returnmap, HttpStatus.OK);
		} else if (tags.getStatus().equalsIgnoreCase("blocked")) {
			returnmap.put("status", "error");
			returnmap.put("message", "Tag id  " + tags.getTagId() + " is blocked.");

			logsServices.addLog(AppConstants.error, AppConstants.WAREHOUSE, "assignWareHouse",
					returnmap.get("message").toString(), null);
			return new ResponseEntity<>(returnmap, HttpStatus.OK);
		} else if (!Utils.isEmptyString(tags.getStatus()) && !tags.getStatus().equalsIgnoreCase("free")) {
			returnmap.put("status", "error");

			if (tags.getStatus().equalsIgnoreCase("assigned"))
				returnmap.put("message", "Tag already assigned to " + tags.getType());
			else
				returnmap.put("message", "Can not assign " + tags.getStatus() + " tag.");

			logsServices.addLog(AppConstants.error, AppConstants.WAREHOUSE, "assignWareHouse",
					returnmap.get("message").toString(), null);
			return new ResponseEntity<>(returnmap, HttpStatus.OK);
		}

		ZoneDetails zoneDetails = zoneRepository
				.findById(Integer.valueOf(requestMap.getOrDefault("zoneId", "0").toString()).intValue());

		if (zoneDetails == null) {
			returnmap.put("status", "error");
			returnmap.put("message", "Zone id not found.");

			logsServices.addLog(AppConstants.error, AppConstants.WAREHOUSE, "assignWareHouse",
					returnmap.get("message").toString(), null);
			return new ResponseEntity<>(returnmap, HttpStatus.OK);
		} else if (!zoneDetails.isActive()) {
			returnmap.put("status", "error");
			returnmap.put("message", "Zone is not active.");

			logsServices.addLog(AppConstants.error, AppConstants.WAREHOUSE, "assignWareHouse",
					returnmap.get("message").toString(), null);
			return new ResponseEntity<>(returnmap, HttpStatus.OK);
		}

		List<WarehouseColumnName> listColumnNames = warehouseColumnNameRepository.findAll();
		List<String> columnNameList = new ArrayList<>();

		HashMap<String, String> hashMap = new HashMap<>();
		listColumnNames.forEach(warehouseColumnName -> {
			hashMap.put(warehouseColumnName.getColumnName(), warehouseColumnName.getFieldName());
			columnNameList.add(warehouseColumnName.getColumnName());
		});

		List<String> keyList = new ArrayList<>();
		Set<String> keyValus = map.keySet();
		for (String key : keyValus) {
			if (!hashMap.containsKey(key)) {
				keyList.add(key);
			}
		}

		if (keyList.size() > 0) {
			int columnSize = 0;
			if (listColumnNames.size() > 1)
				columnSize = listColumnNames.size() - 1;

			for (String key : keyList) {

				WarehouseColumnName warehouseColumnName = new WarehouseColumnName();
				warehouseColumnName.setColumnName(key);
				warehouseColumnName.setFieldName("fieldname" + columnSize);
				warehouseColumnNameRepository.save(warehouseColumnName);

				warehouseRepository.addNewColumn("fieldname" + columnSize);

				columnNameList.add(warehouseColumnName.getColumnName());

				hashMap.put(warehouseColumnName.getColumnName(), warehouseColumnName.getFieldName());

				columnSize++;

			}
		}

		String columnNames = getColumnName(columnNameList, hashMap);
		String columnValues = getColumnValues(columnNameList, hashMap, map, tags.getTagId(), zoneDetails.getId());

		String query = "insert into warehouse (" + columnNames + ") VALUES (" + columnValues + ")";
		if (AppConstants.print_log)
			System.out.println(query);

		boolean insert = warehouseRepository.insertWarehouse(query);
		if (AppConstants.print_log)
			System.out.println(insert ? "Inserted successfully" : "Error in insert");

		updateTag(tags, "WareHouse", "", "" + requestMap.getOrDefault("skuNo", ""), new Date(), "Assigned");

		returnmap.put("message", "Tag id (" + tags.getTagId() + ") assigned to SKU Number ("
				+ requestMap.getOrDefault("skuNo", "") + ") successfully");

		returnmap.put("status", "success");

		logsServices.addLog(AppConstants.error, AppConstants.WAREHOUSE, "assignWareHouse",
				returnmap.get("message").toString(), null);

		return new ResponseEntity<>(returnmap, HttpStatus.OK);
	}
	
	@GetMapping(value = "/wareHouseData/{skuNo}")
	public ResponseEntity<?> wareHouseData(HttpServletRequest request, HttpSession httpSession, @PathVariable Optional<String> skuNo)
			throws InterruptedException, ExecutionException, JSONException {

		Map<String, Object> returnmap = new HashMap<>();

		ResponseEntity<?> responseEntity = checkSessionKey(httpSession, Utils.getCookie(request, "JSESSIONID"), "HTAR");
		if (responseEntity != null) { 
			logsServices.addLog(AppConstants.error, AppConstants.WAREHOUSE, "wareHouseData",
					AppConstants.sessiontimeout, null);
			return responseEntity; 
		}

		JSONObject jsonObject = new JSONObject();
		jsonObject.put("skuNo", skuNo.get());
		 
		Map<String, String> reuestMap = new HashMap<>();
		reuestMap.put("params", jsonObject.toString()); 
		
		logsServices.addLog(AppConstants.event, AppConstants.WAREHOUSE, "wareHouseData",
				"Get WareHouse Data api call", reuestMap);
		
		if (Utils.isEmptyString(skuNo.get())) {

			returnmap.put("status", "error");
			returnmap.put("message", AppConstants.fieldsmissing);

			logsServices.addLog(AppConstants.error, AppConstants.WAREHOUSE, "wareHouseData",
					returnmap.get("message").toString(), null);
			return new ResponseEntity<>(returnmap, HttpStatus.UNAUTHORIZED);
		}

		HashMap<String, String> map = warehouseEvent.getWarehouseDetails(skuNo.get());
		if (map == null) {

			returnmap.put("status", "error");
			returnmap.put("message", "SKU Number not found");

			logsServices.addLog(AppConstants.error, AppConstants.WAREHOUSE, "wareHouseData",
					returnmap.get("message").toString(), null);
			return new ResponseEntity<>(returnmap, HttpStatus.OK);
		}
		returnmap.put("status", "success");
		returnmap.put("wareHouseData", map);
		logsServices.addLog(AppConstants.error, AppConstants.WAREHOUSE, "wareHouseData",
				"wareHouseData send for SKU Number: "+skuNo.get(), null);
		
		return new ResponseEntity<>(returnmap, HttpStatus.OK);
	}
	

	private void updateTag(Tags tags, String type, String subType, String assignmentId, Date assignmentDate,
			String status) {
		if (tags != null) {
			tags.setType(type);
			tags.setSubType(subType);
			tags.setAssignmentId(assignmentId);
			tags.setAssignmentDate(assignmentDate);
			tags.setModifiedDate(new Date());
			tags.setStatus(status);

			tagsRepository.save(tags);
		}

	}

	private String getColumnName(List<String> columnNameList, HashMap<String, String> hashMap) {
		String columnString = "";
		for (String columnName : columnNameList) {

			if (Utils.isEmptyString(columnString))
				columnString = hashMap.get(columnName);
			else
				columnString = columnString + "," + hashMap.get(columnName);
		}

		columnString = columnString + ",created_date,arrival_date,issue_date,modified_date,tag_id,status,zone_id";

		return columnString;
	}

	private String getColumnValues(List<String> columnNameList, HashMap<String, String> hashMap,
			HashMap<String, String> map, String tagId, int zoneId) {
		String valuesString = "";
		for (String columnName : columnNameList) {

			if (Utils.isEmptyString(valuesString))
				valuesString = "'" + map.get(columnName) + "'";
			else
				valuesString = valuesString + ",'" + map.get(columnName) + "'";
		}

		String currentTime = Utils.formatDateTime1(new Date());

		valuesString = valuesString + ",'" + currentTime + "','" + currentTime + "', '" + currentTime + "','"
				+ currentTime + "','" + tagId + "','Valid', " + zoneId;

		return valuesString;
	}

}
