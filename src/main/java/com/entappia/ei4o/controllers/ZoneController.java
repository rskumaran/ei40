package com.entappia.ei4o.controllers;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.concurrent.ExecutionException;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.entappia.ei4o.constants.AppConstants;
import com.entappia.ei4o.constants.AppConstants.ZoneType;
import com.entappia.ei4o.dbmodels.CampusDetails;
import com.entappia.ei4o.dbmodels.ZoneClassification;
import com.entappia.ei4o.dbmodels.ZoneDetails;
import com.entappia.ei4o.repository.CampusDetailsRepository;
import com.entappia.ei4o.repository.ZoneClassificationRepository;
import com.entappia.ei4o.repository.ZoneRepository;
import com.entappia.ei4o.services.LogsServices;
import com.entappia.ei4o.utils.BaseController;
import com.entappia.ei4o.utils.Utils;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

@RestController
public class ZoneController extends BaseController {

	@Autowired
	private LogsServices logsServices;

	@Autowired
	private ZoneRepository zoneRepository;

	@Autowired
	private ZoneClassificationRepository zoneClassificationRepository;

	@Autowired
	private CampusDetailsRepository campusDetailsRepository;

	private static Gson gson = new Gson();

	@RequestMapping(path = { "zone", "zone/{zoneId}" }, method = RequestMethod.GET)
	public ResponseEntity<?> getZone(HttpServletRequest request, HttpSession httpSession,
			@PathVariable Optional<String> zoneId) throws InterruptedException, ExecutionException {
		Map<String, Object> returnmap = new HashMap<>();
		 
		try {
			 

			//, "CZOM"
			ResponseEntity<?> responseEntity = checkSessionKey(httpSession, Utils.getCookie(request, "JSESSIONID"));
			if (responseEntity != null) { 
				logsServices.addLog(AppConstants.error, AppConstants.MAP, "getZone",
						AppConstants.sessiontimeout, null);
				
				return responseEntity; 
			}
			
			JSONObject jsonObject = new JSONObject();
			if (zoneId.isPresent()) {
				jsonObject.put("zoneId", zoneId.get());
			} else {
				jsonObject.put("zoneId", "all");
			}
			Map<String, String> reuestMap = new HashMap<>();
			reuestMap.put("params", jsonObject.toString()); 
			
			logsServices.addLog(AppConstants.event, AppConstants.MAP, "getZone",
					"Get ZoneDetails api call", reuestMap);
			
			List<ZoneDetails> zoneDetails = new ArrayList<>();

			if (zoneId.isPresent()) {
				ZoneDetails zone = zoneRepository.findById(Integer.valueOf(zoneId.get()).intValue());
				if (zone == null) {
					returnmap.put("status", "error");
					returnmap.put("message", "zone id does not exist");
					logsServices.addLog(AppConstants.error, AppConstants.MAP, "getZone", "zone id does not exist",
							null);

					return new ResponseEntity<>(returnmap, HttpStatus.OK);
				}

				zoneDetails.add(zone);
			} else {
				zoneDetails = zoneRepository.findAll();
			}
			logsServices.addLog(AppConstants.success, AppConstants.MAP, "getZone", "map details", null);

			returnmap.put("status", "success");
			returnmap.put("data", zoneDetails);

			return new ResponseEntity<>(returnmap, HttpStatus.OK);
		} catch (Exception e) {
			returnmap.put("status", "error");
			returnmap.put("message", AppConstants.errormsg);
			logsServices.addLog(AppConstants.error, AppConstants.MAP, "getZone", AppConstants.errormsg, null);

			return new ResponseEntity<>(returnmap, HttpStatus.OK);
		}

	}

	@PostMapping(value = "/zone")
	public ResponseEntity<?> addZone(HttpServletRequest request, HttpSession httpSession,

			@RequestBody HashMap<String, Object> requestMap) throws InterruptedException, ExecutionException {
		Map<String, String> returnmap = new HashMap<>();

		try {
			ResponseEntity<?> responseEntity = checkSessionKey(httpSession, Utils.getCookie(request, "JSESSIONID"), "CZOM");
			if (responseEntity != null) { 
				logsServices.addLog(AppConstants.error, AppConstants.MAP, "addZone",
						AppConstants.sessiontimeout, null);
				return responseEntity; 
			}
			 
			Map<String, String> reuestMap = new HashMap<>();
			reuestMap.put("params", new JSONObject(requestMap).toString()); 
			 
			logsServices.addLog(AppConstants.event, AppConstants.MAP, "addZone",
					"Add ZoneDetails api call", reuestMap);
			 
			int capacity = Integer.valueOf(requestMap.getOrDefault("capacity", "0").toString());
			if (Utils.isEmptyString(requestMap.getOrDefault("name", "").toString())
					|| Utils.isEmptyString(requestMap.getOrDefault("type", "").toString())

					|| Utils.isEmptyString(requestMap.getOrDefault("accessControl", "").toString())

					|| Utils.isEmptyString(requestMap.getOrDefault("zoneClassification", "").toString())
					|| Utils.isEmptyString(requestMap.getOrDefault("zoneType", "").toString())
					|| Utils.isEmptyString(requestMap.getOrDefault("description", "").toString())

					|| Utils.isEmptyString(requestMap.getOrDefault("campusId", "").toString())
					|| Utils.isEmptyString(requestMap.getOrDefault("area", "").toString())
					|| Utils.isEmptyString(requestMap.getOrDefault("geometryType", "").toString())
					|| Utils.isEmptyString(requestMap.getOrDefault("capacity", "").toString()) || capacity <= 0
					|| requestMap.getOrDefault("coordinateList", "") == null
					|| requestMap.getOrDefault("gridList", "") == null) {

				returnmap.put("status", "error");
				returnmap.put("message", AppConstants.fieldsmissing);

				logsServices.addLog(AppConstants.error, AppConstants.MAP, "addZone", AppConstants.fieldsmissing, null);
				return new ResponseEntity<>(returnmap, HttpStatus.OK);

			}

			int campusId = Integer.valueOf(requestMap.getOrDefault("campusId", "0").toString());

			CampusDetails campusDetails = campusDetailsRepository.findByCampusId(campusId);
			if (campusDetails == null) {
				returnmap.put("status", "error");
				returnmap.put("message", "Campus id not found");

				logsServices.addLog(AppConstants.error, AppConstants.MAP, "addZone", "Campus id not found", null);
				return new ResponseEntity<>(returnmap, HttpStatus.OK);
			} else if (!campusDetails.isActive()) {
				returnmap.put("status", "error");
				returnmap.put("message", "Campus is not active");

				logsServices.addLog(AppConstants.error, AppConstants.MAP, "addZone", "Campus is not active", null);
				return new ResponseEntity<>(returnmap, HttpStatus.OK);
			}

			int zoneClassificationId = Integer.valueOf(requestMap.getOrDefault("zoneClassification", "0").toString());

			ZoneClassification zoneClassification = zoneClassificationRepository.findById(zoneClassificationId);

			if (zoneClassification == null) {
				returnmap.put("status", "error");
				returnmap.put("message", "Zone Classification id is not found.");

				logsServices.addLog(AppConstants.error, AppConstants.MAP, "addZone", returnmap.get("message"), null);
				return new ResponseEntity<>(returnmap, HttpStatus.OK);
			} else if (!zoneClassification.isActive()) {
				returnmap.put("status", "error");
				returnmap.put("message", "Zone Classification id is not active.");

				logsServices.addLog(AppConstants.error, AppConstants.MAP, "addZone", returnmap.get("message"), null);
				return new ResponseEntity<>(returnmap, HttpStatus.OK);
			}

			ZoneDetails map = zoneRepository.findZoneDetails(requestMap.getOrDefault("name", "").toString(), true);
			if (map != null) {
				returnmap.put("status", "error");
				returnmap.put("message", "Zone Name already exist");

				logsServices.addLog(AppConstants.error, AppConstants.MAP, "addZone", "Zone Name already exist", null);
				return new ResponseEntity<>(returnmap, HttpStatus.OK);
			}

			JSONArray gridJSONArray = new JSONArray(requestMap.getOrDefault("gridList", "").toString());

			List<String> gridList = new ArrayList<>();

			if (gridJSONArray != null && gridJSONArray.length() > 0) {

				for (int i = 0; i < gridJSONArray.length(); i++) {
					String val = String.valueOf(gridJSONArray.get(i));
					gridList.add(val);
				}
			}

			ZoneDetails zoneDetails = new ZoneDetails();

			Type mapType = new TypeToken<List<HashMap<String, String>>>() {
			}.getType();
			List<HashMap<String, String>> coordinateList = gson
					.fromJson(requestMap.getOrDefault("coordinateList", "").toString(), mapType);

			zoneDetails.setName(requestMap.getOrDefault("name", "").toString());
			zoneDetails.setType(requestMap.getOrDefault("type", "").toString());
			zoneDetails.setAccessControl(requestMap.getOrDefault("accessControl", "").toString());
			if (requestMap.getOrDefault("zoneType", "").toString().equalsIgnoreCase("Tracking zone")
					||requestMap.getOrDefault("zoneType", "").toString().equalsIgnoreCase("Trackingzone"))
				zoneDetails.setZoneType(ZoneType.Trackingzone);
			else
				zoneDetails.setZoneType(ZoneType.DeadZone);
			zoneDetails.setCoordinateList(coordinateList);
			zoneDetails.setCampusDetails(campusDetails);
			zoneDetails.setZoneClassification(zoneClassification);
			zoneDetails.setArea(requestMap.getOrDefault("area", "").toString());
			zoneDetails.setGeometryType(requestMap.getOrDefault("geometryType", "").toString());
			zoneDetails.setDescription(requestMap.getOrDefault("description", "").toString());
			zoneDetails.setGridList(gridList);
			zoneDetails.setCapacity(capacity);
			zoneDetails.setModifiedDate(new Date());
			zoneDetails.setCreatedDate(new Date());
			zoneDetails.setActive(true);
			zoneRepository.save(zoneDetails);

			returnmap.put("status", "success");
			returnmap.put("message", zoneDetails.getName() + " added successfully");

			logsServices.addLog(AppConstants.event, AppConstants.MAP, "addZone",
					"map ID -" + zoneDetails.getName() + " added successfully, ", null);

			updateZoneGrids(zoneDetails.getCampusDetails().getCampusId());

			return new ResponseEntity<>(returnmap, HttpStatus.OK);
		} catch (Exception e) {
			returnmap.put("status", "error");
			returnmap.put("message", AppConstants.errormsg);

			logsServices.addLog(AppConstants.error, AppConstants.MAP, "addZone", AppConstants.errormsg, null);
			return new ResponseEntity<>(returnmap, HttpStatus.OK);
		}
	}

	@PutMapping(value = "/zone")
	public ResponseEntity<?> updateZone(HttpServletRequest request, HttpSession httpSession,
			@RequestBody HashMap<String, Object> requestMap) throws InterruptedException, ExecutionException {
		Map<String, String> returnmap = new HashMap<>();
		try {

			ResponseEntity<?> responseEntity = checkSessionKey(httpSession, Utils.getCookie(request, "JSESSIONID"), "CZOM");
			if (responseEntity != null) { 
				logsServices.addLog(AppConstants.error, AppConstants.MAP, "updatemap",
						AppConstants.sessiontimeout, null);
				return responseEntity; 
			} 

			Map<String, String> reuestMap = new HashMap<>();
			reuestMap.put("params", new JSONObject(requestMap).toString()); 

			logsServices.addLog(AppConstants.event, AppConstants.MAP, "updatemap",
					"Update ZoneDetails api call", reuestMap);
			
			int capacity = Integer.valueOf(requestMap.getOrDefault("capacity", "0").toString());

			if (Utils.isEmptyString(requestMap.getOrDefault("id", "").toString())
					|| Utils.isEmptyString(requestMap.getOrDefault("campusId", "").toString())
					|| Utils.isEmptyString(requestMap.getOrDefault("name", "").toString())
					|| Utils.isEmptyString(requestMap.getOrDefault("type", "").toString())
					|| Utils.isEmptyString(requestMap.getOrDefault("accessControl", "").toString())
					|| Utils.isEmptyString(requestMap.getOrDefault("zoneType", "").toString())
					|| (Utils.isEmptyString(requestMap.getOrDefault("capacity", "").toString()) || capacity <= 0)
					|| Utils.isEmptyString(requestMap.getOrDefault("zoneClassification", "").toString())
					|| Utils.isEmptyString(requestMap.getOrDefault("area", "").toString())
					|| Utils.isEmptyString(requestMap.getOrDefault("geometryType", "").toString())
					|| Utils.isEmptyString(requestMap.getOrDefault("description", "").toString())

					|| requestMap.getOrDefault("coordinateList", "") == null
					|| requestMap.getOrDefault("gridList", "") == null) {

				returnmap.put("status", "error");
				returnmap.put("message", AppConstants.fieldsmissing);

				logsServices.addLog(AppConstants.error, AppConstants.MAP, "updateZone", AppConstants.fieldsmissing,
						null);
				return new ResponseEntity<>(returnmap, HttpStatus.OK);

			}
			int zoneId = Integer.valueOf(requestMap.getOrDefault("id", "0").toString());

			ZoneDetails zoneDetails = zoneRepository.findById(zoneId);
			if (zoneDetails == null) {
				returnmap.put("status", "error");
				returnmap.put("message", "map name not exist");

				logsServices.addLog(AppConstants.error, AppConstants.MAP, "updatemap", "map name not  exist", null);
				return new ResponseEntity<>(returnmap, HttpStatus.OK);
			} else if (!zoneDetails.isActive()) {
				returnmap.put("status", "error");
				returnmap.put("message", "map id is not active");

				logsServices.addLog(AppConstants.error, AppConstants.MAP, "updateZone",
						returnmap.get("message").toString(), null);
				return new ResponseEntity<>(returnmap, HttpStatus.OK);
			}

			int campusId = Integer.valueOf(requestMap.getOrDefault("campusId", "0").toString());

			CampusDetails campusDetails = campusDetailsRepository.findByCampusId(campusId);
			if (campusDetails == null) {
				returnmap.put("status", "error");
				returnmap.put("message", "Campus id not found");

				logsServices.addLog(AppConstants.error, AppConstants.MAP, "updatemap", "Campus id not found", null);
				return new ResponseEntity<>(returnmap, HttpStatus.OK);
			} else if (!campusDetails.isActive()) {
				returnmap.put("status", "error");
				returnmap.put("message", "Campus is not active");

				logsServices.addLog(AppConstants.error, AppConstants.MAP, "updatemap", "Campus is not active", null);
				return new ResponseEntity<>(returnmap, HttpStatus.OK);
			}

			ZoneDetails map = zoneRepository.findZoneDetails(requestMap.getOrDefault("name", "").toString(), zoneId,
					true);
			if (map != null) {
				returnmap.put("status", "error");
				returnmap.put("message", "Zone Name already exist");

				logsServices.addLog(AppConstants.error, AppConstants.MAP, "addZone", "Zone Name already exist", null);
				return new ResponseEntity<>(returnmap, HttpStatus.OK);
			}

			int zoneClassificationId = Integer.valueOf(requestMap.getOrDefault("zoneClassification", "0").toString());

			ZoneClassification zoneClassification = zoneClassificationRepository.findById(zoneClassificationId);

			if (zoneClassification == null) {
				returnmap.put("status", "error");
				returnmap.put("message", "Zone Classification id is not found.");

				logsServices.addLog(AppConstants.error, AppConstants.MAP, "updatemap", returnmap.get("message"), null);
				return new ResponseEntity<>(returnmap, HttpStatus.OK);
			} else if (!zoneClassification.isActive()) {
				returnmap.put("status", "error");
				returnmap.put("message", "Zone Classification id is not active.");

				logsServices.addLog(AppConstants.error, AppConstants.MAP, "updatemap", returnmap.get("message"), null);
				return new ResponseEntity<>(returnmap, HttpStatus.OK);
			}

			JSONArray gridJSONArray = new JSONArray(requestMap.getOrDefault("gridList", "").toString());

			List<String> gridList = new ArrayList<>();

			if (gridJSONArray != null && gridJSONArray.length() > 0) {

				for (int i = 0; i < gridJSONArray.length(); i++) {
					String val = gridJSONArray.getString(i);
					gridList.add(val);
				}
			}

			Type mapType = new TypeToken<List<HashMap<String, String>>>() {
			}.getType();

			List<HashMap<String, String>> coordinateList = gson
					.fromJson(requestMap.getOrDefault("coordinateList", "").toString(), mapType);

			zoneDetails.setType(requestMap.getOrDefault("type", "").toString());
			zoneDetails.setAccessControl(requestMap.getOrDefault("accessControl", "").toString());
			zoneDetails.setCoordinateList(coordinateList);
			zoneDetails.setGridList(gridList);
			zoneDetails.setCampusDetails(campusDetails);

			zoneDetails.setName(requestMap.getOrDefault("name", "").toString());
			zoneDetails.setZoneClassification(zoneClassification);
			zoneDetails.setArea(requestMap.getOrDefault("area", "").toString());
			zoneDetails.setGeometryType(requestMap.getOrDefault("geometryType", "").toString());
			zoneDetails.setDescription(requestMap.getOrDefault("description", "").toString());

			zoneDetails.setCapacity(capacity);
			zoneDetails.setModifiedDate(new Date());
			if (requestMap.getOrDefault("zoneType", "").toString().equalsIgnoreCase("Tracking zone")
					|| requestMap.getOrDefault("zoneType", "").toString().equalsIgnoreCase("Trackingzone"))
				zoneDetails.setZoneType(ZoneType.Trackingzone);
			else
				zoneDetails.setZoneType(ZoneType.DeadZone);

			zoneRepository.save(zoneDetails);

			returnmap.put("status", "success");
			returnmap.put("message", zoneDetails.getName() + " updated successfully");

			updateZoneGrids(zoneDetails.getCampusDetails().getCampusId());

			logsServices.addLog(AppConstants.event, AppConstants.MAP, "updateZone",
					"map ID -" + zoneDetails.getName() + " updated successfully, ", null);

			return new ResponseEntity<>(returnmap, HttpStatus.OK);
		} catch (Exception e) {
			returnmap.put("status", "error");
			returnmap.put("message", AppConstants.errormsg);

			logsServices.addLog(AppConstants.error, AppConstants.MAP, "updateZone", AppConstants.errormsg, null);
			return new ResponseEntity<>(returnmap, HttpStatus.OK);
		}
	}

	@DeleteMapping(value = "/zone")
	public ResponseEntity<?> deleteZone(HttpServletRequest request, HttpSession httpSession,
			@RequestBody HashMap<String, Object> requestMap) throws InterruptedException, ExecutionException {

		Map<String, String> returnmap = new HashMap<>();
		try {
			
			ResponseEntity<?> responseEntity = checkSessionKey(httpSession, Utils.getCookie(request, "JSESSIONID"), "CZOM");
			if (responseEntity != null) { 
				logsServices.addLog(AppConstants.error, AppConstants.MAP, "deleteZone",
						AppConstants.sessiontimeout, null);
				return responseEntity; 
			}
			

			Map<String, String> reuestMap = new HashMap<>();
			reuestMap.put("params", new JSONObject(requestMap).toString()); 
			
			logsServices.addLog(AppConstants.event, AppConstants.MAP, "deleteZone",
					"Delete ZoneDetails api call", reuestMap);

			if (Utils.isEmptyString(requestMap.getOrDefault("id", "").toString())) {

				returnmap.put("status", "error");
				returnmap.put("message", AppConstants.fieldsmissing);

				logsServices.addLog(AppConstants.error, AppConstants.MAP, "deleteZone", AppConstants.fieldsmissing,
						null);
				return new ResponseEntity<>(returnmap, HttpStatus.OK);

			}

			int zoneId = Integer.valueOf(requestMap.getOrDefault("id", "0").toString());
			ZoneDetails zoneDetails = zoneRepository.findById(zoneId);

			if (zoneDetails == null) {
				returnmap.put("status", "error");
				returnmap.put("message", "Zone id not found");

				logsServices.addLog(AppConstants.error, AppConstants.MAP, "deleteZone",
						returnmap.get("message").toString(), null);
				return new ResponseEntity<>(returnmap, HttpStatus.OK);
			} else if (!zoneDetails.isActive()) {
				returnmap.put("status", "error");
				returnmap.put("message", "Zone is not active");

				logsServices.addLog(AppConstants.error, AppConstants.MAP, "deleteZone",
						returnmap.get("message").toString(), null);
				return new ResponseEntity<>(returnmap, HttpStatus.OK);
			}

			zoneDetails.setActive(false);
			zoneDetails.setModifiedDate(new Date());
			zoneRepository.save(zoneDetails);

			updateZoneGrids(zoneDetails.getCampusDetails().getCampusId());
			returnmap.put("status", "success");
			returnmap.put("message", "Zone status updated successfully.");

			logsServices.addLog(AppConstants.event, AppConstants.MAP, "deleteZone",
					"Zone -" + zoneDetails.getName() + " status updated successfully, ", null);

			return new ResponseEntity<>(returnmap, HttpStatus.OK);
		} catch (Exception e) {
			returnmap.put("status", "error");
			returnmap.put("message", AppConstants.errormsg);

			logsServices.addLog(AppConstants.error, AppConstants.MAP, "deleteZone", returnmap.get("message").toString(),
					null);
			return new ResponseEntity<>(returnmap, HttpStatus.OK);
		}
	}

	@PostMapping(value = "/importZoneDetails")
	public ResponseEntity<?> importZoneDetails(HttpServletRequest request, HttpSession httpSession,
			@RequestBody HashMap<String, Object> requestMap) throws InterruptedException, ExecutionException {
		Map<String, String> returnmap = new HashMap<>();
		try {
			
			ResponseEntity<?> responseEntity = checkSessionKey(httpSession, Utils.getCookie(request, "JSESSIONID"), "CZOM");
			if (responseEntity != null) { 
				logsServices.addLog(AppConstants.error, AppConstants.MAP, "importZoneDetails",
						AppConstants.sessiontimeout, null);
				return responseEntity; 
			}


			Map<String, String> reuestMap = new HashMap<>();
			reuestMap.put("params", new JSONObject(requestMap).toString()); 
			
			logsServices.addLog(AppConstants.event, AppConstants.MAP, "importZoneDetails",
					"Import ZoneDetails api call", reuestMap);
			
			if (Utils.isEmptyString(requestMap.getOrDefault("version", "").toString())
					|| Utils.isEmptyString(requestMap.getOrDefault("type", "").toString())
					|| Utils.isEmptyString(requestMap.getOrDefault("zone", "").toString())
					|| Utils.isEmptyString(requestMap.getOrDefault("classification", "").toString())) {

				returnmap.put("status", "error");
				returnmap.put("message", AppConstants.fieldsmissing);

				logsServices.addLog(AppConstants.error, AppConstants.MAP, "importZoneDetails",
						AppConstants.fieldsmissing, null);
				return new ResponseEntity<>(returnmap, HttpStatus.OK);

			}

			if (!requestMap.getOrDefault("type", "").toString().equalsIgnoreCase("zone")) {

				returnmap.put("status", "error");
				returnmap.put("message", "Invalid type");

				logsServices.addLog(AppConstants.error, AppConstants.MAP, "importZoneDetails", returnmap.get("message"),
						null);
				return new ResponseEntity<>(returnmap, HttpStatus.OK);
			}

			JSONArray classificationJSONArray = new JSONArray(
					gson.toJson(requestMap.getOrDefault("classification", "")));

			if (classificationJSONArray != null && classificationJSONArray.length() > 0) {
				for (int i = 0; i < classificationJSONArray.length(); i++) {

					JSONObject classificationJSONObject = classificationJSONArray.getJSONObject(i);
					if (classificationJSONObject != null) {
						if (classificationJSONObject.optLong("id") <= 0
								|| Utils.isEmptyString(classificationJSONObject.optString("className"))
								|| Utils.isEmptyString(classificationJSONObject.optString("color"))) {

							returnmap.put("status", "error");
							returnmap.put("message", "Missing Classification data");

							logsServices.addLog(AppConstants.error, AppConstants.MAP, "importZoneDetails",
									returnmap.get("message"), null);
							return new ResponseEntity<>(returnmap, HttpStatus.OK);

						}
					}
				}
			} else {
				returnmap.put("status", "error");
				returnmap.put("message", "Missing Classification data");

				logsServices.addLog(AppConstants.error, AppConstants.MAP, "importZoneDetails", returnmap.get("message"),
						null);
				return new ResponseEntity<>(returnmap, HttpStatus.OK);

			}

			CampusDetails campusDetails = campusDetailsRepository.findByCampusId(1);
			Type mapType = new TypeToken<List<HashMap<String, String>>>() {
			}.getType();

			JSONArray zoneJSONArray = new JSONArray(gson.toJson(requestMap.getOrDefault("zone", "")));

			if (zoneJSONArray != null && zoneJSONArray.length() > 0) {
				for (int i = 0; i < zoneJSONArray.length(); i++) {
					JSONObject zoneJSONObject = zoneJSONArray.getJSONObject(i);
					if (zoneJSONObject != null) {
						int capacity = Integer.valueOf(zoneJSONObject.optString("capacity", "0").toString());

						if (zoneJSONObject.optLong("id") <= 0 || zoneJSONObject.optLong("campusId") <= 0
								|| capacity <= 0 || Utils.isEmptyString(zoneJSONObject.optString("name"))
								|| Utils.isEmptyString(zoneJSONObject.optString("type"))
								|| Utils.isEmptyString(zoneJSONObject.optString("accessControl"))
								|| Utils.isEmptyString(zoneJSONObject.optString("zoneType"))
								|| Utils.isEmptyString(zoneJSONObject.optString("zoneClassification"))
								|| Utils.isEmptyString(zoneJSONObject.optString("area"))
								|| Utils.isEmptyString(zoneJSONObject.optString("geometryType"))
								|| Utils.isEmptyString(zoneJSONObject.optString("description"))
								|| Utils.isEmptyString(zoneJSONObject.getJSONArray("coordinateList").toString())
								|| Utils.isEmptyString(zoneJSONObject.getJSONArray("gridList").toString())) {

							returnmap.put("status", "error");
							returnmap.put("message", "Missing Zone data");

							logsServices.addLog(AppConstants.error, AppConstants.MAP, "importZoneDetails",
									returnmap.get("message"), null);
							return new ResponseEntity<>(returnmap, HttpStatus.OK);

						} else {
							List<HashMap<String, String>> coordinateList = gson
									.fromJson(zoneJSONObject.getJSONArray("coordinateList").toString(), mapType);

							for (int j = 0; j < coordinateList.size(); j++) {
								HashMap<String, String> coordinate = coordinateList.get(j);

								float x1 = Float.valueOf(coordinate.get("x"));
								float y1 = Float.valueOf(coordinate.get("y"));

								boolean contains = isInsideCampus(0f, 0f, campusDetails.getWidth(),
										campusDetails.getHeight(), x1, y1);
								if (!contains) {
									returnmap.put("status", "error");
									returnmap.put("message", "Zone coordinates are not inside the campus");

									logsServices.addLog(AppConstants.error, AppConstants.MAP, "importZoneDetails",
											returnmap.get("message"), null);
									return new ResponseEntity<>(returnmap, HttpStatus.OK);
								}

							}

						}
					}

				}

			} else {
				returnmap.put("status", "error");
				returnmap.put("message", "Missing Zone data");

				logsServices.addLog(AppConstants.error, AppConstants.MAP, "importZoneDetails", returnmap.get("message"),
						null);
				return new ResponseEntity<>(returnmap, HttpStatus.OK);

			}

			List<ZoneClassification> zoneClassifications = new ArrayList<>();
			List<String> classificationNameList = new ArrayList<>();

			HashMap<Long, String> classNameMap = new HashMap<>();

			Date runTime = new Date();
			for (int i = 0; i < classificationJSONArray.length(); i++) {

				JSONObject classificationJSONObject = classificationJSONArray.getJSONObject(i);
				if (classificationJSONObject != null) {

					classNameMap.put(classificationJSONObject.optLong("id"),
							classificationJSONObject.optString("className").toLowerCase());

					ZoneClassification zoneClassification = zoneClassificationRepository
							.findByClassName(classificationJSONObject.optString("className"));
					
					if (zoneClassification == null) {
						zoneClassification = new ZoneClassification();
						zoneClassification.setClassName(classificationJSONObject.optString("className"));
					}
					zoneClassification.setColor(classificationJSONObject.optString("color"));
					zoneClassification.setCreatedDate(runTime);
					zoneClassification.setModifiedDate(runTime);
					zoneClassification.setActive(true);

					zoneClassifications.add(zoneClassification);
					classificationNameList.add(zoneClassification.getClassName().toLowerCase());

				}
			}

			zoneClassificationRepository.saveAll(zoneClassifications);

			HashMap<String, ZoneClassification> classificationNameMap = new HashMap<>();
			List<ZoneClassification> dbZoneClassificationDetails = zoneClassificationRepository.findAll();
			if (dbZoneClassificationDetails != null && dbZoneClassificationDetails.size() > 0) {
				for (ZoneClassification zoneClassification : dbZoneClassificationDetails) {

					classificationNameMap.put(zoneClassification.getClassName().toLowerCase(), zoneClassification);
					if (zoneClassification != null
							&& !classificationNameList.contains(zoneClassification.getClassName().toLowerCase())) {
						zoneClassification.setModifiedDate(runTime);
						zoneClassification.setActive(false);
						zoneClassificationRepository.save(zoneClassification);
					}
				}
			}

			List<Integer> zoneIdList = new ArrayList<>(); 

			for (int i = 0; i < zoneJSONArray.length(); i++) {
				JSONObject zoneJSONObject = zoneJSONArray.getJSONObject(i);
				if (zoneJSONObject != null) {

					JSONArray gridJSONArray = new JSONArray(zoneJSONObject.getJSONArray("gridList").toString());

					List<String> gridList = new ArrayList<>();

					if (gridJSONArray != null && gridJSONArray.length() > 0) {

						for (int j = 0; j < gridJSONArray.length(); j++) {
							String val = String.valueOf(gridJSONArray.get(j));
							gridList.add(val);
						}
					}

					ZoneDetails zoneDetails = new ZoneDetails();

					int capacity = Integer.valueOf(zoneJSONObject.optString("capacity", "0"));

					List<HashMap<String, String>> coordinateList = gson
							.fromJson(zoneJSONObject.getJSONArray("coordinateList").toString(), mapType); 
					zoneDetails.setName(zoneJSONObject.optString("name"));
					zoneDetails.setType(zoneJSONObject.optString("type"));
					zoneDetails.setAccessControl(zoneJSONObject.optString("accessControl"));
					if (zoneJSONObject.optString("zoneType").equalsIgnoreCase("Tracking zone")
							||zoneJSONObject.optString("zoneType").equalsIgnoreCase("Trackingzone"))
						zoneDetails.setZoneType(ZoneType.Trackingzone);
					else
						zoneDetails.setZoneType(ZoneType.DeadZone);
					zoneDetails.setCoordinateList(coordinateList);
					zoneDetails.setCampusDetails(campusDetails);
					zoneDetails.setZoneClassification(classificationNameMap
							.get(classNameMap.get(zoneJSONObject.optLong("zoneClassification"))));
					zoneDetails.setArea(zoneJSONObject.optString("area"));
					zoneDetails.setGeometryType(zoneJSONObject.optString("geometryType"));
					zoneDetails.setDescription(zoneJSONObject.optString("description"));
					zoneDetails.setGridList(gridList);
					zoneDetails.setCapacity(capacity);
					zoneDetails.setModifiedDate(runTime);
					zoneDetails.setCreatedDate(runTime);
					zoneDetails.setActive(true);
 
					zoneDetails = zoneRepository.save(zoneDetails);
					
					zoneIdList.add(zoneDetails.getId());
				}
			}
			

			List<ZoneDetails> dbZoneDetails = zoneRepository.findAll();
			if (dbZoneDetails != null && dbZoneDetails.size() > 0) {
				for (ZoneDetails zoneDetails : dbZoneDetails) {  
					if (zoneDetails != null && !zoneIdList.contains(zoneDetails.getId())) {
						zoneDetails.setModifiedDate(runTime);
						zoneDetails.setActive(false);
						zoneRepository.save(zoneDetails);
					}
				}
			}

			updateZoneGrids(campusDetails.getCampusId());
			returnmap.put("status", "status");
			returnmap.put("message", "Zone imported successfully.");

			logsServices.addLog(AppConstants.event, AppConstants.MAP, "importZoneDetails", returnmap.get("message"),
					null);

			return new ResponseEntity<>(returnmap, HttpStatus.OK);
		} catch (Exception e) {
			returnmap.put("status", "error");
			returnmap.put("message", AppConstants.errormsg);

			logsServices.addLog(AppConstants.error, AppConstants.MAP, "importZoneDetails", AppConstants.errormsg, null);
			return new ResponseEntity<>(returnmap, HttpStatus.OK);
		}
	}

	private boolean isInsideCampus(float x1, float y1, float x2, float y2, float x, float y) {
		if (x >= x1 && x <= x2 && y >= y1 && y <= y2)
			return true;

		return false;
	}

	private void updateZoneGrids(long campusId) {

		CampusDetails campusDetails = campusDetailsRepository.findByCampusId(campusId);
		HashMap<String, List<String>> gridZoneList = Utils.getGridHashmap((int) Math.ceil(campusDetails.getWidth()),
				(int) Math.ceil(campusDetails.getHeight()));

		List<ZoneDetails> zoneDetailsList = zoneRepository.findAll();

		if (zoneDetailsList != null) {
			for (ZoneDetails zoneDetails : zoneDetailsList) {
				if (zoneDetails != null && zoneDetails.isActive()) {
					String sZoneID = String.valueOf(zoneDetails.getId());
					List<String> gridList = zoneDetails.getGridList();
					if (gridList != null && gridList.size() > 0) {
						for (String gridId : gridList) {
							if (gridZoneList.containsKey(gridId)) {
								List<String> zoneList = gridZoneList.get(gridId);
								if (zoneList == null)
									zoneList = new ArrayList<>();

								if (!zoneList.contains(sZoneID)) {
									zoneList.add(sZoneID);
									gridZoneList.put(gridId, zoneList);
								}
							}
						}
					}

				}
			}
		}

		campusDetails.setGridZoneDetails(gridZoneList);
		campusDetailsRepository.save(campusDetails);

	}

}
