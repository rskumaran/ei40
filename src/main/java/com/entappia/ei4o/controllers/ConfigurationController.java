package com.entappia.ei4o.controllers;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.concurrent.ExecutionException;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.entappia.ei4o.constants.AppConstants;
import com.entappia.ei4o.dbmodels.Configuration;
import com.entappia.ei4o.dbmodels.Material;
import com.entappia.ei4o.repository.ConfigurationRepository;
import com.entappia.ei4o.services.LogsServices;
import com.entappia.ei4o.utils.BaseController;
import com.entappia.ei4o.utils.Utils;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
@RestController
public class ConfigurationController extends BaseController{
	
	@Autowired
	ConfigurationRepository configurationRepository;
	@Autowired
	private LogsServices logsServices;
	Gson gson = new Gson();
	Type mapType = new TypeToken<Configuration>() {
	}.getType();
	@RequestMapping(path = { "configuration", "configuration/{orgName}" }, method = RequestMethod.GET)
	public ResponseEntity<?> getConfiguration(HttpServletRequest request, HttpSession httpSession,
			@PathVariable Optional<String> orgName) throws InterruptedException, ExecutionException {
		Map<String, Object> returnmap = new HashMap<>();
		HashMap<String, String> reuestMap = new HashMap<>();
		try {
			  
			ResponseEntity<?> responseEntity = checkSessionKey(httpSession, Utils.getCookie(request, "JSESSIONID"));
			if (responseEntity != null) {
				
				logsServices.addLog(AppConstants.error, AppConstants.configuration, "getConfiguration",
						AppConstants.sessiontimeout, null);
				return responseEntity; 
			}
			
			JSONObject jsonObject = new JSONObject();
			if (orgName.isPresent()) {
				jsonObject.put("orgName", orgName.get());
			} else {
				jsonObject.put("orgName", "all");
			}

			reuestMap.put("params", jsonObject.toString()); 
			logsServices.addLog(AppConstants.event, AppConstants.configuration, "getConfiguration",
					"Configuration get API Details", reuestMap);
			
			List<Configuration> configurationnDetails = new ArrayList<>();

			if (orgName.isPresent()) {
				Configuration configuration = configurationRepository.findByOrgName(orgName.get());
				if (configuration == null) {
					returnmap.put("status", "error");
					returnmap.put("message", "Configuration name does not exist");
					logsServices.addLog(AppConstants.error, AppConstants.configuration, "getConfiguration",
							"Configuration name does not exist", null);

					return new ResponseEntity<>(returnmap, HttpStatus.OK);
				}

				configurationnDetails.add(configuration);
			} else {
				configurationnDetails = configurationRepository.findAll();
			}
			logsServices.addLog(AppConstants.success, AppConstants.configuration, "getConfiguration",
					"Configuration details", null);

			returnmap.put("status", "success");
			returnmap.put("data", configurationnDetails);

			return new ResponseEntity<>(returnmap, HttpStatus.OK);
		} catch (Exception e) {
			returnmap.put("status", "error");
			returnmap.put("message", AppConstants.errormsg);
			logsServices.addLog(AppConstants.error, AppConstants.configuration, "getConfiguration",
					AppConstants.errormsg, null);

			return new ResponseEntity<>(returnmap, HttpStatus.OK);
		}

	}

	@PostMapping(value = "/configuration")
	public ResponseEntity<?> AddConfiguration(HttpServletRequest request, HttpSession httpSession,

			@RequestBody Configuration configuration) throws InterruptedException, ExecutionException {
		Map<String, String> returnmap = new HashMap<>(); 
		try {
			
			ResponseEntity<?> responseEntity = checkSessionKey(httpSession, Utils.getCookie(request, "JSESSIONID"));
			if (responseEntity != null) {
				
				logsServices.addLog(AppConstants.error, AppConstants.configuration, "Addconfiguration",
						AppConstants.sessiontimeout, null);
				return responseEntity; 
			}
			
			Map<String, String> requestLogMap = new HashMap<>(); 
			requestLogMap.put("params", gson.toJson(configuration, mapType));
			 
			logsServices.addLog(AppConstants.event, AppConstants.configuration, "Addconfiguration",
					"Configuration Add API Details", requestLogMap);
			
			if (Utils.isEmptyString(configuration.getOrgName())
					|| configuration.getLogAge()==0|| configuration.getTransactionDataAge()==0
					|| Utils.isEmptyString(configuration.getStaffTagColor())|| Utils.isEmptyString(configuration.getVisitorTagColor())|| Utils.isEmptyString(configuration.getToolTagColor())
					|| Utils.isEmptyString(configuration.getFixtureTagColor())|| Utils.isEmptyString(configuration.getPartTagColor())|| Utils.isEmptyString(configuration.getMaterialTagColor())
					|| Utils.isEmptyString(configuration.getRmaTagColor()) 
					|| Utils.isEmptyString(configuration.getVendorTagColor())
					|| Utils.isEmptyString(configuration.getContractorTagColor())
					|| Utils.isEmptyString(configuration.getVehicleTagColor())
					|| Utils.isEmptyString(configuration.getInventoryTagColor())
					|| Utils.isEmptyString(configuration.getLSLMTagColor())
					|| Utils.isEmptyString(configuration.getSuppliesTagColor())
					|| Utils.isEmptyString(configuration.getWareHouseTagColor())
					|| configuration.getOperationStartTime()==null|| configuration.getOperationEndTime()==null) {

				returnmap.put("status", "error");
				returnmap.put("message", AppConstants.fieldsmissing);

				logsServices.addLog(AppConstants.error, AppConstants.configuration, "Addconfiguration",
						AppConstants.fieldsmissing, null);
				return new ResponseEntity<>(returnmap, HttpStatus.OK);

			}

			Configuration configuration1 = configurationRepository
					.findByOrgName(configuration.getOrgName());

			if (configuration1 != null && configuration1.isActive()) {
				returnmap.put("status", "error");
				returnmap.put("message", "configuration name already exist");

				logsServices.addLog(AppConstants.error, AppConstants.configuration, "Addconfiguration",
						"Configuration Org Name already exist", null);
				return new ResponseEntity<>(returnmap, HttpStatus.OK);
			}

			configuration.setCreatedDate(new Date());
			configuration.setModifiedDate(new Date());
			configuration.setActive(true);
			configurationRepository.save(configuration);

			returnmap.put("status", "success");
			returnmap.put("message", configuration.getOrgName() + " added");

			logsServices.addLog(AppConstants.event, AppConstants.configuration, "AddConfiguration",
					"Configuration -" + configuration.getOrgName() + " added successfully, ", null);

			return new ResponseEntity<>(returnmap, HttpStatus.OK);
		} catch (Exception e) {
			returnmap.put("status", "error");
			returnmap.put("message", AppConstants.errormsg);

			logsServices.addLog(AppConstants.error, AppConstants.configuration, "Addconfiguration",
					AppConstants.errormsg, null);
			return new ResponseEntity<>(returnmap, HttpStatus.OK);
		}
	}

	@PutMapping(value = "/configuration")
	public ResponseEntity<?> UpdateConfiguration(HttpServletRequest request, HttpSession httpSession,

			@RequestBody Configuration configuration) throws InterruptedException, ExecutionException {
		Map<String, String> returnmap = new HashMap<>(); 
		try {
			 
			ResponseEntity<?> responseEntity = checkSessionKey(httpSession, Utils.getCookie(request, "JSESSIONID"));
			if (responseEntity != null) {
				
				logsServices.addLog(AppConstants.error, AppConstants.configuration, "Updateconfiguration",
						AppConstants.sessiontimeout, null);
				return responseEntity; 
			}
			
			Map<String, String> requestLogMap = new HashMap<>(); 
			requestLogMap.put("params", gson.toJson(configuration, mapType));
			 
			logsServices.addLog(AppConstants.event, AppConstants.configuration, "Updateconfiguration",
					"Configuration Update API Details", requestLogMap);

			if (Utils.isEmptyString(configuration.getOrgName())
					|| configuration.getLogAge()==0|| configuration.getTransactionDataAge()==0
					|| Utils.isEmptyString(configuration.getStaffTagColor())|| Utils.isEmptyString(configuration.getVisitorTagColor())|| Utils.isEmptyString(configuration.getToolTagColor())
					|| Utils.isEmptyString(configuration.getFixtureTagColor())|| Utils.isEmptyString(configuration.getPartTagColor())|| Utils.isEmptyString(configuration.getMaterialTagColor())
					|| Utils.isEmptyString(configuration.getRmaTagColor())
					|| Utils.isEmptyString(configuration.getVendorTagColor())
					|| Utils.isEmptyString(configuration.getContractorTagColor())
					|| Utils.isEmptyString(configuration.getVehicleTagColor())
					|| Utils.isEmptyString(configuration.getInventoryTagColor())
					|| Utils.isEmptyString(configuration.getLSLMTagColor())
					|| Utils.isEmptyString(configuration.getSuppliesTagColor())
					|| Utils.isEmptyString(configuration.getWareHouseTagColor())
					|| configuration.getOperationStartTime()==null|| configuration.getOperationEndTime()==null) {


				returnmap.put("status", "error");
				returnmap.put("message", AppConstants.fieldsmissing);

				logsServices.addLog(AppConstants.error, AppConstants.configuration, "Updateconfiguration",
						AppConstants.fieldsmissing, null);
				return new ResponseEntity<>(returnmap, HttpStatus.OK);

			}
			
			Configuration configuration1 = configurationRepository
					.findByOrgName(configuration.getOrgName());

			if (configuration1 == null) {
				returnmap.put("status", "error");
				returnmap.put("message", "configuration Org Name not exist");

				logsServices.addLog(AppConstants.error, AppConstants.configuration, "Updateconfiguration",
						"configuration Org Name not exist", null);
				return new ResponseEntity<>(returnmap, HttpStatus.OK);
			}else 	if (!configuration1.isActive()) {
				returnmap.put("status", "error");
				returnmap.put("message", "configuration Org Name is not active");

				logsServices.addLog(AppConstants.error, AppConstants.configuration, "Updateconfiguration",
						"configuration Org Name is not active", null);
				return new ResponseEntity<>(returnmap, HttpStatus.OK);
			}
			
            configuration.setTimeZone(configuration1.getTimeZone());
			configuration.setCreatedDate(configuration1.getCreatedDate());
			configuration.setModifiedDate(new Date());
			configuration.setActive(true);
			
			configurationRepository.save(configuration);

			returnmap.put("status", "success");
			returnmap.put("message", configuration1.getOrgName() + " updated");

			logsServices.addLog(AppConstants.event, AppConstants.configuration, "Updateconfiguration",
					"Configuration -" + configuration1.getOrgName() + " Update successfully, ", null);

			return new ResponseEntity<>(returnmap, HttpStatus.OK);
		} catch (Exception e) {
			returnmap.put("status", "error");
			returnmap.put("message", AppConstants.errormsg);

			logsServices.addLog(AppConstants.error, AppConstants.configuration, "Updateconfiguration",
					AppConstants.errormsg, null);
			return new ResponseEntity<>(returnmap, HttpStatus.OK);
		}
	}

	@DeleteMapping(value = "/configuration")
	public ResponseEntity<?> DeleteConfiguration(HttpServletRequest request, HttpSession httpSession,
			@RequestBody HashMap<String, Object> requestMap) throws InterruptedException, ExecutionException {
		Map<String, String> returnmap = new HashMap<>(); 
		try {
			 
			ResponseEntity<?> responseEntity = checkSessionKey(httpSession, Utils.getCookie(request, "JSESSIONID"));
			if (responseEntity != null) {
				
				logsServices.addLog(AppConstants.error, AppConstants.configuration, "Deleteconfiguration",
						AppConstants.sessiontimeout, null);
				return responseEntity; 
			}
			
			Map<String, String> requestLogMap = new HashMap<>(); 
			requestLogMap.put("params", new JSONObject(requestMap).toString());
			 
			logsServices.addLog(AppConstants.event, AppConstants.configuration, "Deleteconfiguration",
					"Configuration delete API Details", requestLogMap);
			

			if (!requestMap.containsKey("orgName")) {

				returnmap.put("status", "error");
				returnmap.put("message", AppConstants.fieldsmissing);

				logsServices.addLog(AppConstants.error, AppConstants.configuration, "Deleteconfiguration",
						AppConstants.fieldsmissing, null);
				return new ResponseEntity<>(returnmap, HttpStatus.OK);

			}  

			Configuration configuration = configurationRepository.findByOrgName(requestMap.get("orgName").toString());

			if (configuration == null) {
				returnmap.put("status", "error");
				returnmap.put("message", "configuration Org Name not exist");

				logsServices.addLog(AppConstants.error, AppConstants.configuration, "Deleteconfiguration",
						"configuration Name not exist", null);
				return new ResponseEntity<>(returnmap, HttpStatus.OK);
			}
			configuration.setModifiedDate(new Date());
			configuration.setActive(false);
			configurationRepository.save(configuration);

			returnmap.put("status", "success");
			returnmap.put("message", configuration.getOrgName() + " Deleted");

			logsServices.addLog(AppConstants.event, AppConstants.configuration, "Deleteconfiguration",
					"Configuration -" + configuration.getOrgName() + " Deleted successfully, ", null);

			return new ResponseEntity<>(returnmap, HttpStatus.OK);
		} catch (Exception e) {
			returnmap.put("status", "error");
			returnmap.put("message", AppConstants.errormsg);

			logsServices.addLog(AppConstants.error, AppConstants.configuration, "Deleteconfiguration",
					AppConstants.errormsg, null);
			return new ResponseEntity<>(returnmap, HttpStatus.OK);
		}
	}
}
