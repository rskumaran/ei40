package com.entappia.ei4o.controllers;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.concurrent.ExecutionException;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.entappia.ei4o.constants.AppConstants;
import com.entappia.ei4o.dbmodels.AssetVehicle;
import com.entappia.ei4o.dbmodels.CampusDetails;
import com.entappia.ei4o.dbmodels.OutlinesDetails;
import com.entappia.ei4o.dbmodels.PathSegments;
import com.entappia.ei4o.repository.CampusDetailsRepository;
import com.entappia.ei4o.repository.PathSegmentRepositiory;
import com.entappia.ei4o.services.LogsServices;
import com.entappia.ei4o.utils.BaseController;
import com.entappia.ei4o.utils.Utils;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

@RestController
public class PathSegmentController extends BaseController {

	@Autowired
	private LogsServices logsServices;

	@Autowired
	private CampusDetailsRepository campusDetailsRepository;

	@Autowired
	private PathSegmentRepositiory pathSegmentRepositiory;
	Gson gson = new Gson();
	Type mapType = new TypeToken<PathSegments>() {
	}.getType();

	@RequestMapping(path = { "pathSegments", "pathSegments/{campusId}" }, method = RequestMethod.GET)
	public ResponseEntity<?> getPathSegment(HttpServletRequest request, HttpSession httpSession,
			@PathVariable Optional<Long> campusId) throws InterruptedException, ExecutionException {
		Map<String, Object> returnmap = new HashMap<>();
		try {

			ResponseEntity<?> responseEntity = checkSessionKey(httpSession, Utils.getCookie(request, "JSESSIONID"));
			if (responseEntity != null) { 
				logsServices.addLog(AppConstants.error, AppConstants.PATHSEGMENTS, "getPathSegment",
						AppConstants.sessiontimeout, null);
				return responseEntity; 
			}
			 
			JSONObject jsonObject = new JSONObject();
			if (campusId.isPresent()) {
				jsonObject.put("campusId", campusId.get());
			} else {
				jsonObject.put("campusId", "all");
			}
			Map<String, String> reuestMap = new HashMap<>();
			reuestMap.put("params", jsonObject.toString()); 

			logsServices.addLog(AppConstants.event, AppConstants.PATHSEGMENTS, "getPathSegment",
					"Path Segment Details Get API Details", reuestMap);

			List<PathSegments> pathSegmentsList = new ArrayList<>();

			if (campusId.isPresent()) {
				pathSegmentsList = pathSegmentRepositiory.findByCampusId(campusId.get());
				if (pathSegmentsList == null) {
					returnmap.put("status", "error");
					returnmap.put("message", "Path Segment Details does not exist");
					logsServices.addLog(AppConstants.error, AppConstants.PATHSEGMENTS, "getPathSegment",
							"Path Segment Details does not exist", null);

					return new ResponseEntity<>(returnmap, HttpStatus.OK);
				}

			} else {
				pathSegmentsList = pathSegmentRepositiory.findAllActivePathSegments();
				if (pathSegmentsList == null) {
					returnmap.put("status", "error");
					returnmap.put("message", "Path Segment Details does not exist");
					logsServices.addLog(AppConstants.error, AppConstants.PATHSEGMENTS, "getPathSegment",
							"Path Segment Details does not exist", null);

					return new ResponseEntity<>(returnmap, HttpStatus.OK);
				}
			}
			logsServices.addLog(AppConstants.success, AppConstants.PATHSEGMENTS, "getPathSegment",
					"Work Station details", null);

			returnmap.put("status", "success");
			returnmap.put("data", pathSegmentsList);

			return new ResponseEntity<>(returnmap, HttpStatus.OK);
		} catch (Exception e) {
			returnmap.put("status", "error");
			returnmap.put("message", AppConstants.errormsg);
			logsServices.addLog(AppConstants.error, AppConstants.PATHSEGMENTS, "getPathSegment", AppConstants.errormsg,
					null);

			return new ResponseEntity<>(returnmap, HttpStatus.OK);
		}

	}

	@PostMapping(value = "/pathSegments/{campusId}")
	public ResponseEntity<?> addPathSegment(HttpServletRequest request, HttpSession httpSession, 
			@PathVariable Optional<Long> campusId, @RequestBody PathSegments pathSegments)
			throws InterruptedException, ExecutionException {
		Map<String, String> returnmap = new HashMap<>();

		try {
			
			ResponseEntity<?> responseEntity = checkSessionKey(httpSession, Utils.getCookie(request, "JSESSIONID"), "CTPE");
			if (responseEntity != null) { 
				logsServices.addLog(AppConstants.error, AppConstants.PATHSEGMENTS, "addPathSegment",
						AppConstants.sessiontimeout, null);
				return responseEntity; 
			}
			 
			 
			Map<String, String> reuestMap = new HashMap<>();
			reuestMap.put("params", gson.toJson(pathSegments, mapType));

			logsServices.addLog(AppConstants.event, AppConstants.PATHSEGMENTS, "addPathSegment",
					"Path Segment Details Add API Details", reuestMap);

			if (!campusId.isPresent() || Utils.isEmptyString(pathSegments.getName())
					|| Utils.isEmptyString(pathSegments.getGeometryType())
					|| Utils.isEmptyString(pathSegments.getWallType())
					|| Utils.isEmptyString(pathSegments.getTextType()) || pathSegments.getTextAngle() < 0
					|| Utils.isEmptyString(pathSegments.getColorCode())
							| Utils.isEmptyString(pathSegments.getGeometryType())
					|| pathSegments.getCoordinates() == null) {

				returnmap.put("status", "error");
				returnmap.put("message", AppConstants.fieldsmissing);

				logsServices.addLog(AppConstants.error, AppConstants.PATHSEGMENTS, "addPathSegment",
						AppConstants.fieldsmissing, null);
				return new ResponseEntity<>(returnmap, HttpStatus.OK);

			}

			CampusDetails campusDetails = campusDetailsRepository.findByCampusId(campusId.get());
			if (campusDetails == null) {
				returnmap.put("status", "error");
				returnmap.put("message", "Campus id not found");

				logsServices.addLog(AppConstants.error, AppConstants.PATHSEGMENTS, "addPathSegment",
						"Campus id not found", null);
				return new ResponseEntity<>(returnmap, HttpStatus.OK);
			} else if (!campusDetails.isActive()) {
				returnmap.put("status", "error");
				returnmap.put("message", "Campus is not active");

				logsServices.addLog(AppConstants.error, AppConstants.PATHSEGMENTS, "addPathSegment",
						"Campus is not active", null);
				return new ResponseEntity<>(returnmap, HttpStatus.OK);
			}

			pathSegments.setCampusDetails(campusDetails);
			pathSegments.setActive(true);
			pathSegments.setCreatedDate(new Date());
			pathSegments.setModifiedDate(new Date());
			pathSegmentRepositiory.save(pathSegments);

			returnmap.put("status", "success");
			returnmap.put("message", pathSegments.getName() + " added successfully");

			logsServices.addLog(AppConstants.event, AppConstants.PATHSEGMENTS, "addPathSegment",
					"Path Segment -" + pathSegments.getName() + " added successfully, ", null);

			return new ResponseEntity<>(returnmap, HttpStatus.OK);
		} catch (Exception e) {
			returnmap.put("status", "error");
			returnmap.put("message", AppConstants.errormsg);

			logsServices.addLog(AppConstants.error, AppConstants.PATHSEGMENTS, "addPathSegment", AppConstants.errormsg,
					null);
			return new ResponseEntity<>(returnmap, HttpStatus.OK);
		}
	}

	@PutMapping(value = "/pathSegments/{campusId}")
	public ResponseEntity<?> updatePathSegment(HttpServletRequest request, HttpSession httpSession,

			@PathVariable Optional<Long> campusId, @RequestBody PathSegments pathSegments)
			throws InterruptedException, ExecutionException {
		Map<String, String> returnmap = new HashMap<>();

		try {
			
			ResponseEntity<?> responseEntity = checkSessionKey(httpSession, Utils.getCookie(request, "JSESSIONID"), "CTPE");
			if (responseEntity != null) { 
				logsServices.addLog(AppConstants.error, AppConstants.PATHSEGMENTS, "updatePathSegment",
						AppConstants.sessiontimeout, null);
				return responseEntity; 
			}
			 
			 
			Map<String, String> reuestMap = new HashMap<>();
			reuestMap.put("params", gson.toJson(pathSegments, mapType));

			logsServices.addLog(AppConstants.event, AppConstants.PATHSEGMENTS, "updatePathSegment",
					"Path Segment Details Update API Details", reuestMap);


			if (!campusId.isPresent() || pathSegments.getId() <= 0 || Utils.isEmptyString(pathSegments.getName()) 
					|| Utils.isEmptyString(pathSegments.getWallType())
					|| Utils.isEmptyString(pathSegments.getTextType()) || pathSegments.getTextAngle() < 0
					|| Utils.isEmptyString(pathSegments.getColorCode())
							| Utils.isEmptyString(pathSegments.getGeometryType())
					|| pathSegments.getCoordinates() == null) {

				returnmap.put("status", "error");
				returnmap.put("message", AppConstants.fieldsmissing);

				logsServices.addLog(AppConstants.error, AppConstants.PATHSEGMENTS, "updatePathSegment",
						AppConstants.fieldsmissing, null);
				return new ResponseEntity<>(returnmap, HttpStatus.OK);

			}

			PathSegments dbPathSegments = pathSegmentRepositiory.findById(pathSegments.getId());
			
			
			if (dbPathSegments == null) {
				returnmap.put("status", "error");
				returnmap.put("message", "Path Segment id not found");

				logsServices.addLog(AppConstants.error, AppConstants.PATHSEGMENTS, "updatePathSegment",
						"Path Segment id not found", null);
				return new ResponseEntity<>(returnmap, HttpStatus.OK);
			} else if (!dbPathSegments.isActive()) {
				returnmap.put("status", "error");
				returnmap.put("message", "Path Segment is not active");

				logsServices.addLog(AppConstants.error, AppConstants.PATHSEGMENTS, "updatePathSegment",
						"Path Segment is not active", null);
				return new ResponseEntity<>(returnmap, HttpStatus.OK);
			}
			
			CampusDetails campusDetails = campusDetailsRepository.findByCampusId(campusId.get());
			if (campusDetails == null) {
				returnmap.put("status", "error");
				returnmap.put("message", "Campus id not found");

				logsServices.addLog(AppConstants.error, AppConstants.PATHSEGMENTS, "updatePathSegment",
						"Campus id not found", null);
				return new ResponseEntity<>(returnmap, HttpStatus.OK);
			} else if (!campusDetails.isActive()) {
				returnmap.put("status", "error");
				returnmap.put("message", "Campus is not active");

				logsServices.addLog(AppConstants.error, AppConstants.PATHSEGMENTS, "updatePathSegment",
						"Campus is not active", null);
				return new ResponseEntity<>(returnmap, HttpStatus.OK);
			}

			dbPathSegments.setName(pathSegments.getName());
			dbPathSegments.setWallType(pathSegments.getWallType());
			dbPathSegments.setTextType(pathSegments.getTextType());
			dbPathSegments.setTextAngle(pathSegments.getTextAngle());
			dbPathSegments.setColorCode(pathSegments.getColorCode());
			dbPathSegments.setGeometryType(pathSegments.getGeometryType());
			dbPathSegments.setCoordinates(pathSegments.getCoordinates());
			dbPathSegments.setCampusDetails(campusDetails);
			dbPathSegments.setModifiedDate(new Date());
			pathSegmentRepositiory.save(dbPathSegments);

			returnmap.put("status", "success");
			returnmap.put("message", dbPathSegments.getName() + " added successfully");

			logsServices.addLog(AppConstants.event, AppConstants.PATHSEGMENTS, "updatePathSegment",
					"Path Segment -" + dbPathSegments.getName() + " added successfully, ", null);

			return new ResponseEntity<>(returnmap, HttpStatus.OK);
		} catch (Exception e) {
			returnmap.put("status", "error");
			returnmap.put("message", AppConstants.errormsg);

			logsServices.addLog(AppConstants.error, AppConstants.PATHSEGMENTS, "updatePathSegment", AppConstants.errormsg,
					null);
			return new ResponseEntity<>(returnmap, HttpStatus.OK);
		}
	}
	
	@PutMapping(value = "/pathSegments")
	public ResponseEntity<?> deletePathSegment(HttpServletRequest request, HttpSession httpSession,
			@RequestBody PathSegments pathSegments)
			throws InterruptedException, ExecutionException {
		Map<String, String> returnmap = new HashMap<>();

		try {

			ResponseEntity<?> responseEntity = checkSessionKey(httpSession, Utils.getCookie(request, "JSESSIONID"), "CTPE");
			if (responseEntity != null) { 
				logsServices.addLog(AppConstants.error, AppConstants.PATHSEGMENTS, "deletePathSegment",
						AppConstants.sessiontimeout, null);
				return responseEntity; 
			}
			 
			 
			Map<String, String> reuestMap = new HashMap<>();
			reuestMap.put("params", gson.toJson(pathSegments, mapType));

			logsServices.addLog(AppConstants.event, AppConstants.PATHSEGMENTS, "deletePathSegment",
					"Path Segment Details Delete API Details", reuestMap);
			
			if (pathSegments.getId() <= 0) {

				returnmap.put("status", "error");
				returnmap.put("message", AppConstants.fieldsmissing);

				logsServices.addLog(AppConstants.error, AppConstants.PATHSEGMENTS, "deletePathSegment",
						AppConstants.fieldsmissing, null);
				return new ResponseEntity<>(returnmap, HttpStatus.OK);

			}

			PathSegments dbPathSegments = pathSegmentRepositiory.findById(pathSegments.getId());
			
			
			if (dbPathSegments == null) {
				returnmap.put("status", "error");
				returnmap.put("message", "Path Segment id not found");

				logsServices.addLog(AppConstants.error, AppConstants.PATHSEGMENTS, "deletePathSegment",
						"Path Segment id not found", null);
				return new ResponseEntity<>(returnmap, HttpStatus.OK);
			} else if (!dbPathSegments.isActive()) {
				returnmap.put("status", "error");
				returnmap.put("message", "Path Segment is not active");

				logsServices.addLog(AppConstants.error, AppConstants.PATHSEGMENTS, "deletePathSegment",
						"Path Segment is not active", null);
				return new ResponseEntity<>(returnmap, HttpStatus.OK);
			}
			
			dbPathSegments.setActive(false);
			dbPathSegments.setModifiedDate(new Date());
			pathSegmentRepositiory.save(dbPathSegments);

			returnmap.put("status", "success");
			returnmap.put("message", dbPathSegments.getName() + " status added successfully");

			logsServices.addLog(AppConstants.event, AppConstants.PATHSEGMENTS, "deletePathSegment",
					"Path Segment -" + dbPathSegments.getName() + " status added successfully, ", null);

			return new ResponseEntity<>(returnmap, HttpStatus.OK);
		} catch (Exception e) {
			returnmap.put("status", "error");
			returnmap.put("message", AppConstants.errormsg);

			logsServices.addLog(AppConstants.error, AppConstants.PATHSEGMENTS, "deletePathSegment", AppConstants.errormsg,
					null);
			return new ResponseEntity<>(returnmap, HttpStatus.OK);
		}
	}

}
