package com.entappia.ei4o.controllers;

import java.lang.reflect.Type;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.entappia.ei4o.constants.AppConstants;
import com.entappia.ei4o.services.LogsServices;
import com.entappia.ei4o.services.QuuppaAPIService;
import com.entappia.ei4o.utils.BaseController;
import com.entappia.ei4o.utils.Utils;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

@RestController
public class LocatorController extends BaseController {

	@Autowired
	private LogsServices logsServices;

	Gson gson = new Gson();

	@Autowired
	private QuuppaAPIService quuppaAPIService;

	@RequestMapping(path = { "locatorDetails", "locatorDetails/{status}" }, method = RequestMethod.GET)
	public ResponseEntity<?> getLocatorDetails(HttpServletRequest request, HttpSession httpSession,
			@PathVariable Optional<String> status) throws InterruptedException, ExecutionException {
		Map<String, Object> returnmap = new HashMap<>();
		try {
			Map<String, String> reuestMap = new HashMap<>();
			ResponseEntity<?> responseEntity = checkSessionKey(httpSession, Utils.getCookie(request, "JSESSIONID"));
			if (responseEntity != null) { 
				logsServices.addLog(AppConstants.error, AppConstants.LOCATOR, "getLocatorDetails",
						AppConstants.sessiontimeout, null);
				return responseEntity; 
			}
			
			
			JSONObject jsonObject = new JSONObject();
			if (status.isPresent()) {
				jsonObject.put("status", status.get());
			} else {
				jsonObject.put("status", "all");
			}

			reuestMap.put("params", jsonObject.toString()); 

			logsServices.addLog(AppConstants.event, AppConstants.LOCATOR, "getLocatorDetails",
					"Locator Details Get API Details", reuestMap);

			JSONObject jsonObj = null;
			CompletableFuture<JSONObject> locatorInfoCompletableFuture = quuppaAPIService.getLocatorInfo();
			CompletableFuture.allOf(locatorInfoCompletableFuture).join();
			if (locatorInfoCompletableFuture.isDone()) {

				jsonObj = locatorInfoCompletableFuture.get();

			}

			Type mapType = new TypeToken<List<HashMap<String, String>>>() {
			}.getType();

			if (jsonObj != null && jsonObj.getString("status").equalsIgnoreCase("success")) {

				List<HashMap<String, String>> locatorsList = gson.fromJson(jsonObj.getJSONArray("locators").toString(),
						mapType);
				if (!status.isPresent()) {
					returnmap.put("status", "success");
					returnmap.put("locator", locatorsList);
				} else {
					CompletableFuture<HashMap<String, String>> locatorStatusInfoCompletableFuture = quuppaAPIService.getLocatorStatusInfo();
					CompletableFuture.allOf(locatorStatusInfoCompletableFuture).join();
					if (locatorStatusInfoCompletableFuture.isDone()) {

						HashMap<String, String> locatorStatusInfo = locatorStatusInfoCompletableFuture.get();
						if(locatorStatusInfo!=null) {
							for(int i=0; i<locatorsList.size(); i++) { 
								HashMap<String, String> map = locatorsList.get(i);
								map.put("status", locatorStatusInfo.getOrDefault(map.get("id"), ""));
							}
						}
						returnmap.put("status", "success");
						returnmap.put("locator", locatorsList);

					}
				}
			} else {
				returnmap.put("status", "error");
				returnmap.put("message", jsonObj.getString("message"));
			}


			logsServices.addLog(AppConstants.success, AppConstants.LOCATOR, "getLocatorDetails", "Locator details",
					null);
			return new ResponseEntity<>(returnmap, HttpStatus.OK);
		} catch (Exception e) {
			returnmap.put("status", "error");
			returnmap.put("message", AppConstants.errormsg);
			logsServices.addLog(AppConstants.error, AppConstants.LOCATOR, "getLocatorDetails", AppConstants.errormsg,
					null);

			return new ResponseEntity<>(returnmap, HttpStatus.OK);
		}

	}

}
