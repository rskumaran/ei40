package com.entappia.ei4o.controllers;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.concurrent.ExecutionException;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.entappia.ei4o.constants.AppConstants;
import com.entappia.ei4o.dbmodels.WorkStation;
import com.entappia.ei4o.dbmodels.ZoneDetails;
import com.entappia.ei4o.repository.WorkStationRepository;
import com.entappia.ei4o.repository.ZoneRepository;
import com.entappia.ei4o.services.LogsServices;
import com.entappia.ei4o.utils.BaseController;
import com.entappia.ei4o.utils.Utils;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

@RestController
public class WorkStationController extends BaseController {

	@Autowired
	private LogsServices logsServices;

	@Autowired
	private WorkStationRepository workStationRepository;

	@Autowired
	private ZoneRepository zoneRepository;

	@RequestMapping(path = { "workStation", "workStation/{stationId}" }, method = RequestMethod.GET)
	public ResponseEntity<?> getWorkStation(HttpServletRequest request, HttpSession httpSession,
			@PathVariable Optional<String> stationId) throws InterruptedException, ExecutionException {
		Map<String, Object> returnmap = new HashMap<>();
 		try {
			//, "WWVA"
			ResponseEntity<?> responseEntity = checkSessionKey(httpSession, Utils.getCookie(request, "JSESSIONID"));
			if (responseEntity != null) { 
				logsServices.addLog(AppConstants.error, AppConstants.WORK_STATION, "getWorkStation",
						AppConstants.sessiontimeout, null);
				return responseEntity; 
			}
			

			JSONObject jsonObject = new JSONObject();
			if (stationId.isPresent()) {
				jsonObject.put("stationId", stationId.get());
			} else {
				jsonObject.put("stationId", "all");
			}
			Map<String, String> reuestMap = new HashMap<>();
			reuestMap.put("params", jsonObject.toString()); 
			
			logsServices.addLog(AppConstants.event, AppConstants.WORK_STATION, "getWorkStation",
					"Get WorkStation api call", reuestMap);
			
			if (stationId.isPresent()) {
				reuestMap.put("stationId id", stationId.get());
			} else {
				reuestMap.put("params", "getallWorkStations");
			}

			 

			List<WorkStation> workStations = new ArrayList<>();

			if (stationId.isPresent()) {
				WorkStation workStation = workStationRepository.findByStationId(stationId.get());
				if (workStation == null) {
					returnmap.put("status", "error");
					returnmap.put("message", "Work Station id does not exist");
					logsServices.addLog(AppConstants.error, AppConstants.WORK_STATION, "getWorkStation",
							"Work Station id does not exist", null);

					return new ResponseEntity<>(returnmap, HttpStatus.OK);
				}

				workStations.add(workStation);
			} else {
				workStations = workStationRepository.findAll();
			}
			logsServices.addLog(AppConstants.success, AppConstants.WORK_STATION, "getWorkStation",
					"Work Station details", null);

			returnmap.put("status", "success");
			returnmap.put("data", workStations);

			return new ResponseEntity<>(returnmap, HttpStatus.OK);
		} catch (Exception e) {
			returnmap.put("status", "error");
			returnmap.put("message", AppConstants.errormsg);
			logsServices.addLog(AppConstants.error, AppConstants.WORK_STATION, "getWorkStation", AppConstants.errormsg,
					null);

			return new ResponseEntity<>(returnmap, HttpStatus.OK);
		}

	}

	@PostMapping(value = "/workStation")
	public ResponseEntity<?> addWorkStation(HttpServletRequest request, HttpSession httpSession,

			@RequestBody HashMap<String, Object> requestMap) throws InterruptedException, ExecutionException {
		Map<String, String> returnmap = new HashMap<>();

		try {

			ResponseEntity<?> responseEntity = checkSessionKey(httpSession, Utils.getCookie(request, "JSESSIONID"), "WWAD");
			if (responseEntity != null) {
				logsServices.addLog(AppConstants.error, AppConstants.WORK_STATION, "addWorkStation",
						AppConstants.sessiontimeout, null); 
				return responseEntity; 
			}
			
			Map<String, String> reuestMap = new HashMap<>();
			reuestMap.put("params", new JSONObject(requestMap).toString()); 
			

			logsServices.addLog(AppConstants.event, AppConstants.WORK_STATION, "addWorkStation",
					"Add WorkStation api call", reuestMap);
			
			if (Utils.isEmptyString(requestMap.getOrDefault("stationId", "").toString())
					|| Utils.isEmptyString(requestMap.getOrDefault("stationType", "").toString())
					|| Utils.isEmptyString(requestMap.getOrDefault("inbound", "").toString())
					|| Utils.isEmptyString(requestMap.getOrDefault("outbound", "").toString())
					|| Utils.isEmptyString(requestMap.getOrDefault("workArea", "").toString())
					|| Utils.isEmptyString(requestMap.getOrDefault("storage", "").toString())
					|| Utils.isEmptyString(requestMap.getOrDefault("operatorList", "").toString())
					|| Utils.isEmptyString(requestMap.getOrDefault("inLimit", "").toString())
					|| Utils.isEmptyString(requestMap.getOrDefault("outLimit", "").toString())
					|| Utils.isEmptyString(requestMap.getOrDefault("zone", "").toString())) {

				returnmap.put("status", "error");
				returnmap.put("message", AppConstants.fieldsmissing);

				logsServices.addLog(AppConstants.error, AppConstants.WORK_STATION, "addWorkStation",
						AppConstants.fieldsmissing, null);
				return new ResponseEntity<>(returnmap, HttpStatus.OK);

			}

			WorkStation workStation = workStationRepository
					.findByStationId(requestMap.getOrDefault("stationId", "").toString());
			if (workStation != null && workStation.isActive()) {
				returnmap.put("status", "error");
				returnmap.put("message", "Work Station id already exist");

				logsServices.addLog(AppConstants.error, AppConstants.WORK_STATION, "addWorkStation",
						"Work Station id already exist", null);
				return new ResponseEntity<>(returnmap, HttpStatus.OK);
			}

			ZoneDetails inbound = zoneRepository.findById((int) requestMap.getOrDefault("inbound", ""));

			if (inbound == null) {
				returnmap.put("status", "error");
				returnmap.put("message", "Inbound zone id does not exist");
				logsServices.addLog(AppConstants.error, AppConstants.WORK_STATION, "addWorkStation",
						returnmap.get("message"), null);

				return new ResponseEntity<>(returnmap, HttpStatus.OK);
			}else if (!inbound.isActive()) {
				returnmap.put("status", "error");
				returnmap.put("message", "Inbound zone id is not active");
				logsServices.addLog(AppConstants.error, AppConstants.WORK_STATION, "addWorkStation",
						returnmap.get("message"), null);

				return new ResponseEntity<>(returnmap, HttpStatus.OK);
			}
			
			ZoneDetails outbound = zoneRepository.findById((int) requestMap.getOrDefault("outbound", ""));

			if (outbound == null) {
				returnmap.put("status", "error");
				returnmap.put("message", "Outbound zone id does not exist");
				logsServices.addLog(AppConstants.error, AppConstants.WORK_STATION, "addWorkStation",
						returnmap.get("message"), null);

				return new ResponseEntity<>(returnmap, HttpStatus.OK);
			}else if (!outbound.isActive()) {
				returnmap.put("status", "error");
				returnmap.put("message", "Outbound zone id is not active");
				logsServices.addLog(AppConstants.error, AppConstants.WORK_STATION, "addWorkStation",
						returnmap.get("message"), null);

				return new ResponseEntity<>(returnmap, HttpStatus.OK);
			}
			
			ZoneDetails workArea = zoneRepository.findById((int) requestMap.getOrDefault("workArea", ""));

			if (workArea == null) {
				returnmap.put("status", "error");
				returnmap.put("message", "WorkArea zone id does not exist");
				logsServices.addLog(AppConstants.error, AppConstants.WORK_STATION, "addWorkStation",
						returnmap.get("message"), null);

				return new ResponseEntity<>(returnmap, HttpStatus.OK);
			}else if (!workArea.isActive()) {
				returnmap.put("status", "error");
				returnmap.put("message", "WorkArea zone id is not active");
				logsServices.addLog(AppConstants.error, AppConstants.WORK_STATION, "addWorkStation",
						returnmap.get("message"), null);

				return new ResponseEntity<>(returnmap, HttpStatus.OK);
			}
			
			ZoneDetails storage = zoneRepository.findById((int) requestMap.getOrDefault("storage", ""));

			if (storage == null) {
				returnmap.put("status", "error");
				returnmap.put("message", "Storage zone id does not exist");
				logsServices.addLog(AppConstants.error, AppConstants.WORK_STATION, "addWorkStation",
						returnmap.get("message"), null);

				return new ResponseEntity<>(returnmap, HttpStatus.OK);
			}else if (!storage.isActive()) {
				returnmap.put("status", "error");
				returnmap.put("message", "Storage zone id is not active");
				logsServices.addLog(AppConstants.error, AppConstants.WORK_STATION, "addWorkStation",
						returnmap.get("message"), null);

				return new ResponseEntity<>(returnmap, HttpStatus.OK);
			}
 
			JSONArray operatorJSONArray = new JSONArray(requestMap.getOrDefault("operatorList", "").toString());

			List<String> operatorList = new ArrayList<>();

			if (operatorJSONArray != null && operatorJSONArray.length() > 0) {

				for (int i = 0; i < operatorJSONArray.length(); i++) {
					String val = operatorJSONArray.getString(i);
					operatorList.add(val);
				}
			}

			workStation = new WorkStation();
			workStation.setStationId(requestMap.getOrDefault("stationId", "").toString());
			workStation.setStationType(requestMap.getOrDefault("stationType", "").toString());
			workStation.setInbound(inbound);
			workStation.setOutbound(outbound);
			workStation.setWorkArea(workArea);
			workStation.setStorage(storage);
			workStation.setZone(requestMap.getOrDefault("zone", "").toString());

			workStation.setOperatorList(operatorList);
			workStation.setInLimit(Integer.valueOf(requestMap.getOrDefault("inLimit", "").toString()));
			workStation.setOutLimit(Integer.valueOf(requestMap.getOrDefault("outLimit", "").toString()));
			workStation.setStatus(true);
			workStation.setActive(true);
			workStation.setCreatedDate(new Date());
			workStation.setModifiedDate(new Date());

			workStationRepository.save(workStation);

			returnmap.put("status", "success");
			returnmap.put("message", workStation.getStationId() + " added successfully");

			logsServices.addLog(AppConstants.event, AppConstants.WORK_STATION, "addWorkStation",
					"Work Station ID -" + workStation.getStationId() + " added successfully, ", null);

			return new ResponseEntity<>(returnmap, HttpStatus.OK);
		} catch (Exception e) {
			returnmap.put("status", "error");
			returnmap.put("message", AppConstants.errormsg);

			logsServices.addLog(AppConstants.error, AppConstants.WORK_STATION, "addWorkStation", AppConstants.errormsg,
					null);
			return new ResponseEntity<>(returnmap, HttpStatus.OK);
		}
	}

	@PutMapping(value = "/workStation")
	public ResponseEntity<?> updateWorkStation(HttpServletRequest request, HttpSession httpSession,

			@RequestBody HashMap<String, Object> requestMap) throws InterruptedException, ExecutionException {
		Map<String, String> returnmap = new HashMap<>();

		try {


			ResponseEntity<?> responseEntity = checkSessionKey(httpSession, Utils.getCookie(request, "JSESSIONID"), "WWED");
			if (responseEntity != null) { 
				logsServices.addLog(AppConstants.error, AppConstants.WORK_STATION, "updateWorkStation",
						AppConstants.sessiontimeout, null);
				return responseEntity; 
			}
			

			Map<String, String> reuestMap = new HashMap<>();
			reuestMap.put("params", new JSONObject(requestMap).toString()); 
			
			logsServices.addLog(AppConstants.event, AppConstants.WORK_STATION, "updateWorkStation",
					"Update WorkStation api call", reuestMap);
			
			if (Utils.isEmptyString(requestMap.getOrDefault("stationId", "").toString())
					|| Utils.isEmptyString(requestMap.getOrDefault("stationType", "").toString())
					|| Utils.isEmptyString(requestMap.getOrDefault("inbound", "").toString())
					|| Utils.isEmptyString(requestMap.getOrDefault("outbound", "").toString())
					|| Utils.isEmptyString(requestMap.getOrDefault("workArea", "").toString())
					|| Utils.isEmptyString(requestMap.getOrDefault("storage", "").toString())
					|| Utils.isEmptyString(requestMap.getOrDefault("operatorList", "").toString())
					|| Utils.isEmptyString(requestMap.getOrDefault("inLimit", "").toString())
					|| Utils.isEmptyString(requestMap.getOrDefault("outLimit", "").toString())
					|| Utils.isEmptyString(requestMap.getOrDefault("zone", "").toString())) {

				returnmap.put("status", "error");
				returnmap.put("message", AppConstants.fieldsmissing);

				logsServices.addLog(AppConstants.error, AppConstants.WORK_STATION, "updateWorkStation",
						AppConstants.fieldsmissing, null);
				return new ResponseEntity<>(returnmap, HttpStatus.OK);

			}

			WorkStation workStation = workStationRepository
					.findByStationId(requestMap.getOrDefault("stationId", "").toString());
			if (workStation == null) {
				returnmap.put("status", "error");
				returnmap.put("message", "Work Station id already exist");

				logsServices.addLog(AppConstants.error, AppConstants.WORK_STATION, "updateWorkStation",
						"Work Station id already exist", null);
				return new ResponseEntity<>(returnmap, HttpStatus.OK);
			} else if (!workStation.isStatus()) {
				returnmap.put("status", "error");
				returnmap.put("message", "Work Station is not active");

				logsServices.addLog(AppConstants.error, AppConstants.WORK_STATION, "updateWorkStation",
						"Work Station is not active", null);
				return new ResponseEntity<>(returnmap, HttpStatus.OK);
			}
 
			
			ZoneDetails inbound = zoneRepository.findById((int) requestMap.getOrDefault("inbound", ""));

			if (inbound == null) {
				returnmap.put("status", "error");
				returnmap.put("message", "Inbound zone id does not exist");
				logsServices.addLog(AppConstants.error, AppConstants.WORK_STATION, "addWorkStation",
						returnmap.get("message"), null);

				return new ResponseEntity<>(returnmap, HttpStatus.OK);
			}else if (!inbound.isActive()) {
				returnmap.put("status", "error");
				returnmap.put("message", "Inbound zone id is not active");
				logsServices.addLog(AppConstants.error, AppConstants.WORK_STATION, "addWorkStation",
						returnmap.get("message"), null);

				return new ResponseEntity<>(returnmap, HttpStatus.OK);
			}
			
			ZoneDetails outbound = zoneRepository.findById((int) requestMap.getOrDefault("outbound", ""));

			if (outbound == null) {
				returnmap.put("status", "error");
				returnmap.put("message", "Outbound zone id does not exist");
				logsServices.addLog(AppConstants.error, AppConstants.WORK_STATION, "addWorkStation",
						returnmap.get("message"), null);

				return new ResponseEntity<>(returnmap, HttpStatus.OK);
			}else if (!outbound.isActive()) {
				returnmap.put("status", "error");
				returnmap.put("message", "Outbound zone id is not active");
				logsServices.addLog(AppConstants.error, AppConstants.WORK_STATION, "addWorkStation",
						returnmap.get("message"), null);

				return new ResponseEntity<>(returnmap, HttpStatus.OK);
			}
			
			ZoneDetails workArea = zoneRepository.findById((int) requestMap.getOrDefault("workArea", ""));

			if (workArea == null) {
				returnmap.put("status", "error");
				returnmap.put("message", "WorkArea zone id does not exist");
				logsServices.addLog(AppConstants.error, AppConstants.WORK_STATION, "addWorkStation",
						returnmap.get("message"), null);

				return new ResponseEntity<>(returnmap, HttpStatus.OK);
			}else if (!workArea.isActive()) {
				returnmap.put("status", "error");
				returnmap.put("message", "WorkArea zone id is not active");
				logsServices.addLog(AppConstants.error, AppConstants.WORK_STATION, "addWorkStation",
						returnmap.get("message"), null);

				return new ResponseEntity<>(returnmap, HttpStatus.OK);
			}
			
			ZoneDetails storage = zoneRepository.findById((int) requestMap.getOrDefault("storage", ""));

			if (storage == null) {
				returnmap.put("status", "error");
				returnmap.put("message", "Storage zone id does not exist");
				logsServices.addLog(AppConstants.error, AppConstants.WORK_STATION, "addWorkStation",
						returnmap.get("message"), null);

				return new ResponseEntity<>(returnmap, HttpStatus.OK);
			}else if (!storage.isActive()) {
				returnmap.put("status", "error");
				returnmap.put("message", "Storage zone id is not active");
				logsServices.addLog(AppConstants.error, AppConstants.WORK_STATION, "addWorkStation",
						returnmap.get("message"), null);

				return new ResponseEntity<>(returnmap, HttpStatus.OK);
			}

			JSONArray operatorJSONArray = new JSONArray(requestMap.getOrDefault("operatorList", "").toString());

			List<String> operatorList = new ArrayList<>();

			if (operatorJSONArray != null && operatorJSONArray.length() > 0) {

				for (int i = 0; i < operatorJSONArray.length(); i++) {
					String val = operatorJSONArray.getString(i);
					operatorList.add(val);
				}
			}

			workStation.setStationId(requestMap.getOrDefault("stationId", "").toString());
			workStation.setStationType(requestMap.getOrDefault("stationType", "").toString());
			workStation.setInbound(inbound);
			workStation.setOutbound(outbound);
			workStation.setWorkArea(workArea);
			workStation.setStorage(storage);
			workStation.setZone(requestMap.getOrDefault("zone", "").toString());

			workStation.setOperatorList(operatorList);
			workStation.setInLimit(Integer.valueOf(requestMap.getOrDefault("inLimit", "").toString()));
			workStation.setOutLimit(Integer.valueOf(requestMap.getOrDefault("outLimit", "").toString()));

			workStation.setModifiedDate(new Date());

			workStationRepository.save(workStation);

			returnmap.put("status", "success");
			returnmap.put("message", workStation.getStationId() + " updated successfully");

			logsServices.addLog(AppConstants.event, AppConstants.WORK_STATION, "updateWorkStation",
					"Work Station ID -" + workStation.getStationId() + " updated successfully, ", null);

			return new ResponseEntity<>(returnmap, HttpStatus.OK);
		} catch (Exception e) {
			returnmap.put("status", "error");
			returnmap.put("message", AppConstants.errormsg);

			logsServices.addLog(AppConstants.error, AppConstants.WORK_STATION, "updateWorkStation",
					AppConstants.errormsg, null);
			return new ResponseEntity<>(returnmap, HttpStatus.OK);
		}
	}

	@DeleteMapping(value = "/workStation")
	public ResponseEntity<?> deleteWorkStation(HttpServletRequest request, HttpSession httpSession,
			@RequestBody HashMap<String, Object> requestMap) throws InterruptedException, ExecutionException {

		Map<String, String> returnmap = new HashMap<>();
		try {

			ResponseEntity<?> responseEntity = checkSessionKey(httpSession, Utils.getCookie(request, "JSESSIONID"), "WWED");
			if (responseEntity != null) { 
				logsServices.addLog(AppConstants.error, AppConstants.WORK_STATION, "deleteWorkStation",
						AppConstants.sessiontimeout, null);
				return responseEntity; 
			}
			

			Map<String, String> reuestMap = new HashMap<>();
			reuestMap.put("params", new JSONObject(requestMap).toString()); 
			
			logsServices.addLog(AppConstants.event, AppConstants.WORK_STATION, "deleteWorkStation",
					"Delete WorkStation api call", reuestMap);
			
			if (Utils.isEmptyString(requestMap.getOrDefault("stationId", "").toString())) {

				returnmap.put("status", "error");
				returnmap.put("message", AppConstants.fieldsmissing);

				logsServices.addLog(AppConstants.error, AppConstants.WORK_STATION, "deleteWorkStation",
						AppConstants.fieldsmissing, null);
				return new ResponseEntity<>(returnmap, HttpStatus.OK);

			}

			WorkStation workStation = workStationRepository
					.findByStationId(requestMap.getOrDefault("stationId", "").toString());

			if (workStation == null) {
				returnmap.put("status", "error");
				returnmap.put("message", "Work Station Id not found");

				logsServices.addLog(AppConstants.error, AppConstants.WORK_STATION, "deleteWorkStation",
						returnmap.get("message").toString(), null);
				return new ResponseEntity<>(returnmap, HttpStatus.OK);
			} else if (!workStation.isActive()) {
				returnmap.put("status", "error");
				returnmap.put("message", "Work Station id is not active");

				logsServices.addLog(AppConstants.error, AppConstants.WORK_STATION, "deleteWorkStation",
						returnmap.get("message").toString(), null);
				return new ResponseEntity<>(returnmap, HttpStatus.OK);
			}

			workStation.setActive(false);
			workStation.setModifiedDate(new Date());
			workStationRepository.save(workStation);

			returnmap.put("status", "success");
			returnmap.put("message", "Work Station status updated successfully.");

			logsServices.addLog(AppConstants.event, AppConstants.WORK_STATION, "deleteWorkStation",
					"Work Station Id-" + workStation.getStationId() + " status updated successfully, ", null);

			return new ResponseEntity<>(returnmap, HttpStatus.OK);
		} catch (Exception e) {
			returnmap.put("status", "error");
			returnmap.put("message", AppConstants.errormsg);

			logsServices.addLog(AppConstants.error, AppConstants.WORK_STATION, "deleteWorkStation",
					returnmap.get("message").toString(), null);
			return new ResponseEntity<>(returnmap, HttpStatus.OK);
		}
	}

}
