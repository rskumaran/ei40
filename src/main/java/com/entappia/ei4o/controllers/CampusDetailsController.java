package com.entappia.ei4o.controllers;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.concurrent.ExecutionException;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.entappia.ei4o.constants.AppConstants;
import com.entappia.ei4o.dbmodels.AssetChrono;
import com.entappia.ei4o.dbmodels.CampusDetails;
import com.entappia.ei4o.dbmodels.Doors;
import com.entappia.ei4o.dbmodels.Material;
import com.entappia.ei4o.dbmodels.OutlinesDetails;
import com.entappia.ei4o.dbmodels.Partitions;
import com.entappia.ei4o.dbmodels.PathWay;
import com.entappia.ei4o.dbmodels.Walls;
import com.entappia.ei4o.repository.CampusDetailsRepository;
import com.entappia.ei4o.repository.DoorsRepository;
import com.entappia.ei4o.repository.OutLineRepository;
import com.entappia.ei4o.repository.PartitionsRepository;
import com.entappia.ei4o.repository.PathWayRepository;
import com.entappia.ei4o.repository.WallsRepository;
import com.entappia.ei4o.services.LogsServices;
import com.entappia.ei4o.utils.BaseController;
import com.entappia.ei4o.utils.Utils;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

@RestController
public class CampusDetailsController extends BaseController {

	@Autowired
	private LogsServices logsServices;

	private static Gson gson = new Gson();

	@Autowired
	private CampusDetailsRepository campusDetailsRepository;

	@Autowired
	private OutLineRepository outLineRepository;

	@Autowired
	private PartitionsRepository partitionsRepository;

	@Autowired
	private PathWayRepository pathWayRepository;

	@Autowired
	private DoorsRepository doorsRepository;

	@Autowired
	private WallsRepository wallsRepository;

	Type mapType = new TypeToken<CampusDetails>() {
	}.getType();

	@RequestMapping(path = { "campusDetails", "campusDetails/{campusId}" }, method = RequestMethod.GET)
	public ResponseEntity<?> getCampusDetails(HttpServletRequest request, HttpSession httpSession,
			@PathVariable Optional<Long> campusId) throws InterruptedException, ExecutionException {
		Map<String, Object> returnmap = new HashMap<>();
		HashMap<String, String> reuestMap = new HashMap<>();
		try {
			 
			//"CTPE"
			ResponseEntity<?> responseEntity = checkSessionKey(httpSession, Utils.getCookie(request, "JSESSIONID"));
			if (responseEntity != null) {
				
				logsServices.addLog(AppConstants.error, AppConstants.campusdetails, "getcampusDetails",
						AppConstants.sessiontimeout, null);
				return responseEntity; 
			}
			
			JSONObject jsonObject = new JSONObject();
			if (campusId.isPresent()) {
				jsonObject.put("campusId", campusId.get());
			} else {
				jsonObject.put("campusId", "all");
			}

			reuestMap.put("params", jsonObject.toString());

			logsServices.addLog(AppConstants.event, AppConstants.campusdetails, "getcampusDetails",
					"Campus Details get API Details", reuestMap);
			
			CampusDetails campusDetails = null;
			if (campusId.isPresent()) {
				campusDetails = campusDetailsRepository.findByCampusId(campusId.get());
				if (campusDetails == null) {
					returnmap.put("status", "error");
					returnmap.put("message", "Campus Id does not exist");
					logsServices.addLog(AppConstants.error, AppConstants.campusdetails, "getcampusDetails",
							returnmap.get("message").toString(), null);

					return new ResponseEntity<>(returnmap, HttpStatus.OK);
				}

			} else {
				List<CampusDetails> campusDetailsList = campusDetailsRepository.findAll();
				if (campusDetailsList != null && campusDetailsList.size() > 0)
					campusDetails = campusDetailsList.get(0);
			}
			logsServices.addLog(AppConstants.success, AppConstants.campusdetails, "getcampusDetails",
					"get campus Details", null);

			returnmap.put("status", "success");
			returnmap.put("data", campusDetails);

			return new ResponseEntity<>(returnmap, HttpStatus.OK);
		} catch (Exception e) {
			returnmap.put("status", "error");
			returnmap.put("message", AppConstants.errormsg);
			logsServices.addLog(AppConstants.error, AppConstants.campusdetails, "getcampusDetails",
					AppConstants.errormsg, null);

			return new ResponseEntity<>(returnmap, HttpStatus.OK);
		}

	}

	@PostMapping(value = "/campusDetails")
	public ResponseEntity<?> AddcampusDetails(HttpServletRequest request, HttpSession httpSession,
			@RequestBody CampusDetails campusDetails) throws InterruptedException, ExecutionException {
		Map<String, String> returnmap = new HashMap<>();
		try {
			
			ResponseEntity<?> responseEntity = checkSessionKey(httpSession, Utils.getCookie(request, "JSESSIONID"), "CTPE");
			if (responseEntity != null) { 
				logsServices.addLog(AppConstants.error, AppConstants.campusdetails, "AddcampusDetails",
					AppConstants.sessiontimeout, null);
				return responseEntity; 
			}
			
			Map<String, String> requestLogMap = new HashMap<>(); 
			requestLogMap.put("params", gson.toJson(campusDetails, mapType));
			
			logsServices.addLog(AppConstants.event, AppConstants.campusdetails, "AddcampusDetails",
					"Campus Details add API Details", requestLogMap);
			 
			if (campusDetails.getWidth() <= 0 || campusDetails.getHeight() <= 0
					|| Utils.isEmptyString(campusDetails.getName())
					|| Utils.isEmptyString(campusDetails.getColorCode())) {

				returnmap.put("status", "error");
				returnmap.put("message", AppConstants.fieldsmissing);

				logsServices.addLog(AppConstants.error, AppConstants.campusdetails, "AddcampusDetails",
						AppConstants.fieldsmissing, null);
				return new ResponseEntity<>(returnmap, HttpStatus.OK);
			}

			List<CampusDetails> campusDetailsList = campusDetailsRepository.findAll();

			if (campusDetailsList != null && campusDetailsList.size() > 0) {
				returnmap.put("status", "error");
				returnmap.put("message", "Campus Details already exist");

				logsServices.addLog(AppConstants.error, AppConstants.campusdetails, "AddcampusDetails",
						returnmap.get("message"), null);
				return new ResponseEntity<>(returnmap, HttpStatus.OK);
			}

			CampusDetails campusDetails2 = new CampusDetails();

			campusDetails2.setCreatedDate(new Date());
			campusDetails2.setModifiedDate(new Date());
			campusDetails2.setWidth(campusDetails.getWidth());
			campusDetails2.setHeight(campusDetails.getHeight());
			campusDetails2.setColorCode(campusDetails.getColorCode());
			campusDetails2.setActive(true);
			campusDetailsRepository.save(campusDetails2);

			returnmap.put("status", "success");
			returnmap.put("message", "Campus Details added successfully.");

			logsServices.addLog(AppConstants.event, AppConstants.campusdetails, "AddcampusDetails",
					"Campus Details added successfully.", null);

			return new ResponseEntity<>(returnmap, HttpStatus.OK);
		} catch (Exception e) {
			returnmap.put("status", "error");
			returnmap.put("message", AppConstants.errormsg);

			logsServices.addLog(AppConstants.error, AppConstants.campusdetails, "AddcampusDetails",
					AppConstants.errormsg, null);
			return new ResponseEntity<>(returnmap, HttpStatus.OK);
		}
	}

	@PutMapping(value = "/campusDetails")
	public ResponseEntity<?> updatecampusDetails(HttpServletRequest request, HttpSession httpSession,
			@RequestBody CampusDetails campusDetails) throws InterruptedException, ExecutionException {
		Map<String, String> returnmap = new HashMap<>();
		try {
			ResponseEntity<?> responseEntity = checkSessionKey(httpSession, Utils.getCookie(request, "JSESSIONID"), "CTPE");
			if (responseEntity != null) { 
				logsServices.addLog(AppConstants.error, AppConstants.campusdetails, "updatecampusDetails",
						AppConstants.sessiontimeout, null);
				return responseEntity; 
			}
			
			Map<String, String> requestLogMap = new HashMap<>(); 
			requestLogMap.put("params", gson.toJson(campusDetails, mapType));
			
			logsServices.addLog(AppConstants.event, AppConstants.campusdetails, "updatecampusDetails",
					"Campus Details update API Details", requestLogMap);
			
		 

			if (campusDetails.getCampusId() <= 0 || campusDetails.getWidth() <= 0 || campusDetails.getHeight() <= 0
					|| Utils.isEmptyString(campusDetails.getColorCode())
					|| Utils.isEmptyString(campusDetails.getName())) {

				returnmap.put("status", "error");
				returnmap.put("message", AppConstants.fieldsmissing);

				logsServices.addLog(AppConstants.error, AppConstants.campusdetails, "updatecampusDetails",
						AppConstants.fieldsmissing, null);
				return new ResponseEntity<>(returnmap, HttpStatus.OK);
			}

			CampusDetails campusDetails1 = campusDetailsRepository.findByCampusId(campusDetails.getCampusId());

			if (campusDetails1 == null) {
				returnmap.put("status", "error");
				returnmap.put("message", "Campus Details not exist");

				logsServices.addLog(AppConstants.error, AppConstants.campusdetails, "updatecampusDetails",
						returnmap.get("message"), null);
				return new ResponseEntity<>(returnmap, HttpStatus.OK);
			}

			campusDetails1.setModifiedDate(new Date());
			campusDetails1.setName(campusDetails.getName());
			campusDetails1.setWidth(campusDetails.getWidth());
			campusDetails1.setHeight(campusDetails.getHeight());
			campusDetails1.setColorCode(campusDetails.getColorCode());
			campusDetailsRepository.save(campusDetails1);

			returnmap.put("status", "success");
			returnmap.put("message", "Campus Details updated successfully.");

			logsServices.addLog(AppConstants.event, AppConstants.campusdetails, "updatecampusDetails",
					"Campus Details updated successfully.", null);

			return new ResponseEntity<>(returnmap, HttpStatus.OK);
		} catch (Exception e) {
			returnmap.put("status", "error");
			returnmap.put("message", AppConstants.errormsg);

			logsServices.addLog(AppConstants.error, AppConstants.campusdetails, "updatecampusDetails",
					AppConstants.errormsg, null);
			return new ResponseEntity<>(returnmap, HttpStatus.OK);
		}
	}

	@PostMapping(value = "/importCampusDetails")
	public ResponseEntity<?> importCampusDetails(HttpServletRequest request, HttpSession httpSession,
			@RequestBody HashMap<String, Object> requestMap) throws InterruptedException, ExecutionException {
		Map<String, String> returnmap = new HashMap<>();
		try {
			ResponseEntity<?> responseEntity = checkSessionKey(httpSession, Utils.getCookie(request, "JSESSIONID"), "CTPE");
			if (responseEntity != null) { 
				logsServices.addLog(AppConstants.error, AppConstants.campusdetails, "importCampusDetails",
						AppConstants.sessiontimeout, null);
				return responseEntity; 
			}
			
			Map<String, String> requestLogMap = new HashMap<>(); 
			requestLogMap.put("params", new JSONObject(requestMap).toString());
			
			logsServices.addLog(AppConstants.event, AppConstants.campusdetails, "importCampusDetails",
					"Import Campus Details API Details", requestLogMap);
			
			if (Utils.isEmptyString(requestMap.getOrDefault("version", "").toString())
					|| Utils.isEmptyString(requestMap.getOrDefault("type", "").toString())
					|| Utils.isEmptyString(requestMap.getOrDefault("campus", "").toString())
					|| Utils.isEmptyString(requestMap.getOrDefault("outline", "").toString())
					|| Utils.isEmptyString(requestMap.getOrDefault("door", "").toString())
					|| Utils.isEmptyString(requestMap.getOrDefault("pathway", "").toString())
					|| Utils.isEmptyString(requestMap.getOrDefault("partition", "").toString())
					|| Utils.isEmptyString(requestMap.getOrDefault("wall", "").toString())) {

				returnmap.put("status", "error");
				returnmap.put("message", AppConstants.fieldsmissing);

				logsServices.addLog(AppConstants.error, AppConstants.campusdetails, "importCampusDetails",
						AppConstants.fieldsmissing, null);
				return new ResponseEntity<>(returnmap, HttpStatus.OK);

			}

			if (!requestMap.getOrDefault("type", "").toString().equalsIgnoreCase("topology")) {

				returnmap.put("status", "error");
				returnmap.put("message", "Invalid type");

				logsServices.addLog(AppConstants.error, AppConstants.campusdetails, "importCampusDetails",
						returnmap.get("message"), null);
				return new ResponseEntity<>(returnmap, HttpStatus.OK);
			}

			Type campusDetailsMapType = new TypeToken<CampusDetails>() {
			}.getType();

			CampusDetails campusDetails = gson.fromJson(gson.toJson(requestMap.getOrDefault("campus", "")),
					campusDetailsMapType);

			if (campusDetails.getWidth() <= 0 || campusDetails.getHeight() <= 0
					|| Utils.isEmptyString(campusDetails.getName())
					|| Utils.isEmptyString(campusDetails.getColorCode())) {

				returnmap.put("status", "error");
				returnmap.put("message", "Missing CampusDetails data");

				logsServices.addLog(AppConstants.error, AppConstants.campusdetails, "importCampusDetails",
						returnmap.get("message"), null);
				return new ResponseEntity<>(returnmap, HttpStatus.OK);
			}

			/*
			 * Type outlinesDetailsMapType = new
			 * TypeToken<List<OutlinesDetails>>() { }.getType();
			 * 
			 * List<OutlinesDetails> outlinesDetail =
			 * gson.fromJson(gson.toJson(requestMap.getOrDefault("outline",
			 * "")), outlinesDetailsMapType);
			 */

			JSONArray outlineJSONArray = new JSONArray(gson.toJson(requestMap.getOrDefault("outline", "")));

			if (outlineJSONArray != null && outlineJSONArray.length() > 0) {
				for (int i = 0; i < outlineJSONArray.length(); i++) {

					JSONObject outlineJSONObject = outlineJSONArray.getJSONObject(i);
					if (outlineJSONObject != null) {

						if (outlineJSONObject.optLong("outlinesId") <= 0 ||  outlineJSONObject.optLong("campusId") <= 0
								|| Utils.isEmptyString(outlineJSONObject.getString("name"))
								|| Utils.isEmptyString(outlineJSONObject.getString("placeType"))
								|| Utils.isEmptyString(outlineJSONObject.getString("wallType"))
								|| Utils.isEmptyString(outlineJSONObject.getString("outlinesArea"))
								|| Utils.isEmptyString(outlineJSONObject.getString("bgColor"))
								|| Utils.isEmptyString(outlineJSONObject.getString("geometryType"))
								|| Utils.isEmptyString(outlineJSONObject.getJSONArray("coordinateList").toString())) {

							returnmap.put("status", "error");
							returnmap.put("message", "Missing Outline data");

							logsServices.addLog(AppConstants.error, AppConstants.campusdetails, "importCampusDetails",
									returnmap.get("message"), null);
							return new ResponseEntity<>(returnmap, HttpStatus.OK);

						}
					}
				}
			} else {
				returnmap.put("status", "error");
				returnmap.put("message", "Missing Outline data");

				logsServices.addLog(AppConstants.error, AppConstants.campusdetails, "importCampusDetails",
						returnmap.get("message"), null);
				return new ResponseEntity<>(returnmap, HttpStatus.OK);

			}

			JSONArray partitionJSONArray = new JSONArray(gson.toJson(requestMap.getOrDefault("partition", "")));

			if (partitionJSONArray != null && partitionJSONArray.length() > 0) {
				for (int i = 0; i < partitionJSONArray.length(); i++) {

					JSONObject pathwayJSONObject = partitionJSONArray.getJSONObject(i);
					if (pathwayJSONObject != null) {
						if (pathwayJSONObject.optLong("id") <= 0 || pathwayJSONObject.optLong("outlinesId") <= 0
								|| Utils.isEmptyString(pathwayJSONObject.optString("name"))
								|| Utils.isEmptyString(pathwayJSONObject.optString("geometryType"))
								|| Utils.isEmptyString(pathwayJSONObject.optString("wallType"))
								|| Utils.isEmptyString(pathwayJSONObject.optString("colorCode"))
								|| Utils.isEmptyString(pathwayJSONObject.getJSONArray("coordinates").toString())) {

							returnmap.put("status", "error");
							returnmap.put("message", "Missing Partition data");

							logsServices.addLog(AppConstants.error, AppConstants.campusdetails, "importCampusDetails",
									returnmap.get("message"), null);
							return new ResponseEntity<>(returnmap, HttpStatus.OK);

						}
					}

				}

			} else {
				returnmap.put("status", "error");
				returnmap.put("message", "Missing Partition data");

				logsServices.addLog(AppConstants.error, AppConstants.campusdetails, "importCampusDetails",
						returnmap.get("message"), null);
				return new ResponseEntity<>(returnmap, HttpStatus.OK);

			}

			JSONArray pathwayJSONArray = new JSONArray(gson.toJson(requestMap.getOrDefault("pathway", "")));

			if (pathwayJSONArray != null && pathwayJSONArray.length() > 0) {
				for (int i = 0; i < pathwayJSONArray.length(); i++) {

					JSONObject pathwayJSONObject = pathwayJSONArray.getJSONObject(i);
					if (pathwayJSONObject != null) {
						if (pathwayJSONObject.optLong("id") <= 0 || pathwayJSONObject.optLong("campusId") <= 0
								|| Utils.isEmptyString(pathwayJSONObject.optString("name"))
								|| Utils.isEmptyString(pathwayJSONObject.optString("geometryType"))
								|| Utils.isEmptyString(pathwayJSONObject.optString("pathwayType"))
								|| Utils.isEmptyString(pathwayJSONObject.optString("pathwayPattern"))
								|| Utils.isEmptyString(pathwayJSONObject.optString("colorCode"))
								|| Utils.isEmptyString(pathwayJSONObject.getJSONArray("coordinates").toString())) {

							returnmap.put("status", "error");
							returnmap.put("message", "Missing Pathway data");

							logsServices.addLog(AppConstants.error, AppConstants.campusdetails, "importCampusDetails",
									returnmap.get("message"), null);
							return new ResponseEntity<>(returnmap, HttpStatus.OK);

						}
					}

				}

			} else {
				returnmap.put("status", "error");
				returnmap.put("message", "Missing Pathway data");

				logsServices.addLog(AppConstants.error, AppConstants.campusdetails, "importCampusDetails",
						returnmap.get("message"), null);
				return new ResponseEntity<>(returnmap, HttpStatus.OK);

			}

			JSONArray wallJSONArray = new JSONArray(gson.toJson(requestMap.getOrDefault("wall", "")));

			if (wallJSONArray != null && wallJSONArray.length() > 0) {
				for (int i = 0; i < wallJSONArray.length(); i++) {

					JSONObject wallJSONObject = wallJSONArray.getJSONObject(i);
					if (wallJSONObject != null) {
						if (wallJSONObject.optLong("id") <= 0 || wallJSONObject.optLong("outlinesId") <= 0
								|| Utils.isEmptyString(wallJSONObject.optString("name"))
								|| Utils.isEmptyString(wallJSONObject.optString("colorCode"))
								|| Utils.isEmptyString(wallJSONObject.optString("geometryType"))
								|| Utils.isEmptyString(wallJSONObject.optString("wallType"))
								|| Utils.isEmptyString(wallJSONObject.getJSONArray("coordinates").toString())) {

							returnmap.put("status", "error");
							returnmap.put("message", "Missing Wall data");

							logsServices.addLog(AppConstants.error, AppConstants.campusdetails, "importCampusDetails",
									returnmap.get("message"), null);
							return new ResponseEntity<>(returnmap, HttpStatus.OK);

						}
					}

				}

			} else {
				returnmap.put("status", "error");
				returnmap.put("message", "Missing Wall data");

				logsServices.addLog(AppConstants.error, AppConstants.campusdetails, "importCampusDetails",
						returnmap.get("message"), null);
				return new ResponseEntity<>(returnmap, HttpStatus.OK);

			}

			JSONArray doorJSONArray = new JSONArray(gson.toJson(requestMap.getOrDefault("door", "")));

			if (doorJSONArray != null && doorJSONArray.length() > 0) {
				for (int i = 0; i < doorJSONArray.length(); i++) {

					JSONObject doorJSONObject = doorJSONArray.getJSONObject(i);
					if (doorJSONObject != null) {
						if (doorJSONObject.optLong("id") <= 0 || doorJSONObject.optLong("outlinesId") <= 0
								|| Utils.isEmptyString(doorJSONObject.optString("name"))
								|| Utils.isEmptyString(doorJSONObject.optString("colorCode"))
								|| Utils.isEmptyString(doorJSONObject.optString("wallType"))
								|| Utils.isEmptyString(doorJSONObject.getJSONArray("coordinates").toString())) {

							returnmap.put("status", "error");
							returnmap.put("message", "Missing Doors data");

							logsServices.addLog(AppConstants.error, AppConstants.campusdetails, "importCampusDetails",
									returnmap.get("message"), null);
							return new ResponseEntity<>(returnmap, HttpStatus.OK);

						}
					}

				}

			} else {
				returnmap.put("status", "error");
				returnmap.put("message", "Missing Doors data");

				logsServices.addLog(AppConstants.error, AppConstants.campusdetails, "importCampusDetails",
						returnmap.get("message"), null);
				return new ResponseEntity<>(returnmap, HttpStatus.OK);

			}
			Date runTime = new Date();
			CampusDetails campusDetails2 = new CampusDetails();
			campusDetails2.setCampusId(1);
			campusDetails2.setCreatedDate(runTime);
			campusDetails2.setModifiedDate(runTime);
			campusDetails2.setName(campusDetails.getName());
			campusDetails2.setWidth(campusDetails.getWidth());
			campusDetails2.setHeight(campusDetails.getHeight());
			campusDetails2.setColorCode(campusDetails.getColorCode());
			campusDetails2.setGridZoneDetails(Utils.getGridHashmap((int) Math.ceil(campusDetails.getWidth()),
					(int) Math.ceil(campusDetails.getHeight())));
			campusDetails2.setActive(true);
			campusDetailsRepository.save(campusDetails2);

			List<Long> outlineIdList = new ArrayList<>(); 

			// ----------------- Outlines Start--------------
			//List<OutlinesDetails> outlinesList = new ArrayList<>(); 
			HashMap<Long, OutlinesDetails> outlinesDBMap = getOutlinesDetails();
			HashMap<Long, OutlinesDetails> outlinesIdMap = new HashMap<>();
			
			Type mapType = new TypeToken<List<HashMap<String, String>>>() {
			}.getType();

			for (int i = 0; i < outlineJSONArray.length(); i++) {

				JSONObject outlineJSONObject = outlineJSONArray.getJSONObject(i);
				if (outlineJSONObject != null) {

					List<HashMap<String, String>> coordinateList = gson
							.fromJson(outlineJSONObject.getJSONArray("coordinateList").toString(), mapType);

					OutlinesDetails outlines = new OutlinesDetails();
					if(outlinesDBMap.containsKey(outlineJSONObject.optLong("outlinesId")))
						outlines.setOutlinesId(outlineJSONObject.optLong("outlinesId"));
					
					outlines.setCampusDetails(campusDetails2);
					outlines.setName(outlineJSONObject.getString("name"));
					outlines.setPlaceType(outlineJSONObject.getString("placeType"));
					outlines.setWallType(outlineJSONObject.getString("wallType"));
					outlines.setOutlinesArea(outlineJSONObject.getString("outlinesArea"));
					outlines.setBgColor(outlineJSONObject.getString("bgColor"));
					outlines.setGeometryType(outlineJSONObject.getString("geometryType"));
					outlines.setCoordinateList(coordinateList);
					outlines.setActive(true);
					outlines.setCreatedDate(runTime);
					outlines.setModifiedDate(runTime);
 
					OutlinesDetails outline1 = outLineRepository.save(outlines);
					 
					outlinesIdMap.put(outlineJSONObject.optLong("outlinesId"), outline1);
					outlineIdList.add(outline1.getOutlinesId());
				}
			}
			

			// ----------------- outLine End--------------

			// ----------------- Partition Start --------------

			
			List<Partitions> partitionList = new ArrayList<>();
			partitionsRepository.truncateTable();
			
			for (int i = 0; i < partitionJSONArray.length(); i++) {

				JSONObject pathwayJSONObject = partitionJSONArray.getJSONObject(i);
				if (pathwayJSONObject != null) {

					List<HashMap<String, String>> coordinateList = gson
							.fromJson(pathwayJSONObject.getJSONArray("coordinates").toString(), mapType);

					Partitions partitions = new Partitions(); 
					partitions.setName(pathwayJSONObject.optString("name"));
					partitions.setOutlinesDetails(outlinesIdMap.get(pathwayJSONObject.optLong("outlinesId")));
					partitions.setGeometryType(pathwayJSONObject.optString("geometryType"));
					partitions.setWallType(pathwayJSONObject.optString("wallType"));
					partitions.setColorCode(pathwayJSONObject.optString("colorCode"));
					partitions.setCoordinates(coordinateList);
					partitions.setCreatedDate(runTime);
					partitions.setModifiedDate(runTime);
					partitions.setActive(true);
					partitionList.add(partitions); 

				}
			}
			
			partitionsRepository.saveAll(partitionList);
			// ----------------- partition End--------------

			// ----------------- Pathway Start --------------
			pathWayRepository.truncateTable();
			
			List<PathWay> pathwayList = new ArrayList<>();
			for (int i = 0; i < pathwayJSONArray.length(); i++) {

				JSONObject pathwayJSONObject = pathwayJSONArray.getJSONObject(i);
				if (pathwayJSONObject != null) {

					List<HashMap<String, String>> coordinateList = gson
							.fromJson(pathwayJSONObject.getJSONArray("coordinates").toString(), mapType);

					PathWay pathWay = new PathWay(); 
					pathWay.setName(pathwayJSONObject.optString("name"));
					pathWay.setCampusDetails(campusDetails2);
					pathWay.setGeometryType(pathwayJSONObject.optString("geometryType"));
					pathWay.setPathwayType(pathwayJSONObject.optString("pathwayType"));
					pathWay.setPathwayPattern(pathwayJSONObject.optString("pathwayPattern"));
					pathWay.setColorCode(pathwayJSONObject.optString("colorCode"));
					pathWay.setCoordinates(coordinateList);
					pathWay.setCreatedDate(runTime);
					pathWay.setModifiedDate(runTime);
					pathWay.setActive(true);
					pathwayList.add(pathWay); 

				}
			}
			pathWayRepository.saveAll(pathwayList);
			// ----------------- Pathway End --------------

			// ----------------- Door Start --------------

			List<Doors> doorList = new ArrayList<>();
			doorsRepository.truncateTable();
			
			for (int i = 0; i < doorJSONArray.length(); i++) {

				JSONObject doorJSONObject = doorJSONArray.getJSONObject(i);
				if (doorJSONObject != null) {

					List<HashMap<String, String>> coordinateList = gson
							.fromJson(doorJSONObject.getJSONArray("coordinates").toString(), mapType);

					Doors doors = new Doors(); 
					doors.setName(doorJSONObject.optString("name"));
					doors.setOutlinesDetails(outlinesIdMap.get(doorJSONObject.optLong("outlinesId")));
					doors.setColorCode(doorJSONObject.optString("colorCode"));
					doors.setWallType(doorJSONObject.optString("wallType"));
					doors.setCoordinates(coordinateList);
					doors.setCreatedDate(runTime);
					doors.setModifiedDate(runTime);
					doors.setActive(true);

					doorList.add(doors); 

				}
			}

			doorsRepository.saveAll(doorList);
			// ----------------- Door End --------------

			// ----------------- Wall Start --------------
			List<Walls> wallList = new ArrayList<>();

			wallsRepository.truncateTable();
			
			for (int i = 0; i < wallJSONArray.length(); i++) {

				JSONObject wallJSONObject = wallJSONArray.getJSONObject(i);
				if (wallJSONObject != null) {

					Walls walls = new Walls();
					List<HashMap<String, String>> coordinateList = gson
							.fromJson(wallJSONObject.getJSONArray("coordinates").toString(), mapType);
 
					walls.setName(wallJSONObject.optString("name"));
					walls.setOutlinesDetails(outlinesIdMap.get(wallJSONObject.optLong("outlinesId")));
					walls.setColorCode(wallJSONObject.optString("colorCode"));
					walls.setGeometryType(wallJSONObject.optString("geometryType"));
					walls.setWallType(wallJSONObject.optString("wallType"));
					walls.setCoordinates(coordinateList);
					walls.setCreatedDate(runTime);
					walls.setModifiedDate(runTime);
					walls.setActive(true);

					wallList.add(walls); 
				}

			}

			wallsRepository.saveAll(wallList);
			// ----------------- Wall End --------------

			List<OutlinesDetails> dbOutlinesDetails = outLineRepository.findAll();
			if (dbOutlinesDetails != null && dbOutlinesDetails.size() > 0) {
				for (OutlinesDetails outlinesDetails : dbOutlinesDetails) {
					if (outlinesDetails != null && !outlineIdList.contains(outlinesDetails.getOutlinesId())) {
						outlinesDetails.setModifiedDate(runTime);
						outlinesDetails.setActive(false);
						outLineRepository.save(outlinesDetails);
					}
				}
			}
 

			returnmap.put("status", "success");
			returnmap.put("message", "Campus Details imported successfully.");

			logsServices.addLog(AppConstants.event, AppConstants.campusdetails, "importCampusDetails",
					"Campus Details added successfully.", null);

			return new ResponseEntity<>(returnmap, HttpStatus.OK);
		} catch (Exception e) {
			returnmap.put("status", "error");
			returnmap.put("message", AppConstants.errormsg);
			e.printStackTrace();
			logsServices.addLog(AppConstants.error, AppConstants.campusdetails, "importCampusDetails",
					AppConstants.errormsg, null);
			return new ResponseEntity<>(returnmap, HttpStatus.OK);
		}
	} 
	 
	
	private HashMap<Long, OutlinesDetails> getOutlinesDetails() {
		// TODO Auto-generated method stub

		HashMap<Long, OutlinesDetails> map = new HashMap<Long, OutlinesDetails>();

		outLineRepository.findAll().stream().forEach(outlinesDetails -> {
			map.put(outlinesDetails.getOutlinesId(), outlinesDetails);

		});

		return map;
	}

}
