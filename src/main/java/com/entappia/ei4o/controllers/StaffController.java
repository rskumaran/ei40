package com.entappia.ei4o.controllers;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.concurrent.ExecutionException;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.entappia.ei4o.constants.AppConstants;
import com.entappia.ei4o.dbmodels.AssetChrono;
import com.entappia.ei4o.dbmodels.Department;
import com.entappia.ei4o.dbmodels.Shift;
import com.entappia.ei4o.dbmodels.Staff;
import com.entappia.ei4o.dbmodels.Tags;
import com.entappia.ei4o.repository.DepartmentRepository;
import com.entappia.ei4o.repository.ShiftRepository;
import com.entappia.ei4o.repository.StaffRepository;
import com.entappia.ei4o.repository.TagsRepository;
import com.entappia.ei4o.services.LogsServices;
import com.entappia.ei4o.utils.BaseController;
import com.entappia.ei4o.utils.Utils;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;

@RestController
public class StaffController extends BaseController {

	@Autowired
	StaffRepository staffRepository;

	@Autowired
	DepartmentRepository departmentRepository;

	@Autowired
	ShiftRepository shiftRepository;

	@Autowired
	private LogsServices logsServices;

	@Autowired
	private TagsRepository tagsRepository;

	@RequestMapping(path = { "staff", "staff/{empId}" }, method = RequestMethod.GET)
	public ResponseEntity<?> getstaff(HttpServletRequest request, HttpSession httpSession,
			@PathVariable Optional<String> empId) throws InterruptedException, ExecutionException, JSONException {

		ResponseEntity<?> responseEntity = checkSessionKey(httpSession, Utils.getCookie(request, "JSESSIONID"));
		if (responseEntity != null) { 
			logsServices.addLog(AppConstants.error, AppConstants.staff, "staff",
					AppConstants.sessiontimeout, null);
			return responseEntity; 
		}
		
		JSONObject jsonObject = new JSONObject();
		if (empId.isPresent()) {
			jsonObject.put("empId", empId.get());
		} else {
			jsonObject.put("empId", "all");
		}
		Map<String, String> reuestMap = new HashMap<>();
		reuestMap.put("params", jsonObject.toString());
		

		logsServices.addLog(AppConstants.event, AppConstants.staff, "staff",
				"Get Staffs api call", reuestMap);
		
		Map<String, Object> returnmap = new HashMap<>();
		try {

			List<Staff> staffList = new ArrayList<>();

			List<Tags> tagsList = tagsRepository.listOfAssignedTagsByType("Employee");

			HashMap<String, Tags> tagsMap = new HashMap<>();

			tagsList.forEach(tags -> {
				tagsMap.put(tags.getAssignmentId(), tags);
			});

			if (empId.isPresent()) {
				Staff staffDetails = staffRepository.findByEmpId(empId.get());

				if (staffDetails == null) {
					returnmap.put("status", "error");
					returnmap.put("message", "empId not found");

					logsServices.addLog(AppConstants.error, AppConstants.staff, "staff",
							returnmap.get("message").toString(), null);
					return new ResponseEntity<>(returnmap, HttpStatus.OK);
				} else
					staffList.add(staffDetails);
			} else
				staffList = staffRepository.findAll();

			List<Map<String, Object>> staffMapDetails = new ArrayList<>();

			final ObjectMapper mapper = new ObjectMapper();
			for (Staff staff : staffList) {

				Map<String, Object> map = mapper.convertValue(staff, new TypeReference<Map<String, Object>>() {
				});
				Tags tags = tagsMap.get(staff.getEmpId());
				if (tags != null) {
					map.put("assignedTagMacId", tags.getMacId());
					map.put("assignedStatus", tags.getStatus());
				} else
				{
					map.put("assignedTagMacId", "");
					map.put("assignedStatus", "");
				}

				staffMapDetails.add(map);
			}

			logsServices.addLog(AppConstants.event, AppConstants.staff, "getstaff", "get Shift details", null);

			returnmap.put("status", "success");
			returnmap.put("data", staffMapDetails);

			return new ResponseEntity<>(returnmap, HttpStatus.OK);
		} catch (Exception e) {
			returnmap.put("status", "error");
			returnmap.put("message", AppConstants.errormsg);

			logsServices.addLog(AppConstants.error, AppConstants.staff, "getstaff", returnmap.get("message").toString(),
					null);
			return new ResponseEntity<>(returnmap, HttpStatus.OK);
		}
	}

	@PostMapping(value = "/staff")
	public ResponseEntity<?> addStaff(HttpServletRequest request, HttpSession httpSession,
			@RequestBody HashMap<String, Object> requestMap) throws InterruptedException, ExecutionException {

		Map<String, String> returnmap = new HashMap<>();
		try {

			ResponseEntity<?> responseEntity = checkSessionKey(httpSession, Utils.getCookie(request, "JSESSIONID"), "PEAD");
			if (responseEntity != null) { 
				logsServices.addLog(AppConstants.error, AppConstants.staff, "addStaff",
						AppConstants.sessiontimeout, null);
				return responseEntity; 
			}
			 
			Map<String, String> reuestMap = new HashMap<>();
			reuestMap.put("params", new JSONObject(requestMap).toString());  
			
			logsServices.addLog(AppConstants.event, AppConstants.staff, "addStaff",
					"Add Staffs api call", reuestMap);
			
			if (Utils.isEmptyString(requestMap.getOrDefault("empId", "").toString())
					|| Utils.isEmptyString(requestMap.getOrDefault("firstName", "").toString())
					|| Utils.isEmptyString(requestMap.getOrDefault("lastName", "").toString())
					|| Utils.isEmptyString(requestMap.getOrDefault("location1", "").toString())
					|| Utils.isEmptyString(requestMap.getOrDefault("location2", "").toString())
					|| Utils.isEmptyString(requestMap.getOrDefault("location3", "").toString()) 
					|| Utils.isEmptyString(requestMap.getOrDefault("status", "").toString())
					|| Utils.isEmptyString(requestMap.getOrDefault("department", "").toString())
					|| Utils.isEmptyString(requestMap.getOrDefault("shiftDetails", "").toString())) {

				returnmap.put("status", "error");
				returnmap.put("message", AppConstants.fieldsmissing);

				logsServices.addLog(AppConstants.error, AppConstants.staff, "addStaff", AppConstants.fieldsmissing,
						null);
				return new ResponseEntity<>(returnmap, HttpStatus.OK);

			}

			Staff staffDetails = staffRepository.findByEmpId(requestMap.getOrDefault("empId", "").toString());

			if (staffDetails != null && staffDetails.isActive()) {
				returnmap.put("status", "error");
				returnmap.put("message", "Staff Id already exist");

				logsServices.addLog(AppConstants.error, AppConstants.shift, "addStaff",
						returnmap.get("message").toString(), null);
				return new ResponseEntity<>(returnmap, HttpStatus.OK);
			}

			Department department = departmentRepository
					.findByDeptName(requestMap.getOrDefault("department", "").toString());

			if (department == null || !department.isActive()) {
				returnmap.put("status", "error");
				returnmap.put("message", "Department name not exist");

				logsServices.addLog(AppConstants.error, AppConstants.shift, "addStaff", "Department Name already exist",
						null);
				return new ResponseEntity<>(returnmap, HttpStatus.OK);
			}

			Shift shift = shiftRepository.findByShiftName(requestMap.getOrDefault("shiftDetails", "").toString());

			if (shift == null || !shift.isActive()) {
				returnmap.put("status", "error");
				returnmap.put("message", "Shift Name not exist");

				logsServices.addLog(AppConstants.error, AppConstants.shift, "addStaff",
						returnmap.get("message").toString(), null);
				return new ResponseEntity<>(returnmap, HttpStatus.OK);
			}

			Staff staff = new Staff();
			staff.setEmpId(requestMap.getOrDefault("empId", "").toString());
			staff.setFirstName(requestMap.getOrDefault("firstName", "").toString());
			staff.setLastName(requestMap.getOrDefault("lastName", "").toString());
			staff.setLocation1(requestMap.getOrDefault("location1", "").toString());
			staff.setLocation2(requestMap.getOrDefault("location2", "").toString());
			staff.setLocation3(requestMap.getOrDefault("location3", "").toString());
			staff.setPhoneNumber(requestMap.getOrDefault("phoneNumber", "").toString());
			staff.setStatus(requestMap.getOrDefault("status", "").toString());
			staff.setDepartment(department);
			staff.setShiftDetails(shift);
			staff.setActive(true);
			staff.setCreatedDate(new Date());
			staff.setModifiedDate(new Date());
			staffRepository.save(staff);

			returnmap.put("status", "success");
			returnmap.put("message", "Staff added successfully.");

			logsServices.addLog(AppConstants.event, AppConstants.staff, "addStaff",
					"Staff-" + staff.getEmpId() + " added successfully, ", null);

			return new ResponseEntity<>(returnmap, HttpStatus.OK);
		} catch (Exception e) {
			returnmap.put("status", "error");
			returnmap.put("message", AppConstants.errormsg);

			logsServices.addLog(AppConstants.error, AppConstants.staff, "addStaff", returnmap.get("message").toString(),
					null);
			return new ResponseEntity<>(returnmap, HttpStatus.OK);
		}
	}

	@PutMapping(value = "/staff")
	public ResponseEntity<?> updateStaff(HttpServletRequest request, HttpSession httpSession,
			@RequestBody HashMap<String, Object> requestMap) throws InterruptedException, ExecutionException {

		Map<String, String> returnmap = new HashMap<>();
		try {
			ResponseEntity<?> responseEntity = checkSessionKey(httpSession, Utils.getCookie(request, "JSESSIONID"), "PEED");
			if (responseEntity != null) { 
				logsServices.addLog(AppConstants.error, AppConstants.staff, "updateStaff",
						AppConstants.sessiontimeout, null);
				return responseEntity; 
			}
			

			Map<String, String> reuestMap = new HashMap<>();
			reuestMap.put("params", new JSONObject(requestMap).toString());  
			
			logsServices.addLog(AppConstants.event, AppConstants.staff, "updateStaff",
					"Update Staffs api call", reuestMap);
			
			if (Utils.isEmptyString(requestMap.getOrDefault("empId", "").toString())
					|| Utils.isEmptyString(requestMap.getOrDefault("firstName", "").toString())
					|| Utils.isEmptyString(requestMap.getOrDefault("lastName", "").toString())
					|| Utils.isEmptyString(requestMap.getOrDefault("location1", "").toString())
					|| Utils.isEmptyString(requestMap.getOrDefault("location2", "").toString())
					|| Utils.isEmptyString(requestMap.getOrDefault("location3", "").toString()) 
					|| Utils.isEmptyString(requestMap.getOrDefault("status", "").toString())
					|| Utils.isEmptyString(requestMap.getOrDefault("department", "").toString())
					|| Utils.isEmptyString(requestMap.getOrDefault("shiftDetails", "").toString())) {

				returnmap.put("status", "error");
				returnmap.put("message", AppConstants.fieldsmissing);

				logsServices.addLog(AppConstants.error, AppConstants.staff, "updateStaff", AppConstants.fieldsmissing,
						null);
				return new ResponseEntity<>(returnmap, HttpStatus.OK);

			}

			Staff staffDetails = staffRepository.findByEmpId(requestMap.getOrDefault("empId", "").toString());

			if (staffDetails == null) {
				returnmap.put("status", "error");
				returnmap.put("message", "Staff Id not found");

				logsServices.addLog(AppConstants.error, AppConstants.staff, "updateStaff",
						returnmap.get("message").toString(), null);
				return new ResponseEntity<>(returnmap, HttpStatus.OK);
			} else if (!staffDetails.isActive()) {
				returnmap.put("status", "error");
				returnmap.put("message", "Staff is not active");

				logsServices.addLog(AppConstants.error, AppConstants.staff, "updateStaff",
						returnmap.get("message").toString(), null);
				return new ResponseEntity<>(returnmap, HttpStatus.OK);
			}

			Department department = departmentRepository
					.findByDeptName(requestMap.getOrDefault("department", "").toString());

			if (department == null || !department.isActive()) {
				returnmap.put("status", "error");
				returnmap.put("message", "Department name not exist");

				logsServices.addLog(AppConstants.error, AppConstants.shift, "updateStaff",
						"Department Name already exist", null);
				return new ResponseEntity<>(returnmap, HttpStatus.OK);
			}

			Shift shift = shiftRepository.findByShiftName(requestMap.getOrDefault("shiftDetails", "").toString());

			if (shift == null || !shift.isActive()) {
				returnmap.put("status", "error");
				returnmap.put("message", "Shift Name not exist");

				logsServices.addLog(AppConstants.error, AppConstants.shift, "updateStaff",
						returnmap.get("message").toString(), null);
				return new ResponseEntity<>(returnmap, HttpStatus.OK);
			}

			staffDetails.setEmpId(requestMap.getOrDefault("empId", "").toString());
			staffDetails.setFirstName(requestMap.getOrDefault("firstName", "").toString());
			staffDetails.setLastName(requestMap.getOrDefault("lastName", "").toString());
			staffDetails.setLocation1(requestMap.getOrDefault("location1", "").toString());
			staffDetails.setLocation2(requestMap.getOrDefault("location2", "").toString());
			staffDetails.setLocation3(requestMap.getOrDefault("location3", "").toString());
			staffDetails.setPhoneNumber(requestMap.getOrDefault("phoneNumber", "").toString());
			staffDetails.setStatus(requestMap.getOrDefault("status", "").toString());
			staffDetails.setDepartment(department);
			staffDetails.setShiftDetails(shift);
			staffDetails.setActive(true);
			staffDetails.setModifiedDate(new Date());
			staffRepository.save(staffDetails);

			returnmap.put("status", "success");
			returnmap.put("message", "Staff updated successfully.");

			logsServices.addLog(AppConstants.event, AppConstants.staff, "updateStaff",
					"Staff-" + staffDetails.getEmpId() + " updated successfully, ", null);

			return new ResponseEntity<>(returnmap, HttpStatus.OK);
		} catch (Exception e) {
			returnmap.put("status", "error");
			returnmap.put("message", AppConstants.errormsg);

			logsServices.addLog(AppConstants.error, AppConstants.staff, "updateStaff",
					returnmap.get("message").toString(), null);
			return new ResponseEntity<>(returnmap, HttpStatus.OK);
		}
	}

	@DeleteMapping(value = "/staff")
	public ResponseEntity<?> deleteStaff(HttpServletRequest request, HttpSession httpSession,
			@RequestBody HashMap<String, Object> requestMap) throws InterruptedException, ExecutionException {

		Map<String, String> returnmap = new HashMap<>();
		try {
			ResponseEntity<?> responseEntity = checkSessionKey(httpSession, Utils.getCookie(request, "JSESSIONID"), "PEED");
			if (responseEntity != null) { 
				logsServices.addLog(AppConstants.error, AppConstants.staff, "deleteStaff",
						AppConstants.sessiontimeout, null);
				return responseEntity; 
			}


			Map<String, String> reuestMap = new HashMap<>();
			reuestMap.put("params", new JSONObject(requestMap).toString());  
			
			
			logsServices.addLog(AppConstants.event, AppConstants.staff, "deleteStaff",
					"Delete Staffs api call", reuestMap);
			
			if (Utils.isEmptyString(requestMap.getOrDefault("empId", "").toString())) {

				returnmap.put("status", "error");
				returnmap.put("message", AppConstants.fieldsmissing);

				logsServices.addLog(AppConstants.error, AppConstants.staff, "deleteStaff", AppConstants.fieldsmissing,
						null);
				return new ResponseEntity<>(returnmap, HttpStatus.OK);

			}

			Staff staffDetails = staffRepository.findByEmpId(requestMap.getOrDefault("empId", "").toString());

			if (staffDetails == null) {
				returnmap.put("status", "error");
				returnmap.put("message", "Staff id not found");

				logsServices.addLog(AppConstants.error, AppConstants.staff, "deleteStaff",
						returnmap.get("message").toString(), null);
				return new ResponseEntity<>(returnmap, HttpStatus.OK);
			} else if (!staffDetails.isActive()) {
				returnmap.put("status", "error");
				returnmap.put("message", "Staff is not active");

				logsServices.addLog(AppConstants.error, AppConstants.staff, "deleteStaff",
						returnmap.get("message").toString(), null);
				return new ResponseEntity<>(returnmap, HttpStatus.OK);
			}
			staffDetails.setActive(false);
			staffDetails.setModifiedDate(new Date());
			staffRepository.save(staffDetails);

			returnmap.put("status", "success");
			returnmap.put("message", "Staff status updated successfully.");

			logsServices.addLog(AppConstants.event, AppConstants.staff, "deleteStaff",
					"Staff-" + staffDetails.getEmpId() + " status updated successfully, ", null);

			return new ResponseEntity<>(returnmap, HttpStatus.OK);
		} catch (Exception e) {
			returnmap.put("status", "error");
			returnmap.put("message", AppConstants.errormsg);

			logsServices.addLog(AppConstants.error, AppConstants.staff, "deleteStaff",
					returnmap.get("message").toString(), null);
			return new ResponseEntity<>(returnmap, HttpStatus.OK);
		}
	}

	@PostMapping(value = "/multipleStaffs")
	public ResponseEntity<?> addMultipleStaffs(HttpServletRequest request, HttpSession httpSession,
			@RequestBody List<HashMap<String, Object>> requestMap) throws InterruptedException, ExecutionException {

		Map<String, Object> returnmap = new HashMap<>();
		try {

			ResponseEntity<?> responseEntity = checkSessionKey(httpSession, Utils.getCookie(request, "JSESSIONID"), "PEAD");
			if (responseEntity != null) { 
				logsServices.addLog(AppConstants.error, AppConstants.staff, "addMultipleStaffs",
						AppConstants.sessiontimeout, null);
				return responseEntity; 
			}
			
			ObjectMapper objectMapper = new ObjectMapper(); 
			String json = objectMapper.writeValueAsString(requestMap);
			 
			Map<String, String> requestLogMap = new HashMap<>(); 
			requestLogMap.put("params", json); 
			
			logsServices.addLog(AppConstants.event, AppConstants.staff, "addMultipleStaffs",
					"Add Multiple Staffs api call", requestLogMap);
			
			if (requestMap == null || requestMap.size() == 0) {
				returnmap.put("status", "error");
				returnmap.put("message", AppConstants.fieldsmissing);

				logsServices.addLog(AppConstants.error, AppConstants.staff, "addMultipleStaffs",
						AppConstants.fieldsmissing, null);
				return new ResponseEntity<>(returnmap, HttpStatus.OK);
			}

			List<String> shiftList = getShiftNames();
			List<String> departmentList = getDepartmentNames();

			List<String> errorIds = new ArrayList<>();
			List<String> savedIds = new ArrayList<>();
			HashMap<String, Staff> staffMap = getStaffsNames();

			List<Staff> staffList = new ArrayList<>();
			for (HashMap<String, Object> map : requestMap) {
				if (Utils.isEmptyString(map.getOrDefault("empId", "").toString())
						|| Utils.isEmptyString(map.getOrDefault("firstName", "").toString())
						|| Utils.isEmptyString(map.getOrDefault("lastName", "").toString())
						|| Utils.isEmptyString(map.getOrDefault("location1", "").toString())
						|| Utils.isEmptyString(map.getOrDefault("location2", "").toString())
						|| Utils.isEmptyString(map.getOrDefault("location3", "").toString())
						//|| Utils.isEmptyString(map.getOrDefault("phoneNumber", "").toString())
						|| Utils.isEmptyString(map.getOrDefault("status", "").toString())
						|| Utils.isEmptyString(map.getOrDefault("department", "").toString())
						|| Utils.isEmptyString(map.getOrDefault("shiftDetails", "").toString())) {

					errorIds.add(map.getOrDefault("empId", "").toString());

				} else {
					if (!shiftList.contains(map.getOrDefault("shiftDetails", "").toString())
							|| !departmentList.contains(map.getOrDefault("department", "").toString())) {

						errorIds.add(map.getOrDefault("empId", "").toString());
					} else {

						Staff staff = null;

						if (staffMap.containsKey(map.getOrDefault("empId", "").toString())) {
							staff = staffMap.get(map.getOrDefault("empId", "").toString());
							if (staff == null) {
								staff = new Staff();
								staff.setEmpId(map.getOrDefault("empId", "").toString());
								staff.setActive(true);
								staff.setCreatedDate(new Date());
							}
							staff.setModifiedDate(new Date());
						} else {
							staff = new Staff();
							staff.setCreatedDate(new Date());
							staff.setModifiedDate(new Date());
							staff.setEmpId(map.getOrDefault("empId", "").toString());
							staff.setActive(true);
						}

						if (staff.isActive()) {
							Shift shift = new Shift();
							shift.setShiftName(map.getOrDefault("shiftDetails", "").toString());

							Department department = new Department();
							department.setDeptName(map.getOrDefault("department", "").toString());

							staff.setFirstName(map.getOrDefault("firstName", "").toString());
							staff.setLastName(map.getOrDefault("lastName", "").toString());
							staff.setLocation1(map.getOrDefault("location1", "").toString());
							staff.setLocation2(map.getOrDefault("location2", "").toString());
							staff.setLocation3(map.getOrDefault("location3", "").toString());
							staff.setPhoneNumber(map.getOrDefault("phoneNumber", "").toString());
							staff.setStatus(map.getOrDefault("status", "").toString());
							staff.setDepartment(department);
							staff.setShiftDetails(shift);

							staffList.add(staff);

							savedIds.add(map.getOrDefault("empId", "").toString());
						} else {
							errorIds.add(map.getOrDefault("empId", "").toString());
						}
					}
				}
			}

			if (staffList.size() > 0)
				staffRepository.saveAll(staffList);

			returnmap.put("status", "success");
			returnmap.put("message", savedIds.size() + " Staffs added successfully.");

			returnmap.put("savedIds", savedIds);
			returnmap.put("errorIds", errorIds);

			logsServices.addLog(AppConstants.event, AppConstants.staff, "addMultipleStaffs", "Staff- saved ids ["
					+ String.join(",", savedIds) + "], error ids [" + String.join(",", errorIds) + "].", null);

			return new ResponseEntity<>(returnmap, HttpStatus.OK);
		} catch (Exception e) {
			returnmap.put("status", "error");
			returnmap.put("message", AppConstants.errormsg);

			logsServices.addLog(AppConstants.error, AppConstants.staff, "addMultipleStaffs",
					returnmap.get("message").toString(), null);
			return new ResponseEntity<>(returnmap, HttpStatus.OK);
		}
	}

	private List<String> getShiftNames() {
		// TODO Auto-generated method stub

		List<String> list = new ArrayList<>();

		shiftRepository.findAll().stream().forEach(shift -> {
			if (shift.isActive()) {
				list.add(shift.getShiftName());
			}
		});

		return list;
	}

	private List<String> getDepartmentNames() {
		// TODO Auto-generated method stub

		List<String> list = new ArrayList<>();

		departmentRepository.findAll().stream().forEach(department -> {
			if (department.isActive()) {
				list.add(department.getDeptName());
			}
		});

		return list;
	}

	private HashMap<String, Staff> getStaffsNames() {
		// TODO Auto-generated method stub

		HashMap<String, Staff> map = new HashMap<String, Staff>();

		staffRepository.findAll().stream().forEach(staff -> {
			if (staff.isActive()) {
				map.put(staff.getEmpId(), staff);
			}
		});

		return map;
	}

}
