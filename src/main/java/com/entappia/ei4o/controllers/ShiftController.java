package com.entappia.ei4o.controllers;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.concurrent.ExecutionException;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.entappia.ei4o.constants.AppConstants;
import com.entappia.ei4o.dbmodels.Shift;
import com.entappia.ei4o.dbmodels.Tags;
import com.entappia.ei4o.repository.ShiftRepository;
import com.entappia.ei4o.services.LogsServices;
import com.entappia.ei4o.utils.BaseController;
import com.entappia.ei4o.utils.Utils;

@RestController
public class ShiftController extends BaseController {

	@Autowired
	ShiftRepository shiftRepository;

	@Autowired
	private LogsServices logsServices;

	@PostMapping(value = "/shift")
	public ResponseEntity<?> addShift(HttpServletRequest request, HttpSession httpSession,
			@RequestBody HashMap<String, Object> requestMap) throws InterruptedException, ExecutionException {

		Map<String, String> returnmap = new HashMap<>();
		try {
			 //, "CORS"
			ResponseEntity<?> responseEntity = checkSessionKey(httpSession, Utils.getCookie(request, "JSESSIONID"));
			if (responseEntity != null) { 
				logsServices.addLog(AppConstants.error, AppConstants.shift, "addshift",
						AppConstants.sessiontimeout, null);
				return responseEntity; 
			}
			
			 
			Map<String, String> reuestMap = new HashMap<>();
			reuestMap.put("params", new JSONObject(requestMap).toString()); 

			
			logsServices.addLog(AppConstants.event, AppConstants.shift, "addshift",
					"Add shift api call", reuestMap);
			
			if (Utils.isEmptyString(requestMap.getOrDefault("shiftName", "").toString())
					|| Utils.isEmptyString(requestMap.getOrDefault("shiftType", "").toString())
					|| Utils.isEmptyString(requestMap.getOrDefault("shiftStartTime", "").toString())
					|| Utils.isEmptyString(requestMap.getOrDefault("shiftEndTime", "").toString())) {

				returnmap.put("status", "error");
				returnmap.put("message", AppConstants.fieldsmissing);

				logsServices.addLog(AppConstants.error, AppConstants.shift, "addshift", AppConstants.fieldsmissing,
						null);
				return new ResponseEntity<>(returnmap, HttpStatus.OK);

			}

			Shift shiftDetails = shiftRepository.findByShiftName(requestMap.getOrDefault("shiftName", "").toString());

			if (shiftDetails != null && shiftDetails.isActive()) {
				returnmap.put("status", "error");
				returnmap.put("message", "Shift Name already exist");

				logsServices.addLog(AppConstants.error, AppConstants.shift, "addshift",
						returnmap.get("message").toString(), null);
				return new ResponseEntity<>(returnmap, HttpStatus.OK);
			}

			Shift shift = new Shift();
			shift.setShiftName(requestMap.getOrDefault("shiftName", "").toString());
			shift.setShiftType(requestMap.getOrDefault("shiftType", "").toString());
  
			Date fromTime = new SimpleDateFormat("HH:mm").parse(requestMap.getOrDefault("shiftStartTime", "").toString());
			Date toTime = new SimpleDateFormat("HH:mm").parse(requestMap.getOrDefault("shiftEndTime", "").toString());

			shift.setShiftStartTime(fromTime); 
			shift.setShiftEndTime(toTime);
			shift.setModifiedDate(new Date());
			shift.setCreatedDate(new Date());
			shift.setActive(true);
			shiftRepository.save(shift);

			returnmap.put("status", "success");
			returnmap.put("message", "Shift added successfully.");

			logsServices.addLog(AppConstants.event, AppConstants.shift, "addshift",
					"Shift-" + shift.getShiftName() + " added successfully, ", null);

			return new ResponseEntity<>(returnmap, HttpStatus.OK);
		} catch (Exception e) {
			returnmap.put("status", "error");
			returnmap.put("message", AppConstants.errormsg);

			logsServices.addLog(AppConstants.error, AppConstants.shift, "addshift", returnmap.get("message").toString(),
					null);
			return new ResponseEntity<>(returnmap, HttpStatus.OK);
		}
	}

	@PutMapping(value = "/shift")
	public ResponseEntity<?> updateShift(HttpServletRequest request, HttpSession httpSession,
			@RequestBody HashMap<String, Object> requestMap) throws InterruptedException, ExecutionException {

		Map<String, String> returnmap = new HashMap<>();
		try {

			ResponseEntity<?> responseEntity = checkSessionKey(httpSession, Utils.getCookie(request, "JSESSIONID"), "CORS");
			if (responseEntity != null) { 
				logsServices.addLog(AppConstants.error, AppConstants.shift, "updateShift",
						AppConstants.sessiontimeout, null);
				return responseEntity; 
			}
			
			Map<String, String> reuestMap = new HashMap<>();
			reuestMap.put("params", new JSONObject(requestMap).toString()); 


			logsServices.addLog(AppConstants.event, AppConstants.shift, "updateShift",
					"Update shift api call", reuestMap);
			
			if (Utils.isEmptyString(requestMap.getOrDefault("shiftName", "").toString())
					|| Utils.isEmptyString(requestMap.getOrDefault("shiftType", "").toString())
					|| Utils.isEmptyString(requestMap.getOrDefault("shiftStartTime", "").toString())
					|| Utils.isEmptyString(requestMap.getOrDefault("shiftEndTime", "").toString())) {

				returnmap.put("status", "error");
				returnmap.put("message", AppConstants.fieldsmissing);

				logsServices.addLog(AppConstants.error, AppConstants.shift, "updateShift", AppConstants.fieldsmissing,
						null);
				return new ResponseEntity<>(returnmap, HttpStatus.OK);

			}

			Shift shiftDetails = shiftRepository.findByShiftName(requestMap.getOrDefault("shiftName", "").toString());

			if (shiftDetails == null) {
				returnmap.put("status", "error");
				returnmap.put("message", "Shift Name not found");

				logsServices.addLog(AppConstants.error, AppConstants.shift, "updateShift",
						returnmap.get("message").toString(), null);
				return new ResponseEntity<>(returnmap, HttpStatus.OK);
			} else if (!shiftDetails.isActive()) {
				returnmap.put("status", "error");
				returnmap.put("message", "Shift is not active");

				logsServices.addLog(AppConstants.error, AppConstants.shift, "updateShift",
						returnmap.get("message").toString(), null);
				return new ResponseEntity<>(returnmap, HttpStatus.OK);
			}
 
			shiftDetails.setShiftName(requestMap.getOrDefault("shiftName", "").toString());
			shiftDetails.setShiftType(requestMap.getOrDefault("shiftType", "").toString());


			Date fromTime = new SimpleDateFormat("HH:mm").parse(requestMap.getOrDefault("shiftStartTime", "").toString());
			Date toTime = new SimpleDateFormat("HH:mm").parse(requestMap.getOrDefault("shiftEndTime", "").toString());

			shiftDetails.setShiftStartTime(fromTime); 
			shiftDetails.setShiftEndTime(toTime);

			shiftDetails.setModifiedDate(new Date()); 
 
			shiftRepository.save(shiftDetails);

			returnmap.put("status", "success");
			returnmap.put("message", "Shift updated successfully.");

			logsServices.addLog(AppConstants.event, AppConstants.shift, "updateShift",
					"Shift-" + shiftDetails.getShiftName() + " updated successfully, ", null);

			return new ResponseEntity<>(returnmap, HttpStatus.OK);
		} catch (Exception e) {
			returnmap.put("status", "error");
			returnmap.put("message", AppConstants.errormsg);

			logsServices.addLog(AppConstants.error, AppConstants.shift, "updateShift",
					returnmap.get("message").toString(), null);
			return new ResponseEntity<>(returnmap, HttpStatus.OK);
		}
	}

	@RequestMapping(path = { "shift", "shift/{shiftName}" }, method = RequestMethod.GET) 
	public ResponseEntity<?> getShift(HttpServletRequest request, HttpSession httpSession,
			@PathVariable Optional<String> shiftName) throws InterruptedException, ExecutionException {

		Map<String, Object> returnmap = new HashMap<>();
		try {
			//, "CORS"
			ResponseEntity<?> responseEntity = checkSessionKey(httpSession, Utils.getCookie(request, "JSESSIONID"));
			if (responseEntity != null) {  
				logsServices.addLog(AppConstants.error, AppConstants.shift, "getShift",
						AppConstants.sessiontimeout, null);
				return responseEntity; 
			}
			
			JSONObject jsonObject = new JSONObject();
			if (shiftName.isPresent()) {
				jsonObject.put("shiftName", shiftName.get());
			} else {
				jsonObject.put("shiftName", "all");
			}
			Map<String, String> reuestMap = new HashMap<>();
			reuestMap.put("params", jsonObject.toString()); 

			
			logsServices.addLog(AppConstants.event, AppConstants.shift, "getShift",
					"Get shifts api call", reuestMap);
			
			List<Shift> shiftList = new ArrayList<>();

			if (shiftName.isPresent()) {
				Shift shiftDetails = shiftRepository.findByShiftName(shiftName.get());

				if (shiftDetails == null) {
					returnmap.put("status", "error");
					returnmap.put("message", "Shift Name not found");

					logsServices.addLog(AppConstants.error, AppConstants.shift, "getShift",
							returnmap.get("message").toString(), null);
					return new ResponseEntity<>(returnmap, HttpStatus.OK);
				}else
					shiftList.add(shiftDetails);
			}else
			  shiftList = shiftRepository.findAll();
 
			returnmap.put("status", "success");
			returnmap.put("data", shiftList);
			
			logsServices.addLog(AppConstants.event, AppConstants.shift, "getShift", "get Shift details", null);

			return new ResponseEntity<>(returnmap, HttpStatus.OK);
		} catch (Exception e) {
			returnmap.put("status", "error");
			returnmap.put("message", AppConstants.errormsg);

			logsServices.addLog(AppConstants.error, AppConstants.shift, "getShift", returnmap.get("message").toString(),
					null);
			return new ResponseEntity<>(returnmap, HttpStatus.OK);
		}
	}

	@DeleteMapping(value = "/shift")
	public ResponseEntity<?> deleteShift(HttpServletRequest request, HttpSession httpSession,
			@RequestBody HashMap<String, Object> requestMap) throws InterruptedException, ExecutionException {

		Map<String, String> returnmap = new HashMap<>();
		try {

			ResponseEntity<?> responseEntity = checkSessionKey(httpSession, Utils.getCookie(request, "JSESSIONID"), "CORS");
			if (responseEntity != null) { 
				logsServices.addLog(AppConstants.error, AppConstants.shift, "deleteShift",
						AppConstants.sessiontimeout, null);
				return responseEntity; 
			}
			
			 
			Map<String, String> reuestMap = new HashMap<>();
			reuestMap.put("params", new JSONObject(requestMap).toString()); 
 
			logsServices.addLog(AppConstants.event, AppConstants.shift, "deleteShift",
					"Delete shift api call", reuestMap);
			
			if (Utils.isEmptyString(requestMap.getOrDefault("shiftName", "").toString())) {

				returnmap.put("status", "error");
				returnmap.put("message", AppConstants.fieldsmissing);

				logsServices.addLog(AppConstants.error, AppConstants.shift, "deleteShift", AppConstants.fieldsmissing,
						null);
				return new ResponseEntity<>(returnmap, HttpStatus.OK);

			}

			Shift shiftDetails = shiftRepository.findByShiftName(requestMap.getOrDefault("shiftName", "").toString());

			if (shiftDetails == null) {
				returnmap.put("status", "error");
				returnmap.put("message", "Shift Name not found");

				logsServices.addLog(AppConstants.error, AppConstants.shift, "deleteShift",
						returnmap.get("message").toString(), null);
				return new ResponseEntity<>(returnmap, HttpStatus.OK);
			} else if (!shiftDetails.isActive()) {
				returnmap.put("status", "error");
				returnmap.put("message", "Shift is not active");

				logsServices.addLog(AppConstants.error, AppConstants.shift, "deleteShift",
						returnmap.get("message").toString(), null);
				return new ResponseEntity<>(returnmap, HttpStatus.OK);
			}

			shiftDetails.setModifiedDate(new Date());
			shiftDetails.setActive(false);
			shiftRepository.save(shiftDetails);

			returnmap.put("status", "success");
			returnmap.put("message", "Shift status updated successfully.");

			logsServices.addLog(AppConstants.event, AppConstants.shift, "deleteShift",
					"Shift-" + shiftDetails.getShiftName() + " status updated successfully, ", null);

			return new ResponseEntity<>(returnmap, HttpStatus.OK);
		} catch (Exception e) {
			returnmap.put("status", "error");
			returnmap.put("message", AppConstants.errormsg);

			logsServices.addLog(AppConstants.error, AppConstants.shift, "deleteShift",
					returnmap.get("message").toString(), null);
			return new ResponseEntity<>(returnmap, HttpStatus.OK);
		}
	}
}
