package com.entappia.ei4o.controllers;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.concurrent.ExecutionException;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.entappia.ei4o.constants.AppConstants;
import com.entappia.ei4o.dbmodels.CampusDetails;
import com.entappia.ei4o.dbmodels.LocationHistory;
import com.entappia.ei4o.dbmodels.TagPosition;
import com.entappia.ei4o.repository.LocationHistoryDataRepository;
import com.entappia.ei4o.repository.TagPositionRepository;
import com.entappia.ei4o.services.LogsServices;
import com.entappia.ei4o.utils.BaseController;
import com.entappia.ei4o.utils.Utils;

@RestController
public class TagPositionController extends BaseController {

	@Autowired
	private TagPositionRepository tagPositionRepository;
	
	@Autowired
	private LocationHistoryDataRepository locationHistoryDataRepository;
	
	@Autowired
	private LogsServices logsServices;
	
	@RequestMapping(path = { "tagPositions" }, method = RequestMethod.GET)
	public ResponseEntity<?> getTagPositions(HttpServletRequest request, HttpSession httpSession)
			throws InterruptedException, ExecutionException {
		Map<String, Object> returnmap = new HashMap<>(); 
		try {

			ResponseEntity<?> responseEntity = checkSessionKey(httpSession, Utils.getCookie(request, "JSESSIONID"), "PLVW");
			if (responseEntity != null) { 
				
				logsServices.addLog(AppConstants.error, AppConstants.tag, "getTagPositions",
					AppConstants.sessiontimeout, null);
				return responseEntity; 
			}
			

			logsServices.addLog(AppConstants.event, AppConstants.tag, "getTagPositions",
					"Get Tag Positions api call" , null);
			
			Calendar cal = Calendar.getInstance();
			cal.add(Calendar.MINUTE, -1);
			long currentTime = cal.getTimeInMillis();

			List<TagPosition> tagPositionList = tagPositionRepository.findAllTagDetails(currentTime);
			logsServices.addLog(AppConstants.success, AppConstants.tag, "getTagPositions",
					"get tag position details", null);

			returnmap.put("status", "success");
			returnmap.put("data", tagPositionList);

			return new ResponseEntity<>(returnmap, HttpStatus.OK);
		} catch (Exception e) {
			returnmap.put("status", "error");
			returnmap.put("message", AppConstants.errormsg);
			logsServices.addLog(AppConstants.error, AppConstants.tag, "getTagPositions",
					AppConstants.errormsg, null);

			return new ResponseEntity<>(returnmap, HttpStatus.OK);
		}

	}
	
	@RequestMapping(path = { "historyTagPositions" }, method = RequestMethod.POST)
	public ResponseEntity<?> getHistoryTagPositions(HttpServletRequest request, HttpSession httpSession,
			@RequestBody HashMap<String, Object> requestMap )
			throws InterruptedException, ExecutionException {
		Map<String, Object> returnmap = new HashMap<>(); 
		try {

		 
			
			ResponseEntity<?> responseEntity = checkSessionKey(httpSession, Utils.getCookie(request, "JSESSIONID"), "PLVW");
			if (responseEntity != null) {
				logsServices.addLog(AppConstants.error, AppConstants.tag, "getHistoryTagPositions",
						AppConstants.sessiontimeout, null);
				return responseEntity; 
			}

			Map<String, String> reuestMap = new HashMap<>();
			reuestMap.put("params", new JSONObject(requestMap).toString());  
			
			logsServices.addLog(AppConstants.event, AppConstants.tag, "getHistoryTagPositions",
					"Get History Tag Positions api call" , reuestMap);
			
			if ( Utils.isEmptyString(requestMap.getOrDefault("date", "").toString()) ) {
				returnmap.put("status", "error");
				returnmap.put("message", AppConstants.fieldsmissing);

				logsServices.addLog(AppConstants.error, AppConstants.tag, "getHistoryTagPositions",
						AppConstants.fieldsmissing, null);
				return new ResponseEntity<>(returnmap, HttpStatus.OK);
			}
			
			
			if (!Utils.validateDate(requestMap.getOrDefault("date", "").toString())) {
				returnmap.put("status", "error");
				returnmap.put("message", AppConstants.errorDatemsg);

				logsServices.addLog(AppConstants.error, AppConstants.tag, "getHistoryTagPositions",
						AppConstants.errorDatemsg, null);
				return new ResponseEntity<>(returnmap, HttpStatus.OK);
			}
			
			Date date = new SimpleDateFormat("yyyy-MMM-dd").parse(requestMap.getOrDefault("date", "").toString());
			LocationHistory locationHistory = locationHistoryDataRepository.findByDate(date);
			
			if(locationHistory==null) {
				
				returnmap.put("status", "error");
				returnmap.put("message", "No history data available for given date");
				
				logsServices.addLog(AppConstants.error, AppConstants.tag, "getHistoryTagPositions",
						"get tag position details", null);
				
			}else { 
				
				returnmap.put("status", "success");
				returnmap.put("data", locationHistory.getLocationData());
				
				logsServices.addLog(AppConstants.success, AppConstants.tag, "getHistoryTagPositions",
						"get tag History position details", null);
				
			}
  
			return new ResponseEntity<>(returnmap, HttpStatus.OK);
		} catch (Exception e) {
			returnmap.put("status", "error");
			returnmap.put("message", AppConstants.errormsg);
			logsServices.addLog(AppConstants.error, AppConstants.tag, "getHistoryTagPositions",
					AppConstants.errormsg, null);

			return new ResponseEntity<>(returnmap, HttpStatus.OK);
		}

	}

}
