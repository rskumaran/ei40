package com.entappia.ei4o.controllers;

import java.lang.reflect.Type;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.entappia.ei4o.constants.AppConstants;
import com.entappia.ei4o.dbmodels.AssetChrono;
import com.entappia.ei4o.dbmodels.AssetChronoAllocation;
import com.entappia.ei4o.dbmodels.AssetNonChrono;
import com.entappia.ei4o.dbmodels.AssetNonChronoAllocation;
import com.entappia.ei4o.dbmodels.AssetVehicle;
import com.entappia.ei4o.dbmodels.Material;
import com.entappia.ei4o.dbmodels.PathSegments;
import com.entappia.ei4o.dbmodels.SKUAllocation;
import com.entappia.ei4o.dbmodels.Staff;
import com.entappia.ei4o.dbmodels.StaffAllocation;
import com.entappia.ei4o.dbmodels.TagPosition;
import com.entappia.ei4o.dbmodels.Tags;
import com.entappia.ei4o.dbmodels.VehicleAllocation;
import com.entappia.ei4o.dbmodels.Visitor;
import com.entappia.ei4o.dbmodels.VisitorAllocation;
import com.entappia.ei4o.dbmodels.WorkMaterialAllocation;
import com.entappia.ei4o.repository.AssetChronoAllocationRepository;
import com.entappia.ei4o.repository.AssetChronoRepository;
import com.entappia.ei4o.repository.AssetNonChronoAllocationRepository;
import com.entappia.ei4o.repository.AssetNonChronoRepository;
import com.entappia.ei4o.repository.AssetVehicleRepository;
import com.entappia.ei4o.repository.MaterialRepository;
import com.entappia.ei4o.repository.SKUAllocationRepository;
import com.entappia.ei4o.repository.StaffAllocationRepository;
import com.entappia.ei4o.repository.StaffRepository;
import com.entappia.ei4o.repository.TagPositionRepository;
import com.entappia.ei4o.repository.TagsRepository;
import com.entappia.ei4o.repository.VehicleAllocationRepository;
import com.entappia.ei4o.repository.VisitorAllocationRepository;
import com.entappia.ei4o.repository.VisitorRepository;
import com.entappia.ei4o.repository.WarehouseRepository;
import com.entappia.ei4o.repository.WorkMaterialAllocationRepository;
import com.entappia.ei4o.services.LogsServices;
import com.entappia.ei4o.services.QuuppaAPIService;
import com.entappia.ei4o.services.SKUEvent;
import com.entappia.ei4o.utils.BaseController;
import com.entappia.ei4o.utils.Utils;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

@RestController
public class TagsController extends BaseController {

	@Autowired
	TagsRepository tagsRepository;

	@Autowired
	private LogsServices logsServices;

	@Autowired
	private VisitorRepository visitorRepository;

	@Autowired
	private StaffRepository staffRepository;

	@Autowired
	private AssetVehicleRepository assetVehicleRepository;

	@Autowired
	private AssetChronoRepository assetChronoRepository;

	@Autowired
	private AssetNonChronoRepository assetNonChronoRepository;

	@Autowired
	private MaterialRepository materialRepository;

	@Autowired
	private SKUEvent skuEvent;

	@Autowired
	private SKUAllocationRepository skuAllocationRepository;

	@Autowired
	private StaffAllocationRepository staffAllocationRepository;

	@Autowired
	private VisitorAllocationRepository visitorAllocationRepository;

	@Autowired
	private VehicleAllocationRepository vehicleAllocationRepository;

	@Autowired
	private AssetNonChronoAllocationRepository assetNonChronoAllocationRepository;

	@Autowired
	private AssetChronoAllocationRepository assetChronoAllocationRepository;

	@Autowired
	private WorkMaterialAllocationRepository workMaterialAllocationRepository;

	@Autowired
	private QuuppaAPIService quuppaAPIService;

	@Autowired
	private WarehouseRepository warehouseRepository;

	@Autowired
	private TagPositionRepository tagPositionRepository;
	Gson gson = new Gson();
	Type mapType = new TypeToken<Tags>() {
	}.getType();

	@SuppressWarnings("unchecked")
	@RequestMapping(path = { "tag", "tag/{tagId}" }, method = RequestMethod.GET)
	public ResponseEntity<?> getTag(HttpServletRequest request, HttpSession httpSession,
			@PathVariable Optional<String> tagId) throws InterruptedException, ExecutionException {
		Map<String, String> returnmap = new HashMap<>();

		try {

			ResponseEntity<?> responseEntity = checkSessionKey(httpSession, Utils.getCookie(request, "JSESSIONID"));
			if (responseEntity != null) {

				logsServices.addLog(AppConstants.error, AppConstants.tag, "getTag", AppConstants.sessiontimeout, null);
				return responseEntity;
			}

			JSONObject jsonObject = new JSONObject();
			if (tagId.isPresent()) {
				jsonObject.put("tagId", tagId.get());
			} else {
				jsonObject.put("tagId", "all");
			}
			Map<String, String> reuestMap = new HashMap<>();
			reuestMap.put("params", jsonObject.toString());

			logsServices.addLog(AppConstants.event, AppConstants.tag, "getTag", "Get Tag api call", reuestMap);

			List<Map<String, Object>> tagDetails = new ArrayList<>();
			ObjectMapper oMapper = new ObjectMapper();

			if (tagId.isPresent()) {
				Tags tagDetails1 = tagsRepository.findByTagId(tagId.get());
				if (tagDetails1 == null) {
					returnmap.put("status", "error");
					returnmap.put("message", "Tag ID does not exist");
					logsServices.addLog(AppConstants.error, AppConstants.tag, "getTag", "Tag ID does not exist", null);

					return new ResponseEntity<>(returnmap, HttpStatus.OK);
				}

				TagPosition tagPosition = tagPositionRepository.findByMacId(tagDetails1.getMacId());
				Date fromTime = null;
				Date toTime = null;

				List<String> accessLimit = null;
				if (!Utils.isEmptyString(tagDetails1.getStatus())
						&& tagDetails1.getStatus().equalsIgnoreCase("Assigned")
						&& !Utils.isEmptyString(tagDetails1.getType())
						&& (tagDetails1.getType().equalsIgnoreCase("Visitor")
								|| tagDetails1.getType().equalsIgnoreCase("Contractor")
								|| tagDetails1.getType().equalsIgnoreCase("Vendor"))
						&& !Utils.isEmptyString(tagDetails1.getAssignmentId())) {
					Visitor visitor = visitorRepository.findByVisitorId(Integer.valueOf(tagDetails1.getAssignmentId()));
					if (visitor != null) {
						accessLimit = visitor.getAccessLimit();
						fromTime = visitor.getFromTime();
						toTime = visitor.getToTime();
					}
				}

				Map<String, Object> map = oMapper.convertValue(tagDetails1, Map.class);

				map.put("accessLimit", accessLimit);
				map.put("fromTime", fromTime);
				map.put("toTime", toTime);
				if (tagPosition != null)
					map.put("last_seen", "" + tagPosition.getLastSeen());
				else
					map.put("last_seen", "");

				tagDetails.add(map);
			} else {

				HashMap<String, String> mapTagPostion = new HashMap<>();
				List<TagPosition> tagPositions = tagPositionRepository.findAll();
				tagPositions.forEach(element -> {
					mapTagPostion.put(element.getMacId(), "" + element.getLastSeen());
				});
				List<Tags> tagList = tagsRepository.findAll();
				if (tagList != null) {
					for (Tags tags : tagList) {

						Date fromTime = null;
						Date toTime = null;
						List<String> accessLimit = null;
						if (!Utils.isEmptyString(tags.getStatus()) && tags.getStatus().equalsIgnoreCase("Assigned")
								&& !Utils.isEmptyString(tags.getType())
								&& (tags.getType().equalsIgnoreCase("Visitor")
										|| tags.getType().equalsIgnoreCase("Contractor")
										|| tags.getType().equalsIgnoreCase("Vendor"))
								&& !Utils.isEmptyString(tags.getAssignmentId())) {
							Visitor visitor = visitorRepository
									.findByVisitorId(Integer.valueOf(tags.getAssignmentId()));
							if (visitor != null) {
								accessLimit = visitor.getAccessLimit();
								fromTime = visitor.getFromTime();
								toTime = visitor.getToTime();
							}
						}

						Map<String, Object> map = oMapper.convertValue(tags, Map.class);
						map.put("accessLimit", accessLimit);
						map.put("fromTime", fromTime);
						map.put("toTime", toTime);

						if (mapTagPostion.containsKey(tags.getMacId()))
							map.put("last_seen", "" + mapTagPostion.get(tags.getMacId()));
						else
							map.put("last_seen", "");

						tagDetails.add(map);
					}
				}
			}

			return new ResponseEntity<>(tagDetails, HttpStatus.OK);
		} catch (Exception e) {
			returnmap.put("status", "error");
			returnmap.put("message", AppConstants.errormsg);
			logsServices.addLog(AppConstants.error, AppConstants.tag, "getTag", AppConstants.errormsg, null);

			return new ResponseEntity<>(returnmap, HttpStatus.OK);
		}
	}

	@PostMapping(value = "/tag")
	public ResponseEntity<?> AddTag(HttpServletRequest request, HttpSession httpSession,

			@RequestBody Tags tagDetails) throws InterruptedException, ExecutionException {
		Map<String, String> returnmap = new HashMap<>();

		ResponseEntity<?> responseEntity = checkSessionKey(httpSession, Utils.getCookie(request, "JSESSIONID"));
		if (responseEntity != null) {

			logsServices.addLog(AppConstants.error, AppConstants.tag, "AddTag", AppConstants.sessiontimeout, null);
			return responseEntity;
		}

		Map<String, String> reuestMap = new HashMap<>();
		reuestMap.put("params", gson.toJson(tagDetails, mapType));

		logsServices.addLog(AppConstants.event, AppConstants.tag, "AddTag", "Add Tag api call", reuestMap);

		try {
			HashMap<String, Object> mapTags = new HashMap<>();
			if (tagDetails != null) {
				mapTags.put("macAddress", tagDetails.getMacId());
				mapTags.put("manufacturer", tagDetails.getManufacturer());
				mapTags.put("GroupId", tagDetails.getGroupId());
				mapTags.put("major", tagDetails.getMajor());
				mapTags.put("minor", tagDetails.getMinor());

			}

			if (Utils.isEmptyString(tagDetails.getGroupId()) || Utils.isEmptyString(tagDetails.getMacId())
					|| Utils.isEmptyString(tagDetails.getManufacturer())) {

				returnmap.put("status", "error");
				returnmap.put("message", AppConstants.fieldsmissing);

				logsServices.addLog(AppConstants.error, AppConstants.tag, "AddTag", AppConstants.fieldsmissing, null);
				return new ResponseEntity<>(returnmap, HttpStatus.OK);

			}

			Tags tagDetail = tagsRepository.findByMacId(tagDetails.getMacId());

			if (tagDetail != null) {
				returnmap.put("status", "error");
				returnmap.put("message", "Mac ID already exist");

				logsServices.addLog(AppConstants.error, AppConstants.tag, "AddTag", "Mac ID already exist", null);
				return new ResponseEntity<>(returnmap, HttpStatus.OK);
			}

			tagDetails.setAssignmentId("");
			tagDetails.setAssignmentDate(null);
			tagDetails.setStatus("Free");
			tagDetails.setType("");

			tagDetails.setCreatedDate(new Date());
			tagDetails.setModifiedDate(new Date());
			tagDetails.setRestart(true);
			tagDetails.setRenamed(false);
			tagDetails.setMajor(0);
			tagDetails.setMinor(0);
			tagsRepository.save(tagDetails);

			// preference.setLongValue(organization + "_" +
			// AppConstants.TAG_LAST_MODIFIED_TIME, currTimeMills);

			returnmap.put("status", "success");
			returnmap.put("message", tagDetails.getMacId() + " added");

			logsServices.addLog(AppConstants.event, AppConstants.tag, "AddTag",
					"Tag-" + tagDetails.getMacId() + " added successfully, ", null);

			return new ResponseEntity<>(returnmap, HttpStatus.OK);
		} catch (Exception e) {
			returnmap.put("status", "error");
			returnmap.put("message", AppConstants.errormsg);

			logsServices.addLog(AppConstants.error, AppConstants.tag, "AddTag", AppConstants.errormsg, null);
			return new ResponseEntity<>(returnmap, HttpStatus.OK);
		}
	}

	@PutMapping(value = "/tag")
	public ResponseEntity<?> updateTag(HttpServletRequest request, HttpSession httpSession,

			@RequestBody Tags tagDetails) throws InterruptedException, ExecutionException {
		Map<String, String> returnmap = new HashMap<>();

		ResponseEntity<?> responseEntity = checkSessionKey(httpSession, Utils.getCookie(request, "JSESSIONID"));
		if (responseEntity != null) {

			logsServices.addLog(AppConstants.error, AppConstants.tag, "updateTag", AppConstants.sessiontimeout, null);
			return responseEntity;
		}

		Map<String, String> reuestMap = new HashMap<>();
		reuestMap.put("params", gson.toJson(tagDetails, mapType));

		logsServices.addLog(AppConstants.event, AppConstants.tag, "updateTag", "Update Tag api call", reuestMap);

		try {
			HashMap<String, Object> mapTags = new HashMap<>();
			if (tagDetails != null) {
				mapTags.put("macAddress", tagDetails.getMacId());
				mapTags.put("manufacturer", tagDetails.getManufacturer());
				mapTags.put("GroupId", tagDetails.getGroupId());
				mapTags.put("major", tagDetails.getMajor());
				mapTags.put("minor", tagDetails.getMinor());

			}

			if (Utils.isEmptyString(tagDetails.getGroupId()) || Utils.isEmptyString(tagDetails.getMacId())
					|| Utils.isEmptyString(tagDetails.getManufacturer())) {

				returnmap.put("status", "error");
				returnmap.put("message", AppConstants.fieldsmissing);

				logsServices.addLog(AppConstants.error, AppConstants.tag, "updateTag", AppConstants.fieldsmissing,
						null);
				return new ResponseEntity<>(returnmap, HttpStatus.OK);

			}

			Tags tags = tagsRepository.findByMacId(tagDetails.getMacId());

			if (tags == null) {
				returnmap.put("status", "error");
				returnmap.put("message", "Mac ID not found");

				logsServices.addLog(AppConstants.error, AppConstants.tag, "updateTag", "Mac ID not found", null);
				return new ResponseEntity<>(returnmap, HttpStatus.OK);
			}

			tags.setManufacturer(tagDetails.getManufacturer());
			tags.setGroupId(tagDetails.getGroupId());
			tags.setMajor(tagDetails.getMajor());
			tags.setMinor(tagDetails.getMinor());
			tags.setModifiedDate(new Date());
			tagsRepository.save(tags);

			// preference.setLongValue(organization + "_" +
			// AppConstants.TAG_LAST_MODIFIED_TIME, currTimeMills);

			returnmap.put("status", "success");
			returnmap.put("message", tagDetails.getMacId() + " added");

			logsServices.addLog(AppConstants.event, AppConstants.tag, "AddTag",
					"Tag-" + tagDetails.getMacId() + " added successfully, ", null);

			return new ResponseEntity<>(returnmap, HttpStatus.OK);
		} catch (Exception e) {
			returnmap.put("status", "error");
			returnmap.put("message", AppConstants.errormsg);

			logsServices.addLog(AppConstants.error, AppConstants.tag, "AddTag", AppConstants.errormsg, null);
			return new ResponseEntity<>(returnmap, HttpStatus.OK);
		}
	}

	@GetMapping(value = "/getTagsCount")
	public ResponseEntity<?> getTagsCount(HttpServletRequest request, HttpSession httpSession)
			throws InterruptedException, ExecutionException {
		Map<String, Object> returnmap = new HashMap<>();

		try {

			ResponseEntity<?> responseEntity = checkSessionKey(httpSession, Utils.getCookie(request, "JSESSIONID"));
			if (responseEntity != null) {
				logsServices.addLog(AppConstants.error, AppConstants.tag, "getTagBatteryDetails",
						AppConstants.sessiontimeout, null);
				return responseEntity;
			}

			HashMap<String, Object> tagsCountDetails = new HashMap<>();
			List<Tags> tagsList = tagsRepository.findAll();
			Calendar calendar = Calendar.getInstance();
			calendar.set(Calendar.MINUTE, -5);

			long timeInMillis = calendar.getTimeInMillis();

			List<TagPosition> tagPositionListFromDB = tagPositionRepository.findAll();
			List<String> findedMacIdList = new ArrayList<String>();
			for (int i = 0; i < tagPositionListFromDB.size(); i++) {

				TagPosition tagPosition = tagPositionListFromDB.get(i);
				if (tagPosition.getLastSeen() > timeInMillis) {
					findedMacIdList.add(tagPosition.getMacId());
				}
			}

			int totalTags = 0;
			int onlineTags = 0;
			int missingTags = 0;
			int batteryOffTags = 0;

			if (tagsList != null) {

				for (Tags tag : tagsList) {
					if (tag != null) {
						if (!tag.isBatteryOn()) {
							batteryOffTags++;
						} else {
							totalTags++;
							if (findedMacIdList.contains(tag.getMacId())) {
								onlineTags++;
							} else
								missingTags++;
						}
					}
				}

			}

			tagsCountDetails.put("total", totalTags);
			tagsCountDetails.put("online", onlineTags);
			tagsCountDetails.put("missing", missingTags);
			tagsCountDetails.put("batteryOff", batteryOffTags);

			returnmap.put("status", "success");
			returnmap.put("tags", tagsCountDetails);
			return new ResponseEntity<>(returnmap, HttpStatus.OK);
		} catch (Exception e) {
			returnmap.put("status", "error");
			returnmap.put("message", AppConstants.errormsg);

			logsServices.addLog(AppConstants.error, AppConstants.tag, "getTagsCount", AppConstants.errormsg, null);
			return new ResponseEntity<>(returnmap, HttpStatus.OK);
		}

	}

	@GetMapping(value = "/getTagBatteryDetails")
	public ResponseEntity<?> getTagBatteryDetails(HttpServletRequest request, HttpSession httpSession)
			throws InterruptedException, ExecutionException {
		Map<String, Object> returnmap = new HashMap<>();

		try {

			ResponseEntity<?> responseEntity = checkSessionKey(httpSession, Utils.getCookie(request, "JSESSIONID"),
					"CTBI");
			if (responseEntity != null) {
				logsServices.addLog(AppConstants.error, AppConstants.tag, "getTagBatteryDetails",
						AppConstants.sessiontimeout, null);
				return responseEntity;
			}

			logsServices.addLog(AppConstants.event, AppConstants.tag, "getTagBatteryDetails",
					"Get Tag BatteryDetails api call", null);

			List<HashMap<String, Object>> tagBatteryDetails = new ArrayList<>();
			CompletableFuture<HashMap<String, HashMap<String, Object>>> batteryStatusInfoCompletableFuture = quuppaAPIService
					.getBatteryInfo();
			CompletableFuture.allOf(batteryStatusInfoCompletableFuture).join();
			if (batteryStatusInfoCompletableFuture.isDone()) {

				HashMap<String, HashMap<String, Object>> batteryInfoMap = batteryStatusInfoCompletableFuture.get();
				List<Tags> tagList = tagsRepository.findAll();

				for (Tags tags : tagList) {

					HashMap<String, Object> batteryInfo = batteryInfoMap.getOrDefault(tags.getMacId(), null);

					HashMap<String, Object> tagBatteryInfo = new HashMap<>();

					tagBatteryInfo.put("tagId", tags.getTagId());
					tagBatteryInfo.put("macId", tags.getMacId());
					tagBatteryInfo.put("status", tags.getStatus());

					if (tags.getStatus().equalsIgnoreCase("assigned")) {
						tagBatteryInfo.put("type", tags.getType());
						tagBatteryInfo.put("subType", tags.getSubType());
						tagBatteryInfo.put("assignmentId", tags.getAssignmentId());
					} else {

						tagBatteryInfo.put("type", "");
						tagBatteryInfo.put("subType", "");
						tagBatteryInfo.put("assignmentId", "");
					}

					if (batteryInfo != null) {
						tagBatteryInfo.put("batteryAlarm", batteryInfo.get("batteryAlarm"));
						tagBatteryInfo.put("batteryVoltage", batteryInfo.get("batteryVoltage"));
						tagBatteryInfo.put("replaceStatus", batteryInfo.get("replaceStatus"));
					} else {

						tagBatteryInfo.put("batteryAlarm", "");
						tagBatteryInfo.put("batteryVoltage", 0);
						tagBatteryInfo.put("replaceStatus", "Yes");
					}

					tagBatteryDetails.add(tagBatteryInfo);

				}
			}

			logsServices.addLog(AppConstants.event, AppConstants.tag, "getTagBatteryDetails",
					"Battery Info sent successfully, ", null);
			returnmap.put("status", "success");
			returnmap.put("batteryInfo", tagBatteryDetails);
			return new ResponseEntity<>(returnmap, HttpStatus.OK);
		} catch (Exception e) {
			returnmap.put("status", "error");
			returnmap.put("message", AppConstants.errormsg);

			logsServices.addLog(AppConstants.error, AppConstants.tag, "getTagBatteryDetails", AppConstants.errormsg,
					null);
			return new ResponseEntity<>(returnmap, HttpStatus.OK);
		}

	}

	@PostMapping(value = "/updateTagStatus")
	public ResponseEntity<?> updateTagStatus(HttpServletRequest request, HttpSession httpSession,
			@RequestBody HashMap<String, Object> requestMap) throws InterruptedException, ExecutionException {

		Map<String, String> returnmap = new HashMap<>();
		try {

			String type = requestMap.getOrDefault("type", "").toString();

			String code = "";
			if (Utils.isEmptyString(type)) {

				returnmap.put("status", "error");
				returnmap.put("message", "Missing input params");

				logsServices.addLog(AppConstants.error, AppConstants.tag, "updateTagStatus", returnmap.get("message"),
						null);
				return new ResponseEntity<>(returnmap, HttpStatus.OK);

			} else {
				if (type.equalsIgnoreCase("tagPeople")) {
					code = "PTED";
				} else if (type.equalsIgnoreCase("tagAsset")) {
					code = "ATED";
				} else if (type.equalsIgnoreCase("tagWorkorder")) {
					code = "WTED";
				} else if (type.equalsIgnoreCase("tagSKU")) {
					code = "STED";
				} else if (type.equalsIgnoreCase("tagWarehouse")) {
					code = "HTED";
				} else {
					returnmap.put("status", "error");
					returnmap.put("message", "Invalid type");

					logsServices.addLog(AppConstants.error, AppConstants.tag, "updateTagStatus",
							returnmap.get("message"), null);
					return new ResponseEntity<>(returnmap, HttpStatus.OK);
				}
			}

			ResponseEntity<?> responseEntity = checkSessionKey(httpSession, Utils.getCookie(request, "JSESSIONID"),
					code);
			if (responseEntity != null) {
				logsServices.addLog(AppConstants.error, AppConstants.tag, "updateTagStatus",
						AppConstants.sessiontimeout, null);
				return responseEntity;
			}

			Map<String, String> reuestMap = new HashMap<>();
			reuestMap.put("params", new JSONObject(requestMap).toString());

			logsServices.addLog(AppConstants.event, AppConstants.tag, "updateTagStatus", "update Tag Status api call",
					reuestMap);

			if (Utils.isEmptyString(requestMap.get("tagId").toString())
					|| Utils.isEmptyString(requestMap.get("status").toString())) {

				returnmap.put("status", "error");
				returnmap.put("message", "Missing input params");

				logsServices.addLog(AppConstants.error, AppConstants.tag, "updateTagStatus", returnmap.get("message"),
						null);
				return new ResponseEntity<>(returnmap, HttpStatus.OK);

			}

			Tags tags = tagsRepository.findByTagId(requestMap.get("tagId").toString());

			if (tags == null) {
				returnmap.put("status", "error");
				returnmap.put("message", "Tag Id not found");

				logsServices.addLog(AppConstants.error, AppConstants.tag, "updateTagStatus", "Tag Id not found", null);
				return new ResponseEntity<>(returnmap, HttpStatus.OK);
			}

			String status = requestMap.get("status").toString();

			if (tags.getStatus().equalsIgnoreCase("free")) {

				if (status.equals("Lost") || status.equals("Not Working") || status.equals("Expired")) {
					tags.setStatus(status);
					tags.setModifiedDate(new Date());
					if (status.equals("Lost"))
						tags.setRestart(false);

					tagsRepository.save(tags);

					returnmap.put("status", "success");
					returnmap.put("message", "Status updated successfully.");

					logsServices.addLog(AppConstants.event, AppConstants.tag, "updateTagStatus",
							returnmap.get("message"), null);
					return new ResponseEntity<>(returnmap, HttpStatus.OK);

				} else {
					returnmap.put("status", "error");
					returnmap.put("message", "Invalid status.");

					logsServices.addLog(AppConstants.error, AppConstants.tag, "updateTagStatus",
							returnmap.get("message"), null);
					return new ResponseEntity<>(returnmap, HttpStatus.OK);
				}

			} else if (tags.getStatus().equalsIgnoreCase("Expired")
					|| tags.getStatus().equalsIgnoreCase("Not Working")) {
				returnmap.put("status", "error");
				returnmap.put("message", "Status can not be change for " + tags.getStatus() + " tag.");

				logsServices.addLog(AppConstants.error, AppConstants.tag, "updateTagStatus", returnmap.get("message"),
						null);
				return new ResponseEntity<>(returnmap, HttpStatus.OK);
			} else if (tags.getStatus().equalsIgnoreCase("Overdue")) {

				if (status.equals("Lost") || status.equals("Not Working") || status.equals("Blocked")) {
					tags.setStatus(status);
					tags.setModifiedDate(new Date());
					if (status.equals("Lost"))
						tags.setRestart(false);

					if (status.equals("Lost") || status.equals("Not Working")) {
						if (!Utils.isEmptyString(tags.getType()) && !Utils.isEmptyString(tags.getMacId())
								&& !Utils.isEmptyString(tags.getAssignmentId()))
							updateAllocationTable(tags.getType(), tags.getMacId(), tags.getAssignmentId());
					}

					tagsRepository.save(tags);

					returnmap.put("status", "success");
					returnmap.put("message", "Status updated successfully.");

					logsServices.addLog(AppConstants.event, AppConstants.tag, "updateTagStatus",
							returnmap.get("message"), null);
					return new ResponseEntity<>(returnmap, HttpStatus.OK);

				} else {
					returnmap.put("status", "error");
					returnmap.put("message", "Invalid status.");

					logsServices.addLog(AppConstants.error, AppConstants.tag, "updateTagStatus",
							returnmap.get("message"), null);
					return new ResponseEntity<>(returnmap, HttpStatus.OK);
				}
			} else if (tags.getStatus().equalsIgnoreCase("Blocked")) {

				if (status.equals("Lost") || status.equals("Not Working") || status.equals("Assigned")) {
					tags.setStatus(status);
					tags.setModifiedDate(new Date());
					if (status.equals("Lost"))
						tags.setRestart(false);

					if (status.equals("Lost") || status.equals("Not Working")) {
						if (!Utils.isEmptyString(tags.getType()) && !Utils.isEmptyString(tags.getMacId())
								&& !Utils.isEmptyString(tags.getAssignmentId()))
							updateAllocationTable(tags.getType(), tags.getMacId(), tags.getAssignmentId());
					}

					tagsRepository.save(tags);

					returnmap.put("status", "success");
					returnmap.put("message", "Status updated successfully.");

					logsServices.addLog(AppConstants.event, AppConstants.tag, "updateTagStatus",
							returnmap.get("message"), null);
					return new ResponseEntity<>(returnmap, HttpStatus.OK);

				} else {
					returnmap.put("status", "error");
					returnmap.put("message", "Invalid status.");

					logsServices.addLog(AppConstants.error, AppConstants.tag, "updateTagStatus",
							returnmap.get("message"), null);
					return new ResponseEntity<>(returnmap, HttpStatus.OK);
				}
			} else if (tags.getStatus().equalsIgnoreCase("lost")) {

				if (!tags.isRestart()) {
					returnmap.put("status", "error");
					returnmap.put("message", "Lost tag not found, Can not change lost tag status.");

					logsServices.addLog(AppConstants.error, AppConstants.tag, "updateTagStatus", "Tag Id not found",
							null);
					return new ResponseEntity<>(returnmap, HttpStatus.OK);
				} else if (status.equals("Free")) {

					if (!Utils.isEmptyString(tags.getType()) && !Utils.isEmptyString(tags.getMacId())
							&& !Utils.isEmptyString(tags.getAssignmentId()))
						updateAllocationTable(tags.getType(), tags.getMacId(), tags.getAssignmentId());

					tags.setStatus(status);
					tags.setModifiedDate(new Date());

					tags.setAssignmentDate(null);
					tags.setAssignmentId("");
					tags.setType("");
					tags.setSubType("");
					tags.setRestart(true);

					tagsRepository.save(tags);

					returnmap.put("status", "success");
					returnmap.put("message", "Status updated successfully.");

					logsServices.addLog(AppConstants.event, AppConstants.tag, "updateTagStatus",
							returnmap.get("message"), null);
					return new ResponseEntity<>(returnmap, HttpStatus.OK);

				} else {
					returnmap.put("status", "error");
					returnmap.put("message", "Invalid status.");

					logsServices.addLog(AppConstants.error, AppConstants.tag, "updateTagStatus",
							returnmap.get("message"), null);
					return new ResponseEntity<>(returnmap, HttpStatus.OK);
				}
			} else if (tags.getStatus().equalsIgnoreCase("Assigned")) {

				if (status.equals("Lost") || status.equals("Not Working") || status.equals("Overdue")
						|| status.equals("Blocked") || status.equals("Expired")) {
					tags.setStatus(status);
					tags.setModifiedDate(new Date());

					if (status.equals("Lost"))
						tags.setRestart(false);

					if (status.equals("Lost") || status.equals("Not Working") || status.equals("Expired")) {
						if (!Utils.isEmptyString(tags.getType()) && !Utils.isEmptyString(tags.getMacId())
								&& !Utils.isEmptyString(tags.getAssignmentId()))
							updateAllocationTable(tags.getType(), tags.getMacId(), tags.getAssignmentId());
					}

					tagsRepository.save(tags);

					returnmap.put("status", "success");
					returnmap.put("message", "Status updated successfully.");

					logsServices.addLog(AppConstants.event, AppConstants.tag, "updateTagStatus",
							returnmap.get("message"), null);
					return new ResponseEntity<>(returnmap, HttpStatus.OK);

				} else {
					returnmap.put("status", "error");
					returnmap.put("message", "Invalid status.");

					logsServices.addLog(AppConstants.error, AppConstants.tag, "updateTagStatus",
							returnmap.get("message"), null);
					return new ResponseEntity<>(returnmap, HttpStatus.OK);
				}
			}

			return new ResponseEntity<>(returnmap, HttpStatus.OK);
		} catch (Exception e) {
			returnmap.put("status", "error");
			returnmap.put("message", AppConstants.errormsg);

			logsServices.addLog(AppConstants.error, AppConstants.tag, "AddTag", AppConstants.errormsg, null);
			return new ResponseEntity<>(returnmap, HttpStatus.OK);
		}
	}

	public void updateAllocationTable(String sType, String macId, String userId) {

		if (sType.equalsIgnoreCase("Employee")) {

			StaffAllocation staffAllocation = staffAllocationRepository.findByStaffIdAndMacId(userId, macId);

			if (staffAllocation != null) {
				staffAllocation.setStatus("Completed");
				staffAllocation.setAllocationEndTime(new Date());
				staffAllocationRepository.save(staffAllocation);
			}
		} else if (sType.equalsIgnoreCase("visitor") || sType.equalsIgnoreCase("Contractor")
				|| sType.equalsIgnoreCase("Vendor")) {

			VisitorAllocation visitorAllocation = visitorAllocationRepository
					.findByVisitorIdAndMacId(Integer.valueOf(userId), macId);

			if (visitorAllocation != null) {
				visitorAllocation.setStatus("Completed");
				visitorAllocation.setAllocationEndTime(new Date());
				visitorAllocationRepository.save(visitorAllocation);

			}
		} else if (sType.equalsIgnoreCase("visitor")) {

			VisitorAllocation visitorAllocation = visitorAllocationRepository
					.findByVisitorIdAndMacId(Integer.valueOf(userId), macId);

			if (visitorAllocation != null) {
				visitorAllocation.setStatus("Completed");
				visitorAllocation.setAllocationEndTime(new Date());
				visitorAllocationRepository.save(visitorAllocation);

			}
		} else if (sType.equalsIgnoreCase("Materials")) {

			WorkMaterialAllocation workMaterialAllocation = workMaterialAllocationRepository
					.findByOrderNoAndMacId(Integer.valueOf(userId), macId);

			if (workMaterialAllocation != null) {
				workMaterialAllocation.setStatus("Completed");
				workMaterialAllocation.setAllocationEndTime(new Date());
				workMaterialAllocationRepository.save(workMaterialAllocation);
			}
		} else if (sType.equalsIgnoreCase("Vehicles")) {

			VehicleAllocation vehicleAllocation = vehicleAllocationRepository.findByVehicleIdAndMacId(userId, macId);

			if (vehicleAllocation != null) {
				vehicleAllocation.setStatus("Completed");
				vehicleAllocation.setAllocationEndTime(new Date());
				vehicleAllocationRepository.save(vehicleAllocation);
			}
		} else if (sType.equalsIgnoreCase("Asset NonChrono")) {

			AssetNonChronoAllocation assetNonChronoAllocation = assetNonChronoAllocationRepository
					.findByAssetIdAndMacId(userId, macId);

			if (assetNonChronoAllocation != null) {
				assetNonChronoAllocation.setStatus("Completed");
				assetNonChronoAllocation.setAllocationEndTime(new Date());
				assetNonChronoAllocationRepository.save(assetNonChronoAllocation);
			}
		} else if (sType.equalsIgnoreCase("Asset Chrono")) {

			AssetChronoAllocation assetChronoAllocation = assetChronoAllocationRepository.findByAssetIdAndMacId(userId,
					macId);

			if (assetChronoAllocation != null && assetChronoAllocation.getStatus().equalsIgnoreCase("Valid")) {
				assetChronoAllocation.setStatus("Completed");
				assetChronoAllocation.setAllocationEndTime(new Date());
				assetChronoAllocationRepository.save(assetChronoAllocation);
			}
		}

	}

	@PostMapping(value = "/renameTag")
	public ResponseEntity<?> renameTag(HttpServletRequest request, HttpSession httpSession,
			@RequestBody HashMap<String, Object> requestMap) throws InterruptedException, ExecutionException {

		Map<String, String> returnmap = new HashMap<>();
		try {

			ResponseEntity<?> responseEntity = checkSessionKey(httpSession, Utils.getCookie(request, "JSESSIONID"),
					"CTRN");
			if (responseEntity != null) {
				logsServices.addLog(AppConstants.error, AppConstants.tag, "renameTag", AppConstants.sessiontimeout,
						null);
				return responseEntity;
			}

			Map<String, String> reuestMap = new HashMap<>();
			reuestMap.put("params", new JSONObject(requestMap).toString());

			logsServices.addLog(AppConstants.event, AppConstants.tag, "renameTag", "Rename Tag api call", reuestMap);

			HashMap<String, Object> mapTags = new HashMap<>();
			if (requestMap != null) {
				mapTags.put("macId", requestMap.get("macId"));
				mapTags.put("tagId", requestMap.get("tagId"));

			}

			if (Utils.isEmptyString(requestMap.getOrDefault("macId", "").toString())
					|| Utils.isEmptyString(requestMap.getOrDefault("tagId", "").toString())) {

				returnmap.put("status", "error");
				returnmap.put("message", AppConstants.fieldsmissing);

				logsServices.addLog(AppConstants.error, AppConstants.tag, "renameTag", AppConstants.fieldsmissing,
						null);
				return new ResponseEntity<>(returnmap, HttpStatus.OK);

			}

			Tags tags = tagsRepository.findByMacId(requestMap.getOrDefault("macId", "").toString());

			if (tags == null) {
				returnmap.put("status", "error");
				returnmap.put("message", "Mac ID not found");

				logsServices.addLog(AppConstants.error, AppConstants.tag, "renameTag", "Mac ID not found", null);
				return new ResponseEntity<>(returnmap, HttpStatus.OK);
			} else if (tags.isRenamed()) {
				returnmap.put("status", "error");
				returnmap.put("message", "Tag id already updated.");

				logsServices.addLog(AppConstants.error, AppConstants.tag, "renameTag", returnmap.get("message"), null);
				return new ResponseEntity<>(returnmap, HttpStatus.OK);
			} else if (!tags.getStatus().equalsIgnoreCase("free")) {
				returnmap.put("status", "error");
				returnmap.put("message", "Can not rename " + tags.getStatus() + " tag.");

				logsServices.addLog(AppConstants.error, AppConstants.tag, "renameTag", returnmap.get("message"), null);
				return new ResponseEntity<>(returnmap, HttpStatus.OK);
			}

			Tags checkTag = tagsRepository.findByTagId(requestMap.getOrDefault("tagId", "").toString());
			if (checkTag != null) {
				returnmap.put("status", "error");
				returnmap.put("message", "Tag id already exist.");

				logsServices.addLog(AppConstants.error, AppConstants.tag, "renameTag", returnmap.get("message"), null);
				return new ResponseEntity<>(returnmap, HttpStatus.OK);
			}

			tags.setModifiedDate(new Date());
			tags.setRenamed(true);
			tags.setTagId(requestMap.getOrDefault("tagId", "").toString());
			tagsRepository.save(tags);

			// preference.setLongValue(organization + "_" +
			// AppConstants.TAG_LAST_MODIFIED_TIME, currTimeMills);

			returnmap.put("status", "success");
			returnmap.put("message", tags.getMacId() + " tag id updated succesfully.");

			logsServices.addLog(AppConstants.event, AppConstants.tag, "AddTag",
					"Tag-" + tags.getMacId() + " tag id updated successfully, ", null);

			return new ResponseEntity<>(returnmap, HttpStatus.OK);
		} catch (Exception e) {
			returnmap.put("status", "error");
			returnmap.put("message", AppConstants.errormsg);

			logsServices.addLog(AppConstants.error, AppConstants.tag, "renameTag", AppConstants.errormsg, null);
			return new ResponseEntity<>(returnmap, HttpStatus.OK);
		}
	}

	@GetMapping(value = "/getAssignedTagDetails")
	public ResponseEntity<?> getAssignedTagDetails(HttpServletRequest request, HttpSession httpSession)
			throws InterruptedException, ExecutionException {

		Map<String, Object> returnmap = new HashMap<>();

		try {

			ResponseEntity<?> responseEntity = checkSessionKey(httpSession, Utils.getCookie(request, "JSESSIONID"));
			if (responseEntity != null) {
				logsServices.addLog(AppConstants.error, AppConstants.tag, "AssignedTagDetails",
						AppConstants.sessiontimeout, null);
				return responseEntity;
			}

			HashMap<String, HashMap<String, String>> assignedIdDetails = new HashMap<>();

			List<Tags> tagList = tagsRepository.findByStatus("Assigned");
			if (tagList != null && tagList.size() > 0) {

				for (Tags tag : tagList) {

					if (tag.getType().equalsIgnoreCase("Visitor") || tag.getType().equalsIgnoreCase("Vendor")
							|| tag.getType().equalsIgnoreCase("Contractor")) {

						if (!Utils.isEmptyString(tag.getAssignmentId())) {
							Visitor visitor = visitorRepository.findByVisitorId(Long.valueOf(tag.getAssignmentId()));
							if (visitor != null) {
								HashMap<String, String> map = new HashMap<>();
								map.put("type", tag.getType());
								map.put("firstName", visitor.getFirstName());
								map.put("lastName", visitor.getLastName());
								assignedIdDetails.put(tag.getMacId(), map);

							}
						}
					} else if (tag.getType().equalsIgnoreCase("Employee")) {

						if (!Utils.isEmptyString(tag.getAssignmentId())) {
							Staff staff = staffRepository.findByEmpId(tag.getAssignmentId());
							if (staff != null) {
								HashMap<String, String> map = new HashMap<>();
								map.put("type", tag.getType());
								map.put("firstName", staff.getFirstName());
								map.put("lastName", staff.getLastName());
								assignedIdDetails.put(tag.getMacId(), map);

							}
						}
					} else if (tag.getType().equalsIgnoreCase("Vehicles")) {

						if (!Utils.isEmptyString(tag.getAssignmentId())) {
							AssetVehicle assetVehicle = assetVehicleRepository.findByAssetId(tag.getAssignmentId());
							if (assetVehicle != null) {
								HashMap<String, String> map = new HashMap<>();
								map.put("type", tag.getType());
								map.put("assetType", assetVehicle.getAssetType());
								map.put("subType", assetVehicle.getSubType());
								assignedIdDetails.put(tag.getMacId(), map);

							}
						}
					} else if (tag.getType().equalsIgnoreCase("Asset Chrono")) {

						if (!Utils.isEmptyString(tag.getAssignmentId())) {
							AssetChrono assetChrono = assetChronoRepository.findByAssetId(tag.getAssignmentId());
							if (assetChrono != null) {
								HashMap<String, String> map = new HashMap<>();
								map.put("type", tag.getSubType());
								map.put("assetType", assetChrono.getAssetType());
								map.put("subType", assetChrono.getSubType());
								assignedIdDetails.put(tag.getMacId(), map);

							}
						}
					} else if (tag.getType().equalsIgnoreCase("Asset NonChrono")) {

						if (!Utils.isEmptyString(tag.getAssignmentId())) {
							AssetNonChrono assetNonChrono = assetNonChronoRepository
									.findByAssetId(tag.getAssignmentId());
							if (assetNonChrono != null) {
								HashMap<String, String> map = new HashMap<>();
								map.put("type", tag.getSubType());
								map.put("assetType", assetNonChrono.getAssetType());
								map.put("subType", assetNonChrono.getSubType());
								assignedIdDetails.put(tag.getMacId(), map);

							}
						}
					} else if (tag.getType().equalsIgnoreCase("Asset NonChrono")) {

						if (!Utils.isEmptyString(tag.getAssignmentId())) {
							AssetNonChrono assetNonChrono = assetNonChronoRepository
									.findByAssetId(tag.getAssignmentId());
							if (assetNonChrono != null) {
								HashMap<String, String> map = new HashMap<>();
								map.put("type", tag.getSubType());
								map.put("assetType", assetNonChrono.getAssetType());
								map.put("subType", assetNonChrono.getSubType());
								assignedIdDetails.put(tag.getMacId(), map);

							}
						}
					} else if (tag.getType().equalsIgnoreCase("Materials")) {

						if (!Utils.isEmptyString(tag.getAssignmentId())) {
							List<Material> materialList = materialRepository
									.findByOrdereNo(Integer.valueOf(tag.getAssignmentId()));
							if (materialList != null && materialList.size() > 0) {

								Material material = materialList.get(0);

								HashMap<String, String> map = new HashMap<>();
								map.put("type", tag.getSubType());
								map.put("ordereNo", "" + material.getOrdereNo());
								map.put("destination", material.getDestination());
								map.put("source", material.getSource());
								assignedIdDetails.put(tag.getMacId(), map);

							}
						}
					} else if (tag.getType().equalsIgnoreCase("LSLM") || tag.getType().equalsIgnoreCase("Inventory")) {

						if (!Utils.isEmptyString(tag.getAssignmentId())) {
							SKUAllocation skuAllocation = skuAllocationRepository.findByUPCCode(tag.getAssignmentId());
							if (skuAllocation != null) {

								HashMap<String, String> map = new HashMap<>();
								map.put("type", tag.getType());
								map.put("description", "" + skuAllocation.getDescription());
								map.put("expiryDate", skuAllocation.getExpiryDate() != null
										? new SimpleDateFormat("yyyy-MMM-dd").format(skuAllocation.getExpiryDate())
										: "");
								map.put("upcCode", skuAllocation.getUpcCode());
								assignedIdDetails.put(tag.getMacId(), map);

							}
						}
					} else if (tag.getType().equalsIgnoreCase("WareHouse")) {

						if (!Utils.isEmptyString(tag.getAssignmentId())) {
							HashMap<String, String> map = new HashMap<>();
							map.put("type", tag.getType());
							map.put("skuNo", tag.getAssignmentId());
							assignedIdDetails.put(tag.getMacId(), map);
						}
					}

				}
			}
			returnmap.put("status", "success");
			returnmap.put("data", assignedIdDetails);

			return new ResponseEntity<>(returnmap, HttpStatus.OK);

		} catch (Exception e) {
			returnmap.put("status", "error");
			returnmap.put("message", AppConstants.errormsg);
			e.printStackTrace();
			logsServices.addLog(AppConstants.error, AppConstants.tag, "buzzerAlert", AppConstants.errormsg, null);
			return new ResponseEntity<>(returnmap, HttpStatus.OK);
		}
	}

	@GetMapping(value = "/buzzerAlert/{macIds}")
	public ResponseEntity<?> buzzerAlert(HttpServletRequest request, HttpSession httpSession,
			@PathVariable Optional<String> macIds) throws InterruptedException, ExecutionException {

		Map<String, String> returnmap = new HashMap<>();

		try {

			ResponseEntity<?> responseEntity = checkSessionKey(httpSession, Utils.getCookie(request, "JSESSIONID"));
			if (responseEntity != null) {
				logsServices.addLog(AppConstants.error, AppConstants.tag, "buzzerAlert", AppConstants.sessiontimeout,
						null);
				return responseEntity;
			}

			JSONObject jsonObject = new JSONObject();
			jsonObject.put("macIds", macIds.get());

			Map<String, String> reuestMap = new HashMap<>();
			reuestMap.put("params", jsonObject.toString());

			logsServices.addLog(AppConstants.event, AppConstants.tag, "buzzerAlert", "Buzzer Alert api call",
					reuestMap);

			if (!macIds.isPresent() || Utils.isEmptyString(macIds.get())) {
				returnmap.put("status", "error");
				returnmap.put("message", "Mac Id missing.");

				logsServices.addLog(AppConstants.error, AppConstants.tag, "buzzerAlert", "Mac Id missing.", null);
				return new ResponseEntity<>(returnmap, HttpStatus.OK);
			}

			Tags tagDetails1 = tagsRepository.findByMacId(macIds.get());
			if (tagDetails1 == null) {
				returnmap.put("status", "error");
				returnmap.put("message", "Mac ID does not exist");
				logsServices.addLog(AppConstants.error, AppConstants.tag, "buzzerAlert", "Mac ID does not exist", null);

				return new ResponseEntity<>(returnmap, HttpStatus.OK);
			}

			quuppaAPIService.sentBuzzerAlert(macIds.get());

			returnmap.put("status", "success");
			returnmap.put("message", "Buzzer alert sent successfully");
			logsServices.addLog(AppConstants.error, AppConstants.tag, "buzzerAlert", returnmap.get("message"), null);

			return new ResponseEntity<>(returnmap, HttpStatus.OK);

		} catch (Exception e) {
			returnmap.put("status", "error");
			returnmap.put("message", AppConstants.errormsg);

			logsServices.addLog(AppConstants.error, AppConstants.tag, "buzzerAlert", AppConstants.errormsg, null);
			return new ResponseEntity<>(returnmap, HttpStatus.OK);
		}
	}

	@PostMapping(value = "/checkTag")
	public ResponseEntity<?> checkTag(HttpServletRequest request, HttpSession httpSession,
			@RequestBody HashMap<String, Object> requestMap) throws InterruptedException, ExecutionException {

		Map<String, Object> returnmap = new HashMap<>();
		try {

			ResponseEntity<?> responseEntity = checkSessionKey(httpSession, Utils.getCookie(request, "JSESSIONID"));
			if (responseEntity != null) {
				logsServices.addLog(AppConstants.error, AppConstants.tag, "checkTag", AppConstants.sessiontimeout,
						null);
				return responseEntity;
			}

			JSONObject jsonObject = new JSONObject();
			jsonObject.put("tagId", requestMap.getOrDefault("tagId", "").toString());

			Map<String, String> reuestMap = new HashMap<>();
			reuestMap.put("params", jsonObject.toString());

			logsServices.addLog(AppConstants.event, AppConstants.tag, "checkTag", "Check Tag api call", reuestMap);

			if (Utils.isEmptyString(requestMap.getOrDefault("tagId", "").toString())) {

				returnmap.put("status", "error");
				returnmap.put("message", AppConstants.fieldsmissing);

				logsServices.addLog(AppConstants.error, AppConstants.tag, "checkTag", AppConstants.fieldsmissing, null);
				return new ResponseEntity<>(returnmap, HttpStatus.OK);

			}

			Tags tags = tagsRepository.findByTagId(requestMap.getOrDefault("tagId", "").toString());

			if (tags == null) {
				returnmap.put("status", "error");
				returnmap.put("message", "Tag ID not found");

				logsServices.addLog(AppConstants.error, AppConstants.tag, "checkTag", "Tag ID not found", null);
				return new ResponseEntity<>(returnmap, HttpStatus.OK);
			}

			if (tags.getStatus().equalsIgnoreCase("free")) {
				returnmap.put("tagStatus", "free");
				returnmap.put("status", "success");
				returnmap.put("tag", tags);
				returnmap.put("message", "Tag is not assigned");

				logsServices.addLog(AppConstants.event, AppConstants.tag, "checkTag",
						"Tag-" + tags.getMacId() + " tag is not assigned ", null);

				return new ResponseEntity<>(returnmap, HttpStatus.OK);
			} else {
				if (tags.getStatus().equalsIgnoreCase("assigned")) {
					returnmap.put("status", "success");
					returnmap.put("tagStatus", "assigned");
					returnmap.put("tag", tags);

					if (tags.getType() != null && tags.getType().equalsIgnoreCase("Materials")) {
						returnmap.put("message",
								"Tag is assigned to " + tags.getType() + ", Work Order No-" + tags.getAssignmentId());

					} else
						returnmap.put("message", "Tag is assigned to " + tags.getType() + ", " + tags.getType()
								+ " Id:-" + tags.getAssignmentId());

					String firstName = "", lastName = "";
					Date fromTime = null;
					Date toTime = null;

					// Work Order No -

					List<String> accessLimit = null;

					if (tags.getType() != null && (tags.getType().equalsIgnoreCase("Visitor")
							|| tags.getType().equalsIgnoreCase("Contractor")
							|| tags.getType().equalsIgnoreCase("Vendor"))) {
						Visitor visitor = visitorRepository.findByVisitorId(Integer.valueOf(tags.getAssignmentId()));
						if (visitor != null) {
							accessLimit = visitor.getAccessLimit();
							fromTime = visitor.getFromTime();
							toTime = visitor.getToTime();
							firstName = visitor.getFirstName();
							lastName = visitor.getLastName();

						}

						returnmap.put("fromTime", fromTime);
						returnmap.put("toTime", toTime);
						returnmap.put("accessLimit", accessLimit);
						returnmap.put("firstName", firstName);
						returnmap.put("lastName", lastName);

					} else if (tags.getType() != null && (tags.getType().equalsIgnoreCase("Inventory")
							|| tags.getType().equalsIgnoreCase("LSLM"))) {

						HashMap<String, String> skuMap = skuEvent.getSKUDetails(tags.getAssignmentId());
						SKUAllocation skuAllocation = skuAllocationRepository.findByTagIdAndUpcCode(tags.getTagId(),
								tags.getAssignmentId());

						returnmap.put("skuDetails", skuMap);
						returnmap.put("skuAllocationDetails", skuAllocation);

					} else if (tags.getType() != null && tags.getType().equalsIgnoreCase("warehouse")) {
						String query = warehouseRepository.getWarehouseQuery();
						query = query + " where wh.sku_no='" + tags.getAssignmentId() + "'";

						List<HashMap<String, Object>> warehouseDetails = warehouseRepository.readWarehouseData(query);
						if (warehouseDetails != null && warehouseDetails.size() > 0) {
							returnmap.put("warehouseDetails", warehouseDetails.get(0));
						}
					}

					logsServices.addLog(AppConstants.error, AppConstants.tag, "checkTag",
							returnmap.get("message").toString(), null);
					return new ResponseEntity<>(returnmap, HttpStatus.OK);
				} else {

					returnmap.put("tagStatus", tags.getStatus());
					returnmap.put("status", "success");
					returnmap.put("tag", tags);
					returnmap.put("message", "Tag is " + tags.getStatus());

					logsServices.addLog(AppConstants.event, AppConstants.tag, "checkTag",
							"Tag-" + tags.getMacId() + " tag is " + tags.getStatus(), null);

					return new ResponseEntity<>(returnmap, HttpStatus.OK);
				}
			}
		} catch (Exception e) {
			returnmap.put("status", "error");
			returnmap.put("message", AppConstants.errormsg);

			logsServices.addLog(AppConstants.error, AppConstants.tag, "checkTag", AppConstants.errormsg, null);
			return new ResponseEntity<>(returnmap, HttpStatus.OK);
		}
	}

}
