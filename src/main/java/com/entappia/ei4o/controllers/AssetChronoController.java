package com.entappia.ei4o.controllers;

import java.lang.reflect.Type;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;
import java.util.concurrent.ExecutionException;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.entappia.ei4o.constants.AppConstants;
import com.entappia.ei4o.constants.AppConstants.AssignmentStatus;
import com.entappia.ei4o.constants.AppConstants.AssignmentType;
import com.entappia.ei4o.dbmodels.AssetChrono;
import com.entappia.ei4o.dbmodels.AssetChronoAllocation;
import com.entappia.ei4o.dbmodels.AssetNonChrono;
import com.entappia.ei4o.dbmodels.AssetNonChronoAllocation;
import com.entappia.ei4o.dbmodels.Department;
import com.entappia.ei4o.dbmodels.Staff;
import com.entappia.ei4o.dbmodels.Tags;
import com.entappia.ei4o.models.AssetAssignmentDetails;
import com.entappia.ei4o.repository.AssetChronoAllocationRepository;
import com.entappia.ei4o.repository.AssetChronoRepository;
import com.entappia.ei4o.repository.DepartmentRepository;
import com.entappia.ei4o.repository.StaffRepository;
import com.entappia.ei4o.repository.TagsRepository;
import com.entappia.ei4o.services.LogsServices;
import com.entappia.ei4o.utils.BaseController;
import com.entappia.ei4o.utils.Utils;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

@RestController
public class AssetChronoController extends BaseController {

	@Autowired
	private LogsServices logsServices;

	@Autowired
	private AssetChronoRepository assetChronoRepository;

	@Autowired
	private DepartmentRepository departmentRepository;

	@Autowired
	private StaffRepository staffRepository;

	@Autowired
	private TagsRepository tagsRepository;

	@Autowired
	private AssetChronoAllocationRepository assetChronoAllocationRepository;

	@RequestMapping(path = { "assetChrono", "assetChrono/{assetId}" }, method = RequestMethod.GET)
	public ResponseEntity<?> getAssetChrono(HttpServletRequest request, HttpSession httpSession,
			@PathVariable Optional<String> assetId) throws InterruptedException, ExecutionException {
		Map<String, Object> returnmap = new HashMap<>();
		Map<String, String> reuestMap = new HashMap<>();
		try {

			//"ACVA"
			ResponseEntity<?> responseEntity = checkSessionKey(httpSession, Utils.getCookie(request, "JSESSIONID"));
			if (responseEntity != null) {
				logsServices.addLog(AppConstants.error, AppConstants.ASSTCHO, "getAssetChrono",
						AppConstants.sessiontimeout, null);
				return responseEntity;
			}

			JSONObject jsonObject = new JSONObject();
			if (assetId.isPresent()) {
				jsonObject.put("assetId", assetId.get());
			} else {
				jsonObject.put("assetId", "all");
			}

			reuestMap.put("params", jsonObject.toString());
			logsServices.addLog(AppConstants.event, AppConstants.ASSTCHO, "getAssetChrono", "Asset Chrono Get Api Call",
					reuestMap);

			List<Tags> tagsList = tagsRepository.listOfAssignedTagsByType("Asset Chrono");

			HashMap<String, Tags> tagsMap = new HashMap<>();

			tagsList.forEach(tags -> {
				tagsMap.put(tags.getAssignmentId(), tags);
			});

			List<AssetChrono> assetChronoDetails = new ArrayList<>();

			if (assetId.isPresent()) {
				AssetChrono assetChrono = assetChronoRepository.findByAssetId(assetId.get());
				if (assetChrono == null) {
					returnmap.put("status", "error");
					returnmap.put("message", "AssetChrono id does not exist");
					logsServices.addLog(AppConstants.error, AppConstants.ASSTCHO, "getAssetChrono",
							"AssetVehicle id does not exist", null);

					return new ResponseEntity<>(returnmap, HttpStatus.OK);
				}

				assetChronoDetails.add(assetChrono);
			} else {
				assetChronoDetails = assetChronoRepository.findAll();
			}
			logsServices.addLog(AppConstants.success, AppConstants.ASSTCHO, "getAssetChrono", "AssetChrono details",
					null);

			List<Map<String, Object>> assetChronoMapDetails = new ArrayList<>();

			final ObjectMapper mapper = new ObjectMapper();
			for (AssetChrono assetChrono : assetChronoDetails) {

				Map<String, Object> map = mapper.convertValue(assetChrono, new TypeReference<Map<String, Object>>() {
				});
				Tags tags = tagsMap.get(assetChrono.getAssetId());
				if (tags != null) {
					map.put("assignedTagMacId", tags.getMacId());
					map.put("assignedStatus", tags.getStatus());
				} else {
					map.put("assignedTagMacId", "");
					map.put("assignedStatus", "");
				}

				assetChronoMapDetails.add(map);
			}

			returnmap.put("status", "success");
			returnmap.put("data", assetChronoMapDetails);

			return new ResponseEntity<>(returnmap, HttpStatus.OK);
		} catch (Exception e) {
			returnmap.put("status", "error");
			returnmap.put("message", AppConstants.errormsg);
			logsServices.addLog(AppConstants.error, AppConstants.ASSTCHO, "getAssetChrono", AppConstants.errormsg,
					null);

			return new ResponseEntity<>(returnmap, HttpStatus.OK);
		}

	}

	@PostMapping(value = "/assetChrono")
	public ResponseEntity<?> AddAssetChrono(HttpServletRequest request, HttpSession httpSession,

			@RequestBody HashMap<String, Object> requestMap) throws InterruptedException, ExecutionException {
		Map<String, String> returnmap = new HashMap<>();

		try {

			ResponseEntity<?> responseEntity = checkSessionKey(httpSession, Utils.getCookie(request, "JSESSIONID"),
					"ACAD");
			if (responseEntity != null) {
				logsServices.addLog(AppConstants.error, AppConstants.ASSTCHO, "AddAssetChrono",
						AppConstants.sessiontimeout, null);
				return responseEntity;
			}

			Map<String, String> requestLogMap = new HashMap<>(); 
			requestLogMap.put("params", new JSONObject(requestMap).toString());

			logsServices.addLog(AppConstants.event, AppConstants.ASSTCHO, "AddAssetChrono", "Asset Chrono Add Api Call",
					requestLogMap);

			if (Utils.isEmptyString(requestMap.getOrDefault("assetId", "").toString())
					|| Utils.isEmptyString(requestMap.getOrDefault("assetType", "").toString())
					|| Utils.isEmptyString(requestMap.getOrDefault("subType", "").toString())
					|| Utils.isEmptyString(requestMap.getOrDefault("type", "").toString())
					|| Utils.isEmptyString(requestMap.getOrDefault("manufacturer", "").toString())
					|| Utils.isEmptyString(requestMap.getOrDefault("validityDate", "").toString())) {

				returnmap.put("status", "error");
				returnmap.put("message", AppConstants.fieldsmissing);

				logsServices.addLog(AppConstants.error, AppConstants.ASSTCHO, "AddAssetChrono",
						AppConstants.fieldsmissing, null);
				return new ResponseEntity<>(returnmap, HttpStatus.OK);

			}

			if (!Utils.validateDate(requestMap.getOrDefault("validityDate", "").toString())) {
				returnmap.put("status", "error");
				returnmap.put("message", AppConstants.errorDatemsg);

				logsServices.addLog(AppConstants.error, AppConstants.ASSTCHO, "AddAssetChrono",
						AppConstants.errorDatemsg, null);
				return new ResponseEntity<>(returnmap, HttpStatus.OK);
			}

			AssetChrono assetChrono = assetChronoRepository
					.findByAssetId(requestMap.getOrDefault("assetId", "").toString());
			if (assetChrono != null && assetChrono.isActive()) {
				returnmap.put("status", "error");
				returnmap.put("message", "AssetChrono id already exist");

				logsServices.addLog(AppConstants.error, AppConstants.ASSTCHO, "AddAssetChrono",
						"AssetVehicle id already exist", null);
				return new ResponseEntity<>(returnmap, HttpStatus.OK);
			}

			assetChrono = new AssetChrono();

			assetChrono.setAssetId(requestMap.getOrDefault("assetId", "").toString());
			assetChrono.setAssetType(requestMap.getOrDefault("assetType", "").toString());
			assetChrono.setSubType(requestMap.getOrDefault("subType", "").toString());
			assetChrono.setType(requestMap.getOrDefault("type", "").toString());
			assetChrono.setRangeValue(requestMap.getOrDefault("rangeValue", "").toString());
			assetChrono.setManufacturer(requestMap.getOrDefault("manufacturer", "").toString());

			assetChrono.setAssetAssignmentDetails(null);
			Date date1 = new SimpleDateFormat("yyyy-MMM-dd")
					.parse(requestMap.getOrDefault("validityDate", "").toString());

			assetChrono.setValidityDate(date1);
			assetChrono.setAssignmentStatus(AssignmentStatus.NOT_ASSIGNED);
			assetChrono.setCreatedDate(new Date());
			assetChrono.setModifiedDate(new Date());
			assetChrono.setActive(true);

			assetChronoRepository.save(assetChrono);

			returnmap.put("status", "success");
			returnmap.put("message", assetChrono.getAssetId() + " added successfully");

			logsServices.addLog(AppConstants.event, AppConstants.ASSTCHO, "AddAssetChrono",
					"AssetChrono ID -" + assetChrono.getAssetId() + " added successfully, ", null);

			return new ResponseEntity<>(returnmap, HttpStatus.OK);
		} catch (Exception e) {
			returnmap.put("status", "error");
			returnmap.put("message", AppConstants.errormsg);

			logsServices.addLog(AppConstants.error, AppConstants.ASSTCHO, "AddAssetChrono", AppConstants.errormsg,
					null);
			return new ResponseEntity<>(returnmap, HttpStatus.OK);
		}
	}

	@PutMapping(value = "/assetChrono")
	public ResponseEntity<?> updateAssetChrono(HttpServletRequest request, HttpSession httpSession,

			@RequestBody HashMap<String, Object> requestMap) throws InterruptedException, ExecutionException {
		Map<String, String> returnmap = new HashMap<>();
		try {

			ResponseEntity<?> responseEntity = checkSessionKey(httpSession, Utils.getCookie(request, "JSESSIONID"),
					"ACED");
			if (responseEntity != null) {

				logsServices.addLog(AppConstants.error, AppConstants.ASSTCHO, "updateAssetChrono",
						AppConstants.sessiontimeout, null);
				return responseEntity;
			}


			Map<String, String> requestLogMap = new HashMap<>(); 
			requestLogMap.put("params", new JSONObject(requestMap).toString());
			
			logsServices.addLog(AppConstants.event, AppConstants.ASSTCHO, "updateAssetChrono",
					"Asset Chrono Update Api Call", requestLogMap);

			if (Utils.isEmptyString(requestMap.getOrDefault("assetId", "").toString())
					|| Utils.isEmptyString(requestMap.getOrDefault("assetType", "").toString())
					|| Utils.isEmptyString(requestMap.getOrDefault("subType", "").toString())
					|| Utils.isEmptyString(requestMap.getOrDefault("type", "").toString()) 
					|| Utils.isEmptyString(requestMap.getOrDefault("manufacturer", "").toString())
					|| Utils.isEmptyString(requestMap.getOrDefault("validityDate", "").toString())) {

				returnmap.put("status", "error");
				returnmap.put("message", AppConstants.fieldsmissing);

				logsServices.addLog(AppConstants.error, AppConstants.ASSTCHO, "updateAssetChrono",
						AppConstants.fieldsmissing, null);
				return new ResponseEntity<>(returnmap, HttpStatus.OK);

			}

			if (!Utils.validateDate(requestMap.getOrDefault("validityDate", "").toString())) {
				returnmap.put("status", "error");
				returnmap.put("message", AppConstants.errorDatemsg);

				logsServices.addLog(AppConstants.error, AppConstants.ASSTCHO, "updateAssetChrono",
						returnmap.get("message"), null);
				return new ResponseEntity<>(returnmap, HttpStatus.OK);
			}

			AssetChrono assetChrono = assetChronoRepository
					.findByAssetId(requestMap.getOrDefault("assetId", "").toString());
			if (assetChrono == null) {
				returnmap.put("status", "error");
				returnmap.put("message", "AssetChrono id not found");

				logsServices.addLog(AppConstants.error, AppConstants.ASSTCHO, "updateAssetChrono",
						"AssetVehicle id already exist", null);
				return new ResponseEntity<>(returnmap, HttpStatus.OK);
			} else if (!assetChrono.isActive()) {
				returnmap.put("status", "error");
				returnmap.put("message", "AssetChrono id is not active");

				logsServices.addLog(AppConstants.error, AppConstants.ASSTCHO, "updateAssetChrono",
						returnmap.get("message").toString(), null);
				return new ResponseEntity<>(returnmap, HttpStatus.OK);
			}

			assetChrono.setAssetId(requestMap.getOrDefault("assetId", "").toString());
			assetChrono.setAssetType(requestMap.getOrDefault("assetType", "").toString());
			assetChrono.setSubType(requestMap.getOrDefault("subType", "").toString());
			assetChrono.setType(requestMap.getOrDefault("type", "").toString());
			assetChrono.setRangeValue(requestMap.getOrDefault("rangeValue", "").toString());
			assetChrono.setManufacturer(requestMap.getOrDefault("manufacturer", "").toString());

			Date date1 = new SimpleDateFormat("yyyy-MMM-dd")
					.parse(requestMap.getOrDefault("validityDate", "").toString());

			assetChrono.setValidityDate(date1);
			assetChrono.setActive(true);
			assetChrono.setModifiedDate(new Date());

			assetChronoRepository.save(assetChrono);

			returnmap.put("status", "success");
			returnmap.put("message", assetChrono.getAssetId() + " updated successfully");

			logsServices.addLog(AppConstants.event, AppConstants.ASSTCHO, "updateAssetChrono",
					"AssetChrono ID -" + assetChrono.getAssetId() + " updated successfully, ", null);

			return new ResponseEntity<>(returnmap, HttpStatus.OK);
		} catch (Exception e) {
			returnmap.put("status", "error");
			returnmap.put("message", AppConstants.errormsg);

			logsServices.addLog(AppConstants.error, AppConstants.ASSTCHO, "updateAssetChrono", AppConstants.errormsg,
					null);
			return new ResponseEntity<>(returnmap, HttpStatus.OK);
		}
	}

	@PutMapping(value = "/assignAssetChrono")
	public ResponseEntity<?> assignAssetChrono(HttpServletRequest request, HttpSession httpSession,

			@RequestBody HashMap<String, Object> requestMap) throws InterruptedException, ExecutionException {
		Map<String, String> returnmap = new HashMap<>();
		try {

			ResponseEntity<?> responseEntity = checkSessionKey(httpSession, Utils.getCookie(request, "JSESSIONID"),
					"ACAR");
			if (responseEntity != null) {

				logsServices.addLog(AppConstants.error, AppConstants.ASSTCHO, "assignAssetChrono",
						AppConstants.sessiontimeout, null);
				return responseEntity;
			}


			Map<String, String> requestLogMap = new HashMap<>(); 
			requestLogMap.put("params", new JSONObject(requestMap).toString());
			
			logsServices.addLog(AppConstants.event, AppConstants.ASSTCHO, "assignAssetChrono",
					"Asset Chrono Assign Api Call", requestLogMap);

			if (Utils.isEmptyString(requestMap.getOrDefault("assetId", "").toString())
					|| Utils.isEmptyString(requestMap.getOrDefault("fromTime", "").toString())
					|| Utils.isEmptyString(requestMap.getOrDefault("toTime", "").toString())
					|| Utils.isEmptyString(requestMap.getOrDefault("assignmentType", "").toString())) {

				returnmap.put("status", "error");
				returnmap.put("message", AppConstants.fieldsmissing);

				logsServices.addLog(AppConstants.error, AppConstants.ASSTCHO, "assignAssetChrono",
						AppConstants.fieldsmissing, null);
				return new ResponseEntity<>(returnmap, HttpStatus.OK);

			}

			if (!Utils.validateDateTime(requestMap.getOrDefault("fromTime", "").toString())
					|| !Utils.validateDateTime(requestMap.getOrDefault("toTime", "").toString())) {
				returnmap.put("status", "error");
				returnmap.put("message", AppConstants.errorDateTimemsg);

				logsServices.addLog(AppConstants.error, AppConstants.ASSTCHO, "assignAssetChrono",
						returnmap.get("message"), null);
				return new ResponseEntity<>(returnmap, HttpStatus.OK);
			}

			AssetChrono assetChrono = assetChronoRepository
					.findByAssetId(requestMap.getOrDefault("assetId", "").toString());
			if (assetChrono == null) {
				returnmap.put("status", "error");
				returnmap.put("message", "AssetChrono id not found");

				logsServices.addLog(AppConstants.error, AppConstants.ASSTCHO, "assignAssetChrono",
						"AssetVehicle id already exist", null);
				return new ResponseEntity<>(returnmap, HttpStatus.OK);
			} else if (!assetChrono.isActive()) {
				returnmap.put("status", "error");
				returnmap.put("message", "AssetChrono id is not active");

				logsServices.addLog(AppConstants.error, AppConstants.ASSTCHO, "assignAssetChrono",
						returnmap.get("message").toString(), null);
				return new ResponseEntity<>(returnmap, HttpStatus.OK);
			}

			AssignmentType assignmentType = null;
			String a_type = requestMap.getOrDefault("assignmentType", "").toString();
			if (a_type.equalsIgnoreCase("PERSON"))
				assignmentType = AssignmentType.PERSON;
			else if (a_type.equalsIgnoreCase("DEPT"))
				assignmentType = AssignmentType.DEPT;
			else {
				returnmap.put("status", "error");
				returnmap.put("message", "Invalid assignment type");

				logsServices.addLog(AppConstants.error, AppConstants.ASSTCHO, "assignAssetChrono",
						returnmap.get("message"), null);
				return new ResponseEntity<>(returnmap, HttpStatus.OK);
			}

			Department department = null;
			Staff staff = null;
			String macId = null;

			if (assignmentType == AssignmentType.DEPT) {
				if (Utils.isEmptyString(requestMap.getOrDefault("deptName", "").toString())) {
					returnmap.put("status", "error");
					returnmap.put("message", "Missing department name");

					logsServices.addLog(AppConstants.error, AppConstants.ASSTCHO, "assignAssetChrono",
							returnmap.get("message"), null);
					return new ResponseEntity<>(returnmap, HttpStatus.OK);
				} else {

					department = departmentRepository
							.findByDeptName(requestMap.getOrDefault("deptName", "").toString());
					if (department == null) {
						returnmap.put("status", "error");
						returnmap.put("message", "Department Name is not exist");

						logsServices.addLog(AppConstants.error, AppConstants.ASSTCHO, "assignAssetChrono",
								returnmap.get("message"), null);
						return new ResponseEntity<>(returnmap, HttpStatus.OK);
					} else if (!department.isActive()) {
						returnmap.put("status", "error");
						returnmap.put("message", "Department Name is not active");

						logsServices.addLog(AppConstants.error, AppConstants.ASSTCHO, "assignAssetChrono",
								returnmap.get("message"), null);
						return new ResponseEntity<>(returnmap, HttpStatus.OK);
					}
					macId = department.getDeptName();
				}
			} else if (assignmentType == AssignmentType.PERSON) {
				if (Utils.isEmptyString(requestMap.getOrDefault("empId", "").toString())) {
					returnmap.put("status", "error");
					returnmap.put("message", "Missing Employee Id");

					logsServices.addLog(AppConstants.error, AppConstants.ASSTCHO, "assignAssetChrono",
							returnmap.get("message"), null);
					return new ResponseEntity<>(returnmap, HttpStatus.OK);
				} else {
					staff = staffRepository.findByEmpId(requestMap.getOrDefault("empId", "").toString());
					if (staff == null) {
						returnmap.put("status", "error");
						returnmap.put("message", "Assigned Person id is not exist");

						logsServices.addLog(AppConstants.error, AppConstants.ASSTCHO, "assignAssetChrono",
								returnmap.get("message"), null);
						return new ResponseEntity<>(returnmap, HttpStatus.OK);
					} else if (!staff.isActive()) {
						returnmap.put("status", "error");
						returnmap.put("message", "Assigned Person id is not active");

						logsServices.addLog(AppConstants.error, AppConstants.ASSTCHO, "assignAssetChrono",
								returnmap.get("message"), null);
						return new ResponseEntity<>(returnmap, HttpStatus.OK);
					}
					macId = staff.getEmpId();
				}
			}

			AssetChronoAllocation assetChronoAllocation = null;
			if (assignmentType == AssignmentType.PERSON) {
				assetChronoAllocation = assetChronoAllocationRepository.findByAssetIdAndAssignmentTypeAndMacId(
						assetChrono.getAssetId(), AppConstants.AssignmentType.PERSON, macId);

				if (assetChronoAllocation != null && assetChronoAllocation.getStatus().equalsIgnoreCase("Valid")) {

					returnmap.put("status", "error");
					returnmap.put("message", "AssetNonChrono id is already assigned to Employee ("
							+ assetChronoAllocation.getMacId() + ")");

					logsServices.addLog(AppConstants.error, AppConstants.ASSTCHO, "assignAssetChrono",
							returnmap.get("message"), null);
					return new ResponseEntity<>(returnmap, HttpStatus.OK);
				}
			} else if (assignmentType == AssignmentType.DEPT) {
				assetChronoAllocation = assetChronoAllocationRepository.findByAssetIdAndAssignmentTypeAndMacId(
						assetChrono.getAssetId(), AppConstants.AssignmentType.DEPT, macId);

				if (assetChronoAllocation != null && assetChronoAllocation.getStatus().equalsIgnoreCase("Valid")) {

					returnmap.put("status", "error");
					returnmap.put("message", "AssetNonChrono id is already assigned to Department ("
							+ assetChronoAllocation.getMacId() + ")");

					logsServices.addLog(AppConstants.error, AppConstants.ASSTCHO, "assignAssetChrono",
							returnmap.get("message"), null);
					return new ResponseEntity<>(returnmap, HttpStatus.OK);
				}

			}

			HashMap<String, AssetAssignmentDetails> assignmentDetails = assetChrono.getAssetAssignmentDetails();
			if (assignmentDetails == null) {
				assignmentDetails = new HashMap<>();
			}

			AssetAssignmentDetails assetAssignmentDetails = null;
			if (assignmentDetails.containsKey(macId)) {
				assetAssignmentDetails = assignmentDetails.get(macId);
			}

			if (assetAssignmentDetails == null) {
				assetAssignmentDetails = new AssetAssignmentDetails();
			}

			if (assignmentType == AssignmentType.PERSON) {
				assetAssignmentDetails.setEmpId(macId);
				assetAssignmentDetails.setDeptName("");
				assetAssignmentDetails.setAssignmentType("PERSON");
			} else {

				assetAssignmentDetails.setEmpId("");
				assetAssignmentDetails.setDeptName(macId);
				assetAssignmentDetails.setAssignmentType("DEPT");
			}

			assetAssignmentDetails.setFromTime(requestMap.getOrDefault("fromTime", "").toString());
			assetAssignmentDetails.setToTime(requestMap.getOrDefault("toTime", "").toString());

			assignmentDetails.put(macId, assetAssignmentDetails);
			assetChrono.setModifiedDate(new Date());
			assetChrono.setAssignmentStatus(AssignmentStatus.ASSIGNED);
			assetChronoRepository.save(assetChrono);

			if (assetChronoAllocation == null) {
				assetChronoAllocation = new AssetChronoAllocation();
				assetChronoAllocation.setAssetId(assetChrono.getAssetId());
				assetChronoAllocation.setMacId(macId);
				assetChronoAllocation.setAssignmentType(assignmentType);
			}

			assetChronoAllocation.setStatus("Valid");
			assetChronoAllocation.setAllocationStartTime(new Date());
			assetChronoAllocation.setAllocationEndTime(null);

			assetChronoAllocationRepository.save(assetChronoAllocation);

			returnmap.put("status", "success");
			returnmap.put("message", assetChrono.getAssetId() + " assigned successfully");

			logsServices.addLog(AppConstants.event, AppConstants.ASSTCHO, "assignAssetChrono",
					"AssetChrono ID -" + assetChrono.getAssetId() + " assigned successfully, ", null);

			return new ResponseEntity<>(returnmap, HttpStatus.OK);
		} catch (Exception e) {
			returnmap.put("status", "error");
			returnmap.put("message", AppConstants.errormsg);

			logsServices.addLog(AppConstants.error, AppConstants.ASSTCHO, "assignAssetChrono", AppConstants.errormsg,
					null);
			return new ResponseEntity<>(returnmap, HttpStatus.OK);
		}
	}

	@PostMapping(value = "/returnAssetChrono")
	public ResponseEntity<?> returnAssetChrono(HttpServletRequest request, HttpSession httpSession,

			@RequestBody HashMap<String, Object> requestMap) throws InterruptedException, ExecutionException {
		Map<String, String> returnmap = new HashMap<>();
		try {

			ResponseEntity<?> responseEntity = checkSessionKey(httpSession, Utils.getCookie(request, "JSESSIONID"),
					"ACAR");
			if (responseEntity != null) {
				logsServices.addLog(AppConstants.error, AppConstants.ASSTCHO, "returnAssetChrono",
						AppConstants.sessiontimeout, null);
				return responseEntity;
			}



			Map<String, String> requestLogMap = new HashMap<>(); 
			requestLogMap.put("params", new JSONObject(requestMap).toString());
			
			logsServices.addLog(AppConstants.event, AppConstants.ASSTCHO, "returnAssetChrono",
					"Asset Chrono return Api Call", requestLogMap);

			if (Utils.isEmptyString(requestMap.getOrDefault("assetId", "").toString())
					|| Utils.isEmptyString(requestMap.getOrDefault("assignmentType", "").toString())) {

				returnmap.put("status", "error");
				returnmap.put("message", AppConstants.fieldsmissing);

				logsServices.addLog(AppConstants.error, AppConstants.ASSTCHO, "returnAssetChrono",
						AppConstants.fieldsmissing, null);
				return new ResponseEntity<>(returnmap, HttpStatus.OK);

			}

			AssignmentType assignmentType = null;
			String a_type = requestMap.getOrDefault("assignmentType", "").toString();
			if (a_type.equalsIgnoreCase("PERSON"))
				assignmentType = AssignmentType.PERSON;
			else if (a_type.equalsIgnoreCase("DEPT"))
				assignmentType = AssignmentType.DEPT;
			else {
				returnmap.put("status", "error");
				returnmap.put("message", "Invalid assignment type");

				logsServices.addLog(AppConstants.error, AppConstants.ASSTCHO, "returnAssetChrono",
						returnmap.get("message"), null);
				return new ResponseEntity<>(returnmap, HttpStatus.OK);
			}

			if (assignmentType == AssignmentType.PERSON
					&& Utils.isEmptyString(requestMap.getOrDefault("empId", "").toString())) {
				returnmap.put("status", "error");
				returnmap.put("message", "Missing Employee id");

				logsServices.addLog(AppConstants.error, AppConstants.ASSTCHO, "returnAssetChrono",
						returnmap.get("message"), null);
				return new ResponseEntity<>(returnmap, HttpStatus.OK);
			} else if (assignmentType == AssignmentType.DEPT
					&& Utils.isEmptyString(requestMap.getOrDefault("deptName", "").toString())) {
				returnmap.put("status", "error");
				returnmap.put("message", "Missing Dept Name");

				logsServices.addLog(AppConstants.error, AppConstants.ASSTCHO, "returnAssetChrono",
						returnmap.get("message"), null);
				return new ResponseEntity<>(returnmap, HttpStatus.OK);
			}

			String macId = "";
			AssetChrono assetChrono = assetChronoRepository
					.findByAssetId(requestMap.getOrDefault("assetId", "").toString());
			if (assetChrono == null) {
				returnmap.put("status", "error");
				returnmap.put("message", "AssetChrono id not exist");

				logsServices.addLog(AppConstants.error, AppConstants.ASSTCHO, "returnAssetChrono",
						"AssetChrono id not exist", null);
				return new ResponseEntity<>(returnmap, HttpStatus.OK);
			} else if (!assetChrono.isActive()) {
				returnmap.put("status", "error");
				returnmap.put("message", "AssetChrono id is not active");

				logsServices.addLog(AppConstants.error, AppConstants.ASSTCHO, "returnAssetChrono",
						returnmap.get("message").toString(), null);
				return new ResponseEntity<>(returnmap, HttpStatus.OK);
			}

			if (assignmentType == AssignmentType.PERSON) {
				Staff staff = staffRepository.findByEmpId(requestMap.getOrDefault("empId", "").toString());
				if (staff == null) {
					returnmap.put("status", "error");
					returnmap.put("message", "Assigned Person id is not exist");

					logsServices.addLog(AppConstants.error, AppConstants.ASSTCHO, "returnAssetChrono",
							returnmap.get("message"), null);
					return new ResponseEntity<>(returnmap, HttpStatus.OK);
				} else if (!staff.isActive()) {
					returnmap.put("status", "error");
					returnmap.put("message", "Assigned Person id is not active");

					logsServices.addLog(AppConstants.error, AppConstants.ASSTCHO, "returnAssetChrono",
							returnmap.get("message"), null);
					return new ResponseEntity<>(returnmap, HttpStatus.OK);
				}
				macId = staff.getEmpId();
			} else if (assignmentType == AssignmentType.DEPT) {

				Department department = departmentRepository
						.findByDeptName(requestMap.getOrDefault("deptName", "").toString());
				if (department == null) {
					returnmap.put("status", "error");
					returnmap.put("message", "Department Name is not exist");

					logsServices.addLog(AppConstants.error, AppConstants.ASSTCHO, "returnAssetChrono",
							returnmap.get("message"), null);
					return new ResponseEntity<>(returnmap, HttpStatus.OK);
				} else if (!department.isActive()) {
					returnmap.put("status", "error");
					returnmap.put("message", "Department Name is not active");

					logsServices.addLog(AppConstants.error, AppConstants.ASSTCHO, "returnAssetChrono",
							returnmap.get("message"), null);
					return new ResponseEntity<>(returnmap, HttpStatus.OK);
				}

				macId = department.getDeptName();

			}

			AssetChronoAllocation assetChronoAllocation = assetChronoAllocationRepository
					.findByAssetIdAndMacId(assetChrono.getAssetId(), macId);

			if (assetChronoAllocation == null || !assetChronoAllocation.getStatus().equalsIgnoreCase("valid")) {
				returnmap.put("status", "error");
				returnmap.put("message", "No assignment found for given asset");

				logsServices.addLog(AppConstants.error, AppConstants.ASSTCHO, "returnAssetChrono",
						returnmap.get("message"), null);
				return new ResponseEntity<>(returnmap, HttpStatus.OK);
			}
			assetChronoAllocation.setStatus("Completed");
			assetChronoAllocation.setAllocationEndTime(new Date());
			assetChronoAllocationRepository.save(assetChronoAllocation);

			HashMap<String, AssetAssignmentDetails> assignmentDetails = assetChrono.getAssetAssignmentDetails();
			if (assignmentDetails != null) {
				if (assignmentDetails.containsKey(macId)) {
					assignmentDetails.remove(macId);
				}

				if (assignmentDetails.size() > 0)
					assetChrono.setAssignmentStatus(AssignmentStatus.ASSIGNED);
				else
					assetChrono.setAssignmentStatus(AssignmentStatus.NOT_ASSIGNED);

				assetChrono.setAssetAssignmentDetails(assignmentDetails);
			} else
				assetChrono.setAssignmentStatus(AssignmentStatus.NOT_ASSIGNED);

			assetChrono.setModifiedDate(new Date());

			assetChronoRepository.save(assetChrono);

			returnmap.put("status", "success");
			returnmap.put("message", assetChrono.getAssetId() + " returned successfully");

			logsServices.addLog(AppConstants.event, AppConstants.ASSTCHO, "returnAssetChrono",
					"AssetChrono ID -" + assetChrono.getAssetId() + " updated successfully, ", null);

			return new ResponseEntity<>(returnmap, HttpStatus.OK);
		} catch (Exception e) {
			returnmap.put("status", "error");
			returnmap.put("message", AppConstants.errormsg);

			logsServices.addLog(AppConstants.error, AppConstants.ASSTCHO, "returnAssetChrono", AppConstants.errormsg,
					null);
			return new ResponseEntity<>(returnmap, HttpStatus.OK);
		}
	}

	@PostMapping(value = "/checkAssetChoronoStatus")
	public ResponseEntity<?> checkAssetChoronoStatus(HttpServletRequest request, HttpSession httpSession,

			@RequestBody HashMap<String, Object> requestMap) throws InterruptedException, ExecutionException {
		Map<String, String> returnmap = new HashMap<>();

		try {
			ResponseEntity<?> responseEntity = checkSessionKey(httpSession, Utils.getCookie(request, "JSESSIONID"),
					"ACAR");
			if (responseEntity != null) {
				logsServices.addLog(AppConstants.error, AppConstants.ASSTCHO, "checkAssetChoronoStatus",
						AppConstants.sessiontimeout, null);
				return responseEntity;
			}


			Map<String, String> requestLogMap = new HashMap<>(); 
			requestLogMap.put("params", new JSONObject(requestMap).toString());
			
			logsServices.addLog(AppConstants.event, AppConstants.ASSTCHO, "checkAssetChoronoStatus",
					"Asset Chrono check status Api Call", requestLogMap);

			if (Utils.isEmptyString(requestMap.getOrDefault("assetId", "").toString())) {

				returnmap.put("status", "error");
				returnmap.put("message", AppConstants.fieldsmissing);

				logsServices.addLog(AppConstants.error, AppConstants.ASSTCHO, "checkAssetChoronoStatus",
						AppConstants.fieldsmissing, null);
				return new ResponseEntity<>(returnmap, HttpStatus.OK);

			}

			AssetChrono assetChrono = assetChronoRepository
					.findByAssetId(requestMap.getOrDefault("assetId", "").toString());
			if (assetChrono == null) {
				returnmap.put("status", "error");
				returnmap.put("message", "AssetChrono id not exist");

				logsServices.addLog(AppConstants.error, AppConstants.ASSTCHO, "checkAssetChoronoStatus",
						"AssetChrono id not exist", null);
				return new ResponseEntity<>(returnmap, HttpStatus.OK);
			} else if (!assetChrono.isActive()) {
				returnmap.put("status", "error");
				returnmap.put("message", "AssetChrono id is not active");

				logsServices.addLog(AppConstants.error, AppConstants.ASSTCHO, "checkAssetChoronoStatus",
						returnmap.get("message").toString(), null);
				return new ResponseEntity<>(returnmap, HttpStatus.OK);
			}

			if (assetChrono.getAssignmentStatus() == AssignmentStatus.NOT_ASSIGNED) {
				returnmap.put("status", "error");
				returnmap.put("message", "AssetChrono id is not assigned.");

				logsServices.addLog(AppConstants.error, AppConstants.ASSTCHO, "checkAssetChoronoStatus",
						returnmap.get("message").toString(), null);
				return new ResponseEntity<>(returnmap, HttpStatus.OK);
			}

			HashMap<String, AssetAssignmentDetails> assignmentDetails = assetChrono.getAssetAssignmentDetails();
			if (assignmentDetails != null) {
				Set<String> keySet = assignmentDetails.keySet();
				for (String key : keySet) {
					AssetAssignmentDetails assetAssignmentDetails = assignmentDetails.get(key);
					if (assetAssignmentDetails != null) {
						/*
						 * if(assetAssignmentDetails.getAssignmentType()==AssignmentType.DEPT) {
						 * Department department =
						 * departmentRepository.findByDeptName(assetAssignmentDetails.getDeptName());
						 * 
						 * }else if(assetAssignmentDetails.getAssignmentType()==AssignmentType.PERSON) {
						 * Staff staff = staffRepository.findByEmpId(assetAssignmentDetails.getEmpId());
						 * }
						 */
					}
				}
			}

			return new ResponseEntity<>(returnmap, HttpStatus.OK);
		} catch (Exception e) {
			returnmap.put("status", "error");
			returnmap.put("message", AppConstants.errormsg);

			logsServices.addLog(AppConstants.error, AppConstants.ASSTCHO, "returnAssetChrono", AppConstants.errormsg,
					null);
			return new ResponseEntity<>(returnmap, HttpStatus.OK);
		}
	}

	@DeleteMapping(value = "/assetChrono")
	public ResponseEntity<?> deleteAssetChrono(HttpServletRequest request, HttpSession httpSession,
			@RequestBody HashMap<String, Object> requestMap) throws InterruptedException, ExecutionException {

		Map<String, String> returnmap = new HashMap<>();
		try {
			ResponseEntity<?> responseEntity = checkSessionKey(httpSession, Utils.getCookie(request, "JSESSIONID"),
					"ACED");
			if (responseEntity != null) {
				logsServices.addLog(AppConstants.error, AppConstants.ASSTCHO, "deleteAssetChrono",
						AppConstants.sessiontimeout, null);
				return responseEntity;
			}

			Map<String, String> requestLogMap = new HashMap<>(); 
			requestLogMap.put("params", new JSONObject(requestMap).toString()); 
			
			logsServices.addLog(AppConstants.event, AppConstants.ASSTCHO, "deleteAssetChrono",
					"Asset Chrono delete asset Api Call", requestLogMap);

			if (Utils.isEmptyString(requestMap.getOrDefault("assetId", "").toString())) {

				returnmap.put("status", "error");
				returnmap.put("message", AppConstants.fieldsmissing);

				logsServices.addLog(AppConstants.error, AppConstants.ASSTCHO, "deleteAssetChrono",
						AppConstants.fieldsmissing, null);
				return new ResponseEntity<>(returnmap, HttpStatus.OK);

			}

			AssetChrono assetChrono = assetChronoRepository
					.findByAssetId(requestMap.getOrDefault("assetId", "").toString());

			if (assetChrono == null) {
				returnmap.put("status", "error");
				returnmap.put("message", "Asset Chrono Id not found");

				logsServices.addLog(AppConstants.error, AppConstants.ASSTCHO, "deleteAssetChrono",
						returnmap.get("message").toString(), null);
				return new ResponseEntity<>(returnmap, HttpStatus.OK);
			} else if (!assetChrono.isActive()) {
				returnmap.put("status", "error");
				returnmap.put("message", "Asset Chrono id is not active");

				logsServices.addLog(AppConstants.error, AppConstants.ASSTCHO, "deleteAssetChrono",
						returnmap.get("message").toString(), null);
				return new ResponseEntity<>(returnmap, HttpStatus.OK);
			}

			assetChrono.setActive(false);
			assetChrono.setModifiedDate(new Date());
			assetChronoRepository.save(assetChrono);

			returnmap.put("status", "success");
			returnmap.put("message", "Asset Chrono status updated successfully.");

			logsServices.addLog(AppConstants.event, AppConstants.ASSTCHO, "deleteAssetChrono",
					"Asset Chrono Id-" + assetChrono.getAssetId() + " status updated successfully, ", null);

			return new ResponseEntity<>(returnmap, HttpStatus.OK);
		} catch (Exception e) {
			returnmap.put("status", "error");
			returnmap.put("message", AppConstants.errormsg);

			logsServices.addLog(AppConstants.error, AppConstants.ASSTCHO, "deleteAssetChrono",
					returnmap.get("message").toString(), null);
			return new ResponseEntity<>(returnmap, HttpStatus.OK);
		}
	}

	@PostMapping(value = "/multipleAssetChronos")
	public ResponseEntity<?> addMultipleAssetChronos(HttpServletRequest request, HttpSession httpSession,
			@RequestBody List<HashMap<String, Object>> requestMap) throws InterruptedException, ExecutionException {

		Map<String, Object> returnmap = new HashMap<>();
		try {
			ResponseEntity<?> responseEntity = checkSessionKey(httpSession, Utils.getCookie(request, "JSESSIONID"),
					"ACAD");
			if (responseEntity != null) {
				logsServices.addLog(AppConstants.error, AppConstants.ASSTCHO, "addMultipleAssetChronos",
						AppConstants.sessiontimeout, null);
				return responseEntity;
			}


			
			ObjectMapper objectMapper = new ObjectMapper(); 
			String json = objectMapper.writeValueAsString(requestMap);
			

			Map<String, String> requestLogMap = new HashMap<>(); 
			requestLogMap.put("params", json); 
			
			logsServices.addLog(AppConstants.event, AppConstants.ASSTCHO, "addMultipleAssetChronos",
					"Asset Chrono add multiple asset Api Call", requestLogMap);

			if (requestMap == null || requestMap.size() == 0) {
				returnmap.put("status", "error");
				returnmap.put("message", AppConstants.fieldsmissing);

				logsServices.addLog(AppConstants.error, AppConstants.ASSTCHO, "addMultipleAssetChronos",
						AppConstants.fieldsmissing, null);
				return new ResponseEntity<>(returnmap, HttpStatus.OK);
			}

			List<String> errorIds = new ArrayList<>();
			List<String> savedIds = new ArrayList<>();
			HashMap<String, AssetChrono> assetChronoMap = getAssetChronosNames();

			List<AssetChrono> assetChronoList = new ArrayList<>();
			for (HashMap<String, Object> map : requestMap) {
				if (Utils.isEmptyString(map.getOrDefault("assetId", "").toString())
						|| Utils.isEmptyString(map.getOrDefault("assetType", "").toString())
						|| Utils.isEmptyString(map.getOrDefault("subType", "").toString())
						|| Utils.isEmptyString(map.getOrDefault("type", "").toString())
						|| Utils.isEmptyString(map.getOrDefault("manufacturer", "").toString())
						|| Utils.isEmptyString(map.getOrDefault("validityDate", "").toString())) {

					errorIds.add(map.getOrDefault("assetId", "").toString());

				} else {
					if (!Utils.validateDate(map.getOrDefault("validityDate", "").toString())) {
						errorIds.add(map.getOrDefault("assetId", "").toString());
					} else {

						AssetChrono assetChrono = null;

						if (assetChronoMap.containsKey(map.getOrDefault("assetId", "").toString())) {
							assetChrono = assetChronoMap.get(map.getOrDefault("assetId", "").toString());
							if (assetChrono == null) {
								assetChrono = new AssetChrono();
								assetChrono.setAssetId(map.getOrDefault("assetId", "").toString());
								assetChrono.setActive(true);
								assetChrono.setCreatedDate(new Date());
							}
							assetChrono.setModifiedDate(new Date());
						} else {
							assetChrono = new AssetChrono();
							assetChrono.setCreatedDate(new Date());
							assetChrono.setModifiedDate(new Date());
							assetChrono.setAssetId(map.getOrDefault("assetId", "").toString());
							assetChrono.setActive(true);
						}

						if (assetChrono.isActive()) {

							Date date1 = new SimpleDateFormat("yyyy-MMM-dd")
									.parse(map.getOrDefault("validityDate", "").toString());

							assetChrono.setAssetType(map.getOrDefault("assetType", "").toString());
							assetChrono.setSubType(map.getOrDefault("subType", "").toString());
							assetChrono.setType(map.getOrDefault("type", "").toString());
							assetChrono.setRangeValue(map.getOrDefault("rangeValue", "").toString());
							assetChrono.setManufacturer(map.getOrDefault("manufacturer", "").toString());
							assetChrono.setValidityDate(date1);

							assetChronoList.add(assetChrono);

							savedIds.add(map.getOrDefault("assetId", "").toString());
						} else {
							errorIds.add(map.getOrDefault("assetId", "").toString());
						}
					}
				}
			}

			if (assetChronoList.size() > 0)
				assetChronoRepository.saveAll(assetChronoList);

			returnmap.put("status", "success");
			returnmap.put("message", savedIds.size() + " AssetChronos added successfully.");

			returnmap.put("savedIds", savedIds);
			returnmap.put("errorIds", errorIds);

			logsServices.addLog(AppConstants.event, AppConstants.ASSTCHO, "addMultipleAssetChronos",
					"AssetChrono- saved ids [" + String.join(",", savedIds) + "], error ids ["
							+ String.join(",", errorIds) + "].",
					null);

			return new ResponseEntity<>(returnmap, HttpStatus.OK);
		} catch (Exception e) {
			returnmap.put("status", "error");
			returnmap.put("message", AppConstants.errormsg);

			logsServices.addLog(AppConstants.error, AppConstants.ASSTCHO, "addMultipleAssetChronos",
					returnmap.get("message").toString(), null);
			return new ResponseEntity<>(returnmap, HttpStatus.OK);
		}
	}

	private List<String> getDepartmentNames() {
		// TODO Auto-generated method stub

		List<String> list = new ArrayList<>();

		departmentRepository.findAll().stream().forEach(department -> {
			if (department.isActive()) {
				list.add(department.getDeptName());
			}
		});

		return list;
	}

	private HashMap<String, AssetChrono> getAssetChronosNames() {
		// TODO Auto-generated method stub

		HashMap<String, AssetChrono> map = new HashMap<String, AssetChrono>();

		assetChronoRepository.findAll().stream().forEach(assetChrono -> {
			if (assetChrono.isActive()) {
				map.put(assetChrono.getAssetId(), assetChrono);
			}
		});

		return map;
	}

}
