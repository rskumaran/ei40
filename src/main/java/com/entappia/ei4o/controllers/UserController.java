package com.entappia.ei4o.controllers;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ExecutionException;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.entappia.ei4o.constants.AppConstants;
import com.entappia.ei4o.dbmodels.Staff;
import com.entappia.ei4o.dbmodels.Tags;
import com.entappia.ei4o.dbmodels.Visitor;
import com.entappia.ei4o.dbmodels.VisitorAllocation;
import com.entappia.ei4o.repository.StaffRepository;
import com.entappia.ei4o.repository.TagsRepository;
import com.entappia.ei4o.repository.VisitorAllocationRepository;
import com.entappia.ei4o.repository.VisitorRepository;
import com.entappia.ei4o.services.LogsServices;
import com.entappia.ei4o.utils.BaseController;
import com.entappia.ei4o.utils.Utils;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;

@RestController
public class UserController extends BaseController {

	@Autowired
	private LogsServices logsServices;

	@Autowired
	private StaffRepository staffRepository;

	@Autowired
	private VisitorRepository visitorRepository;

	@Autowired
	private TagsRepository tagsRepository;

	@Autowired
	private VisitorAllocationRepository visitorAllocationRepository;

	@RequestMapping(path = { "allUsers" }, method = RequestMethod.GET)
	public ResponseEntity<?> getTag(HttpServletRequest request, HttpSession httpSession)
			throws InterruptedException, ExecutionException {
		Map<String, Object> returnmap = new HashMap<>();

		try {
 
			ResponseEntity<?> responseEntity = checkSessionKey(httpSession, Utils.getCookie(request, "JSESSIONID"));
			if (responseEntity != null) { 
				
				logsServices.addLog(AppConstants.error, AppConstants.GAU, "getAllUser",
					AppConstants.sessiontimeout, null);
				return responseEntity; 
			}
			
			logsServices.addLog(AppConstants.event, AppConstants.GAU, "getAllUser",
					"Get All User api call" , null);

			List<Staff> staffDetails = new ArrayList<>();
			List<Visitor> visitorsDetails = new ArrayList<>();

			staffDetails = staffRepository.findAll();
			visitorsDetails = visitorRepository.findAll();

			Map<String, Object> dataMap = new HashMap<>();
			dataMap.put("staffs", staffDetails);
			dataMap.put("others", visitorsDetails);

			returnmap.put("status", "success");
			returnmap.put("data", dataMap);

			logsServices.addLog(AppConstants.error, AppConstants.GAU, "getAllUser", "getAllUser", null);
			return new ResponseEntity<>(returnmap, HttpStatus.OK);
		} catch (Exception e) {
			returnmap.put("status", "error");
			returnmap.put("message", AppConstants.errormsg);
			logsServices.addLog(AppConstants.error, AppConstants.GAU, "getAllUser", AppConstants.errormsg, null);

			return new ResponseEntity<>(returnmap, HttpStatus.OK);
		}
	}

	@RequestMapping(path = { "otherUsers" }, method = RequestMethod.GET)
	public ResponseEntity<?> getOtherUsers(HttpServletRequest request, HttpSession httpSession)
			throws InterruptedException, ExecutionException {
		Map<String, Object> returnmap = new HashMap<>();

		try {

			ResponseEntity<?> responseEntity = checkSessionKey(httpSession, Utils.getCookie(request, "JSESSIONID"));
			if (responseEntity != null) { 
				
				logsServices.addLog(AppConstants.error, AppConstants.GAU, "getOtherUsers",
					AppConstants.sessiontimeout, null);
				return responseEntity; 
			}
			
			logsServices.addLog(AppConstants.event, AppConstants.GAU, "getOtherUsers",
					"Get Other User api call" , null);
			
			List<Tags> visitorTagsList = tagsRepository.listOfAssignedTagsByType("Visitor");

			HashMap<String, Tags> tagsMap = new HashMap<>();

			visitorTagsList.forEach(tags -> {
				tagsMap.put(tags.getAssignmentId(), tags);
			});

			List<Tags> vendorTagsList = tagsRepository.listOfAssignedTagsByType("Vendor");
			vendorTagsList.forEach(tags -> {
				tagsMap.put(tags.getAssignmentId(), tags);
			});
			List<Tags> contractorTagsList = tagsRepository.listOfAssignedTagsByType("Contractor");
			contractorTagsList.forEach(tags -> {
				tagsMap.put(tags.getAssignmentId(), tags);
			});

			List<Visitor> visitorsDetails = new ArrayList<>();

			visitorsDetails = visitorRepository.findAll();

			List<Map<String, Object>> visitorsMapDetails = new ArrayList<>();

			final ObjectMapper mapper = new ObjectMapper();
			for (Visitor visitor : visitorsDetails) {

				Map<String, Object> map = mapper.convertValue(visitor, new TypeReference<Map<String, Object>>() {
				});
				Tags tags = tagsMap.get("" + visitor.getVisitorId());
				if (tags != null) {
					map.put("assignedTagMacId", tags.getMacId());
					map.put("assignedStatus", tags.getStatus());

					VisitorAllocation visitorAllocation = visitorAllocationRepository
							.findByVisitorIdAndMacId(visitor.getVisitorId(), tags.getMacId());
					if (visitorAllocation != null) {

						map.put("status", visitorAllocation.getStatus());
						map.put("allocationStartTime",
								Utils.formatDateTime(visitorAllocation.getAllocationStartTime()));
						map.put("allocationEndTime", Utils.formatDateTime(visitorAllocation.getAllocationEndTime()));

					} else {
						map.put("status", "");
						map.put("allocationStartTime", "");
						map.put("allocationEndTime", "");
					}
				} else {
					map.put("assignedTagMacId", "");
					map.put("status", "");
					map.put("allocationStartTime", "");
					map.put("allocationEndTime", "");
					map.put("assignedStatus", "");
				}

				visitorsMapDetails.add(map);
			}

			Map<String, Object> dataMap = new HashMap<>();
			dataMap.put("otherUsers", visitorsMapDetails);

			returnmap.put("status", "success");
			returnmap.put("data", dataMap);

			logsServices.addLog(AppConstants.error, AppConstants.GAU, "getOtherUsers", "getOtherUsers", null);
			return new ResponseEntity<>(returnmap, HttpStatus.OK);
		} catch (Exception e) {
			returnmap.put("status", "error");
			returnmap.put("message", AppConstants.errormsg);
			logsServices.addLog(AppConstants.error, AppConstants.GAU, "getOtherUsers", AppConstants.errormsg, null);

			return new ResponseEntity<>(returnmap, HttpStatus.OK);
		}
	}

}
