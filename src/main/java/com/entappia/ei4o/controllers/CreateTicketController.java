package com.entappia.ei4o.controllers;

import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.CompletableFuture;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.scheduling.annotation.Async;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

import com.entappia.ei4o.constants.AppConstants;
import com.entappia.ei4o.services.LogsServices;
import com.entappia.ei4o.utils.BaseController;
import com.entappia.ei4o.utils.Preference;
import com.entappia.ei4o.utils.RestErrorHandler;
import com.entappia.ei4o.utils.Utils;

@RestController
public class CreateTicketController extends BaseController {

	@Autowired
	private LogsServices logsServices;

	public static final String ZOHO_ACCESS_TOKEN = "zoho_access_token";
	public static final String ZOHO_ACCESS_TIME = "zoho_access_time";

	public static final String hostUrl = "https://accounts.zoho.com";
	public static final String client_id = "1000.ABCC4XVB11AYIDSZ8ISMVCYL54RUPX";
	public static final String client_secret = "1a732d6d676929041129e1bf45afed918151a63775";
	public static final String refresh_token = "1000.78fa32daf2547577abfb3ac31ef90b35.a1aaced2dd69ee5c286a9d35115d42fe";
	public static final String scope = "aaaserver.profile.read,Desk.tickets.ALL";

	public static final String ORG_ID = "725083593";
	public static final String CONTACT_ID = "541727000000747013";
	public static final String DEPARTMENT_ID = "541727000000006907";

	Preference preference = new Preference(); 

	@PostMapping(value = "/createTicket")
	public ResponseEntity<?> saveEnvironmentConfig(HttpServletRequest request, HttpSession httpSession,
			@RequestBody HashMap<String, Object> requestMap) {
		HashMap<String, Object> returnmap = new HashMap<>();

		ResponseEntity<?> responseEntity = checkSessionKey(httpSession, Utils.getCookie(request, "JSESSIONID"));
		if (responseEntity != null) {
			logsServices.addLog(AppConstants.error, AppConstants.ZOHO_DESK, "createTicket", AppConstants.sessiontimeout,
					null);
			return responseEntity;

		}

		if (Utils.isEmptyString(requestMap.getOrDefault("subject", "").toString())
				|| Utils.isEmptyString(requestMap.getOrDefault("description", "").toString())) {

			returnmap.put("status", "error");
			returnmap.put("message", AppConstants.fieldsmissing);

			logsServices.addLog(AppConstants.error, AppConstants.ZOHO_DESK, "createTicket", AppConstants.fieldsmissing,
					null);
			return new ResponseEntity<>(returnmap, HttpStatus.OK);

		}

		try {

			String mailId = (String) httpSession.getAttribute("mailId");
			 

			String subject = requestMap.getOrDefault("subject", "").toString();
			String description = requestMap.getOrDefault("description", "").toString();

			if (!Utils.isEmptyString(preference.getStringValue(ZOHO_ACCESS_TOKEN))
					&& preference.getLongValue(ZOHO_ACCESS_TIME) != 0) {
				if (System.currentTimeMillis() >= (preference.getLongValue(ZOHO_ACCESS_TIME))) {
					boolean status = accessToken(subject, description, mailId);
					if (status) {
						logsServices.addLog(AppConstants.event, AppConstants.ZOHO_DESK, "createTicket",
								"Ticket Created successfully by " + mailId, null);

						returnmap.put("status", "success");
						returnmap.put("message", "Ticket Created successfully");
					} else {
						logsServices.addLog(AppConstants.error, AppConstants.ZOHO_DESK, "createTicket",
								"Faild to create ticket, Requested Email id-" + mailId, null);
						returnmap.put("status", "error");
						returnmap.put("message", "Faild to create ticket.");
					}
				} else {

					CompletableFuture<JSONObject> ticketCompletableFuture = createZOHOTicket(subject, description,
							mailId);
					CompletableFuture.allOf(ticketCompletableFuture).join();

					if (ticketCompletableFuture.isDone()) {
						JSONObject ticketJSONObject = ticketCompletableFuture.get();
						if (ticketJSONObject != null) {
							String ticketStatus = ticketJSONObject.optString("status");
							if (ticketStatus != null && ticketStatus.equals("success")) {
								logsServices.addLog(AppConstants.event, AppConstants.ZOHO_DESK, "createTicket",
										"Ticket Created successfully by " + mailId, null);

								returnmap.put("status", "success");
								returnmap.put("message", "Ticket Created successfully");
							} else {
								logsServices.addLog(AppConstants.error, AppConstants.ZOHO_DESK, "createTicket",
										"Faild to create ticket, Requested Email id-" + mailId, null);
								returnmap.put("status", "error");
								returnmap.put("message", "Faild to create ticket.");
							}
						}
					}

				}
			} else {
				boolean status = accessToken(subject, description, mailId);
				if (status) {
					logsServices.addLog(AppConstants.event, AppConstants.ZOHO_DESK, "createTicket",
							"Ticket Created successfully by " + mailId, null);

					returnmap.put("status", "success");
					returnmap.put("message", "Ticket Created successfully");
				} else {
					logsServices.addLog(AppConstants.error, AppConstants.ZOHO_DESK, "createTicket",
							"Faild to create ticket, Requested Email id-" + mailId, null);
					returnmap.put("status", "error");
					returnmap.put("message", "Faild to create ticket.");
				}
			}
		} catch (Exception ex) {
			returnmap.put("status", "error");
			returnmap.put("message", "Faild to create ticket.");
			logsServices.addLog(AppConstants.error, AppConstants.ZOHO_DESK, "createTicket", "Faild to create ticket",
					null);
			ex.printStackTrace();
		}
		return new ResponseEntity<>(returnmap, HttpStatus.OK);
	}

	private boolean accessToken(String subject, String description, String mailId) {

		try {

			CompletableFuture<JSONObject> tokenCompletableFuture = createZOHOAccessToken();
			CompletableFuture.allOf(tokenCompletableFuture).join();

			if (tokenCompletableFuture.isDone()) {
				JSONObject jsonObject = tokenCompletableFuture.get();
				if (jsonObject != null) {
					String status = jsonObject.optString("status");
					if (status != null && status.equals("success")) {
						JSONObject jsonDataObject = jsonObject.getJSONObject("data");

						String error = (String) jsonDataObject.optString("error");
						if (Utils.isEmptyString(error)) {
							String access_token = (String) jsonDataObject.optString("access_token");
							if (!Utils.isEmptyString(access_token)) {
								preference.setStringValue(ZOHO_ACCESS_TOKEN, access_token);
								Long currTimeMills = System.currentTimeMillis() + 3600000;
								preference.setLongValue(ZOHO_ACCESS_TIME, currTimeMills);

								logsServices.addLog(AppConstants.event, AppConstants.ZOHO_DESK, "createTicket",
										"Created ZOHO access token", null);

								CompletableFuture<JSONObject> ticketCompletableFuture = createZOHOTicket(subject,
										description, mailId);
								CompletableFuture.allOf(ticketCompletableFuture).join();

								if (ticketCompletableFuture.isDone()) {
									JSONObject ticketJSONObject = ticketCompletableFuture.get();
									if (ticketJSONObject != null) {
										String ticketStatus = ticketJSONObject.optString("status");
										if (ticketStatus != null && ticketStatus.equals("success")) {
											logsServices.addLog(AppConstants.event, AppConstants.ZOHO_DESK,
													"createTicket", "Ticket Created successfully by " + mailId, null);

											return true;
										} else
											return false;
									}
								}
							}
						} else {
							logsServices.addLog(AppConstants.error, AppConstants.ZOHO_DESK, "createTicket",
									"Error in creating ZOHO access token. Error Message-" + error, null);
							return false;
						}
					} else {
						preference.setStringValue(ZOHO_ACCESS_TOKEN, "");
						preference.setLongValue(ZOHO_ACCESS_TIME, 0);

						logsServices.addLog(AppConstants.error, AppConstants.ZOHO_DESK, "createTicket",
								"Error in creating ZOHO access token", null);

						return false;
					}
				}
			}

		} catch (Exception ex) {
			ex.printStackTrace();
			logsServices.addLog(AppConstants.error, AppConstants.ZOHO_DESK, "createTicket",
					"Error in creating ZOHO access token", null);
		}

		return false;
	}

	@Async
	public CompletableFuture<JSONObject> createZOHOTicket(String subject, String description, String mailId)
			throws InterruptedException {

		String accessTokenurl = "https://desk.zoho.com/api/v1/tickets";

		JSONObject accessTokenData = new JSONObject();

		try {
			RestTemplate restTemplate = new RestTemplate();
			restTemplate.setErrorHandler(new RestErrorHandler());

			// create headers
			HttpHeaders headers = new HttpHeaders();
			// set `content-type` header
			headers.setContentType(MediaType.APPLICATION_JSON);
			headers.set("authorization", "Zoho-oauthtoken " + preference.getStringValue(ZOHO_ACCESS_TOKEN));
			headers.set("orgid", ORG_ID);
			// request body parameters
			Map<String, Object> map = new HashMap<>();
			map.put("subject", subject);
			map.put("contactId", CONTACT_ID);
			map.put("departmentId", DEPARTMENT_ID);
			map.put("description", description);
			map.put("email", mailId);

			// build the request
			HttpEntity<Map<String, Object>> entity = new HttpEntity<>(map, headers);
			// send POST request
			ResponseEntity<String> response = restTemplate.postForEntity(accessTokenurl, entity, String.class);
			if (response.getStatusCodeValue() == 200) {
				System.out.println(response.getBody());
				accessTokenData.put("status", "success");
			} else
				accessTokenData.put("status", "error");
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			accessTokenData.put("status", "error");

		}

		return CompletableFuture.completedFuture(accessTokenData);
	}

	@Async
	public CompletableFuture<JSONObject> createZOHOAccessToken() throws InterruptedException {

		String accessTokenurl = hostUrl + "/oauth/v2/token?grant_type=refresh_token&client_id=" + client_id
				+ "&client_secret=" + client_secret + "&refresh_token=" + refresh_token + "&scope=" + scope;

		JSONObject accessTokenData = new JSONObject();

		try {
			RestTemplate restTemplate = new RestTemplate();
			restTemplate.setErrorHandler(new RestErrorHandler());

			// create headers
			HttpHeaders headers = new HttpHeaders();
			// set `content-type` header
			headers.setContentType(MediaType.APPLICATION_JSON);
			// build the request
			HttpEntity<Map<String, Object>> entity = new HttpEntity<>(headers);
			// send POST request
			ResponseEntity<String> response = restTemplate.postForEntity(accessTokenurl, entity, String.class);
			if (response.getStatusCodeValue() == 200) {
				String data = response.getBody();
				JSONObject jsonObject = new JSONObject(data);

				accessTokenData.put("status", "success");
				accessTokenData.put("data", jsonObject);
			} else {
				accessTokenData.put("status", "error");

				preference.setStringValue(ZOHO_ACCESS_TOKEN, "");
				preference.setLongValue(ZOHO_ACCESS_TIME, 0);

			}
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();

			accessTokenData.put("status", "error");

		}

		return CompletableFuture.completedFuture(accessTokenData);
	}
}
