package com.entappia.ei4o.controllers;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.concurrent.ExecutionException;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.entappia.ei4o.constants.AppConstants;
import com.entappia.ei4o.constants.AppConstants.AssignmentType;
import com.entappia.ei4o.constants.AppConstants.NotificationType;
import com.entappia.ei4o.dbmodels.AssetChrono;
import com.entappia.ei4o.dbmodels.AssetChronoAllocation;
import com.entappia.ei4o.dbmodels.AssetNonChrono;
import com.entappia.ei4o.dbmodels.AssetNonChronoAllocation;
import com.entappia.ei4o.dbmodels.AssetVehicle;
import com.entappia.ei4o.dbmodels.Material;
import com.entappia.ei4o.dbmodels.Notifications;
import com.entappia.ei4o.dbmodels.Staff;
import com.entappia.ei4o.dbmodels.StaffAllocation;
import com.entappia.ei4o.dbmodels.Tags;
import com.entappia.ei4o.dbmodels.VehicleAllocation;
import com.entappia.ei4o.dbmodels.Visitor;
import com.entappia.ei4o.dbmodels.VisitorAllocation;
import com.entappia.ei4o.dbmodels.WorkMaterialAllocation;
import com.entappia.ei4o.notification.NotificationMessages;
import com.entappia.ei4o.repository.AssetChronoAllocationRepository;
import com.entappia.ei4o.repository.AssetChronoRepository;
import com.entappia.ei4o.repository.AssetNonChronoAllocationRepository;
import com.entappia.ei4o.repository.AssetNonChronoRepository;
import com.entappia.ei4o.repository.AssetVehicleRepository;
import com.entappia.ei4o.repository.MaterialRepository;
import com.entappia.ei4o.repository.NotificationsRepository;
import com.entappia.ei4o.repository.StaffAllocationRepository;
import com.entappia.ei4o.repository.StaffRepository;
import com.entappia.ei4o.repository.TagsRepository;
import com.entappia.ei4o.repository.VehicleAllocationRepository;
import com.entappia.ei4o.repository.VisitorAllocationRepository;
import com.entappia.ei4o.repository.VisitorRepository;
import com.entappia.ei4o.repository.WarehouseRepository;
import com.entappia.ei4o.repository.WorkMaterialAllocationRepository;
import com.entappia.ei4o.services.LogsServices;
import com.entappia.ei4o.services.SKUEvent;
import com.entappia.ei4o.utils.BaseController;
import com.entappia.ei4o.utils.Utils;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.deser.ValueInstantiator.Gettable;

@RestController
public class AllocationController extends BaseController {

	@Autowired
	private LogsServices logsServices;

	@Autowired
	private AssetNonChronoAllocationRepository assetNonChronoAllocationRepository;

	@Autowired
	private AssetChronoAllocationRepository assetChronoAllocationRepository;

	@Autowired
	private StaffAllocationRepository staffAllocationRepository;

	@Autowired
	private VisitorAllocationRepository visitorAllocationRepository;

	@Autowired
	private WorkMaterialAllocationRepository workMaterialAllocationRepository;

	@Autowired
	private StaffRepository staffRepository;

	@Autowired
	private VisitorRepository visitorRepository;

	@Autowired
	private MaterialRepository materialRepository;

	@Autowired
	private AssetChronoRepository assetChronoRepository;

	@Autowired
	private AssetNonChronoRepository assetNonChronoRepository;

	@Autowired
	private AssetVehicleRepository assetVehicleRepository;

	@Autowired
	private VehicleAllocationRepository vehicleAllocationRepository;

	@Autowired
	private TagsRepository tagsRepository;

	@Autowired
	private SKUEvent skuEvent;

	@Autowired
	private WarehouseRepository warehouseRepository;

	@PostMapping(value = "/allocation")
	public ResponseEntity<?> addAllocation(HttpServletRequest request, HttpSession httpSession,
			@RequestBody HashMap<String, Object> requestMap) throws InterruptedException, ExecutionException {

		Map<String, String> returnmap = new HashMap<>();
		try {

			ResponseEntity<?> responseEntity = checkSessionKey(httpSession, Utils.getCookie(request, "JSESSIONID"));
			if (responseEntity != null) {
				logsServices.addLog(AppConstants.error, AppConstants.ALLOCATION, "addAllocation",
						AppConstants.sessiontimeout, null);
				return responseEntity;
			}

			Map<String, String> reuestMap = new HashMap<>();
			reuestMap.put("params", new JSONObject(requestMap).toString());

			logsServices.addLog(AppConstants.event, AppConstants.ALLOCATION, "addAllocation", "Allocation API call",
					reuestMap);

			String sType = requestMap.getOrDefault("type", "").toString();

			if (Utils.isEmptyString(requestMap.getOrDefault("macId", "").toString()) || Utils.isEmptyString(sType)) {

				returnmap.put("status", "error");
				returnmap.put("message", AppConstants.fieldsmissing);

				logsServices.addLog(AppConstants.error, AppConstants.ALLOCATION, "addAllocation",
						AppConstants.fieldsmissing, null);
				return new ResponseEntity<>(returnmap, HttpStatus.OK);

			}

			if ((sType.equalsIgnoreCase("employee") || sType.equalsIgnoreCase("workmetrial")
					|| sType.equalsIgnoreCase("asset_nonchrono") || sType.equalsIgnoreCase("asset_chrono")
					|| sType.equalsIgnoreCase("vehicle"))
					&& Utils.isEmptyString(requestMap.getOrDefault("peopleId", "").toString())) {
				returnmap.put("status", "error");
				returnmap.put("message", AppConstants.fieldsmissing);

				logsServices.addLog(AppConstants.error, AppConstants.ALLOCATION, "addAllocation",
						AppConstants.fieldsmissing, null);
				return new ResponseEntity<>(returnmap, HttpStatus.OK);
			}

			if (sType.equalsIgnoreCase("workmetrial")
					&& Utils.isEmptyString(requestMap.getOrDefault("subType", "").toString())) {
				returnmap.put("status", "error");
				returnmap.put("message", AppConstants.fieldsmissing);

				logsServices.addLog(AppConstants.error, AppConstants.ALLOCATION, "addAllocation",
						AppConstants.fieldsmissing, null);
				return new ResponseEntity<>(returnmap, HttpStatus.OK);
			}

			if ((sType.equalsIgnoreCase("visitor") || sType.equalsIgnoreCase("Contractor")
					|| sType.equalsIgnoreCase("Vendor"))
					&& (Utils.isEmptyString(requestMap.getOrDefault("firstName", "").toString())
							|| Utils.isEmptyString(requestMap.getOrDefault("lastName", "").toString())
							|| Utils.isEmptyString(requestMap.getOrDefault("company", "").toString())
							|| Utils.isEmptyString(requestMap.getOrDefault("contactNo", "").toString())
							|| Utils.isEmptyString(requestMap.getOrDefault("date", "").toString())
							|| Utils.isEmptyString(requestMap.getOrDefault("fromTime", "").toString())
							|| Utils.isEmptyString(requestMap.getOrDefault("toTime", "").toString())
							|| Utils.isEmptyString(requestMap.getOrDefault("accessLimit", "").toString())
							|| Utils.isEmptyString(requestMap.getOrDefault("contactPerson", "").toString()))) {
				returnmap.put("status", "error");
				returnmap.put("message", AppConstants.fieldsmissing);

				logsServices.addLog(AppConstants.error, AppConstants.ALLOCATION, "addAllocation",
						AppConstants.fieldsmissing, null);
				return new ResponseEntity<>(returnmap, HttpStatus.OK);
			}

			Tags tags = tagsRepository.findByMacId(requestMap.getOrDefault("macId", "").toString());

			if (tags == null) {
				returnmap.put("status", "error");
				returnmap.put("message", "Tag id not found.");

				logsServices.addLog(AppConstants.error, AppConstants.ALLOCATION, "addAllocation",
						returnmap.get("message"), null);
				return new ResponseEntity<>(returnmap, HttpStatus.OK);
			} else if (tags.getStatus().equalsIgnoreCase("blocked")) {
				returnmap.put("status", "error");
				returnmap.put("message", "Tag id  " + tags.getTagId() + " is blocked.");

				logsServices.addLog(AppConstants.error, AppConstants.ALLOCATION, "addAllocation",
						returnmap.get("message"), null);
				return new ResponseEntity<>(returnmap, HttpStatus.OK);
			} else if (!Utils.isEmptyString(tags.getStatus()) && !tags.getStatus().equalsIgnoreCase("free")) {
				returnmap.put("status", "error");

				if (tags.getStatus().equalsIgnoreCase("assigned"))
					returnmap.put("message", "Tag already assigned to " + tags.getType());
				else
					returnmap.put("message", "Can not assign " + tags.getStatus() + " tag.");

				logsServices.addLog(AppConstants.error, AppConstants.ALLOCATION, "addAllocation",
						returnmap.get("message"), null);
				return new ResponseEntity<>(returnmap, HttpStatus.OK);
			}

			if (sType.equalsIgnoreCase("employee")) {

				Staff staff = staffRepository.findByEmpId(requestMap.getOrDefault("peopleId", "").toString());

				if (staff == null) {
					returnmap.put("status", "error");
					returnmap.put("message", "Employee id not found");

					logsServices.addLog(AppConstants.error, AppConstants.ALLOCATION, "addAllocation",
							returnmap.get("message"), null);
					return new ResponseEntity<>(returnmap, HttpStatus.OK);
				} else if (!staff.isActive()) {
					returnmap.put("status", "error");
					returnmap.put("message", "Employee id is not active");

					logsServices.addLog(AppConstants.error, AppConstants.ALLOCATION, "addAllocation",
							returnmap.get("message"), null);
					return new ResponseEntity<>(returnmap, HttpStatus.OK);
				} else if (staff.getStatus().equalsIgnoreCase("blocked")) {
					returnmap.put("status", "error");
					returnmap.put("message", "Employee id  " + staff.getEmpId() + " is blocked.");

					logsServices.addLog(AppConstants.error, AppConstants.ALLOCATION, "addAllocation",
							returnmap.get("message"), null);
					return new ResponseEntity<>(returnmap, HttpStatus.OK);
				}

				StaffAllocation staffAllocation = staffAllocationRepository.findAllocation(staff.getEmpId());
				if (staffAllocation != null) {

					tags = tagsRepository.findByMacId(staffAllocation.getMacId());

					returnmap.put("status", "error");
					returnmap.put("message", "Employee id is already assigned to " + tags.getTagId());

					logsServices.addLog(AppConstants.error, AppConstants.ALLOCATION, "addAllocation",
							returnmap.get("message"), null);
					return new ResponseEntity<>(returnmap, HttpStatus.OK);
				}

				staffAllocation = staffAllocationRepository.findByStaffIdAndMacId(staff.getEmpId(), tags.getMacId());

				if (staffAllocation == null) {
					staffAllocation = new StaffAllocation();
					staffAllocation.setStaffId(staff.getEmpId());
					staffAllocation.setMacId(tags.getMacId());
				}

				staffAllocation.setStatus("Valid");
				staffAllocation.setAllocationStartTime(new Date());
				staffAllocation.setAllocationEndTime(null);

				staffAllocationRepository.save(staffAllocation);

				updateTag(tags, "Employee", "", "" + staffAllocation.getStaffId(),
						staffAllocation.getAllocationStartTime(), "Assigned");

				returnmap.put("status", "success");
				returnmap.put("message", "Tag id (" + tags.getTagId() + ") assigned to Employee ("
						+ staffAllocation.getStaffId() + ") successfully");

				logsServices.addLog(AppConstants.event, AppConstants.ALLOCATION, "addAllocation",
						returnmap.get("message"), null);

				return new ResponseEntity<>(returnmap, HttpStatus.OK);
			} else if (sType.equalsIgnoreCase("visitor") || sType.equalsIgnoreCase("Contractor")
					|| sType.equalsIgnoreCase("Vendor")) {

				if (!Utils.validateDate(requestMap.getOrDefault("date", "").toString())) {
					returnmap.put("status", "error");
					returnmap.put("message", AppConstants.errorDatemsg);

					logsServices.addLog(AppConstants.error, AppConstants.ALLOCATION, "addAllocation",
							AppConstants.errorDatemsg, null);
					return new ResponseEntity<>(returnmap, HttpStatus.OK);
				}

				Visitor visitor = visitorRepository.findByContactNo(requestMap.get("contactNo").toString());
				VisitorAllocation visitorAllocation = null;

				if (visitor != null) {

					visitorAllocation = visitorAllocationRepository.findAllocation(visitor.getVisitorId());
					if (visitorAllocation != null) {

						tags = tagsRepository.findByMacId(visitorAllocation.getMacId());

						returnmap.put("status", "error");
						returnmap.put("message", "This contact no is already assigned to " + tags.getTagId());

						logsServices.addLog(AppConstants.error, AppConstants.ALLOCATION, "addAllocation",
								returnmap.get("message"), null);
						return new ResponseEntity<>(returnmap, HttpStatus.OK);
					}
				} else {
					visitor = new Visitor();
					visitor.setContactNo(requestMap.getOrDefault("contactNo", "").toString());
				}

				JSONArray operatorJSONArray = new JSONArray(requestMap.getOrDefault("accessLimit", "").toString());

				List<String> accessLimit = new ArrayList<>();

				if (operatorJSONArray != null && operatorJSONArray.length() > 0) {

					for (int i = 0; i < operatorJSONArray.length(); i++) {
						String val = operatorJSONArray.getString(i);
						accessLimit.add(val);
					}
				}

				Date date = new SimpleDateFormat("yyyy-MMM-dd").parse(requestMap.getOrDefault("date", "").toString());
				Date fromTime = new SimpleDateFormat("HH:mm").parse(requestMap.getOrDefault("fromTime", "").toString());
				Date toTime = new SimpleDateFormat("HH:mm").parse(requestMap.getOrDefault("toTime", "").toString());

				visitor.setFirstName(requestMap.getOrDefault("firstName", "").toString());
				visitor.setLastName(requestMap.getOrDefault("lastName", "").toString());
				visitor.setCompany(requestMap.getOrDefault("company", "").toString());
				visitor.setDate(date);
				visitor.setFromTime(fromTime);
				visitor.setToTime(toTime);
				visitor.setType(sType);
				visitor.setAccessLimit(accessLimit);
				visitor.setContactPerson(requestMap.getOrDefault("contactPerson", "").toString());
				visitor.setContactPersonNumber(requestMap.getOrDefault("contactPersonNumber", "").toString());

				Visitor visitor1 = visitorRepository.save(visitor);

				visitorAllocation = visitorAllocationRepository.findByVisitorIdAndMacId(visitor1.getVisitorId(),
						tags.getMacId());

				if (visitorAllocation == null) {
					visitorAllocation = new VisitorAllocation();
					visitorAllocation.setVisitorId(visitor1.getVisitorId());
					visitorAllocation.setMacId(tags.getMacId());
				}

				visitorAllocation.setStatus("Valid");
				visitorAllocation.setAllocationStartTime(new Date());
				visitorAllocation.setAllocationEndTime(null);

				visitorAllocationRepository.save(visitorAllocation);

				String sType1 = "";
				if (sType.equalsIgnoreCase("visitor"))
					sType1 = "Visitor";
				else if (sType.equalsIgnoreCase("Contractor"))
					sType1 = "Contractor";
				else
					sType1 = "Vendor";

				updateTag(tags, sType1, "", "" + visitorAllocation.getVisitorId(),
						visitorAllocation.getAllocationStartTime(), "Assigned");

				returnmap.put("status", "success");
				returnmap.put("message", "Tag id (" + tags.getTagId() + ") assigned to " + sType1 + " ("
						+ visitorAllocation.getVisitorId() + ") successfully");

				logsServices.addLog(AppConstants.event, AppConstants.ALLOCATION, "addAllocation",
						returnmap.get("message"), null);

				return new ResponseEntity<>(returnmap, HttpStatus.OK);
			} else if (sType.equalsIgnoreCase("workmetrial")) {

				int orderNo = Integer.valueOf(requestMap.get("peopleId").toString());

				List<Material> materialList = materialRepository.findByOrdereNo(orderNo);

				if (materialList == null || materialList.size() == 0) {
					returnmap.put("status", "error");
					returnmap.put("message", "Material order id not found");

					logsServices.addLog(AppConstants.error, AppConstants.ALLOCATION, "addAllocation",
							returnmap.get("message"), null);
					return new ResponseEntity<>(returnmap, HttpStatus.OK);
				} /*
					 * else if (!material.isActive()) { returnmap.put("status", "error");
					 * returnmap.put("message", "Material order id not active");
					 * 
					 * logsServices.addLog(AppConstants.error, AppConstants.ALLOCATION,
					 * "addAllocation", returnmap.get("message"), null); return new
					 * ResponseEntity<>(returnmap, HttpStatus.OK); }
					 */

				WorkMaterialAllocation workMaterialAllocation = workMaterialAllocationRepository
						.findAllocation(orderNo);
				if (workMaterialAllocation != null) {

					returnmap.put("status", "error");
					if (workMaterialAllocation.getStatus() != null
							&& workMaterialAllocation.getStatus().equalsIgnoreCase("valid")) {
						tags = tagsRepository.findByMacId(workMaterialAllocation.getMacId());
						returnmap.put("message", "Work Order Number is already assigned to " + tags.getTagId());
					} else {
						returnmap.put("message", "Work Order processing is completed.");
					}

					logsServices.addLog(AppConstants.error, AppConstants.ALLOCATION, "addAllocation",
							returnmap.get("message"), null);
					return new ResponseEntity<>(returnmap, HttpStatus.OK);
				}

				workMaterialAllocation = new WorkMaterialAllocation();
				workMaterialAllocation.setOrderNo(orderNo);
				workMaterialAllocation.setMacId(tags.getMacId());
				workMaterialAllocation.setStatus("Valid");
				workMaterialAllocation.setAllocationStartTime(new Date());
				workMaterialAllocation.setAllocationEndTime(null);

				workMaterialAllocationRepository.save(workMaterialAllocation);

				updateTag(tags, "Materials", requestMap.getOrDefault("subType", "").toString(),
						"" + workMaterialAllocation.getOrderNo(), workMaterialAllocation.getAllocationStartTime(),
						"Assigned");

				returnmap.put("status", "success");
				returnmap.put("message", "Tag id (" + tags.getTagId() + ") assigned to Work Order No ("
						+ workMaterialAllocation.getOrderNo() + ") successfully");

				logsServices.addLog(AppConstants.event, AppConstants.ALLOCATION, "addAllocation",
						returnmap.get("message"), null);
				return new ResponseEntity<>(returnmap, HttpStatus.OK);
			} else if (sType.equalsIgnoreCase("asset_nonchrono")) {

				AssetNonChrono assetNonChrono = assetNonChronoRepository
						.findByAssetId(requestMap.getOrDefault("peopleId", "").toString());

				if (assetNonChrono == null) {
					returnmap.put("status", "error");
					returnmap.put("message", "Asset NonChrono id not found");

					logsServices.addLog(AppConstants.error, AppConstants.ALLOCATION, "addAllocation",
							returnmap.get("message"), null);
					return new ResponseEntity<>(returnmap, HttpStatus.OK);
				} else if (!assetNonChrono.isActive()) {
					returnmap.put("status", "error");
					returnmap.put("message", "Asset NonChrono id not active");

					logsServices.addLog(AppConstants.error, AppConstants.ALLOCATION, "addAllocation",
							returnmap.get("message"), null);
					return new ResponseEntity<>(returnmap, HttpStatus.OK);
				}

				AssetNonChronoAllocation assetNonChronoAllocation = assetNonChronoAllocationRepository
						.findByAssetIdAndAssignmentTypeAndStatus(assetNonChrono.getAssetId(),
								AppConstants.AssignmentType.TAG, "Valid");

				if (assetNonChronoAllocation != null) {

					tags = tagsRepository.findByMacId(assetNonChronoAllocation.getMacId());

					returnmap.put("status", "error");
					returnmap.put("message", "Asset NonChrono id is already assigned to " + tags.getTagId());

					logsServices.addLog(AppConstants.error, AppConstants.ALLOCATION, "addAllocation",
							returnmap.get("message"), null);
					return new ResponseEntity<>(returnmap, HttpStatus.OK);
				}

				assetNonChronoAllocation = assetNonChronoAllocationRepository.findByAssetIdAndAssignmentTypeAndMacId(
						assetNonChrono.getAssetId(), AppConstants.AssignmentType.TAG, tags.getMacId());

				if (assetNonChronoAllocation != null
						&& assetNonChronoAllocation.getStatus().equalsIgnoreCase("Valid")) {

					tags = tagsRepository.findByMacId(assetNonChronoAllocation.getMacId());

					returnmap.put("status", "error");
					returnmap.put("message", "Asset NonChrono id is already assigned to " + tags.getTagId());

					logsServices.addLog(AppConstants.error, AppConstants.ALLOCATION, "addAllocation",
							returnmap.get("message"), null);
					return new ResponseEntity<>(returnmap, HttpStatus.OK);
				}

				if (assetNonChronoAllocation == null || assetNonChronoAllocation.getMacId() != tags.getMacId()) {
					assetNonChronoAllocation = new AssetNonChronoAllocation();
					assetNonChronoAllocation.setAssetId(assetNonChrono.getAssetId());
				}

				assetNonChronoAllocation.setMacId(tags.getMacId());
				assetNonChronoAllocation.setStatus("Valid");
				assetNonChronoAllocation.setAllocationStartTime(new Date());
				assetNonChronoAllocation.setAllocationEndTime(null);
				assetNonChronoAllocation.setAssignmentType(AssignmentType.TAG);

				assetNonChronoAllocationRepository.save(assetNonChronoAllocation);

				updateTag(tags, "Asset NonChrono", assetNonChrono.getType(), assetNonChronoAllocation.getAssetId(),
						assetNonChronoAllocation.getAllocationStartTime(), "Assigned");

				returnmap.put("status", "success");
				returnmap.put("message", "Tag id (" + tags.getTagId() + ") assigned to Asset id ("
						+ assetNonChronoAllocation.getAssetId() + ") successfully");

				logsServices.addLog(AppConstants.event, AppConstants.ALLOCATION, "addAllocation",
						returnmap.get("message"), null);

				return new ResponseEntity<>(returnmap, HttpStatus.OK);
			} else if (sType.equalsIgnoreCase("asset_chrono")) {

				AssetChrono assetChrono = assetChronoRepository
						.findByAssetId(requestMap.getOrDefault("peopleId", "").toString());

				if (assetChrono == null) {
					returnmap.put("status", "error");
					returnmap.put("message", "Asset Chrono id not found");

					logsServices.addLog(AppConstants.error, AppConstants.ALLOCATION, "addAllocation",
							returnmap.get("message"), null);
					return new ResponseEntity<>(returnmap, HttpStatus.OK);
				} else if (!assetChrono.isActive()) {
					returnmap.put("status", "error");
					returnmap.put("message", "Asset Chrono id not active");

					logsServices.addLog(AppConstants.error, AppConstants.ALLOCATION, "addAllocation",
							returnmap.get("message"), null);
					return new ResponseEntity<>(returnmap, HttpStatus.OK);
				}

				AssetChronoAllocation assetChronoAllocation = assetChronoAllocationRepository
						.findByAssetIdAndAssignmentTypeAndStatus(assetChrono.getAssetId(),
								AppConstants.AssignmentType.TAG, "Valid");

				if (assetChronoAllocation != null && assetChronoAllocation.getStatus().equalsIgnoreCase("Valid")) {

					tags = tagsRepository.findByMacId(assetChronoAllocation.getMacId());

					returnmap.put("status", "error");
					returnmap.put("message", "Asset Chrono id is already assigned to " + tags.getTagId());

					logsServices.addLog(AppConstants.error, AppConstants.ALLOCATION, "addAllocation",
							returnmap.get("message"), null);
					return new ResponseEntity<>(returnmap, HttpStatus.OK);
				}

				assetChronoAllocation = assetChronoAllocationRepository.findByAssetIdAndAssignmentTypeAndMacId(
						assetChrono.getAssetId(), AppConstants.AssignmentType.TAG, tags.getMacId());

				if (assetChronoAllocation != null && assetChronoAllocation.getStatus().equalsIgnoreCase("Valid")) {

					tags = tagsRepository.findByMacId(assetChronoAllocation.getMacId());

					returnmap.put("status", "error");
					returnmap.put("message", "Asset Chrono id is already assigned to " + tags.getTagId());

					logsServices.addLog(AppConstants.error, AppConstants.ALLOCATION, "addAllocation",
							returnmap.get("message"), null);
					return new ResponseEntity<>(returnmap, HttpStatus.OK);
				}

				if (assetChronoAllocation == null || assetChronoAllocation.getMacId() != tags.getMacId()) {
					assetChronoAllocation = new AssetChronoAllocation();
					assetChronoAllocation.setAssetId(assetChrono.getAssetId());
				}

				assetChronoAllocation.setMacId(tags.getMacId());
				assetChronoAllocation.setStatus("Valid");
				assetChronoAllocation.setAllocationStartTime(new Date());
				assetChronoAllocation.setAllocationEndTime(null);
				assetChronoAllocation.setAssignmentType(AssignmentType.TAG);

				assetChronoAllocationRepository.save(assetChronoAllocation);

				updateTag(tags, "Asset Chrono", assetChrono.getType(), assetChronoAllocation.getAssetId(),
						assetChronoAllocation.getAllocationStartTime(), "Assigned");

				returnmap.put("status", "success");
				returnmap.put("message", "Tag id (" + tags.getTagId() + ") assigned to Asset id ("
						+ assetChronoAllocation.getAssetId() + ") successfully");

				logsServices.addLog(AppConstants.event, AppConstants.ALLOCATION, "addAllocation",
						returnmap.get("message"), null);

				return new ResponseEntity<>(returnmap, HttpStatus.OK);
			} else if (sType.equalsIgnoreCase("vehicle")) {

				AssetVehicle assetVehicle = assetVehicleRepository
						.findByAssetId(requestMap.getOrDefault("peopleId", "").toString());

				if (assetVehicle == null) {
					returnmap.put("status", "error");
					returnmap.put("message", "Vehicle id not found");

					logsServices.addLog(AppConstants.error, AppConstants.ALLOCATION, "addAllocation",
							returnmap.get("message"), null);
					return new ResponseEntity<>(returnmap, HttpStatus.OK);
				} else if (!assetVehicle.isActive()) {
					returnmap.put("status", "error");
					returnmap.put("message", "Vehicle id not active");

					logsServices.addLog(AppConstants.error, AppConstants.ALLOCATION, "addAllocation",
							returnmap.get("message"), null);
					return new ResponseEntity<>(returnmap, HttpStatus.OK);
				}

				VehicleAllocation vehicleAllocation = vehicleAllocationRepository
						.findAllocation(assetVehicle.getAssetId());
				if (vehicleAllocation != null) {
					tags = tagsRepository.findByMacId(vehicleAllocation.getMacId());

					returnmap.put("status", "error");
					returnmap.put("message", "Vehicle id is already assigned to " + tags.getTagId());

					logsServices.addLog(AppConstants.error, AppConstants.ALLOCATION, "addAllocation",
							returnmap.get("message"), null);
					return new ResponseEntity<>(returnmap, HttpStatus.OK);
				}

				vehicleAllocation = vehicleAllocationRepository.findByVehicleIdAndMacId(assetVehicle.getAssetId(),
						tags.getMacId());

				if (vehicleAllocation == null) {
					vehicleAllocation = new VehicleAllocation();
					vehicleAllocation.setVehicleId(assetVehicle.getAssetId());
					vehicleAllocation.setMacId(tags.getMacId());
				}

				vehicleAllocation.setStatus("Valid");
				vehicleAllocation.setAllocationStartTime(new Date());
				vehicleAllocation.setAllocationEndTime(null);

				vehicleAllocationRepository.save(vehicleAllocation);

				updateTag(tags, "Vehicles", "", assetVehicle.getAssetId(), vehicleAllocation.getAllocationStartTime(),
						"Assigned");

				returnmap.put("status", "success");
				returnmap.put("message", "Tag id (" + tags.getTagId() + ") assigned to Vehicle id ("
						+ vehicleAllocation.getVehicleId() + ") successfully");

				logsServices.addLog(AppConstants.event, AppConstants.ALLOCATION, "addAllocation",
						returnmap.get("message"), null);

				return new ResponseEntity<>(returnmap, HttpStatus.OK);

			} else {
				returnmap.put("status", "error");
				returnmap.put("message", "Invalid type");

				logsServices.addLog(AppConstants.error, AppConstants.ALLOCATION, "addAllocation",
						returnmap.get("message"), null);
				return new ResponseEntity<>(returnmap, HttpStatus.OK);
			}

		} catch (Exception e) {
			e.printStackTrace();
			returnmap.put("status", "error");
			returnmap.put("message", AppConstants.errormsg);

			logsServices.addLog(AppConstants.error, AppConstants.ALLOCATION, "addAllocation", AppConstants.errormsg,
					null);
			return new ResponseEntity<>(returnmap, HttpStatus.OK);
		}
	}

	@PostMapping(value = "/returnTag")
	public ResponseEntity<?> returnTag(HttpServletRequest request, HttpSession httpSession,
			@RequestBody HashMap<String, Object> requestMap) throws InterruptedException, ExecutionException {
		Map<String, String> returnmap = new HashMap<>();
		try {

			ResponseEntity<?> responseEntity = checkSessionKey(httpSession, Utils.getCookie(request, "JSESSIONID"));
			if (responseEntity != null) {
				logsServices.addLog(AppConstants.error, AppConstants.ALLOCATION, "returnTag",
						AppConstants.sessiontimeout, null);
				return responseEntity;
			}

			Map<String, String> reuestMap = new HashMap<>();
			reuestMap.put("params", new JSONObject(requestMap).toString());

			logsServices.addLog(AppConstants.event, AppConstants.ALLOCATION, "returnTag", "Return tag API", reuestMap);

			String sType = requestMap.getOrDefault("type", "").toString();
			String peopleId = requestMap.getOrDefault("peopleId", "").toString();

			if (Utils.isEmptyString(requestMap.getOrDefault("macId", "").toString()) || Utils.isEmptyString(sType)
					|| Utils.isEmptyString(peopleId)) {

				returnmap.put("status", "error");
				returnmap.put("message", AppConstants.fieldsmissing);

				logsServices.addLog(AppConstants.error, AppConstants.ALLOCATION, "returnTag",
						AppConstants.fieldsmissing, null);
				return new ResponseEntity<>(returnmap, HttpStatus.OK);
			}

			Tags tags = tagsRepository.findByMacId(requestMap.getOrDefault("macId", "").toString());

			if (tags == null) {
				returnmap.put("status", "error");
				returnmap.put("message", "Tag id not found.");

				logsServices.addLog(AppConstants.error, AppConstants.ALLOCATION, "returnTag", returnmap.get("message"),
						null);
				return new ResponseEntity<>(returnmap, HttpStatus.OK);
			} else if (tags.getStatus().equalsIgnoreCase("free")) {
				returnmap.put("status", "error");
				returnmap.put("message", "Tag not assigned to any user");

				logsServices.addLog(AppConstants.error, AppConstants.ALLOCATION, "returnTag", returnmap.get("message"),
						null);
				return new ResponseEntity<>(returnmap, HttpStatus.OK);
			}

			if (sType.equalsIgnoreCase("Employee")) {

				StaffAllocation staffAllocation = staffAllocationRepository.findByStaffIdAndMacId(peopleId,
						tags.getMacId());

				if (staffAllocation != null) {
					staffAllocation.setStatus("Completed");
					staffAllocation.setAllocationEndTime(new Date());
					staffAllocationRepository.save(staffAllocation);

					returnTag(tags, "Free");

					returnmap.put("status", "success");
					returnmap.put("message", "Tag returned successfully.");

					logsServices.addLog(AppConstants.event, AppConstants.ALLOCATION, "returnTag",
							returnmap.get("message"), null);

					return new ResponseEntity<>(returnmap, HttpStatus.OK);

				} else {
					returnmap.put("status", "error");
					returnmap.put("message",
							"No assigned data found for Employee Id-" + peopleId + " and Mac id-" + tags.getMacId());

					logsServices.addLog(AppConstants.error, AppConstants.ALLOCATION, "returnTag",
							returnmap.get("message"), null);

					return new ResponseEntity<>(returnmap, HttpStatus.OK);
				}

			} else if (sType.equalsIgnoreCase("visitor") || sType.equalsIgnoreCase("Contractor")
					|| sType.equalsIgnoreCase("Vendor")) {

				VisitorAllocation visitorAllocation = visitorAllocationRepository
						.findByVisitorIdAndMacId(Integer.valueOf(peopleId), tags.getMacId());

				if (visitorAllocation != null) {

					visitorAllocation.setStatus("Completed");
					visitorAllocation.setAllocationEndTime(new Date());
					visitorAllocationRepository.save(visitorAllocation);

					/*
					 * if (tags.getStatus().equalsIgnoreCase("lost")) { Notifications notification =
					 * new Notifications(); notification.setMessage("Lost Tag returned. Tag id- " +
					 * tags.getTagId()); notification.setNotifyTime(new Date());
					 * notification.setType(NotificationType.INFORMATION);
					 * notification.setMessageObj(null); notification.setSource("EI40");
					 * notificationRepository.save(notification);
					 * notificationMessages.lostTagReturnAlert(tags.getTagId(), Utils.getDate(new
					 * Date())); }
					 */
					returnTag(tags, "Free");

					returnmap.put("status", "success");
					returnmap.put("message", "Tag returned successfully.");

					logsServices.addLog(AppConstants.event, AppConstants.ALLOCATION, "returnTag",
							returnmap.get("message"), null);

				} else {
					returnmap.put("status", "error");
					returnmap.put("message",
							"No assigned data found for Id-" + peopleId + " and Mac id-" + tags.getMacId());

					logsServices.addLog(AppConstants.error, AppConstants.ALLOCATION, "returnTag",
							returnmap.get("message"), null);

					return new ResponseEntity<>(returnmap, HttpStatus.OK);
				}

			} else if (sType.equalsIgnoreCase("workmetrial")) {

				WorkMaterialAllocation workMaterialAllocation = workMaterialAllocationRepository
						.findByOrderNoAndMacId(Integer.valueOf(peopleId), tags.getMacId());

				if (workMaterialAllocation != null) {
					workMaterialAllocation.setStatus("Completed");
					workMaterialAllocation.setAllocationEndTime(new Date());
					workMaterialAllocationRepository.save(workMaterialAllocation);

					returnTag(tags, "Free");

					returnmap.put("status", "success");
					returnmap.put("message", "Tag returned successfully.");

					logsServices.addLog(AppConstants.event, AppConstants.ALLOCATION, "returnTag",
							returnmap.get("message"), null);
				} else {
					returnmap.put("status", "error");
					returnmap.put("message",
							"No assigned data found for Material Id-" + peopleId + " and Mac id-" + tags.getMacId());

					logsServices.addLog(AppConstants.error, AppConstants.ALLOCATION, "returnTag",
							returnmap.get("message"), null);

					return new ResponseEntity<>(returnmap, HttpStatus.OK);
				}

			} else if (sType.equalsIgnoreCase("asset_nonchrono")) {

				AssetNonChronoAllocation assetNonChronoAllocation = assetNonChronoAllocationRepository
						.findByAssetIdAndMacId(peopleId, tags.getMacId());

				if (assetNonChronoAllocation != null
						&& assetNonChronoAllocation.getStatus().equalsIgnoreCase("Valid")) {
					assetNonChronoAllocation.setStatus("Completed");
					assetNonChronoAllocation.setAllocationEndTime(new Date());
					assetNonChronoAllocationRepository.save(assetNonChronoAllocation);

					returnTag(tags, "Free");

					returnmap.put("status", "success");
					returnmap.put("message", "Tag returned successfully.");

					logsServices.addLog(AppConstants.event, AppConstants.ALLOCATION, "returnTag",
							returnmap.get("message"), null);
				} else {
					returnmap.put("status", "error");
					returnmap.put("message", "No assigned data found for Asset NonChrono Id-" + peopleId
							+ " and Mac id-" + tags.getMacId());

					logsServices.addLog(AppConstants.error, AppConstants.ALLOCATION, "returnTag",
							returnmap.get("message"), null);

					return new ResponseEntity<>(returnmap, HttpStatus.OK);
				}

			} else if (sType.equalsIgnoreCase("asset_chrono")) {

				AssetChronoAllocation assetChronoAllocation = assetChronoAllocationRepository
						.findByAssetIdAndMacId(peopleId, tags.getMacId());

				if (assetChronoAllocation != null && assetChronoAllocation.getStatus().equalsIgnoreCase("Valid")) {
					assetChronoAllocation.setStatus("Completed");
					assetChronoAllocation.setAllocationEndTime(new Date());
					assetChronoAllocationRepository.save(assetChronoAllocation);

					returnTag(tags, "Free");

					returnmap.put("status", "success");
					returnmap.put("message", "Tag returned successfully.");

					logsServices.addLog(AppConstants.event, AppConstants.ALLOCATION, "returnTag",
							returnmap.get("message"), null);
				} else {
					returnmap.put("status", "error");
					returnmap.put("message", "No assigned data found for Asset Chrono Id-" + peopleId + " and Mac id-"
							+ tags.getMacId());

					logsServices.addLog(AppConstants.error, AppConstants.ALLOCATION, "returnTag",
							returnmap.get("message"), null);

					return new ResponseEntity<>(returnmap, HttpStatus.OK);
				}
			} else if (sType.equalsIgnoreCase("vehicle")) {

				VehicleAllocation vehicleAllocation = vehicleAllocationRepository.findByVehicleIdAndMacId(peopleId,
						tags.getMacId());

				if (vehicleAllocation != null) {
					vehicleAllocation.setStatus("Completed");
					vehicleAllocation.setAllocationEndTime(new Date());
					vehicleAllocationRepository.save(vehicleAllocation);

					returnTag(tags, "Free");

					returnmap.put("status", "success");
					returnmap.put("message", "Tag returned successfully.");

					logsServices.addLog(AppConstants.event, AppConstants.ALLOCATION, "returnTag",
							returnmap.get("message"), null);
				} else {
					returnmap.put("status", "error");
					returnmap.put("message",
							"No assigned data found for Vehicle Id-" + peopleId + " and Mac id-" + tags.getMacId());

					logsServices.addLog(AppConstants.error, AppConstants.ALLOCATION, "returnTag",
							returnmap.get("message"), null);

					return new ResponseEntity<>(returnmap, HttpStatus.OK);
				}
			} else if (sType.equalsIgnoreCase("warehouse")) {

				HashMap<String, Object> map = warehouseRepository.getWarehouseBySKUNo(peopleId);
				if (map != null) {
					if (!map.getOrDefault("tag_id", "").toString().equalsIgnoreCase(tags.getTagId())) {

						returnmap.put("status", "error");
						returnmap.put("message", "Warehouse SKU Number not assigned to " + tags.getTagId());

						logsServices.addLog(AppConstants.error, AppConstants.ALLOCATION, "returnTag",
								returnmap.get("message"), null);

						return new ResponseEntity<>(returnmap, HttpStatus.OK);

					} else if (!map.getOrDefault("status", "").toString().equalsIgnoreCase("valid")) {
						returnmap.put("status", "error");
						returnmap.put("message", "Warehouse SKU Number not assigned to any tags");

						logsServices.addLog(AppConstants.error, AppConstants.ALLOCATION, "returnTag",
								returnmap.get("message"), null);

						return new ResponseEntity<>(returnmap, HttpStatus.OK);
					} else {
						returnmap.put("status", "success");
						returnmap.put("message", "Tags returned successfully.");

						returnTag(tags, "Free");

						String currentTime = Utils.formatDateTime1(new Date());
						String query = "UPDATE warehouse SET modified_date ='" + currentTime + "', dispatch_date = '"
								+ currentTime + "', status='Completed' WHERE sku_no='" + peopleId + "';";
						boolean insert = warehouseRepository.updateWarehouse(query);
						if (AppConstants.print_log)
							System.out.println(insert ? "Updated successfully" : "Error in Updated");

						logsServices.addLog(AppConstants.event, AppConstants.ALLOCATION, "returnTag",
								returnmap.get("message"), null);

						return new ResponseEntity<>(returnmap, HttpStatus.OK);
					}
				} else {
					returnmap.put("status", "error");
					returnmap.put("message", "Warehouse SKU Number-" + peopleId + " not found.");

					logsServices.addLog(AppConstants.error, AppConstants.ALLOCATION, "returnTag",
							returnmap.get("message"), null);

					return new ResponseEntity<>(returnmap, HttpStatus.OK);
				}
			}

		} catch (Exception e) {
			returnmap.put("status", "error");
			returnmap.put("message", AppConstants.errormsg);

			logsServices.addLog(AppConstants.error, AppConstants.ALLOCATION, "returnTag", AppConstants.errormsg, null);
			return new ResponseEntity<>(returnmap, HttpStatus.OK);
		}

		return new ResponseEntity<>(returnmap, HttpStatus.OK);
	}

	@SuppressWarnings("unchecked")
	@RequestMapping(path = { "assignmentDetails/{macId}" }, method = RequestMethod.GET)
	public ResponseEntity<?> getAssignmentDetails(HttpServletRequest request, HttpSession httpSession,
			@PathVariable Optional<String> macId) throws InterruptedException, ExecutionException {

		Map<String, Object> returnmap = new HashMap<>();

		try {

			ResponseEntity<?> responseEntity = checkSessionKey(httpSession, Utils.getCookie(request, "JSESSIONID"));
			if (responseEntity != null) {
				logsServices.addLog(AppConstants.error, AppConstants.ALLOCATION, "getAssignmentDetails",
						AppConstants.sessiontimeout, null);
				return responseEntity;
			}

			JSONObject jsonObject = new JSONObject();
			jsonObject.put("macId", macId.get());
			Map<String, String> reuestMap = new HashMap<>();
			reuestMap.put("params", jsonObject.toString());

			logsServices.addLog(AppConstants.event, AppConstants.ALLOCATION, "getAssignmentDetails",
					"Assignment Details get api", reuestMap);

			Tags tagDetails = tagsRepository.findByMacId(macId.get());
			if (tagDetails == null) {
				returnmap.put("status", "error");
				returnmap.put("message", "Mac ID does not exist");
				logsServices.addLog(AppConstants.error, AppConstants.ALLOCATION, "getAssignmentDetails",
						"Mac ID does not exist", null);

				return new ResponseEntity<>(returnmap, HttpStatus.OK);
			} else if (tagDetails.getStatus() == null || !tagDetails.getStatus().equalsIgnoreCase("Assigned")) {
				returnmap.put("status", "error");
				returnmap.put("message", tagDetails.getTagId() + " not assigned");
				logsServices.addLog(AppConstants.error, AppConstants.ALLOCATION, "getAssignmentDetails",
						returnmap.get("message").toString(), null);

				return new ResponseEntity<>(returnmap, HttpStatus.OK);
			}

			String sType = tagDetails.getType();
			returnmap.put("type", sType);
			returnmap.put("macId", tagDetails.getMacId());
			returnmap.put("tagId", tagDetails.getTagId());
			returnmap.put("status", "success");

			String sAssignmentId = tagDetails.getAssignmentId();
			if (sType.equalsIgnoreCase("Employee")) {

				Staff staff = staffRepository.findByEmpId(sAssignmentId);
				returnmap.put(sType, staff);

			} else if (sType.equalsIgnoreCase("visitor") || sType.equalsIgnoreCase("Contractor")
					|| sType.equalsIgnoreCase("Vendor")) {

				Visitor visitor = visitorRepository.findByVisitorId(Integer.valueOf(sAssignmentId));
				returnmap.put(sType, visitor);
			} else if (sType.equalsIgnoreCase("Materials")) {

				List<Material> materialList = materialRepository.findByOrdereNo(Integer.valueOf(sAssignmentId));
				returnmap.put(sType, materialList);

			} else if (sType.equalsIgnoreCase("Asset NonChrono")) {

				AssetNonChrono assetNonChrono = assetNonChronoRepository.findByAssetId(sAssignmentId);
				returnmap.put(sType, assetNonChrono);
			} else if (sType.equalsIgnoreCase("Asset Chrono")) {
				AssetChrono assetChrono = assetChronoRepository.findByAssetId(sAssignmentId);
				returnmap.put(sType, assetChrono);
			} else if (sType.equalsIgnoreCase("Vehicles")) {
				AssetVehicle assetVehicle = assetVehicleRepository.findByAssetId(sAssignmentId);
				returnmap.put(sType, assetVehicle);
			} else if (sType.equalsIgnoreCase("LSLM") || sType.equalsIgnoreCase("Inventory")) {

				HashMap<String, String> skuMap = skuEvent.getSKUDetails(sAssignmentId);
				returnmap.put(sType, skuMap);
			} else if (sType.equalsIgnoreCase("WareHouse")) {

				String query = warehouseRepository.getWarehouseQuery();
				query = query + " where wh.sku_no='" + sAssignmentId + "'";

				List<HashMap<String, Object>> warehouseDetails = warehouseRepository.readWarehouseData(query);
				if (warehouseDetails != null && warehouseDetails.size() > 0) {
					returnmap.put(sType, warehouseDetails.get(0));
				}
			}

			logsServices.addLog(AppConstants.success, AppConstants.ALLOCATION, "getAssignmentDetails",
					"Assignment details", null);
			return new ResponseEntity<>(returnmap, HttpStatus.OK);
		} catch (Exception e) {
			returnmap.put("status", "error");
			returnmap.put("message", AppConstants.errormsg);
			logsServices.addLog(AppConstants.error, AppConstants.ALLOCATION, "getAssignmentDetails",
					AppConstants.errormsg, null);

			return new ResponseEntity<>(returnmap, HttpStatus.OK);
		}
	}

	private void updateTag(Tags tags, String type, String subType, String assignmentId, Date assignmentDate,
			String status) {
		if (tags != null) {
			tags.setType(type);
			tags.setSubType(subType);
			tags.setAssignmentId(assignmentId);
			tags.setAssignmentDate(assignmentDate);
			tags.setModifiedDate(new Date());
			tags.setStatus(status);

			tagsRepository.save(tags);
		}

	}

	private void returnTag(Tags tags, String status) {
		if (tags != null) {
			tags.setModifiedDate(new Date());
			tags.setStatus(status);
			tags.setType("");
			tags.setSubType("");
			tags.setAssignmentId("");
			tags.setAssignmentDate(null);
			tags.setModifiedDate(new Date());
			tagsRepository.save(tags);
		}

	}

}
